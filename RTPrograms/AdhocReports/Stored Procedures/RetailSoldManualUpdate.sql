﻿



CREATE PROCEDURE [AdhocReports].[RetailSoldManualUpdate] @SEASON INT,  @STATEMENTCODE INT
AS
BEGIN
	/*
	Accepts unlock statementcodde



	*/
	
	BEGIN TRY

	BEGIN TRANSACTION

			UPDATE ST 
				SET ST.RetailerCode = RP.RetailerCode,
					ST.ActiveRetailerCode = Rp.SoldRetailerCode
			--select ST.RetailerCode, RP.RetailerCode,ST.ActiveRetailerCode , Rp.SoldRetailerCode
			FROM RP_Statements ST
			INNER JOIN RetailerProfile RP ON  RP.SoldRetailerCode = ST.RetailerCode AND RP.Status = 'Sold'
			WHERE StatementCode = @STATEMENTCODE

			--UPDATE RPWeb_Statements
			UPDATE ST 
				SET ST.RetailerCode = RP.RetailerCode,
					ST.ActiveRetailerCode = Rp.SoldRetailerCode
			FROM RPWeb_Statements ST
			INNER JOIN RetailerProfile RP ON  RP.SoldRetailerCode = ST.RetailerCode AND RP.Status = 'Sold'
			WHERE StatementCode = @STATEMENTCODE


			--3. UPDATE MAPPINGS
			UPDATE MAP
			SET RetailerCode=RP.RetailerCode
			FROM RP_StatementsMappings MAP   
				INNER JOIN RP_Statements ST        ON ST.StatementCode=MAP.StatementCode
				INNER JOIN RetailerProfile RP    ON RP.SoldRetailerCode=MAP.ActiveRetailerCode AND RP.Status='Sold'
			WHERE ST.Status='Active' AND ST.Season = @SEASON AND ST.StatementType='Actual'
				AND RP.SoldDate > '2019-10-01' AND RP.SoldDate <= '2020-10-01'
				AND MAP.RetailerCode <> RP.RetailerCode
				AND MAP.StatementCode=@STATEMENTCODE
	
		COMMIT TRANSACTION

		END TRY
					
		BEGIN CATCH
			SELECT 'Failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
			ROLLBACK TRANSACTION
			RETURN;	
		END CATCH		
	

	   SELECT 'SUCCESS' AS Status, '' AS ErrorMessage

END
