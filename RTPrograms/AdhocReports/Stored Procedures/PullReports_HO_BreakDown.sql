﻿


CREATE PROCEDURE [AdhocReports].[PullReports_HO_BreakDown] @SEASON INT,  @RETAILERCODE VARCHAR(50)
AS
BEGIN
	
	DROP TABLE IF EXISTS #TEMP
	SELECT L5.Retailercode AS DistributorCode, L5.RetailerNAme AS Distirbutor, L1.RetailerName, L1.RetailerCode, L1.MailingCity, ML.Title AS MarketLetter, PR.Title AS Program
		,OU_Reward 
		,OU_Sales
		,Sales AS Retailer_Sales
		,PR.ProgramCode
		,IIF(OU_Sales=0 OR OU_Reward=0, 0, OU_Reward * (Sales/OU_Sales)) AS Retailer_Reward
	INTO #TEMP
	FROM RPWeb_Statements ST
		INNER JOIN(
		Select RetailerCode,VersionType, MAX(Locked_date) Locked_date FROM RPWeb_Statements where VersionType IN('Locked','Archive') AND Status = 'Active' and StatementType = 'Actual'
		GROUP BY RetailerCode,VersionType
		)MostRecent ON MostRecent.RetailerCode = ST.RetailerCode AND  ST.Locked_Date = MostRecent.Locked_Date AND MostRecent.VersionType = ST.VersionType
		INNER JOIN (
			SELECT Statementcode, MarketLetterCode, ProgramCode, RetailerCode
				,MAX(OU_Reward) AS OU_Reward
				,SUM(Sales) AS Sales
				,MAX(OU_SAles) AS OU_Sales
			FROM [dbo].[RPWeb_HO_BreakDownSummary]
			WHERE ProgramCode IN ('RP2020_W_AGREWARDS_BONUS','RP2020_W_CLL_SUPPORT','RP2020_W_CLW_SUPPORT','RP2020_W_TANK_MIX_BONUS','RP2020_W_NOD_DUO_SCG_REW')
			GROUP BY StatementCode, MarketLetterCode, ProgramCode, RetailerCode
		) D
			ON D.StatementCode=ST.StatementCode
			INNER JOIN RetailerProfile L1 ON L1.RetailerCode=D.RetailerCode
			INNER JOIN RetailerProfile L5 ON L5.RetailerCode=ST.RetailerCode 
			LEFT JOIN RP_Config_MArketLetters ML	ON ML.MarketLetterCode=D.MarketLetterCode
			LEFT JOIN RP_Config_Programs PR			ON PR.ProgramCode=D.ProgramCode
			WHERE ST.Season = @SEASON

	--SELECT * FROM #TEMP

	DROP TABLE IF EXISTS #Summary
	SELECT DistributorCode,Distirbutor,RetailerName,RetailerCode,MailingCity
	--,MarketLetter 
		,SUM(IIF(ProgramCode='RP2020_W_AGREWARDS_BONUS',Retailer_Reward,0)) AS 'Ag Rewards Bonus'
		,SUM(IIF(ProgramCode='RP2020_W_CLL_SUPPORT',Retailer_Reward,0)) AS 'Clearfield Lentils Support Reward'
		,SUM(IIF(ProgramCode='RP2020_W_CLW_SUPPORT',Retailer_Reward,0)) AS 'Clearfield Wheat Support Reward'
		,SUM(IIF(ProgramCode='RP2020_W_TANK_MIX_BONUS',Retailer_Reward,0)) AS 'Liberty/Centurion/Facet L Tank Mix Bonus'
		,SUM(IIF(ProgramCode='RP2020_W_NOD_DUO_SCG_REW',Retailer_Reward,0)) AS 'Nodulator DUO SCG Acquisition Program'
	INTO #Summary
	FROM #TEMP
	GROUP BY DistributorCode,Distirbutor,RetailerName,RetailerCode,MailingCity

	
	--,MarketLetter

--	SELECT * FROM #Summary WHERE DistributorCode='D0000107'
	SELECT * FROM #Summary WHERE DistributorCode = @RETAILERCODE
--	SELECT * FROM #Summary WHERE DistributorCode='D0000137'
--	SELECT * FROM #Summary WHERE DistributorCode='D0000244'

END
