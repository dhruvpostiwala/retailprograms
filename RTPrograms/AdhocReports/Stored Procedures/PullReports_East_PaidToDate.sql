﻿


CREATE PROCEDURE [AdhocReports].[PullReports_East_PaidToDate] @SEASON INT
AS
BEGIN
	DECLARE @SEASON_STRING VARCHAR(4) = CONVERT(VARCHAR(4),@SEASON)
	DECLARE @CPP VARCHAR(50) = 'ML'+ @SEASON_STRING +'_E_CCP';
	DECLARE @INV VARCHAR(50) = 'ML'+@SEASON_STRING+'_E_INV';

	IF @SEASON = 2020
		SET @CPP  = 'ML'+ @SEASON_STRING +'_E_CCP'
	ELSE
		SET @CPP  = 'ML'+ @SEASON_STRING +'_E_CPP'


	DROP TABLE IF EXISTS #TEMP
	
	SELECT Rp.RetailerCode, Rp.RetailerName,ST.StatementLevel
	,SUM(PaidtoDate) as Total
	,SUM(IIF(RS.MarketLetterCode = @INV,PaidtoDate,0)) as 'PreviousPayments - Invigor'
	,SUM(IIF(RS.MarketLetterCode = @CPP,PaidtoDate,0)) as 'PreviousPayments - Crop Protection'
	,SUM(IIF(RS.MarketLetterCode NOT IN(@CPP,@INV),PaidtoDate,0)) as 'PreviousPayments - Other'
	INTO #TEMP
	FROM RPWeb_Statements ST
		INNER JOIN RPWeb_Rewards_Summary RS on RS.STatementCode = ST.StatementCode
		INNER JOIn RetailerProfile RP ON ST.RetailerCode = RP.RetailerCode
	WHERE ST.Season = @SEASON AND ST.Region = 'EAST' and VersionType = 'Unlocked' AND ST.Status = 'Active' and St.StatementType = 'Actual'
	GROUP BY ST.StatementCode,ST.StatementLevel, RP.RetailerCode,Rp.RetailerName

	
	SELECT * FROM #TEMP 


END
