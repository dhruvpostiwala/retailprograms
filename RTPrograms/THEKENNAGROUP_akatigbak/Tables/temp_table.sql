﻿CREATE TABLE [THEKENNAGROUP\akatigbak].[temp_table] (
    [StatementCode] INT      NULL,
    [Amount]        MONEY    NULL,
    [DateCreated]   DATETIME NULL,
    [ModifiedDate]  DATETIME NOT NULL
);

