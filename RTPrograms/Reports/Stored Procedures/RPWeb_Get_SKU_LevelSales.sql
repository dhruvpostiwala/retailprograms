﻿

CREATE  PROCEDURE [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT;	
	DECLARE @SQL_COMMAND nvarchar (max);
	
	
	SELECT @SEASON=Season FROM RPWeb_Statements where StatementCode = @STATEMENTCODE;

	--INSERT INTO @T([MarketLetterCode],[ProductCode],[Sequence],[MarketLetterName],[ProductName],[LYQuantity],[LYSales],[LYEligibleQty],[LYEligibleSales],[CYQuantity],[CYSales],[CYEligibleQty],[CYEligibleSales],[Forecast_Sales])
	SET @SQL_Command = '
	DROP TABLE IF EXISTS #SKU_LEVEL_SALES 	

	DECLARE @SEASON INT;
	DECLARE @CY INT;
	DECLARE @LY INT;
	DECLARE @REGION varchar(10);	
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @STATEMENTLEVEL VARCHAR(20);	
	DECLARE @SUMMARY VARCHAR(3)='''';
	DECLARE @VERSIONTYPE VARCHAR(50)='''';

	DECLARE @TARGET_STATEMENTS TABLE (Statementcode INT NOT NULL)

	SELECT @RETAILERCODE=ST.RetailerCode, @SEASON=ST.Season, @CY=ST.Season, @LY=ST.Season-1, @Region=ST.Region , @STATEMENTLEVEL=ST.StatementLevel
		,@VERSIONTYPE=ST.VersionType
		,@SUMMARY=ISNULL(HO.Summary,''No'')
	FROM RPWeb_Statements ST
		LEFT JOIN RP_Config_HOPaymentLevel HO
		ON HO.Level5Code=ST.RetailerCode AND HO.Season=ST.Season
	WHERE ST.StatementCode=@STATEMENTCODE;

	IF @STATEMENTLEVEL=''LEVEL 5'' AND @SUMMARY=''YES''
		INSERT INTO @TARGET_STATEMENTS(StatementCode)
		SELECT StatementCode
		FROM RPWeb_Statements
		WHERE StatementLevel=''LEVEL 1'' AND DataSetCode_L5=@StatementCode AND Status=''Active''
	ELSE
		INSERT INTO @TARGET_STATEMENTS(StatementCode)
		VALUES(@STATEMENTCODE)



	SELECT TX.MarketLetterCode, TX.ProductCode, ml.[Sequence],
				MAX(ML.Title) AS  MarketLetterName,				
				MAX(PR.ChemicalGroup) ChemicalGroup,
				MAX(PR.ProductName) ProductName, 								
				SUM(TX.Acres_Q_LY1 / PR.Conversion) AS [LYQuantity],				

				SUM(CASE 
					WHEN  TX.Pricing_Type=''SRP'' THEN TX.Price_Q_SRP_LY1
					WHEN  TX.Pricing_Type=''SWP'' THEN TX.Price_Q_SWP_LY1
					ELSE TX.Price_Q_SDP_LY1
				END) [LYSales],

				SUM(TX.Acres_LY1 / PR.Conversion) AS [LYEligibleQty],
				SUM(Price_LY1) AS [LYEligibleSales],
				SUM(TX.Acres_Q_CY/ PR.Conversion) AS [CYQuantity],
				
				SUM(CASE 
					WHEN  TX.Pricing_Type=''SRP'' THEN TX.Price_Q_SRP_CY
					WHEN  TX.Pricing_Type=''SWP'' THEN TX.Price_Q_SWP_CY
					ELSE TX.Price_Q_SDP_CY
				END) [CYSales],
				
				SUM(TX.Acres_CY / PR.Conversion) AS [CYEligibleQty],
				SUM(Price_CY) AS [CYEligibleSales],
				SUM(Price_Forecast) AS [Forecast_Sales]				
	INTO #SKU_LEVEL_SALES
	FROM @TARGET_STATEMENTS  T1
		INNER JOIN RP' + CAST(@SEASON AS VARCHAR(4)) + 'Web_Sales_Consolidated TX
		ON TX.Statementcode=T1.Statementcode
		INNER JOIN ( 
			SELECT ChemicalGroup ,ProductCode, ProductName, IIF(@REGION=''WEST'',ConversionW,ConversionE) AS Conversion
			FROM ProductReference 
			WHERE DOCTYPE IN (10,80)
		) PR 
		ON PR.PRODUCTCODE = TX.PRODUCTCODE
		INNER JOIN (
			SELECT MarketLetterCode, Title, [Sequence] FROM RP_Config_MarketLetters 
				UNION
			SELECT ''NON_REWARD_BRANDS'' MarketLetterCode, ''Non-Reward Brands'' AS  Title, 99 AS [Sequence] 
		) ML 
		ON ML.MarketLetterCode=TX.MarketLetterCode
	WHERE (TX.ProgramCode=''ELIGIBLE_SUMMARY'' AND TX.GroupType=''ELIGIBLE_BRANDS'')
		OR TX.Programcode=''NON_REWARD_BRANDS_SUMMARY''
	GROUP BY TX.MarketLetterCode, TX.ProductCode, ml.[Sequence]		



	/*	
	INSERT INTO #SKU_LEVEL_SALES
	SELECT ''NON_REWARD_BRANDS'' AS MarketLetterCode, tx.ProductCode, 99 as [Sequence],
			''Non-Reward Brands'' MarketLetterName,		   
			MAX(PR.ChemicalGroup) ChemicalGroup, 
			MAX(PR.ProductName) ProductName, 
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.BASFSeason=@LY, tx.Quantity,0)) AS ''LYQuantity'',							
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.BASFSeason=@LY, tx.Price_SDP, 0)) AS ''LYSales'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.InRadius=1 AND TX.BASFSeason=@LY, tx.Quantity,0)) AS ''LYEligibleQty'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.InRadius=1 AND TX.BASFSeason=@LY, tx.Price_SDP, 0)) AS ''LYEligibleSales'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.BASFSeason=@CY, tx.Quantity, 0)) AS ''CYQuantity'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.BASFSeason=@CY, tx.Price_SDP, 0)) AS ''CYSales'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.InRadius=1 AND TX.BASFSeason=@CY, tx.Quantity,0)) AS ''CYEligibleQty'',
			SUM(IIF(TX.TransType=''RET_TRANSACTION'' AND TX.InRadius=1 AND TX.BASFSeason=@CY, tx.Price_SDP, 0)) AS ''CYEligibleSales'',
			SUM(IIF(TX.TransType=''POG_PLAN'',tx.Price_SDP,0)) AS [Forecast_Sales]	
	FROM (
			SELECT ''RET_TRANSACTION'' AS TransType, BASFSeason, InRadius, Productcode, Quantity, Price_SDP
			FROM @TARGET_STATEMENTS  T1
				INNER JOIN RPWeb_Transactions TX
				ON TX.Statementcode=T1.Statementcode
			WHERE BASFSeason IN (@CY,@LY) 
				-- AND (TX.StatementCode=@STATEMENTCODE OR TX.DataSetCode_L5=@STATEMENTCODE)
			
				UNION ALL

			SELECT ''POG_PLAN'' AS TransType, @Season AS BASFSeason, 1  AS InRadius, Productcode, Quantity, Price_SDP
			FROM @TARGET_STATEMENTS  T1
				INNER JOIN RPWeb_ScenarioTransactions TX
				ON TX.Statementcode=T1.Statementcode		
		) TX
		INNER JOIN ProductReference PR	
		ON PR.PRODUCTCODE=TX.PRODUCTCODE
		LEFT JOIN (
			SELECT DISTINCT ProductCode FROM #SKU_LEVEL_SALES					
		) Elg_Brands
		ON Elg_Brands.Productcode=TX.ProductCode
	WHERE Elg_Brands.Productcode IS NULL 
	GROUP BY TX.ProductCode 
	*/

	DELETE #SKU_LEVEL_SALES
	WHERE CHEMICALGROUP IN (''DUET'',''MIZUNA'') AND @RETAILERCODE <> ''D520062427''
	   
	SELECT [MarketLetterCode],[MarketLetterName],[Sequence],[ChemicalGroup],[ProductCode],[ProductName],[LYQuantity],[LYSales],	[LYEligibleQty],[LYEligibleSales],[CYQuantity],[CYSales],[CYEligibleQty],[CYEligibleSales],[Forecast_Sales]  
	FROM  #SKU_LEVEL_SALES 	'

	EXEC SP_EXECUTESQL @SQL_Command, N'@STATEMENTCODE INT',  @STATEMENTCODE


END

