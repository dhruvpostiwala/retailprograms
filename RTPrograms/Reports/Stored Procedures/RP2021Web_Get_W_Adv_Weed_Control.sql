﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_Adv_Weed_Control] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @Level5Code VARCHAR(20);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	SELECT -- will be used to set specific fine print
		@Level5Code = Level5Code
	FROM RPWEB_Statements
	WHERE StatementCode = @StatementCode
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	DROP TABLE IF EXISTS #DETAILS
	SELECT	IIF(ISNULL(GFI.PIPEDAFarmStatus,'')='','No',GFI.PIPEDAFarmStatus) PIPEDAFarmStatus
			,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower_Name
			,T1.Farmcode
			,T1.IMI_Acres
			,T1.Heat_Acres
			,T1.Matched_Acres
			,T1.Reward_Per_Acre
			,T1.Reward
	INTO #DETAILS
	FROM RP2021Web_Rewards_W_Adv_Weed_Control T1
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode=T1.Farmcode
	WHERE T1.Statementcode=@STATEMENTCODE

	

	/* ############################### PRODUCT DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible IMI Acres</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible Heat Acres</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible Matched Acres</th>
			<th>Reward per Acre</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA', ISNULL(Grower_Name,'') ) AS 'td' for xml path(''), type)
		,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(IMI_Acres, '')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Heat_Acres, '')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Matched_Acres, '')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Per_Acre, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY PIPEDAFarmStatus DESC, Grower_Name
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + ISNULL(@ML_TBODY_ROWS,'')  + '</table>		
					</div>
				</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	IF @Level5Code = 'D520062427'-- nutrien
		BEGIN
		SET @DISCLAIMERTEXT = 
		'<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
					<strong>Qualifying IMI Brands:</strong> Odyssey brands, Solo brands, and Viper ADV.
				</div>
				<div class="rprew_subtabletext">
					<strong>Qualifying Heat Brands:</strong> Heat (Pre-Seed), Heat LQ (Pre-Seed) and Heat Complete.
				</div>
				<div class="rprew_subtabletext">
					<strong>Qualification Requirements:</strong> Grower must purchase a minimum of 300 acres in each segment to qualify.
				</div>
			</div>
		 </div>'
		 END
	ELSE
		BEGIN
		SET @DISCLAIMERTEXT = 
		'<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
					<strong>Qualifying IMI Brands:</strong> Odyssey brands, Solo brands, and Viper ADV.
				</div>
				<div class="rprew_subtabletext">
					<strong>Qualifying Heat Brands:</strong> Heat LQ (Pre-Seed) and Heat Complete.
				</div>
				<div class="rprew_subtabletext">
					<strong>Qualification Requirements:</strong> Grower must purchase a minimum of 300 acres in each segment to qualify.
				</div>
			</div>
		 </div>'
		 END
		
	
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

