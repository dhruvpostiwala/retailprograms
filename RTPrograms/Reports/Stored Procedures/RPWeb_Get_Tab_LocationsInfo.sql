﻿
CREATE PROCEDURE [Reports].[RPWeb_Get_Tab_LocationsInfo] @STATEMENTCODE INT, @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT;
	DECLARE @SUMMARY VARCHAR(3);	
	DECLARE @Locations_Data AS NVARCHAR(MAX);

	DECLARE @RP_SOLD_START_DATE VARCHAR(10);
	DECLARE @RP_SOLD_END_DATE VARCHAR(10);

	SELECT @SEASON=SEASON
		,@REGION=Region
		/*
		,@DATASETCODE=CASE
						WHEN (Region='EAST' AND StatementLevel='LEVEL 1') THEN DataSetCode
						WHEN (VersionType = 'Locked') THEN SRC_StatementCode
						ELSE StatementCode
					END
		*/
		,@DATASETCODE=IIF(DataSetCode <> 0, DataSetCode ,StatementCode)
	FROM RPWeb_Statements 
	WHERE StatementCode=@STATEMENTCODE

	SET @SUMMARY = (
		SELECT ISNULL(HO.Summary, 'No')
		FROM RPWeb_Statements ST
			LEFT JOIN RP_Config_HOPaymentLevel HO
			ON HO.Level5Code = ST.RetailerCode AND HO.Season = ST.Season
		WHERE ST.StatementCode = @STATEMENTCODE AND ST.Season = @SEASON AND ST.Status = 'Active' AND ST.StatementType = 'Actual'
	);
	
	SET  @RP_SOLD_START_DATE = CAST(@SEASON-1  AS VARCHAR(4)) + '-10-02';
	SET  @RP_SOLD_END_DATE = CAST(@SEASON AS VARCHAR(4)) + '-10-01';

	
	/* COLLECT SOLD TO START AND END DATES */
	DROP TABLE IF EXISTS #SoldRetails
	SELECT  RetailerCode, SoldRetailerCode
		,Convert(varchar(10), SoldDate, 120) AS  SoldDate
		,RowNum = ROW_NUMBER() OVER(ORDER BY SoldRetailerCode, SoldDate DESC)	
	INTO #SoldRetails
	FROM (
		SELECT RP.RetailerCode, RP.SoldRetailerCode, RP.SoldDate
		FROM RetailerProfile RP
			INNER JOIN (
				SELECT RetailerCode 
				FROM RPWeb_StatementLocations 
				WHERE StatementCode=@STATEMENTCODE
			) ST 		
			ON ST.RetailerCode=RP.RetailerCode 
		WHERE RP.Status='Sold' AND RP.SoldDate BETWEEN CAST(@RP_SOLD_START_DATE AS DATE) AND CAST(@RP_SOLD_END_DATE AS DATE)
	) DATA

	DROP TABLE IF EXISTS #SoldDates
	SELECT lo.RetailerCode, lo.SoldRetailerCode
		,StartDate = ISNULL(hi.SoldDate, @RP_SOLD_START_DATE)
		,EndDate = CASE WHEN lo.SoldDate >= @RP_SOLD_END_DATE  THEN @RP_SOLD_END_DATE  ELSE lo.SoldDate END		
	INTO #SoldDates 
	FROM #SoldRetails lo
		LEFT JOIN #SoldRetails hi 
		ON lo.RowNum = hi.RowNum -1 AND lo.SoldRetailerCode = hi.SoldRetailerCode	
	
	IF @SUMMARY='No' AND @REGION='WEST'
	BEGIN 
		SET @Locations_Data=CONVERT(NVARCHAR(MAX), (	
				SELECT  RP.retailercode as 'td', RP.[Status] as  'td', RP.RetailerName as 'td', RP.MailingCity as 'td', RP.MailingProvince as 'td', RP.RetailerType as 'td'
				,IIF(ISNULL(SD.RetailerCode,'')='','',SD.StartDate) AS 'td'
				,IIF(ISNULL(SD.RetailerCode,'')='','',SD.EndDate) AS 'td'
			FROM RetailerProfile RP
				INNER JOIN RPWeb_StatementLocations ST 		ON ST.RetailerCode=RP.RetailerCode
				LEFT JOIN #SoldDates SD						ON SD.RetailerCode=RP.RetailerCode
			WHERE ST.StatementCode=@STATEMENTCODE
			ORDER BY RetailerType DESC, RetailerName
			FOR XML RAW('tr'), ELEMENTS, TYPE )
		)

		SET @HTML_Data = '<div class="rp_rewards_tab">'
		SET @HTML_Data += '<table class="rp_report_table"><tr><th>Retailer Code</th><th>Retailer Status</th><th>Retailer Name</th><th>City</th><th>Province</th><th>Retailer Type</th><th>Start Date</th><th>End Date</th></tr>' + @Locations_Data + '</table>'
		SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	END

	IF @SUMMARY='No' AND @REGION='EAST'
	BEGIN
			SET @Locations_Data=CONVERT(NVARCHAR(MAX), (	
			/*
					IIF(SRC.[StatementCode]=@Statementcode
						,'<span class="current_statement_icon"></span>'
						,'<a  href="#" onclick="rp_LoadStatement(' + cast(SRC.Statementcode as varchar) + ',''&redirect=false'')"><img alt="Open Statement" title="Open Statement" style="width: 30px; height: 30px;" src="/basf/agprocan/rp2020.nsf/link_icon.jpg" ></a>') AS 'td'
			*/
					SELECT (select 
							IIF(SRC.[StatementCode]=@Statementcode,'star', 'link_icon') [td/@class],
							IIF(SRC.[StatementCode]=@Statementcode,'', 'Open Statement') [td/@title],
							IIF(SRC.[StatementCode]=@Statementcode,'""','rp_LoadStatement(' + cast(SRC.Statementcode as varchar) + ',''&redirect=false'')') [td/@onclick],
							'' as 'td' for xml path(''), type)
					,'<span class="' + IIF(STL.StatementLevel='LEVEL 2','level2_icon','level_one_icon')  + '"></span>' as 'td'
					,STL.retailercode as 'td'
					,RP.[Status] as  'td'
					,RP.RetailerName as 'td'
					,RP.MailingCity as 'td' 
					,RP.MailingProvince as 'td' 
					,RP.RetailerType as 'td'
					,IIF(ISNULL(SD.RetailerCode,'')='','',SD.StartDate) AS 'td'
					,IIF(ISNULL(SD.RetailerCode,'')='','',SD.EndDate) AS 'td'					
					,SRC.[Status] as 'td'
			FROM ( 
				SELECT RetailerCode, StatementLevel
				FROM RPWeb_StatementLocations LOC					
				WHERE StatementCode=@DataSetCode
			) STL
				INNER JOIN RetailerProfile RP	
				ON RP.RetailerCode=STL.RetailerCode				
				INNER JOIN (
					SELECT ST.StatementCode, ST.StatementLevel, ST.RetailerCode, ST.DataSetCode
						, COALESCE(STI_ARC.FieldValue,STI_LK.FieldValue,STI_UL.FieldValue,'Open') AS [Status]						
					FROM RPWeb_Statements ST
						LEFT JOIN RPWeb_StatementInformation STI_UL --UNLOCK
						ON STI_UL.StatementCode=ST.StatementCode AND STI_UL.FieldName='Status'
						LEFT JOIN RPWeb_StatementInformation STI_LK --LOCKED
						ON STI_LK.StatementCode=ST.SRC_StatementCode AND STI_LK.FieldName='Status'
						LEFT JOIN RPWeb_StatementInformation STI_ARC --ARCHIVE
						ON STI_ARC.StatementCode=ST.StatementCode AND STI_ARC.FieldName='Payment_Status'
					WHERE ST.[Status]='Active' AND (ST.[StatementCode]=@DataSetCode /*Level 2 statement */ OR ST.[DataSetCode]=@DATASETCODE /* Level 1 Statement*/)

				) SRC
				ON SRC.RetailerCode=STL.RetailerCode AND SRC.StatementLevel=STL.StatementLevel
				LEFT JOIN #SoldDates SD						
				ON SD.RetailerCode=RP.RetailerCode
			ORDER BY STL.StatementLevel DESC 
			--,RP.RetailerType DESC
			,RP.RetailerName
			FOR XML RAW('tr'), ELEMENTS, TYPE )
		)

		SET @HTML_Data = '<div class="rp_rewards_tab">'
		SET @HTML_Data += '<table class="rp_report_table">
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>		
				<th>Retailer Code</th>				
				<th>Retailer Status</th>
				<th>Retailer Name</th>
				<th>City</th><th>Province</th>
				<th>Retailer Type</th>
				<th>Start Date</th>
				<th>End Date</th>						
				<th>Status</th>
			</tr>' + @Locations_Data + '</table>'
		SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	END
		
	IF @SUMMARY='YES'
	BEGIN
		SET @Locations_Data = CONVERT(NVARCHAR(MAX), (				
					/*
					SELECT (select 
							'link_icon' [td/@class]	
							,'Open Statement' [td/@title] 
							,'rp_LoadStatement(' + cast(SRC.Statementcode as varchar) + ',''&redirect=false&summary=false'')' [td/@onclick]
							,'' as 'td' for xml path(''), type)							
					,'<span icon_col="" class="' + IIF(STL.StatementLevel='LEVEL 2','level2_icon','level_one_icon')  + '"></span>' as 'td'
					*/
					SELECT
				   [STL].[RetailerCode] AS 'td'
				   ,[RP].[Status] AS 'td'
				   ,[RP].[RetailerName] AS 'td'
				   ,[RP].[MailingCity] AS 'td'
				   ,[RP].[MailingProvince] AS 'td'
				   ,[RP].[RetailerType] AS 'td'
				   ,IIF(ISNULL([SD].[RetailerCode], '') = '', '', [SD].[StartDate]) AS 'td'
				   ,IIF(ISNULL([SD].[RetailerCode], '') = '', '', [SD].[EndDate]) AS 'td'
				   ,[STL].[StatementLevel] AS 'td'
				   ,[SRC].[Status] AS 'td'
			FROM (
				SELECT [RetailerCode], [StatementLevel]
				FROM [RPWeb_Statements]
				WHERE [DataSetCode_L5] = @DATASETCODE AND [Status] = 'Active' AND (Region='WEST' OR [StatementLevel] = 'LEVEL 1')
			) [STL]
				INNER JOIN [RetailerProfile] [RP]
				ON [RP].[RetailerCode] = [STL].[RetailerCode]
				INNER JOIN (
					SELECT [ST].[StatementCode], [ST].[StatementLevel], [ST].[RetailerCode]	
						,COALESCE([STI_ARC].[FieldValue], [STI_LK].[FieldValue], [STI_UL].[FieldValue], 'Open') AS [Status]						
					FROM [RPWeb_Statements] [ST]
						LEFT JOIN [RPWeb_StatementInformation] [STI_UL] --UNLOCK
						ON [STI_UL].[StatementCode] = [ST].[StatementCode] AND [STI_UL].[FieldName] = 'Status'
						LEFT JOIN [RPWeb_StatementInformation] [STI_LK] --LOCKED
						ON [STI_LK].[StatementCode] = [ST].[SRC_StatementCode] AND [STI_LK].[FieldName] = 'Status'
						LEFT JOIN [RPWeb_StatementInformation] [STI_ARC] --ARCHIVE
						ON [STI_ARC].[StatementCode] = [ST].[StatementCode] AND [STI_ARC].[FieldName] = 'Payment_Status'
					WHERE [ST].[Status] = 'Active' AND [ST].[DataSetCode_L5] = @DATASETCODE
				) [SRC]
				ON [SRC].[RetailerCode] = [STL].[RetailerCode] AND [SRC].[StatementLevel] = [STL].[StatementLevel]
				LEFT JOIN #SoldDates [SD]
				ON [SD].[RetailerCode] = [RP].[RetailerCode]
			ORDER BY [RP].[RetailerName], [STL].[StatementLevel] DESC				
			FOR XML RAW('tr'), ELEMENTS, TYPE
		));

		SET @HTML_Data = '<div class="rp_rewards_tab">'
		SET @HTML_Data += '<table class="rp_report_table">
								<tr>					
									<th>Retailer Code</th>									
									<th>Retailer Status</th>
									<th>Retailer Name</th>
									<th>City</th>
									<th>Province</th>
									<th>Retailer Type</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Statement Level</th>									
									<th>Statement Status</th>
								</tr>' +
								@Locations_Data +
							'</table>'
		SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	END

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	IF @SEASON <= 2020
			SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE 
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
	
	
	--SELECT @HTML_DATA AS [data]
END

