﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_Program_Exception] 	 
	@STATEMENTCODE INT
	,@PROGRAMCODE Varchar(50)
	,@TEMPLATE_CODE VARCHAR(50)='Default'  
	,@ORGANIC_DATA VARCHAR(MAX)  
	,@EXCEPTION_REWARDTABLE NVARCHAR(MAX)='' OUTPUT
	
AS
BEGIN			
/*
Template : 3C-1R
{
	"sales" : [1234.56,7872.23",
	"reward_margins": [0.05,0.045]
    "col_headers": ["2021 Eligible Reward Brands"], 
	"row_labels": ["Label 1","Label 2"]
}
*/


	DECLARE @SEASON INT
	DECLARE @REWARD_CODE VARCHAR(50)
	DECLARE @STMT_VERSION_TYPE VARCHAR(20)=NULL, @EXCEPTION_VERSION_TYPE VARCHAR(20)=NULL
	DECLARE @TABLE_HEADER_COLUMNS NVARCHAR(MAX)='', @TABLE_BODY_TEXT NVARCHAR(MAX)=''
	
	DECLARE @EXCEPTION_REWARD_PERC_A DECIMAL(6,4), @EXCEPTION_REWARD_PERC_B DECIMAL(6,4), @EXCEPTION_REWARD_PERC_C DECIMAL(6,4), @EXCEPTION_REWARD_PERC_D DECIMAL(6,4)
	DECLARE @EXCEPTION_VALUE_A MONEY, @EXCEPTION_VALUE_B MONEY, @EXCEPTION_VALUE_C MONEY, @EXCEPTION_VALUE_D MONEY
	DECLARE @EXCEPTION_VALUE MONEY
	DECLARE @REWARD_MARGIN_DELTA_A DECIMAL(6,4), @REWARD_MARGIN_DELTA_B DECIMAL(6,4), @REWARD_MARGIN_DELTA_C DECIMAL(6,4), @REWARD_MARGIN_DELTA_D DECIMAL(6,4)

	
	/********************************************/
	SELECT @REWARD_CODE = RewardCode+'X' 	FROM RP_Config_Programs 	WHERE ProgramCode = @PROGRAMCODE
		
	SELECT  @EXCEPTION_VERSION_TYPE = VersionType
			,@EXCEPTION_VALUE = ISNULL(Exception_Value,0)
			,@EXCEPTION_REWARD_PERC_A  = ISNULL(Exception_Reward_Margin_A, 0)			
			,@EXCEPTION_REWARD_PERC_B  = ISNULL(Exception_Reward_Margin_B, 0)			
			,@EXCEPTION_VALUE_A = ISNULL(Exception_Value_A, 0)									
			,@EXCEPTION_VALUE_B = ISNULL(Exception_Value_B, 0)
			--,@EXCEPTION_VALUE_C = ISNULL(Exception_Value_C, 0)									
			--,@EXCEPTION_VALUE_D = ISNULL(Exception_Value_D, 0)
	FROM [dbo].[RPWeb_Exceptions]
	WHERE VersionType = 'Current' AND  StatementCode=@StatementCode AND  [Status]='Approved' AND REWARDCODE = @REWARD_CODE

	IF ISNULL(@EXCEPTION_VERSION_TYPE,'')=''
	BEGIN
		SET @EXCEPTION_REWARDTABLE = ''
		RETURN
	END

	/*****************************************************************/

	-- SELECT @SEASON=Season, @STMT_VERSION_TYPE = VersionType 	FROM RPWeb_Statements  	WHERE StatementCode=@STATEMENTCODE

	DECLARE @COLUMN_HEADER_1 AS VARCHAR(500) = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.col_headers[0]'),'')
	DECLARE @COLUMN_HEADER_2 AS VARCHAR(500) = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.col_headers[1]'),'')
	DECLARE @ROW_LABEL_1 VARCHAR(500) = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.row_labels[0]'),'')
	DECLARE @ROW_LABEL_2 VARCHAR(500) = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.row_labels[1]'),'')
	DECLARE @SALES_A AS MONEY = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.sales[0]'),0)
	DECLARE @SALES_B AS MONEY = ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.sales[1]'),0)
	DECLARE @ORGANIC_REWARD_PERC_A DECIMAL(6,4)	= ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.reward_margins[0]'),0)
	DECLARE @ORGANIC_REWARD_PERC_B DECIMAL(6,4)	= ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.reward_margins[1]'),0)
	DECLARE @ORGANIC_REWARD_PERC_C DECIMAL(6,4)	= ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.reward_margins[2]'),0)
	DECLARE @ORGANIC_REWARD_PERC_D DECIMAL(6,4)	= ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.reward_margins[3]'),0)
	   	 	
	SET @REWARD_MARGIN_DELTA_A = @EXCEPTION_REWARD_PERC_A - @ORGANIC_REWARD_PERC_A  
	SET @REWARD_MARGIN_DELTA_B = @EXCEPTION_REWARD_PERC_B - @ORGANIC_REWARD_PERC_B
	SET @REWARD_MARGIN_DELTA_C = @EXCEPTION_REWARD_PERC_C - @ORGANIC_REWARD_PERC_C  
	SET @REWARD_MARGIN_DELTA_D = @EXCEPTION_REWARD_PERC_D - @ORGANIC_REWARD_PERC_D

	

	IF @TEMPLATE_CODE='Default'
	BEGIN
		SET @TABLE_HEADER_COLUMNS = '
						<th> ' + @COLUMN_HEADER_1 + ' </th> 
						<th>Exception %</th>
						<th>Exception $</th>'
		
		SET @TABLE_BODY_TEXT ='
			<tr>					
				<td class="right_align">'+ [dbo].[SVF_Commify](@SALES_A,'$') +'</td>
				<td class="center_align">'+ [dbo].[SVF_Commify](@REWARD_MARGIN_DELTA_A,'%')  +'</td>
				<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE_A,'$')  +'</td>
			</tr>				
		'
		GOTO FINAL
	END

	-- 4C-2R Four Columns and Two Rows  
	-- 4C-4R Four Columns and Four Rows  
	IF @TEMPLATE_CODE IN ('4C-2R','4C-4R')
	BEGIN
		SET @TABLE_HEADER_COLUMNS = '
				<th> ' + @COLUMN_HEADER_1 + ' </th>
				<th> ' + @COLUMN_HEADER_2 + ' </th>
				<th>Exception %</th>
				<th>Exception $</th>'

		IF @REWARD_MARGIN_DELTA_A > 0
		SET @TABLE_BODY_TEXT += '
			<tr>
				<td class="center_align">'+ @ROW_LABEL_1 +'</td>
				<td class="right_align">'+ [dbo].[SVF_Commify](@SALES_A,'$') +'</td>
				<td class="center_align">'+ [dbo].[SVF_Commify](@REWARD_MARGIN_DELTA_A,'%')  +'</td>
				<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE_A,'$')  +'</td>
			</tr>'

		IF @REWARD_MARGIN_DELTA_B > 0
		SET @TABLE_BODY_TEXT += '
			<tr>
				<td class="center_align">'+ @ROW_LABEL_2 +'</td>
				<td class="right_align">'+ [dbo].[SVF_Commify](@SALES_B,'$') +'</td>
				<td class="center_align">'+ [dbo].[SVF_Commify](@REWARD_MARGIN_DELTA_B,'%')  +'</td>
				<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE_B,'$')  +'</td>
			</tr>
		'		

		IF RIGHT(@TEMPLATE_CODE,2)='4R'
		BEGIN
			IF @REWARD_MARGIN_DELTA_C > 0
			SET @TABLE_BODY_TEXT += '
				<tr>
					<td class="center_align">'+ ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.row_labels[2]'),'') +'</td>
					<td class="right_align">'+ [dbo].[SVF_Commify](ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.sales[2]'),0),'$') +'</td>
					<td class="center_align">'+ [dbo].[SVF_Commify](@REWARD_MARGIN_DELTA_C,'%')  +'</td>
					<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE_C,'$')  +'</td>
				</tr>'

			IF @REWARD_MARGIN_DELTA_D > 0
			SET @TABLE_BODY_TEXT += '
				<tr>
					<td class="center_align">'+ ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.row_labels[3]'),'') +'</td>
					<td class="right_align">'+ [dbo].[SVF_Commify](ISNULL(JSON_VALUE(@ORGANIC_DATA,'$.sales[3]'),0),'$') +'</td>
					<td class="center_align">'+ [dbo].[SVF_Commify](@REWARD_MARGIN_DELTA_B,'%')  +'</td>
					<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE_D,'$')  +'</td>
				</tr>
			'	
		END
			   


		/*
					<tr>
				<td class="total_row" colspan="3">Exceptions Total</td>					
				<td class="right_align">'+ [dbo].[SVF_Commify](@EXCEPTION_VALUE,'$')  +'</td>
			</tr>
		*/
		GOTO FINAL
	END


FINAL:
	SET @EXCEPTION_REWARDTABLE = '
	<div class="st_section">
		<div class="st_content open">
			<div>
				<b>Exception Information</b> 
			</div>
			<table class="rp_report_table"> 
				<thead>	<tr> '+ @TABLE_HEADER_COLUMNS + '</tr></thead>
				<tbody>'+ @TABLE_BODY_TEXT + '</tbody>
			</table>
		</div>
	</div>' 

	SET @EXCEPTION_REWARDTABLE = ISNULL(@EXCEPTION_REWARDTABLE,'')

END