﻿



CREATE PROCEDURE [Reports].[RP2020Web_Get_W_CustomSeedTreatment] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';
	DECLARE @RETAILERCODE VARCHAR(50);
	DECLARE @LEVEL5CODE VARCHAR(20);

	Declare @RewardSummarySection nvarchar(max)='';
	--Declare @HTML_Data as nvarchar(max);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @QUANTITY AS DECIMAL(16,2)=0;
	DECLARE @REWARD MONEY;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';
	DECLARE @ML_INSURE_SECTION AS NVARCHAR(MAX)='';
	DECLARE @ML_NODULATOR_SECTION AS NVARCHAR(MAX)='';
	DECLARE @ML_TOTAL_SECTION AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX)='';
	DECLARE @ML_SUB_TOTAL_ROW AS NVARCHAR(MAX)='';
	DECLARE @ML_TBODY_ROWS AS NVARCHAR(MAX)='';

	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';
	   		
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @RETAILERCODE=RetailerCode, @LEVEL5CODE=Level5Code FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	SET @MARKETLETTERCODE='ML' + @SEASON_STRING + '_W_ST_INC'
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE 
	
	--check for line company for pricing
	DECLARE @LINECOMPANY BIT;
	SET @LINECOMPANY = IIF(@RETAILERCODE IN('D0000244','D0000137','D520062427','D0000107'),1,0);

	IF NOT OBJECT_ID('tempdb..#ProductRef') IS NULL DROP TABLE #ProductRef
	SELECT PR.Productcode, ChemicalGroup, ProductName
		,ProductUOM, ConversionW, Jugs,  Volume
		,ConversionW/Jugs AS AcresPerJug
		,ConversionW/Volume as AcresPerLitre
		,Volume/Jugs AS LitresPerJug						
		,PRP.SDP
		,PRP.SDP/Jugs AS PricePerJug_SDP
		,PRP.SDP/Volume AS PricePerLitre_SDP
		,PRP.SWP
		,PRP.SWP/Jugs AS PricePerJug_SWP
		,PRP.SWP/Volume AS PricePerLitre_SWP
	INTO #ProductRef
	FROM ProductReference PR
		INNER JOIN ProductReferencePricing PRP
		ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'							
	WHERE Chemicalgroup in ('INSURE PULSE','INSURE CEREAL','INSURE CEREAL FX4')  OR Product='Nodulator PRO 100'

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand
		,RewardBrand_Sales, RewardBrand_Acres, RewardBrand_Litres
		,CustomApp_Sales, CustomApp_Acres, CustomApp_Litres
		,Rewardable_Sales, Rewardable_Acres, Rewardable_Litres
		,Reward_Percentage, Reward
	INTO #TEMP
	FROM RP2020Web_Rewards_W_CustomSeed 
	WHERE [Statementcode]=@Statementcode AND [MarketLetterCode]=@MarketLetterCode


/*	   	
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>					
					</div>
				</div>' 	
		GOTO FINAL
	END 				   				   
*/

	IF NOT OBJECT_ID('tempdb..#SKULevelDetails') IS NULL DROP TABLE #SKULevelDetails

	-- SELECT ISNULL(actual.GroupLabel,cap.GroupLabel) as RewardBrand
	SELECT P.ChemicalGroup AS RewardBrand
		,ISNULL(actual.ProductName,cap.ProductName) as productname
		,ISNULL(actual.Sales,0) as actual_sales, ISNULL(actual.quantity,0) as actual_quantity, ISNULL(actual.litres,0) as actual_litres
		,isnull(cap.Sales,0) as cap_sales, isnull(cap.quantity,0) as cap_quantity, isnull(cap.litres,0) as cap_litres
	INTO #SKULevelDetails
	FROM #ProductRef p
		LEFT JOIN (
			SELECT pr.Productcode, pr.productname, tx.GroupLabel 
				,SUM(tx.Price_CY) AS [Sales]
				,SUM(tx.Acres_CY/PR.ConversionW) AS [Quantity]			
				,SUM(IIF(tx.GroupLabel='Nodulator PRO',0,(tx.Acres_CY/PR.ConversionW) * PR.Volume)) AS [Litres]
				,SUM(tx.Acres_CY) AS [Acres]			
			FROM RP2020Web_Sales_Consolidated TX
				INNER JOIN #ProductRef PR			ON PR.Productcode=TX.Productcode
			WHERE tx.Statementcode=@Statementcode AND tx.Programcode=@ProgramCode AND tx.GroupType='REWARD_BRANDS' 
			GROUP BY pr.Productcode, pr.productname, tx.grouplabel
		) Actual
		on p.productcode=actual.productcode
		FULL OUTER JOIN (
			SELECT pr.Productcode
				,MAX(pr.ProductName) as ProductName
				,MAX(pr.ChemicalGroup) AS GroupLabel
				,SUM(
						cap.Quantity *
						(
							CASE WHEN cap.PackageSize='Jugs' THEN
								IIF(@LINECOMPANY = 1,PR.PricePerJug_SWP,PR.PricePerJug_SDP)
							WHEN cap.PackageSize='Litres' THEN
								IIF(@LINECOMPANY = 1,PR.PricePerLitre_SWP,PR.PricePerLitre_SDP)
							ELSE
								IIF(@LINECOMPANY = 1,PR.SWP,PR.SDP)
							END
						)
						) AS [Sales]
				,SUM(ISNULL(IIF(cap.PackageSize='Jugs',cap.Quantity / pr.jugs, cap.Quantity),0)) AS [Quantity]	
				,SUM(ISNULL(cap.Quantity * IIF(cap.PackageSize='Jugs',PR.LitresPerJug,IIF(cap.PackageSize='Litres',1,PR.Volume)),0)) [Litres]																	
				,SUM(ISNULL(cap.Quantity * IIF(cap.PackageSize='Jugs',AcresPerJug,IIF(cap.PackageSize='Litres',PR.AcresPerLitre,PR.ConversionW)),0)) AS [Acres]		
			FROM #ProductRef PR						
				LEFT JOIN RPWeb_CustomAppProducts CAP
				ON CAP.Productcode=PR.Productcode AND  CAP.Statementcode=@Statementcode			
			GROUP BY pr.Productcode
		) CAP
		ON CAP.Productcode=p.Productcode
	Order By RewardBrand, ProductName



	-- ######################## INSURE CEREAL AND INSURE PULSE ########################   	 
	SET @ML_THEAD_ROW='
	<thead>
	<tr>
		<th>Products</th>
		<th>' + @SEASON_STRING + ' Eligible<br/>POG QTY</th>
		<th>' + @SEASON_STRING + ' Custom<br/>App QTY</th>
		<th>Total Eligible<br/>POG Litres</th>
		<th>Total Custom<br/>App Litres</th>
		<th>Eligible Litres</th>
		<th class="mw_80p">Eligible $</th>
	</tr>
	</thead>'

	-- INSURE CEREAL / FX4
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 'sub_total_row' as [@class]
			,(select RewardBrand as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Customapp_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Rewardable_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Rewardable_Sales, '$')  as 'td' for xml path(''), type)
		FROM (
			SELECT 'INSURE Cereal' AS RewardBrand
				,SUM(ISNULL(RewardBrand_Litres,0)) AS RewardBrand_Litres
				,SUM(ISNULL(Customapp_Litres,0)) AS Customapp_Litres
				,SUM(ISNULL(Rewardable_Litres,0)) AS Rewardable_Litres
				,SUM(ISNULL(Rewardable_Sales,0)) AS Rewardable_Sales
			FROM (
					SELECT DISTINCT ChemicalGroup FROM #ProductRef	
				) p
				LEFT JOIN #Temp t
				ON T.RewardBrand=P.ChemicalGroup
			WHERE p.[ChemicalGroup] IN ('INSURE CEREAL','INSURE CEREAL FX4')
		) D
		ORDER BY RewardBrand
		FOR XML PATH('tr')	, ELEMENTS, TYPE))

	-- SKU LEVEL DETAILS
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT
				(select ProductName as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(actual_quantity, '') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(cap_quantity,'') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(actual_litres, '') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(cap_litres,'') as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)					
			,(select '' as 'td' for xml path(''), type)		
		FROM #SKULevelDetails SLD
		WHERE [RewardBrand] IN ('INSURE CEREAL', 'INSURE CEREAL FX4')
		ORDER BY ProductName
		FOR XML PATH('tr')	, ELEMENTS, TYPE))		

	-- INSURE PULSE
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 'sub_total_row' as [@class]
			,(select RewardBrand as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Customapp_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Rewardable_Litres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Rewardable_Sales, '$')  as 'td' for xml path(''), type)
		FROM (
			SELECT 'INSURE Pulse' AS RewardBrand
				,SUM(ISNULL(RewardBrand_Litres,0)) AS RewardBrand_Litres
				,SUM(ISNULL(Customapp_Litres,0)) AS Customapp_Litres
				,SUM(ISNULL(Rewardable_Litres,0)) AS Rewardable_Litres
				,SUM(ISNULL(Rewardable_Sales,0)) AS Rewardable_Sales
			FROM (
					SELECT DISTINCT ChemicalGroup FROM #ProductRef	
				) p
				LEFT JOIN #Temp t
				ON T.RewardBrand=P.ChemicalGroup
			WHERE p.[ChemicalGroup] = 'Insure Pulse'
		) D
		ORDER BY RewardBrand
		FOR XML PATH('tr')	, ELEMENTS, TYPE))	

	-- SKU LEVEL DETAILS
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT
				(select ProductName as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(actual_quantity, '') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(cap_quantity,'') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(actual_litres, '') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(cap_litres,'') as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)
			,(select '' as 'td' for xml path(''), type)			
		FROM #SKULevelDetails SLD
		WHERE [RewardBrand] = 'INSURE Pulse'
		ORDER BY ProductName
		FOR XML PATH('tr')	, ELEMENTS, TYPE))	

	SET @ML_INSURE_SECTION = '
		<table class="rp_report_table">'
			+ @ML_THEAD_ROW +  + '<tbody>' +  @ML_TBODY_ROWS  + '</tbody>
		</table>' 



	-- ######################## NODULATOR PRO ########################
	SET @ML_TBODY_ROWS=''
	
	SET @ML_THEAD_ROW='
	<thead>
	<tr>
		<th>Products</th>
		<th>' + @SEASON_STRING + ' Eligible<br/>POG QTY</th>
		<th>' + @SEASON_STRING + ' Custom<br/>App QTY</th>
		<th>Total Eligible POG $</th>
		<th>Total Custom App $</th>			
		<th class="mw_80p">Eligible $</th>
	</tr>
	</thead>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 'sub_total_row' as [@class]
		,(select '' as 'td' for xml path(''), type)
		,(select '' as 'td' for xml path(''), type)
		,(select '' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(RewardBrand_Sales,0), '$')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(CustomApp_Sales,0), '$')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(Rewardable_Sales,0), '$')  as 'td' for xml path(''), type)
		FROM (
					SELECT DISTINCT ChemicalGroup FROM #ProductRef	
		) p
		LEFT JOIN #Temp t
		ON t.RewardBrand=p.ChemicalGroup
	WHERE P.[ChemicalGroup] = 'NODULATOR PRO'
	FOR XML PATH('tr')	, ELEMENTS, TYPE))	


	-- SKU LEVEL DETAILS
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT
				(select ProductName as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(actual_quantity,0), '') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(cap_quantity,0),'') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(actual_sales,0),'$') as 'td' for xml path(''), type)							
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(cap_sales,0),'$') as 'td' for xml path(''), type)											
			,(select '' as 'td' for xml path(''), type)
		FROM #SKULevelDetails SLD
		WHERE [RewardBrand] = 'NODULATOR PRO'
		ORDER BY ProductName
		FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @ML_NODULATOR_SECTION = '
		<table class="rp_report_table">'
			+ @ML_THEAD_ROW +  + '<tbody>' +  @ML_TBODY_ROWS  + '</tbody>
		</table>'  
	

	-- TOTALS TABLE
	SET @ML_THEAD_ROW='
	<thead>
	<tr>
		<th>Products</th>
		<th>Eligible $</th>
		<th>Reward %</th>
		<th>Reward $</th>
	</tr>
	</thead>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 
		(select 'Insure Cereal' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Rewardable_Sales,0)), '$') as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MAX(ISNULL(Reward_Percentage,0)), '%')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Reward,0)), '$')  as 'td' for xml path(''), type)					
	FROM (
					SELECT DISTINCT ChemicalGroup FROM #ProductRef	
		) p
		LEFT JOIN #Temp t
		ON T.RewardBrand=P.ChemicalGroup
	WHERE P.[ChemicalGroup] IN ('INSURE CEREAL', 'INSURE CEREAL FX4')
	FOR XML PATH('tr')	, ELEMENTS, TYPE));

	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 
		(select 'Insure Pulse' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Rewardable_Sales,0)), '$') as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MAX(ISNULL(Reward_Percentage,0)), '%')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Reward,0)), '$')  as 'td' for xml path(''), type)					
	FROM (
			SELECT DISTINCT ChemicalGroup FROM #ProductRef	
		) p
		LEFT JOIN #Temp t
		ON T.RewardBrand=P.ChemicalGroup
	WHERE P.[ChemicalGroup] = 'INSURE PULSE'
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 
		(select 'Nodulator Pro' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Rewardable_Sales,0)), '$') as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MAX(ISNULL(Reward_Percentage,0)), '%')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Reward,0)), '$')  as 'td' for xml path(''), type)					
	FROM (
			SELECT DISTINCT ChemicalGroup FROM #ProductRef	
		) p
		LEFT JOIN #Temp t
		ON T.RewardBrand=P.ChemicalGroup
	WHERE P.[ChemicalGroup] = 'NODULATOR PRO'
	FOR XML PATH('tr')	, ELEMENTS, TYPE))
	
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT  'total_row' as [@class]
		,(select 'TOTAL' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Rewardable_Sales,0)), '$')  as 'td' for xml path(''), type)
		,(select '' as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(ISNULL(Reward,0)), '$')  as 'td' for xml path(''), type)					
	FROM (
					SELECT DISTINCT ChemicalGroup FROM #ProductRef	
				) p
		LEFT JOIN #Temp t
		ON T.RewardBrand=P.ChemicalGroup	
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	
	SET @ML_TOTAL_SECTION='
		<table class="rp_report_table">'
			+ @ML_THEAD_ROW +  + '<tbody>' +  @ML_TBODY_ROWS  + '</tbody>
		</table>'  



	SET @ML_SECTIONS = '<div class="st_section">
								<div class = "st_header">
									<span>' + @MARKETLETTERNAME + '</span>
									<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
									<span class="st_right"</span>
								</div>
							<div class="st_content open">' 
							+ @ML_INSURE_SECTION + @ML_NODULATOR_SECTION + @ML_TOTAL_SECTION
							+'</div>
						</div>' 


	/* ############################### FINAL OUTPUT ############################### */
FINAL:

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP

	-- DISCLAIMER TEXT
	SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">For Seed Treatments, the minimum required to qualify for this reward is 450 Litres of Insure Cereal and Insure Cereal FX4 AND/OR 360 Litres of Insure Pulse custom applied.</div>
				<div class="rprew_subtabletext">For Nodulator PRO, the minimum required to qualify for this reward is $20,000 of Nodulator PRO inoculant custom applied.</div>
			</div>
		</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @ML_SECTIONS 		
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed


	
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
END


