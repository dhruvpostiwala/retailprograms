﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_PlanSuppBusiness] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @ELIGIBILITYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	DECLARE @CY_POG_PLAN MONEY=0;
	DECLARE @SALES_LY MONEY=0;
	DECLARE @QUALIFYING_PERCENTAGE FLOAT =0;
	DECLARE @CPP_SALES MONEY=0;
	DECLARE @CPP_PERCENTAGE DECIMAL(6,4)=0;
	DECLARE @CPP_REWARD MONEY=0;
	DECLARE @LIBERTY_SALES MONEY=0;
	DECLARE @LIBERTY_REWARD_PERCENTAGE DECIMAL(6,4)=0;
	DECLARE @LIBERTY_REWARD MONEY=0;


	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--FETCH REWARD DISPLAY VALUES


	SELECT @CY_POG_PLAN =  SUM(ISNULL(CY_POG_Plan, 0))
		  ,@SALES_LY= SUM(ISNULL(LY_Sales, 0))
		  ,@CPP_SALES = SUM(IIF(RewardBrand <> 'LIBERTY 150', RewardBrand_Sales, 0))
		  ,@CPP_PERCENTAGE = MAX(IIF(RewardBrand <> 'LIBERTY 150', Reward_Percentage, 0)) 
		  ,@CPP_REWARD = SUM(IIF(RewardBrand <> 'LIBERTY 150', Reward, 0))
		  ,@LIBERTY_SALES = SUM(IIF(RewardBrand = 'LIBERTY 150', RewardBrand_Sales, 0))
		  ,@LIBERTY_REWARD_PERCENTAGE = MAX(IIF(RewardBrand = 'LIBERTY 150', Reward_Percentage, 0)) 
		  ,@LIBERTY_REWARD = SUM(IIF(RewardBrand = 'LIBERTY 150', Reward, 0))
	FROM	RP2021Web_Rewards_W_PlanningTheBusiness_Actual
	WHERE	STATEMENTCODE = @STATEMENTCODE
	GROUP	BY StatementCode, ProgramCode, MarketLetterCode


	SET	@QUALIFYING_PERCENTAGE = 
		CASE
			WHEN @CY_POG_PLAN > 0 AND @SALES_LY <= 0 THEN 9.9999
			WHEN @CY_POG_PLAN <= 0 AND @SALES_LY <= 0 THEN 0
			WHEN @CY_POG_PLAN/@SALES_LY > 9.9999 THEN 9.9999
			ELSE @CY_POG_PLAN/@SALES_LY
		END




	/* ############################### ELIGIBILITY TABLE ############################### */
	SET @HEADER = '<th>' + cast(@Season-1 as varchar(4)) + ' Eligible POG $</th> 
				   <th> '+ cast(@Season as varchar(4)) + ' POG Plan</th>
				   <th>Planned Growth %</th>'

		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_LY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@CY_POG_PLAN,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_PERCENTAGE,'%')  as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @ELIGIBILITYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @REWARDSUMMARYTABLE = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Reward Brands</th>
					<th>'+ cast(@Season as varchar(4)) + ' Eligible POG $</th>
					<th>Reward %</th>
					<th class="mw_80p">Reward $</th>
				</tr>
				<tr>
					<td class>Crop Protection</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@CPP_SALES,'$') +'</td>
					<td class="center_align">' + [dbo].[SVF_Commify](@CPP_PERCENTAGE,'%') +'</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@CPP_REWARD,'$') +'</td>
				</tr>	
				<tr>
					<td class>Liberty 150</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@LIBERTY_SALES,'$') +'</td>
					<td class="center_align">' + [dbo].[SVF_Commify](@LIBERTY_REWARD_PERCENTAGE,'%') +'</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@LIBERTY_REWARD,'$') +'</td>
				</tr>	
			</table>
		</div>
	</div>' 




	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying Brands:</strong> All 2021 Crop Protection Reward Brands including Centurion.
			</div>
			<div class="rprew_subtabletext">
				<strong>Reward Brands:</strong> All 2021 Crop Protection Reward Brands excluding Centurion.
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ELIGIBILITYTABLE 
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

