﻿



CREATE    PROCEDURE [Reports].[RP2020Web_Get_E_HorticultureRetailSupport] @STATEMENTCODE INT, @PROGRAMCODE varchar(200),  @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @REWARD_PERC DECIMAL(2,2)=0;
	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD MONEY;
	DECLARE @TYAVG AS MONEY=0;
	DECLARE @SALES_CY_E AS MONEY=0;
	DECLARE @SALES_CY_Actual AS MONEY=0;
	DECLARE @SALES_LY_Actual AS MONEY=0;
	DECLARE @IS_OU INT;

	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE = RewardCode  From RP_Config_Programs Where ProgramCode=@PROGRAMCODE
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	--FETCH REWARD DISPLAY VALUES
	SELECT @SALES_CY_E=SUM(ISNULL(RewardBrand_ElgSales_CY,0))
		  ,@SALES_CY_Actual=SUM(ISNULL(RewardBrand_ActSales_CY,0))
		  ,@SALES_LY_Actual=SUM(ISNULL(RewardBrand_ActSales_LY,0))
		  ,@REWARD_PERC =MAX(ISNULL(Reward_Percentage,0)) 
		  ,@REWARD =SUM(ISNULL(Reward,0)) 
	From [dbo].[RP2020Web_Rewards_E_HortiCultureRetailSupport]
	Where STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;

	--FOR OU QUALIFYING AMOUNT
	DROP TABLE IF EXISTS #TEMP_OU

	Select SUM(RewardBrand_ElgSales_CY) as RewardBrand_ElgSales_CY
	,SUM(RewardBrand_ActSales_CY) as RewardBrand_ActSales_CY
	,SUM(RewardBrand_ActSales_LY) as RewardBrand_ActSales_LY
	INTO 
	#TEMP_OU
	FROM [dbo].[RP2020Web_Rewards_E_HortiCultureRetailSupport] WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT  @QUAL_PERCENTAGE = IIF(RewardBrand_ActSales_LY > 0, RewardBrand_ActSales_CY / RewardBrand_ActSales_LY, IIF(RewardBrand_ActSales_CY > 0, 9.9999, 0)) FROM #TEMP_OU
	
	
	/*############################### REWARD SUMMARY TABLE ###############################*/
	DECLARE @HEADER NVARCHAR(MAX);
	
	IF @IS_OU = 0 
	BEGIN
		SET @HEADER = '<th>' + cast((@Season - 1) as varchar(4)) + ' Actual POG Sales of Reward Brands $</th><th>' + cast(@Season as varchar(4)) + ' Actual POG Sales of Reward Brands $</th><th>Qualifying % </th><th>' + cast(@Season as varchar(4)) + ' Eligible POG Sales of Reward Brands $</th><th>Reward%</th><th>Reward $</th>'
		
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_LY_Actual,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_Actual,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))
	
		SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
						<div class="rprew_subtabletext">*Retails must acquire at least $100,000 in total actual POG $ in 2020 AND at least $75,000 in actual POG of Qualifying Horticulture Fungicides to be eligible for this reward.</div>
				</div>
			</div>'
	END
	ELSE
	BEGIN
		SET @HEADER = '<th>' + cast(@Season as varchar(4)) + ' Eligible POG Sales of Reward Brands $</th><th>Qualifying OU % </th><th>Reward%</th><th>Reward $</th>'
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))

		SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
				<div class="rprew_subtabletext">*Qualifying % based on Operating Unit Totals</div>
				</div>
			</div>'
	END

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

