﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_EfficiencyBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @GROWTH_REWARDTABLE AS NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	DECLARE @SUPPORT_REWARD_PERC DECIMAL(6,4)=0;
	DECLARE	@ESCALATOR_REWARD_PERC DECIMAL(6,4)=0;
	DECLARE @GROWTH_PERCENTAGE FLOAT =0;
	DECLARE @QUALIFYING_SALES_CY MONEY=0;
	DECLARE @GROWTH_SALES_CY MONEY=0;
	DECLARE @GROWTH_SALES_LY MONEY=0;
	DECLARE @REWARD_SALES MONEY=0;
	DECLARE @REWARD_SUPPORT MONEY=0;
	DECLARE @REWARD_ESCALATOR MONEY=0;
	DECLARE @REWARD_TOTAL MONEY=0;
	DECLARE @ORGANIC_DATA VARCHAR(MAX)='';

	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	SELECT @REWARD_SALES= SUM(ISNULL(Support_Reward_Sales, 0))
		  ,@SUPPORT_REWARD_PERC = MAX(ISNULL(Support_Reward_Percentage, 0))
		  ,@REWARD_SUPPORT = SUM(ISNULL(Support_Reward, 0))
		  ,@ESCALATOR_REWARD_PERC = MAX(ISNULL(Growth_Reward_Percentage, 0)) 
		  ,@REWARD_ESCALATOR = SUM(ISNULL(Growth_Reward, 0)) 
		  ,@REWARD_TOTAL = SUM(ISNULL(Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_EFF_BONUS]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT @GROWTH_SALES_CY= SUM(ISNULL(Growth_QualSales_CY, 0))
		  ,@GROWTH_SALES_LY = SUM(ISNULL(Growth_QualSales_LY, 0))
		  ,@GROWTH_PERCENTAGE = MAX(ISNULL(Growth, 0))
		  ,@QUALIFYING_SALES_CY = SUM(ISNULL(All_QualSales_CY, 0)) 
	From [dbo].[RP2021Web_Rewards_W_EFF_BONUS_QUAL]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode
	

	IF (@GROWTH_SALES_CY > 0 AND @GROWTH_SALES_LY <= 0) OR @GROWTH_PERCENTAGE > 9.9999
	BEGIN
		SET @GROWTH_PERCENTAGE = 9.9999
	END


	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @HEADER = '<th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th> 
					<th> Reward %</th>
					<th> ' + cast(@Season as varchar(4)) + ' Eligible InVigor POG $</th>
					<th> Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_SALES_CY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SUPPORT_REWARD_PERC,'%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SUPPORT,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 


	/* ############################### GROWTH REWARD TABLE ############################### */
	/* Note: We can reuse @HEADER AND @SUMMARY since the data it holds is not needed anymore. */
	SET @HEADER = '<th> ' + cast(@Season-1 as varchar(4)) + ' Eligible POG $</th> 
					<th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th> 
					<th>Growth %</th>
					<th>Escalator Bonus %</th>
					<th>Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@GROWTH_SALES_LY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@GROWTH_SALES_CY,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@GROWTH_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@ESCALATOR_REWARD_PERC,'%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_ESCALATOR,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	IF	@SUPPORT_REWARD_PERC < 0.025
	BEGIN
		SET @GROWTH_REWARDTABLE += '<div class="st_section">
			<div class="st_content open">
				<table class="rp_report_table"> 
					<thead>
					<tr>
							'+ @HEADER +'						
					</tr>
					</thead>
							'+ @SUMMARY + '
				</table>
			</div>
		</div>' 
	END

	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	SET @ORGANIC_DATA = '{
							"sales":['+cast(@REWARD_SALES as varchar(20))+'],
							"reward_margins":['+cast(@SUPPORT_REWARD_PERC as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible InVigor POG $"],
							"row_labels":[]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='Default', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT

	/* ############################### REWARD PERCENTAGE TABLE ############################### */
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Total Portfolio Support</th>
					<th>Reward %</th>
				</tr>
				<tr>
					<td>Achieve $1M - $1.49M in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 1.00% </td>
				</tr>
				<tr>
					<td>Achieve $1.5M - $1.9M in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 1.75% </td>
				</tr>
				
				<tr>
					<td>Achieve $2M + in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 2.50% </td>
				</tr>						
			</table>
		</div>
	</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying Brands:</strong> Eligible 2021 Crop Protection and InVigor Reward Brands.
			</div>
			<div class="rprew_subtabletext">
                <strong>Reward Brands:</strong> Eligible 2021 InVigor Reward Brands.
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @GROWTH_REWARDTABLE
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

