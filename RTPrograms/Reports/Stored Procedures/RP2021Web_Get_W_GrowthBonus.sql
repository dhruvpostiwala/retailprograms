﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_GrowthBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @L5CODE AS VARCHAR(10)=''

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);


	DECLARE @QUAL_SALES_LY MONEY=0;
	DECLARE @QUAL_SALES_CY MONEY=0;
	DECLARE @GROWTH_PERCENTAGE FLOAT=0;
	DECLARE @REWARD_PERCENTAGE DECIMAL(6,4)=0;
	DECLARE @REWARD MONEY=0;
	DECLARE @ORGANIC_DATA VARCHAR(MAX);
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	SELECT @L5CODE=Level5Code FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE


	--FETCH REWARD DISPLAY VALUES
	SELECT @QUAL_SALES_LY= SUM(ISNULL(QualSales_LY_INDEX, 0))  -- GET THE INDEX VALUE
		  ,@QUAL_SALES_CY= SUM(ISNULL(QualSales_CY, 0))
		  ,@GROWTH_PERCENTAGE = MAX(ISNULL(Growth_Percentage, 0))
		  ,@REWARD_PERCENTAGE = MAX(ISNULL(Reward_Percentage, 0))
		  ,@REWARD = SUM(ISNULL(Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_GrowthBonus]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SET @GROWTH_PERCENTAGE = IIF(@GROWTH_PERCENTAGE > 9.9999, 9.9999, @GROWTH_PERCENTAGE)

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT



	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @HEADER = '<th> ' + cast(@Season-1 as varchar(4)) + ' Eligible POG $</th> 
				   <th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th> 
				   <th>Growth %</th>
					<th>Reward %</th>
					<th>Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_SALES_LY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_SALES_CY,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@GROWTH_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	SET @ORGANIC_DATA = '{
							"sales":['+cast(@QUAL_SALES_CY as varchar(20))+'],
							"reward_margins":['+cast(@REWARD_PERCENTAGE as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible POG $"],
							"row_labels":[]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='Default', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT

	/* ############################### REWARD PERCENTAGE TABLE ############################### */
	IF @L5CODE = 'D0000117' -- WEST COOP REWARD MATRIX
	BEGIN
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>2020 to 2021 Eligible POG Growth</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align">4.5%</td>
					</tr>
					<tr>
						<td class="center_align">100% - 104.9%</td>
						<td class="center_align">3.75%</td>
					</tr>				
					<tr>
						<td class="center_align">90% to 99.9%</td>
						<td class="center_align">2%</td>
					</tr>
					<tr>
						<td class="center_align">< 90%</td>
						<td class="center_align">1%</td>
					</tr>	
				</table>
			</div>
		</div>' 

	END

	ELSE -- DEFAULT ACTION REWARD MATRIX (AKA WEST INDEPENDENT)
	BEGIN
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>2020 to 2021 Eligible POG Growth</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align">6.25%</td>
					</tr>
					<tr>
						<td class="center_align">100% - 104.9%</td>
						<td class="center_align">5.75%</td>
					</tr>				
					<tr>
						<td class="center_align">90% to 99.9%</td>
						<td class="center_align">5%</td>
					</tr>
					<tr>
						<td class="center_align">< 90%</td>
						<td class="center_align">3%</td>
					</tr>	
				</table>
			</div>
		</div>' 

	END	

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Caramba, Certitude, Cimegra, Cotegra, Dyax, Engenia, Facet L, Forum, Frontier Max, Headline, Insure Cereal, Insure Cereal FX4, Insure Pulse, Lance, Lance AG, Nexicor, all Nodulator brands, Priaxor, Sefina, Sercadis, Teraxxa, Titan, Twinline, Liberty 200, and Zidua.
			</div>
			<div class="rprew_subtabletext">
				<strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agriculture Regions(s) for your location(s).
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'	
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

