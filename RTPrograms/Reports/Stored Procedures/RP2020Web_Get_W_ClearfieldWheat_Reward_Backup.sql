﻿



CREATE   PROCEDURE [Reports].[RP2020Web_Get_W_ClearfieldWheat_Reward_Backup] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(200),  @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @STATEMENT_LEVEL AS VARCHAR(20)='';
	DECLARE @REGION AS VARCHAR(4)='';

	Declare @RewardSummarySection nvarchar(max)='';
	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';

	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @RETAILER_NAME NVARCHAR(MAX);
	DECLARE @RETAILER_CITY NVARCHAR(MAX);
	
	
	DECLARE @RET_MATCHING_HERB_SECTIONS NVARCHAR(MAX)=''
	DECLARE @RET_COMMITMENT_SECTIONS NVARCHAR(MAX)=''
	
	DECLARE @THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @TBODY_ROWS NVARCHAR(MAX)=''
	DECLARE @TOTAL_ROW NVARCHAR(MAX)=''
	
	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';

	DECLARE @RET_SubTotal MONEY=0;
			
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @STATEMENT_LEVEL=StatementLevel FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))


	SET @MARKETLETTERCODE='ML' + @SEASON_STRING + '_W_CER_PUL_SB'
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE
	
	-- GET MATCHING HERBICIDES DATA
	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 	
	SELECT RP.RetailerCode, RP.RetailerName as Retailer_Name, RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
		,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower 
		,T1.Farmcode
		,WheatAcres,HerbAcres,MatchedAcres,MatchedSales
		,Reward_Percentage,Reward		
	INTO #TEMP
	FROM (			
			SELECT Retailercode, Farmcode 
				,MAX(IIF(RewardBrand='CLEARFIELD WHEAT',Acres,0)) AS WheatAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD WHEAT',Acres,0)) AS HerbAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD WHEAT',MatchedAcres,0)) AS MatchedAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD WHEAT',MatchedSales,0)) AS MatchedSales
				,MAX(Reward_Percentage) AS Reward_Percentage
				,SUM(Reward) AS Reward
			FROM RP2020Web_Rewards_W_CLW_Herb
			WHERE Statementcode=@Statementcode 	
			GROUP BY Retailercode, Farmcode 					
		) T1				
		LEFT JOIN RetailerProfile RP	
		ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC		
		ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'

	-- MATCHING HERBICIDES SECTION
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @RET_MATCHING_HERB_SECTIONS = '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + ' - REWARD SUMMARY</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>					
					</div>
				</div>' 
		GOTO COMMITMENT_REWARDS_SECTION
	END 
	
	SET @THEAD_ROW='<tr>
		<thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>Wheat Acres</th>
			<th>Herbicide Acres</th>
			<th>Matched Acres</th>
			<th>Matched ' + @SEASON_STRING + '<br/>Herbicide POG $</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>
		</thead>
		</tr>'
			
/*	
	DECLARE RETAILER_LOOP CURSOR FOR 
		SELECT DISTINCT  Retailercode, Retailer_Name, Retailer_City FROM #TEMP ORDER BY [Retailer_Name]
	OPEN RETAILER_LOOP
	FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
	WHILE(@@FETCH_STATUS=0)
		BEGIN
*/
			--SET @TBODY_ROWS += '<div class="rprew_subsectionheader">' + @RETAILER_NAME + ' (' + @RETAILER_CITY + ')</div>'
			IF @STATEMENT_LEVEL='LEVEL 2'
			BEGIN
				SELECT @RET_SubTotal=SUM(Reward) FROM #TEMP WHERE RetailerCode=@RETAILERCODE
				SET @TBODY_ROWS += '<tr class="sub_total_row"><td colspan=7>' + @RETAILER_NAME + ' - ' + @RETAILER_CITY + '</td><td class="right_align">' + dbo.SVF_Commify(@RET_SubTotal,'$') + '</td></tr>'
			END 

			SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select Grower AS 'td' for xml path(''), type)
					,(select FarmCode AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(WheatAcres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(HerbAcres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedAcres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedSales, '$')  as 'td' for xml path(''), type)							
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM #TEMP
				--WHERE RetailerCode=@RETAILERCODE
				ORDER BY Grower
				FOR XML PATH('tr')	, ELEMENTS, TYPE))
/*
		FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
		END
	CLOSE RETAILER_LOOP
	DEALLOCATE RETAILER_LOOP		
*/	   
	-- TOTAL ROW
		SET @TOTAL_ROW = CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 'Total' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedSales, '$')  as 'td' for xml path(''), type)							
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM (
					SELECT SUM(MatchedSales) AS MatchedSales, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward
					FROM #TEMP			
				)  D				
				FOR XML PATH('tr')	, ELEMENTS, TYPE))



		SET @RET_MATCHING_HERB_SECTIONS='<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<table class="rp_report_table">' + @THEAD_ROW + @TBODY_ROWS  + @TOTAL_ROW + '</table>				
					</div>
				</div>' 

COMMITMENT_REWARDS_SECTION:

	SET @RET_COMMITMENT_SECTIONS=''
	DECLARE @REWARD_TYPE VARCHAR(50)='Early Commitment'
	DECLARE @SECTION_HEADER VARCHAR(200)= 'COMMITTED ACRES ($200/COMMITMENT)'
/*
	SET @THEAD_ROW='<tr>
		<thead>
			<th style="width: 40%">Grower</th>
			<th>Farm ID</th>
			<th>Commitment Date</th>			
			<th>Received by<br/>Jul 09, 2020</th>
			<th class="mw_80p">Reward $</th>
		</thead>
		</tr>'
*/
	SET @THEAD_ROW='<tr>
		<thead>
			<th style="width: 40%">Grower</th>
			<th>Farm ID</th>
			<th>Commitment Date</th>			
			<th class="mw_80p">Reward $</th>
		</thead>
		</tr>'

COMMITMENT_REWARD_TYPE_LOOP:

	-- GET COMMITMENTS REWARDS
	IF NOT OBJECT_ID('tempdb..#COMM_REWARDS') IS NULL DROP TABLE #COMM_REWARDS	
	SELECT RP.RetailerCode, RP.RetailerName as Retailer_Name, RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
		,T1.Farmcode 
		,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower 
		, GC.City + IIF(GC.Province='','',', ' + GC.Province) as Grower_City
		,GFI.PIPEDAFarmStatus
		,convert(varchar, CommitmentDate, 107) AS CommitmentDate		
		,Req_ReceivedByDate
		,Reward		
	INTO #COMM_REWARDS
	FROM (	
			SELECT Retailercode, Farmcode
				,MIN(CommitmentDate) AS CommitmentDate
				,MIN(IIF(Req_ReceivedByDate=1,'YES','NO')) AS Req_ReceivedByDate
				,SUM(Reward) AS Reward
			FROM RP2020Web_Rewards_W_CLW_Comm
			WHERE Statementcode=@Statementcode AND RewardType=@REWARD_TYPE
			GROUP BY Retailercode, Farmcode			
		) T1
		LEFT JOIN RetailerProfile RP			ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode=T1.Farmcode
	

	IF NOT EXISTS(SELECT * FROM #COMM_REWARDS)		
		SET @RET_COMMITMENT_SECTIONS += '
		<div class="st_section">
			<div class = "st_header">
				<span>' + @SECTION_HEADER + '</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">					
				<div class="rp_tab_message">NO COMMITMENTS FOUND</td></div>					
			</div>
		</div> ' 						
	ELSE
		BEGIN
		SET @TBODY_ROWS =''
/*
		DECLARE RETAILER_LOOP CURSOR FOR 
			SELECT  DISTINCT Retailercode, Retailer_Name, Retailer_City FROM #COMM_REWARDS ORDER BY [Retailer_Name]
		OPEN RETAILER_LOOP
		FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
		WHILE(@@FETCH_STATUS=0)
		BEGIN		
*/
			IF @STATEMENT_LEVEL='LEVEL 2'
			BEGIN
				SELECT @RET_SubTotal=SUM(Reward) FROM #COMM_REWARDS WHERE RetailerCode=@RETAILERCODE
				SET @TBODY_ROWS += '<tr class="sub_total_row"><td colspan=3>' + @RETAILER_NAME + ' - ' + @RETAILER_CITY + '</td><td class="right_align">' + dbo.SVF_Commify(@RET_SubTotal,'$') + '</td></tr>'
			END 
			
			SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower + ' (' + Farmcode + ')') AS 'td' for xml path(''), type)
					,(select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',FarmCode) AS 'td' for xml path(''), type)
					,(select 'center_align' as [td/@class] ,CommitmentDate  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM #COMM_REWARDS
				--WHERE RetailerCode=@RETAILERCODE 
				ORDER BY PIPEDAFarmStatus DESC, Grower
				FOR XML PATH('tr')	, ELEMENTS, TYPE))
/*
		FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
		END
		CLOSE RETAILER_LOOP
		DEALLOCATE RETAILER_LOOP
*/
		-- TOTAL ROW
		SET @TOTAL_ROW = CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 'Total' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM (
					SELECT SUM(Reward) AS Reward
					FROM #COMM_REWARDS			
				)  D				
				FOR XML PATH('tr')	, ELEMENTS, TYPE))

		
			SET @RET_COMMITMENT_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTION_HEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<table class="rp_report_table">' + @THEAD_ROW + @TBODY_ROWS  + @TOTAL_ROW + '</table>				
						<div class="rprew_subtabletext">Registered Clearfield Wheat acres must be received by BASF on or before July 9, 2020 for a retailer to be Eligible for the reward.</div>
					</div>
				</div>' 

	END

	IF @REWARD_TYPE='Early Commitment'
	BEGIN
		SET @REWARD_TYPE='Late Commitment'
		SET @SECTION_HEADER='COMMITTED ACRES ($100/COMMITMENT)'
		GOTO COMMITMENT_REWARD_TYPE_LOOP
	END
	
	   	 		
	/* ############################### FINAL OUTPUT ############################### */

FINAL:
	SET @DisclaimerText='
<div class="st_static_section">
	<div class="st_content open">
		<div class="rprew_subtabletext"><b>*CEREAL, PULSE & SOYBEAN MARKET LETTER  REWARD BRANDS = </b>Altitude FX brands 
		</div>
		<div class="rprew_subtabletext">*$200/commitment for newly signed & submitted evergreen Clearfield Commitments on or before February 29, ' + @SEASON_STRING + '</div>
		<div class="rprew_subtabletext">**$100/commitment for newly signed & submitted evergreen Clearfield Commitments between March 1st, ' + @SEASON_STRING + ' & October 8th, ' + @SEASON_STRING + '</div>
		<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Grower Level & summed up.</span></div>
	</div>
</div>'


	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: block;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @RET_MATCHING_HERB_SECTIONS 		
	SET @HTML_Data += @RET_COMMITMENT_SECTIONS 		
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	--SELECT @HTML_DATA AS [data]

	
END

