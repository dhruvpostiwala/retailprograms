﻿


CREATE   PROCEDURE [Reports].[RP2020Web_Get_W_CustomAerialApp_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	Declare @Season int=2020;
	Declare @ProgramTitle as varchar(200);
	DECLARE @RetailerCode VARCHAR(50);
	--Declare @HTML_Data as nvarchar(max);
	Declare @RewardSummaryTable as  nvarchar(max)='';
	Declare @RequirementsTable_cent as  nvarchar(max)='';
	Declare @RequirementsTable_facet as  nvarchar(max)='';
	Declare @RewardsPercentageMatrix as nvarchar(max)='';
	Declare @DisclaimerText as nvarchar(max) = '';
	Declare @SectionHeader Varchar(200);
	
	DECLARE @RewardCode as varchar(200);
	DECLARE @RewardSummarySection as nvarchar(max) = '';
	DECLARE @Category AS VARCHAR(50)='';

	SELECT @ProgramTitle=Title, @RewardCode = RewardCode  
	FROM RP_Config_Programs 
	WHERE ProgramCode=@ProgramCode

	SELECT @RetailerCode=RetailerCode , @Category=Category
	FROM RPWeb_Statements 
	WHERE StatementCode=@STATEMENTCODE

	DECLARE @TotalProductSales as decimal(15,2)=0;
	DECLARE @TotalCADollars as decimal(15,2)=0;
	DECLARE @TotalEligible as decimal(15,2)=0;
	DECLARE @TotalReward as decimal(15,2)=0;
	
	SELECT 
		@TotalProductSales = SUM(Product_Sales)
		,@TotalCADollars = SUM(Product_CustApp_Dollars)
		,@TotalEligible = SUM(Elig_Dollars)
		,@TotalReward = SUM(Reward)
	FROM RP2020Web_Rewards_W_CustomAerial
	WHERE STATEMENTCODE = @STATEMENTCODE
	
	
	/* ******************REWARD SUMMARY**************************** */
	--FETCH REWARD SUMMARY
	IF @CATEGORY='REGULAR'
		EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT

	
	DECLARE @Eligible_Products TABLE (
		 productcode VARCHAR (50) NOT NULL
	); 

	/*
		**New September 15 2020 RP-2758
	Added Products
	Priaxor
	Sercadis
	Twinline
	*/

	DROP TABLE IF EXISTS #ProductRef
	SELECT DISTINCT PRODUCTCODE,ProductName 
	INTO #ProductRef
	FROM ProductReference 
	WHERE ChemicalGroup IN ('CABRIO PLUS','CARAMBA','COTEGRA','DYAX','FORUM','HEADLINE','LANCE','LANCE AG','NEXICOR','PRIAXOR','SERCADIS','TWINLINE') 
	AND Status = 'Active'

	--** NEW DEMAREY SHOW ALLPRODUCT ROWS REWARDLESS OF ACTUAL SALES
	IF @CATEGORY='REGULAR'
	BEGIN
		SELECT @RewardSummaryTable=CONVERT(NVARCHAR(MAX), (			
				SELECT
					'rp_report_table' as [@class] -- Table Class
					,(SELECT 'Product' as th, CAST(@Season AS Varchar(4)) + ' Eligible POG QTY' AS th,CAST(@Season AS Varchar(4)) + ' Eligible POG $' AS th, CAST(@Season AS Varchar(4)) + ' Custom App QTY' AS th, CAST(@Season AS Varchar(4)) + ' Custom App $' AS th, 'Eligible $' AS th, 'Reward %' as th, 
					'Reward $' as th FOR XML RAW('tr'), ELEMENTS, TYPE) AS 'thead'
					, (  
						SELECT
						 (SELECT CA.ProductName AS 'td' FOR XML PATH(''),TYPE)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_Quantity, '') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_Sales, '$') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_CustApp_Quantity,'') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_CustApp_Dollars,'$') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Elig_Dollars, '$') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Reward_Percentage, '%') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Reward, '$') as 'td' for xml path(''), type)				
						FROM (
							SELECT PR.ProductName, ISNULL(Product_Quantity,0) Product_Quantity, ISNULL(Product_Sales,0) Product_Sales, ISNULL(Product_CustApp_Quantity,0) Product_CustApp_Quantity
							,ISNULL(Product_CustApp_Dollars,0) Product_CustApp_Dollars, ISNULL(Elig_Dollars,0) Elig_Dollars, ISNULL(Reward_Percentage,0) Reward_Percentage, ISNULL(Reward,0) Reward
							FROM #ProductRef PR
								LEFT JOIN RP2020Web_Rewards_W_CustomAerial tx 
								ON tx.ProductCode = PR.ProductCode AND tx.StatementCode = @STATEMENTCODE
						) CA
						ORDER BY ProductName
						FOR XML PATH('tr')	, ELEMENTS, TYPE
					) AS 'tbody'
					,(							
						SELECT
							'total_row' as [@Class] -- Footer TR Class							
							,(select 'Total' as 'td' for xml path(''), type)
							,(select '' as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalProductSales, '$') as 'td' for xml path(''), type)								
							,(select '' as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalCADollars, '$') as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalEligible, '$') as 'td' for xml path(''), type)
							,(select '' as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalReward, '$') as 'td' for xml path(''), type)
						FOR XML PATH('tr')	, ELEMENTS, TYPE
					) AS 'tfoot'
				FOR XML PATH('table')  
			)
		) 
	END

	IF @CATEGORY='CUSTOM AERIAL'
	BEGIN
		SELECT @RewardSummaryTable=CONVERT(NVARCHAR(MAX), (			
				SELECT
					'rp_report_table' as [@class] -- Table Class
					,(
						SELECT 'Product' as th
					--	,CAST(@Season AS Varchar(4)) + ' Eligible POG QTY' AS th
					--	,CAST(@Season AS Varchar(4)) + ' Eligible POG $' AS th
						,CAST(@Season AS Varchar(4)) + ' Custom App QTY' AS th
						,CAST(@Season AS Varchar(4)) + ' Custom App $' AS th
					--	,'Eligible $' AS th
						,'Reward %' as th
						,'Reward $' as th FOR XML RAW('tr'), ELEMENTS, TYPE
					) AS 'thead'
					,(  
						SELECT
						 (SELECT CA.ProductName AS 'td' FOR XML PATH(''),TYPE)
					--	,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_Quantity, '') as 'td' for xml path(''), type)
					--	,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_Sales, '$') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_CustApp_Quantity,'') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Product_CustApp_Dollars,'$') as 'td' for xml path(''), type)
					--	,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Elig_Dollars, '$') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Reward_Percentage, '%') as 'td' for xml path(''), type)
						,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CA.Reward, '$') as 'td' for xml path(''), type)				
						FROM (
							SELECT PR.ProductName, ISNULL(Product_Quantity,0) Product_Quantity, ISNULL(Product_Sales,0) Product_Sales, ISNULL(Product_CustApp_Quantity,0) Product_CustApp_Quantity
							,ISNULL(Product_CustApp_Dollars,0) Product_CustApp_Dollars, ISNULL(Elig_Dollars,0) Elig_Dollars, ISNULL(Reward_Percentage,0) Reward_Percentage, ISNULL(Reward,0) Reward
							FROM #ProductRef PR
								LEFT JOIN RP2020Web_Rewards_W_CustomAerial tx 
								ON tx.ProductCode = PR.ProductCode AND tx.StatementCode = @STATEMENTCODE
						) CA
						ORDER BY ProductName
						FOR XML PATH('tr')	, ELEMENTS, TYPE
					) AS 'tbody'
					,(							
						SELECT
							'total_row' as [@Class] -- Footer TR Class							
							,(select 'Total' as 'td' for xml path(''), type)
						--	,(select '' as 'td' for xml path(''), type)
						--	,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalProductSales, '$') as 'td' for xml path(''), type)								
							,(select '' as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalCADollars, '$') as 'td' for xml path(''), type)
						--	,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalEligible, '$') as 'td' for xml path(''), type)
							,(select '' as 'td' for xml path(''), type)
							,(select 'right_align' as [td/@class], dbo.SVF_Commify(@TotalReward, '$') as 'td' for xml path(''), type)
						FOR XML PATH('tr')	, ELEMENTS, TYPE
					) AS 'tfoot'
				FOR XML PATH('table')  
			)
		)
	END
	   
	SET @SectionHeader='CUSTOM AERIAL APPLICATION REWARD'
	SET @RewardSummaryTable = '
	<div class="st_section">
		<div class="st_header">
			<span>' + @SectionHeader + '</span>
			<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
		</div>
		<div class="st_content open">
			' + @RewardSummaryTable + '
			</div>
	</div>'
	
	SET @DisclaimerText='<div class="rprew_subtabletext"><b>*Qualifying & Reward Brands</b> = Cabrio Plus, Caramba, Cotegra, Dyax, Forum, Headline, Lance, Lance AG, Nexicor, Priaxor, Sercadis, Twinline.</div>'
	/*
	IF @RetailerCode = 'D0000107' -- If Cargill
		SET @DisclaimerText='<div class="rprew_subtabletext"><b>*Qualifying & Reward Brands</b> = Cabrio Plus, Caramba, Cotegra, Dyax, Forum, Headline, Lance, Lance AG, Nexicor.</div>'
	ELSE IF @RetailerCode <> 'D0000137' -- If Richardson
		SET @DisclaimerText='<div class="rprew_subtabletext">*Reward is calculated at a Product Level and summed up at a Retail Family.</div>
							 <div class="rprew_subtabletext"><b>*Qualifying & Reward Brands</b> = Cabrio Plus, Caramba, Cotegra, Dyax, Forum, Headline, Lance, Lance AG, Nexicor.</div>'
    */
/* ********************* FINAL OUTPUT ************************************************ */
SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @RewardSummaryTable
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END

