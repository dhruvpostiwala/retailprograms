﻿
CREATE PROCEDURE [Reports].[RP2020Web_Get_W_TitanCompProg_Reward] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	Declare @SeasonString varchar(4) ='2020';
	Declare @ProgramTitle as varchar(200);
	Declare @RewardCode as varchar(200);

	Declare @RewardSummarySection nvarchar(max)='';
	Declare @RewardSummaryTable as  nvarchar(max)='';
	
	Declare @DisclaimerText as nvarchar(500)='';  -- not used if needed in the future can easily be used

	Declare @titan_table NVARCHAR(MAX);

	DECLARE @Reward_Perc INT = 158;
	Declare @Quantity as decimal(15,2)=0;
	DECLARE @Reward money;

	SELECT @ProgramTitle=Title, @RewardCode = RewardCode  From RP_Config_Programs Where ProgramCode=@ProgramCode

	--FETCH REWARD DISPLAY VALUES
	SELECT @Reward=ISNULL(Reward,0)
		  ,@Quantity=ISNULL(Quantity,0)		
	From RP2020Web_Rewards_W_TitanCompProg 
	Where STATEMENTCODE = @StatementCode

	/* ############################### MARKET LETTER SUMMARY TABLE ############################### */

	--FETCH MARKETLETTERHEADER
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	SELECT @titan_table = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , dbo.SVF_Commify(@Quantity,'')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , dbo.SVF_Commify(@Reward_Perc,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,  dbo.SVF_Commify(@Reward,'') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	SET @RewardSummaryTable += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead><tr><th>' + @SeasonString + ' Eligible Titan Cases</th><th>Reward</th><th>Reward $</th></tr></thead>
						'+ @titan_table + '
			</table>
		</div>
	</div>' 
		
	/* ############################### FINAL OUTPUT ############################### */
	SET @DisclaimerText='*Reward calculated at a Retail Location Level'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @RewardSummaryTable 		
	SET @HTML_Data += @DisclaimerText;
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
END


