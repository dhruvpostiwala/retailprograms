﻿

CREATE  PROCEDURE [Reports].[RP2020Web_Get_W_AgRewBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);

	DECLARE @RETAILERCODE VARCHAR(50);

	DECLARE @SEASON AS INT;
	DECLARE @REGION AS VARCHAR(4)='';
	DECLARE @STATEMENT_LEVEL AS VARCHAR(20)='';
	DECLARE @DisclaimerText as nvarchar(max);
	DECLARE @RewardSummarySection as NVARCHAR(max)='';

	DECLARE @SectionHeader as varchar(200);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @SECTION_TYPES TABLE ( 
		LeadClosed VARCHAR(3)
	)
	
	DECLARE @SECTIONS AS NVARCHAR(MAX)='';
	DECLARE @THEAD AS NVARCHAR(MAX)=''
	DECLARE @TOTAL_ROW AS NVARCHAR(MAX)='';
	DECLARE @TBODY_ROWS AS NVARCHAR(MAX)='';

	DECLARE @RewardsPercentageMatrix as nvarchar(max)='';
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @STATEMENT_LEVEL=StatementLevel, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT

	DROP TABLE IF EXISTS #TEMP 	
	SELECT T1.RetailerCode
	, RP.RetailerName as Retailer_Name
	, RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
	,T1.Farmcode 
	,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower_Name
	,ISNULL(GC.City,'') + ' , ' + ISNULL(GC.Province,'') AS Grower_City
	,LeadClosed	,SegmentsCount, Invigor_Bonus			
		,Reward_Percentage	,RewardBrand_Sales	,Reward		
	INTO #TEMP
	FROM (
			SELECT Retailercode
				,IIF(HasPIPEDA='Yes',Farmcode,'Non-PIPEDA') as Farmcode,  LeadClosed	,SegmentsCount
				,ISNULL(InvigorBonus, 'No') AS Invigor_Bonus
				,Reward_Percentage, RewardBrand_Sales,Reward
			FROM (
				SELECT Retailercode, Farmcode, HasPIPEDA, LeadClosed, InVigorBonus, SegmentsCount, Reward_Percentage
					,SUM(RewardBrand_Sales) AS RewardBrand_Sales
					,SUM(Reward) AS Reward
				FROM RP2020Web_Rewards_W_AgRewBonus_Actual	
				WHERE Statementcode=@Statementcode			
				GROUP BY Retailercode, Farmcode, HasPIPEDA, LeadClosed, InVigorBonus, SegmentsCount, Reward_Percentage
			) T0
		) T1
		LEFT JOIN RetailerProfile RP	ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC		ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'
	ORDER BY Retailer_Name, Retailer_City, IIF(T1.Farmcode='Non-PIPEDA',2,1) ,Grower_Name

	IF @RETAILERCODE IN ('D0000107', 'D0000137')
	SET @THEAD='
		<thead>
		<tr>
			<th>Retailer Code</th>' +
			IIF(@RETAILERCODE IN ('D0000107', 'D0000137'), '', '<th>Retailer City</th>') + -- If Cargill or Richardson Statement
			'<th>Grower</th>
			<th>Farm ID</th>				
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG$</th>
			<th>InVigor Bonus</th>
			<th>Number of Segments</th>
			<th>Reward %</th>
			<th>Reward $</th>
		</tr>
		</thead>'
	ELSE
	SET @THEAD='
		<thead>
		<tr>
			<th>Grower</th>
			<th>Farm ID</th>				
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG$</th>
			<th>InVigor Bonus</th>
			<th>Number of Segments</th>
			<th>Reward %</th>
			<th>Reward $</th>
		</tr>
		</thead>'
			
	INSERT INTO @SECTION_TYPES(LeadClosed)
	VALUES('Yes'),('No')

	DECLARE @LEAD_CLOSED VARCHAR(3)
	DECLARE @FARMTYPE VARCHAR(10)='PIPEDA'
	DECLARE SECTIONS_LOOP CURSOR FOR 
		SELECT [LeadClosed] FROM @SECTION_TYPES  ORDER BY [LeadClosed] DESC
	OPEN SECTIONS_LOOP
	FETCH NEXT FROM SECTIONS_LOOP INTO @LEAD_CLOSED
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			IF @LEAD_CLOSED='YES'
				SET @SECTIONHEADER =  'LEAD CLOSED'
			ELSE
				SET @SECTIONHEADER =  'NO LEAD CLOSED'
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE LeadClosed=@Lead_Closed)
			BEGIN
				SET @SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"></span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
					</div>
				</div>' 
	
				GOTO FETCH_NEXT
			END 

			IF @RETAILERCODE IN ('D0000107', 'D0000137') -- If Cargill or Richardson Statement
				BEGIN
					SET @TBODY_ROWS = ''
					SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
						(select RetailerCode as 'td' for xml path(''), type)
						,(select IIF(Farmcode='Non-PIPEDA','Non-PIPEDA',ISNULL(Grower_Name,'')) as 'td' for xml path(''), type)
						,(select IIF(Farmcode='Non-PIPEDA','',FarmCode) as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
						,(select 'center_align' as [td/@class] ,Invigor_Bonus  as 'td' for xml path(''), type)
						,(select 'center_align' as [td/@class] ,SegmentsCount  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)							
						,(select 'w80p right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP						
					WHERE LeadClosed=@Lead_Closed 
					ORDER BY IIF(Farmcode='Non-PIPEDA',2,1), Grower_Name
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

				SET @TOTAL_ROW = CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 'Total' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM (
						SELECT SUM(RewardBrand_Sales) AS RewardBrand_Sales ,SUM(Reward) AS Reward
						FROM #TEMP
						WHERE LeadClosed=@Lead_Closed
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))
				END
			ELSE
				BEGIN
					SET @TBODY_ROWS = ''
					SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
						(select IIF(Farmcode='Non-PIPEDA','Non-PIPEDA',ISNULL(Grower_Name,'')) as 'td' for xml path(''), type)
						,(select IIF(Farmcode='Non-PIPEDA','',FarmCode) as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
						,(select 'center_align' as [td/@class] ,Invigor_Bonus  as 'td' for xml path(''), type)
						,(select 'center_align' as [td/@class] ,SegmentsCount  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)							
						,(select 'w80p right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP						
					WHERE LeadClosed=@Lead_Closed 
					ORDER BY  IIF(Farmcode='Non-PIPEDA',2,1) ,Grower_Name
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

				SET @TOTAL_ROW = CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 'Total' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select '' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM (
						SELECT SUM(RewardBrand_Sales) AS RewardBrand_Sales ,SUM(Reward) AS Reward
						FROM #TEMP
						WHERE LeadClosed=@Lead_Closed
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))
				END

			SET @SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @THEAD +  @TBODY_ROWS  + @TOTAL_ROW + '
					</table>
				</div>
			</div>' 
FETCH_NEXT:
			FETCH NEXT FROM SECTIONS_LOOP INTO @LEAD_CLOSED
			END
	CLOSE SECTIONS_LOOP
	DEALLOCATE SECTIONS_LOOP
	   	 

 /* ****************************** MATRIX TABLE ******************************** */

 FINAL:
	SET @SectionHeader = 'REWARD PERCENTAGES'
	SET @RewardsPercentageMatrix = '
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + @sectionHeader + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"</span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				<tr><th>Requirement</th><th colspan=2>Reward</th></tr>
				<tr><td>“Close the Lead” on a Grower’s Crop Plan containing at least one BASF brand?</td><td class="center_align">Yes</td><td class="center_align">No</td></tr>
				<tr><td>Qualifies for an InVigor Bonus</td><td class="center_align">4%</td><td class="center_align">2%</td></tr>
				<tr><td>Qualifies for a Two Segment Baseline Rebate</td><td class="center_align">4.5%</td><td class="center_align">2.5%</td></tr>
				<tr><td>Qualifies for a Three Segment Baseline Rebate</td><td class="center_align">6%</td><td class="center_align">4%</td></tr>
			</table>
		</div>
	</div>' 

/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

	IF @RETAILERCODE = 'D520062427' -- If NUTRIEN
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> =  Acrobat, Altitude FX, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Duet, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Mizuna, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV & Zidua SC.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator Brands.</div>
			</div>
		</div>'
	ELSE
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV & Zidua SC.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator Brands.</div>
			</div>
		</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @SECTIONS
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END



