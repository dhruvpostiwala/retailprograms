﻿







CREATE  PROCEDURE [Reports].[RP2021Web_Get_E_POGSalesBonus_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @SEASON_LY INT = @SEASON - 1 ;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @REWARD_PERCENTAGE FLOAT=0;
	DECLARE @REWARD MONEY;
	DECLARE @SALES_CY AS MONEY=0;
	DECLARE @IS_OU INT;

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	

	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode  
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE
	
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	SELECT @SALES_CY=ISNULL(RewardBrand_Sales,0)
		  ,@REWARD =ISNULL(Reward,0) 
	From [dbo].[RP2021Web_Rewards_E_POGSalesBonus]
	Where STATEMENTCODE = @STATEMENTCODE
	
	--OU Level 
	Select @REWARD_PERCENTAGE=ISNULL(Reward_Percentage,0) 
	FROM [dbo].[RP2021Web_Rewards_E_POGSalesBonus] 
	WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE)
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);

		Declare @ELIGIBLEPOG_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+CAST(@SEASON AS VARCHAR(4))+' ($)' ELSE  CAST(@SEASON AS VARCHAR(4)) +  ' Eligible POG $' END; -- RP-4183

		SET @HEADER = '<th>'+@ELIGIBLEPOG_FrenchEnglish+'</th><th>Reward %</th><th>Reward $</th>'

		IF @LANGUAGE = 'F' -- RP-4183
			BEGIN
				Set @HEADER = REPLACE(@HEADER,'Reward %','Récompense (%)');
				Set @HEADER = REPLACE(@HEADER,'Reward $','Récompense ($)');
			END
		
		SET @HEADER = IIF(@IS_OU = 0,REPLACE(@HEADER,'<REW>','Reward'),REPLACE(@HEADER,'<REW>','Reward OU Level'));

		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE,@PERCENT_FORMAT)  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))
	
		SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext"><LOC></div>
				</div>
			</div>'
		SET @DISCLAIMERTEXT = IIF(@IS_OU = 0,REPLACE(@DISCLAIMERTEXT,'<LOC>',''),REPLACE(@DISCLAIMERTEXT,'<LOC>','Qualifying % based on Operating Unit Totals')) 

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

