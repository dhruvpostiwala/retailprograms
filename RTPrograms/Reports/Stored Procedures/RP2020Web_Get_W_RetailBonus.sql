﻿
CREATE PROCEDURE [Reports].[RP2020Web_Get_W_RetailBonus] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASONSTRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';

	Declare @RewardSummarySection nvarchar(max)='';
	--Declare @HTML_Data as nvarchar(max);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';
	DECLARE @SECTIONHEADER AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX)='';
	DECLARE @TEMP_THEAD AS NVARCHAR(MAX)='';
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';
	DECLARE @PART AS VARCHAR(4)='';


	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';
	DECLARE @RewardsPercentageMatrix AS NVARCHAR(MAX)='';
		
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASONSTRING=CAST(@SEASON AS VARCHAR(4))



	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	--only appears on this market letter 
	SET @MARKETLETTERCODE='ML' + @SEASONSTRING + '_W_CCP'
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE 
		
	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT StatementCode, MarketLetterCode, Part, Rew_Sales_CY, TYAvg,Brand_CY,Reward_Percentage,Reward
	INTO #TEMP
	FROM 
	(
		SELECT 
		  StatementCode
		, MarketLetterCode
		,'LIB' as  Part
		, QualBrand_Sales_CY as Rew_Sales_CY
		, (Growth_Sales_LY1 + Growth_Sales_LY2)/2  as TYAvg
		, Lib_Reward_Sales as Brand_CY
		, Lib_Reward_Percentage as Reward_Percentage
		, Lib_Reward as Reward
		FROM RP2020Web_Rewards_W_RetailBonus
		

		UNION  

		SELECT 
		  StatementCode
		, MarketLetterCode
		,'CENT' as  Part
		, QualBrand_Sales_CY as Rew_Sales_CY
		, (Growth_Sales_LY1 + Growth_Sales_LY2)/2  as TYAvg
		, Cent_Reward_Sales as Brand_CY
		, Cent_Reward_Percentage as Reward_Percentage
		, Cent_Reward as Reward
		FROM RP2020Web_Rewards_W_RetailBonus
	)ALLPARTS
	WHERE ALLPARTS.[Statementcode]=@Statementcode AND ALLPARTS.[MarketLetterCode]=@MarketLetterCode
	
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 	
		GOTO FINAL
	END 

	SET @TEMP_THEAD='
	<thead>
	<tr>
		<th>' + @SEASONSTRING + ' Eligible POG$</th>
		<th>2-Year Average</th>
		<th>Qualifying %</th>
		<th> '+ @SEASONSTRING + ' Eligible <header> POG$</th>
		<th> Reward %</th>
		<th>Reward $</th>
	</tr>
	</thead>'

	DECLARE PART_LOOP CURSOR FOR 
		SELECT  Part FROM #TEMP  ORDER BY [PART] DESC
	OPEN PART_LOOP
	FETCH NEXT FROM PART_LOOP INTO @PART
	WHILE(@@FETCH_STATUS=0)
		BEGIN

			IF @PART='LIB'
			BEGIN
				SET @ML_THEAD_ROW = REPLACE(@TEMP_THEAD,'<header>','Liberty')
			END
			ELSE 
			BEGIN
				SET @ML_THEAD_ROW = REPLACE(@TEMP_THEAD,'<header>','Centurion')
			END

			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Rew_Sales_CY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(TYAvg, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(IIF(TYAvg > 0, Rew_Sales_CY / TYAvg, IIF(Rew_Sales_CY > 0, 999.99/100, 0)), '%')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Brand_CY, '$')  as 'td' for xml path(''), type)						
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)							
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP
					WHERE Part = @PART
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '
			<div class="st_section">
				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD_ROW +   @ML_TBODY  + '
					</table>
				</div>
			</div>' 	
		FETCH NEXT FROM PART_LOOP INTO  @PART
	END
	CLOSE PART_LOOP
	DEALLOCATE PART_LOOP

	/* ****************************** MATRIX TABLE ******************************** */
	SET @RewardsPercentageMatrix = '
	<div class="st_static_section">
		<div class="st_content open">
			<div class = "st_header" style='+'text-align:center; background-color: #DDDDDD;'+'>
						<span>Reward on Liberty 150</span>
			</div>
			<table class="rp_report_table">
				<tr>
					<th>Growth Calculation:
						2020 Eligible POG Sales relative to the 2-year average
					</th>
					<th>
					>= $2.0 Million of Total 2020 Eligible POG
					sales of all reward brands
					</th>
					<th>
					< $2.0 Million of Total 2020 Eligible POG Sales of reward brands
					</th>
				</tr>
				<tr>
					<td>0 to 99.9%</td>
					<td>3%</td>
					<td>2%</td>
				</tr>
				<tr>
					<td>100% to 104.9%</td>
					<td>6%</td>
					<td>4%</td>
				</tr>
			</table>


			<div class = "st_header" style='+'text-align:center; background-color: #DDDDDD;'+'>
						<span>Reward on Centurion</span>
			</div>
			<table class="rp_report_table">
				<tr>
					<th>Growth Calculation:
						2020 Eligible POG Sales relative to the 2-year average
					</th>
					<th>
					>= $2.0 Million of Total 2020 Eligible POG
					sales of all reward brands
					</th>
					<th>
					< $2.0 Million of Total 2020 Eligible POG Sales of reward brands
					</th>
				</tr>
				<tr>
					<td>0 to 99.9%</td>
					<td>1%</td>
					<td>1%</td>
				</tr>
				<tr>
					<td>100% to 104.9%</td>
					<td>3%</td>
					<td>2%</td>
				</tr>
			</table>

		</div>
	</div>' 

	
	

FINAL:	
	/* ############################### FINAL OUTPUT ############################### */

	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

		-- DISCLAIMER TEXT
	SET @DisclaimerText='
	<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier Max, Headline, Heat Complete, Heat LQ, Heat WG, Insure Cereal, Insure Cereal FX4, Insure Pulse, Nealta, Nexicor, Nodulator, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Odyssey WG, Outlook, Poast Ultra, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper ADV, Zampro, and Zidua SC brands</div>
			<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Reward Brands</b> = Liberty & Centurion</div>
			<div class="rprew_subtabletext">*Rewards calculated in Suggested Retail Price (SRP)</div>
		</div>
	</div>'	

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END

	
