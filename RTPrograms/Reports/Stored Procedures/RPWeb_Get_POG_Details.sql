﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_POG_Details] @STATEMENTCODE INT, @LANGUAGE AS VARCHAR(1) = 'E', @HTML_Data VARCHAR(MAX)=NULL OUTPUT

AS
BEGIN
	-----------------
	/*
	Author : Demarey Baker
	April 3 2020
	Retrieves POG Report
	*/
	SET NOCOUNT ON;

	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4);
	DECLARE @CY INT;
	DECLARE @LY INT;
	DECLARE @Region varchar(10);	
	
	DECLARE @SEQUENCE int;
	SET @HTML_Data='';
	DECLARE @DISCLAIMER_TEXT nvarchar(max)='';

	DECLARE @ML_EAST_INVIGOR VARCHAR(50)=''	
	DECLARE @MarketLetterCode Varchar(50);
	DECLARE @MarketLetterName Varchar(200);
	DECLARE @RetailerCode VARCHAR(10);

	DECLARE @Summary_Section NVARCHAR(MAX)='';
	DECLARE @Summary_Thead_Row nvarchar(max) = '';
	DECLARE @Summary_Body nvarchar(max) = '';

	DECLARE @ML_Sections NVARCHAR(MAX)='';			
	DECLARE @ML_Thead_Row nvarchar(max) = '';
	DECLARE @ML_Sub_Total_Row nvarchar(max) = '';
	DECLARE @ML_Product_Rows nvarchar(max) = '';
	
	DECLARE @TOTAL_AMOUNT MONEY=0;
	DECLARE @TOTAL_REWARD MONEY=0;

	DECLARE @STORED_PROCEDURE_NAME VARCHAR(255)
	DECLARE @REWARD_MARGINS AS UDT_RP_REWARD_MARGINS;
		
	DECLARE @Eligible_MarketLetters TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterName Varchar(200) NOT NULL,
		[Sequence] INT NOT NULL,
		[TotalAmount] MONEY NOT NULL DEFAULT 0,
		[TotalReward] MONEY NOT NULL DEFAULT 0
	)

	DECLARE @SKU_LEVEL_SALES AS UDT_RP_SKU_LEVEL_SALES;
	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END

	SELECT @SEASON=Season, @CY=Season, @LY=Season-1, @Region=Region, @RetailerCode=RetailerCode FROM RPWeb_Statements where StatementCode = @STATEMENTCODE;
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))
	SET @ML_EAST_INVIGOR = 'ML' + @SEASON_STRING + '_E_INV'

	--<div class="rprew_subtabletext">* "' + CAST(@LY AS VARCHAR(4)) + ' Eligible" includes 150 km radius compliant POG$</div>
	IF @LANGUAGE = 'F'
		IF @RetailerCode <> 'D0000137'
			SET @Disclaimer_Text='
				<div class="rprew_subtabletext">* Définition de produits non récompensés : Produits BASF exclus du programme de récompenses ' +  CAST(@CY AS VARCHAR(4)) + ' et/ou non considérés aux fins du calcul des récompenses.</div>
				<div class="rprew_subtabletext">*Les VAS admissibles ' + CAST(@LY AS VARCHAR(4)) + ' et ' + CAST(@CY AS VARCHAR(4)) + ' incluent les VAS qui respectent la règle du rayon de 150 km.</div>	
				<br/>'
		ELSE
			SET @Disclaimer_Text='
				<div class="rprew_subtabletext">* Définition de produits non récompensés : Produits BASF exclus du programme de récompenses ' +  CAST(@CY AS VARCHAR(4)) + ' et/ou non considérés aux fins du calcul des récompenses.</div>'
	ELSE
		IF @RetailerCode <> 'D0000137'
			SET @Disclaimer_Text='
				<div class="rprew_subtabletext">* Non-Reward Brands defined as BASF brands that are not used in the qualification &/or calculation of ' + CAST(@CY AS VARCHAR(4)) + ' rewards programs</div>
				<div class="rprew_subtabletext">* "' + CAST(@LY AS VARCHAR(4)) + ' & ' + CAST(@CY AS VARCHAR(4)) + ' Eligible POG$" includes 150 km radius compliant POG$</div>	
				<br/>'
		ELSE
			SET @Disclaimer_Text='
				<div class="rprew_subtabletext">* Non-Reward Brands defined as BASF brands that are not used in the qualification &/or calculation of ' + CAST(@CY AS VARCHAR(4)) + ' rewards programs</div>'

	INSERT INTO @Eligible_MarketLetters (MarketLetterCode, MarketLetterName, [Sequence])
	SELECT 'NON_REWARD_BRANDS' AS MarketLetterCode, CASE WHEN @LANGUAGE = 'F' THEN 'Produits non récompensés' ELSE 'Non-Reward Brands' END MarketLetterName
	, 99 as [Sequence]		
		UNION	
	SELECT MarketLetterCode, CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence]
	FROM RP_Config_MarketLetters 
	WHERE IncludeInActual='Yes' AND MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE [StatementCode]=@STATEMENTCODE)
	/*********************** DO NOT REMOVE FOLLOWING COMMENTED CODE  ***********************/
		/*
				UNION
			SELECT 'NON-EXISTING' AS MarketLetterCode, 'Non-Existing Brands' MarketLetterName, 0 as [Sequence]
		*/
	
		/*
		-- TRANSACTIONS @ SKU LEVELS
		IF(NOT(OBJECT_ID('tempdb..#SKU_LEVEL_SALES') IS NULL)) DROP TABLE #SKU_LEVEL_SALES 

		SELECT *
		INTO #SKU_LEVEL_SALES
		FROM TVF2020_Get_MarketLetter_Sales(@StatementCode)
		*/
	/*********************** DO NOT REMOVE FOLLOWING COMMENTED CODE  ***********************/

	INSERT @SKU_LEVEL_SALES
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE
	
	-- WE ONLY NEED TO SHOW BRANDS THAT ACTUALLY HAVE EITHER CY OR LY TRANSACTIONS
	DELETE FROM @SKU_LEVEL_SALES 	WHERE CYQuantity=0 AND LYQuantity=0 

	-- IF NO LIBERTY 200 TRANSACTIONS EXIST, DO NOT DISPLAY LIBERTY 200 IN SUMMARY AND DETAILS 
	IF @SEASON=2020
		DELETE EML
		FROM @Eligible_MarketLetters  EML	
			LEFT JOIN (
				SELECT DISTINCT MarketLetterCode FROM @SKU_LEVEL_SALES WHERE MarketLetterCode='ML2020_W_LIBERTY_200'
			) SKU
			ON SKU.MarketLetterCode=EML.MarketLetterCode
		WHERE EML.MarketLetterCode='ML2020_W_LIBERTY_200' AND ISNULL(SKU.MarketLetterCode,'')=''

	UPDATE T1
	SET [TotalAmount]=T2.CYEligibleSales
	FROM @Eligible_MarketLetters T1
		INNER JOIN (
			SELECT MarketLetterCode
				,SUM(CYSales) as CYSales
				,SUM(CYEligibleSales) as CYEligibleSales
			FROM @SKU_LEVEL_SALES			
			GROUP BY MarketLetterCode
		) T2
		ON T2.MarketLetterCode=T1.MarketLetterCode

	
	UPDATE T1
	SET [TotalReward]=T2.[TotalReward]
	FROM @Eligible_MarketLetters T1
		INNER JOIN (
			SELECT MarketLetterCode, SUM(TotalReward) AS TotalReward
			FROM RPWeb_Rewards_Summary
			WHERE Statementcode=@Statementcode
			GROUP BY MarketLetterCode
		) T2
		ON T2.MarketLetterCode=T1.MarketLetterCode


	BEGIN
		SET @STORED_PROCEDURE_NAME = 'Reports.RP' + @SEASON_STRING + '_Get_RewardMargins_' + @REGION

		INSERT @REWARD_MARGINS
		EXEC  @STORED_PROCEDURE_NAME @STATEMENTCODE
	END

	-- ****************************************************************** --

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs pog_report">
		<div class="rp_program_header">
			<h1>' + CASE WHEN @LANGUAGE = 'F' THEN 'RAPPORT DE VAS' ELSE 'POG REPORT' END + '</h1>
			<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
		</div>';

	IF @SEASON >= 2021
		BEGIN

			IF @LANGUAGE = 'F' 
				SET @ML_Thead_Row = CONVERT(NVARCHAR(MAX), (
						SELECT 'Nom du produit' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@LY) + ' (quantité)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@LY) + ' ($)' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@CY) + ' (quantité)' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@CY) + ' ($)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@CY) +' (quantité)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@CY) +' ($)' AS 'th' 
							,'Récompense par produit ' + convert(varchar(4),@CY) +' (%)' AS 'th' 
						FOR XML RAW('tr'), ELEMENTS, TYPE))
			ELSE
				SET @ML_Thead_Row = CONVERT(NVARCHAR(MAX), (
					SELECT 'Product Name' AS 'th'
						,convert(varchar(4),@LY) + ' Eligible POG QTY' AS 'th'
						,convert(varchar(4),@LY) + ' Eligible POG$' AS 'th'
						,convert(varchar(4),@CY) + ' Actual POG QTY' AS 'th'
						,convert(varchar(4),@CY) + ' Actual POG$' AS 'th'
						,convert(varchar(4),@CY) +' Eligible POG QTY' AS 'th'
						,convert(varchar(4),@CY) +' Eligible POG$' AS 'th' 
						,'Margin %' AS 'th' 
					FOR XML RAW('tr'), ELEMENTS, TYPE))

		END
	ELSE
		BEGIN

			IF @LANGUAGE = 'F' 
				SET @ML_Thead_Row = CONVERT(NVARCHAR(MAX), (
						SELECT 'Nom du produit' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@LY) + ' (quantité)' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@LY) + ' ($)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@LY) + ' (quantité)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@LY) + ' ($)' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@CY) + ' (quantité)' AS 'th'
							,'VAS réalisées ' + convert(varchar(4),@CY) + ' ($)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@CY) +' (quantité)' AS 'th'
							,'VAS admissibles ' + convert(varchar(4),@CY) +' ($)' AS 'th' 
							,'Récompense par produit ' + convert(varchar(4),@CY) +' (%)' AS 'th' 
						FOR XML RAW('tr'), ELEMENTS, TYPE))
			ELSE
				SET @ML_Thead_Row = CONVERT(NVARCHAR(MAX), (
					SELECT 'Product Name' AS 'th'
						,convert(varchar(4),@LY) + ' Actual POG QTY' AS 'th'
						,convert(varchar(4),@LY) + ' Actual POG$' AS 'th'
						,convert(varchar(4),@LY) + ' Eligible POG QTY' AS 'th'
						,convert(varchar(4),@LY) + ' Eligible POG$' AS 'th'
						,convert(varchar(4),@CY) + ' Actual POG QTY' AS 'th'
						,convert(varchar(4),@CY) + ' Actual POG$' AS 'th'
						,convert(varchar(4),@CY) +' Eligible POG QTY' AS 'th'
						,convert(varchar(4),@CY) +' Eligible POG$' AS 'th' 
						,convert(varchar(4),@CY) +' Reward Margin/Brand' AS 'th' 
					FOR XML RAW('tr'), ELEMENTS, TYPE))

		END


		--new demarey - remove pog report heading RP-2518
		SET @HTML_Data += '<div class="st_section first">' + IIF(@RetailerCode = 'D0000137' OR @SEASON >= 2021, '', @DISCLAIMER_TEXT) + 
				IIF(@RETAILERCODE = 'D0000137', '', '<!--<div class = "st_header">
					<span>POG Report</span>
					<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>-->') +
				'<div class="st_content open">
					<table class="rp_report_table">
						<thead>' + @ML_Thead_Row + '</thead>
						<tbody>'

	--LOOP THROUGH MARKET LETTERS
	DECLARE @FirstMarketLetter BIT=1;

	DECLARE MARKET_LETTER_LOOP CURSOR FOR 
		SELECT [MarketLetterCode], [MarketLetterName], [Sequence], [TotalAmount], [TotalReward] FROM @Eligible_MarketLetters	ORDER BY [Sequence]
	OPEN MARKET_LETTER_LOOP
	FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE, @TOTAL_AMOUNT, @TOTAL_REWARD
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @ML_Sub_Total_Row = ''
			SET @ML_Product_Rows = ''
			DROP TABLE IF EXISTS #ML_SKU_DETAILS 

			SELECT MarketLetterName , T1.ChemicalGroup AS RewardBrand ,ProductName
				,LYQuantity ,LYEligibleQty ,LYSales ,LYEligibleSales
				,CYQuantity ,CYSales ,CYEligibleQty ,CYEligibleSales
				,ISNULL(T2.Reward_Margin,0) AS Reward_Margin				
			INTO #ML_SKU_DETAILS
			FROM @SKU_LEVEL_SALES T1
				LEFT JOIN (
					SELECT MarketLetterCode, ChemicalGroup ,SUM(Overall_Reward_Percentage) AS Reward_Margin
					FROM @REWARD_MARGINS
					WHERE MarketLetterCode=@MarketLettercode
					GROUP BY MarketLetterCode, ChemicalGroup
				) T2
				ON T2.MarketLetterCode=T1.MarketLettercode AND T2.ChemicalGroup=T1.ChemicalGroup
			WHERE T1.MarketletterCode = @MARKETLETTERCODE  AND T1.ChemicalGroup <> 'CLEARFIELD LENTILS'	
			ORDER BY ProductName


			IF @SEASON >= 2021
				BEGIN
					IF ( @MARKETLETTERCODE <> @ML_EAST_INVIGOR
							OR 							
						EXISTS (SELECT TOP 1 1 FROM #ML_SKU_DETAILS) 
					)
					BEGIN
					DECLARE @A int;
					-- MARKET LETTER - SUB TOTAL ROW
					SET @ML_Sub_Total_Row = CONVERT(NVARCHAR(MAX), (
											SELECT 'sub_total_row' as [@class], (SELECT @MARKETLETTERNAME AS 'td' for xml path(''), type) 
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CAST(LYEligibleSales AS MONEY),@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(@TOTAL_AMOUNT=0,0,@TOTAL_REWARD/@TOTAL_AMOUNT),@PERCENT_FORMAT) as 'td' for xml path(''), type)
										FROM (
												SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
												FROM #ML_SKU_DETAILS									
											)  D 
											FOR XML PATH('tr'), ELEMENTS, TYPE))
								

					-- MARKET LETTER - PRODCUTS SKU ROWS
					SET @ML_Product_Rows = ISNULL( CONVERT(NVARCHAR(MAX),(
												SELECT	(SELECT  ProductName as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(LYEligibleQty,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(CYQuantity,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(CYEligibleQty,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(REWARD_MARGIN,@PERCENT_FORMAT) as 'td' for xml path(''), type)
										FROM (
												SELECT ProductName
													,LYQuantity ,LYSales ,LYEligibleQty, LYEligibleSales
													,CYQuantity ,CYSales ,CYEligibleQty, CYEligibleSales	
													,REWARD_MARGIN
												FROM #ML_SKU_DETAILS									
										)  D
										ORDER BY ProductName
										FOR XML RAW('tr'), ELEMENTS, TYPE))
									,'<tr><td colspan=10 align=center>'+CASE WHEN @LANGUAGE ='F' THEN 'AUCUNE TRANSACTION ENREGISTRÉE' ELSE 'NO TRANSACTIONS FOUND' END+'</td></tr>')
					END

					IF @MARKETLETTERCODE='NON_REWARD_BRANDS' 
					BEGIN
						SET @HTML_Data	+= ISNULL(
										CONVERT(NVARCHAR(MAX), (
											SELECT  'sub_total_row' as [@class], (SELECT 
											CASE WHEN @LANGUAGE = 'F' THEN 'Total des produits récompensés' 
											ELSE 'Total Reward Brands' END AS 'td' for xml path(''), type) 																													
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
										FROM (
											SELECT SUM(LYSales) as LYSales
												,SUM(LYEligibleSales) as LYEligibleSales
												,SUM(CYSales) as CYSales
												,SUM(CYEligibleSales) as CYEligibleSales
											FROM @SKU_LEVEL_SALES
											WHERE MarketLetterCode <> 'NON_REWARD_BRANDS'														
										)  D
										FOR XML PATH('tr'), ELEMENTS, TYPE))
									,'<tr class="sub_total_row formatPOGMain">
											<td>' + CASE WHEN @LANGUAGE = 'F' THEN 'Total des produits récompensés' 
											ELSE 'Total Reward Brands' END + '</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
									</tr>')

						SET @HTML_Data	+= '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>'
					END					
			
				END

			ELSE
				BEGIN
					--IF EXISTS (SELECT * FROM #ML_SKU_DETAILS WHERE @MARKETLETTERCODE = 'ML' + CAST(@SEASON AS VARCHAR(4)) + '_E_INV') OR @MARKETLETTERCODE <> 'ML' + CAST(@SEASON AS VARCHAR(4)) + '_E_INV'
					IF ( @MARKETLETTERCODE <> @ML_EAST_INVIGOR
							OR 							
						EXISTS (SELECT TOP 1 1 FROM #ML_SKU_DETAILS) 
					)
					BEGIN
					-- MARKET LETTER - SUB TOTAL ROW
					SET @ML_Sub_Total_Row = CONVERT(NVARCHAR(MAX), (
											SELECT 'sub_total_row' as [@class], (SELECT @MARKETLETTERNAME AS 'td' for xml path(''), type) 
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(@TOTAL_AMOUNT=0,0,@TOTAL_REWARD/@TOTAL_AMOUNT),@PERCENT_FORMAT) as 'td' for xml path(''), type)
										FROM (
												SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
												FROM #ML_SKU_DETAILS									
											)  D 
											FOR XML PATH('tr'), ELEMENTS, TYPE))
								

					-- MARKET LETTER - PRODCUTS SKU ROWS
					SET @ML_Product_Rows = ISNULL( CONVERT(NVARCHAR(MAX),(
												SELECT	(SELECT  ProductName as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] , dbo.DisplayValuesFinancially(LYQuantity,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(LYEligibleQty,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(CYQuantity,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.DisplayValuesFinancially(CYEligibleQty,0) as 'td' for xml path(''), type)
												,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
												--,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(@TOTAL_AMOUNT=0,0,  (@TOTAL_REWARD/@TOTAL_AMOUNT) * (CYSales/@TOTAL_AMOUNT)),'%') as 'td' for xml path(''), type)
												,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(REWARD_MARGIN,'%') as 'td' for xml path(''), type)
										FROM (
												SELECT ProductName
													,LYQuantity ,LYSales ,LYEligibleQty, LYEligibleSales
													,CYQuantity ,CYSales ,CYEligibleQty, CYEligibleSales	
													,REWARD_MARGIN
												FROM #ML_SKU_DETAILS									
										)  D
										ORDER BY ProductName
										FOR XML RAW('tr'), ELEMENTS, TYPE))
									,'<tr><td colspan=10 align=center>NO TRANSACTIONS FOUND</td></tr>')
					END

					IF @MARKETLETTERCODE='NON_REWARD_BRANDS' 
					BEGIN
						SET @HTML_Data	+= ISNULL(
										CONVERT(NVARCHAR(MAX), (
											SELECT  'sub_total_row' as [@class], (SELECT 
											CASE WHEN @LANGUAGE = 'F' THEN 'Total des produits récompensés' 
											ELSE 'Total Reward Brands' END AS 'td' for xml path(''), type) 															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
										FROM (
											SELECT SUM(LYSales) as LYSales
												,SUM(LYEligibleSales) as LYEligibleSales
												,SUM(CYSales) as CYSales
												,SUM(CYEligibleSales) as CYEligibleSales
											FROM @SKU_LEVEL_SALES
											WHERE MarketLetterCode <> 'NON_REWARD_BRANDS'																		
										)  D
										FOR XML PATH('tr'), ELEMENTS, TYPE))
									,'<tr class="sub_total_row formatPOGMain">
											<td>' + CASE WHEN @LANGUAGE = 'F' THEN 'Total des produits récompensés' 
											ELSE 'Total Reward Brands' END + '</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
									</tr>')

						SET @HTML_Data	+= '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>'
					END

				END

			SET @HTML_Data += @ML_Sub_Total_Row + @ML_Product_Rows
			
			SET @FirstMarketLetter=0
			FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE, @TOTAL_AMOUNT, @TOTAL_REWARD
		END
	CLOSE MARKET_LETTER_LOOP
	DEALLOCATE MARKET_LETTER_LOOP


	IF @SEASON >= 2021
		BEGIN
			SET @HTML_Data +=  ISNULL(
										CONVERT(NVARCHAR(MAX), (
										SELECT 'total_row formatPOGMain' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 																
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
										FROM (
											SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
											FROM @SKU_LEVEL_SALES									
										)  D
										FOR XML PATH('tr'), ELEMENTS, TYPE))
									,'<tr class="total_row formatPOGMain">
											<td>Total</td>									
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
										')
		END
	ELSE
		BEGIN
			SET @HTML_Data +=  ISNULL(
										CONVERT(NVARCHAR(MAX), (
										SELECT 'total_row formatPOGMain' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 		
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
											,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
										FROM (
											SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
											FROM @SKU_LEVEL_SALES									
										)  D
										FOR XML PATH('tr'), ELEMENTS, TYPE))
									,'<tr class="total_row formatPOGMain">
											<td>Total</td>									
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
											<td class="right_align">$0.00</td>
											<td class="right_align"></td>
										')
		END

	SET @HTML_Data +=  '</tbody>
					</table>
				</div>
				' + IIF(@RetailerCode = 'D0000137' OR @SEASON >= 2021, @Disclaimer_Text, '') + '
			</div>
		</div>' 

	IF(NOT(OBJECT_ID('tempdb..#ML_SKU_DETAILS') IS NULL)) DROP TABLE #ML_SKU_DETAILS --Drop the temp table incase it exists

		   
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')

	IF @Season <= 2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
		
END
