﻿
CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_InventoryManagement] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @LEVEL5_CODE VARCHAR(20)

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @REWARD_SALES MONEY=0;
	DECLARE @REWARD_PERCENTAGE DECIMAL(2,2)=0;
	DECLARE @ENDING_INV_PERCENTAGE DECIMAL(6,4)
	DECLARE @REWARD MONEY=0;
	
	SELECT @LEVEL5_CODE = Level5Code
	FROM RPWeb_Statements
	WHERE StatementCode = @StatementCode

	SELECT @PROGRAMTITLE = Title, @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

		-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT
	
	/* ############################### CPP REWARD SUMMARY TABLE ############################### */
	--FETCH REWARD DISPLAY VALUES
	
	SELECT @REWARD_SALES= SUM(ISNULL(CPP_Sales,0) + ISNULL(Liberty_Sales,0) + ISNULL(Centurion_Sales, 0))
		  ,@REWARD_PERCENTAGE = MAX(ISNULL(CPP_Percentage, 0))
		  ,@REWARD = SUM(ISNULL(Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_InventoryManagement]
	WHERE STATEMENTCODE = @STATEMENTCODE AND MarketLetterCode='ML2021_W_CPP'
	GROUP BY Statementcode,MarketLetterCode,ProgramCode
	

	IF @LEVEL5_CODE <> 'D520062427' -- NUTRIEN DOES NOT HAVE SEGMENT A REWARD
	BEGIN
		-- SEGMENT A
		SET @HEADER = '<th>' + cast(@Season as varchar(4)) + ' Eligible Crop Protection POG $</th> 
						<th>Reward %</th>
						<th>Reward $</th>'
		
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$')  as 'td' for xml path(''), type)
					,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)		
		FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		SET @REWARDSUMMARYTABLE += '<div class="st_section">
			<div class="st_content open">
				<h2>SEGMENT A</h2>
				<table class="rp_report_table"> 
					<thead>
					<tr>
							'+ @HEADER +'						
					</tr>
					</thead>
							'+ @SUMMARY + '
				</table>
			</div>
		</div>' 
	END


	/* ############################### INV REWARD SUMMARY TABLE ############################### */
	IF @LEVEL5_CODE IN ('D0000107','D0000244','D0000137','D520062427')
	BEGIN
		-- SEGMENT B

		IF @LEVEL5_CODE = 'D0000107'
			SET @ENDING_INV_PERCENTAGE  = 0.075 -- 7.5%
		ELSE IF @LEVEL5_CODE = 'D0000244'
			SET @ENDING_INV_PERCENTAGE  = 0.067 -- 6.7%
		ELSE IF @LEVEL5_CODE = 'D0000137'
			SET @ENDING_INV_PERCENTAGE  = 0.048 -- 4.8%
		ELSE IF @LEVEL5_CODE = 'D520062427'
			SET @ENDING_INV_PERCENTAGE  = 0.087 -- 8.7%

		SET @HEADER = '<th>' + cast(@Season as varchar(4)) + ' Eligible InVigor POG $</th> 
						<th>Ending Inventory %</th>
						<th>Reward %</th>
						<th>Reward $</th>'
		
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](ISNULL(InVigor_Sales,0),'$')  as 'td' for xml path(''), type)
					,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@ENDING_INV_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](InVigor_Reward_Percentage,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](Reward,'$') as 'td' for xml path(''), type)
		FROM [dbo].[RP2021Web_Rewards_W_InventoryManagement]
		WHERE STATEMENTCODE = @STATEMENTCODE AND MarketLetterCode='ML2021_W_INV'
		FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		SET @REWARDSUMMARYTABLE += '<div class="st_section">		
			<div class="st_content open">
				<h2>SEGMENT B</h2>
				<table class="rp_report_table"> 
					<thead>
					<tr>
							'+ @HEADER +'						
					</tr>
					</thead>
							'+ @SUMMARY + '
				</table>
			</div>
		</div>' 
	END
	   
	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">'

	IF @LEVEL5_CODE <> 'D520062427'
	SET @DISCLAIMERTEXT +=' 
			<div class="rprew_subtabletext">
				<strong>Segment A Qualifying and Reward Brands:</strong> Eligible 2021 Crop Protection Reward Brands excluding Nodulator brands.
			</div>'

	IF @LEVEL5_CODE IN ('D0000107','D0000244','D0000137','D520062427') 
	SET @DISCLAIMERTEXT += ' 
			<div class="rprew_subtabletext">
				<strong>Segment B Qualifying and Reward Brands:</strong> Eligible 2021 InVigor Reward brands.
			</div>'
	
	SET @DISCLAIMERTEXT += ' 
		</div>
	 </div>'
	
	IF @LEVEL5_CODE = 'D520062427'
	BEGIN
		SET @REWARDSUMMARYTABLE = REPLACE(@REWARDSUMMARYTABLE,'<h2>SEGMENT B</h2>','')
		SET @DISCLAIMERTEXT = REPLACE(@DISCLAIMERTEXT,'Segment B Qualifying','Qualifying')
	END 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END