﻿
CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_InvLibertyLoyaltyBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS VARCHAR(MAX)='';

	DECLARE @QUALIFICATION_TABLE  AS NVARCHAR(MAX)='';
	DECLARE @REWARD_TABLE  AS NVARCHAR(MAX)='';
	DECLARE @HEADER AS NVARCHAR(MAX);
	DECLARE @SUMMARY  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	

	DECLARE @INVIGOR_BAGS FLOAT = 0;
	DECLARE @LIBERTY_JUGS FLOAT = 0;
	DECLARE @INVIGOR_LIBERTY_RATIO FLOAT = 0;

	DECLARE @ORGANIC_DATA VARCHAR(MAX)='';

	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--FETCH REWARD DISPLAY VALUES
	SELECT @INVIGOR_BAGS = MAX(Invigor_Acres / 10)
		   ,@LIBERTY_JUGS = MAX(Liberty_Acres / 10)
	FROM RP2021Web_Rewards_W_INV_LLB T1
	WHERE T1.StatementCode = @STATEMENTCODE
	GROUP BY T1.StatementCode, T1.ProgramCode

	SET @INVIGOR_LIBERTY_RATIO = 
		CASE
			WHEN @INVIGOR_BAGS = 0 OR @LIBERTY_JUGS <= 0 THEN 0						
			WHEN @LIBERTY_JUGS/@INVIGOR_BAGS >= 9.9999 THEN 9.9999
			ELSE @LIBERTY_JUGS/@INVIGOR_BAGS
		END
			
	DROP TABLE IF EXISTS #DETAILS
	SELECT	T1.RewardBrand,
			SUM(T1.RewardBrand_Sales) AS RewardBrand_Sales,
			MAX(T1.Bonus_Percentage + T1.Addendum_Percentage) AS Reward_Percentage,
			SUM(T1.Reward) AS Reward
	INTO #DETAILS
	FROM RP2021Web_Rewards_W_INV_LLB T1
	WHERE T1.StatementCode = @STATEMENTCODE
	GROUP BY T1.StatementCode, T1.ProgramCode, T1.RewardBrand

			
	IF NOT EXISTS(SELECT * FROM #DETAILS)
	BEGIN
		SET @QUALIFICATION_TABLE = '<div class="st_section">
				<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 
		GOTO FINAL
	END 





	/* ############################### QUALIFICATION TABLE ############################### */

	SET @HEADER='<tr><thead>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible InVigor Bags</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible Liberty Jugs</th>
			<th>Liberty to InVigor Ratio</th>		
		</thead></tr>'

	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@INVIGOR_BAGS,'')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@LIBERTY_JUGS,'')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@INVIGOR_LIBERTY_RATIO,'')  as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	SET @QUALIFICATION_TABLE ='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 

	/* ############################### REWARD TABLE ############################### */
	-- NOTE: GOING TO REUSE @HEADER AND @SUMMARY

	SET @HEADER='<tr><thead>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible Reward Brands</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RewardBrand AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)										
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY RewardBrand
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @REWARD_TABLE='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 

	
	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	SET @ORGANIC_DATA = '{
							"sales":['+cast((SELECT RewardBrand_Sales FROM #DETAILS WHERE RewardBrand='InVigor') as varchar(20))+','+cast((SELECT RewardBrand_Sales FROM #DETAILS WHERE RewardBrand='Liberty 150') as varchar(20))+'],
							"reward_margins":['+cast((SELECT Reward_Percentage FROM #DETAILS WHERE RewardBrand='InVigor') as varchar(20))+','+cast((SELECT Reward_Percentage FROM #DETAILS WHERE RewardBrand='Liberty 150') as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible Reward Brands","'+cast(@Season as varchar(4))+' Eligible InVigor POG $"],
							"row_labels":["InVigor","Liberty 150"]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='4C-2R', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT
	
	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Liberty 150, InVigor L340PC, LR344PC, L345PC, L352C, L357P, L230, L233P, L234PC, L241C, L252, L255PC.
			</div>
			<div class="rprew_subtabletext">
				Note: For the purpose of calculating the Liberty to InVigor Ratio, 2021 Eligible InVigor Bags does not include any InVigor Reseed amounts
			</div>
		</div>
	 </div>'

FINAL:		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @QUALIFICATION_TABLE
	SET @HTML_Data += @REWARD_TABLE
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

