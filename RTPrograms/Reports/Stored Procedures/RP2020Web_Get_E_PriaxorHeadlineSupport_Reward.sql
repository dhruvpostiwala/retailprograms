﻿





CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_PriaxorHeadlineSupport_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @SEASON_LY INT = @SEASON - 1 ;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @PRIAXOR_ACRES FLOAT = 0;
	DECLARE @HEADLINE_ACRES FLOAT = 0;
	DECLARE @PRIAXOR_SALES MONEY = 0;
	DECLARE @HEADLINE_SALES MONEY = 0;
	DECLARE @PRIAXOR_REWARD MONEY = 0;
	DECLARE @HEADLINE_REWARD MONEY = 0;
	DECLARE @REWARD MONEY;
	DECLARE @IS_OU INT;
	
	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode 
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	--FETCH REWARD DISPLAY VALUES
	SELECT @PRIAXOR_ACRES=ISNULL(Priaxor_Acres,0)
		  ,@PRIAXOR_SALES=ISNULL(Priaxor_Sales,0)
		  ,@HEADLINE_ACRES=ISNULL(Headline_Acres,0)
		  ,@HEADLINE_SALES=ISNULL(Headline_Sales,0)
		  ,@PRIAXOR_REWARD=ISNULL(Priaxor_Reward,0)
		  ,@HEADLINE_REWARD=ISNULL(Headline_Reward,0) 
		  ,@REWARD =ISNULL(Reward,0) 
	From [dbo].[RP2020Web_Rewards_E_PriaxorHeadlineSupport]
	Where STATEMENTCODE = @STATEMENTCODE
	
	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;
					
	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);
	
		IF @LANGUAGE = 'F'
			SET @HEADER = '<th>Produits</th>
						<th>VAS admissibles '+CAST(@SEASON AS VARCHAR(4))+' ($)</th> 
						<th>VAS admissibles ' + cast(@Season as varchar(4)) + ' (acres)</th>
						<th>$ par acre</th>
						<th>Récompense ($)</th>'
		ELSE
			SET @HEADER = '<th>Products</th>
							<th>'+CAST(@SEASON AS VARCHAR(4))+' Eligible POG $</th> 
							<th> ' + cast(@Season as varchar(4)) + ' Eligible POG Acres</th>
							<th>$ per Acre</th>
							<th>Reward $</th>'
		
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'left_align' as [td/@class] , 'Headline' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@HEADLINE_SALES,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@HEADLINE_ACRES,'')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](1.50,'$') as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@HEADLINE_REWARD,'$')  as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))

		SELECT @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'left_align' as [td/@class] , 'Priaxor' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@PRIAXOR_SALES,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@PRIAXOR_ACRES,'')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](1.00,'$') as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@PRIAXOR_REWARD,'$')  as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))

		--Total Row
		SELECT @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT
					(select 'left_align rp_bold' as [td/@class] , 'Total' as 'td' for xml path(''), type)
					,(select 'right_align rp_bold' as [td/@class] , [dbo].[SVF_Commify](@HEADLINE_SALES + @PRIAXOR_SALES ,'$')  as 'td' for xml path(''), type)
					,(select 'right_align rp_bold' as [td/@class] , [dbo].[SVF_Commify](@HEADLINE_ACRES + @PRIAXOR_ACRES ,'')  as 'td' for xml path(''), type)
					,(select 'right_align rp_bold' as [td/@class] , ''  as 'td' for xml path(''), type)
					,(select 'right_align rp_bold' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$')  as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))




	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 


		SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">' +
			CASE WHEN @LANGUAGE = 'F' THEN '
				<tr>
					<th>Produits </th>
					<th>Dose (acres/boîte)</th>
				</tr>' ELSE '<tr>
					<th>Products </th>
					<th>Acres/Case Application Rate</th>
				</tr>' END + '
				<tr>
					<td class="left_align">Headline </td>
					<td class="left_align"> 72 acres </td>
				</tr>
				<tr>
					<td class="left_align">Priaxor</td>
					<td class="left_align">160 acres</td>
				</tr>				
			</table>
		</div>
	</div>' 


	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'	
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX 
	SET @HTML_Data += @DISCLAIMERTEXT
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

