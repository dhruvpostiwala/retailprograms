﻿





CREATE  PROCEDURE [Reports].[RP2021Web_Get_E_SalesSupportLoyaltyBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @REWARD_PERC FLOAT=0;
	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD MONEY;
	DECLARE @SALES_CY_E AS MONEY=0;
	DECLARE @SALES_FOR AS MONEY=0;
	DECLARE @IS_OU INT;

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	
	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode  
	From RP_Config_Programs 
	Where ProgramCode=@PROGRAMCODE
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE   ---returns zero if its an OU

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	

	--FETCH REWARD DISPLAY VALUES
	SELECT @SALES_FOR =ISNULL(RewardBrand_POG_Sales,0)
		  ,@SALES_CY_E=ISNULL(RewardBrand_Sales,0)
		  ,@REWARD_PERC =ISNULL(Reward_Percentage,0) 
		  ,@REWARD =ISNULL(Reward,0) 
	From [dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
	Where STATEMENTCODE = @STATEMENTCODE
	
	--OU Level 
	Select @QUAL_PERCENTAGE=ISNULL(RewardBrand_Sales/NULLIF(RewardBrand_POG_Sales,0),0) 
	FROM [dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus] WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE)
	   	
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);
	
	IF @LANGUAGE = 'F'
	BEGIN
		
		IF @IS_OU = 0 
		--OU
		BEGIN
			SET @HEADER = '<th>VAS prévues '+ cast(@Season as varchar(4)) + '</th> 
						<th> VAS réalisées ' + cast(@Season as varchar(4)) + ' ($)</th>
						<th>% admissible </th>
						<th>VAS admissibles ' + cast(@Season as varchar(4)) + ' ($)</th>
						<th>Récompense (%)</th>
						<th>Récompense ($)</th>'
			
			SET @REWARDSPERCENTAGEMATRIX = '
				<div class="st_static_section">
					<div class="st_content open">
						<table class="rp_report_table">
							<tr>
								<th>VAS admissibles '+cast(@Season as varchar(4))+' vs plan de VAS '+cast(@Season as varchar(4))+'</th>
								<th>Récompense</th>
							</tr>
							<tr>
								<td class="center_align">100 % et +</td>
								<td class="center_align"> 4,25 % </td>
							</tr>
							<tr>
								<td class="center_align">95 % à 99,9 %</td>
								<td class="center_align"> 2,5 % </td>
							</tr>
							<tr>
								<td class="center_align">90 % à 94,9 %</td>
								<td class="center_align"> 1 % </td>
							</tr>
				
						</table>
					</div>
				</div>' 


		END
		ELSE
		BEGIN
			SET @HEADER = '<th>VAS admissibles ' + cast(@Season as varchar(4)) + ' ($)</th>
							<th>% admissible OU Level</th>
							<th>Récompense (%)</th>
							<th>Récompense ($)</th>'

			SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
					<div class="rprew_subtabletext">*% Éligible basé sur les totaux des unités opérationnelles</div>
					</div>
				</div>'
		END
	END
	ELSE
	BEGIN
		IF @IS_OU = 0 
		--OU
		BEGIN
			SET @HEADER = '<th>'+ cast(@Season as varchar(4)) + ' POG Sales Forecast</th> 
						<th>Qualifying % </th>
						<th>' + cast(@Season as varchar(4)) + ' Eligible POG$</th>
						<th>Reward%</th>
						<th>Reward $</th>'
			
			SET @REWARDSPERCENTAGEMATRIX = '
				<div class="st_static_section">
					<div class="st_content open">
						<table class="rp_report_table">
							
							<tr>
								<th>Eligible 2021 POG vs 2021 POG Plan</th>
								<th>Reward</th>
							</tr>
							
							<tr>
								<td>100% +</td>
								<td class="center_align"> 4.25% </td>
							</tr>
							
							<tr>
								<td>95% - 99.9%</td>
								<td class="center_align"> 2.5% </td>
							</tr>

							
							<tr>
								<td>90% - 94.9%</td>
								<td class="center_align"> 1% </td>
							</tr>
							
							
				
						</table>
					</div>
				</div>' 


		END
		ELSE
		BEGIN
			SET @HEADER = '<th>' + cast(@Season as varchar(4)) + ' Eligible POG $</th><th>Qualifying % OU Level </th><th>Reward%</th><th>Reward $</th>'

			SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
					<div class="rprew_subtabletext"></div>
					</div>
				</div>'
		END
	END

	IF @IS_OU = 0
	--OU
	BEGIN
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
						(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_FOR,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,@PERCENT_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,@PERCENT_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
			FOR XML RAW('tr'), ELEMENTS, TYPE))
	END
	ELSE
	BEGIN
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
						(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,@PERCENT_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,@PERCENT_FORMAT)  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
			FOR XML RAW('tr'), ELEMENTS, TYPE))
	END

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

