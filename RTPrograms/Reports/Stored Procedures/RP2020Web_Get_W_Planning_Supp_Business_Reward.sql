﻿



CREATE PROCEDURE [Reports].[RP2020Web_Get_W_Planning_Supp_Business_Reward] @StatementCode int, @ProgramCode varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @LEVEL5CODE VARCHAR(50);

	DECLARE @RETAILERCODE VARCHAR(50);

	DECLARE @SEASON AS INT;
	DECLARE @REGION AS VARCHAR(4)='';

	DECLARE @RewardSummarySection as NVARCHAR(max)='';

	DECLARE @SectionHeader as varchar(200);


	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';

	DECLARE @ML_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';
	
	DECLARE @ML_GrowthSummary_Section as nvarchar(max)='';		
	DECLARE @ML_GrowthSummary_TBody as nvarchar(max)='';

	DECLARE @RewardsPercentageMatrix as nvarchar(max)='';
	DECLARE @DisclaimerText AS NVARCHAR(MAX) = '';
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @RETAILERCODE=RetailerCode, @LEVEL5CODE=Level5Code FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT


	DROP TABLE IF EXISTS #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)
		

	DROP TABLE IF EXISTS #TEMP 
	SELECT MarketLetterCode,ProgramCode,GroupLabel,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales,Growth_Reward_Percentage,Growth_Reward,Lib_Reward_Sales,Lib_Reward_Percentage,Lib_Reward,Cent_Reward_Sales
		,Cent_Reward_Percentage,Cent_Reward,Reward,Early_Order_Percentage,Take_Delivery_Percentage
	INTO #TEMP
	FROM RP2020Web_Rewards_W_PlanningTheBusiness PTB		
	WHERE PTB.Statementcode=@StatementCode 
		
	UPDATE #TEMP
	SET GroupLabel='Spring Herbicides'
	WHERE MarketLetterCode='ML2020_W_CER_PUL_SB' AND GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)')

	UPDATE #TEMP
	SET GroupLabel='Fall Herbicides'
	WHERE MarketLetterCode='ML2020_W_CER_PUL_SB' AND GroupLabel <> 'Spring Herbicides'
	
	-- Add column and values for table row groups to temp table	
	DECLARE @ML_BRANDS TABLE (
		MarketLetterCode VARCHAR(50) NOT NULL,
		RowLabel VARCHAR(200) NOT NULL,
		Brand VARCHAR(200) NOT NULL,
		DisplaySequence  INT NOT NULL
	)
	   
	INSERT INTO @ML_BRANDS(MarketLetterCode, RowLabel, Brand, DisplaySequence)
	VALUES('ML2020_W_CCP','Facet L, Lance, Lance AG, Cotegra','FACET L',1)
	,('ML2020_W_CCP','Facet L, Lance, Lance AG, Cotegra','LANCE',1)
	,('ML2020_W_CCP','Facet L, Lance, Lance AG, Cotegra','LANCE AG',1)
	,('ML2020_W_CCP','Facet L, Lance, Lance AG, Cotegra','COTEGRA',1)
	,('ML2020_W_CCP',IIF(@Level5Code='D000001', 'Liberty 150', 'Liberty Brands'),'LIBERTY',2)
	,('ML2020_W_CCP','Centurion','CENTURION',3)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','INSURE CEREAL',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','INSURE CEREAL FX4',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','INSURE PULSE',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR DUO SCG',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR GRANULAR',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR LQ',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR N/T',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR PRO',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR SCG',1)
	,('ML2020_W_ST_INC','Insure Cereal, Insure Pulse & Nodulator Brands','NODULATOR XL',1)
	,('ML2020_W_CER_PUL_SB','Spring Herbicides, Insecticides & Fungicides','Spring Herbicides',1) -- NOT IN (DISTINCT, IGNITE, HEAT/LQ (PRE-HARVEST))
	,('ML2020_W_CER_PUL_SB','Fall Herbicides','Fall Herbicides',2) --  IN (DISTINCT, IGNITE, HEAT/LQ (PRE-HARVEST))

	IF @Level5Code = 'D0000117'
		DELETE FROM @ML_BRANDS WHERE BRAND='CENTURION'
	
	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary';
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"></span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
					</div>
				</div>';
	
				GOTO FETCH_NEXT
			END 			
					
			SET @ML_THEAD = '
			<tr>
				<th>Market Letter Brand</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' POG Plan $</th>
				<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Eligible POG $</th>									
				<th>Planned Growth %</th>
				<th>Planned Growth Reward %</th>
				<th>' + IIF(@Level5Code='D0000117', 'Preliminary Plan<br/>POG Reward', 'Early Order ' + IIF(@MARKETLETTERCODE = 'ML2020_W_ST_INC', '75', '60'))  + ' %</th>
				<th>' + IIF(@Level5Code='D0000117', 'Final Plan<br/>POG Reward %','Take Order %') + '</th>
				<th>Reward %</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th>Reward $</th>
			</tr>'
			
			-- Set rows to be displayed in each market letter table
			IF @Level5Code <> 'D0000117' -- If retailer is not West FCL / Coop
			BEGIN
				SET @ML_GrowthSummary_TBody = CONVERT(NVARCHAR(MAX),(SELECT

							(SELECT RowLabel  as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_CY,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_LY,'$') as 'td' for xml path(''), type)				
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(IIF(Growth_Percentage > 9.9999,9.9999,Growth_Percentage),'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT 'empty-cell-grey' AS [td/@class], '' AS 'td' FOR XML PATH(''), TYPE), NULL))
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(Growth_Reward_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT 'empty-cell-grey' AS [td/@class], '' AS 'td' FOR XML PATH(''), TYPE), NULL))							
						
						--	,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Early_Order_Percentage, '%') as 'td' for xml path(''), type)
							,IIF(@Level5Code = 'D000001' AND @MARKETLETTERCODE = 'ML2020_W_CCP' AND RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION'), 
									(SELECT 'empty-cell-grey' AS [td/@class], '' AS 'td' FOR XML PATH(''), TYPE), 
									(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Early_Order_Percentage, '%') as 'td' for xml path(''), type)
								)						
						

							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Take_Delivery_Percentage, '%') as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Total_Reward_Percentage, '%')  as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Eligible_POG,'$')  as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,'$')  as 'td' for xml path(''), type)
						FROM (
								SELECT * 								
									,CASE 
										WHEN RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION') THEN 0
										WHEN TotalQualSales_CY > 0 AND TotalQualSales_LY <= 0 THEN 999.99/100
										WHEN TotalQualSales_LY > 0 THEN TotalQualSales_CY/TotalQualSales_LY 
									END	AS Growth_Percentage														
								FROM (
									SELECT *
										,Growth_Reward_Percentage + Early_Order_Percentage + Take_Delivery_Percentage AS Total_Reward_Percentage
										,SUM(IIF(RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION'), 0, Growth_QualSales_CY)) OVER() AS TotalQualSales_CY
										,SUM(IIF(RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION'), 0, Growth_QualSales_LY)) OVER() AS TotalQualSales_LY
									FROM (
										SELECT MLB.RowLabel, MLB.DisplaySequence
											,SUM(ISNULL(Growth_QualSales_CY,0)) AS Growth_QualSales_CY
											,SUM(ISNULL(Growth_QualSales_LY,0)) AS Growth_QualSales_LY
											,SUM(ISNULL(Growth_Reward_Sales,0)) AS Growth_Reward_Sales
											,MAX(ISNULL(Growth_Reward_Percentage,0)) AS Growth_Reward_Percentage
											,MAX(ISNULL(Early_Order_Percentage,0)) AS Early_Order_Percentage
											,MAX(ISNULL(Take_Delivery_Percentage,0)) AS Take_Delivery_Percentage
											,MAX(ISNULL(Lib_Reward_Sales,0)) AS Lib_Reward_Sales
											,MAX(ISNULL(Cent_Reward_Sales,0)) AS Cent_Reward_Sales
											,SUM(ISNULL(Growth_Reward_Sales,0) + ISNULL(Lib_Reward_Sales,0) + ISNULL(Cent_Reward_Sales,0)) AS Eligible_POG
											,SUM(ISNULL(Reward,0)) AS Reward
											,COUNT(*) OVER() AS [RowCount]
										FROM @ML_BRANDS MLB
											LEFT JOIN #Temp T1
											ON T1.MarketLetterCode=MLB.MarketLetterCode AND T1.GroupLabel=MLB.Brand
										WHERE MLB.MARKETLETTERCODE = @MARKETLETTERCODE
										GROUP BY MLB.RowLabel, MLB.DisplaySequence
									) D0
								) D1
						) D 
						ORDER BY DisplaySequence
						FOR XML PATH('tr'), ELEMENTS, TYPE))
			END
			ELSE
			BEGIN
				SET @ML_GrowthSummary_TBody = CONVERT(NVARCHAR(MAX),(SELECT
							(SELECT RowLabel  as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_CY,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_LY,'$') as 'td' for xml path(''), type)		
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(IIF(Growth_Percentage > 9.9999,9.9999,Growth_Percentage),'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT 'empty-cell-grey' AS [td/@class], '' AS 'td' FOR XML PATH(''), TYPE), NULL))
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(Growth_Reward_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT 'empty-cell-grey' AS [td/@class], '' AS 'td' FOR XML PATH(''), TYPE), NULL))
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(Early_Order_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT IIF(RowLabel='Centurion', 'center_align empty-cell-grey','right_align') AS [td/@class], IIF(RowLabel='Centurion', '', dbo.SVF_Commify(Early_Order_Percentage,'%')) AS 'td' FOR XML PATH(''), TYPE), NULL))
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(Take_Delivery_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT IIF(RowLabel='Centurion', 'center_align empty-cell-grey','right_align') AS [td/@class], IIF(RowLabel='Centurion', '', dbo.SVF_Commify(Take_Delivery_Percentage,'%')) AS 'td' FOR XML PATH(''), TYPE), NULL))
							,IIF(DisplaySequence = 1, (SELECT 'right_align' AS [td/@class], IIF(@MARKETLETTERCODE <> 'ML2020_W_CCP', [RowCount], NULL) AS [td/@rowspan], dbo.SVF_Commify(Total_Reward_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), IIF(@MARKETLETTERCODE = 'ML2020_W_CCP', (SELECT 'right_align' AS [td/@class], dbo.SVF_Commify(Total_Reward_Percentage,'%') AS 'td' FOR XML PATH(''), TYPE), NULL))
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Eligible_POG,'$')  as 'td' for xml path(''), type)
							,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,'$')  as 'td' for xml path(''), type)
						FROM (
								SELECT * 								
									,CASE 
										WHEN RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION') THEN 0
										WHEN TotalQualSales_CY > 0 AND TotalQualSales_LY <= 0 THEN 999.99/100
										WHEN TotalQualSales_LY > 0 THEN TotalQualSales_CY/TotalQualSales_LY 
									END	AS Growth_Percentage														
								FROM (
									SELECT *
										,Growth_Reward_Percentage + Early_Order_Percentage + Take_Delivery_Percentage AS Total_Reward_Percentage
										,SUM(IIF(RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION'), 0, Growth_QualSales_CY)) OVER() AS TotalQualSales_CY
										,SUM(IIF(RowLabel IN ('LIBERTY 150','Liberty Brands','CENTURION'), 0, Growth_QualSales_LY)) OVER() AS TotalQualSales_LY
									FROM (
										SELECT MLB.RowLabel, MLB.DisplaySequence
											,SUM(ISNULL(Growth_QualSales_CY,0)) AS Growth_QualSales_CY
											,SUM(ISNULL(Growth_QualSales_LY,0)) AS Growth_QualSales_LY
											,SUM(ISNULL(Growth_Reward_Sales,0)) AS Growth_Reward_Sales
											,MAX(ISNULL(Growth_Reward_Percentage,0)) AS Growth_Reward_Percentage
											,MAX(ISNULL(Early_Order_Percentage,0)) AS Early_Order_Percentage
											,MAX(ISNULL(Take_Delivery_Percentage,0)) AS Take_Delivery_Percentage
											,MAX(ISNULL(Lib_Reward_Sales,0)) AS 	Lib_Reward_Sales
											,MAX(ISNULL(Cent_Reward_Sales,0)) AS 	Cent_Reward_Sales
											,SUM(ISNULL(Growth_Reward_Sales,0) + ISNULL(Lib_Reward_Sales,0) + ISNULL(Cent_Reward_Sales,0)) AS Eligible_POG
											,SUM(ISNULL(Reward,0)) AS Reward
											,COUNT(*) OVER() AS [RowCount]
										FROM @ML_BRANDS MLB
											LEFT JOIN #Temp T1
											ON T1.MarketLetterCode=MLB.MarketLetterCode AND T1.GroupLabel=MLB.Brand
										WHERE MLB.MARKETLETTERCODE = @MARKETLETTERCODE
										GROUP BY MLB.RowLabel, MLB.DisplaySequence
									) D0
								) D1
						) D 
						ORDER BY DisplaySequence
						FOR XML PATH('tr'), ELEMENTS, TYPE))
			END

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' +  @ML_THEAD + @ML_GrowthSummary_TBody + '
					</table>
				</div>
			</div>' 


	FETCH_NEXT:	
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
			END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP



 /* ****************************** MATRIX TABLE ******************************** */
	SET @SectionHeader = 'REWARD PERCENTAGES'

	IF  @Level5Code='D000001'
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class = "st_header">
				<span>' + @sectionHeader + '</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
						<tr><th>Reward Opportunity</th><th>Criteria</th><th>Reward on Reward Brands</th></tr>
						<tr><td rowspan="3"><b>Planned Growth:</b><br/><br/>% 2020 POG Plan $ relative to 2019 POG $<br/>(in terms of 2020 invoice price)</td><td style="text-align: center; font-weight: bold">90% to 100%</td><td style="text-align: center; font-weight: bold">1%</td></tr>
						<tr><td style="text-align: center; font-weight: bold">100% to 109.9%</td><td style="text-align: center; font-weight: bold">2%</td></tr>
						<tr><td style="text-align: center; font-weight: bold">&GreaterEqual; 110%</td><td style="text-align: center; font-weight: bold">3%</td></tr>
						<tr><td><b>Early Order Plan (60%) by:</b><br/><br/>February 14th/20 of Cotegra, Lance, Lance AG & Facet L<br/>February 14th/20  of Spring Herbicide, Insecticide & Fungicide Brands<br/>June 15th/20 of Fall Herbicides Brands (Distinct, Ignite & Heat Pre-Harvest)</td><td style="text-align: center; font-weight: bold">Yes</td><td colspan="3" style="text-align: center; font-weight: bold">1%</td></tr>
						<tr><td><b>Early Order Plan (75%) by:</b><br/><br/>December 21st/19 of Insure Cereal, Insure Pulse & Nodulator Brands</td><td style="text-align: center; font-weight: bold">Yes</td><td colspan="3" style="text-align: center; font-weight: bold">1%</td></tr>
						<tr><td><b>Take 25% delivery of the Customer Order Plan by:</b><br/><br/>February 28th/20 of Liberty 150</td><td style="text-align: center; font-weight: bold">Yes</td><td colspan="3" style="text-align: center; font-weight: bold">0.5%</td></tr>
						<tr><td><b>Take 75% delivery of the Customer Order Plan by:</b><br/><br/>March 30th/20 of Insure Cereal, Insure Pulse & Nodulator Brands<br/>April 30th/20 of Facet L, Liberty 150 & Centurion<br/>May 29th/20 of Spring Herbicides, Insecticides & Fungicides<br/>May 30th/20 of Cotegra, Lance & Lance AG</td><td style="text-align: center; font-weight: bold">Yes</td><td colspan="3" style="text-align: center; font-weight: bold">0.5%<br/>1% (Liberty Only)</td></tr>
						<tr><td><b>Take 95% delivery of the Customer Order Plan by:</b><br/><br/>May 15th/20 of Insure Pulse, Insure Cereal & Nodulator Brands<br/>June 29th/20 of Cotegra, Facet L, Lance, Lance AG, Liberty & Centurion<br/>June 30th/20 of Spring Herbicides, Insecticides & Fungicides<br/>September 11th/20 of Fall Herbicides (Distinct, Ignite & Heat Pre-Harvest)</td><td style="text-align: center; font-weight: bold">Yes</td><td colspan="3" style="text-align: center; font-weight: bold">0.5%</td></tr>
				</table>
			</div>
		</div>'

	ELSE IF @Level5Code='D0000117' -- If retailer is West FCL / Coop
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class = "st_header">
				<span>' + @sectionHeader + '</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
						<tr><th>Reward Opportunity</th><th>Criteria</th><th>Reward on Reward Brands</th></tr>
						<tr><td rowspan="3"><b>Planned Growth:</b><br/>% 2020 POG Plan $ relative to 2019 POG $<br/>(in terms of 2020 invoice price)</td><td style="text-align: center; font-weight: bold">90% to 100%</td><td style="text-align: center; font-weight: bold">1%</td>							</tr>
						<tr><td style="text-align: center; font-weight: bold">100% to 109.9%</td><td style="text-align: center; font-weight: bold">2%</td></tr>
						<tr><td style="text-align: center; font-weight: bold">&GreaterEqual; 110%</td><td style="text-align: center; font-weight: bold">3%</td></tr>
						<tr><td><b>Submit Preliminary POG Plan by:</b><br/>October 12/19 of Seed Treatment & Inoculant Brands<br/>December 2/19 of Canola Crop Protection Brands<br/>December 2/19 of Cereal Pulse and Soybean Brands</td><td style="text-align: center;">Yes</td><td style="text-align: center;">0.5%</td>							</tr>
						<tr><td><b>Submit Preliminary POG plan by:</b><br/>December 2/19 of Canola Crop Protection Liberty Brands</td><td style="text-align: center;">Yes</td><td style="text-align: center;">0.75%</td></tr>
						<tr><td><b>Submit Final POG Plan by:</b><br/>December 2/19 of Seed Treatment and Inoculant Brands<br/>January 31/20 of Canola Crop Protection Brands<br/>January 31/20 Spring Herbicides, Insecticides & Fungicide Brands<br/>June 1/20 of Fall Herbicide Brands</td><td style="text-align: center;">Yes</td><td style="text-align: center;">0.5%</td></tr>
						<tr><td><b>Submit Final POG Plan by:</b><br/>January 31/20 of Canola Crop Protection Liberty Brands</td><td style="text-align: center;">Yes</td><td style="text-align: center;">1%</td>							</tr>
				 </table>
			</div>
		</div>'

	ELSE
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class = "st_header">
				<span>' + @sectionHeader + '</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
						<tr><th>Reward Opportunity</th><th>Criteria</th><th>Reward on Seed Treatment & Inoculant Reward Brands</th>
						<th>Reward on Canola Crop Protection<br/>-Cotegra, Lance, Lance AG and Facet L Reward Brands</th>
						<th>Reward on Cereal, Pulse & Soybean Reward Brands</th></tr>
						<tr><td rowspan="3"><b>Planned Growth:</b><br/>% 2020 POG Plan $ relative to 2019 POG $<br/>(in terms of 2020 invoice price)</td><td style="text-align: center; font-weight: bold">90% to 100%</td><td style="text-align: center; font-weight: bold">2%</td><td style="text-align: center; font-weight: bold">1%</td><td style="text-align: center; font-weight: bold">2%</td></tr>
						<tr><td style="text-align: center; font-weight: bold">100% to 109.9%</td><td style="text-align: center; font-weight: bold">3%</td><td style="text-align: center; font-weight: bold">2%</td><td style="text-align: center; font-weight: bold">3%</td></tr>
						<tr><td style="text-align: center; font-weight: bold">&GreaterEqual; 110%</td><td style="text-align: center; font-weight: bold">4%</td><td style="text-align: center; font-weight: bold">3%</td><td style="text-align: center; font-weight: bold">4%</td></tr>
						<tr><td><b>Early Order (60%) of Customer Order Plan by:</b><br/><br/>February 14/20 of Cotegra, Lance, Lance AG, Facet L, Liberty & Centurion<br/><br/>February 14/20 of Spring Herbicide, Insecticide & Fungicide Brands<br/></br>June 15/20 of Fall Herbicides</td><td style="text-align: center;"><b>Yes</b></td><td colspan = "3" style="text-align: center;"><b>1%<br/>0.5% (Liberty and Centurion Only)</b></td></tr>
						<tr><td><b>Early Order (75%) of Customer Order Plan by:</b><br/><br/>December 21/20 of Insure Cereal, Insure Pulse & Nodulator Brands</td><td style="text-align: center;"><b>Yes</b></td><td colspan = "3" style="text-align: center;"><b>1%</b></td></tr>
						<tr><td><b>Take 25% delivery of Customer Order Plan by:</b><br/><br/>February 28/20 of Liberty and Centurion</td><td style="text-align: center;"><b>Yes</b></td><td colspan = "3" style="text-align: center;"><b>1% (Liberty Only)</b><br/><b>0.5% (Centurion Only)</b></td></tr>
						<tr><td><b>Take 75% delivery of Customer Order Plan by:</b><br/><br/>March 30/20 of Insure Cereal, Insure Pulse & Nodulator Brands<br/><br/>April 30/20 of Cotegra, Lance, Lance AG, Facet L, Liberty & Centurion<br/><br/>May 29/20 of Spring Herbicides, Insecticides & Fungicides</td><td style="text-align: center;"><b>Yes</b></td><td colspan = "3" style="text-align: center;"><b>0.5%<br/>3% (Liberty Only)<br/>1% (Cotegra, Lance, Lance AG, Facet L & Centurion)</b></td></tr>
						<tr><td><b>Take 95% delivery of Customer Order Plan by:</b><br/><br/>May 15/20 of Insure Pulse, Insure Cereal & Nodulator Brands<br/><br/>June 29/20 of Cotegra, Facet L, Lance, Lance AG, Liberty & Centurion<br/><br/>June 30/ 20 of Spring Herbicides, Insecticides & Fungicides<br/><br/>September 11/20 of Fall Herbicides</td><td style="text-align: center;"><b>Yes</b></td><td colspan = "3" style="text-align: center;"><b>0.5%<br/>1% (Cotegra, Lance, Lance AG, Facet L & Liberty)</b></td></tr>	 
				 </table>
			</div>
		</div>'

	IF @RETAILERCODE in ('D0000137', 'D0000107', 'D0000244')  -- All West Lines Companies Except Nutrien (RP - 3239)
		SET @DisclaimerText = '
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> = Centurion, Cotegra, Facet L, Lance, Lance AG, Liberty Brands.</div>
		<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zampro & Zidua SC.</div>
		<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
		<div class="rprew_subtabletext">*Centurion & Liberty are not included in the Growth Calculation component.</div>'
	ELSE IF @RETAILERCODE = 'D520062427'		--Nutrien
		SET @DisclaimerText = '
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> = Centurion, Cotegra, Facet L, Lance, Lance AG, Liberty Brands.</div>
		<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Altitude FX, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zampro & Zidua SC.</div>
		<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
		<div class="rprew_subtabletext">*Centurion & Liberty are not included in the Growth Calculation component.</div>'
	ELSE IF @Level5Code='D0000117' -- If retailer is West FCL / Coop
		SET @DisclaimerText = '
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying Brands</b> = Cotegra, Facet L, Lance & Lance AG.</div>
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Reward Brands</b> = Cotegra, Facet L, Lance, Lance AG, Liberty Brands.</div>
		<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zidua SC & Zampro.</div>
		<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator brands.</div>
		<div class="rprew_subtabletext">*Titan calculated in Suggested Retail Price (SRP)</div>'
	ELSE IF @Level5Code='D000001' -- Independent West
		SET @DisclaimerText = '
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying Brands</b> = Cotegra, Facet L, Lance & Lance AG.</div>
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Reward Brands</b> = Cotegra, Facet L, Lance, Lance AG, Liberty 150.</div>
		<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zidua SC & Zampro.</div>
		<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
		<div class="rprew_subtabletext">*Centurion & Liberty 150 are not included in the Growth Calculation component.</div>'
	ELSE
		SET @DisclaimerText = '
		<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Centurion, Cotegra, Facet L, Lance, Lance AG, Liberty 150.</div>
		<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zampro & Zidua SC.</div>
		<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator Duo, Nodulator Pro, Nodulator SCG, Nodulator XL</div>
		<div class="rprew_subtabletext">*Centurion & Liberty 150 are not included in the Growth Calculation component.</div>'

/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
			
END




