﻿


CREATE PROCEDURE [Reports].[RP2020_Get_RewardMargins_East] @Statementcode INT
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @TEMP TABLE ( 
		[StatementCode] [int] NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL,		
		[ProgramCode]	[varchar](50) NOT NULL,
		[ChemicalGroup] [varchar](50) NOT NULL,
		[RewardBrand_Sales] MONEY NOT NULL DEFAULT 0,
		[Rewarded_Sales] MONEY NOT NULL DEFAULT 0,		
		[Reward] MONEY NOT NULL DEFAULT 0,		
		[Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0,
		[Overall_Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0
	)

	
	
	DECLARE @INV_ML_CODE AS VARCHAR(50)='ML2020_E_INV'			--InVigor Hybrids
	DECLARE @INV_CCP_CODE AS VARCHAR(50)='ML2020_E_CCP'			--Canola Crop Protection

	INSERT @TEMP(Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales)
	SELECT tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup, SUM(tx.Price_CY) Price_CY
	FROM RP2020Web_Sales_Consolidated TX		
		INNER JOIN ProductReference PR		ON PR.Productcode=tx.ProductCode 
	WHERE tx.Statementcode=@Statementcode AND tx.GroupType IN ('REWARD_BRANDS', 'ELIGIBLE_BRANDS') AND tx.ProgramCode <> 'ELIGIBLE_SUMMARY'   -- Eligible brands can work here
	GROUP BY tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup
	
	UPDATE T1
	SET Rewarded_Sales=T2.Rewarded_Sales
		,Reward=T2.Reward
	FROM @TEMP T1
		INNER JOIN (
			
			--Programs
			/*
			[dbo].[RP2020Web_Rewards_E_CustomGroundApplication]
			[dbo].[RP2020Web_Rewards_E_PlanningTheBusiness]
			[dbo].[RP2020Web_Rewards_E_POGSalesBonus]
			[dbo].[RP2020Web_Rewards_E_PortfolioSupport]
			[dbo].[RP2020Web_Rewards_E_PriaxorHeadlineSupport]
			[dbo].[RP2020Web_Rewards_E_PursuitSupport]
			[dbo].[RP2020Web_Rewards_E_RowCropFungSupport]
			[dbo].[RP2020Web_Rewards_E_SollioGrowth]
			[dbo].[RP2020Web_Rewards_E_SupplySales]
			*/
					
					
			-- CUSTOM GROUUND APPLICATION
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM [RP2020Web_Rewards_E_CustomGroundApplication] 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0
			
		
			UNION ALL
			--Planning and Supporting the Business
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_PlanningTheBusiness 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0

			UNION ALL

			-- POG SALES BONUS
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_POGSalesBonus 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0	
			
			UNION ALL
			--Portfolio Support
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales,Reward AS Reward 
				FROM RP2020Web_Rewards_E_PortfolioSupport 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0	

			UNION ALL
		
			-------**** PRIAXOR HEADLINE REWARD SPLIT THEM*****--------
			--PRIAXOR
			SELECT Statementcode, MarketLetterCode, Programcode
				,'PRIAXOR' AS RewardBrand ,Priaxor_Sales AS Rewarded_Sales,Priaxor_Reward AS Reward 
				FROM RP2020Web_Rewards_E_PriaxorHeadlineSupport 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0		
			
			UNION ALL

			--HEADLINE
			SELECT Statementcode, MarketLetterCode, Programcode
				,'HEADLINE' AS RewardBrand ,Headline_Sales AS Rewarded_Sales,Headline_Reward AS Reward 
				FROM RP2020Web_Rewards_E_PriaxorHeadlineSupport 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0
			-------------------------------------------------------------------------
		
			UNION ALL
			-- PURSUIT SUPPORT REWARD   
			SELECT Statementcode, MarketLetterCode, Programcode
				,'PURSUIT' AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_PursuitSupport 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0		
				
			
			UNION ALL

			-- ROW CROP FUNGICIDE SUPPORT		
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM [RP2020Web_Rewards_E_RowCropFungSupport] 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0
			
		
			UNION ALL


			-- SALES SUPPORT LOYALTY BONUS
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_CY_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_SSLB 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0		
			
		
			UNION ALL

			--SOLLIO GROWTH 
			SELECT Statementcode, MarketLetterCode, Programcode
				, RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_SollioGrowth 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0		
			
			
			UNION ALL
			
			--SUPPY SALES 
			SELECT Statementcode, MarketLetterCode, Programcode
				,IIF(MarketLetterCode = @INV_ML_CODE,'INVIGOR',RewardBrand) AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_E_SupplySales 
			WHERE Statementcode=@STATEMENTCODE	--AND Reward > 0		
	) T2
	ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.Programcode=T1.Programcode AND T2.RewardBrand=T1.ChemicalGroup
	   	 
	UPDATE @TEMP SET [Reward_Percentage]=[Reward]/[Rewarded_Sales]  WHERE [Reward] > 0 AND  [Rewarded_Sales] > 0
	UPDATE @TEMP SET [Overall_Reward_Percentage]=[Reward]/[RewardBrand_Sales] WHERE [Reward] > 0  AND [RewardBrand_Sales] > 0 
	
	SELECT Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales, Rewarded_Sales, Reward, Reward_Percentage, [Overall_Reward_Percentage]
	FROM @TEMP

END
