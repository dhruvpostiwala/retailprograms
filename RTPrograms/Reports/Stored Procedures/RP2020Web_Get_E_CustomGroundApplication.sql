﻿CREATE PROCEDURE [Reports].[RP2020Web_Get_E_CustomGroundApplication] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE AS VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @DATASETCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @MARKETLETTERCODE AS VARCHAR(50) = ''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESSECTION AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTHEAD AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTBODY AS NVARCHAR(MAX) = '';

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode 
	FROM RP_Config_Programs 
	WHERE ProgramCode=@PROGRAMCODE;

	SELECT @SEASON=Season, @DATASETCODE=DataSetCode FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;


	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;
	
	-- Create temporary table that holds all eligible market letters
	IF NOT OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence]
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE);

	--Create a THEAD for the first table
	IF @LANGUAGE = 'F'
		SET @ML_THEAD = '
		<thead>
			<tr>
				<th>Produits</th>' +
				IIF(@DATASETCODE = 0, '<th>VAS ' + CAST(@SEASON AS VARCHAR(4)) + ' (acres)</th>', '') +
				'<th>Application à forfait ' + CAST(@SEASON AS VARCHAR(4)) + ' (acres)</th>
				<th>Récompense/acre</th>
				<th>Récompense ($)</th>
			</tr>
		</thead>
		';
	ELSE 
		SET @ML_THEAD = '
		<thead>
			<tr>
				<th>Products</th>' +
				IIF(@DATASETCODE = 0, '<th>' + CAST(@SEASON AS VARCHAR(4)) + ' POG Acres</th>', '') +
				'<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Custom App Acres</th>
				<th>Reward/Acre</th>
				<th>Reward $</th>
			</tr>
		</thead>
		';

	-- Loop through eligible market letters
	DECLARE ML_LOOP CURSOR FOR
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			-- Get the header for the current table being generated
			IF @LANGUAGE = 'F'
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Sommaires des récompenses';
			ELSE 
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary';

			-- If there are no transactions, generate HTML that states as such and fetch the next market letter
			IF NOT EXISTS(SELECT * FROM RP2020Web_Rewards_E_CustomGroundApplication WHERE MarketLetterCode=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE)-- AND Reward <> 0)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
										<div class = "st_header">
											<span>' + @SECTIONHEADER + '</span>
											<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
											<span class="st_right"></span>
										</div>
										<div class="st_content open">					
											<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
										</div>
									</div>';
	
				GOTO FETCH_NEXT;
			END 

			-- Use FOR XML PATH to generate HTML from the data from the table
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select CASE WHEN RewardBrand  = 'PROWL' THEN 'PROWL H2O' WHEN RewardBrand  = 'FRONTIER' THEN 'FRONTIER MAX' ELSE RewardBrand END AS 'td' for xml path(''), type)
					,IIF(@DATASETCODE = 0, (select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Acres,'') as 'td' for xml path(''), type), NULL)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_CustApp,'') as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Amount, '$') as 'td' for xml path(''), type)		
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$') as 'td' for xml path(''), type)
					FROM RP2020Web_Rewards_E_CustomGroundApplication
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE-- AND Reward <> 0
					ORDER BY RewardBrand
					FOR XML PATH('tr')	, ELEMENTS, TYPE));

			-- Use FOR XML PATH to generate HTML for the totals from the data from the table
			SET @ML_TBODY += CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select 'rp_bold' as [td/@class] ,'Total' as 'td' for xml path(''), type)
					,IIF(@DATASETCODE = 0, (select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify((SUM(RewardBrand_Acres)),'') as 'td' for xml path(''), type), NULL)
					,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify((SUM(RewardBrand_CustApp)),'') as 'td' for xml path(''), type)
					,(select 'rp_bold right_align' as [td/@class] ,'' as 'td' for xml path(''), type)		
					,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(SUM(Reward), '$') as 'td' for xml path(''), type)
					FROM RP2020Web_Rewards_E_CustomGroundApplication
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE-- AND Reward <> 0
					FOR XML PATH('tr')	, ELEMENTS, TYPE));

			--Generate the HTML for the section if there are transactions
			SET @ML_SECTIONS += '<div class="st_section">
									<div class = "st_header">
										<span>' + @SECTIONHEADER + '</span>
										<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
										<span class="st_right"></span>
									</div>
				

									<div class="st_content open">									
										<table class="rp_report_table">
										' + @ML_THEAD +   @ML_TBODY  + '
										</table>
									</div>
								</div>';
		FETCH_NEXT:	
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
		END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP

	-- Get application rates HTML
	IF @LANGUAGE = 'F'
		SET @APPLICATIONRATESTHEAD = 
		'
		<thead>
			<tr>
				<th>Produits</th>
				<th>Dose (acres/boîte)</th>
			</tr>
		</thead>
		';
	ELSE
		SET @APPLICATIONRATESTHEAD = 
		'
		<thead>
			<tr>
				<th>Products</th>
				<th>Acres/Case Application Rate</th>
			</tr>
		</thead>
		';

	SET @APPLICATIONRATESTBODY =
	'
	<tbody>
		<tr>
			<td>Cantus</td><td>160 Acres</td>
		</tr>
		<tr>
			<td>Caramba</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Conquest LQ</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Cotegra</td><td>70 Acres</td>
		</tr>
		<tr>
			<td>Engenia</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Frontier Max</td><td>60 Acres</td>
		</tr>
		<tr>
			<td>Headline AMP</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Integrity</td><td>40 Acres' + (CASE WHEN @LANGUAGE = 'F' THEN ' (dose max.)' ELSE ' (Full Rate)' END) + '</td>
		</tr>
		<tr>
			<td>Lance</td><td>25 Acres</td>
		</tr>
		<tr>
			<td>Optill</td><td>120 Acres</td>
		</tr>
		<tr>
			<td>Priaxor</td><td>160 Acres</td>
		</tr>
		<tr>
			<td>Prowl H20</td><td>20 Acres</td>
		</tr>
		<tr>
			<td>Sefina</td><td>80 Acres</td>
		</tr>
		<tr>
			<td>Twinline</td><td>80 Acres</td>
		</tr>
		<tr>
			<td>Zidua SC</td><td>80 Acres</td>
		</tr>
	</tbody>
	'

	SET @APPLICATIONRATESSECTION = 
	'
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + (CASE WHEN @LANGUAGE = 'F' THEN 'TAUX D''APPLICATION' ELSE 'APPLICATION RATES' END) + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"></span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				' + @APPLICATIONRATESTHEAD + '
				' + @APPLICATIONRATESTBODY + '
			</table>
		</div>
	</div>
	'

	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @APPLICATIONRATESSECTION;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json');
END
