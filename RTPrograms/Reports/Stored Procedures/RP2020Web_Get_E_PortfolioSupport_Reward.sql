﻿



CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_PortfolioSupport_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @DISCLAIMERTEXTFRENCH AS NVARCHAR(MAX) = '';
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @REWARD_PERC DECIMAL(2,2)=0;
	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD MONEY;
	DECLARE @TYAVG AS MONEY=0;
	DECLARE @SALES_ACTUAL AS MONEY=0;
	DECLARE @SALES_CY_E AS MONEY=0;
	DECLARE @IS_OU INT;

	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	--FETCH REWARD DISPLAY VALUES
	SELECT @TYAVG=SUM(ISNULL((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2,0))
		  ,@SALES_ACTUAL=SUM(ISNULL(QualBrand_Sales_CY,0))
		  ,@SALES_CY_E=SUM(ISNULL(RewardBrand_Sales,0))
		  ,@REWARD_PERC =MAX(ISNULL(Reward_Percentage,0)) 
		  ,@REWARD =SUM(ISNULL(Reward,0)) 
	From [dbo].[RP2020Web_Rewards_E_PortfolioSupport]
	Where STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;



	--FOR OU QUALIFYING AMOUNT
	IF(NOT(OBJECT_ID('tempdb..#TEMP_OU') IS NULL)) DROP TABLE #TEMP_OU 	
	Select SUM(QualBrand_Sales_CY) QualBrand_Sales_CY,SUM((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2) AS TYAvg
	INTO 
	#TEMP_OU
	FROM [dbo].[RP2020Web_Rewards_E_PortfolioSupport] WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT  @QUAL_PERCENTAGE = IIF(TYAvg > 0, QualBrand_Sales_CY / TYAvg, IIF(QualBrand_Sales_CY > 0, 9.9999, 0)) FROM #TEMP_OU
	
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);
	
	IF @IS_OU = 0 
	BEGIN
		IF @LANGUAGE = 'F'
			SET @HEADER = '<th> VAS – Moyenne 2 ans ($)</th> 
						<th> VAS réalisées ' + cast(@Season as varchar(4)) + ' ($)</th>
						<th>% admissible </th>
						<th>VAS admissibles de produits récompensés ' + cast(@Season as varchar(4)) + ' ($)</th>
						<th>Récompense (%)</th>
						<th>Récompense ($)</th>'
		ELSE
			SET @HEADER = '<th> 2 Year Average POG $</th> 
							<th> ' + cast(@Season as varchar(4)) + ' Actual POG $</th>
							<th>Qualifying % </th>
							<th>' + cast(@Season as varchar(4)) + ' Eligible POG Sales of Reward Brands $</th>
							<th>Reward%</th>
							<th>Reward $</th>'
		
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@TYAVG,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_ACTUAL,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		IF @LANGUAGE = 'F'
			SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
						<div class="rprew_subtabletext">Pour être admissibles à cette récompense, les détaillants doivent atteindre un total d''au moins 100 000 $ de VAS de produits BASF en 2020 et avoir présenté un plan de VAS au plus tard le 13 décembre 2019.</div>
				</div>
			</div>'
		ELSE 
			SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
							<div class="rprew_subtabletext">*Retails must acquire at least $100,000 in total actual POG $ in 2020 and submit a POG Plan by December 13, 2019 to be eligible for this reward.</div>
					</div>
				</div>'
	END
	ELSE
	BEGIN
		IF @LANGUAGE = 'F'
			SET @HEADER = '<th>VAS admissibles de produits récompensés ' + cast(@Season as varchar(4)) + ' ($)</th>
							<th>% admissible OU</th>
							<th>Récompense (%)</th>
							<th>Récompense ($)</th>'
		ELSE
			SET @HEADER = '<th>' + cast(@Season as varchar(4)) + ' Eligible POG Sales of Reward Brands $</th>
							<th>Qualifying OU % </th>
							<th>Reward%</th>
							<th>Reward $</th>'

		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUAL_PERCENTAGE,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERC,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))

		SET @DISCLAIMERTEXTFRENCH = '% Éligible basé sur les totaux des unités opérationnelles'

		SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
				<div class="rprew_subtabletext">*' + CASE WHEN @LANGUAGE = 'F' THEN @DISCLAIMERTEXTFRENCH ELSE 'Qualifying % based on Operating Unit Totals' END + '</div>
				</div>
			</div>'
	END

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 



	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + CASE WHEN @LANGUAGE = 'F' THEN 'POURCENTAGES DE RÉCOMPENSES' ELSE 'REWARD PERCENTAGES' END + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"></span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
			' + CASE WHEN @LANGUAGE = 'F' THEN '
				<tr>
					<th>VAS admissibles de produits récompensés 2020 ($) </th>
					<th>Récompense sur VAS 2020</th>
				</tr>'
				ELSE 
				'<tr>
					<th>2020 Eligible POG Sales of Reward Brands $ </th>
					<th>Reward on 2020 POG Sales</th>
				</tr>' END + '
				<tr>
					<td class="center_align">90 - 94.99%+ </td>
					<td class="center_align"> 3% </td>
				</tr>
				<tr>
					<td class="center_align">95 - 99.99%+ </td>
					<td class="center_align"> 4% </td>
				</tr>
				<tr>
					<td class="center_align">100%+ </td>
					<td class="center_align"> 6% </td>
				</tr>
				
			</table>
		</div>
	</div>' 

		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

