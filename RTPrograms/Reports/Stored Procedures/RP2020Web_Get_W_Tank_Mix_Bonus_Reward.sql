﻿


CREATE PROCEDURE [Reports].[RP2020Web_Get_W_Tank_Mix_Bonus_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
	

	SET NOCOUNT ON;


	--SET @HTML_Data = '<div>TESTING<div/>';
	--return;


	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @TOPFINEPRINT AS NVARCHAR(MAX);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @STATEMENT_LEVEL AS VARCHAR(20)='';
	DECLARE @L5CODE AS VARCHAR(10)='';
	DECLARE @REGION AS VARCHAR(4)='';
	DECLARE @RETAILERCODE_CARGILL VARCHAR(50);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	

	DECLARE @REWARDSUMMARYSECTION nvarchar(max)='';
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)=''
	DECLARE @DISCLAIMER_SECTION AS NVARCHAR(MAX)='';
	
	DECLARE @SECTION_HEADER AS NVARCHAR(MAX)='';
	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';

	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_SUBTOTAL_ROW  AS NVARCHAR(MAX)='';
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	DECLARE @ML_TOTAL_ROW  AS NVARCHAR(MAX)='';
	
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @PREV_RETAILERCODE VARCHAR(20)=''
	DECLARE @RETAILER_NAME NVARCHAR(MAX);
	DECLARE @RETAILER_CITY NVARCHAR(MAX);
	DECLARE @RET_SubTotal MONEY=0;

	DECLARE @FARMCODE VARCHAR(20);
	DECLARE @GROWER_NAME NVARCHAR(MAX);
	DECLARE @GROWER_CITY NVARCHAR(MAX);
	DECLARE @PIPEDAFarmStatus VARCHAR(3)='';

	DECLARE @NUMDOLLARSPERJUG_LIBCENT INTEGER = 4;
	DECLARE @NUMDOLLARSPERJUG_LIBCENTFACET INTEGER = 2;

	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @L5CODE =Level5Code, @STATEMENT_LEVEL=StatementLevel, @RETAILERCODE_CARGILL=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	

	SELECT @MARKETLETTERNAME=Title,
			@MARKETLETTERCODE = MarketLetterCode
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)


	IF(NOT(OBJECT_ID('tempdb..#DETAILS') IS NULL)) DROP TABLE #DETAILS
	SELECT RP.RetailerCode, RP.RetailerName as Retailer_Name, RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
		,T1.Farmcode 
		,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower_Name
		, GC.City + IIF(GC.Province='','',', ' + GC.Province) as Grower_City
		,IIF(GFI.PIPEDAFarmStatus='','No',GFI.PIPEDAFarmStatus) PIPEDAFarmStatus
		,Liberty_Acres,Centurion_Acres,Facet_L_Acres
		,Lib_Cent_Matched_Acres,Lib_Facet_Matched_Acres
		,Lib_Cent_Reward,Lib_Facet_Reward,Reward		
	INTO #DETAILS
	FROM RP2020Web_Rewards_W_TankMixBonus T1
		LEFT JOIN RetailerProfile RP			ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode=T1.Farmcode
	WHERE T1.Statementcode=@STATEMENTCODE
	ORDER BY Retailer_Name, Retailer_City, Grower_Name, Grower_City


	-- Set the fine print at the top of the report
	SET @TOPFINEPRINT = (
		SELECT '
		<div class="rprew_subtabletext rp_bold">TOTAL REWARD = ' + dbo.SVF_Commify(TotalReward, '$') + '</div>
		<div class="rprew_subtabletext">' + FORMAT(LibCentQty, 'N0') + ' Jugs * ' + dbo.SVF_Commify(@NUMDOLLARSPERJUG_LIBCENT, '$') + '/Jug = ' + dbo.SVF_Commify(LibCentReward, '$') + ' (Liberty & Centurion Reward)</div>
		<div class="rprew_subtabletext">' + FORMAT(LibCentFacetQty, 'N0') + ' Jugs * ' + dbo.SVF_Commify(@NUMDOLLARSPERJUG_LIBCENTFACET, '$') + '/Jug = ' + dbo.SVF_Commify(LibCentFacetReward, '$') + ' (Liberty, Centurion & Facet L Reward)</div>
		'
		FROM (
			SELECT SUM(Reward) [TotalReward], (SUM(Lib_Cent_Reward) / @NUMDOLLARSPERJUG_LIBCENT) [LibCentQty], SUM(Lib_Cent_Reward) [LibCentReward], (SUM(Lib_Facet_Reward) / @NUMDOLLARSPERJUG_LIBCENTFACET) [LibCentFacetQty], SUM(Lib_Facet_Reward) [LibCentFacetReward]
			FROM #DETAILS
		) [TopFinePrint_Data]
	)

--	RetailerCode	FarmCode	
	IF NOT EXISTS(SELECT * FROM #DETAILS)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + ' - REWARD SUMMARY</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"></span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 
		GOTO FINAL
	END 

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>' + @SEASON_STRING + ' Eligible<br/>Liberty Acres</th>
			<th>' + @SEASON_STRING + ' Eligible<br/>Centurion Acres</th>
			<th>' + @SEASON_STRING + ' Eligible<br/>Facet L Acres</th>
			<th>Matched Liberty<br/>&<br/>Centurion Jug Equivalents<br/>($' + CAST(@NUMDOLLARSPERJUG_LIBCENT AS VARCHAR(10)) + ' / Jug)</th>
			<th>Matched Liberty<br/>&<br/>Centurion<br/>&<br/>Facet L Jug Equivalents<br/>($' + CAST(@NUMDOLLARSPERJUG_LIBCENTFACET AS VARCHAR(10)) + ' / Jug)</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'


	/* REWARD DETAILS SECTIONS */
	/*  DONT GROUP BY RETAILER RP-2458
	DECLARE RETAILER_LOOP CURSOR FOR 
		SELECT DISTINCT  Retailercode, Retailer_Name, Retailer_City FROM #DETAILS ORDER BY [Retailer_Name], [Retailer_City]
	OPEN RETAILER_LOOP
	FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
	WHILE(@@FETCH_STATUS=0)
		BEGIN			
		/*
			IF @STATEMENT_LEVEL='LEVEL 2'
			BEGIN
				SELECT @RET_SubTotal=SUM(Reward) FROM #Details WHERE RetailerCode=@RETAILERCODE
				SET @ML_TBODY_ROWS += '<tr class="sub_total_row"><td colspan=7>' + @RETAILER_NAME + ' - ' + @RETAILER_CITY + '</td><td class="right_align">' + dbo.SVF_Commify(@RET_SubTotal,'$') + '</td></tr>'
			END
		*/
			--RP-2372
			SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ) AS 'td' for xml path(''), type)
					,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Liberty_Acres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Centurion_Acres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Facet_L_Acres, '')  as 'td' for xml path(''), type)
					--,(select 'right_align' as [td/@class] ,CASE WHEN @RetailerCode IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') THEN FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0') ELSE dbo.SVF_Commify(Lib_Cent_Reward, '$') END as 'td' for xml path(''), type)
					--,(select 'right_align' as [td/@class] ,CASE WHEN @RetailerCode IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') THEN FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0') ELSE dbo.SVF_Commify(Lib_Facet_Reward, '$') END as 'td' for xml path('')
					,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0')  as 'td' for xml path(''), type)												
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM #DETAILS
				WHERE RetailerCode=@RETAILERCODE
				FOR XML PATH('tr')	, ELEMENTS, TYPE))

		FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CITY
		END
	CLOSE RETAILER_LOOP
	DEALLOCATE RETAILER_LOOP	

	DECLARE GROWER_LOOP CURSOR FOR 
		SELECT DISTINCT FarmCode FROM #DETAILS ORDER BY FarmCode
	OPEN GROWER_LOOP
	FETCH NEXT FROM GROWER_LOOP INTO @FARMCODE
	WHILE(@@FETCH_STATUS=0)
		BEGIN			
		
			--RP-2372
			SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ) AS 'td' for xml path(''), type)
					,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Liberty_Acres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Centurion_Acres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Facet_L_Acres, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0')  as 'td' for xml path(''), type)												
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM #DETAILS
				WHERE FarmCode=@FARMCODE
				FOR XML PATH('tr')	, ELEMENTS, TYPE))

		FETCH NEXT FROM GROWER_LOOP INTO @FARMCODE
		END
	CLOSE GROWER_LOOP
	DEALLOCATE GROWER_LOOP

*/

		SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
			(select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ) AS 'td' for xml path(''), type)
			,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Liberty_Acres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Centurion_Acres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Facet_L_Acres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0')  as 'td' for xml path(''), type)												
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
		FROM #DETAILS
		ORDER BY PIPEDAFarmStatus DESC, Grower_Name
		FOR XML PATH('tr')	, ELEMENTS, TYPE))


	-- TOTAL ROW
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
				,(select 5 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0') as 'td' for xml path(''), type)							
				,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0') as 'td' for xml path(''), type)							
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
			FROM (
				SELECT SUM(Lib_Cent_Reward) AS Lib_Cent_Reward	,SUM(Lib_Facet_Reward) AS Lib_Facet_Reward		,SUM(Reward) AS Reward
				FROM #DETAILS
			)  D				
			FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @ML_SECTIONS='<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + ' - Rewards Summary</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"></span>
					</div>
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 
/*
						<div class="rprew_subtabletext">REWARD (A): Matched Liberty & Centurion Jug Equivalents ($4 * Jug)</div>
						<div class="rprew_subtabletext">REWARD (B): Matched Liberty, Centurion & Facet L Jug Equivalents ($2 * Jug)</div>

*/
	


	/* ********************* FINAL OUTPUT ************************************************ */

FINAL:
 SET @SECTION_HEADER = 'REWARD PERCENTAGES'
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + @SECTION_HEADER + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"></span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				<tr><th>Product</th><th>Area Conversion</th></tr>
				<tr><td style="text-align: center">Liberty</td><td style="text-align: center">10 Acres/13.5 L Jug</td></tr>
				<tr><td style="text-align: center">Facet L</td><td style="text-align: center">80 Acres/Jug</td></tr>
				<tr><td style="text-align: center">Centurion</td><td style="text-align: center">60 Acres/Jug</td></tr>
			</table>
		</div>
	</div>' 

	
	SET @DISCLAIMER_SECTION='
	<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying Brands</b> =  Liberty 150, Centurion, Facet L</div>
			<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Reward Brands</b> = Liberty 150</div>
		</div>
	</div>
	'


SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
		' + ISNULL(@TOPFINEPRINT,'') + '
	</div>'
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMER_SECTION
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	
	
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	

END



