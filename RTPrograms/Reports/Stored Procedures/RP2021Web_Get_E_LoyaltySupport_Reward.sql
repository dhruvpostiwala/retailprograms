﻿

CREATE PROCEDURE [Reports].[RP2021Web_Get_E_LoyaltySupport_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	/*
	 PLEASE NOTE LOYALTY SUPPORT REWARD IS USING RP2021_E_PURSUIT_REWARD AS PROGRAM CODE AND PURS_SUP_E
	*/


	SET NOCOUNT ON;
	
    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @STATEMENTLEVEL AS VARCHAR(10) = '';
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD2 AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY2 AS NVARCHAR(MAX) = '';
	DECLARE @REWARDCONDITIONTEXT AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_TBODY AS NVARCHAR(MAX) = '';

	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @TOTAL_REWARD MONEY=0;
	DECLARE @IS_OU INT;

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	DECLARE @DECIMAL_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F'  ELSE ' ' END

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE;
	SELECT @IS_OU = DatasetCode, @SEASON=Season, @STATEMENTLEVEL=StatementLevel FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT
	

	SELECT	@TOTAL_REWARD = SUM(Reward)
	FROM	[dbo].[RP2021Web_Rewards_E_PursuitSupport]
	WHERE	STATEMENTCODE=@STATEMENTCODE

	DECLARE @REWARD_PRODUCT_TABLE TABLE
	(
		RewardBrand VARCHAR(50),
		RewardPerCase MONEY
	)
	INSERT	INTO @REWARD_PRODUCT_TABLE VALUES 
	('CONQUEST LQ', 40),
	('FORUM', 105),
	('FRONTIER', 40),
	('HEADLINE', 108),
	('PRIAXOR', 160),
	('PURSUIT', 75)


	DROP TABLE if EXISTS #TEMP 

	SELECT	
			CASE
				WHEN T1.RewardBrand = 'FRONTIER' THEN 'FRONTIER MAX'
				ELSE T1.RewardBrand
			END as RewardBrand
			,ISNULL(RewardBrand_Qty, 0) AS RewardBrand_Qty
			,ISNULL(QualBrand_Sales_LY, 0) AS QualBrand_Sales_LY
			,ISNULL(QualBrand_Sales_CY, 0) AS QualBrand_Sales_CY
			,ISNULL(T2.RewardPerCase, T1.RewardPerCase) AS RewardPerCase
			,ISNULL(Reward, 0) AS Reward
			,ISNULL(Qual_percentage, 0) AS Qual_percentage
	INTO	#TEMP
	FROM	@REWARD_PRODUCT_TABLE T1
	LEFT	JOIN (
		SELECT	RewardBrand, RewardBrand_Qty ,QualBrand_Sales_LY, QualBrand_Sales_CY, RewardPErCase, Reward
				,IIF(QualBrand_Sales_LY > 0, QualBrand_Sales_CY / QualBrand_Sales_LY, IIF(QualBrand_Sales_CY > 0, 9.99, 0))  AS Qual_percentage
		FROM	[dbo].[RP2021Web_Rewards_E_PursuitSupport]
		WHERE	STATEMENTCODE=@STATEMENTCODE
	)T2
		ON T1.RewardBrand = T2.RewardBrand


	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
							<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 	
		GOTO FINAL
	END


	Declare @LYPOG_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+CAST(@SEASON-1 AS VARCHAR(4))+' ($)' ELSE  CAST(@SEASON-1 AS VARCHAR(4)) +  ' Eligible POG $' END; -- RP-4183
	Declare @CYPOG_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+CAST(@SEASON AS VARCHAR(4))+' ($)' ELSE  CAST(@SEASON AS VARCHAR(4)) +  ' Eligible POG $' END;
	Declare @QUALPERCENT_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN '% admissible' ELSE  CAST(@SEASON AS VARCHAR(4)) + IIF(@STATEMENTLEVEL = 'Level 1' and @IS_OU <> 0, 'OU Qualifying % ', 'Qualifying % ') END;
	Declare @CYPOGQUANT_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+CAST(@SEASON AS VARCHAR(4))+' (quantité)' ELSE  CAST(@SEASON AS VARCHAR(4)) +  ' Eligible POG QTY' END;


	SET @ML_THEAD1 = '
		<thead>
			<tr>
				<th>Products</th>
				<th>'+ @LYPOG_FrenchEnglish +'</th>
				<th>'+ @CYPOG_FrenchEnglish +'</th>
				<th>'+ @QUALPERCENT_FrenchEnglish +'</th>
				<th>'+ @CYPOGQUANT_FrenchEnglish +'</th>
				<th>Reward Per Case</th>
				<th>Reward $ </th>
			</tr>
		</thead>
		';
		


		

		-- Use FOR XML PATH to generate HTML from the data from the table
		SET @ML_TBODY1 = CONVERT(NVARCHAR(MAX),(SELECT
				(select RewardBrand as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(QualBrand_Sales_LY,0), @CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(QualBrand_Sales_CY,0), @CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(Qual_percentage,0), @PERCENT_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Qty,@DECIMAL_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardPerCase,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				FROM #TEMP
				FOR XML PATH('tr')	, ELEMENTS, TYPE));
		
		-- Use FOR XML PATH to generate HTML for the totals from the data from the table
		
		SET @ML_TBODY1 += CONVERT(NVARCHAR(MAX),(SELECT top 1	
					(select 'rp_bold' as [td/@class],'6' as [td/@colspan] ,'Total' as 'td' for xml path(''), type)
					,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(@TOTAL_REWARD,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				FROM #TEMP 
				FOR XML PATH('tr')	, ELEMENTS, TYPE));

	
		--Generate the HTML for the section if there are transactions
		SET @ML_SECTIONS += '<div class="st_section">
								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD1 +   @ML_TBODY1  + '
									</table>
								</div>
							</div>';
	

FINAL:	
		-- Get application rates HTML
		SET @APPLICATIONRATESTABLE_THEAD = 
		'
		<thead>
			<tr>
				<th>Products</th>
				<th>Reward Per Case</th>
			</tr>
		</thead>
		';


	SET @APPLICATIONRATESTABLE_TBODY =
	'
	<tbody>

		<tr>
			<td>Conquest LQ </td><td>'+dbo.SVF_Commify(40,@CURRENCY_FORMAT)+'</td>
		</tr>
		<tr>
			<td>Forum</td><td>'+dbo.SVF_Commify(105,@CURRENCY_FORMAT)+'</td>
		</tr>
		<tr>
			<td>Frontier Max </td><td>'+dbo.SVF_Commify(40,@CURRENCY_FORMAT)+'</td>
		</tr>
		<tr>
			<td>Headline</td><td>'+dbo.SVF_Commify(108,@CURRENCY_FORMAT)+'</td>
		</tr>
		<tr>
			<td>Priaxor</td><td>'+dbo.SVF_Commify(160,@CURRENCY_FORMAT)+'</td>
		</tr>
		<tr>
			<td>Pursuit</td><td>'+dbo.SVF_Commify(75,@CURRENCY_FORMAT)+'</td>
		</tr>
		
	</tbody>
	'

	SET @APPLICATIONRATESTABLE = 
	'
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				' + @APPLICATIONRATESTABLE_THEAD + '
				' + @APPLICATIONRATESTABLE_TBODY + '
			</table>
		</div>
	</div>
	'

	SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
					<strong>Reward Qualification Requirements:</strong> 2021 Eligible POG $ for a product must be equal to or greater than their 2020 Eligible POG $ for the same product to qualify for a reward.
				</div>
			</div>'

	IF @LANGUAGE = 'F' -- RP-4183
		BEGIN
			Set @ML_SECTIONS = REPLACE(@ML_SECTIONS,'Products','Produit');
			Set @ML_SECTIONS = REPLACE(@ML_SECTIONS,'Reward Per Case','Récompense par boîte');
			Set @ML_SECTIONS = REPLACE(@ML_SECTIONS,'Reward $ ','Récompense ($)');
			Set @APPLICATIONRATESTABLE = REPLACE(@APPLICATIONRATESTABLE,'Products','Produits');
			Set @APPLICATIONRATESTABLE = REPLACE(@APPLICATIONRATESTABLE,'Reward Per Case','Récompense par boîte');
			Set @REWARDCONDITIONTEXT = REPLACE(@REWARDCONDITIONTEXT,'Reward Qualification Requirements:','Conditions de qualification pour la récompense :');
			Set @REWARDCONDITIONTEXT = REPLACE(@REWARDCONDITIONTEXT,' 2021 Eligible POG $ for a product must be equal to or greater than their 2020 Eligible POG $ for the same product to qualify for a reward.',' La valeur des VAS admissibles en 2021 d''un produit doit être égale ou supérieure à celle des VAS admissibles en 2020 de ce même produit.');
		END


	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @APPLICATIONRATESTABLE;
	SET @HTML_Data += @REWARDCONDITIONTEXT;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'');
END
