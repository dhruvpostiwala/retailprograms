﻿


CREATE PROCEDURE [Reports].[RP2020Web_Get_W_NodDUO_SCG_Prog_Reward] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT = 2020;
	DECLARE @SEASON_LY INT = @SEASON-1;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  -- NOT USED IF NEEDED IN THE FUTURE CAN EASILY BE USED
	DECLARE @TABLE NVARCHAR(MAX);

	
	DECLARE @REWARD_PERC DECIMAL(2,2)=0;
	DECLARE @REWARD MONEY;
	DECLARE @SALES_LY AS MONEY=0;
	DECLARE @SALES_CY AS MONEY=0;
	DECLARE @GROWTH FLOAT; 

	DECLARE @NOD_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @NOD_TBODY_ROWS  AS NVARCHAR(MAX)='';
	DECLARE @NOD_TOTAL_ROW  AS NVARCHAR(MAX)='';
	DECLARE @NOD_SECTION  AS NVARCHAR(MAX)='';
	
	DECLARE @CONVERSION int = 15;

	SELECT @PROGRAMTITLE=Title, @REWARDCODE = RewardCode  From RP_Config_Programs Where ProgramCode=@ProgramCode

	--FETCH REWARD DISPLAY VALUES
	SELECT @REWARD=ISNULL(Reward,0)
		  ,@SALES_LY=ISNULL(Sales_LY,0)
		  ,@SALES_CY=ISNULL(RewardBrand_Sales,0)
		  ,@GROWTH=IIF(ISNULL(Sales_LY,0) = 0 AND ISNULL(RewardBrand_Sales,0) > 0,round(9.9999,4), ISNULL(Growth,0))
		  ,@REWARD_PERC =ISNULL(Reward_Perc,0)  --conversion
	From RP2020Web_Rewards_W_NodDUO_SCG_Prog
	Where STATEMENTCODE = @StatementCode

	/* ############################### MARKET LETTER SUMMARY TABLE ############################### */
	--FETCH MARKETLETTERHEADER
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	SELECT @TABLE = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@Sales_LY,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@Sales_CY,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@Growth,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@Reward_Perc,'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@Reward,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead><tr><th>' + cast(@Season_LY as varchar(4)) + ' Eligible Nodulator Duo SCG POG $</th><th>' + cast(@Season as varchar(4)) + ' Eligible Nodulator Duo SCG POG  $</th><th>Qualifying%</th><th>Reward%</th><th>Reward $</th>
								
				</tr>
				</thead>
						'+ @table + '
			</table>
		</div>
	</div>' 

	-----------------------------------------
	---PART B
	
	SET @NOD_THEAD_ROW = '<tr><thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>' + cast(@Season as varchar(4)) + ' Eligible Nodulator Duo SCG Bags</th>
			<th>Reward / Bag</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'
	
	DROP TABLE IF EXISTS #NEWGROWERS 
	SELECT
		T1.Farmcode
		,IIF(ISNULL(GC.FirstName + ' ' + GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' ' + GC.lastName) as Grower_Name
		, GC.City + IIF(GC.Province='','',', ' + GC.Province) as Grower_City
		,IIF(GFI.PIPEDAFarmStatus='','No',GFI.PIPEDAFarmStatus) PIPEDAFarmStatus
		,RewardBrand_Acres/ @CONVERSION as New_Nod_Bags
		,Reward_Perc
		,Reward		
	INTO #NEWGROWERS
	FROM RP2020Web_Rewards_W_NodDUO_SCG_Prog_NewGrowers T1
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode=T1.Farmcode
	WHERE T1.Statementcode = @STATEMENTCODE AND RewardBrand_Acres > 0


	SET @NOD_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		 (select ISNULL(IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ),'') AS 'td' for xml path(''), type)
		,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(New_Nod_Bags, '')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Perc, '$')  as 'td' for xml path(''), type)												
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #NEWGROWERS
	ORDER BY PIPEDAFarmStatus DESC, Grower_Name, Reward
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	IF EXISTS (SELECT * FROM #NEWGROWERS)
	BEGIN
		-- TOTAL ROW
		SET @NOD_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 2 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class],dbo.SVF_Commify(New_Nod_Bags, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class],dbo.SVF_Commify(Reward_Perc, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class],dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM (
					SELECT SUM(New_Nod_Bags) AS New_Nod_Bags,SUM(Reward) AS Reward,AVG(Reward_Perc) AS Reward_Perc
					FROM #NEWGROWERS
					WHERE Reward > 0
				)  NG				
				FOR XML PATH('tr')	, ELEMENTS, TYPE))

		SET @NOD_SECTION='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @NOD_THEAD_ROW + @NOD_TBODY_ROWS  + '</table>		
					</div>
				</div>'
	END
	ELSE SET @NOD_SECTION = ''
		
	/* ############################### FINAL OUTPUT ############################### */
	SET @DISCLAIMERTEXT='<b>*Qualifying & Reward Brands</b> = Nodulator Duo SCG'

	SET @HTML_DATA = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_DATA += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_DATA += @REWARDSUMMARYSECTION 		
	SET @HTML_DATA += @REWARDSUMMARYTABLE 	
	SET @HTML_DATA += @NOD_SECTION 	
	SET @HTML_DATA += @DISCLAIMERTEXT;
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END

