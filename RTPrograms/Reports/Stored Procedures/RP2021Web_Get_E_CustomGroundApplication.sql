﻿
CREATE PROCEDURE [Reports].[RP2021Web_Get_E_CustomGroundApplication] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE AS VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @DATASETCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
		
	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';

	DECLARE @ML_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESSECTION AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTHEAD AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTBODY AS NVARCHAR(MAX) = '';

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	DECLARE @DECIMAL_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F'  ELSE ' ' END

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode 
	FROM RP_Config_Programs 
	WHERE ProgramCode=@PROGRAMCODE;

	SELECT @SEASON=Season, @DATASETCODE=DataSetCode FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--GET DISPLAY VALUES
		
	-- GET THE LIST OF ALL PRODUCTS 
	DROP TABLE IF EXISTS #ALL_REWARD_PRODUCTS
	SELECT DISTINCT GroupLabel, ChemicalGroup AS RewardBrand 
	INTO #ALL_REWARD_PRODUCTS
	FROM RP_Config_Groups G 
	INNER JOIN RP_Config_Groups_Brands B ON G.GroupID = B.GroupID
	WHERE ProgramCode = @PROGRAMCODE AND G.GroupType = 'REWARD_BRANDS'


	-- GET DISPLAY VALUES FOR THE STATEMENT
	DROP TABLE IF EXISTS #CUSTOM_APP_DETAILS
	SELECT	T1.RewardBrand
			,T1.GroupLabel
			,SUM(RewardBrand_Acres) AS RewardBrand_Acres
			,SUM(Custom_App_Acres) AS RewardBrand_CustApp
			,SUM(Reward) AS Reward
	INTO #CUSTOM_APP_DETAILS
	FROM RP2021Web_Rewards_E_CustomGroundApplication T1
	WHERE T1.Statementcode=@STATEMENTCODE
	GROUP BY T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, T1.RewardBrand, T1.GroupLabel


	-- CREATE FINAL TABLE
	DROP TABLE IF EXISTS #DETAILS
	SELECT PR.RewardBrand
		   ,PR.GroupLabel
		   ,ISNULL(CA.RewardBrand_Acres, 0) AS RewardBrand_Acres
		   ,ISNULL(CA.RewardBrand_CustApp, 0) AS RewardBrand_CustApp
		   ,ISNULL(CA.Reward, 0) AS Reward
	INTO #DETAILS
	FROM #ALL_REWARD_PRODUCTS PR
	LEFT JOIN #CUSTOM_APP_DETAILS CA ON PR.RewardBrand = CA.RewardBrand AND PR.GroupLabel = CA.GroupLabel

	DECLARE @ProductHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Produits' ELSE 'Products' END; -- RP-4183
	DECLARE @EligiblePogHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+  CAST(@SEASON AS VARCHAR(4))+' (acres)' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG Acres' END; 
	DECLARE @CustomAppAcresHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Application à forfait '+ CAST(@SEASON AS VARCHAR(4)) +' (acres)' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Custom App Acres' END; 
	DECLARE @RewardPerAcreHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Récompense par acre' ELSE 'Reward Per Acre' END; 
	DECLARE @RewardHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Récompense ($)' ELSE 'Reward $' END; 


	SET @ML_THEAD = '
		<thead>
			<tr>
				<th>'+@ProductHeader_FrenchEnglish+'</th>' +
				'<th>'+@EligiblePogHeader_FrenchEnglish + '</th> 
				<th>'+@CustomAppAcresHeader_FrenchEnglish +'</th>
				<th>'+@RewardPerAcreHeader_FrenchEnglish +'</th>
				<th>'+@RewardHeader_FrenchEnglish+'</th>
			</tr>
		</thead>
		';



			-- If there are no transactions, generate HTML that states as such and fetch the next market letter
		IF NOT EXISTS(SELECT * FROM RP2021Web_Rewards_E_CustomGroundApplication WHERE STATEMENTCODE=@STATEMENTCODE)-- AND Reward <> 0)
			BEGIN
				SET @ML_SECTIONS = '<div class="st_section">
										<div class="st_content open">					
											<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
										</div>
									</div>';
			END 

			-- Use FOR XML PATH to generate HTML from the data from the table
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select CASE WHEN RewardBrand  = 'PROWL' THEN 'PROWL H2O' WHEN RewardBrand  = 'FRONTIER' THEN 'FRONTIER MAX' ELSE RewardBrand END AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Acres,@DECIMAL_FORMAT) as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_CustApp,@DECIMAL_FORMAT) as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(GroupLabel ='REWARD GROUP 2',0.50,1), @CURRENCY_FORMAT) as 'td' for xml path(''), type)		
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, @CURRENCY_FORMAT) as 'td' for xml path(''), type)
					FROM #DETAILS
					ORDER BY RewardBrand
					FOR XML PATH('tr')	, ELEMENTS, TYPE));

			/* 
			-- Use FOR XML PATH to generate HTML for the totals from the data from the table
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select 'rp_bold' as [td/@class] ,'Total' as 'td' for xml path(''), type)
					,IIF(@DATASETCODE = 0, (select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify((SUM(RewardBrand_Acres)),'') as 'td' for xml path(''), type), NULL)
					,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify((SUM(RewardBrand_CustApp)),'') as 'td' for xml path(''), type)
					,(select 'rp_bold right_align' as [td/@class] ,'' as 'td' for xml path(''), type)		
					,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(SUM(Reward), '$') as 'td' for xml path(''), type)
					FROM RP2020Web_Rewards_E_CustomGroundApplication
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE-- AND Reward <> 0
					FOR XML PATH('tr')	, ELEMENTS, TYPE));
			*/

			--Generate the HTML for the section if there are transactions
			SET @ML_SECTIONS += '<div class="st_section">
									<div class="st_content open">									
										<table class="rp_report_table">
										' + @ML_THEAD +   @ML_TBODY  + '
										</table>
									</div>
								</div>';


	-- Get application rates HTML
	IF @LANGUAGE = 'F'
		SET @APPLICATIONRATESTHEAD = 
		'
		<thead>
			<tr>
				<th>Produits</th>
				<th>Dose (acres/boîte)</th>
			</tr>
		</thead>
		';
	ELSE
		SET @APPLICATIONRATESTHEAD = 
		'
		<thead>
			<tr>
				<th>Products</th>
				<th>Acres Per Case</th>
			</tr>
		</thead>
		';


	Declare @Acres_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'acres' ELSE 'Acres' END; 
	SET @APPLICATIONRATESTBODY =
	'
	<tbody>
		<tr>
			<td>Armezon Pro</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		
		<tr>
			<td>Cantus</td><td>160 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Caramba</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Conquest LQ</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Cotegra</td><td>70 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Engenia</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Eragon LQ </td><td>80 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Frontier Max</td><td>60 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Headline AMP</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Integrity</td><td>40 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Liberty 200</td><td>20 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Optill</td><td>20 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Priaxor</td><td>160 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Prowl H20</td><td>20 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Sefina</td><td>80 '+@Acres_FrenchEnglish+'</td>
		</tr>
		<tr>
			<td>Zidua SC</td><td>80 '+@Acres_FrenchEnglish+'</td>
		</tr>
	</tbody>
	'

	SET @APPLICATIONRATESSECTION = 
	'
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				' + @APPLICATIONRATESTHEAD + '
				' + @APPLICATIONRATESTBODY + '
			</table>
		</div>
	</div>
	'

	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @APPLICATIONRATESSECTION;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'');
END
