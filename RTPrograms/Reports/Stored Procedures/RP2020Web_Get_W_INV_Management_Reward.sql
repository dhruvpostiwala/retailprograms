﻿
CREATE PROCEDURE [Reports].[RP2020Web_Get_W_INV_Management_Reward] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
	

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @RETAILERCODE VARCHAR(50);

	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';
	--DECLARE @HTML_Data as nvarchar(max);
	DECLARE @DisclaimerText as nvarchar(max);
	DECLARE @RewardSummarySection as NVARCHAR(max)='';

	DECLARE @SectionHeader as varchar(200);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';

	DECLARE @ML_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';

	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	IF(NOT(OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL)) DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)

	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP 
	SELECT MarketLetterCode,  SUM(RewardBrand_Sales) AS RewardBrand_Sales, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward
	INTO #TEMP
	FROM RP2020Web_Rewards_W_InventoryManagement 
	WHERE STATEMENTCODE=@STATEMENTCODE
	GROUP BY MarketLetterCode

	IF @RetailerCode IN ('D0000107', 'D0000137', 'D0000244', 'D520062427')
		SET @ML_THEAD='
			<tr>						
				<th>' + @SEASON_STRING + ' Eligible POG $</th>
				<th>Reward %</th>
				<th>Reward $</th>
			</tr>'
	ELSE
		SET @ML_THEAD='
			<tr>						
				<th>' + @SEASON_STRING + ' POG$ of Qualifying Brands</th>
				<th>Reward %</th>
				<th>Reward $</th>
			</tr>'


	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary'
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
					</div>
				</div>' 
	
				GOTO FETCH_NEXT
			END 
			
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD +   @ML_TBODY  + '
					</table>
				</div>
			</div>' 



	FETCH_NEXT:	
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
			END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP

/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

	IF @RetailerCode IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- If West Line
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> = Cotegra, Facet L, Lance, Lance AG, Liberty & Centurion</div>
				<div class="rprew_subtabletext"><b>*InVigor Market Letter Qualifying & Reward Brands</b> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
			</div>
		</div>'
	ELSE IF @RETAILERCODE = 'D0000137'
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> = Cotegra, Facet L, Lance, Lance AG, Liberty & Centurion</div>
				<div class="rprew_subtabletext"><span class="highlight_yellow"><b>*InVigor Market Letter Qualifying & Reward Brands</b> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</span></div>
			</div>
		</div>'
	ELSE
	----Removed 	<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div> on Line 154 - Shehrooz Khandaker
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG</div>
			
			</div>
		</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS	
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END
