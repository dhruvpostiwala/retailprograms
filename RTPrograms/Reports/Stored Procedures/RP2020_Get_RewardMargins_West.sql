﻿


CREATE PROCEDURE [Reports].[RP2020_Get_RewardMargins_West] @Statementcode INT
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @TEMP TABLE ( 
		[StatementCode] [int] NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL,		
		[ProgramCode]	[varchar](50) NOT NULL,
		[ChemicalGroup] [varchar](50) NOT NULL,
		[RewardBrand_Sales] MONEY NOT NULL DEFAULT 0,
		[Rewarded_Sales] MONEY NOT NULL DEFAULT 0,		
		[Reward] MONEY NOT NULL DEFAULT 0,		
		[Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0,
		[Overall_Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0
	)

	
	
	DECLARE @INV_ML_CODE AS VARCHAR(50)='ML2020_W_INV'			--InVigor Hybrids
	DECLARE @INV_CCP_CODE AS VARCHAR(50)='ML2020_W_CCP'			--Canola Crop Protection
	DECLARE @INV_CPS_CODE AS VARCHAR(50)='ML2020_W_CER_PUL_SB'	--Cereal, Pulse and Soybean Crop Protection
	DECLARE @INV_ST_CODE AS VARCHAR(50)='ML2020_W_ST_INC'		--Seed Treatment and Inoculants
	DECLARE @INV_LIB200_CODE AS VARCHAR(50)='ML2020_W_LIBERTY_200'	--Liberty 200
	


	--  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
	/*
		GROUPS WITH AND WITHOU REWARD BRANDS SETUP
		SELECT t1.Programcode, t2.programcode
		FROM RP_Config_Programs t1
			INNER JOIN (
				SELECT DISTINCT Programcode 
				FROM RP_Config_ML_Programs 
			)	MLP
			ON MLP.Programcode=T1.Programcode
			LEFT JOIN (
				Select Distinct Programcode From RP2020Web_Sales_Consolidated Where GroupType='REWARD_BRANDS' 
			) t2
			on t2.programcode=t1.programcode
		order by t1.programcode	
	
	*/


	--DELETE FROM RPWeb_RewardMargins WHERE Statementcode IN (SELECT Statementcode From @Statements)

	INSERT  @TEMP(Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales)
	SELECT tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup, SUM(tx.Price_CY) Price_CY
	FROM RP2020Web_Sales_Consolidated TX		
		INNER JOIN ProductReference PR		ON PR.Productcode=tx.ProductCode 
	WHERE tx.Statementcode=@Statementcode AND tx.GroupType='Reward_Brands'    --WHERE tx.ProgramCode='ELIGIBLE_SUMMARY' AND tx.GroupType='ELIGIBLE_BRANDS'
	GROUP BY tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup


	-- WEST TANK MIX
	INSERT  @TEMP(Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales, Rewarded_Sales, Reward)
	SELECT TNK.Statementcode, TNK.MarketLetterCode, TNK.Programcode	,'LIBERTY 150' AS ChemicalGroup ,TX.RewardBrand_Sales AS Rewarded_Sales, TX.RewardBrand_Sales AS Rewarded_Sales, TNK.Reward 
	FROM (
			SELECT StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Reward
			FROM RP2020Web_Rewards_W_TankMixBonus 
			WHERE StatementCode=@STATEMENTCODE AND Reward <> 0
			GROUP BY StatementCode, MarketLetterCode, ProgramCode
		) tnk
		INNER JOIN (
			SELECT MarketLetterCode, SUM(Price_CY) AS RewardBrand_Sales
			FROM RP2020Web_Sales_Consolidated
			WHERE Statementcode=@STATEMENTCODE AND ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS' AND GroupLabel LIKE 'Liberty%' 
			GROUP BY MarketLetterCode
		) TX
		ON TX.MarketLEtterCode=TNK.MarketLetterCode


	
	/*
	UPDATE T1
	SET RewardBrand_Sales=T2.Price_CY
	FROM @TEMP T1
		INNER JOIN (
			SELECT pr.ChemicalGroup,  SUM(tx.Price_CY) Price_CY
			FROM RPWeb2020_Sales_Consolidated tx
				INNER JOIN ProductReference PR		ON PR.Productcode=tx.ProductCode 
			WHERE tx.Statementcode=@Statementcode AND ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS'
			GROUP BY GroupLabel
			HAVING SUM(tx.Price_CY) > 0 
		) RewardBrands
		ON RewardBrands.ChemicalGroup=T1.ChemicalGroup
	*/
	
	UPDATE T1
	SET Rewarded_Sales=T2.Rewarded_Sales
		,Reward=T2.Reward
	FROM @TEMP T1
		INNER JOIN (
			-- WEST INVIGOR LIBERTY LOYALTY BONUS
			SELECT Statementcode, MarketLetterCode, Programcode
				,IIF(RewardBrand='Liberty','Liberty 150',RewardBrand) AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_INV_LLB 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0		
				
				UNION ALL

			-- WEST INVIGOR PERFORMANCE		
			SELECT Statementcode, MarketLetterCode, Programcode
				,IIF(MarketLetterCode=@INV_ML_CODE,'InVigor','Liberty 150') AS RewardBrand, RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_INV_PERF 
			WHERE Statementcode=@STATEMENTCODE AND Reward > 0

				UNION ALL

			-- WEST Efficiency Rebate 
			SELECT Statementcode, MarketLetterCode, Programcode
				,IIF(GroupLabel='Liberty','Liberty 150',GroupLabel) AS RewardBrand	,Support_Reward_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_EFF_REBATE 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST Payment ON Time
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_PaymentOnTime 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST BRAND SPECIFIC SUPPORT
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_BrandSpecificSupport 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- AG REWARDS BONUS 
			-- THIS REWARD IS CALCULATED AT RETAILERCODE + FARMCODE	COMBINATION
			SELECT Statementcode, MarketLetterCode, 'RP2020_W_AGREWARDS_BONUS' Programcode
				,GroupLabel  AS RewardBrand
				,SUM(SalesAmount) AS Rewarded_Sales
				,SUM(Reward) AS Reward
			FROM RP2020Web_Rewards_W_AgRewBonus_Sales_Actual
			WHERE Statementcode=@STATEMENTCODE	
			GROUP BY Statementcode, MarketLetterCode, GroupLabel 
			
				UNION ALL

			-- WEST CLEARFILED LENTILS SUPPORT
			-- THIS REWARD IS CALCULATED AT RETAILERCODE + FARMCODE	COMBINATION
			SELECT Statementcode, MarketLettercode, Programcode
				,RewardBrand
				,SUM(MatchedSales) AS Rewarded_Sales				
				,SUM(Reward) AS Reward
			FROM RP2020Web_Rewards_W_CLL_Herb_Actual 
			WHERE Statementcode=@statementcode
			GROUP BY Statementcode, MarketLettercode, Programcode, RewardBrand

				UNION ALL

			-- WEST CUSTOM SEED TREATMENT
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,Rewardable_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_CustomSeed 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST INVENTORY MANAGEMENT
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_InventoryManagement 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST Planing the business, 3 parts 
			-- PART 1 OF 3
			SELECT Statementcode, MarketLetterCode, Programcode
				,GroupLabel AS RewardBrand ,Growth_Reward_Sales AS Rewarded_Sales, Growth_Reward AS Reward 
			FROM RP2020Web_Rewards_W_PlanningTheBusiness 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0 AND GroupLabel NOT IN ('LIBERTY','LIBERTY 150','CENTURION')

				UNION ALL

			-- PART 2 OF 3
			SELECT Statementcode, MarketLetterCode, Programcode
				,'LIBERTY 150' AS RewardBrand ,Lib_Reward_Sales AS Rewarded_Sales, Lib_Reward AS Reward 
			FROM RP2020Web_Rewards_W_PlanningTheBusiness 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0 AND GroupLabel IN ('LIBERTY','LIBERTY 150')

				UNION ALL

			-- PART 3 OF 3
			SELECT Statementcode, MarketLetterCode, Programcode
				,GroupLabel AS RewardBrand ,Cent_Reward_Sales AS Rewarded_Sales, Cent_Reward AS Reward 
			FROM RP2020Web_Rewards_W_PlanningTheBusiness 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0 AND GroupLabel = 'CENTURION'

				UNION ALL

			-- WEST  PORTFOLIO ACHIEVEMENT
			SELECT Statementcode, MarketLetterCode, Programcode
				,GroupLabel AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_PortfolioAchievement 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST SOYBEAN PULS PLANNING
			SELECT Statementcode, MarketLetterCode, Programcode
				,GroupLabel AS RewardBrand ,PartA_Reward_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL

			-- WEST SOYBEAN PULS PLANNING
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_WarehousePayments 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0

				UNION ALL
			
			-- WEST CENTURION AFTER MARKET REWARD
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_Cent_AFM 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0	
			
				UNION ALL

			-- WEST LIBERTY AFTER MARKET REWARD
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_Lib_AFM 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0	

			
				UNION ALL
				
			-- WEST LIBERTY SALES FORECAST
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_Lib_SalesForecast 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL
				
			-- WEST LIBERTY SALES FORECAST EXECUTION
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2020Web_Rewards_W_Lib_SalesForecastExec 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0						   				
	) T2
	ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.Programcode=T1.Programcode AND T2.RewardBrand=T1.ChemicalGroup



/*			
	--MARKET LETTER
	
	
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_Cent_AFM]   -- Add marketlettercode and programcode
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_Lib_AFM]
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_Lib_SalesForecast]
	
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_TankMixBonus] -- $ value paid, not a reward percentage
	
	

	-- COOP 
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_RetailBonus]
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_TruckloadReward]
	

	-- ADDITIONAL PROGRAMS
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_NodDUO_SCG_Prog]
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_CustomAerial]
	SELECT TOP 1 * FROM	[dbo].[RP2020Web_Rewards_W_TitanCompProg]
*/

	--SELECT * FROM @TEMP WHERE Rewarded_Sales > 0
	--RETURN

	UPDATE @TEMP SET [Reward_Percentage]=[Reward]/[Rewarded_Sales]  WHERE [Reward] > 0 AND  [Rewarded_Sales] > 0
	UPDATE @TEMP SET [Overall_Reward_Percentage]=[Reward]/[RewardBrand_Sales] WHERE [Reward] > 0  AND [RewardBrand_Sales] > 0 
	
	SELECT Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales, Rewarded_Sales, Reward, Reward_Percentage, [Overall_Reward_Percentage]
	FROM @TEMP

END
