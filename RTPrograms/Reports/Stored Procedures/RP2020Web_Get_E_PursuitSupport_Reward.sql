﻿


CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_PursuitSupport_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @SEASON_LY INT = @SEASON - 1 ;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	
	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD MONEY;
	DECLARE @TYAVG AS MONEY=0;
	DECLARE @SALES_LY_QTY AS MONEY=0;
	DECLARE @SALES_CY_E_QTY AS MONEY=0;
	DECLARE @SALES_LY_E AS MONEY=0;
	DECLARE @SALES_CY_E AS MONEY=0;
	DECLARE @IS_OU INT;
	
	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode  
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	
				

	--FETCH REWARD DISPLAY VALUES
	SELECT @SALES_LY_QTY=ISNULL(Pursuit_Qty_LY,0)
		  ,@SALES_CY_E_QTY=ISNULL(Pursuit_Qty_CY,0) 
		  ,@SALES_LY_E=ISNULL(QualBrand_Sales_LY,0)
		  ,@SALES_CY_E=ISNULL(QualBrand_Sales_CY,0) 
		  ,@REWARD =ISNULL(Reward,0) 
	From [dbo].[RP2020Web_Rewards_E_PursuitSupport]
	Where STATEMENTCODE = @STATEMENTCODE

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;
	
	--FOR OU QUALIFYING AMOUNT
	IF(NOT(OBJECT_ID('tempdb..#TEMP_OU') IS NULL)) DROP TABLE #TEMP_OU 	
	Select SUM(QualBrand_Sales_LY) QualBrand_Sales_LY,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
	INTO 
	#TEMP_OU
	FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport] WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT  @QUAL_PERCENTAGE = IIF(QualBrand_Sales_LY > 0, QualBrand_Sales_CY / QualBrand_Sales_LY, IIF(QualBrand_Sales_CY > 0, 9.9999, 0)) FROM #TEMP_OU

					
	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);
	
		IF @LANGUAGE = 'F'
			SET @HEADER = '<th></th>
							<th>VAS réalisées '+CAST(@SEASON_LY AS VARCHAR(4))+' ($)</th>
							<th>VAS réalisées ' + cast(@Season as varchar(4)) + ' ($)</th>
							<th><QUAL></th>
							<th>VAS admissibles ' + cast(@Season as varchar(4)) + ' (quantité)</th>
							<th>Récompense</th>
							<th>Récompense ($)</th>'
		ELSE
			SET @HEADER = '<th></th>
							<th>'+CAST(@SEASON_LY AS VARCHAR(4))+' Actual POG $</th>
							<th> ' + cast(@Season as varchar(4)) + ' Actual POG $</th>
							<th><QUAL></th>
							<th>' + cast(@Season as varchar(4)) + ' Eligible POG QTY </th>
							<th>Reward</th>
							<th>Reward $</th>'
		
		SET @HEADER = IIF(@LANGUAGE = 'F', REPLACE(@HEADER,'<QUAL>','% admissible'), 
		IIF(@IS_OU = 0,REPLACE(@HEADER,'<QUAL>','Qualifying %'),REPLACE(@HEADER,'<QUAL>','Qualifying OU %'))) 

		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] , 'PURSUIT HERBICIDE (E) 2 X 3.3 L CASE'  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_LY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](IIF(@SALES_LY_QTY = 0 AND @SALES_CY_E_QTY > 0 , 9.9999, @QUAL_PERCENTAGE),'%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@SALES_CY_E_QTY,'')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,  '$75/case'  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
		FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		IF @LANGUAGE = 'F'
			SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">* Pour se qualifier pour cette récompense, les détaillants doivent réaliser au moins 100 % de leurs VAS admissibles de Pursuit de 2019.</div>
					</div>
				</div>'
		ELSE
			SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">*Retailers must have a minimum of 100% of their 2019 POG Eligible Sales of Pursuit to qualify for this reward.</div>
					</div>
				</div>'


	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @DISCLAIMERTEXT
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

