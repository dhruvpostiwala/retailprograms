﻿

CREATE PROCEDURE [Reports].[RP2020Web_Get_W_PaymentOnTime_Reward] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
			
	SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @RewardSummarySection as  nvarchar(max)='';

	--DECLARE @HTML_Data AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD AS NVARCHAR(MAX)='';
	DECLARE @ML_SUB_TOTAL_ROW AS NVARCHAR(MAX)='';
	DECLARE @ML_TBODY_ROWS AS NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';
	DECLARE @DisclaimerText as nvarchar(max);

	
	/*----------------------------------------------------------------------------------------*/
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	IF NOT OBJECT_ID('tempdb..#Eligible_MARKETLETTERS') IS NULL DROP TABLE #Eligible_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #Eligible_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)

	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP 
	SELECT Marketlettercode
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,MAX(Reward_percentage) AS Reward_percentage
		,SUM(Reward) AS Reward
	INTO #TEMP
	FROM RP2020Web_Rewards_W_PaymentOnTime	
	WHERE Statementcode=@STATEMENTCODE	
	GROUP BY MarketLetterCode

	SET @ML_THEAD = '
		<tr>
			<thead>			
			<th></th>
			<th>' +  @SEASON_STRING+ ' Eligible POG$<br/>Reward Brands</th>
			<th>Reward %</th>
			<th>Reward $</th>
			</thead>
		</tr>	'


	DECLARE MARKET_LETTER_LOOP CURSOR FOR 
		SELECT [MarketLetterCode],[Title],[Sequence] FROM #Eligible_MARKETLETTERS	ORDER BY [Sequence]
	OPEN MARKET_LETTER_LOOP
	FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN 
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">No Eligible POG Transactions Found</td></div>					
					</div>
				</div>' 
				GOTO FETCH_NEXT
			END				


			SET @ML_TBODY_ROWS=CONVERT(NVARCHAR(MAX),(
				SELECT								 
					(SELECT '<b>Total</b>' as td for xml path(''), type)
					,(SELECT 'right_align' as [td/@class],dbo.SVF_Commify(RewardBrand_Sales,'$') as 'td' for xml path(''), type)	
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_percentage,'%')as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,'$') as 'td' for xml path(''), type)
				FROM #Temp		
				WHERE MarketLetterCode=@MARKETLETTERCODE
				FOR XML PATH('tr'), ELEMENTS, TYPE))

				/*
			SET @ML_SUB_TOTAL_ROW = CONVERT(NVARCHAR(MAX), (
									SELECT 'sub_total_row ' as [@class]
										,(SELECT 'Total' AS 'td' for xml path(''), type) 
										,(SELECT '' AS 'td' for xml path(''), type) 
										,(SELECT 'right_align ' as [td/@class],dbo.SVF_Commify(D.TOTAL,'$') as 'td' for xml path(''), type)									
									FROM (
										SELECT SUM(Reward) as Total FROM #TEMP
									)  D 
									FOR XML PATH('tr'), ELEMENTS, TYPE))
			*/
		
			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @MARKETLETTERNAME + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">'
						+ @ML_THEAD +  + '<tbody>' +  @ML_TBODY_ROWS  + '</tbody>
					</table>
				</div>
			</div>' 

	FETCH_NEXT:	
			FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	END
	CLOSE MARKET_LETTER_LOOP
	DEALLOCATE MARKET_LETTER_LOOP

	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP
	--Removed 	<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div>
	-- DISCLAIMER TEXT
	SET @DisclaimerText='
	<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">*<b>InVigor Market Letter Qualifying & Reward Brands</b> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC.</div>
			<div class="rprew_subtabletext">*<b>Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Centurion, Cotegra, Facet L, Lance, Lance AG, Liberty 150.</div>
			<div class="rprew_subtabletext">*<b>Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, & Zidua SC.</div>
			<div class="rprew_subtabletext">*<b>Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> =  Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator Brands.</div>
			<div class="rprew_subtabletext">*<b>Liberty 200 Market Letter Qualifying & Reward Brands</b> = Liberty 200</div>
		
		</div>
	</div>'
	


	/* ############################### FINAL OUTPUT ############################### */
	
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS	
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END

