﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_TankMixBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @NUMDOLLARSPERJUG_LIBCENT INTEGER = 4;
	DECLARE @NUMDOLLARSPERJUG_LIBCENTFACET INTEGER = 2;
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE


	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--FETCH REWARD DISPLAY VALUES
	IF(NOT(OBJECT_ID('tempdb..#DETAILS') IS NULL)) DROP TABLE #DETAILS
	SELECT	RP.RetailerCode
			,RP.RetailerName as Retailer_Name
			,RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
			,T1.Farmcode 
			,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '' ,ISNULL(GC.CompanyName,'')	,ISNULL(GC.FirstName,'') + ' '+ ISNULL(GC.lastName,'')) as Grower_Name
			,ISNULL(GC.City,'') + IIF(ISNULL(GC.Province,'')='','',', ' + GC.Province) as Grower_City
			,IIF(ISNULL(GFI.PIPEDAFarmStatus,'')='','No',GFI.PIPEDAFarmStatus) PIPEDAFarmStatus
			,Liberty_Acres
			,Centurion_Acres
			,Facet_L_Acres
			,Lib_Cent_Matched_Acres
			,Lib_Facet_Matched_Acres
			,Lib_Cent_Reward
			,Lib_Facet_Reward
			,Reward		
	INTO #DETAILS
	FROM RP2021Web_Rewards_W_TankMixBonus T1
		LEFT JOIN RetailerProfile RP			ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode=T1.Farmcode AND GC.IsPrimaryFarmContact='Yes' and GC.[Status]='Active'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode=T1.Farmcode
	WHERE T1.Statementcode=@STATEMENTCODE
	ORDER BY Retailer_Name, Retailer_City, Grower_Name, Grower_City


	
	/* ############################### GROWER DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible<br/>Liberty Acres</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible<br/>Centurion Acres</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible<br/>Facet L Acres</th>
			<th>Matched Liberty &<br/>Centurion Jug Equivalents<br/>($' + CAST(@NUMDOLLARSPERJUG_LIBCENT AS VARCHAR(10)) + ' / Jug)</th>
			<th>Matched Liberty &<br/>Centurion &<br/>Facet L Jug Equivalents<br/>($' + CAST(@NUMDOLLARSPERJUG_LIBCENTFACET AS VARCHAR(10)) + ' / Jug)</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ) AS 'td' for xml path(''), type)
		,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Liberty_Acres, '')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Centurion_Acres, '')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Facet_L_Acres, '')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0')  as 'td' for xml path(''), type)												
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY PIPEDAFarmStatus DESC, Grower_Name
	FOR XML PATH('tr')	, ELEMENTS, TYPE))
/*
	-- TOTAL ROW
	SET @ML_TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
				,(select 5 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,FORMAT(Lib_Cent_Reward / @NUMDOLLARSPERJUG_LIBCENT, 'N0') as 'td' for xml path(''), type)							
				,(select 'right_align' as [td/@class] ,FORMAT(Lib_Facet_Reward / @NUMDOLLARSPERJUG_LIBCENTFACET, 'N0') as 'td' for xml path(''), type)							
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
			FROM (
				SELECT SUM(Lib_Cent_Reward) AS Lib_Cent_Reward	,SUM(Lib_Facet_Reward) AS Lib_Facet_Reward		,SUM(Reward) AS Reward
				FROM #DETAILS
			)  D				
			FOR XML PATH('tr')	, ELEMENTS, TYPE))
*/	

		
	IF NOT EXISTS(SELECT * FROM #DETAILS)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
				<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 
		GOTO FINAL
	END 




	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 

	
	/* ############################### CONVERSION TABLE ############################### */
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Product</th>
					<th>Conversion</th>
				</tr>
				<tr>
					<td>Centurion</td>
					<td>60 acres/case</td>
				</tr>
				<tr>
					<td>Facet L</td>
					<td>80 acres/jug</td>
				</tr>
				
				<tr>
					<td>Liberty 150</td>
					<td>10 acres/13.5L Jug</td>
				</tr>						
			</table>
		</div>
	</div>' 
	
	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying Brands:</strong> Liberty 150, Centurion, and Facet L.
			</div>
			<div class="rprew_subtabletext">
				<strong>Reward Brands:</strong> Liberty 150.
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */
FINAL:
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'	
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

