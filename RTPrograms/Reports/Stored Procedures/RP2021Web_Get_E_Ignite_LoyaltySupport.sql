﻿


CREATE  PROCEDURE [Reports].[RP2021Web_Get_E_Ignite_LoyaltySupport] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @SEASON_LY INT = @SEASON - 1 ;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);

	DECLARE @IS_OU INT;
	

	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode  
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE
	
	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES

	DECLARE @REWARD_PRODUCT_TABLE TABLE
	(
		ProductName VARCHAR(255),
		RewardPerCase MONEY
	)
	INSERT	INTO @REWARD_PRODUCT_TABLE VALUES 
	('IGNITE 150 100 L DRUM', 250),
	('IGNITE 150 2 X 10 L CASE', 100),
	('IGNITE 150 50 L DRUM', 550)

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT	T1.ProductName
			,ISNULL(T2.RewardBrand_QTY, 0) AS RewardBrand_QTY
			,ISNULL(T2.RewardPerUnit, T1.RewardPerCase) AS RewardPerUnit
			,ISNULL(T2.Reward, 0) AS Reward
	INTO	#TEMP
	FROM	@REWARD_PRODUCT_TABLE T1
	LEFT	JOIN (
		SELECT	PR.ProductName
				,RewardBrand_QTY
				,RewardPerUnit
				,Reward
		From	RP2021Web_Rewards_E_Ignite_LoyaltySupport T1
		INNER	JOIN ProductReference PR ON PR.ProductCode = T1.ProductCode
		WHERE	T1.STATEMENTCODE = @STATEMENTCODE
	)T2
		ON T1.ProductName = T2.ProductName



	/* ############################### REWARD SUMMARY TABLE ############################### */
	DECLARE @HEADER NVARCHAR(MAX);

	SET @HEADER =' 
			<thead>
			<tr>
				<th>Product</th>
				<th>' + cast(@Season as varchar(4)) + ' Eligible POG QTY</th>
				<th>Reward Per Unit</th>
				<th class="mw_80p">Reward $</th>
			</tr>
			</thead>'


	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT
			(select ProductName as 'td' for xml path(''), type)
			,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_QTY, '') as 'td' for xml path(''), type)
			,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(RewardPerUnit,'$') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,'$') as 'td' for xml path(''), type)			
		FROM #TEMP 
		ORDER BY RewardPerUnit ASC, ProductName ASC
		FOR XML PATH('tr')	, ELEMENTS, TYPE))	


	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 


	/* ############################### DISCLAIMER TEXT ############################### */
		SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Reward Brands:</strong> Ignite 150.
			</div>
		</div>
	 </div>'	



	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

