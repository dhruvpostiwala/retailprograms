﻿


CREATE PROCEDURE [Reports].[RP2021Web_Get_W_CentAfterMarket] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';
	Declare @DisclaimerText as nvarchar(500)='';

	Declare @RewardSummarySection nvarchar(max)='';
	--Declare @HTML_Data as nvarchar(max);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @QUANTITY AS DECIMAL(16,2)=0;
	DECLARE @REWARD MONEY;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX);
	DECLARE @ML_THEAD AS NVARCHAR(MAX);
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	SET @MARKETLETTERCODE='ML' + @SEASON_STRING + '_W_CPP'
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE 
	   	
	IF NOT EXISTS(SELECT * FROM RP2021Web_Rewards_W_Cent_AFM	WHERE Statementcode=@STATEMENTCODE)
		SET @ML_SECTIONS = '<div class="st_section">

					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>					
					</div>
				</div>' 
	ELSE		
		BEGIN
			SELECT @QUANTITY=Quantity, @REWARD=Reward			
			FROM RP2021Web_Rewards_W_Cent_AFM	
			WHERE Statementcode=@STATEMENTCODE	

			SET @ML_SECTIONS = '<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">
							<tr>
							<thead>	
								<th>' +  @SEASON_STRING+ ' Eligible Centurion Cases</th>
								<th>Reward Per Case</th>
								<th>Reward $</th>
							</thead>
							</tr>
							<tr>
								<td class="right_align">' + dbo.SVF_Commify(@QUANTITY,'') + '</td>
								<td class="right_align">' + dbo.SVF_Commify(2.46,'$') + '</td>
								<td class="right_align">' + dbo.SVF_Commify(@REWARD,'$') + '</td>
							</tr>
						</table>
					</div>
				</div>'

		END

		
	/* ############################### FINAL OUTPUT ############################### */
	SET @DisclaimerText='<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>Qualifying & Reward Brands</b> = Centurion.
				</div>
			</div>
		</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @ML_SECTIONS 	
	SET @HTML_Data += @DisclaimerText;	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')

	
END

