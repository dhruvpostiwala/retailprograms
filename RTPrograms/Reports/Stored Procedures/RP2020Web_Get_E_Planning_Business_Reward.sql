﻿CREATE PROCEDURE [Reports].[RP2020Web_Get_E_Planning_Business_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @MARKETLETTERCODE AS VARCHAR(50) = ''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY AS NVARCHAR(MAX) = '';

	--Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode 
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE;

	SELECT @SEASON=Season FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	--Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;

	--Create temporary table that holds all eligible market letters
	IF NOT OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence]
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE);

	--Create THEAD for the tables
	IF @LANGUAGE = 'F'
		SET @ML_THEAD = '
			<thead>
				<tr>
					<th>VAS admissibles ' + CAST(@SEASON AS VARCHAR(4)) + ' ($)</th>
					<th>Récompense (%)</th>
					<th>Récompense ($)</th>
				</tr>
			</thead>
			';
	ELSE
		SET @ML_THEAD = '
		<thead>
			<tr>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th>Reward %</th>
				<th>Reward $</th>
			</tr>
		</thead>
		';

	--Loop through eligible market letters
	DECLARE ML_LOOP CURSOR FOR
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			--Get the header for the current table being generated
			IF @LANGUAGE = 'F'
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Sommaires des récompenses';
			ELSE 
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary';

			--If there are no transactions, generate HTML that states as such and fetch the next market letter
			IF NOT EXISTS(SELECT * FROM RP2020Web_Rewards_E_PlanningTheBusiness WHERE MarketLetterCode=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
										<div class = "st_header">
											<span>' + @SECTIONHEADER + '</span>
											<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
											<span class="st_right"></span>
										</div>
										<div class="st_content open">					
											<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
										</div>
									</div>';
	
				GOTO FETCH_NEXT;
			END 

			--Use FOR XML PATH to generate HTML from the data from the table
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(RewardBrand_Sales), '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MAX(Reward_Percentage), '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(Reward), '$')  as 'td' for xml path(''), type)						
					FROM RP2020Web_Rewards_E_PlanningTheBusiness
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE AND STATEMENTCODE=@STATEMENTCODE
					FOR XML PATH('tr')	, ELEMENTS, TYPE));

			--Generate the HTML for the section if there are transactions
			SET @ML_SECTIONS += '<div class="st_section">
									<div class = "st_header">
										<span>' + @SECTIONHEADER + '</span>
										<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
										<span class="st_right"></span>
									</div>
				

									<div class="st_content open">									
										<table class="rp_report_table">
										' + @ML_THEAD +   @ML_TBODY  + '
										</table>
									</div>
								</div>';
		FETCH_NEXT:	
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
		END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP

	--Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	--Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json');
END
