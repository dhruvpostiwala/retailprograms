﻿




CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_SollioGrowthReward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @MARKETLETTERCODE AS VARCHAR(50) = 'ML2020_E_CCP'
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX) = '';
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX) = '';
	DECLARE @DISCLAIMERTEXTFRENCH AS NVARCHAR(MAX) = '';


	
	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD_PERC DECIMAL(2,2)=0;
	DECLARE @REWARD MONEY;
	DECLARE @LYSALES AS MONEY=0;
	DECLARE @CYSALES AS MONEY=0;
	DECLARE @REWARDBRAND_SALES_CY AS MONEY=0;
	DECLARE @IS_OU INT;
	
	

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode 
	FROM RP_Config_Programs 
	WHERE ProgramCode=@PROGRAMCODE;

	SELECT @IS_OU = DatasetCode,@SEASON=Season FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;
	
	--FETCH REWARD DISPLAY VALUES
	SELECT @LYSALES=SUM(ISNULL(LYSales,0))
		  ,@CYSALES=SUM(ISNULL(CYSales,0))
		  ,@REWARDBRAND_SALES_CY=SUM(ISNULL(RewardBrand_Sales,0))
		  ,@REWARD_PERC =MAX(ISNULL(Reward_Percentage,0)) 
		  ,@REWARD =SUM(ISNULL(Reward,0)) 
	From [dbo].[RP2020Web_Rewards_E_SollioGrowth]
	Where STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode


	--FOR OU QUALIFYING AMOUNT
	IF(NOT(OBJECT_ID('tempdb..#TEMP_OU') IS NULL)) DROP TABLE #TEMP_OU 	
	Select SUM(CYSales) CYSales,SUM(LYSales) AS LYSales
	INTO 
	#TEMP_OU
	FROM [dbo].[RP2020Web_Rewards_E_SollioGrowth] WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT @QUAL_PERCENTAGE = IIF(LYSales > 0, CYSales / LYSales, IIF(CYSales > 0, 9.9999, 0)) FROM #TEMP_OU


		IF @LANGUAGE = 'F'
		BEGIN
			--Create a THEAD for the table
			SET @ML_THEAD = '
			<thead>
				<tr>
					<th>VAS réalisées ' + CAST(@SEASON-1 AS VARCHAR(4)) + ' ($)</th>
					<th>VAS réalisées ' + CAST(@SEASON AS VARCHAR(4)) + ' ($)</th>
					<th>% admissible <OU></th>
					<th>VAS admissibles ' + CAST(@SEASON AS VARCHAR(4)) + ' ($)</th>
					<th>Récompense (%)</th>
					<th>Récompense ($)</th>
				</tr>
			</thead>';

			SET @REWARDSPERCENTAGEMATRIX = '
			<div class="st_static_section">
				<div class = "st_header">
					<span>POURCENTAGES DE RÉCOMPENSES</span>
					<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr>
							<th>VAS 2020</th>
							<th>Récompense</th>
						</tr>
						<tr>
							<td>10 % d''augmentation des VAS 2020 par rapport à 2019</td>
							<td class="center_align">3%</td>
						</tr>
						<tr>
							<td>15 % d''augmentation des VAS 2020 par rapport à 2019</td>
							<td class="center_align">4%</td>
						</tr>
						<tr>
							<td>20 % d''augmentation des VAS 2020 par rapport à 2019</td>
							<td class="center_align">5%</td>
						</tr>
					</table>
				</div>
			</div>';
		END
		ELSE
		BEGIN
			--Create a THEAD for the table
			SET @ML_THEAD = '
			<thead>
				<tr>
					<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Actual POG $</th>
					<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Actual POG $</th>
					<th>Qualifying <OU> %</th>
					<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
					<th>Reward %</th>
					<th>Reward $</th>
				</tr>
			</thead>';

			SET @REWARDSPERCENTAGEMATRIX = '
			<div class="st_static_section">
				<div class = "st_header">
					<span>REWARD PERCENTAGES</span>
					<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr>
							<th>2020 POG Sales</th>
							<th>Reward</th>
						</tr>
						<tr>
							<td>10% increase in 2020 POG sales relative to 2019</td>
							<td class="center_align">3%</td>
						</tr>
						<tr>
							<td>15% increase in 2020 POG sales relative to 2019</td>
							<td class="center_align">4%</td>
						</tr>
						<tr>
							<td>20% increase in 2020 POG sales relative to 2019</td>
							<td class="center_align">5%</td>
						</tr>
					</table>
				</div>
			</div>';
		END


		IF @IS_OU = 0
		BEGIN
			SET @ML_THEAD = REPLACE(@ML_THEAD,'<OU>','')
		END
		ELSE 
		BEGIN
			SET @ML_THEAD = REPLACE(@ML_THEAD,'<OU>',' OU')	

		END

		-- Use FOR XML PATH to generate HTML from the data from the table
		SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@LYSALES, '$') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@CYSALES, '$') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@QUAL_PERCENTAGE, '%') as 'td' for xml path(''), type)		
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@REWARDBRAND_SALES_CY, '$') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@REWARD_PERC, '%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@REWARD, '$') as 'td' for xml path(''), type)
				FOR XML PATH('tr')	, ELEMENTS, TYPE));

		--Generate the HTML for the section if there are transactions
		SET @ML_SECTIONS += '<div class="st_section">	
								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD +   @ML_TBODY  + '
									</table>
									<qualifyingdisclaimer>
								</div>
							</div>';

	SET @DISCLAIMERTEXT='
			<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext"><LOC></div>
				</div>
			</div>'

	SET @DISCLAIMERTEXTFRENCH = '% Éligible basé sur les totaux des unités opérationnelles'

	SET @DISCLAIMERTEXT = IIF(@IS_OU = 0,REPLACE(@DISCLAIMERTEXT,'<LOC>',''),REPLACE(@DISCLAIMERTEXT,'<LOC>',CASE WHEN @LANGUAGE = 'F' THEN @DISCLAIMERTEXTFRENCH ELSE 'Qualifying % based on Operating Unit Totals' END)) 

	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @DISCLAIMERTEXT;
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json');
END

