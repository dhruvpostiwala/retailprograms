﻿


CREATE PROCEDURE [Reports].[RPWeb_Get_StatementReportMenu] @STATEMENTCODE INT, @Data VARCHAR(MAX)=NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON INT;
	
	SELECT @SEASON=SEASON FROM RPWeb_Statements WHERE Statementcode=@Statementcode

	SELECT @Data=ISNULL(
	(
		SELECT title, programcode
		FROM (
			SELECT 'POG Report' as title, 'pog_report' as programcode, -99 as TabDisplayOrder	
			
			UNION

			SELECT 'Head Office Breakdown' as title, 'head_office_breakdown' as programcode, -98 as TabDisplayOrder
			FROM RPWEB_STATEMENTS
			WHERE statementLevel IN ('LEVEL 2','LEVEL 5') AND STATEMENTCODE = @STATEMENTCODE AND Region = 'WEST'
			
--https://kennahome.jira.com/browse/RP-2970
/*			
				UNION

			SELECT CFG.RewardLabel,  CFG.RewardCode, -2 AS TabDisplayOrder
			FROM [dbo].[RP_Config_Rewards_Summary] cfg
				INNER JOIN (
					SELECT DISTINCT RewardCode
					FROM RPWeb_Rewards_Summary 
					WHERE Statementcode=@STATEMENTCODE AND RewardEligibility='Eligible'
				) REW
				ON REW.RewardCode=CFG.RewardCode
			WHERE CFG.Season=@Season AND CFG.RewardType='OTP'
*/
			UNION
			
			/*
				-- STARTING 2021, Incentives to be displayed under reports menu
			SELECT programs.title,  programs.programcode, Programs.TabDisplayOrder
			FROM RP_Config_Programs programs
				INNER JOIN (
					SELECT DISTINCT RewardCode
					FROM RPWeb_Rewards_Summary 
					WHERE Statementcode=@STATEMENTCODE AND RewardEligibility='Eligible'
				) REW
				ON REW.RewardCode=Programs.RewardCode
			WHERE programs.Season=@Season AND programs.programcode <> 'RP' + CAST(@SEASON AS VARCHAR(4)) + '_W_INCENTIVE'		
			*/


			SELECT programs.title
				,programs.programcode
				,programs.TabDisplayOrder
			FROM RP_Config_Programs programs
				INNER JOIN (
					SELECT RewardCode
					FROM RPWeb_Rewards_Summary 
					WHERE Statementcode=@STATEMENTCODE AND RewardEligibility='Eligible'
					GROUP BY RewardCode
				) REW
				ON REW.RewardCode=Programs.RewardCode
			WHERE programs.Season=@Season


		) D
		ORDER BY title
	FOR JSON AUTO),'[]')


END
