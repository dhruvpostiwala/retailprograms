﻿
CREATE PROCEDURE [Reports].[RP2021_Get_RewardMargins_West] @Statementcode INT
AS
BEGIN
SET NOCOUNT ON;

	/* PROGRAM TABLES:
		[dbo].[RP2021Web_Rewards_W_Adv_Weed_Control] -- TO-DO: HOW TO MAP TO CHEMICALGROUP?		
		[dbo].[RP2021Web_Rewards_W_Cereal_SFO] -- TO-DO: HOW TO MAP TO CHEMICALGROUP?			
		[dbo].[RP2021Web_Rewards_W_DicambaTolerant] -- TO-DO: WHICH BRAND TO REWARD?

	*/

	DECLARE @INV_ML_CODE AS VARCHAR(50)='ML2021_W_INV'			--InVigor Hybrids
	DECLARE @CPP_ML_CODE AS VARCHAR(50)='ML2021_W_CPP'			--Crop Protection Products

	DROP TABLE IF EXISTS #TEMP

	CREATE TABLE #TEMP (
		[StatementCode] [int] NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL,		
		[ProgramCode]	[varchar](50) NOT NULL,
		[ChemicalGroup] [varchar](50) NOT NULL,
		[RewardBrand_Sales] MONEY NOT NULL DEFAULT 0,
		[Rewarded_Sales] MONEY NOT NULL DEFAULT 0,		
		[Reward] MONEY NOT NULL DEFAULT 0,		
		[Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0,
		[Overall_Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[ChemicalGroup] ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)
	

	--DELETE FROM RPWeb_RewardMargins WHERE Statementcode IN (SELECT Statementcode From @Statements)
	
	INSERT #TEMP(Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales)
	SELECT tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup, SUM(tx.Price_CY) Price_CY
	FROM RP2021Web_Sales_Consolidated TX
		INNER JOIN ProductReference PR		
		ON PR.Productcode = TX.ProductCode 
	WHERE tx.Statementcode=@Statementcode AND tx.ProgramCode <> 'ELIGIBLE_SUMMARY' 
		 AND tx.GroupType IN ('REWARD_BRANDS', 'ALL_CPP_BRANDS')  
		/* 
		AND
			(tx.GroupType IN ('REWARD_BRANDS', 'ALL_CPP_BRANDS')   -- Eligible brands can work here
				OR
				--KM: Extra condition because Segment Selling DOES NOT have reward brands. 
			 (TX.ProgramCode = 'RP2021_W_SEGMENT_SELLING' AND TX.GroupType = 'ALL_CPP_BRANDS' AND PR.ChemicalGroup NOT IN ('LIBERTY 150', 'LIBERTY 200', 'CENTURION'))
				OR
			 (TX.ProgramCode = 'RP2021_W_PLANNING_SUPP_BUSINESS' AND TX.GroupType = 'ALL_CPP_BRANDS' AND PR.ChemicalGroup <> 'CENTURION')
			)
		*/
	GROUP BY tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup
	OPTION (RECOMPILE)


	DELETE #TEMP WHERE ProgramCode = 'RP2021_W_SEGMENT_SELLING' AND ChemicalGroup NOT IN ('LIBERTY 150', 'LIBERTY 200', 'CENTURION')
	DELETE #TEMP WHERE ProgramCode = 'RP2021_W_PLANNING_SUPP_BUSINESS' AND ChemicalGroup NOT IN ('CENTURION')
				  
	-- TANK MIX UPDATE
	-- Note: Tank Mix Table does not have price related sales, so calculation for dollar amounts is done here. 
	UPDATE	T1
	SET		Rewarded_Sales=T2.Rewarded_Sales
			,Reward=T2.Reward
	FROM	#TEMP T1
	INNER	JOIN (
		SELECT	TNK.Statementcode
				,TNK.MarketLetterCode
				,TNK.ProgramCode
				,'LIBERTY 150' AS ChemicalGroup
				,TX.RewardBrand_Sales AS Rewarded_Sales
				,TNK.Reward 
		FROM (
				SELECT StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Reward
				FROM [dbo].[RP2021Web_Rewards_W_TankMixBonus] 
				WHERE StatementCode=@STATEMENTCODE AND Reward <> 0
				GROUP BY StatementCode, MarketLetterCode, ProgramCode
			) tnk
			INNER JOIN (
				SELECT MarketLetterCode, SUM(Price_CY) AS RewardBrand_Sales
				FROM RP2021Web_Sales_Consolidated
				WHERE Statementcode=@STATEMENTCODE AND ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS' AND GroupLabel LIKE 'Liberty%' 
				GROUP BY MarketLetterCode
			) TX
			ON TX.MarketLEtterCode=TNK.MarketLetterCode
		)T2
		ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.Programcode=T1.Programcode AND T2.ChemicalGroup=T1.ChemicalGroup
	OPTION  (RECOMPILE)

	/*
	-- LOGISTICS SUPPORT REWARD
	-- Note: Logistic Support does not save at a brand level, calculations are done to find brand level sales and reward

	DROP TABLE IF EXISTS #Logistics_Rewards_Data
	SELECT	StatementCode, MArketLetterCode, ProgramCode, BrandSegment, MAX(Segment_Reward_Percentage) AS Segment_Reward_Percentage
	INTO #Logistics_Rewards_Data
	FROM	[dbo].[RP2021Web_Rewards_W_LogisticsSupport]
	WHERE	StatementCode =  @STATEMENTCODE
	GROUP	BY StatementCode, MArketLetterCode, ProgramCode, BrandSegment


	DROP TABLE IF EXISTS #Logistics_Sales
	SELECT	StatementCode,MarketLetterCode,ProgramCode,GroupLabel,ChemicalGroup,SUM(TX.Price_CY) AS RewardBrand_Sales
	INTO #Logistics_Sales
	FROM	RP2021Web_Sales_Consolidated TX
			INNER	JOIN ProductReference PR 
			ON PR.ProductCode = TX.ProductCode
	WHERE	Statementcode=@STATEMENTCODE AND ProgramCode='RP2021_W_LOGISTICS_SUPPORT' AND GroupType = 'REWARD_BRANDS'
	GROUP	BY StatementCode,MarketLetterCode,ProgramCode,GroupLabel,ChemicalGroup

	UPDATE	A1
	SET		Rewarded_Sales=A2.Rewarded_Sales
			,Reward=A2.Reward
	FROM	#TEMP A1
	INNER	JOIN (	
		SELECT	T1.StatementCode
				,T1.MarketLetterCode
				,T1.ProgramCode
				,T2.ChemicalGroup
				,T2.RewardBrand_Sales AS Rewarded_Sales
				,ISNULL(T1.Segment_Reward_Percentage*RewardBrand_Sales, 0) AS Reward 
		FROM #Logistics_Rewards_Data T1
			INNER JOIN #Logistics_Sales T2
			ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.BrandSegment = T2.GroupLabel
	)A2
		ON A2.Statementcode=A1.Statementcode AND A2.MarketLetterCode=A1.MarketLetterCode AND A2.Programcode=A1.Programcode AND A2.ChemicalGroup=A1.ChemicalGroup
	--OPTION  (OPTIMIZE FOR UNKNOWN) 
	OPTION  (RECOMPILE)
	*/
	
	UPDATE T1
	SET Rewarded_Sales=T2.Rewarded_Sales
		,Reward=T2.Reward
	FROM #TEMP T1
		INNER JOIN (

			-- BRAND SPECIFIC SUPPORT
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_W_BrandSpecificSupport]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0		
				
				UNION ALL

			-- EFFICIENCY BONUS REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,GroupLabel ,Support_Reward_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_W_EFF_BONUS]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL

			-- FUNGICIDE REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(RewardBrand_Sales) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_Fung_Support]
			WHERE	Statementcode=@STATEMENTCODE 	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- INVIGOR INNOVATION REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(RewardBrand_Sales) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_INV_INNOV]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- LIBERTY INVIGOR LOYALTY REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward
			FROM	[dbo].[RP2021Web_Rewards_W_INV_LLB]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL

			-- INVIGOR PERFORMANCE REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'INVIGOR' AS RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward
			FROM	[dbo].[RP2021Web_Rewards_W_INV_PERF]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			
				UNION ALL

			-- INVENTORY MANAGEMENT REWARD - ALL CPP EXCEPTION OF LIBERTY AND CENTURION
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,CPP_Sales AS Rewarded_Sales, CPP_Reward AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_InventoryManagement]
			WHERE	Statementcode=@STATEMENTCODE AND MarketLetterCode = @CPP_ML_CODE AND CPP_Reward > 0	

			
				UNION ALL

			-- INVENTORY MANAGEMENT REWARD - LIBERTY 150
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'LIBERTY 150' AS RewardBrand ,  Liberty_Sales   AS Rewarded_Sales, Liberty_Reward
			FROM	[dbo].[RP2021Web_Rewards_W_InventoryManagement]
			WHERE	Statementcode=@STATEMENTCODE AND MarketLetterCode = @CPP_ML_CODE AND Liberty_Reward > 0	

				UNION ALL

			-- INVENTORY MANAGEMENT REWARD - CENTURION
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'CENTURION' AS RewardBrand ,  Centurion_Sales   AS Rewarded_Sales, Centurion_Reward
			FROM	[dbo].[RP2021Web_Rewards_W_InventoryManagement]
			WHERE	Statementcode=@STATEMENTCODE AND MarketLetterCode = @CPP_ML_CODE AND Centurion_Reward > 0	
			
				UNION ALL
			
			-- INVENTORY MANAGEMENT REWARD - INVIGOR REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,IIF(ISNULL(RewardBrand,'')='','InVigor',RewardBrand) AS RewardBrand ,  InVigor_Sales   AS Rewarded_Sales, Reward
			FROM	[dbo].[RP2021Web_Rewards_W_InventoryManagement]
			WHERE	Statementcode=@STATEMENTCODE AND MarketLetterCode = @INV_ML_CODE AND Reward > 0	
			

				UNION ALL

			-- PAYMENT ON TIME REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward
			FROM	[dbo].[RP2021Web_Rewards_W_PaymentOnTime]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL

			-- WAREHOUSE PAYMENT REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward
			FROM	[dbo].[RP2021Web_Rewards_W_WarehousePayments]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL
						
			-- CLEARFIELD LENTIL REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,MatchedSales , Reward
			FROM	[dbo].[RP2021Web_Rewards_W_ClearfieldLentils_Actual]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	

				UNION ALL

			-- CUSTOM AERIAL
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand , SUM(IIF(RewardBrand_Sales <= RewardBrand_Cust_App, RewardBrand_Sales, RewardBrand_Cust_App)), SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_CustomAerial]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand 

				UNION ALL

			-- CUSTOM SEED TREATING
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand , SUM(IIF(Planned_POG_Sales <= Custom_Treated_POG_Sales, Planned_POG_Sales, Custom_Treated_POG_Sales)), SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_CustomSeed]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand 

				UNION ALL

			-- NODULATOR DUO SCG REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'NODULATOR DUO SCG' AS RewardBrand , SUM(RewardBrand_Sales), SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_Nod_Duo_SCG] 
			GROUP	BY Statementcode, MarketLetterCode, Programcode 
				
				UNION ALL

			-- SEGMENT SELLING REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(RewardBrand_Sales) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_SegmentSelling]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- GROWTH BONUS REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(QualSales_CY) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_GrowthBonus]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- LOYALTY BONUS (PART A)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(SegmentA_Reward_Sales) AS Rewarded_Sales, SUM(SegmentA_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
			WHERE	Statementcode=@STATEMENTCODE AND SegmentA_Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand
				
				UNION ALL

			-- LOYALTY BONUS (PART B)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(SegmentB_Reward_Sales) AS Rewarded_Sales, SUM(SegmentB_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
			WHERE	Statementcode=@STATEMENTCODE AND SegmentB_Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- LOYALTY BONUS (PART C)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(SegmentC_Reward_Sales) AS Rewarded_Sales, SUM(SegmentC_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
			WHERE	Statementcode=@STATEMENTCODE AND SegmentC_Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			-- LOYALTY BONUS (PART D)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(SegmentD_Reward_Sales) AS Rewarded_Sales, SUM(SegmentD_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
			WHERE	Statementcode=@STATEMENTCODE AND SegmentD_Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand
				
				UNION ALL

			-- RETAIL BONUS PAYMENT (CENTURION)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'CENTURION' AS RewardBrand, SUM(Cent_Reward_Sales) AS Rewarded_Sales, SUM(Cent_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_RetailBonus]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode

				UNION ALL

			-- RETAIL BONUS PAYMENT (LIBERTY 150)
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'LIBERTY 150' AS RewardBrand, SUM(Lib_Reward_Sales) AS Rewarded_Sales, SUM(Lib_Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_RetailBonus]
			WHERE	Statementcode=@STATEMENTCODE	AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode
				
				UNION ALL

			-- PLANNING SUPPORTING BUSINESS
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand ,SUM(RewardBrand_Sales) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_W_PlanningTheBusiness_Actual]
			WHERE	Statementcode=@STATEMENTCODE AND Reward > 0	
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

				UNION ALL

			SELECT	StatementCode, MarketLetterCode, ProgramCode
					,BrandSegment AS RewardBrand
					,Segment_Reward_Sales AS Rewarded_Sales, Reward									
			FROM	[dbo].[RP2021Web_Rewards_W_LogisticsSupport]
			WHERE	StatementCode =  @STATEMENTCODE  AND Reward > 0


				UNION ALL
			
			-- WEST CENTURION AFTER MARKET REWARD
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2021Web_Rewards_W_Cent_AFM 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0	
			
				UNION ALL

			-- WEST LIBERTY AFTER MARKET REWARD
			SELECT Statementcode, MarketLetterCode, Programcode
				,RewardBrand ,RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM RP2021Web_Rewards_W_Lib_AFM 
			WHERE Statementcode=@STATEMENTCODE	AND Reward > 0	


	) T2
	ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.Programcode=T1.Programcode AND T2.RewardBrand=T1.ChemicalGroup
	--OPTION  (OPTIMIZE FOR UNKNOWN) 
	--OPTION  (RECOMPILE)

	--DELETE #TEMP WHERE [RewardBrand_Sales]=0  
	UPDATE #TEMP SET [Reward_Percentage]=[Reward]/[Rewarded_Sales]  WHERE [Reward] > 0 AND  [Rewarded_Sales] > 0
	UPDATE #TEMP SET [Overall_Reward_Percentage]=[Reward]/[RewardBrand_Sales] WHERE [Reward] > 0  AND [RewardBrand_Sales] > 0 

	SELECT Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales, Rewarded_Sales, Reward, Reward_Percentage, [Overall_Reward_Percentage]
	FROM #TEMP
	--WHERE Overall_Reward_Percentage <> 0
END