﻿


CREATE   PROCEDURE [Reports].[RP2020Web_Get_W_Soybean_Pulse_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';
	DECLARE @RETAILERCODE VARCHAR(50);
	DECLARE @LEVEL5Code VARCHAR(50);

	Declare @RewardSummarySection nvarchar(max)='';
	--Declare @HTML_Data as nvarchar(max);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';

	DECLARE @PART AS VARCHAR(50)='';

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';
	DECLARE @SECTIONHEADER AS NVARCHAR(MAX)='';
	DECLARE @TEMP_THEAD AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX)='';
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';

	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';
	DECLARE @RewardsPercentageMatrix AS NVARCHAR(MAX)='';
		
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @RETAILERCODE=RetailerCode, @LEVEL5CODE=Level5Code FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))
	
	DECLARE @L_SEASONSTRING AS VARCHAR(4) = CAST(@SEASON-1 AS varchar(4));

	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	SET @MARKETLETTERCODE='ML' + @SEASON_STRING + '_W_CER_PUL_SB'
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE 

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT StatementCode, MarketLetterCode, Part, QualSales_CY, QualSales_LY,Reward_Percentage,Reward,Reward_Sales
	INTO #TEMP
	FROM 
	(
		SELECT 
		  StatementCode
		, MarketLetterCode
		,'Part A' Part
		, SUM([PartA_QualSales_CY]) QualSales_CY
		, SUM([PartA_QualSales_LY]) QualSales_LY
		, MAX([PartA_Reward_Percentage]) Reward_Percentage
		, SUM([PartA_Reward]) Reward
		, NULL Reward_Sales
		FROM RP2020Web_Rewards_W_SoybeanPulsePlanning
		GROUP BY StatementCode, MarketLetterCode

		UNION  

		SELECT 
		  StatementCode
		, MarketLetterCode
		,'Part B' Part
		, SUM([PartB_QualSales_CY]) QualSales_CY
		, SUM([PartB_QualSales_LY]) QualSales_LY
		, MAX([PartB_Reward_Percentage]) Reward_Percentage
		, SUM([PartB_Reward]) Reward
		, SUM([PartB_Reward_Sales]) Reward_Sales
		FROM RP2020Web_Rewards_W_SoybeanPulsePlanning
		GROUP BY StatementCode, MarketLetterCode

		UNION 
	
		SELECT 
		  StatementCode
		, MarketLetterCode
		,'Part C' Part
		, SUM([PartC_QualSales_CY]) QualSales_CY
		, SUM([PartC_QualSales_LY]) QualSales_LY
		, MAX([PartC_Reward_Percentage]) Reward_Percentage
		, SUM([PartC_Reward]) Reward
		, SUM([PartC_Reward_Sales]) Reward_Sales
		FROM RP2020Web_Rewards_W_SoybeanPulsePlanning
		GROUP BY StatementCode, MarketLetterCode
	)ALLPARTS
	WHERE ALLPARTS.[Statementcode]=@Statementcode AND ALLPARTS.[MarketLetterCode]=@MarketLetterCode
	
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
					<div class = "st_header">
						<span>' + @MARKETLETTERNAME + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>					
					</div>
				</div>' 	
		GOTO FINAL
	END 

	DECLARE PART_LOOP CURSOR FOR 
		SELECT  Part FROM #TEMP  ORDER BY [PART]
	OPEN PART_LOOP
	FETCH NEXT FROM PART_LOOP INTO @PART
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			IF @PART = 'Part A'
				SET @TEMP_THEAD='
				<thead>
				<tr>
					<th><header></th>
					<th>' + @SEASON_STRING + ' Eligible IMI POG $</th>
					<th> Qualifying % </th>
					<th> Reward %</th>
					<th>Reward $</th>
				</tr>
				</thead>'
			IF @PART = 'Part B'
				SET @TEMP_THEAD='
				<thead>
				<tr>
					<th><header></th>
					<th>' + @SEASON_STRING + ' Eligible Heat LQ/Heat Complete POG $</th>
					<th> Qualifying % </th>
					<th>' + @SEASON_STRING + ' Eligible IMI POG $</th>
					<th> Reward %</th>
					<th>Reward $</th>
				</tr>
				</thead>'
			IF @PART = 'Part C'
				SET @TEMP_THEAD='
				<thead>
				<tr>
					<th><header></th>
					<th>' + @SEASON_STRING + ' Eligible Priaxor/Dyax POG $</th>
					<th> Qualifying % </th>
					<th>' + @SEASON_STRING + ' Eligible IMI POG $</th>
					<th> Reward %</th>
					<th>Reward $</th>
				</tr>
				</thead>'

			IF @PART='Part A'
			BEGIN
				SET @ML_THEAD_ROW = REPLACE(@TEMP_THEAD,'<header>', @L_SEASONSTRING + ' Eligible IMI POG $')
				SET @SECTIONHEADER =  'Soybean/Pulse Planning and Support Reward - Reward Summary'
			END
			ELSE IF @PART='Part B'
			BEGIN
				SET @ML_THEAD_ROW = REPLACE(@TEMP_THEAD,'<header>','Heat LQ/Heat Complete Target')
				SET @SECTIONHEADER =  'Heat LQ/Heat Complete Reward'
			END
			ELSE 
			BEGIN
				SET @ML_THEAD_ROW = REPLACE(@TEMP_THEAD,'<header>','Priaxor/Dyax Target')
				SET @SECTIONHEADER =  'Priaxor/Dyax Reward'
			END
		
			IF @PART <> 'Part A'
				SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(QualSales_LY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(QualSales_CY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(IIF(QualSales_LY > 0, QualSales_CY/QualSales_LY, IIF(QualSales_CY > 0, 999.99/100, 0)), '%')  as 'td' for xml path(''), type)	
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward_Sales, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)							
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP
					WHERE Part = @PART
					FOR XML PATH('tr')	, ELEMENTS, TYPE))
			ELSE
				SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(QualSales_LY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(QualSales_CY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(IIF(QualSales_LY > 0, QualSales_CY/QualSales_LY, IIF(QualSales_CY > 0, 999.99/100, 0)), '%')  as 'td' for xml path(''), type)		
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)							
					,(select 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP
					WHERE Part = @PART
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD_ROW +   @ML_TBODY  + '
					</table>
				</div>
			</div>' 	
		FETCH NEXT FROM PART_LOOP INTO  @PART
	END
	CLOSE PART_LOOP
	DEALLOCATE PART_LOOP

	/* ****************************** MATRIX TABLE ******************************** */
	SET @RewardsPercentageMatrix = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Reward</th><th>Criteria</th>
					<th>Reward on 2020 POG Sales of<br>IMI Herbicide Brands 
				</tr>
				<tr>
					<td rowspan="3"><b>(A) Soybean/Pulse and Support Reward*:</b><br>2020 POG sales relative to 2019 sales (using 2020 pricing)</td><td style="text-align: center">0-94.9%</td><td style="text-align: center">0%</td>
				</tr>
				<tr>
					<td style="text-align: center">95-99.9%</td><td style="text-align: center">2%</td>
				</tr>
				<td style="text-align: center">>= 100%</td><td style="text-align: center">4%</td></tr>
				<tr>
					<td><b>(B) Heat LQ/Heat Complete Bonus Reward*:</b><br>Meet or Exceed Heat LQ/Heat Complete Target</td><td style="text-align: center">YES</td><td style="text-align: center">1%</td>
				</tr>
				<tr>
					<td><b>(C) Priaxor/Dyax Bonus Reward*:</b><br>Meet or Exceed Priaxor/Dyax Target</td><td style="text-align: center">YES</td><td style="text-align: center">1%</td>
				</tr>
			</table>
		</div>
	</div>' 

	

FINAL:	
	/* ############################### FINAL OUTPUT ############################### */

	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

	-- DISCLAIMER TEXT
	IF @RetailerCode IN ('D520062427') -- If Nutrien
		SET @DisclaimerText='
				<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext">*Heat LQ/Heat Complete Bonus Reward & Priaxor/Dyax Bonus Reward qualification are dependent on the qualification of Soybean/Pulse Planning & Support Reward.</div>
					<div class="rprew_subtabletext"><b>*Soybean/Pulse Planning & Support Qualifying & Reward Brands</b> = Duet, Mizuna, Odyssey® NXT herbicide, Odyssey Ultra NXT, Solo® ADV herbicide, Solo Ultra, Viper® ADV herbicide (all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT)</div>
					<div class="rprew_subtabletext"><b>*Heat LQ/Heat Complete Bonus Reward Qualifying Brands</b> = Heat LQ & Heat Complete.</div>
					<div class="rprew_subtabletext"><b>*Priaxor/Dyax Bonus Reward Qualifying Brands</b> = Priaxor & Dyax</div>
				</div>
			</div>'
	ELSE
		SET @DisclaimerText='
			<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext">*Heat LQ/Heat Complete Bonus Reward & Priaxor/Dyax Bonus Reward qualification are dependent on the qualification of Soybean/Pulse Planning & Support Reward.</div>
					<div class="rprew_subtabletext"><b>*Soybean/Pulse Planning & Support Qualifying & Reward Brands</b> = Odyssey® NXT herbicide, Odyssey Ultra NXT, Solo® ADV herbicide, Solo Ultra, Viper® ADV herbicide (all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT)</div>
					<div class="rprew_subtabletext"><b>*Heat LQ/Heat Complete Bonus Reward Qualifying Brands</b> = Heat LQ & Heat Complete.</div>
					<div class="rprew_subtabletext"><b>*Priaxor/Dyax Bonus Reward Qualifying Brands</b> = Priaxor & Dyax</div>
				</div>
			</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END
