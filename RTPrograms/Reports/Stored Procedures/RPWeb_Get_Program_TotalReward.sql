﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE INT, @PROGRAMCODE Varchar(50), @LANGUAGE AS VARCHAR(1) = 'E', @REWARDSUMMARYSECTION NVARCHAR(MAX) OUTPUT
	
AS
BEGIN			
	DECLARE @SEASON INT;	
	DECLARE @REGION varchar(4);
	DECLARE @LEVEL5_CODE VARCHAR(20);
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @REWARD MONEY;
	DECLARE @TOTALREWARD_TEXT VARCHAR(200) = '';
	DECLARE @TOTALREWARD_SUMMARY NVARCHAR(2000);
	DECLARE @EXCEPTION_VALUE MONEY;
	DECLARE @REWARD_CODE VARCHAR(50)
	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183

	SELECT @SEASON=Season, @REGION=Region, @LEVEL5_CODE=Level5Code, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE




	/* ############################### TEXT DISPLAY ############################### */
	SET @TOTALREWARD_TEXT = 
		CASE 
			WHEN @LANGUAGE = 'F' THEN 'Récompense totale'
			ELSE 'Total Reward'
		END

	/* ############################### REWARD DISPLAY VALUE ############################### */
		

		IF @SEASON >= 2021
		BEGIN
			SELECT 	@REWARD_CODE = RewardCode
			FROM RP_Config_Programs
			WHERE ProgramCode = @PROGRAMCODE

			SELECT	@REWARD = SUM(TotalReward)
			FROM	RPWeb_Rewards_Summary 
			WHERE	StatementCode=@STATEMENTCODE
			 AND	RewardCode IN (@REWARD_CODE,@REWARD_CODE+'X')
		END

		ELSE
			SELECT	@REWARD = SUM(TotalReward)
			FROM	RPWeb_Rewards_Summary SUMM
					INNER	JOIN RP_Config_Programs CONF 
					ON SUMM.RewardCode = CONF.RewardCode 
			WHERE	SUMM.StatementCode=@STATEMENTCODE 
				AND CONF.ProgramCode = @PROGRAMCODE


	/* ############################### FINAL OUTPUT ############################### */
	SET @REWARDSUMMARYSECTION = '
		<div class="st_static_section first">
			<div class="st_content open">
				<table class="rp_report_table"> 
					<thead>
						<tr class = "total_row">
							<td>' + @TOTALREWARD_TEXT + '</th>
							<td style ="text-align:right" width="20%">' + [dbo].[SVF_Commify](@REWARD,@CURRENCY_FORMAT)  +'</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>'

END
