﻿


CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_SegmentSelling] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @Level5Code VARCHAR(20);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @SECTION_PART_A AS NVARCHAR(MAX)='';
	DECLARE @SECTION_PART_B AS NVARCHAR(MAX)='';
	DECLARE @SECTION_PART_ADJUSTMENT  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(2000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @GUARENTEED_REWARD_PERCENT NUMERIC(6,4) = 0.035;


	SELECT -- will be used to set specific fine print
		@Level5Code = Level5Code
	FROM RPWEB_Statements
	WHERE StatementCode = @StatementCode
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT



	IF(NOT(OBJECT_ID('tempdb..#DETAILS') IS NULL)) DROP TABLE #DETAILS
	SELECT	T1.StatementCode, T1.MArketLetterCode, T1.ProgramCode
			,RP.RetailerName AS RetailerName
			,T1.RetailerCode AS RetailerCode
			,RP.MailingCity AS City
			,RP.MailingProvince AS Province
			,SUM(RewardBrand_Sales) AS RewardBrand_Sales
			,MAX(SegmentsAVG_CY) AS SegmentsAVG_CY
			,MAX(SegmentsAVG_LY) AS SegmentsAVG_LY
			,MAX(Growth) AS Growth
			,MAX(Reward_Percentage_A) AS Reward_Percentage_A
			,MAX(Reward_Percentage_B) AS Reward_Percentage_B
			,MAX(Reward_Percentage_Adjustment) AS Reward_Percentage_Adjustment
			,MAX(Reward_Percentage) AS Reward_Percentage
			,SUM(Reward_A) AS Reward_A
			,SUM(Reward_B) AS Reward_B
			,SUM(Reward_Adjustment) AS Reward_Adjustment
			,SUM(Reward) AS Reward
	INTO	#DETAILS
	FROM	[dbo].[RP2021Web_Rewards_W_SegmentSelling] T1
	INNER	JOIN RetailerProfile RP ON RP.RetailerCode = T1.RetailerCode
	WHERE	T1.Statementcode=@STATEMENTCODE
	GROUP	BY T1.StatementCode, T1.MArketLetterCode, T1.ProgramCode ,RP.RetailerName, T1.RetailerCode, RP.MailingProvince, RP.MailingCity

	-- MATCHING HERBICIDES SECTION
	IF NOT EXISTS(SELECT * FROM #DETAILS)
	BEGIN
		SET @SECTION_PART_A = '<div class="st_section">
				<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 
		GOTO FINAL
	END 	

	/* ############################### PART A TABLE ############################### */

	SET @HEADER=
		'<tr>
			<thead>
				<th>Retail Location</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Average Segments per Grower</th>
				<th>Reward %</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th class="mw_80p">Reward $</th>			
			</thead>
		</tr>'

	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RetailerName + ' (' + City + ', ' + Province  + ')' AS 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(SegmentsAVG_CY, '')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage_A, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_A, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY RetailerCode
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	-- TOTAL ROW
	SET @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
			,(select 3 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_A, '$')  as 'td' for xml path(''), type)							
		FROM (
			SELECT SUM(RewardBrand_Sales) AS RewardBrand_Sales, SUM(Reward_A) AS Reward_A
			FROM #DETAILS
		)  D				
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @SECTION_PART_A=
				'<div class="st_static_section">
						<strong>Part A</strong>
				</div>  
				
				<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 


	/* ############################### PART B TABLE ############################### */

	SET @HEADER=
		'<tr>
			<thead>
				<th>Retail Location</th>
				<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Average Segments per Grower</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Average Segments per Grower</th>
				<th>Growth %</th>
				<th>Reward %</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th class="mw_80p">Reward $</th>			
			</thead>
		</tr>'

	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RetailerName + ' (' + City + ', ' + Province  + ')' AS 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(SegmentsAVG_LY, '')  as 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(SegmentsAVG_CY, '')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Growth, '%')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage_B, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_B, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY RetailerCode
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	-- TOTAL ROW
	SET @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
			,(select 5 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_B, '$')  as 'td' for xml path(''), type)							
		FROM (
			SELECT SUM(RewardBrand_Sales) AS RewardBrand_Sales, SUM(Reward_B) AS Reward_B
			FROM #DETAILS
		)  D				
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @SECTION_PART_B =
				'<div class="st_static_section">
						<strong>Part B</strong>
				</div>  

				<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 


	/* ############################### GUARENTEEED REWARD ADJUSTMENT TABLE ############################### */

	SET @HEADER=
		'<tr>
			<thead>
				<th>Retail Location</th>
				<th>Total Reward %</th>
				<th>Guaranteed Reward %</th>
				<th>Reward Adjustment %</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th class="mw_80p">Reward $</th>			
			</thead>
		</tr>'

	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RetailerName + ' (' + City + ', ' + Province  + ')' AS 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage_A + Reward_Percentage_B, '%')  as 'td' for xml path(''), type)		
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(@GUARENTEED_REWARD_PERCENT, '%')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage_Adjustment, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Adjustment, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY RetailerCode
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	-- TOTAL ROW
	SET @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
			,(select 4 AS [td/@colspan], 'Total' AS 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Adjustment, '$')  as 'td' for xml path(''), type)							
		FROM (
			SELECT SUM(RewardBrand_Sales) AS RewardBrand_Sales, SUM(Reward_Adjustment) AS Reward_Adjustment
			FROM #DETAILS
		)  D				
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @SECTION_PART_ADJUSTMENT =
				'<div class="st_static_section">
						<strong>Guaranteed Reward Adjustment</strong>
				</div>  

				<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
FINAL:	
	IF @Level5Code = 'D520062427'-- nutrien
		SET @DISCLAIMERTEXT = 
			'<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext">
						<strong>Inoculant and Seed Treatment Segment Qualifying Brands:</strong> Insure Cereal, Insure Cereal FX 4, Insure Pulse, Nodulator brands, Cimegra, Teraxxa and Titan brands.
					</div>
					<div class="rprew_subtabletext">
						<strong>Herbicide Segment Qualifying Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Engenia, Facet L, Frontier Max, Heat Complete, Heat (Pre-Seed), Heat (Pre-Harvest), Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Odyssey brands, Solo brands, Viper brands, and Zidua.
					</div>
					<div class="rprew_subtabletext">
						<strong>Fungicide Segment Qualifying Brands:</strong> Cantus, Caramba, Cevya, Cotegra, Dyax, Forum, Lance, Nexicor, Priaxor, Sefina, Sercadis, and Twinline.
					</div>			
					<div class="rprew_subtabletext">
						<strong>Liberty 150 and/or Centurion Segment Qualifying Brands:</strong> Liberty 150 and Centurion.
					</div>	
					<div class="rprew_subtabletext">
						<strong>Reward Brands:</strong> Eligible 2021 Crop Protection reward brands except Liberty 150 and Centurion.
					</div>	
				</div>
			 </div>'
	ELSE
		SET @DISCLAIMERTEXT = 
			'<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext">
						<strong>Inoculant and Seed Treatment Segment Qualifying Brands:</strong> Insure Cereal, Insure Cereal FX 4, Insure Pulse, Nodulator brands, Cimegra, Teraxxa and Titan brands.
					</div>
					<div class="rprew_subtabletext">
						<strong>Herbicide Segment Qualifying Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Engenia, Facet L, Frontier Max, Heat Complete, Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Odyssey brands, Outlook, Solo brands, Viper brands, and Zidua.
					</div>
					<div class="rprew_subtabletext">
						<strong>Fungicide Segment Qualifying Brands:</strong> Cantus, Caramba, Cevya, Cotegra, Dyax, Forum, Lance, Nexicor, Priaxor, Sefina, Sercadis, and Twinline.
					</div>			
					<div class="rprew_subtabletext">
						<strong>Liberty 150 and/or Centurion Segment Qualifying Brands:</strong> Liberty 150 and Centurion.
					</div>	
					<div class="rprew_subtabletext">
						<strong>Reward Brands:</strong> Eligible 2021 Crop Protection reward brands except Liberty 150 and Centurion.
					</div>	
				</div>
			 </div>'
		
	
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @SECTION_PART_A 
	SET @HTML_Data += @SECTION_PART_B
	SET @HTML_Data += @SECTION_PART_ADJUSTMENT 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

