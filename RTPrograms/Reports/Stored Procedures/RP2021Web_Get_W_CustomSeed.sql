﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_CustomSeed] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT


AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1500)='';  
	
	DECLARE @ELIGIBLITY_TABLE AS NVARCHAR(MAX)='';
	DECLARE @PRODUCT_DETAIL_TABLE_PART_A  AS NVARCHAR(MAX)='';
	DECLARE @PRODUCT_DETAIL_TABLE_PART_B  AS NVARCHAR(MAX)='';
	DECLARE @REWARD_TABLE  AS NVARCHAR(MAX)='';

	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	

	DECLARE @CUSTOM_APP_SALES MONEY=0;
	DECLARE @PART_A_REWARD_PERCENTAGE DECIMAL(4,4)=0;
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	--GET RETAILERCODE
	DECLARE @RETAILERCODE VARCHAR(50);
	DECLARE @LEVEL5CODE VARCHAR(20);
	SELECT @RETAILERCODE=RetailerCode , @LEVEL5CODE = Level5Code
	FROM RPWeb_Statements 
	WHERE Statementcode=@STATEMENTCODE

	--check for line company for pricing
	DECLARE @LINECOMPANY BIT=0
	DECLARE @IS_FCL_COOP BIT=0
		
	SET @LINECOMPANY = IIF(@RETAILERCODE IN('D0000244','D0000137','D520062427','D0000107'),1,0);
	SET @IS_FCL_COOP = IIF(@LEVEL5CODE = 'D0000117',1,0)

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	DROP TABLE IF EXISTS #TEMP 
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand
		,ROUND(Planned_POG_Sales,2) AS Planned_POG_Sales
		,ROUND(Custom_Treated_POG_Sales,2) AS Custom_Treated_POG_Sales
		,RewardBrand_Percentage, Reward
		,IIF(ROUND(Custom_Treated_POG_Sales,2) <= ROUND(Planned_POG_Sales,2), ROUND(Custom_Treated_POG_Sales,2), ROUND(Planned_POG_Sales,2)) AS Matched_Sales
	INTO #TEMP
	FROM RP2021Web_Rewards_W_CustomSeed 
	WHERE [Statementcode]=@Statementcode


	SELECT @CUSTOM_APP_SALES = SUM(Custom_Treated_POG_Sales)
		   ,@PART_A_REWARD_PERCENTAGE = MAX(RewardBrand_Percentage)				   
	FROM #TEMP
	--WHERE RewardBrand <> 'NODULATOR PRO'


	/* ############################### ELIGBILITY TABLE ############################### */
	SET @HEADER = 
		'<th>Total ' + cast(@Season as varchar(4)) + ' Custom App $</th> 
		<th>Part A Reward %</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@CUSTOM_APP_SALES,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@PART_A_REWARD_PERCENTAGE,'%')  as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
	   
		
	SET @ELIGIBLITY_TABLE = '
	<div class="st_static_section">
		<strong>Part A – Custom Cereal and Pulse Seed Treatment</strong>
	</div>  

	
	<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 



	/* ############################### PRODUCT DETAIL TABLE (PART A) ############################### */
	--NOTE: WILL REUSE @HEADER AND @SUMMARY


	SET @HEADER =' 
	<thead>
	<tr>
		<th>Product</th>
		<th>' + cast(@Season as varchar(4)) + ' Eligible POG $</th>
		<th>' + cast(@Season as varchar(4)) + ' Eligible Custom App $</th>
	
		<th>Reward %</th>
		<th class="mw_80p">Reward $</th>
	</tr>
	</thead>'

		SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 
			(select UPPER(P.RewardBrand) as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(ISNULL(Planned_POG_Sales,0), '$') as 'td' for xml path(''), type)			
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(ISNULL(Matched_Sales,0), '$') as 'td' for xml path(''), type)
			,(select 'center_align' as [td/@class] , dbo.SVF_Commify(ISNULL(RewardBrand_Percentage,@PART_A_REWARD_PERCENTAGE) , '%')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(Reward,0), '$')  as 'td' for xml path(''), type)
		FROM (
			SELECT ChemicalGroup AS RewardBrand	
			FROM ProductReference 
			WHERE Chemicalgroup in ('INSURE PULSE','INSURE CEREAL','INSURE CEREAL FX4', 'TERAXXA F4')  
			GROUP BY ChemicalGroup
		) P
			LEFT JOIN #Temp T
		ON T.RewardBrand = P.RewardBrand			
		ORDER BY P.RewardBrand
		FOR XML PATH('tr')	, ELEMENTS, TYPE))


		-- TOTAL ROW
		SET @SUMMARY += CONVERT(NVARCHAR(MAX),(SELECT 'total_row' as [@class]
			,(select 'Total' as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(Planned_POG_Sales, '$') as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(Matched_Sales, '$') as 'td' for xml path(''), type)
			,(select 'center_align' as [td/@class] , dbo.SVF_Commify(@PART_A_REWARD_PERCENTAGE, '%')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)
		FROM (
			SELECT 
				SUM(Planned_POG_Sales) AS Planned_POG_Sales
				,SUM(Matched_Sales) AS Matched_Sales	
				,SUM(Reward) AS Reward
			FROM #TEMP	
			WHERE RewardBrand IN ('TERAXXA F4', 'INSURE CEREAL', 'INSURE CEREAL FX4', 'INSURE PULSE')
		) D		
		FOR XML PATH('tr')	, ELEMENTS, TYPE))




	SET @PRODUCT_DETAIL_TABLE_PART_A ='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 


	/* ############################### PRODUCT DETAIL TABLE (PART B) ############################### */
	--NOTE: WILL REUSE @HEADER AND @SUMMARY

	SET @HEADER =' 
	<thead>
	<tr>
		<th>Product</th>
		<th>' + cast(@Season as varchar(4)) + ' Eligible POG $</th>		
		<th>' + cast(@Season as varchar(4)) + ' Eligible Custom App $</th>
		<th>Reward %</th>
		<th class="mw_80p">Reward $</th>
	</tr>
	</thead>'
	
	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT
			(select RewardBrand as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(Planned_POG_Sales, '$') as 'td' for xml path(''), type)		
			,(select 'right_align' as [td/@class] , dbo.SVF_Commify(Custom_Treated_POG_EligibleSales, '$') as 'td' for xml path(''), type)
			,(select 'center_align' as [td/@class] , dbo.SVF_Commify(0.08, '%')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)
		FROM (
			SELECT 'NODULATOR PRO 100 CASE' AS RewardBrand
				,ISNULL(t.Planned_POG_Sales,0) AS Planned_POG_Sales
				,ISNULL(t.Custom_Treated_POG_Sales,0) AS Custom_Treated_POG_Sales
				,ISNULL(IIF(t.Planned_POG_Sales > 0 AND Planned_POG_Sales < Custom_Treated_POG_Sales,Planned_POG_Sales , Custom_Treated_POG_Sales),0) AS Custom_Treated_POG_EligibleSales
				,ISNULL(t.RewardBrand_Percentage,0.08) AS RewardBrand_Percentage
				,ISNULL(t.Reward,0) AS Reward
			FROM (
					SELECT 'NODULATOR PRO' AS ChemicalGroup					
				) p
				LEFT JOIN #Temp t
				ON T.RewardBrand=P.ChemicalGroup			
		) D
		ORDER BY RewardBrand
		FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @PRODUCT_DETAIL_TABLE_PART_B =
	'	
	<div class="st_static_section">
		<strong>Part B – Custom Nodulator Seed Treatment</strong>
	</div>  
	
	<div class="st_section">
		<div class="st_content open">					
			<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
		</div>
	</div>' 

	/* ############################### Reward Table #################################### */

	SET @HEADER =' 
		<thead>
		<tr>
			<th>Total Invoice Value of Combined Business (Part A)</th>
			<th>Reward</th>
		</tr>
		</thead>'

	IF @IS_FCL_COOP=1 	
		SET @Summary = '
			<tbody>
				<tr>
					<td class="center_align">&gt; $225,000.00</td>
					<td class="center_align">22%</td>
				</tr>
				<tr>
					<td class="center_align">$100,000 - $225,000.00</td>
					<td class="center_align">18%</td>
				</tr>
				<tr>
					<td class="center_align">&lt; $100,000.00</td>
					<td class="center_align">14%</td>
				</tr>
			</tbody>'
	ELSE IF @LEVEL5CODE IN ('D0000107','D00000137')
	SET @Summary = '
		<tbody>
			<tr>
				<td class="center_align">$184,500.00+</td>
				<td class="center_align">24%</td>
			</tr>
			<tr>
				<td class="center_align">$80,000 - $184,499.99</td>
				<td class="center_align">20%</td>
			</tr>
			<tr>
				<td class="center_align">&lt; $80,000.00</td>
				<td class="center_align">16%</td>
			</tr>
		</tbody>'
	ELSE
	SET @Summary = '
		<tbody>
			<tr>
				<td class="center_align">$185,000.00+</td>
				<td class="center_align">24%</td>
			</tr>
			<tr>
				<td class="center_align">$80,000 - $184,999.99</td>
				<td class="center_align">20%</td>
			</tr>
			<tr>
				<td class="center_align">&lt; $80,000.00</td>
				<td class="center_align">16%</td>
			</tr>
		</tbody>'

	SET @REWARD_TABLE = '	
	<div class="st_section">
		<div class="st_content open">					
			<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
		</div>
	</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Insure Cereal, Insure Cereal FX4, Insure Pulse, Teraxxa, and Nodulator PRO 100.
			</div>

		</div>
	 </div>'		

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ELIGIBLITY_TABLE 
	SET @HTML_Data += @PRODUCT_DETAIL_TABLE_PART_A 
	SET @HTML_Data += @PRODUCT_DETAIL_TABLE_PART_B
	SET @HTML_Data += @REWARD_TABLE
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END
