﻿CREATE PROCEDURE [Reports].[RPWeb_Get_ExposureReportData] @ID INT
AS

BEGIN

	SELECT JSON_DATA=(
		SELECT T1.reportdate, Ex.region, ml.marketlettercode, ML.title as market_letter_title, RSC.RewardLabel as reward_title
			,ex.AccrualAmount as budget
			,ex.AccrualAmount-ex.paidtodate-ex.currentpayment as budget_remaining 
			,ex.rewardamount as reward_amount, ex.paidtodate as paid_to_date, ex.currentpayment as current_payment
			,ex.NextPayment as next_payment
			,SUM(ex.AccrualAmount) OVER() AS total_budget
			,SUM(ex.rewardamount) OVER() AS total_reward
			,SUM(ex.paidtodate) OVER() AS total_paid
			,SUM(ex.currentpayment) OVER() AS total_current_payment
			,SUM(ex.NextPayment) OVER() AS total_next_payment

			,SUM(AccrualAmount) OVER(PARTITION BY ex.region) AS region_total_budget
			,SUM(rewardamount) OVER(PARTITION BY ex.region) AS region_total_reward
			,SUM(ex.paidtodate) OVER(PARTITION BY ex.region) AS region_total_paid
			,SUM(ex.currentpayment) OVER(PARTITION BY ex.region) AS region_current_payment
			,SUM(ex.NextPayment) OVER(PARTITION BY ex.region) AS region_next_payment

			,SUM(AccrualAmount) OVER(PARTITION BY ex.marketlettercode) AS ml_total_budget
			,SUM(rewardamount) OVER(PARTITION BY ex.marketlettercode) AS ml_total_reward
			,SUM(ex.paidtodate) OVER(PARTITION BY ex.marketlettercode) AS ml_total_paid
			,SUM(ex.currentpayment) OVER(PARTITION BY ex.marketlettercode) AS ml_current_payment
			,SUM(ex.NextPayment) OVER(PARTITION BY ex.marketlettercode) AS ml_next_payment

		FROM RPWeb_Reports_Exposure T1
			INNER JOIN RPWeb_Reports_Exposure_AccrualSummary Ex				ON Ex.ExposureID=T1.ExposureID
			INNER JOIN RP_Config_MarketLetters ML							ON ML.MarketLettercode=Ex.MarketLetterCode AND ML.Season=T1.Season
			INNER JOIN RP_Config_Rewards_Summary RSC						ON RSC.RewardCode=ex.RewardCode AND RSC.Season=T1.Season 
		WHERE T1.ExposureID=@ID
		ORDER BY  ML.[Region] DESC, ML.[Sequence], RSC.RewardLabel
		FOR JSON PATH)

END
