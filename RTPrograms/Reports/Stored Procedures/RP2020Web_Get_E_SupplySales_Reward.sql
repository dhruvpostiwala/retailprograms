﻿
CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_SupplySales_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @SEASON_LY INT = @SEASON - 1 ;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @REWARDTABLE AS NVARCHAR(MAX) = '';
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX)=''; 
	DECLARE @DISCLAIMERTEXTFRENCH AS NVARCHAR(MAX) = '';

	DECLARE @ML_THEAD AS NVARCHAR(MAX)=''; 
	DECLARE @ML_TBODY AS NVARCHAR(MAX)=''; 
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)=''; 
	DECLARE @MARKETLETTERCODE AS VARCHAR(250)=''; 
	DECLARE @MARKETLETTERNAME AS VARCHAR(100)=''; 
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(500)=''; 
	
	
	DECLARE @IS_OU INT;
	
	
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE = RewardCode 
	From RP_Config_Programs 
	Where ProgramCode=@PROGRAMCODE

	Select @IS_OU = DatasetCode from RPWeb_Statements where StatementCode =@STATEMENTCODE

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;


	IF(NOT(OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL)) DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)
	
	
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP 
	SELECT Marketlettercode,RewardBrand,RewardBrand_Sales ,Reward_Percentage,Reward
	INTO #TEMP
	FROM RP2020Web_Rewards_E_SupplySales	
	WHERE Statementcode=@STATEMENTCODE


	/* ############################### REWARD SUMMARY TABLE ############################### */
	
	IF @LANGUAGE = 'F'
	BEGIN
		SET @ML_THEAD = '<th>VAS admissibles ' + cast(@Season as varchar(4)) + ' ($)</th><th>Récompense (%)</th><th>Récompense ($)</th>' 
	END
	ELSE
	BEGIN
		SET @ML_THEAD = '<th>' + cast(@Season as varchar(4)) + ' Eligible POG $</th><th><REW> % </th><th>Reward $</th>'
		
		SET @ML_THEAD = IIF(@IS_OU = 0,REPLACE(@ML_THEAD,'<REW>','Reward'),REPLACE(@ML_THEAD,'<REW>','Reward OU Level'))
	END

	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			IF @LANGUAGE = 'F'
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Sommaire des récompenses'
			ELSE
				SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary'
			
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM (
						SELECT	 SUM(ISNULL(RewardBrand_Sales,0)) AS RewardBrand_Sales
								,MAX(ISNULL(Reward_Percentage,0)) AS Reward_Percentage
								,SUM(ISNULL(Reward,0)) AS Reward
						FROM #TEMP
						WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD +   @ML_TBODY  + '
					</table>
				</div>
			</div>' 

			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
		END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP

	IF @LANGUAGE = 'F'
	BEGIN
		SET @REWARDTABLE = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>NOMBRE DE TRANSMISSIONS</th><th>PÉRIODE DE VAS</th><th>RÉCEPTION PAR BASF</th><th>PAIEMENT</th></tr>
					<tr><td style="text-align: center; font-weight: bold">1 transmission</td><td style="text-align: center;">1 octobre 2019 au 30 septembre 2020</td><td style="text-align: center; font-weight: bold">9 octobre 2020</td><td style="text-align: center;">0,25%</td></tr>
					<tr><td rowspan="3" style="text-align: center; font-weight: bold">3 transmissions</td><td style="text-align: center;">1 octobre 2019 au 30 juin 2020</td><td style="text-align: center; font-weight: bold">9 juillet 2020</td><td rowspan="3" style="text-align: center;">0,5%</td></tr>
					<tr><td style="text-align: center;">1 octobre 2019 au 24 août 2020</td><td style="text-align: center; font-weight: bold">28 août 2020</td></tr>
					<tr><td style="text-align: center;">1 octobre 2019 au 30 septembre 2020</td><td style="text-align: center; font-weight: bold">9 octobre 2020</td></tr>
					<tr><td style="text-align: center; font-weight: bold">Mensuellement</td><td style="text-align: center;">Janvier 2020 à septembre 2020</td><td style="text-align: center; font-weight: bold">Le 9 de chaque mois</td><td style="text-align: center;">0,75%</td></tr>
				</table>
			</div>
		</div>
		'
		SET @DISCLAIMERTEXT='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Produits récompensés</b>: Tous les produits de cultures en lignes et tous les produits d''horticulture dans l''Est.</div>
				<div class="rprew_subtabletext">Remarque : Les inoculants sont exclus.</div>
				<div class="rprew_subtabletext"><LOC></div>
			</div>
		</div>'
	END
	ELSE
	BEGIN
		SET @REWARDTABLE = '
			<div class="st_static_section">
				<div class="st_content open">
					<table class="rp_report_table">
						<tr><th>NUMBER OF SUBMISSIONS</th><th>POG SALES PERIOD</th><th>RECEIVED BY BASF</th><th>PAYMENT</th></tr>
						<tr><td style="text-align: center; font-weight: bold">1 Submission</td><td style="text-align: center;">October 1, 2019 to September 30, 2020</td><td style="text-align: center; font-weight: bold">October 9, 2020</td><td style="text-align: center;">0.25%</td></tr>
						<tr><td rowspan="3" style="text-align: center; font-weight: bold">3 Submissions</td><td style="text-align: center;">October 1, 2019 to June 30, 2020</td><td style="text-align: center; font-weight: bold">July 9, 2020</td><td rowspan="3" style="text-align: center;">0.5%</td></tr>
						<tr><td style="text-align: center;">October 1, 2019 to August 24, 2020</td><td style="text-align: center; font-weight: bold">August 28, 2020</td></tr>
						<tr><td style="text-align: center;">October 1, 2019 to September 30, 2020</td><td style="text-align: center; font-weight: bold">October 9, 2020</td></tr>
						<tr><td style="text-align: center; font-weight: bold">Monthly</td><td style="text-align: center;">January 2020 to September 2020</td><td style="text-align: center; font-weight: bold">9th of Each Month</td><td style="text-align: center;">0.75%</td></tr>
					</table>
				</div>
			</div>
			'
		SET @DISCLAIMERTEXT='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Reward Brands</b>: All row crop products and all horticulture products in the East.</div>
				<div class="rprew_subtabletext"> Note: Exclude Inoculants </div>
				<div class="rprew_subtabletext"><LOC></div>
			</div>
		</div>'
	END

	SET @DISCLAIMERTEXTFRENCH = '% Éligible basé sur les totaux des unités opérationnelles'

	SET @DISCLAIMERTEXT = IIF(@IS_OU = 0,REPLACE(@DISCLAIMERTEXT,'<LOC>',''),REPLACE(@DISCLAIMERTEXT,'<LOC>',CASE WHEN @LANGUAGE = 'F' THEN @DISCLAIMERTEXTFRENCH ELSE 'Qualifying % based on Operating Unit Totals' END)) 
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @REWARDTABLE
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

