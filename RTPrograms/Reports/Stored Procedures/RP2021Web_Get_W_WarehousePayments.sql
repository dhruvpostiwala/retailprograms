﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_WarehousePayments] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @REWARD_SALES MONEY=0;
	DECLARE @REWARD_PERCENTAGE DECIMAL(2,2)=0;
	DECLARE @REWARD MONEY=0;
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	SELECT @REWARD_SALES= SUM(ISNULL(RewardBrand_Sales, 0))
		  ,@REWARD_PERCENTAGE = MAX(ISNULL(Reward_Percentage, 0))
		  ,@REWARD = SUM(ISNULL(Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_WarehousePayments]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	-- Get reward summary
	--EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;


	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @HEADER = '<th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th> 
					<th> Reward %</th>
					<th> Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### REWARD PERCENTAGE TABLE ############################### */
/*	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Total Portfolio Support</th>
					<th>Reward %</th>
				</tr>
				<tr>
					<td>Achieve $1M - $1.49M in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 1.00% </td>
				</tr>
				<tr>
					<td>Achieve $1.5M - $1.9M in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 1.75% </td>
				</tr>
				
				<tr>
					<td>Achieve $2M + in BASF Sales valued at 2021 RetailConnect Invoice Price</td>
					<td class="center_align"> 2.50% </td>
				</tr>						
			</table>
		</div>
	</div>' 
*/
	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Eligible 2021 InVigor Reward Brands.
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

