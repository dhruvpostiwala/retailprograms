﻿
CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_BrandSpecificSupport] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @Level5Code VARCHAR(20);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS NVARCHAR(MAX)='';
	DECLARE @ORGANIC_DATA AS VARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	DECLARE @QUALIFYING_SALES_CY MONEY=0;
	DECLARE @QUALIFYING_SALES_2YR_AVG MONEY=0;
	DECLARE @QUALIFYING_PERCENTAGE FLOAT =0;
	DECLARE @REWARD_SALES MONEY=0;
	DECLARE @REWARD_PERCENT DECIMAL(6,4)=0;
	DECLARE @REWARD_TOTAL MONEY=0;


	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	SELECT
		@Level5Code = Level5Code
	FROM RPWEB_Statements
	WHERE StatementCode = @StatementCode

	--FETCH REWARD DISPLAY VALUES
	SELECT @QUALIFYING_SALES_CY= SUM(ISNULL(QualBrand_Sales_CY, 0))
		  ,@QUALIFYING_SALES_2YR_AVG = 				
				CASE 
					WHEN SUM(ISNULL(QualBrand_Sales_LY1, 0)) > 0 AND SUM(ISNULL(QualBrand_Sales_LY2, 0)) > 0 THEN SUM(ISNULL((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2,0))
					WHEN SUM(ISNULL(QualBrand_Sales_LY1, 0)) > 0 AND SUM(ISNULL(QualBrand_Sales_LY2, 0)) <= 0 THEN SUM(ISNULL(QualBrand_Sales_LY1, 0))
					WHEN SUM(ISNULL(QualBrand_Sales_LY1, 0)) <= 0 AND SUM(ISNULL(QualBrand_Sales_LY2, 0)) > 0 THEN SUM(ISNULL(QualBrand_Sales_LY2, 0))
					ELSE 0
				END
		  ,@REWARD_SALES = SUM(ISNULL(RewardBrand_Sales, 0)) 
		  ,@REWARD_PERCENT = MAX(ISNULL(Reward_Percentage, 0)) 
		  ,@REWARD_TOTAL = SUM(ISNULL(Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_BrandSpecificSupport]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SET	@QUALIFYING_PERCENTAGE = 
		CASE
			WHEN @QUALIFYING_SALES_CY > 0 AND @QUALIFYING_SALES_2YR_AVG <= 0 THEN 9.9999
			WHEN @QUALIFYING_SALES_CY <= 0 AND @QUALIFYING_SALES_2YR_AVG <= 0 THEN 0.00
			WHEN @QUALIFYING_SALES_CY/@QUALIFYING_SALES_2YR_AVG > 9.99 THEN 9.9999
			ELSE @QUALIFYING_SALES_CY/@QUALIFYING_SALES_2YR_AVG
		END



	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @HEADER = '<th>2-Year Average Eligible POG $</th> 
				   <th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th>
				   <th>Growth %</th>
				   <th> ' + cast(@Season as varchar(4)) + ' Eligible InVigor POG $</th>
				   <th>Reward %</th>
				   <th>Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_SALES_2YR_AVG,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_SALES_CY,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$') as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENT,'%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_TOTAL,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	SET @ORGANIC_DATA = '{
							"sales":['+cast(@REWARD_SALES as varchar(20))+'],
							"reward_margins":['+cast(@REWARD_PERCENT as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible InVigor POG $"],
							"row_labels":[]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='Default', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT

	/* ############################### REWARD PERCENTAGE TABLE ############################### */
	IF @Level5Code IN ('D0000107','D0000244')	
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>Growth in 2021 Relative to the 2-Year Average</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align"> 3.5% </td>
					</tr>
					<tr>
						<td class="center_align">95% to 104.9%</td>
						<td class="center_align"> 3.0% </td>
					</tr>
					<tr>
						<td class="center_align">85% to 94.9%</td>
						<td class="center_align"> 2.0% </td>
					</tr>	
					<tr>
						<td class="center_align">< 85%</td>
						<td class="center_align"> 1.0% </td>
					</tr>
				</table>
			</div>
		</div>' 	
	ELSE IF @Level5Code = 'D0000137'
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>Growth in 2021 Relative to the 2-Year Average</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align"> 4.5% </td>
					</tr>
					<tr>
						<td class="center_align">95% to 104.9%</td>
						<td class="center_align"> 4.0% </td>
					</tr>
					<tr>
						<td class="center_align">85% to 94.9%</td>
						<td class="center_align"> 3.0% </td>
					</tr>	
					<tr>
						<td class="center_align">< 85%</td>
						<td class="center_align"> 2.0% </td>
					</tr>						
				</table>
			</div>
		</div>' 
	ELSE IF @Level5Code = 'D520062427'
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>Growth in 2021 Relative to the 2-Year Average</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align"> 1.0% </td>
					</tr>
					<tr>
						<td class="center_align">95% to 104.9%</td>
						<td class="center_align"> 0.5% </td>
					</tr>
					</tr>						
				</table>
			</div>
		</div>' 
	ELSE
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>Growth in 2021 Relative to the 2-Year Average</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">105% +</td>
						<td class="center_align"> 4.5%</td>
					</tr>
					<tr>
						<td class="center_align">90% to 104.9%</td>  
						<td class="center_align"> 4.0%</td>
					</tr>
				
					<tr>
						<td class="center_align">< 90%</td>
						<td class="center_align"> 3.0%</td>
					</tr>						
				</table>
			</div>
		</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	-- JIRA:RP-4048
	IF @Level5Code IN ('D0000244', 'D0000137', 'D0000107') -- all westlines except nutrien as of 2021-09-29
	BEGIN
		SET @DISCLAIMERTEXT = '	
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
					<strong>2-Year Average Qualifying Brands:</strong>  Acrobat, Armezon, Basagran, Basagran Forte, Caramba, Centurion, Certitude, Cevya, Cimegra, Distinct, Dyax, Engenia, Forum, Frontier MAX, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC
				</div>
				<div class="rprew_subtabletext">
					<strong>2021 Qualifying Brands:</strong> Acrobat, Armezon, Basagran, Basagran Forte, Caramba, Centurion, Certitude, Cevya, Cimegra, Distinct, Dyax, Engenia, Forum, Frontier MAX, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC
					</div>
				<div class="rprew_subtabletext">
					<strong>Reward Brands:</strong> Eligible 2021 InVigor Reward Brands.
				</div>
			</div>
		</div>	'
	END
	ELSE IF  @Level5Code = 'D520062427' -- nutrien
	BEGIN
		SET @DISCLAIMERTEXT = '	
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
					<strong>2-Year Average Qualifying Brands:</strong> Acrobat, Altitude FX2, Altitude FX3, Armezon, Basagran, Basagran Forte, Caramba, Centurion, Certitude, Cevya, Cimegra, Distinct, Duet, Dyax, Engenia, Forum, Frontier MAX, Heat, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Mizuna, Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC.
				</div>
				<div class="rprew_subtabletext">
					<strong>2021 Qualifying Brands:</strong>Acrobat, Altitude FX2, Altitude FX3, Armezon, Basagran, Basagran Forte, Caramba, Centurion, Certitude, Cevya, Cimegra, Distinct, Duet, Dyax, Engenia, Forum, Frontier MAX, Heat, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Mizuna, Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC.
				</div>
				<div class="rprew_subtabletext">
					<strong>Reward Brands:</strong> Eligible 2021 InVigor Reward Brands.
				</div>
			</div>
		</div>	'
	END
	ELSE -- non west line
	BEGIN
		SET @DISCLAIMERTEXT = '	
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
					<strong>2-Year Average Qualifying Brands:</strong> Acrobat, Armezon, Basagran, Basagran Forte, BASF 28% UAN, Caramba, Centurion, Certitude, Cevya, Cimegra, Distinct, Dyax, Engenia, Forum, Frontier MAX, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC 
				</div>
				<div class="rprew_subtabletext">
					<strong>2021 Qualifying Brands:</strong> Acrobat, Armezon, Basagran, Basagran Forte, BASF 28% UAN, Caramba (125%), Centurion, Certitude, Cevya, Cimegra, Cotegra (50%), Distinct, Dyax, Engenia, Forum, Frontier MAX, Heat LQ, Heat Complete, Insure Cereal Brands, Insure Pulse, Lance (50%), Lance AG (50%), Nealta, Nexicor, Nodulator brands, Odyssey brands, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, Viper ADV, and Zidua SC
				</div>
				<div class="rprew_subtabletext">
					<strong>Reward Brands:</strong> InVigor L340PC, LR344PC, L345PC, L352C, L357P, L230, L233P, L234PC, L241C, L252, and L255PC.
				</div>
			</div>
		</div>	'
	END

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END
