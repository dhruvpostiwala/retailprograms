﻿




CREATE PROCEDURE [Reports].[RP2020Web_Get_W_Portfolio_Achievement_Reward] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @L5CODE AS VARCHAR(10)='';
	DECLARE @RETAILERCODE VARCHAR(50);

	DECLARE @SEASON AS INT;
	DECLARE @REGION AS VARCHAR(4)='';
	
	--DECLARE @HTML_Data as nvarchar(max);
	DECLARE @DisclaimerText as nvarchar(max);
	DECLARE @RewardSummarySection as NVARCHAR(max)='';

	DECLARE @SectionHeader as varchar(200);

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';

	DECLARE @ML_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';

	DECLARE @RewardsPercentageMatrix as nvarchar(max)='';
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @L5CODE= Level5Code, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT


	IF(NOT(OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL)) DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)
	
	
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP 
	SELECT Marketlettercode , GroupLabel AS Brand,RewardBrand_Sales ,QualBrand_Sales_CY ,QualBrand_Sales_LY ,Reward_Percentage ,Reward
	INTO #TEMP
	FROM RP2020Web_Rewards_W_PortfolioAchievement	
	WHERE Statementcode=@STATEMENTCODE
	
	SET @ML_THEAD='
		<tr>
			<th></th>
			<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Eligible POG$</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG$</th>
			<th>Qualifying %</th>
			<th>Reward %</th>
			<th>Reward $</th>
		</tr>'

	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary'
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
					</div>
				</div>' 
	
				GOTO FETCH_NEXT
			END 
			
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select '<b>Total</b>' as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_LY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_CY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(QualBrand_Sales_LY > 0, QualBrand_Sales_CY/QualBrand_Sales_LY, IIF(QualBrand_Sales_CY > 0, 9.9999, 1.15)), '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM (
						SELECT SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
							,SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
							,MAX(Reward_Percentage) AS Reward_Percentage
							,SUM(Reward) AS Reward
						FROM #TEMP
						WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD +   @ML_TBODY  + '
					</table>
				</div>
			</div>' 
FETCH_NEXT:
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
			END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP



/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

 /* ****************************** MATRIX TABLE ******************************** */
	SET @SectionHeader = 'REWARD PERCENTAGES'

	IF @L5CODE = 'D0000117' 
	BEGIN
		SET @RewardsPercentageMatrix = '
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + @sectionHeader + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"</span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				<tr><th>2020 POG $ Qualifying Brands/2019 POG $ Qualifying Brands</th><th>Portfolio Achievement Bonus on Qualifying Brands</th></tr>
				<tr><td class="center_align">115%+</td><td class="center_align">6.00%</td></tr>
				<tr><td class="center_align">110 - 114.9%+</td><td class="center_align">5.50%</td></tr>
				<tr><td class="center_align">95 - 109.5%+</td><td class="center_align">5.00%</td></tr>
				<tr><td class="center_align">90 - 94.9%+</td><td class="center_align">4.00%</td></tr>
				<tr><td class="center_align">85 - 89.9%+</td><td class="center_align">3.00%</td></tr>
				<tr><td class="center_align"><85%+</td><td class="center_align">2.00%</td></tr>
			</table>
		</div>
	</div>' 
	SET @DisclaimerText='
	<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
			<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier Max, Headline, Heat Complete, Heat LQ, Heat WG, Nealta, Nexicor, Odyssey DLX, Odyssey NXT, 
			Odyssey Ultra, Odyssey Ultra NXT, Odyssey WG, Outlook, Poast Ultra, Priaxor, Prowl H2O, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper ADV, Zampro & Zidua SC.
			</div>
			<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
			<!--<div class="rprew_subtabletext">*Titan calculated in Suggested Retail Price (SRP)</div>-->
			<!--<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div>-->
		</div>
	</div>'
	END
	ELSE
	BEGIN
		IF @RETAILERCODE IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- If West Line Company statement
			SET @RewardsPercentageMatrix = '
			<div class="st_static_section">
				<div class = "st_header">
					<span>' + @sectionHeader + '</span>
					<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr><th>2020 Eligible POG $ / 2019 Eligible POG $</th><th>Reward %</th></tr>
						<tr><td class="center_align">115%+</td><td class="center_align">7.00%</td></tr>
						<tr><td class="center_align">110 - 114.9%+</td><td class="center_align">6.50%</td></tr>
						<tr><td class="center_align">95 - 109.9%+</td><td class="center_align">6.00%</td></tr>
						<tr><td class="center_align">90 - 94.9%+</td><td class="center_align">5.00%</td></tr>
						<tr><td class="center_align">85 - 89.9%+</td><td class="center_align">4.00%</td></tr>
						<tr><td class="center_align"><85%</td><td class="center_align">3.00%</td></tr>
					</table>
				</div>
			</div>'
		ELSE
			SET @RewardsPercentageMatrix = '
			<div class="st_static_section">
				<div class = "st_header">
					<span>' + @sectionHeader + '</span>
					<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr><th>2020 POG $ Qualifying Brands/2019 POG $ Qualifying Brands</th><th>Portfolio Achievement Bonus on Qualifying Brands</th></tr>
						<tr><td class="center_align">115%+</td><td class="center_align">7.00%</td></tr>
						<tr><td class="center_align">110 - 114.9%+</td><td class="center_align">6.50%</td></tr>
						<tr><td class="center_align">95 - 109.9%+</td><td class="center_align">6.00%</td></tr>
						<tr><td class="center_align">90 - 94.9%+</td><td class="center_align">5.00%</td></tr>
						<tr><td class="center_align">85 - 89.9%+</td><td class="center_align">4.00%</td></tr>
						<tr><td class="center_align"><85%</td><td class="center_align">3.00%</td></tr>
					</table>
				</div>
			</div>'

	IF @RETAILERCODE = 'D520062427' -- Nutrien
	SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Altitude FX, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Duet, Dyax, 
				Engenia, Forum, Frontier Max, Headline, Heat Complete, Heat LQ, Heat WG, Mizuna, Nealta, Nexicor, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Odyssey WG, Outlook, Poast Ultra, Priaxor, Prowl H2O, Pursuit, Sefina, Sercadis, 
				Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper ADV, Zampro & Zidua SC.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
				<!--<div class="rprew_subtabletext"><span>*Reward calculated at a Retail Location Level</span></div>-->
			</div>
		</div>'

	/**IF @RETAILERCODE = 'D0000107' -- Cargill
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, 
				Engenia, Forum, Frontier, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, 
				Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper, Viper ADV, Zidua SC & Zampro.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator Duo, Nodulator Pro, Nodulator XL.</div>
				<div class="rprew_subtabletext"><span>*Reward calculated at a Retail Location Level</span></div>
			</div>
		</div>'
	ELSE IF @RETAILERCODE = 'D0000137' -- Richardson
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier Max, Headline, Heat, Heat Complete, Heat LQ, Nealta, Nexicor, Odyssey, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Outlook, Poast Ultra, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper ADV, Zampro & Zidua SC.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
				<div class="rprew_subtabletext"><span>*Reward calculated at a Retail Location Level</span></div>
			</div>
		</div>' **/
	ELSE
		SET @DisclaimerText='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Qualifying & Reward Brands</b> =  Cotegra, Facet L, Lance, Lance AG.</div>
				<div class="rprew_subtabletext"><b>*Cereal, Pulse & Soybean Market Letter Qualifying & Reward Brands</b> =  Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Cevya, Distinct, Dyax, Engenia, Forum, Frontier Max, Headline, 
				Heat Complete, Heat LQ, Heat WG, Nealta, Nexicor, Odyssey DLX, Odyssey NXT, Odyssey Ultra, Odyssey Ultra NXT, Odyssey WG, Outlook, Poast Ultra, Priaxor, Prowl H2O, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline, Viper ADV, Zampro & Zidua SC.</div>
				<div class="rprew_subtabletext"><b>*Seed Treatment & Inoculant Market Letter Qualifying & Reward Brands</b> = Insure Cereal, Insure Cereal FX4, Insure Pulse & Nodulator Brands.</div>
				<!--<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div>-->
			</div>
		</div>'
	END

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END



