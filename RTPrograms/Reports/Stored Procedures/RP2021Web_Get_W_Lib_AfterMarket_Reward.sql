﻿

CREATE PROCEDURE [Reports].[RP2021Web_Get_W_Lib_AfterMarket_Reward] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	Declare @SeasonString varchar(4) ='2021';
	Declare @ProgramTitle as varchar(200);
	Declare @RewardCode as varchar(200);
	--Declare @HTML_Data as nvarchar(max);

	Declare @RewardSummarySection nvarchar(max)='';
	Declare @RewardSummaryTable as  nvarchar(max)='';
	
	Declare @DisclaimerText as nvarchar(500)='';  -- not used if needed in the future can easily be used

	Declare @lib_table NVARCHAR(MAX);

	DECLARE @Reward_Per_Jug FLOAT = 2.57;
	Declare @Quantity as decimal(15,2)=0;
	DECLARE @Reward money;

	SELECT @ProgramTitle=Title, @RewardCode = RewardCode  From RP_Config_Programs Where ProgramCode=@ProgramCode

	--FETCH REWARD VALUES
	SELECT @Reward=ISNULL(Reward,0)
		  ,@Quantity=ISNULL(Quantity,0)		
	From RP2021Web_Rewards_W_Lib_AFM 
	Where STATEMENTCODE=@StatementCode

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT
	
	/* ############################### REWARD SUMMARY TABLE ############################### */
	
	SELECT @lib_table = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(@Quantity,'')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , dbo.SVF_Commify(@Reward_Per_Jug,'$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,  dbo.SVF_Commify(@Reward,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	SET @RewardSummaryTable += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead><tr><th>' + @SeasonString + ' Eligible Liberty Jugs</th><th>Reward per Jug</th><th>Reward $</th></tr></thead>
						'+ @lib_table + '
			</table>
		</div>
	</div>' 
		
	/* ############################### FINAL OUTPUT ############################### */
	SET @DisclaimerText='<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext"><b>Qualifying & Reward Brands </b>= Liberty 150.
				</div>
			</div>
		</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection 		
	SET @HTML_Data += @RewardSummaryTable 		
	SET @HTML_Data += @DisclaimerText;
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT		
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
		
	

END
