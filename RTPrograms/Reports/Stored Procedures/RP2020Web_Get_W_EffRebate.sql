﻿


CREATE PROCEDURE [Reports].[RP2020Web_Get_W_EffRebate] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);

	DECLARE @SEASON AS INT;
	DECLARE @REGION AS VARCHAR(4)='';
	--DECLARE @HTML_Data as nvarchar(max);
	DECLARE @DisclaimerText as nvarchar(max);
	DECLARE @RewardSummarySection as NVARCHAR(max)='';

	DECLARE @SectionHeader as varchar(200);


	DECLARE @ELIGIBILITY_SECTION AS NVARCHAR(MAX)

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';


	DECLARE @ML_GROWTH_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_GROWTH_TBODY AS NVARCHAR(MAX)='';


	DECLARE @ML_REWARD_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_REWARD_TBODY AS NVARCHAR(MAX)='';

	DECLARE @RewardsPercentageMatrix as nvarchar(max)='';

	DECLARE @GROWTH_SALES_CY MONEY=0;
	DECLARE @GROWTH_SALES_LY MONEY=0;
	DECLARE @GROWTH DECIMAL(6,4)=0;
	DECLARE @GROWTH_REWARD_PERCENTAGE DECIMAL(6,4)=0;

	DECLARE @ALL_QUAL_SALES MONEY=0;
	DECLARE @SUPPORT_REWARD_PERCENTAGE DECIMAL(6,4)=0;
		
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT


	IF NOT OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)
		
	SELECT @ALL_QUAL_SALES=ISNULL(All_QualSales_CY,0)
		,@GROWTH_SALES_CY=ISNULL(Growth_QualSales_CY,0)
		,@GROWTH_SALES_LY=ISNULL(Growth_QualSales_LY,0)
		,@Growth=ISNULL(Growth,0)
	FROM RP2020Web_Rewards_W_EFF_REBATE_QUAL 
	WHERE Statementcode=@STATEMENTCODE
		
	SELECT @SUPPORT_REWARD_PERCENTAGE=MAX(Support_Reward_Percentage) FROM RP2020Web_Rewards_W_EFF_REBATE WHERE Statementcode=@STATEMENTCODE
	SELECT @GROWTH_REWARD_PERCENTAGE=MAX(ISNULL(Growth_Reward_Percentage,0)) FROM RP2020Web_Rewards_W_EFF_REBATE WHERE Statementcode=@STATEMENTCODE

	IF @GROWTH_SALES_LY <= 0 AND @GROWTH_SALES_CY > 0 
		SET @Growth=(999.99/100)


	SET @ELIGIBILITY_SECTION = '
		<div class="st_section">
			<div class = "st_header">
				<span>REWARD PERCENTAGE SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<div class="rprew_subsectionheader">SUPPORT REWARD PERCENTAGE</div>
				<table  class="rp_report_table">
					<tr><th>2020 Eligible BASF $</th><th>Reward %</th></tr>
					<tr><td class="right_align">' + dbo.SVF_Commify(@ALL_QUAL_SALES,'$')  + '</td><td class="right_align">'  + dbo.SVF_Commify(@SUPPORT_REWARD_PERCENTAGE,'%')  + '</td></tr>
				</table>'

				SET @ELIGIBILITY_SECTION += '<div class="rprew_subsectionheader">ESCALATOR BONUS PERCENTAGE</div>'
				--New Demarey SHOW ESCLATOR BONUS EVEN IF REWARD IS ZERO --RP-2742
				--IF @SUPPORT_REWARD_PERCENTAGE > 0 					
					SET @ELIGIBILITY_SECTION += '
							<table  class="rp_report_table">
								<tr><th>2020 Eligible Reward<br/>Brand POG$</th><th>2019 Eligible Reward<br/>Brand POG$</th><th>Growth %</th><th>Escalator<br/>Bonus %</th></tr>
								<tr>
									<td class="right_align">' + dbo.SVF_Commify(@GROWTH_SALES_CY,'$')  + '</td>
									<td class="right_align">' + dbo.SVF_Commify(@GROWTH_SALES_LY,'$')  + '</td>
									<td class="right_align">' + dbo.SVF_Commify(@GROWTH,'%')  + '</td>
									<td class="right_align">' + dbo.SVF_Commify(@GROWTH_REWARD_PERCENTAGE,'%')  + '</td>
								</tr>
							</table>'
				--ELSE
					--SET @ELIGIBILITY_SECTION +='<div class="rp_tab_message">NOT ELIGIBLE FOR ESCALATOR BONUS<br/>REASON: DID NOT QUALIFY FOR SUPPORT REWARD PERCENTAGE</td></div>'

				SET @ELIGIBILITY_SECTION += '</div>	
					</div>'		


	-- MARKET LETTER SECTIONS
	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT T1.Marketlettercode 
		,SUM(T1.Support_Reward_Sales) AS Reward_Brand_Sales
		,MAX(T1.Support_Reward_Percentage) AS Support_Reward_Percentage
		
		,MAX(T1.Growth_Reward_Percentage) AS Growth_Reward_Percentage		
		,MAX(T2.Growth_QualSales_CY) AS Growth_QualSales_CY
		,MAX(T2.Growth_QualSales_LY) AS Growth_QualSales_LY
		,MAX(T2.Growth) AS Growth

		,SUM(T1.Reward) AS Reward

	INTO #TEMP
	FROM RP2020WEB_Rewards_W_EFF_REBATE	 T1
		INNER JOIN RP2020Web_Rewards_W_EFF_REBATE_QUAL T2
		ON T2.StatementCode=T1.StatementCode --AND T2.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.Statementcode=@STATEMENTCODE
	GROUP BY T1.MarketLetterCode

	/*
	SET @ML_GROWTH_THEAD='
		<tr>
			<th>2020 ELIGIBLE REWARD BRAND POG$</th>
			<th>2019 ELIGIBLE REWARD BRAND POG$</th>
			<th>GROWTH %</th>
			<th>ESCALATOR BONUS %</th>
		</tr>'
*/

	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary'
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>	
					</div>
				</div>' 
	
				GOTO FETCH_NEXT
			END


			IF (0 >= (SELECT max(Growth_Reward_Percentage) FROM #TEMP WHERE MARKETLETTERCODE=@MARKETLETTERCODE group by MarketLetterCode))
				OR
				(2.5 <= (SELECT max(Support_Reward_Percentage) FROM #TEMP WHERE MARKETLETTERCODE=@MARKETLETTERCODE group by MarketLetterCode))
			BEGIN
				SET @ML_REWARD_THEAD='
				<tr>
					<th>2020 Eligible Reward<br/>Brand POG$</th>
					<th>Support Reward %</th>
					<th>Support Reward</th>
					<th>Reward</th>			
				</tr>'
				SET @ML_REWARD_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 						
						(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Brand_Sales, '$')  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Support_Reward_Percentage, '%')  as 'td' for xml path(''), type)
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Brand_Sales * Support_Reward_Percentage , '$')  as 'td' for xml path(''), type)					
						,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
						FROM (
							SELECT Reward_Brand_Sales, Support_Reward_Percentage,Growth_Reward_Percentage ,Reward							
							FROM #TEMP
							WHERE MARKETLETTERCODE=@MARKETLETTERCODE
						) d
						FOR XML PATH('tr')	, ELEMENTS, TYPE))
			END
			ELSE
			BEGIN
				SET @ML_REWARD_THEAD='
				<tr>
					<th>2020 Eligible Reward<br/>Brand POG$</th>
					<th>Support Reward %</th>
					<th>Support Reward</th>
					<th>Escalator Bonus Reward %</th>
					<th>Escalator Bonus Reward</th>
					<th>Reward</th>
				</tr>'

				SET @ML_REWARD_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 						
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Brand_Sales, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Support_Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Brand_Sales * Support_Reward_Percentage , '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Brand_Sales * Growth_Reward_Percentage , '$')  as 'td' for xml path(''), type)					
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM (
						SELECT Reward_Brand_Sales, Support_Reward_Percentage,Growth_Reward_Percentage ,Reward							
						FROM #TEMP
						WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))
			END
		/*	
			 SET @ML_GROWTH_TBODY =  CONVERT(NVARCHAR(MAX),(SELECT 						
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_CY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_QualSales_LY, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth, '%')  as 'td' for xml path(''), type)	
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Growth_Reward_Percentage, '%')  as 'td' for xml path(''), type)										
					FROM (
						SELECT Growth_QualSales_CY, Growth_QualSales_LY, Growth, Growth_Reward_Percentage
						FROM #TEMP
						WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					) d
					FOR XML PATH('tr')	, ELEMENTS, TYPE))
		*/

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">														
					<table class="rp_report_table">
					' + @ML_REWARD_THEAD +   @ML_REWARD_TBODY  + '
					</table>
				</div>
			</div>' 

			/*
				<div class="rprew_subsectionheader">GROWTH REWARD PERCENTAGE SUMMARY</div>
					<table class="rp_report_table">
					' + @ML_GROWTH_THEAD +   @ML_GROWTH_TBODY  + '
					</table>

					<div class="rprew_subsectionheader">REWARD SUMMARY</div>			
			*/

FETCH_NEXT:
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
			END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP




	/* REWARD PERCENTAGES TIER SECTION */
	SET @SectionHeader = 'REWARD PERCENTAGES'
	SET @RewardsPercentageMatrix = '
	<div class="st_static_section">
		<div class = "st_header">
			<span>' + @sectionHeader + '</span>
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"</span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				<tr><th>Total Portfolio Support</th><th>Reward %</th></tr>
				<tr><td>Achieve $1M - $1.49M in BASF $</td><td class="right_align">1.00%</td></tr>
				<tr><td>Achieve $1.5M - $1.99M in BASF $</td><td class="right_align">1.75%</td></tr>
				<tr><td>Achieve $2M+ in BASF $</td><td class="right_align">2.50%</td></tr>
				<tr class="sub_total_row"><td colspan=2 class="center_align">Escalator Bonus</td></tr>
				<tr><td>2020 Eligible Reward Brand POG$/ 2019 Eligible Reward brand POG$ => 15%</td><td>Move to next reward tier</td></tr>
			</table>
		</div>
	</div>' 


	/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP
	--Removed <div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div> 
	SET @DisclaimerText='
<div class="st_static_section">
	<div class="st_content open">
		<div class="rprew_subtabletext">*Qualifying Brands = 2020 Eligible Reward brands.</div>
		<div class="rprew_subtabletext">*Escalator Bonus Brands = 2020 Eligible BASF Crop Protection Portfolio Brands with the exception of Liberty and InVigor brands.</div>
		<div class="rprew_subtabletext">*InVigor Reward Brands = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC.</div>
		<div class="rprew_subtabletext">*Canola Crop Protection Reward Brands = Centurion and Liberty 150.</div>
		
	</div>
</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ELIGIBILITY_SECTION
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END