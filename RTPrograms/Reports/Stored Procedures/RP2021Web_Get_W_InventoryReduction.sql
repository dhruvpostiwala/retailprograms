﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_InventoryReduction] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	SELECT @PROGRAMTITLE = Title
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT
	

	/* ############################### PRODUCT DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Product</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Opening Inventory $</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>MAX ' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>Reward %</th>
			<th>Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = ''; 

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RewardBrand AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Inventory_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Matched_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(RewardPer, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM RP2021Web_Rewards_W_InventoryReduction
	WHERE StatementCode = @STATEMENTCODE AND ProgramCode = @PROGRAMCODE AND (Inventory_Sales<>0 OR RewardBrand_Sales <>0) 
	ORDER BY RewardBrand, RewardPer
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + ISNULL(@ML_TBODY_ROWS,'')  + '</table>		
					</div>
				</div>' 

	
/* ############################### REWARD PERCENTAGE TABLE ############################### */
	-- CENTURION 
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Products</th>
					<th>Reward %</th>
				</tr>
				<tr>
					<td>Insure Cereal, Insure Cereal FX4, Insure Pulse, Armezon, Banvel, Banvel II, Distinct, Engenia, Facet L, Heat Complete, Heat LQ, Odyssey NXT, </br>Poast, Sefina, Sercadis, Solo ADV, and Liberty 200</td>
					<td class="right_align">2.00%</td>
				</tr>
				<tr>
					<td>Basagran, Basagran Forte, Caramba, Cotegra, Dyax, Headline, Lance, Lance AG, Nexicor, Twinline, and Priaxor</td>
					<td class="right_align">2.50%</td>
				</tr>
				
				<tr>
					<td>Odyssey, Odyssey Ultra, Odyssey Ultra NXT, Pursuit, Solo, Solo Ultra, and Viper ADV</td>
					<td class="right_align">2.75%</td>
				</tr>						
			</table>
		</div>
	</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Note:</strong> Max 2021 Eligible POG $ cannot exceed 2021 Opening Inventory $
			</div>
		</div>
	 </div>'
		
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

