﻿
CREATE PROCEDURE [Reports].[RP2021Web_Get_E_Planning_Business_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX)='';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY AS NVARCHAR(MAX) = '';

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END

	--Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode 
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE;

	SELECT @SEASON=Season FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	--Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--Create temporary table that holds all eligible market letters
	IF NOT OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence]
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE);

	--Create THEAD for the tables
	IF @LANGUAGE = 'F'
		SET @ML_THEAD = '
			<thead>
				<tr>
					<th>VAS admissibles ' + CAST(@SEASON AS VARCHAR(4)) + ' ($)</th>
					<th>Récompense (%)</th>
					<th>Récompense ($)</th>
				</tr>
			</thead>
			';
	ELSE
		SET @ML_THEAD = '
		<thead>
			<tr>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
				<th>Reward %</th>
				<th>Reward $</th>
			</tr>
		</thead>
		';
	
	SET @ML_SECTIONS = '<div class="st_section">
							
							<div class="st_content open">					
								<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
							</div>
							</div>';
	
		
	

	--USE FOR XML PATH TO GENERATE HTML FROM THE DATA FROM THE TABLE
	SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					 (select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(RewardBrand_Sales), @CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MAX(Reward_Percentage), @PERCENT_FORMAT)  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(Reward), @CURRENCY_FORMAT)  as 'td' for xml path(''), type)						
					FROM RP2021Web_Rewards_E_PlanningTheBusiness
					WHERE STATEMENTCODE=@STATEMENTCODE
					FOR XML PATH('tr')	, ELEMENTS, TYPE));

	--GENERATE THE HTML FOR THE SECTION IF THERE ARE TRANSACTIONS
	SET @ML_SECTIONS = '<div class="st_section">
							<div class="st_content open">									
								<table class="rp_report_table">
								' + @ML_THEAD +   @ML_TBODY  + '
								</table>
							</div>
						</div>';
	
	SET @DISCLAIMERTEXT='
				<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">
							<strong>Reward Qualification Requirements:</strong> Retails must submit a POG Plan by December 11, 2020 to be eligible for this reward.
						</div>
					</div>
				</div>'


	IF @LANGUAGE = 'F' -- RP-4183
		BEGIN

			Set @DISCLAIMERTEXT = REPLACE(@DISCLAIMERTEXT,'Reward Qualification Requirements:','Conditions de qualification pour la récompense :');
			Set @DISCLAIMERTEXT = REPLACE(@DISCLAIMERTEXT,'Retails must submit a POG Plan by December 11, 2020 to be eligible for this reward.',' Un détaillant doit transmettre son plan de VAS au plus tard le 11 décembre 2020.');
		END


	--CONSTRUCT HTML STRUCTURE
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @DISCLAIMERTEXT;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	--Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'');
END
