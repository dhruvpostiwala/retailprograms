﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_Custom_Aerial] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY_TABLE  AS NVARCHAR(MAX)='';
	DECLARE @PRODUCT_DETAIL_TABLE  AS NVARCHAR(MAX)='';
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	

	DECLARE @ELIGIBLE_CA_ACRES FLOAT=0;
	DECLARE @REWARD_PERCENTAGE DECIMAL(3,3)=0;
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	/*
		GET DISPLAY VALUES
	*/
	-- GET TOTAL CUSTOM ACRES AND REWARD PERCENTAGE
	SELECT	@ELIGIBLE_CA_ACRES = SUM(Cust_App_Acres)
			,@REWARD_PERCENTAGE = MAX(Reward_Percentage)
	FROM RP2021Web_Rewards_W_CustomAerial T1
	WHERE T1.Statementcode=@STATEMENTCODE
	GROUP BY T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode


	-- GET THE LIST OF ALL PRODUCTS 
	/*
	DROP TABLE IF EXISTS #ALL_REWARD_PRODUCTS
	SELECT PR.ProductName, PR.ProductCode
	INTO #ALL_REWARD_PRODUCTS
	FROM RP_Config_Groups G
	INNER JOIN ProductReference PR ON PR.ChemicalGroup = REPLACE(G.GroupLabel,'_X','')
	WHERE ProgramCode = @PROGRAMCODE AND G.GroupType = 'REWARD_BRANDS' AND [Status] = 'Active'
	*/
	
	DROP TABLE IF EXISTS #ALL_REWARD_PRODUCTS
	SELECT ProductName, ProductCode
	INTO #ALL_REWARD_PRODUCTS
	FROM ProductReference
	WHERE ChemicalGroup IN ('CARAMBA','COTEGRA','DYAX','FORUM','HEADLINE','LANCE','LANCE AG','NEXICOR','PRIAXOR','SERCADIS','TWINLINE')
		AND DocType=10
		AND Status='Active'

	-- GET DISPLAY VALUES FOR THE STATEMENT
	DROP TABLE IF EXISTS #CUSTOM_APP_DETAILS
	SELECT	T1.ProductCode
			,SUM(RewardBrand_Sales) AS RewardBrand_Sales
			,SUM(RewardBrand_Cust_App) AS RewardBrand_Cust_App
			,MAX(Reward_Percentage) AS Reward_Percentage
			,SUM(Reward) AS Reward
	INTO #CUSTOM_APP_DETAILS
	FROM RP2021Web_Rewards_W_CustomAerial T1
	WHERE T1.Statementcode=@STATEMENTCODE
	GROUP BY T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, T1.ProductCode

	-- CREATE FINAL TABLE
	DROP TABLE IF EXISTS #DETAILS
	SELECT PR.ProductName 
		   ,ISNULL(CA.RewardBrand_Sales, 0) AS RewardBrand_Sales
		   ,ISNULL(CA.RewardBrand_Cust_App, 0) AS RewardBrand_Cust_App
		   ,ISNULL(CA.Reward_Percentage, @REWARD_PERCENTAGE) AS Reward_Percentage
		   ,ISNULL(CA.Reward, 0) AS Reward
	INTO #DETAILS
	FROM #ALL_REWARD_PRODUCTS PR
	LEFT JOIN #CUSTOM_APP_DETAILS CA ON CA.ProductCode = PR.ProductCode

	
	
	/* ############################### CUSTOM APP SUMMARY TABLE ############################### */
	SET @HEADER = '<th> ' + cast(@Season as varchar(4)) + ' Eligible Custom App Acres</th> 
					<th>Reward %</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@ELIGIBLE_CA_ACRES,'')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE,'%')  as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @SUMMARY_TABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 



	/* ############################### PRODUCT DETAIL TABLE ############################### */
	--NOTE: WILL REUSE @HEADER AND @SUMMARY


	SET @HEADER='<tr><thead>
			<th>Product</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible Custom App $</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select ProductName AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Cust_App, '$')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY ProductName
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @PRODUCT_DETAIL_TABLE='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @HEADER + @SUMMARY  + '</table>		
					</div>
				</div>' 


	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Caramba, Cotegra, Dyax, Forum, Headline, Lance, Lance AG, Nexicor, Priaxor, Sercadis, and Twinline.
			</div>
			<div class="rprew_subtabletext">
				<strong>Qualification Requirements:</strong> A minimum total eligible Custom Application amount of 5,000 acres is required to qualify for this reward. Only Custom Application records from October 1, 2020 to September 30, 2021 will be considered.
			</div>
			<div class="rprew_subtabletext">
				<strong>Note:</strong> 2021 Eligible POG $ and 2021 Eligible Custom App $ are based on 2021 Suggested Retail Pricing.
			</div>
		</div>
	 </div>'	
		

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @SUMMARY_TABLE 
	SET @HTML_Data += @PRODUCT_DETAIL_TABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

