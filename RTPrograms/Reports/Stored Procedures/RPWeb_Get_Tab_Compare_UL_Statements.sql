﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_Tab_Compare_UL_Statements] @STATEMENTCODE INT, @HTML_DATA NVARCHAR(MAX) = NULL OUTPUT

AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

	--CHECK IF A LOCKED STATEMENT OR PAID STATEMENT EXIST

	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4)='';
	DECLARE @CY INT;
	DECLARE @LY INT;
	DECLARE @REGION varchar(10);	
	DECLARE @RETAILERCODE VARCHAR(10);
	DECLARE @LEVEL5CODE VARCHAR(20);
	DECLARE @VERSIONTYPE VARCHAR(50)='';
	DECLARE @SRC_STATEMENTCODE INT=0;

	DECLARE @STATEMENTCODE_UL INT=0;
	DECLARE @STATEMENTCODE_L INT=0;

	DECLARE @LOCKED_DATE DATE=''
	DECLARE @SUMMARY VARCHAR(3);

	SET @HTML_DATA='';
	

	
	DECLARE @NOTHING_TO_COMPARE NVARCHAR(MAX)='';

	--DECLARE @POG_SUMMARY_THEAD_ROW NVARCHAR(MAX)='';
	--DECLARE @POG_SUMMARY_BODY NVARCHAR(MAX)='';
	--DECLARE @POG_SUMMARY_SECTION NVARCHAR(MAX)='';

	DECLARE @DISCLAIMER_TEXT NVARCHAR(MAX)='';

	DECLARE @SEQUNECE INT;

	DECLARE @ML_SUMMARY_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @ML_SUMMARY_BODY NVARCHAR(MAX)='';
	DECLARE @ML_SUMMARY_SECTION NVARCHAR(MAX)='';

	DECLARE @OTHER_REWARDS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_BODY NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_SECTION NVARCHAR(MAX)='';


	DECLARE @REWARDS_DETAILS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @REWARDS_DETAILS_BODY NVARCHAR(MAX)='';
	DECLARE @REWARDS_DETAILS_SECTION NVARCHAR(MAX)='';


	DECLARE @OTHER_REWARDS_DETAILS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_DETAILS_BODY NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_DETAILS_SECTION NVARCHAR(MAX)='';

	
	DECLARE @PAYMENT_DETAILS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @PAYMENT_DETAILS_BODY NVARCHAR(MAX)='';
	DECLARE @PAYMENT_DETAILS_SECTION NVARCHAR(MAX)='';

		   
	DECLARE @Eligible_MarketLetters TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterName Varchar(200) NOT NULL,
		[Sequence] INT NOT NULL
	)

	DECLARE @SKU_LEVEL_SALES_L AS UDT_RP_SKU_LEVEL_SALES;
	DECLARE @SKU_LEVEL_SALES_UL AS UDT_RP_SKU_LEVEL_SALES;
	



	
	DECLARE @TEMP TABLE ( 
		[Title] [varchar](50) NOT NULL,		
		[RewardCode]	[varchar](50) NOT NULL,
		[ShowRewardMargin] [varchar](3) NOT NULL,
		[Margin_U] DECIMAL(5,2) NOT NULL DEFAULT 0,
		[Margin_L] DECIMAL(5,2)NOT NULL DEFAULT 0,	
		[Reward_U] MONEY NOT NULL DEFAULT 0,		
		[Reward_L] MONEY NOT NULL DEFAULT 0		
	)


	SELECT @SEASON=Season, @CY=Season, @LY=Season-1, @Region=Region
	FROM RPWeb_Statements 
	WHERE StatementCode = @STATEMENTCODE;

	--CHECK FOR LOCKED STATEMENT
	--GET UNLOCKED STATEMENTCODE
	SELECT @STATEMENTCODE_UL = 
			CASE WHEN VersionType = 'Locked' THEN SRC_StatementCode 
			WHEN VersionType = 'Unlocked' THEN StatementCode
			END
	FROM RPWeb_Statements WHERE StatementCode = @STATEMENTCODE AND Status = 'Active'

	--GET UNLOCKED STATEMENTCODE
	SELECT @STATEMENTCODE_L = ISNULL(StatementCode,0)
	FROM RPWeb_Statements WHERE SRC_StatementCode = @STATEMENTCODE_UL and VersionType = 'Locked' and Status = 'Active'


	IF (@STATEMENTCODE_L = 0)
	BEGIN
		SET @NOTHING_TO_COMPARE = '
				<div class="rp_rewards_tab ">
				<div class="rp_tab_message">THERE IS NO LOCKED STATEMENT TO COMPARE TO</div>
				</div>'
		GOTO  FINAL
	END


	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	-- ELIGIBLE MARKET LETTERS
	INSERT INTO @Eligible_MarketLetters (MarketLetterCode, MarketLetterName, [Sequence])
	SELECT MarketLetterCode, Title, [Sequence]
	FROM RP_Config_MarketLetters 
	WHERE Season=@SEASON AND Region=@REGION
		AND MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE [StatementCode] = @STATEMENTCODE_UL OR [DataSetCode_L5]= @STATEMENTCODE_UL)

	-- SKU LEVEL SALES Unlocked
	INSERT @SKU_LEVEL_SALES_UL
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE_UL

	-- SKU LEVEL SALES Locked
	INSERT @SKU_LEVEL_SALES_L
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE_L


	-- POG SALES SUMMARY BY MARKET LETTER
	IF NOT OBJECT_ID('tempdb..#ML_Level_Sales') IS NULL DROP TABLE #ML_Level_Sales  
	SELECT UL.MarketLetterCode
		,SUM(ISNULL(UL.CYSales,0)) as CYSales_UL
		,SUM(ISNULL(L.CYSales,0)) AS CYSales_L
		,SUM(ISNULL(UL.CYEligibleSales,0)) as CYEligibleSales_UL
		,SUM(ISNULL(L.CYEligibleSales,0)) AS CYEligibleSales_L
	INTO #ML_Level_Sales
	FROM @SKU_LEVEL_SALES_UL UL 
	LEFT JOIN @SKU_LEVEL_SALES_L L ON L.MarketLetterCode = UL.MarketLetterCode AND L.ProductCode = UL.ProductCode
	GROUP BY UL.MarketLetterCode
	
	--Table Headers
	SET @ML_SUMMARY_THEAD_ROW = '<tr>
		<th>Market Letter</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual<br/>Unlocked</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual<br/>Locked</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual<br/>Difference</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible<br/>Unlocked</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible<br/>Locked</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible<br/>Difference</th>
	</tr>'
	
	-- EACH MARKET LETTER TOTALS (MULTIPLE ROWS)
	SET @ML_SUMMARY_BODY = CONVERT(NVARCHAR(MAX),(
			SELECT IIF (CYSalesDif <> 0,'formatRed','') as [@class], (SELECT  'formatPOGMain' as [td/@class], MarketLetterName as 'td' for xml path(''), type)									
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSales_UL,'$') as 'td' for xml path(''), type)									
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSales_L,'$') as 'td' for xml path(''), type)									
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSalesDif,'$') as 'td' for xml path(''), type)
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales_UL,'$') as 'td' for xml path(''), type)									
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales_L,'$') as 'td' for xml path(''), type)									
				,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSalesDif,'$') as 'td' for xml path(''), type)									
				
			FROM (
				SELECT ML.[Sequence], ML.MarketLetterName	
					,MLS.CYEligibleSales_UL
					,MLS.CYEligibleSales_L 
					,MLS.CYSales_UL
					,MLS.CYSales_L 
					,ISNULL(MLS.CYEligibleSales_UL,0) - ISNULL(MLS.CYEligibleSales_L,0) as CYEligibleSalesDif
					,ISNULL(MLS.CYSales_UL,0) - ISNULL(MLS.CYSales_L,0) as CYSalesDif
				FROM @Eligible_MarketLetters ML
					LEFT JOIN #ML_Level_Sales MLS		ON	 MLS.MarketLetterCode=ML.MarketLetterCode																		
			)  D
			ORDER BY [Sequence]
	FOR XML PATH('tr'), ELEMENTS, TYPE))

	IF @ML_SUMMARY_BODY IS NULL
		SET @ML_SUMMARY_BODY=''


	-- GRAND TOTAL ROW
	SET @ML_SUMMARY_BODY +=  ISNULL(
							CONVERT(NVARCHAR(MAX), (
							SELECT IIF (CYSalesDif <> 0,'formatRed total_row formatPOGMain','total_row formatPOGMain') as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 																								
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSales_UL,'$') as 'td' for xml path(''), type)															
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSales_L,'$') as 'td' for xml path(''), type)															
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYSalesDif,'$') as 'td' for xml path(''), type)
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales_UL,'$') as 'td' for xml path(''), type)															
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales_L,'$') as 'td' for xml path(''), type)															
								,(SELECT 'right_align formatPOGNormal' as [td/@class] ,dbo.SVF_Commify(CYEligibleSalesDif,'$') as 'td' for xml path(''), type)															
								
							FROM (
								SELECT SUM(CYEligibleSales_UL) as CYEligibleSales_UL 
								,SUM(CYEligibleSales_L) as CYEligibleSales_L
								,SUM(CYEligibleSales_UL - CYEligibleSales_L) as CYEligibleSalesDif 
								,SUM(CYSales_UL) as CYSales_UL
								,SUM(CYSales_L) as CYSales_L
								,SUM(CYSales_UL - CYSales_L) as CYSalesDif 
								FROM #ML_Level_Sales									
							)  D
							FOR XML PATH('tr'), ELEMENTS, TYPE))
						,'<tr class="total_row formatPOGMain"><td>Total</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td></tr>')

	SET @ML_SUMMARY_SECTION = '<div class="st_section first">
			<div class = "st_header" style="display: none;">
				<span>POG SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @ML_SUMMARY_THEAD_ROW + @ML_SUMMARY_BODY  + '
				</table> 

			</div>
		</div>' 
		
	IF(NOT(OBJECT_ID('tempdb..#REWARD_SUMMARY') IS NULL)) DROP TABLE #REWARD_SUMMARY --Drop the temp table incase it exists
	SELECT E.StatementCode, P.Title, P.RewardCode
	,ShowRewardMargin
	,ISNULL(Price_CY,0) as CYSales
	,ISNULL(RS.TotalReward,0) AS TotalReward
	INTO #REWARD_SUMMARY
	FROM   
		(
		Select DISTINCT ProgramCode,StatementCode  FROM RPWeb_ML_ELG_Programs
		) E 
	LEFT JOIN 
		(
			SELECT StatementCode,ProgramCode,SUM(ISNULL(Price_CY,0)) Price_CY FROM RP2020Web_Sales_Consolidated
			GROUP BY StatementCode,ProgramCode 
		)tx ON E.ProgramCode = tx.ProgramCode AND E.StatementCode = tx.StatementCode
	INNER JOIN RP_Config_Programs P ON E.ProgramCode = P.ProgramCode 
	LEFT JOIN (
			SELECT StatementCode, RewardCode, SUM(ISNULL(TotalReward,0)) TotalReward FROM RPWeb_Rewards_Summary
			GROUP BY StatementCode,RewardCode
			) RS ON RS.RewardCode = P.RewardCode AND E.StatementCode = RS.StatementCode
	INNER JOIN (
			SELECT RewardCode,ShowRewardMargin
			FROM RP_Config_Rewards_Summary 
			WHERE Season=@Season AND Region = @Region AND RewardType = 'REGULAR'
		) RSC						ON RSC.RewardCode = P.RewardCode 
	WHERE E.StatementCode IN ( @STATEMENTCODE_UL, @STATEMENTCODE_L ) 



	INSERT INTO @TEMP(Title,RewardCode,ShowRewardMargin,Margin_U,Reward_U)
	SELECT Title, RewardCode
		,ShowRewardMargin
		,ISNULL(TotalReward/NULLIF(CYSales,0),0) AS Margin_U
		,ISNULL(TotalReward,0) AS Reward_U
		 FROM #REWARD_SUMMARY 
		 WHERE StatementCode =  @STATEMENTCODE_UL
		 
	
	--UPDATE  Locked StatementValues
	UPDATE T1
	SET T1.Margin_L = ISNULL(TotalReward/NULLIF(CYSales,0),0),
		T1.[Reward_L] = RS.TotalReward
	FROM @TEMP T1
	INNER JOIN #REWARD_SUMMARY RS ON RS.RewardCode = T1.RewardCode
	WHERE RS.StatementCode = @STATEMENTCODE_L

	SET @REWARDS_DETAILS_THEAD_ROW = '
	<tr>
		<th>Market Letter Rewards</th>
		<th>Reward % Unlocked</th>
		<th>Reward % Locked</th>
		<th>Reward $ Unlocked</th>
		<th>Reward $ Locked</th>
		<th>Difference</th>' +
	'</tr>'

	-- EACH MARKET LETTER	
	SELECT @REWARDS_DETAILS_BODY = ISNULL(CONVERT(NVARCHAR(MAX),(
									SELECT IIF(Reward_Difference <>0,'formatRed','') as [@class],  (SELECT  'formatReportMain' as [td/@class], Title as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_U),'%')) as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_L),'%')) as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Difference,'$')  as 'td' for xml path(''), type)
							FROM (
								SELECT *, Reward_U - Reward_L AS Reward_Difference
								FROM 
								@TEMP 
							  ) D
							ORDER BY Title
							FOR XML PATH('tr'), ELEMENTS, TYPE)) , '')



	-- TOTAL ROW		
	SELECT @REWARDS_DETAILS_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT IIF(Reward_Difference <> 0,'formatRed total_row','total_row') as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 
					,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$') as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Difference,'$')  as 'td' for xml path(''), type)
				FROM (
						SELECT SUM(Reward_U) AS Reward_U
							,SUM(Reward_L) AS Reward_L
							,SUM(Reward_U - Reward_L ) AS Reward_Difference
						FROM @TEMP						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')
				
						
		SET @REWARDS_DETAILS_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>MARKET LETTER REWARDS SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @REWARDS_DETAILS_THEAD_ROW + @REWARDS_DETAILS_BODY  + '
				</table>
			</div>
		</div>' 

   	 
	 /***************OTHER REWARDS********************/

	IF(NOT(OBJECT_ID('tempdb..#OTHER_REWARD_SUMMARY') IS NULL)) DROP TABLE #OTHER_REWARD_SUMMARY --Drop the temp table incase it exists
	SELECT E.StatementCode, P.Title, P.RewardCode
	,ISNULL(MAX(RSC.ShowRewardMargin),'') ShowRewardMargin
	--,ISNULL(SUM(RS.TotalReward)/NULLIF(SUM(Price_CY),0),0) as Margin Need to figure out how to do this if posisble ignoring for now
	,SUM(ISNULL(RS.TotalReward,0)) AS TotalReward 
	INTO #OTHER_REWARD_SUMMARY
	FROM   
		(
		Select DISTINCT RewardCode,StatementCode  FROM RPWeb_Rewards_Summary WHERE MarketLetterCode = 'ML2020_W_ADD_PROGRAMS'
		) E 
	--LEFT JOIN RP_DT_Transactions tx ON E.StatementCode = tx.StatementCode AND tx.BasfSeason = 2020 AND tx.InRadius=1  wont work this way
	INNER JOIN RP_Config_Programs P ON E.RewardCode = P.RewardCode 
	LEFT JOIN RPWeb_Rewards_Summary RS ON RS.RewardCode = P.RewardCode AND E.StatementCode = RS.StatementCode 
	INNER JOIN (
			SELECT RewardCode,ShowRewardMargin
			FROM RP_Config_Rewards_Summary 
			WHERE Season=@Season AND Region = @Region AND RewardType = 'REGULAR'
			) RSC						ON RSC.RewardCode = E.RewardCode 
	WHERE E.StatementCode IN ( @STATEMENTCODE_UL, @STATEMENTCODE_L )
	GROUP BY E.StatementCode,P.ProgramCode,P.Title,P.RewardCode
	
	--CLEAN TEMP TABLE TO USE AGAIN
	DELETE FROM @TEMP

	INSERT INTO @TEMP(Title,RewardCode,ShowRewardMargin,Margin_U,Reward_U)
	SELECT Title, RewardCode
		,ShowRewardMargin
		,0 as Margin_U
		,TotalReward AS Reward_U
		 FROM #OTHER_REWARD_SUMMARY 
		 WHERE StatementCode =  @STATEMENTCODE_UL   
	
	--UPDATE  Locked StatementValues
	UPDATE T1
	SET --T1.Margin_L = ISNULL(RS.Margin,0),
		T1.[Reward_L] = ISNULL(RS.TotalReward,0)
	FROM @TEMP T1
	INNER JOIN #OTHER_REWARD_SUMMARY RS ON RS.RewardCode = T1.RewardCode
	WHERE RS.StatementCode = @STATEMENTCODE_L

 
	 SET @OTHER_REWARDS_DETAILS_THEAD_ROW = '
	<tr>
		<th>Additional Market Letter Rewards</th>
		<!--
			<th>Reward % Unlocked</th>
			<th>Reward % Locked</th>
		-->
		<th>Reward $ Unlocked</th>
		<th>Reward $ Locked</th>
		<th>Difference(Unlocked - Locked)</th>' +
	'</tr>'

	-- EACH MARKET LETTER	
	SELECT @OTHER_REWARDS_DETAILS_BODY = ISNULL(CONVERT(NVARCHAR(MAX),(
									SELECT IIF(Reward_Difference <>0,'formatRed','') as [@class], (SELECT  'formatReportMain' as [td/@class], Title as 'td' for xml path(''), type)
									--,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_U),'%')) as 'td' for xml path(''), type)
									--,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_L),'%')) as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Difference,'$')  as 'td' for xml path(''), type)
							FROM (
								SELECT *, Reward_U - Reward_L AS Reward_Difference
								FROM 
								@TEMP) D 
							ORDER BY Title
							FOR XML RAW('tr'), ELEMENTS, TYPE)) , '')



	-- TOTAL ROW		
	SELECT @OTHER_REWARDS_DETAILS_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT IIF(Reward_Difference <> 0,'formatRed total_row','total_row') as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 
					--,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					--,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$') as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U - Reward_L,'$')  as 'td' for xml path(''), type)
				FROM (
						SELECT SUM(Reward_U) AS Reward_U
							,SUM(Reward_L) AS Reward_L
							,SUM(Reward_U - Reward_L) AS Reward_Difference
						FROM @TEMP						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')
				
						
		SET @OTHER_REWARDS_DETAILS_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>ADDITIONAL MARKET LETTER REWARDS SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @OTHER_REWARDS_DETAILS_THEAD_ROW + @OTHER_REWARDS_DETAILS_BODY  + '
				</table>
			</div>
		</div>'  




	/* PAYMENT SECTION */
	SET @PAYMENT_DETAILS_THEAD_ROW = '
	<tr>
		<th>Payment Details</th>
		<!--
			<th>Reward % Unlocked</th>
			<th>Reward % Locked</th>
		-->
		<th>Reward $ Unlocked</th>
		<th>Reward $ Locked</th>
		<th>Difference(Unlocked - Locked)</th>' +
	'</tr>'



	-- EACH MARKET LETTER	
	SELECT @PAYMENT_DETAILS_BODY = ISNULL(CONVERT(NVARCHAR(MAX),(
									SELECT	(SELECT  'formatReportMain' as [td/@class], Title as 'td' for xml path(''), type)
									--,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_U),'%')) as 'td' for xml path(''), type)
									--,(SELECT 'right_align' as [td/@class] ,IIF(ShowRewardMargin='No','',dbo.SVF_Commify((Margin_L),'%')) as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U - Reward_L,'$')  as 'td' for xml path(''), type)
							FROM @TEMP 
							ORDER BY Title
							FOR XML RAW('tr'), ELEMENTS, TYPE)) , '')



	-- TOTAL ROW		
	SELECT @PAYMENT_DETAILS_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT 'total_row' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 
					--,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					--,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U,'$') as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_L,'$')  as 'td' for xml path(''), type)
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_U - Reward_L,'$')  as 'td' for xml path(''), type)
				FROM (
						SELECT SUM(Reward_U) AS Reward_U
							,SUM(Reward_L) AS Reward_L
						FROM @TEMP						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')
				
						
		SET @PAYMENT_DETAILS_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>PAYMENT DETAILS</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @PAYMENT_DETAILS_THEAD_ROW + @PAYMENT_DETAILS_BODY  + '
				</table>
			</div>
		</div>'  
	



FINAL:

	SET @HTML_DATA = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_DATA += 	'<div class="rp_program_header">
		<h1>COMPARE TOOL</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_DATA += isnull(@NOTHING_TO_COMPARE,'')
	SET @HTML_DATA += isnull(@ML_SUMMARY_SECTION,'')
	SET @HTML_DATA += isnull(@REWARDS_DETAILS_SECTION,'')
	SET @HTML_DATA += isnull(@OTHER_REWARDS_DETAILS_SECTION,'')
	--SET @HTML_DATA += isnull(@PAYMENT_DETAILS_SECTION,'')
	
	
	SET @HTML_DATA += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_DATA = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	IF @SEASON <= 2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE 
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
	
END




