﻿


CREATE  PROCEDURE [Reports].[RP2020Web_Get_W_Warehouse_Payment_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	Declare @Invigor_subheader as nvarchar(max) = '';
	Declare @thead as nvarchar(max) = '';
	Declare @Invigor_row as nvarchar(max) = '';
	Declare @ProgramTitle as nvarchar(max) = '';
	Declare @RewardCode as varchar(200);
	--Declare @HTML_Data as nvarchar(max);
	Declare @InvigorSummaryTable as nvarchar(max) = '';
	Declare @RewardSummarySection as nvarchar(max) = '';
	Declare @tfoot as nvarchar(max) = '';
	



SELECT @ProgramTitle=Title, @RewardCode = RewardCode  From RP_Config_Programs Where ProgramCode=@ProgramCode 


EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	SELECT @Invigor_subheader = CONVERT(NVARCHAR(MAX),(
				SELECT 
					'sub_total_row' as [@class]
					,(SELECT '5' as [td/@ColSpan], D.Title as 'td' for xml path(''), type)	
					
				FROM(
					SELECT CM.TITLE, T2.TOTAL FROM RPWeb_ML_ELG_Programs ML
						INNER JOIN RP_Config_MarketLetters CM
						ON CM.MarketLetterCode = ML.MarketLetterCodE
						LEFT JOIN 
						(
							SELECT SUM(REWARD) AS TOTAL, WR.MARKETLETTERCODE FROM RP2020Web_Rewards_W_WarehousePayments WR
							WHERE WR.Statementcode = @STATEMENTCODE
							GROUP BY WR.MarketLetterCode) T2
						ON T2.MARKETLETTERCODE = ML.MarketLetterCode
					WHERE ML.MARKETLETTERCODE = 'ML2020_W_INV' AND ML.PROGRAMCODE = 'RP2020_W_WAREHOUSE_PAYMENT' AND ML.Statementcode = @STATEMENTCODE) D FOR XML PATH('tr'), ELEMENTS, TYPE))
	SELECT @thead = CONVERT(NVARCHAR(MAX),
	
	(SELECT 'Reward Brand 'AS th,'2020 Eligible POG$ Reward Brands' AS th,
	' Reward %' AS th, 'Reward $' AS th FOR XML RAW('tr'), ELEMENTS, TYPE))


	SELECT @Invigor_row = ISNULL(CONVERT(NVARCHAR(MAX),(
			SELECT
				--(SELECT  D1.GroupLabel as 'td' for xml path(''), type)
				 (SELECT  convert(varchar(50), CAST(D1.RB as varchar), -1)as 'td' for xml path(''), type)
				,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(D1.RBS,'$') as 'td' for xml path(''), type)	
				,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(D1.RP,'%')as 'td' for xml path(''), type)
				,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(D1.Reward,'$') as 'td' for xml path(''), type)
			FROM(					
				Select RewardBrand as RB, RewardBrand_Sales as RBS, Reward_percentage as RP, Reward  
				From RP2020Web_Rewards_W_WarehousePayments
				WHERE MARKETLETTERCODE = 'ML2020_W_INV' AND Statementcode = @STATEMENTCODE) D1 FOR XML PATH('tr'), ELEMENTS, TYPE)),
				'<tr><td colspan=10>NO ELIGIBLE POG TRANSACTIONS FOUND</td></tr>')
	
	--Removed <div class="rprew_subtabletext">*Reward calculated at a Retail Location Level</div>
	 SELECT @tfoot ='
	 <div class="rprew_subtabletext">*<strong>Qualifying Brands & Reward Brands</strong> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>

	 '
	
	SET @InvigorSummaryTable = '
<div class="st_section ">
	<div class="st_header">
	<span>'+@Invigor_subheader+'</span>
		<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
	</div>
	<div class="st_content open">
		<table class = "rp_report_table">' + @thead + @Invigor_row + 
		'</table>		
		</div>
		</div>'
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @InvigorSummaryTable
	SET @HTML_Data += @tfoot
	SET @HTML_Data += '</div>'
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
	SET NOCOUNT ON;
	END

