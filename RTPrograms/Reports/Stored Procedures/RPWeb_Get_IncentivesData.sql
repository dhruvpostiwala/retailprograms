﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_IncentivesData] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON INT
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	DECLARE @TOTAL_AMOUNT AS MONEY = 0	


	SELECT @SEASON=Season
	FROM RPWeb_Statements
	WHERE StatementCode = @STATEMENTCODE

	/***************************/
	SELECT @PROGRAMTITLE = Title,  @REWARDCODE = RewardCode
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE
	   
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	/*****************************/
	--FETCH REWARD DISPLAY VALUES
	DROP TABLE IF EXISTS #DETAILS	
	SELECT t1.IncentiveCode ,t1.Amount 
		,ISNULL(t2.Incentive,'') AS IncentiveName
		,ISNULL(RP.RetailerName,'') + ', ' + ISNULL(RP.MailingCity,'') + IIF(ISNULL(RP.MailingProvince,'')='','',', ' + RP.MailingProvince) As RetailerInfo
	INTO #DETAILS
	FROM  dbo.RPWeb_Incentives T1
		LEFT JOIN dbo.incentive  T2 ON t2.incentivecode = t1.incentivecode
		LEFT JOIN dbo.RetailerProfile rp ON rp.retailercode = t1.retailercode
	WHERE T1.StatementCode = @STATEMENTCODE

	SELECT @TOTAL_AMOUNT = SUM(AMOUNT) FROM #DETAILS
	
	/***********************************************/
	SET @ML_THEAD_ROW='<tr><thead>
			<th width=40%>Retail Name</th>
			<th width=30%>Incentive Name</th>
			<th width=20%>Incentive Code</th>
			<th width=10%>Total $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select RetailerInfo AS 'td' for xml path(''), type)
		,(select IncentiveName AS 'td' for xml path(''), type)
		,(select IncentiveCode AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Amount, '$')  as 'td' for xml path(''), type)		
	FROM #DETAILS
	ORDER BY RetailerInfo
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


--	SET @ML_TBODY_ROWS += '<tr class="total_row"><td ColSpan=3>Total</td><td class="right_align">' + dbo.SVF_Commify(@TOTAL_AMOUNT, '$') + '</td></tr>'


	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>		
	</div>'		

	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	--SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	--SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')


	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')

	IF @Season <= 2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')

END
