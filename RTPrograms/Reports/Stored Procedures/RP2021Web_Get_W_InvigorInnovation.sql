﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_InvigorInnovation] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	-- FETCH REWARD DISPLAY VALUES

	-- GET INVIGOR 300 RELATED REWARD DATA
	IF(NOT(OBJECT_ID('tempdb..#DETAILS') IS NULL)) DROP TABLE #DETAILS
	SELECT	RP.RetailerName AS RetailerName,
			T1.RetailerCode AS RetailerCode,
			RP.MailingCity AS RetailerCity,
			RP.MailingProvince AS RetailerProvince,
			Innovation_Bags, 
			InVigor_Bags, 
			Innovation_Sales AS Innovation_Sales,
			Qualifying_Percentage, 
			'300 Series Hybrids' AS Reward_Brands,
			InVigor300_Sales AS RewardBrand_Sales,
			InVigor300_Percentage AS Reward_Percentage,
			InVigor300_Reward AS Reward,
			1 AS DisplaySequence
	INTO #DETAILS
	FROM	RP2021Web_Rewards_W_INV_INNOV T1
	LEFT	JOIN RetailerProfile RP			ON RP.Retailercode=T1.RetailerCode 
	WHERE T1.Statementcode=@STATEMENTCODE

	-- GET INVIGOR 200 RELATED REWARD DATA
	INSERT	INTO #DETAILS
	SELECT	RP.RetailerName AS RetailerName,
			T1.RetailerCode AS RetailerCode,
			RP.MailingCity AS RetailerCity,
			RP.MailingProvince AS RetailerProvince,
			Innovation_Bags, 
			InVigor_Bags, 
			Innovation_Sales AS Innovation_Sales,
			Qualifying_Percentage, 
			'200 Series Hybrids' AS Reward_Brands,
			InVigor200_Sales AS RewardBrand_Sales,
			InVigor200_Percentage AS Reward_Percentage,
			InVigor200_Reward AS Reward,
			2 AS DisplaySequence
	FROM	RP2021Web_Rewards_W_INV_INNOV T1
	LEFT	JOIN RetailerProfile RP			ON RP.Retailercode=T1.RetailerCode 
	WHERE T1.Statementcode=@STATEMENTCODE


		-- If there are no transactions, generate HTML that states as such and fetch the next market letter
	IF NOT EXISTS(SELECT * FROM RP2021Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE)-- AND Reward <> 0)
		BEGIN
			SET @ML_SECTIONS = '<div class="st_section">
									<div class="st_content open">					
										<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>	
									</div>
								</div>';
		END 
	ELSE
		BEGIN
	
			/* ############################### PRODUCT DETAIL TABLE ############################### */
			-- [td/@rowspan]
			SET @ML_THEAD_ROW='<tr><thead>
					<th>Retail Location</th>
					<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible InVigor Innovation Bags</th>
					<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible InVigor Bags</th>
					<th>Total InVigor to InVigor Innovation Brands</th>
					<th width="102">  Reward Brands  </th>
					<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
					<th>Reward %</th>
					<th class="mw_80p">Reward $</th>			
				</thead></tr>'

			SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
				IIF(DisplaySequence = 1, (select 'left_align' as [td/@class], 2 AS [td/@rowspan], RetailerName + ' (' + RetailerCity + ', ' + RetailerProvince + ')' as 'td' for xml path(''), type), NULL)
				,IIF(DisplaySequence = 1, (select 'center_align' as [td/@class], 2 AS [td/@rowspan], dbo.SVF_Commify(Innovation_Bags, '')  as 'td' for xml path(''), type), NULL)
				,IIF(DisplaySequence = 1, (select 'right_align' as [td/@class], 2 AS [td/@rowspan], dbo.SVF_Commify(InVigor_Bags, '')  as 'td' for xml path(''), type), NULL)
				,IIF(DisplaySequence = 1, (select 'center_align' as [td/@class], 2 AS [td/@rowspan], dbo.SVF_Commify(Qualifying_Percentage, '%')  as 'td' for xml path(''), type), NULL)
				,(select 'center_align' as [td/@class] , Reward_Brands  as 'td' for xml path(''), type)	
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)	
				,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)	
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
			FROM #DETAILS
			ORDER BY RetailerName ASC, RetailerCode ASC, DisplaySequence ASC
			FOR XML PATH('tr')	, ELEMENTS, TYPE))


			SET @ML_SECTIONS='<div class="st_section">
							<div class="st_content open">					
								<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
							</div>
						</div>' 
		END

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying Brands:</strong> InVigor L340PC, LR344PC, L345PC, L352C, L357P, L230, L233P, L234PC, L241C, L252, L255PC.
			</div>
			<div class="rprew_subtabletext">
				<strong>Reward Qualification Requirements:</strong> Retail must have a minimum of 20 InVigor Innovation bags to be eligible for this reward.
			</div>
			<div class="rprew_subtabletext">
				<strong>InVigor Innovation Brands:</strong> InVigor L234PC, L340PC, LR344PC, L345PC, L352C, L357P.
			</div>
			<div class="rprew_subtabletext">
				<strong>200 Series Reward Brands:</strong> InVigor L230, L233P, L234PC, L241C, L252, L255PC.
			</div>
			<div class="rprew_subtabletext">
				<strong>300 Series Reward Brands:</strong> InVigor L340PC, LR344PC, L345PC, L352C, L357P.
			</div>
			<div class="rprew_subtabletext">
				<strong>Note:</strong> For the purpose of calculating the Total InVigor to InVigor Innovation Brands, 2021 Eligible InVigor Innovation Bags and 2021 Eligible InVigor Bags do not include any InVigor Reseed amounts.
			</div>
		</div>
	 </div>'
		
	
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

