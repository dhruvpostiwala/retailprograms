﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE INT, @PROGRAMCODE Varchar(50), @REWARDCODE Varchar(50), @LANGUAGE AS VARCHAR(1) = 'E',  @REWARDSUMMARYSECTION NVARCHAR(MAX) OUTPUT
	
AS
BEGIN			
	DECLARE @SEASON INT;	
	DECLARE @REGION varchar(4);
	DECLARE @LEVEL5_CODE VARCHAR(20);
	DECLARE @RETAILERCODE VARCHAR(20);

	DECLARE @MarketLetterCode Varchar(50);
	DECLARE @MarketLetterName Varchar(200);
	DECLARE @INCLUDE_IN_ACTUAL VARCHAR(4)='';
	DECLARE @REWARD_SUMMARY VARCHAR(100)=''
	DECLARE @IS_APPLICABLE VARCHAR(2)='';
	DECLARE @REWARD MONEY=0;
	DECLARE @SEQUENCE INT=0;
	DECLARE @TOTAL_REWARD MONEY=0;
	DECLARE @THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @TBODY_ROW NVARCHAR(MAX)='';
		
	DECLARE @TBODY2_ROWS NVARCHAR(MAX)='';

	SELECT @SEASON=Season, @REGION=Region, @LEVEL5_CODE=Level5Code, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE

	DROP TABLE IF EXISTS #ML_SUMMARY

	IF @LANGUAGE = 'F'
		SET @REWARD_SUMMARY = 'SOMMAIRE DES RÉCOMPENSES'
	ELSE
		SET @REWARD_SUMMARY = 'REWARD SUMMARY'

	--fetch relevant marketletter info
	SELECT ML.[Sequence], ML.[MarketLetterCode], CASE WHEN @LANGUAGE = 'F' THEN ML.[Title_FR] ELSE ML.[Title] END AS TITLE
		,ML.IncludeInActual 
		,ISNULL(Rewards.[IsApplicable],'NA') AS [IsApplicable]
		,IIF( ISNULL(Rewards.IsApplicable,'')='',0,ISNULL(Rewards.Total,0)) AS [Reward]
		,Rewards.RewardCode
	INTO #ML_SUMMARY
	FROM RP_Config_MarketLetters ML
		LEFT JOIN (
			SELECT MarketLetterCode,RewardCode, SUM(TotalReward) AS Total, 'Yes' AS [IsApplicable]
			FROM RPWeb_Rewards_Summary
			WHERE StatementCode=@STATEMENTCODE AND RewardCode=@REWARDCODE
			GROUP BY MarketLetterCode,RewardCode
		) Rewards 
		ON Rewards.MarketLetterCode=ML.MarketLetterCode
	WHERE ML.Season=@SEASON AND ML.Region=@REGION AND ML.Status = 'Active'
		 --AND ML.IncludeInActual='Yes'
	ORDER BY ML.[Sequence]
	   
	SET @MarketLetterCode = 'ML' + CAST(@SEASON AS VARCHAR(4)) + '_W_LIBERTY_200'
	IF @LEVEL5_CODE = 'D000001'
		--UPDATED BY VEDANT TO NOT DISPLAY LIBERTY-200 IF NO ELIGIBLE SALES ARE THERE RP-2847
		DELETE #ML_SUMMARY WHERE MarketLetterCode = @MarketLetterCode AND [Reward] = 0
	ELSE
		DELETE #ML_SUMMARY WHERE MarketLetterCode = @MarketLetterCode
		
	--try to get rid of invigor RP-2364
	IF @REGION = 'EAST' AND @Season = 2020 
	BEGIN
		DELETE #ML_SUMMARY WHERE MarketLetterCode LIKE '%ML%_ADD_PROGRAMS'

		--Added by Vedant to Delete Exceptions Column from Back up Reports
		-- RP 3166
		DELETE #ML_SUMMARY WHERE MarketLetterCode LIKE 'ML%_E_EXCEPTION'

		--Level at which we want to calculate if Retailer Account Type for EAST
		DECLARE @RetailerCode_AccountCheck as VARCHAR(50);
		DECLARE @IS_Invigor_Retailer AS BIT;


		SELECT @RetailerCode_AccountCheck =  IIF(DataSetCode = 0, RetailerCode, Level2Code)  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE

		SELECT @IS_Invigor_Retailer = 
		IIF(CFG.AccountType = 'InVigor Retailer - East',1,0 )
		FROM RetailerProfileAccountType T1
		INNER JOIN RP_Config_RetailerAccountTypes CFG
		ON CFG.AccountType=T1.AccountType
		WHERE 
		CFG.Season=@SEASON AND CFG.Region='EAST' AND 
		T1.RetailerCode = @RetailerCode_AccountCheck  
		
		DELETE #ML_SUMMARY WHERE [IsApplicable] = 'NA' AND @IS_Invigor_Retailer <> 1
	END

	--Added by Vedant to Delete Exceptions Column from Back up Reports for East and West
	-- RP 3234
	DELETE #ML_SUMMARY 
	WHERE MarketLetterCode LIKE 'ML%_E_EXCEPTION'
	OR MarketLetterCode LIKE 'ML%_W_EXCEPTION'

	-- DO NOT INCLUDE ADDITIONAL PROGRAMS COLUMN , IF REWARD ITEM IS NOT FROM ADDITIONAL PROGRAMS (NOT A TRUE MARKET LETTER)
/*
	DELETE T1
	FROM #ML_SUMMARY T1	
	WHERE T1.MarketLetterCode LIKE '%ML%_ADD_PROGRAMS' AND @ProgramCode IN (SELECT ProgramCode FROM RP_CONFIG_ML_Programs)
*/

	DECLARE MARKET_LETTER_LOOP CURSOR FOR 
		SELECT [MarketLetterCode], [Title], [IncludeInActual], [IsApplicable], [Reward] ,[Sequence] FROM #ML_SUMMARY ORDER BY [Sequence]
	OPEN MARKET_LETTER_LOOP
		FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @INCLUDE_IN_ACTUAL, @IS_APPLICABLE, @REWARD, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			
			SET @TOTAL_REWARD += @REWARD
			SET @THEAD_ROW += '<th>' + @MARKETLETTERNAME + '</th>'
			IF @IS_APPLICABLE='NA'
				SET @TBODY_ROW += '<td class="center_align">NA</td>' 				
			ELSE
				SET @TBODY_ROW += '<td class="right_align">' + dbo.SVF_Commify(@REWARD,'$') + '</td>'

			/*
			-- VERSION 2
			SET @TBODY2_ROWS += '<tr>'
			SET @TBODY2_ROWS += '<td>'+ @MARKETLETTERNAME +'</td>'
			IF @IS_APPLICABLE='NA'
				SET @TBODY2_ROWS += '<td class="right_align">NA</td>' 				
			ELSE
				SET @TBODY2_ROWS += '<td class="right_align">' + dbo.SVF_Commify(@REWARD,'$') + '</td>'
			SET @TBODY2_ROWS += '</tr>'
			*/

			FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @INCLUDE_IN_ACTUAL, @IS_APPLICABLE, @REWARD, @SEQUENCE
		END
	CLOSE MARKET_LETTER_LOOP
	DEALLOCATE MARKET_LETTER_LOOP	

	SET @THEAD_ROW += '<th class="mw_80p">TOTAL</th>'
	SET @TBODY_ROW += '<td class="right_align"><b>' + dbo.SVF_Commify(@TOTAL_REWARD,'$') + '</b></td>'

	   
	SET @REWARDSUMMARYSECTION = '
		<div class="st_static_section first">
			<div class="st_header">
				<span>' + @REWARD_SUMMARY + '</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>' + @THEAD_ROW + '</tr><tr>'+ @TBODY_ROW + '</tr>
				</table>
			</div>
		</div>'

/*
	SET @RewardSummarySection += '
		<div class="st_section">
			<div class="st_header">
				<span>REWARD SUMMARY - ALTERNATE VERSION</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>Market Letter Name</th><th>REWARD</th></tr>'					
					+ @TBODY2_ROWS + '					
					<tr class="sub_total_row"><td>TOTAL</td><td class="right_align">' + dbo.SVF_Commify(@TOTAL_REWARD,'$') + '</td></tr>
				</table>
			</div>
		</div>'
*/
	
END
