﻿

CREATE PROCEDURE [Reports].[RP2021Web_Get_W_ClearfieldLentils_Reward] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @STATEMENT_LEVEL AS VARCHAR(20)='';
	DECLARE @REGION AS VARCHAR(4)='';

	Declare @RewardSummarySection nvarchar(max)='';
	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';

	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @RETAILER_NAME NVARCHAR(MAX);
	DECLARE @RETAILER_CITY NVARCHAR(MAX);

	DECLARE @FARMCODE VARCHAR(20);
	DECLARE @GROWER_NAME VARCHAR(200);
		
	DECLARE @RET_MATCHING_HERB_SECTIONS NVARCHAR(MAX)=''
	DECLARE @RET_COMMITMENT_SECTIONS NVARCHAR(MAX)=''
	
	DECLARE @THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @TBODY_ROWS NVARCHAR(MAX)=''
	DECLARE @TOTAL_ROW NVARCHAR(MAX)=''

	DECLARE @RET_SubTotal MONEY=0;

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

			
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @STATEMENT_LEVEL=StatementLevel FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	
	-- GET MATCHING HERBICIDES DATA
	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 	
	SELECT RP.RetailerCode, RP.RetailerName as Retailer_Name
		,RP.MailingCity + IIF(RP.MailingProvince='','',', ' + RP.MailingProvince) as Retailer_City
		,IIF(ISNULL(GC.FirstName + ' '+ GC.lastName,'') = '', GC.CompanyName,  GC.FirstName + ' '+ GC.lastName) as Grower_Name
		, T1.Farmcode
		,IIF(ISNULL(GFI.PIPEDAFarmStatus,'')='','No',GFI.PIPEDAFarmStatus) PIPEDAFarmStatus
		,LentilAcres,HerbAcres
		,MatchedAcres,MatchedSales
		,Reward_Percentage
		,Reward		
	INTO #TEMP
	FROM (			
		SELECT Retailercode
				 ,Farmcode 
				,MAX(IIF(RewardBrand='CLEARFIELD LENTILS',Acres,0)) AS LentilAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD LENTILS',Acres,0)) AS HerbAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD LENTILS',MatchedAcres,0)) AS MatchedAcres
				,SUM(IIF(RewardBrand <> 'CLEARFIELD LENTILS',MatchedSales,0)) AS MatchedSales
				,MAX(Reward_Percentage) AS Reward_Percentage
				,SUM(Reward) AS Reward
			FROM RP2021Web_Rewards_W_ClearfieldLentils_Actual	
			WHERE Statementcode=@STATEMENTCODE 	
			GROUP BY Retailercode, Farmcode 					
		) T1				
		LEFT JOIN RetailerProfile RP	
		ON RP.Retailercode=T1.RetailerCode 
		LEFT JOIN GrowerContacts GC				ON GC.Farmcode = T1.Farmcode AND GC.IsPrimaryFarmContact='Yes'
		LEFT JOIN GrowerFarmInformation GFI		ON GFI.Farmcode = T1.Farmcode
		ORDER BY Retailer_Name, Retailer_City, Grower_Name

	
	-- MATCHING HERBICIDES SECTION
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @RET_MATCHING_HERB_SECTIONS = '<div class="st_section">
				<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 
		GOTO FINAL
	END 
	
	SET @THEAD_ROW='<tr>
		<thead>
			<th>Grower</th>
			<th>Farm ID</th>
			<th>' + @SEASON_STRING +' Eligible Lentil Acres</th>
			<th>' + @SEASON_STRING +' Eligible Herbicide Acres</th>
			<th>' + @SEASON_STRING +' Eligible Matched Acres</th>
			<th>' + @SEASON_STRING + ' Eligible Matched POG $</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>
		</thead>
		</tr>'
		

 	SET @TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
				(select IIF(PIPEDAFarmStatus <> 'YES','Non-PIPEDA',Grower_Name ) AS 'td' for xml path(''), type)
			,(select IIF(PIPEDAFarmStatus <> 'YES','',FarmCode) AS 'td'  for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(LentilAcres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(HerbAcres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedAcres, '')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedSales, '$')  as 'td' for xml path(''), type)							
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
			,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
		FROM #TEMP
		ORDER BY PIPEDAFarmStatus DESC, Grower_Name
		FOR XML PATH('tr')	, ELEMENTS, TYPE))
	/*
	-- TOTAL ROW
	SET @TOTAL_ROW = CONVERT(NVARCHAR(MAX),(SELECT 	'total_row' as [@class]
					,(select 'Total' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select '' AS 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(MatchedSales, '$')  as 'td' for xml path(''), type)							
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
				FROM (
					SELECT SUM(MatchedSales) AS MatchedSales, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward
					FROM #TEMP			
				)  D				
				FOR XML PATH('tr')	, ELEMENTS, TYPE))
	*/


	SET @RET_MATCHING_HERB_SECTIONS = '<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @THEAD_ROW + @TBODY_ROWS  + @TOTAL_ROW + '</table>				
					</div>
				</div>' 

	   	 		
	/* ############################### FINAL OUTPUT ############################### */

FINAL:
		SET @DISCLAIMERTEXT='
		<div class="st_static_section">
			<div class="st_content open">
				<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands: </strong> Odyssey brands and Solo brands.
				</div>
				<div class="rprew_subtabletext"><strong>Qualification Requirements:</strong> Growers must have a valid evergreen Clearfield Lentil Commitment. Clearfield Lentil acres must be registered by July 9, 2021.</div>	
			</div>
		</div>'


	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @RET_MATCHING_HERB_SECTIONS 	 		
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	
	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')

END
