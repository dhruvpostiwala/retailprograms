﻿CREATE PROCEDURE [Reports].[RPWeb_Get_ExposureRewardData] @ID INT
AS
BEGIN
	SET NOCOUNT ON

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP

	SELECT SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region) AS region_reward_amount,
		   SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region) AS region_current_payment,
		   SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region) AS region_previous_payments,
		   SUM(L5.NextPayment) OVER(PARTITION BY ML.Region) AS region_next_payment,
		   --SUM(L5.Claims) OVER(PARTITION BY ML.Region) AS region_claims,
		   --AVG(L5.PaidToDate + L5.CurrentPayment) OVER(PARTITION BY ML.Region) AS region_avg_payment,
		   
		   IIF(ML.Region = 'EAST', 0, SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.RetailerName)) AS retailer_reward_amount,
		   IIF(ML.Region = 'EAST', 0, SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.RetailerName)) AS retailer_current_payment,
		   IIF(ML.Region = 'EAST', 0, SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.RetailerName)) AS retailer_previous_payments,
		   IIF(ML.Region = 'EAST', 0, SUM(L5.NextPayment) OVER(PARTITION BY ML.Region, L5.RetailerName)) AS retailer_next_payments,
		   --SUM(L5.Claims) OVER(PARTITION BY ML.Region, L5.RetailerName) AS retailer_claims,
		   --AVG(L5.PaidToDate + L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.RetailerName) AS retailer_avg_payment,
		   
		   IIF(ML.Region = 'EAST', SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.MarketLetterCode), SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode)) AS ml_reward_amount,
		   IIF(ML.Region = 'EAST', SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode), SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode)) AS ml_current_payment,
		   IIF(ML.Region = 'EAST', SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.MarketLetterCode), SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode)) AS ml_previous_payments,
		   IIF(ML.Region = 'EAST', SUM(L5.NextPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode), SUM(L5.NextPayment) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode)) AS ml_next_payments,
		   --SUM(L5.Claims) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode) AS ml_claims,
		   --AVG(L5.PaidToDate + L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode) AS ml_avg_payment,
		   
		   --SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.MarketLetterCode) AS east_ml_reward_amount,
		   --SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode) AS east_ml_current_payment,
		   --SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.MarketLetterCode) AS east_ml_previous_payments,
		   
		   IIF(ML.Region = 'EAST', SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode), L5.RewardAmount) AS program_reward_amount,
		   IIF(ML.Region = 'EAST', SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode), L5.CurrentPayment) AS program_current_payment, 
		   IIF(ML.Region = 'EAST', SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode), L5.PaidToDate) AS program_previous_payments,
		   IIF(ML.Region = 'EAST', SUM(L5.NextPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode), L5.NextPayment) AS program_next_payments,
		   --L5.Claims AS program_claims, 
		   --AVG (L5.PaidToDate + L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.RetailerName, L5.MarketLetterCode, L5.RewardCode) AS program_avg_payment,
		   
		   --SUM(L5.RewardAmount) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode) AS east_program_reward_amount,
		   --SUM(L5.CurrentPayment) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode) AS east_program_current_payment,
		   --SUM(L5.PaidToDate) OVER(PARTITION BY ML.Region, L5.MarketLetterCode, L5.RewardCode) AS east_program_previous_payments,
		   
		   SUM(L5.RewardAmount) OVER() AS total_reward_amount,
		   SUM(L5.CurrentPayment) OVER() AS total_current_payment,
		   SUM(L5.PaidToDate) OVER() AS total_previous_payments,
		   SUM(L5.NextPayment) OVER() AS total_next_payments,
		   --SUM(L5.Claims) OVER() AS total_claims,
		   --AVG(L5.PaidToDate + L5.CurrentPayment) OVER() AS total_avg_payment,
		   
		   ML.Region AS region, IIF(ML.Region = 'EAST', '', L5.RetailerName) AS retailer_name, ML.MarketLetterCode AS market_letter_code, ML.Title AS market_letter_title, 
		   RSC.RewardLabel AS reward_title, ML.[Sequence], RSC.RewardLabel

	INTO #TEMP
	FROM RPWeb_Reports_Exposure_L5_Summary L5
		INNER JOIN RP_Config_MarketLetters ML			ON L5.MarketLetterCode = ML.MarketLetterCode
		INNER JOIN RP_Config_Rewards_Summary RSC		ON L5.RewardCode=RSC.RewardCode AND RSC.Season=ML.Season 
	WHERE L5.ExposureID = @ID

	SELECT JSON_DATA=(
		SELECT DISTINCT * FROM #TEMP
		ORDER BY Region, retailer_name, [Sequence], RewardLabel
		FOR JSON PATH
	)

END