﻿



CREATE PROCEDURE [Reports].[RP2021_Get_RewardMargins_East] @Statementcode INT
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @TEMP TABLE ( 
		[StatementCode] [int] NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL,		
		[ProgramCode]	[varchar](50) NOT NULL,
		[ChemicalGroup] [varchar](50) NOT NULL,
		[RewardBrand_Sales] MONEY NOT NULL DEFAULT 0,
		[Rewarded_Sales] MONEY NOT NULL DEFAULT 0,		
		[Reward] MONEY NOT NULL DEFAULT 0,		
		[Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0,
		[Overall_Reward_Percentage] NUMERIC(6,4) NOT NULL	DEFAULT 0
	)

	
	DECLARE @INV_ML_CODE AS VARCHAR(50)='ML2021_E_INV'			--InVigor Hybrids
	DECLARE @CPP_ML_CODE AS VARCHAR(50)='ML2021_E_CPP'			--Crop Protection Products

	INSERT @TEMP(Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales)
	
	SELECT	Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, Price_CY
	FROM	(
		SELECT	tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup, TX.GroupType
				,ROW_NUMBER() OVER (PARTITION BY tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup ORDER BY CASE WHEN GroupType IN ('REWARD_BRANDS','ELIGIBLE_BRANDS') THEN 1 ELSE 2 END) AS Priority_Type
				,SUM(tx.Price_CY) Price_CY
		FROM RP2021Web_Sales_Consolidated TX
			INNER JOIN ProductReference PR		ON PR.Productcode = TX.ProductCode 
		WHERE tx.Statementcode=@Statementcode AND tx.GroupType IN ('REWARD_BRANDS', 'ELIGIBLE_BRANDS', 'ALL_EAST_BRANDS') AND tx.ProgramCode <> 'ELIGIBLE_SUMMARY'   -- Eligible brands can work here
		GROUP BY tx.Statementcode, tx.marketlettercode, tx.Programcode, pr.ChemicalGroup, TX.GroupType
	)A
	WHERE	Priority_Type = 1
	
	UPDATE T1
	SET Rewarded_Sales=T2.Rewarded_Sales
		,Reward=T2.Reward
	FROM @TEMP T1
		INNER JOIN (
			
			--Programs
			/*
			[dbo].[RP2021Web_Rewards_E_CustomGroundApplication]
			[dbo].[RP2021Web_Rewards_E_FungicidesSupport]
			[dbo].[RP2021Web_Rewards_E_Ignite_LoyaltySupport] 
			[dbo].[RP2021Web_Rewards_E_PlanningTheBusiness]
			[dbo].[RP2021Web_Rewards_E_POGSalesBonus]
			[dbo].[RP2021Web_Rewards_E_PortfolioSupport]
			[dbo].[RP2021Web_Rewards_E_PursuitSupport]
			[dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
			[dbo].[RP2021Web_Rewards_E_Sollio_Growth_Bonus]
			[dbo].[RP2021Web_Rewards_E_SupplySales]
			*/
					
			-- CUSTOM GROUUND APPLICATION
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[RP2021Web_Rewards_E_CustomGroundApplication] 
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
			
		
			UNION ALL

			-- FUNGICIDE SUPPORT
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , QualBrand_Sales_CY AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_FungicidesSupport]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0

			UNION ALL

			-- PLANNING THE BUSINESS
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_PlanningTheBusiness]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
			
			UNION ALL

			-- POG SALES BONUS
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_POGSalesBonus]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
		
			UNION ALL

			-- PORTFOLIO SUPPORT  
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_PortfolioSupport]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0	
					
			UNION ALL

			-- SALES SUPPORT - LOYALTY BONUS	
			SELECT	Statementcode, MarketLetterCode, Programcode
					,'INVIGOR' AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
			
			UNION ALL

			-- SOLLIO GROWTH 
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_Sollio_Growth_Bonus]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0			
			
			UNION ALL
			
			--SUPPY SALES 
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , RewardBrand_Sales AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_SupplySales]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0

			UNION ALL

			-- IGNITE REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , SUM(RewardBrand_Sales) AS Rewarded_Sales, SUM(Reward) AS Reward
			FROM	[dbo].[RP2021Web_Rewards_E_Ignite_LoyaltySupport]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
			GROUP	BY Statementcode, MarketLetterCode, Programcode, RewardBrand

			UNION ALL
			
			-- LOYALTY SUPPORT REWARD
			SELECT	Statementcode, MarketLetterCode, Programcode
					,RewardBrand AS RewardBrand , QualBrand_Sales_CY AS Rewarded_Sales, Reward 
			FROM	[dbo].[RP2021Web_Rewards_E_PursuitSupport]
			WHERE	Statementcode=@STATEMENTCODE	--AND Reward > 0
			
	) T2
	ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.Programcode=T1.Programcode AND T2.RewardBrand=T1.ChemicalGroup
	 

	UPDATE @TEMP SET [Reward_Percentage]=[Reward]/[Rewarded_Sales]  WHERE [Reward] > 0 AND  [Rewarded_Sales] > 0
	UPDATE @TEMP SET [Overall_Reward_Percentage]=[Reward]/[RewardBrand_Sales] WHERE [Reward] > 0  AND [RewardBrand_Sales] > 0 
	
	SELECT Statementcode, MarketLetterCode, ProgramCode, ChemicalGroup, RewardBrand_Sales, Rewarded_Sales, Reward, Reward_Percentage, [Overall_Reward_Percentage]
	FROM @TEMP
END
