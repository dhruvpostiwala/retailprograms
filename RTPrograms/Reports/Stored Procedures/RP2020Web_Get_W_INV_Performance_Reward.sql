﻿

CREATE   PROCEDURE [Reports].[RP2020Web_Get_W_INV_Performance_Reward] @STATEMENTCODE INT, @ProgramCode varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN
		SET NOCOUNT ON;

	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @RETAILERCODE VARCHAR(50);

	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4)='';
	DECLARE @REGION AS VARCHAR(4)='';
	--DECLARE @HTML_Data as nvarchar(max);
	DECLARE @DisclaimerText as nvarchar(max);
	DECLARE @RewardSummarySection as NVARCHAR(max)='';
	DECLARE @RewardsPercentageMatrix AS NVARCHAR(MAX)='';

	DECLARE @SectionHeader as varchar(200);
	DECLARE @L5CODE AS VARCHAR(10)=''
	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX)='';

	DECLARE @ML_THEAD AS NVARCHAR(MAX)=''
	DECLARE @ML_THEAD_TEMP AS NVARCHAR(MAX)=''
	DECLARE @ML_TBODY AS NVARCHAR(MAX)='';

	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @L5CODE=Level5Code, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	IF NOT OBJECT_ID('tempdb..#ELIGIBLE_MARKETLETTERS') IS NULL DROP TABLE #ELIGIBLE_MARKETLETTERS
	SELECT [MarketLetterCode], [Title], [Sequence] 
	INTO #ELIGIBLE_MARKETLETTERS
	FROM RP_Config_MarketLetters 
	WHERE MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE StatementCode=@STATEMENTCODE AND ProgramCode=@PROGRAMCODE)

	IF NOT OBJECT_ID('tempdb..#TEMP') IS NULL DROP TABLE #TEMP 
	SELECT MarketLetterCode,RewardBrand_Sales,RewardBrand_Qty,Sales_Target,Reward_Percentage,Reward
		,CASE WHEN RewardBrand_Qty > 0 AND Sales_Target = 0 THEN 1
			WHEN RewardBrand_Qty > 0 AND Sales_Target > 0 THEN ROUND(RewardBrand_Qty/Sales_Target,4)
			ELSE 0 END  AS Qualifying_Percentage				
	INTO #TEMP
	FROM RP2020Web_Rewards_W_INV_PERF 
	WHERE STATEMENTCODE=@STATEMENTCODE
	
	-- <th>' + @SEASON_STRING + ' Eligible InVigor<br>(Bags)</th> was <th>' + @SEASON_STRING + ' BASF InVigor<br>(Bags)</th>
	SET @ML_THEAD='
		<thead>
			<tr>
				<th>' + @SEASON_STRING + ' Eligible InVigor<br>(Bags)</th>
				<th>' + CAST(@SEASON AS VARCHAR(4))  + ' BASF InVigor<br>Target (Bags)</th>
				<th>Qualifying %</th>
				<th>' + @SEASON_STRING + ' Eligible POG$ of <brand></th>
				<th>Reward %</th><th>Reward $</th></tr>
		</thead>'


	DECLARE ML_LOOP CURSOR FOR 
		SELECT  [MarketLetterCode],[Title],[Sequence] FROM #ELIGIBLE_MARKETLETTERS  ORDER BY [SEQUENCE]
	OPEN ML_LOOP
	FETCH NEXT FROM ML_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @SECTIONHEADER =  @MARKETLETTERNAME + ' - Reward Summary'
			
			IF NOT EXISTS(SELECT * FROM #TEMP WHERE MarketLetterCode=@MARKETLETTERCODE)
			BEGIN
				SET @ML_SECTIONS += '<div class="st_section">
					<div class = "st_header">
						<span>' + @SECTIONHEADER + '</span>
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						<span class="st_right"</span>
					</div>
					<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>	
					</div>
				</div>' 
	
				GOTO FETCH_NEXT
			END 


			IF @MARKETLETTERCODE='ML2020_W_INV'
				SET @ML_THEAD_TEMP=REPLACE(@ML_THEAD,'<brand>','InVigor')
			ELSE
				SET @ML_THEAD_TEMP=REPLACE(@ML_THEAD,'<brand>','Liberty')
			
			SET @ML_TBODY = CONVERT(NVARCHAR(MAX),(SELECT 	
					(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Qty, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Sales_Target, '')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Qualifying_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
					FROM #TEMP
					WHERE MARKETLETTERCODE=@MARKETLETTERCODE
					FOR XML PATH('tr')	, ELEMENTS, TYPE))

			SET @ML_SECTIONS += '<div class="st_section">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">	' +  @ML_THEAD_TEMP +   @ML_TBODY  + '	</table>
				</div>
			</div>' 

	FETCH_NEXT:	
			FETCH NEXT FROM ML_LOOP INTO  @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE
			END
	CLOSE ML_LOOP
	DEALLOCATE ML_LOOP

/* ############################### FINAL OUTPUT ############################### */
	-- FINAL OUTPUT
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP

SET @SectionHeader = 'REBATE PERCENTAGES'


	IF @L5CODE = 'D0000117' 
	BEGIN 
			SET @RewardsPercentageMatrix = '
			<div class="st_static_section">
				<div class="st_header">
					<span>' + @SectionHeader + '</span>
					<span class="st_right"></span>		
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr><th>' + CAST(@Season AS Varchar(4)) +' POG $(in Bags) relative to InVigor Target</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of Liberty</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of InVigor</th></tr>
						<tr><td style="text-align: center">100%+</td><td style="text-align: center">3.45%</td><td style="text-align: center">3.45%</td></tr>
						<tr><td style="text-align: center">97.5% - 99.9%</td><td style="text-align: center">2.75%</td><td style="text-align: center">2.75%</td></tr>
						<tr><td style="text-align: center">95.0% - 97.49%</td><td style="text-align: center">2.00%</td><td style="text-align: center">2.00%</td></tr>
						<tr><td style="text-align: center">92.5% - 94.99%</td><td style="text-align: center">1.50%</td><td style="text-align: center">1.50%</td></tr>
						<tr><td style="text-align: center">90.0% - 92.49%</td><td style="text-align: center">1.00%</td><td style="text-align: center">1.00%</td></tr>
						<tr><td style="text-align: center">85.0% - 89.99%</td><td style="text-align: center">0.50%</td><td style="text-align: center">0.50%</td></tr>
					</table>		
				</div>
			</div>'	

			SET @DisclaimerText='
				<div class="st_static_section">
				<div class="st_content open">
				<div class="rprew_subtabletext"><b>InVigor Market Letter Qualifying Brands & Reward Brands</b> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
				<div class="rprew_subtabletext"><b>*Canola Crop Protection Market Letter Reward Brands</b> = Liberty Brands.</div>
				<div class="rprew_subtabletext"><span class="highlight_yellow">*Rewards calculated in Suggested Retail Price (SRP)</span></div>
				</div>
				</div>'
	END
	ELSE
	BEGIN
		IF @RETAILERCODE = 'D0000107' OR @RETAILERCODE = 'D0000244' OR @RETAILERCODE = 'D0000137'  --WestLine Companies except Nutrien
				SET @RewardsPercentageMatrix = '
				<div class="st_static_section">
					<div class="st_header">
						<span>' + @SectionHeader + '</span>
						<span class="st_right"></span>		
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					</div>
					<div class="st_content open">
						<table class="rp_report_table">
							<tr><th>' + CAST(@Season AS Varchar(4)) +' POG $(in Bags) relative to InVigor Target</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of Liberty and InVigor</th></tr>
							<tr><td style="text-align: center">100%+</td><td style="text-align: center">5%</td></tr>
							<tr><td style="text-align: center">97.5% - 99.9%</td><td style="text-align: center">4.5%</td></tr>
							<tr><td style="text-align: center">95.0% - 97.49%</td><td style="text-align: center">4%</td></tr>
							<tr><td style="text-align: center">92.5% - 94.99%</td><td style="text-align: center">3%</td></tr>
							<tr><td style="text-align: center">90.0% - 92.49%</td><td style="text-align: center">2%</td></tr>
							<tr><td style="text-align: center">85.0% - 89.99%</td><td style="text-align: center">1%</td></tr>
						</table>		
					</div>
				</div>'

			ELSE IF @RETAILERCODE = 'D520062427'  --WestLine Nutrien
					SET @RewardsPercentageMatrix = '
					<div class="st_static_section">
						<div class="st_header">
							<span>' + @SectionHeader + '</span>
							<span class="st_right"></span>		
							<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
						</div>
						<div class="st_content open">
							<table class="rp_report_table">
								<tr><th>' + CAST(@Season AS Varchar(4)) +' POG $(in Bags) relative to InVigor Target</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of Liberty</th></tr>
								<tr><td style="text-align: center">100%+</td><td style="text-align: center">6%</td></tr>
								<tr><td style="text-align: center">97.5% - 99.9%</td><td style="text-align: center">5.5%</td></tr>
								<tr><td style="text-align: center">95.0% - 97.49%</td><td style="text-align: center">5%</td></tr>
								<tr><td style="text-align: center">92.5% - 94.99%</td><td style="text-align: center">4%</td></tr>
								<tr><td style="text-align: center">90.0% - 92.49%</td><td style="text-align: center">3%</td></tr>
								<tr><td style="text-align: center">85.0% - 89.99%</td><td style="text-align: center">2%</td></tr>
							</table>		
						</div>
					</div>'

			--DeFAULT ACTION
			ELSE
			SET @RewardsPercentageMatrix = '
				<div class="st_static_section">
					<div class="st_header">
						<span>' + @SectionHeader + '</span>
						<span class="st_right"></span>		
						<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					</div>
					<div class="st_content open">
						<table class="rp_report_table">
							<tr><th>' + CAST(@Season AS Varchar(4)) +' POG $(in Bags) relative to InVigor Target</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of Liberty</th><th>Reward on ' + CAST(@Season AS Varchar(4)) + ' POG sales of InVigor</th></tr>
							<tr><td style="text-align: center">100%+</td><td style="text-align: center">4.00%</td><td style="text-align: center">4.00%</td></tr>
							<tr><td style="text-align: center">97.5% - 99.9%</td><td style="text-align: center">3.00%</td><td style="text-align: center">3.50%</td></tr>
							<tr><td style="text-align: center">95.0% - 97.49%</td><td style="text-align: center">2.00%</td><td style="text-align: center">3.00%</td></tr>
							<tr><td style="text-align: center">92.5% - 94.99%</td><td style="text-align: center">1.50%</td><td style="text-align: center">2.00%</td></tr>
							<tr><td style="text-align: center">90.0% - 92.49%</td><td style="text-align: center">1.00%</td><td style="text-align: center">1.50%</td></tr>
							<tr><td style="text-align: center">85.0% - 89.99%</td><td style="text-align: center">0.50%</td><td style="text-align: center">1.00%</td></tr>
						</table>		
					</div>
				</div>'	

			IF @RETAILERCODE in ('D0000137', 'D0000107', 'D0000244', 'D520062427')
				SET @DisclaimerText='
				<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext"><b>*InVigor Qualifying Brands & Reward Brands</b> = InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
						<div class="rprew_subtabletext"><b>*Canola Crop Protection Reward Brands </b>= Liberty brands.</div>
					</div>
				</div>'
			ELSE
				SET @DisclaimerText='
				<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext"><b>*InVigor Qualifying Brands & Reward Brands</b> =  InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
						<div class="rprew_subtabletext"><b>*Canola Crop Protection Reward Brands </b>= Liberty 150</div>
					</div>
				</div>'

	END


	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	
	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS	
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END
