﻿

CREATE   PROCEDURE [Reports].[RPWeb_Get_ML_RewardSummary] @STATEMENTCODE INT, @LANGUAGE AS VARCHAR(1) = 'E', @HTML_DATA VARCHAR(MAX)=NULL OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4)='';
	DECLARE @REGION VARCHAR(4)='';
	DECLARE @DATASETCODE_L5 VARCHAR(20);

	DECLARE @THEAD nvarchar(max) = '';
	DECLARE @MARKET_LETTER_CODE varchar(200) = '';
	DECLARE @RETAILERCODE VARCHAR(10) = '';
	DECLARE @VERSIONTYPE VARCHAR(50) = '';
	DECLARE @MARKETLETTER_TITLE VARCHAR(200)='';
	

	DECLARE @ML_SECTIONS nvarchar(max)='';

	DECLARE @TITLE varchar(200) = '';
	DECLARE @TOTAL_ROW nvarchar(max) = '';
	DECLARE @DETAILS_ROWS nvarchar(max) = '';
	DECLARE @SEQUENCE INT;
	
	DECLARE @CLASS_FIRST_SECTION VARCHAR(5)='';
	DECLARE @SECTIONHEADER Varchar(200);

	DECLARE @SKU_LEVEL_SALES AS UDT_RP_SKU_LEVEL_SALES;

	DECLARE @TOTAL_REWARD MONEY=0;
	DECLARE @TOTAL_POG_AMOUNT MONEY=0;
	DECLARE @REARD_MARGIN MONEY=0;

	DECLARE @ML_PROG_SALES TABLE(
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrands_SalesAmount MONEY NOT NULL
	)

	DECLARE @ADDITIONAL_ML_CODE VARCHAR(50)='' 

	DECLARE @SQL_COMMAND nvarchar (max);

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	
	SET @HTML_DATA='';
		   	  
	SELECT @SEASON=Season, @REGION=Region, @RETAILERCODE=RetailerCode, @VERSIONTYPE=VersionType
		,@DATASETCODE_L5=CASE WHEN VersionType = 'Locked' THEN SRC_StatementCode ELSE StatementCode END
	FROM RPWeb_Statements 
	WHERE StatementCode=@STATEMENTCODE
	
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))
	
	SET @ADDITIONAL_ML_CODE='ML'+ @SEASON_STRING + '_' + LEFT(@REGION,1) + '_ADD_PROGRAMS'

	INSERT @SKU_LEVEL_SALES
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE
	
	-- MAREKET LEVEL POG SALES		
	DROP TABLE IF EXISTS #ML_POG_Sales 
	SELECT MarketLetterCode
		,SUM([CYSales]) AS [Actual_Amount]
		,SUM(CYEligibleSales) AS [Eligible_Amount]
	INTO #ML_POG_Sales
	FROM @SKU_LEVEL_SALES
	GROUP BY MarketLetterCode
	   	   

	DROP TABLE IF EXISTS #TARGET_STATEMENTS	
	SELECT StatementCode
	INTO #TARGET_STATEMENTS
	FROM (
		SELECT @STATEMENTCODE AS Statementcode

			UNION

		SELECT StatementCode 
		FROM RPWeb_Statements 
		WHERE [Status]='Active' AND  DataSetCode_L5=@DATASETCODE_L5
	) D

	
	-- Market Letter + ProgramCode ,Reward brand sales
	IF @REGION='EAST'
		SET @SQL_COMMAND='
/*
		SELECT MarketLetterCode, ProgramCode, SUM(Price_CY) AS [RewardBrands_SalesAmount]
		FROM RP' + @SEASON_STRING + 'Web_Sales_Consolidated
		WHERE ProgramCode <> ''ELIGIBLE_SUMMARY'' AND  GroupType IN (''REWARD_BRANDS'',''ELIGIBLE_BRANDS'') AND ([Statementcode]=@Statementcode OR DataSetCode_L5=@DATASETCODE_L5)
		GROUP BY MarketLetterCode, ProgramCode  
*/		
		SELECT	MarketLetterCode, ProgramCode, [RewardBrands_SalesAmount]
		FROM	(
			SELECT	MarketLetterCode, ProgramCode
					,ROW_NUMBER() OVER (PARTITION BY MarketLetterCode, ProgramCode ORDER BY CASE WHEN GroupType IN (''REWARD_BRANDS'',''ELIGIBLE_BRANDS'') THEN 1 ELSE 2 END) AS Priority_Type
					,SUM(Price_CY) AS [RewardBrands_SalesAmount]
			FROM RP' + @SEASON_STRING + 'Web_Sales_Consolidated
			WHERE ProgramCode <> ''ELIGIBLE_SUMMARY'' AND  GroupType IN (''REWARD_BRANDS'',''ELIGIBLE_BRANDS'', ''ALL_EAST_BRANDS'') AND ([Statementcode]=@Statementcode OR DataSetCode_L5=@DATASETCODE_L5)
			GROUP BY MarketLetterCode, ProgramCode, GroupType
		)A
		WHERE Priority_Type = 1   
		
		'

	ELSE
		SET @SQL_COMMAND='
/*		
		SELECT MarketLetterCode, ProgramCode, SUM(Price_CY) AS [RewardBrands_SalesAmount]
		FROM RP' + @SEASON_STRING + 'Web_Sales_Consolidated
		WHERE GroupType=''REWARD_BRANDS'' AND ([Statementcode]=@Statementcode OR DataSetCode_L5=@DATASETCODE_L5)
		GROUP BY MarketLetterCode, ProgramCode  
*/
		SELECT	MarketLetterCode, ProgramCode, [RewardBrands_SalesAmount]
		FROM	(
			SELECT	MarketLetterCode, ProgramCode
					,ROW_NUMBER() OVER (PARTITION BY MarketLetterCode, ProgramCode ORDER BY CASE WHEN GroupType = ''REWARD_BRANDS'' THEN 1 ELSE 2 END) AS Priority_Type
					,SUM(Price_CY) AS [RewardBrands_SalesAmount]
			FROM RP' + @SEASON_STRING + 'Web_Sales_Consolidated
			WHERE ProgramCode <> ''ELIGIBLE_SUMMARY'' AND  GroupType IN (''REWARD_BRANDS'', ''ALL_CPP_BRANDS'') AND ([Statementcode]=@Statementcode OR DataSetCode_L5=@DATASETCODE_L5)
			GROUP BY MarketLetterCode, ProgramCode, GroupType
		)A
		WHERE Priority_Type = 1  
		
		'

	
	INSERT @ML_PROG_SALES
	EXEC SP_EXECUTESQL @SQL_Command, N'@STATEMENTCODE INT, @DATASETCODE_L5 INT',  @STATEMENTCODE, @DATASETCODE_L5

	-- REWARD SUMMARY 
	DROP TABLE IF EXISTS #REWARD_SUMMARY
	SELECT ml.[Sequence]
		,CASE WHEN @LANGUAGE = 'F' THEN ML.Title_FR ELSE ML.Title END as ML_Title
		, ml.MarketLetterCode
		,CASE WHEN @LANGUAGE = 'F' THEN ISNULL(prog.Title_FR, Prog.Title) ELSE crs.RewardLabel END as RewardLabel
		,ISNULL(crs.ShowRewardMargin,'No') as ShowRewardMargin
		,CAST(rs.TotalReward AS DECIMAL(18,2)) AS TotalReward
		,CAST(rs.PaidToDate AS DECIMAL(18,2)) AS PaidToDate
		,CAST(rs.CurrentPayment AS DECIMAL(18,2)) AS CurrentPayment
		,CAST(rs.NextPayment AS DECIMAL(18,2)) AS NextPayment
		,ISNULL(TX.RewardBrands_SalesAmount,0) AS [RewardBrands_SalesAmount]
	INTO #REWARD_SUMMARY
	FROM (
		SELECT StatementCode,MarketLetterCode,RewardCode			
			,SUM(TotalReward) TotalReward
			,SUM(PaidToDate) PaidToDate
			,SUM(CurrentPayment) CurrentPayment
			,SUM(NextPayment) NextPayment
			,SUM(PaymentAmount) PaymentAmount
		FROM (
			SELECT StatementCode,MarketLetterCode
				,IIF(@SEASON <= 2020, RewardCode, REPLACE(REPLACE(RewardCode,'_WX','_W'),'_EX','_E')) AS RewardCode
				,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount
			FROM RPWeb_Rewards_Summary 
			WHERE StatementCode=@StatementCode		
				AND (RewardEligibility='Eligible' OR PaidToDate <> 0 OR CurrentPayment <> 0)
		)RS0
		GROUP BY StatementCode,MarketLetterCode,RewardCode

	) RS
	--	INNER JOIN #TARGET_STATEMENTS TGT			ON TGT.Statementcode=RS.Statementcode
		INNER JOIN RP_Config_Rewards_Summary CRS	ON CRS.rewardcode=RS.rewardcode and crs.season=@SEASON
		INNER JOIN RP_Config_MarketLetters ML		ON ML.MarketLetterCode=RS.MarketLetterCode 
		INNER JOIN RP_Config_Programs	Prog		ON Prog.Rewardcode=RS.RewardCode AND Prog.Season=@SEASON	
		INNER JOIN (
			SELECT DISTINCT ELP.MarketLetterCode 
			FROM RPWEB_ML_ElG_Programs	 ELP
				INNER JOIN #TARGET_STATEMENTS TS
				ON TS.Statementcode=ELP.Statementcode

					UNION

			SELECT @ADDITIONAL_ML_CODE AS MarketLetterCode

		) MLP 
		ON MLP.MarketLetterCode=ML.MarketLetterCode		
		
		LEFT JOIN (
			SELECT MarketLetterCode, ProgramCode, RewardBrands_SalesAmount 
			FROM @ML_PROG_SALES
		) TX
		ON TX.MarketLetterCode=RS.MarketLetterCode AND TX.ProgramCode=Prog.Programcode	
	--WHERE (RS.RewardEligibility='Eligible' OR RS.PaidToDate <> 0 OR RS.CurrentPayment <> 0)	
	
--	DELETE FROM #REWARD_SUMMARY WHERE MarketLetterCode=@ADDITIONAL_ML_CODE AND RewardBrands_SalesAmount <= 0

	IF @REGION='WEST' -- AND @SEASON=2020
	BEGIN
		DELETE T1
		FROM #REWARD_SUMMARY  T1
			LEFT JOIN (
				SELECT SUM(Price_SDP) AS Price_SDP
				FROM RPWeb_Transactions 
				WHERE StatementCode=@STATEMENTCODE AND BASFSeason=@SEASON AND ChemicalGroup='LIBERTY 200'				
				HAVING SUM(Price_SDP) > 0
			) T2
			ON 1=1
		WHERE T1.MarketLetterCode='ML'+@SEASON_STRING+'_W_LIBERTY_200' AND ISNULL(T2.Price_SDP,0) <= 0
	END 
	
	--https://kennahome.jira.com/browse/RP-2941
	--IF @REGION = 'EAST' 
	--BEGIN
	--	DELETE T1
	--	FROM #REWARD_SUMMARY  T1
	--	WHERE T1.MarketLetterCode='ML' + @SEASON_STRING + '_E_INV' AND ISNULL(T1.TotalReward,0) <= 0
	--END

	IF @LANGUAGE = 'F'
		SET @THEAD='
			<tr>
				<th>Récompenses prévues dans la lettre de commercialisation</th>
				<th style="width: 80px">Récompense (%)</th>
				<th style="width: 80px">Récompense totale</th>
				<th style="width: 110px">Paiement antérieur</th>
				<th style="width: 110px">Paiement actuel</th>' +
				IIF(@RETAILERCODE = 'D0000137' OR @VERSIONTYPE IN ('Locked','Archive'), '', '<th style="width: 80px">Prochain paiement</th>') +
			'</tr>'
	ELSE
		SET @THEAD='
			<tr>
				<th>Market Letter Rewards</th>
				<th style="width: 80px">Reward %</th>
				<th style="width: 80px">Total Reward</th>				
				<th style="width: 110px">Previous Payment</th>
				<th style="width: 110px">Current Payment</th>' +
				IIF(@RETAILERCODE = 'D0000137' OR @VERSIONTYPE IN ('Locked','Archive'), '', '<th style="width: 80px">Next Payment</th>') +
			'</tr>'

	DROP TABLE IF EXISTS #ML_REWARD_SUMMARY 
	SET @CLASS_FIRST_SECTION='first'
	DECLARE SUMMARY_DETAILS_LOOP CURSOR FOR 
		SELECT DISTINCT [MarketLetterCode], [ML_Title], [Sequence] FROM #REWARD_SUMMARY ORDER BY [Sequence]
	OPEN SUMMARY_DETAILS_LOOP
	FETCH NEXT FROM SUMMARY_DETAILS_LOOP INTO @MARKET_LETTER_CODE, @MARKETLETTER_TITLE, @SEQUENCE
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SELECT T1.ML_Title ,T1.MarketLetterCode ,T1.RewardLabel ,T1.ShowRewardMargin ,T1.TotalReward ,T1.PaidToDate ,T1.CurrentPayment ,T1.NextPayment, T1.RewardBrands_SalesAmount
				,SUM(TotalReward) OVER() AS [ML_Reward]				
			INTO #ML_REWARD_SUMMARY
			FROM #REWARD_SUMMARY T1
			WHERE T1.MarketLetterCode=@MARKET_LETTER_CODE

			SELECT @DETAILS_ROWS = CONVERT(NVARCHAR(MAX),(SELECT	
									(SELECT  '' as [td/@class], RewardLabel as 'td' for xml path(''), type)							
									,IIF(@MARKET_LETTER_CODE = @ADDITIONAL_ML_CODE, NULL, (SELECT 'right_align' as [td/@class] ,IIF(MAX(ShowRewardMargin)='No','',dbo.SVF_Commify(MAX(RewardMargin),@PERCENT_FORMAT)) as 'td' for xml path(''), type))									
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(TotalReward),@CURRENCY_FORMAT)  as 'td' for xml path(''), type)									
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(PaidToDate),@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(CurrentPayment),@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,IIF(@RETAILERCODE = 'D0000137' OR @VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(SUM(NextPayment),@CURRENCY_FORMAT) as 'td' for xml path(''), type))
								FROM (
									SELECT *
										,CAST(IIF(RewardBrands_SalesAmount=0,0,TotalReward/RewardBrands_SalesAmount) AS decimal(6,4)) AS RewardMargin										
									FROM #ML_REWARD_SUMMARY 
								) D
								GROUP BY RewardLabel--, ShowRewardMargin
								FOR XML PATH('tr'), ELEMENTS, TYPE))			

			SELECT @TOTAL_ROW = CONVERT(NVARCHAR(MAX), (SELECT 'total_row ' as [@class]
								,(SELECT 'TOTAL' AS 'td' for xml path(''), type) 								
								,IIF(@MARKET_LETTER_CODE = @ADDITIONAL_ML_CODE, NULL, (SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(IIF(POG_Amount=0,0,TotalReward/POG_Amount),@PERCENT_FORMAT)  as 'td' for xml path(''), type))
								,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
								,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,IIF(@RETAILERCODE = 'D0000137' OR @VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
									FROM (
										SELECT SUM(TotalReward) AS [TotalReward]
											,SUM(PaidToDate) AS [PaidToDate]
											,SUM(CurrentPayment) AS [CurrentPayment]
											,SUM(NextPayment) AS [NextPayment]
											,MAX(T2.Eligible_Amount) AS POG_Amount
										FROM #ML_REWARD_SUMMARY	 T1
											LEFT JOIN #ML_POG_Sales T2					
											ON T2.MarketLetterCode=T1.MarketLetterCode
									) D 
								FOR XML PATH('tr'), ELEMENTS, TYPE))
			
			SET @SECTIONHEADER = @MARKETLETTER_TITLE;

			IF @MARKET_LETTER_CODE = @ADDITIONAL_ML_CODE AND @LANGUAGE <> 'F'
			SET @THEAD='
					<tr>
						<th>Market Letter Rewards</th>
						<th style="width: 80px">Total Reward</th>
						<th style="width: 110px">Previous Payment</th>
						<th style="width: 110px">Current Payment</th>' +
						IIF(@RETAILERCODE = 'D0000137' OR @VERSIONTYPE IN ('Locked','Archive'), '', '<th style="width: 80px">Next Payment</th>') +
					'</tr>'
					
			
			SET @ML_SECTIONS += '<div class="st_section ' + @CLASS_FIRST_SECTION + '">
				<div class = "st_header">
					<span>' + @SECTIONHEADER + ' - ' + CASE WHEN @LANGUAGE = 'F' THEN 'Sommaire des récompenses' ELSE 'Reward Summary' END + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">' + IIF(@SECTIONHEADER='Additional Programs', REPLACE(@THEAD,'Market','Non Market') ,@THEAD) + @DETAILS_ROWS + @TOTAL_ROW + '
					</table>
				</div>
			</div>' 

			SET @CLASS_FIRST_SECTION=''
			DROP TABLE IF EXISTS #ML_REWARD_SUMMARY 
			FETCH NEXT FROM SUMMARY_DETAILS_LOOP INTO @MARKET_LETTER_CODE, @MARKETLETTER_TITLE, @SEQUENCE
		END
	CLOSE SUMMARY_DETAILS_LOOP
	DEALLOCATE SUMMARY_DETAILS_LOOP
	
	-- NEW STORED PROCEDURE , SHOULD ACCEPT SEASON AS PARAM
	-- CHECK IN RPWEB_REWARDS_SUMMARY
	
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>' + CASE WHEN @LANGUAGE = 'F' THEN 'SOMMAIRE DE LA LETTRE DE COMMERCIALISATION' ELSE 'MARKET LETTER SUMMARY' END + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')

	IF @SEASON <= 2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE 
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')

	
	--SELECT @HTML_DATA AS [data]
	
	IF(NOT(OBJECT_ID('tempdb..#REWARD_SUMMARY') IS NULL)) DROP TABLE #REWARD_SUMMARY --Drop the temp table incase it exists
	IF(NOT(OBJECT_ID('tempdb..#ML_REWARD_SUMMARY') IS NULL)) DROP TABLE #ML_REWARD_SUMMARY --Drop the temp table incase it exists
END
