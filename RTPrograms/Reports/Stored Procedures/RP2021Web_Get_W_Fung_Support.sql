﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_Fung_Support] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @CARAMBA_REWARD NUMERIC(18,2) = 10.00;
	DECLARE @COTEGRA_REWARD NUMERIC(18,2) = 20.00;
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	/* ############################### PRODUCT DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Product</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG QTY</th>
			<th>Reward per Unit</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = ''; 

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select ProductName AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_QTY, '')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Per_Quantity, '$')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM RP2021Web_Rewards_W_Fung_Support 
	WHERE Statementcode=@STATEMENTCODE 
	ORDER BY RewardBrand, Reward_Per_Quantity 
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 

	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">				
				<strong>Qualifying and Reward Brands:</strong> Caramba and Cotegra.
			</div>			
		</div>
	 </div>'	
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

