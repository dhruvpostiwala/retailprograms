﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_PaymentOnTime] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(2000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @NUMDOLLARSPERJUG_LIBCENT INTEGER = 4;
	DECLARE @NUMDOLLARSPERJUG_LIBCENTFACET INTEGER = 2;
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--FETCH REWARD DISPLAY VALUES
	IF(NOT(OBJECT_ID('tempdb..#DETAILS') IS NULL)) DROP TABLE #DETAILS
	SELECT	ML.Title,
			SUM(T1.RewardBrand_Sales) AS RewardBrand_Sales,
			MAX(T1.Reward_Percentage) AS Reward_Percentage,
			SUM(T1.Reward) AS Reward
	INTO #DETAILS
	FROM RP2021Web_Rewards_W_PaymentOnTime T1
	INNER JOIN RP_Config_MarketLetters ML ON T1.MarketLetterCode = ML.MarketLetterCode
	WHERE T1.StatementCode = @STATEMENTCODE
	GROUP BY T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, ML.Title


	
	/* ############################### GROWER DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Market Letter</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select Title AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)										
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY Title
	FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 

	
	
	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> Eligible 2021 Crop Protection and InVigor Reward Brands.
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

