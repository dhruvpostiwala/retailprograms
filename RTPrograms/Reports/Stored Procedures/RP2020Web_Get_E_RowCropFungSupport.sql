﻿CREATE PROCEDURE [Reports].[RP2020Web_Get_E_RowCropFungSupport] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @STATEMENTLEVEL AS VARCHAR(10) = '';
	DECLARE @MARKETLETTERCODE AS VARCHAR(50) = 'ML2020_E_CCP'
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD2 AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY2 AS NVARCHAR(MAX) = '';
	DECLARE @REWARDCONDITIONTEXT AS VARCHAR(300) = '';
	DECLARE @REWARDRATETABLE AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_TBODY AS NVARCHAR(MAX) = '';

	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @IS_OU INT;

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE;
	SELECT @IS_OU = DatasetCode, @SEASON=Season, @STATEMENTLEVEL=StatementLevel FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;
	
	IF(NOT(OBJECT_ID('tempdb..#TEMP') IS NULL)) DROP TABLE #TEMP 	

	SELECT *,
	 SUM(RewardBrand_Acres)	 OVER(PARTITION  BY StatementCode) AS  RewardBrand_Acres_SUM
	,SUM(QualBrand_Acres_CY) OVER(PARTITION  BY StatementCode) AS QualBrand_Acres_CY_SUM
	,SUM(QualBrand_Acres_LY) OVER(PARTITION  BY StatementCode) AS QualBrand_Acres_LY_SUM
	,SUM(QualBrand_Sales_CY) OVER(PARTITION  BY StatementCode) AS QualBrand_Sales_CY_SUM
	,SUM(QualBrand_Sales_LY) OVER(PARTITION  BY StatementCode) AS QualBrand_Sales_LY_SUM
	,MAX(Reward_Amount)		 OVER(PARTITION  BY StatementCode) AS Reward_Amount_Tier
	,SUM(Reward)			 OVER(PARTITION  BY StatementCode) AS Reward_SUM
	INTO #TEMP
	FROM  RP2020Web_Rewards_E_RowCropFungSupport
	WHERE STATEMENTCODE=@STATEMENTCODE


	
	/*
	--FOR OU QUALIFYING AMOUNT
	IF(NOT(OBJECT_ID('tempdb..#TEMP_OU') IS NULL)) DROP TABLE #TEMP_OU 	
	Select SUM(QualBrand_Acres_CY) QualBrand_Acres_CY,SUM(QualBrand_Acres_LY) QualBrand_Acres_LY
	INTO 
	#TEMP_OU
	FROM [dbo].RP2020Web_Rewards_E_RowCropFungSupport WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT  @QUAL_PERCENTAGE = IIF(QualBrand_Acres_LY > 0, QualBrand_Acres_CY / QualBrand_Acres_LY, IIF(QualBrand_Acres_CY > 0, 9.9999, 0)) FROM #TEMP_OU
	*/

	--RP 2929
	--FOR OU QUALIFYING AMOUNT
	IF(NOT(OBJECT_ID('tempdb..#TEMP_OU') IS NULL)) DROP TABLE #TEMP_OU 	
	Select SUM(QualBrand_Sales_CY) QualBrand_Sales_CY,SUM(QualBrand_Sales_LY) QualBrand_Sales_LY
	INTO 
	#TEMP_OU
	FROM [dbo].RP2020Web_Rewards_E_RowCropFungSupport WHERE StatementCode =  (SELECT IIF(DataSetCode = 0, StatementCode, DatasetCode) Code  from RPWeb_Statements WHERE StatementCode = @STATEMENTCODE) 
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	SELECT  @QUAL_PERCENTAGE = IIF(QualBrand_Sales_LY > 0, QualBrand_Sales_CY / QualBrand_Sales_LY, IIF(QualBrand_Sales_CY > 0, 9.9999, 0)) FROM #TEMP_OU
	SELECT  @QUAL_PERCENTAGE = IIF(@QUAL_PERCENTAGE > 9.9999, 9.9999, @QUAL_PERCENTAGE)

	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
							<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 	
		GOTO FINAL
	END


	-- Create a THEAD for the first table
	IF @LANGUAGE = 'F'
	BEGIN
		SET @ML_THEAD1 = '
		<thead>
			<tr>
				<th>Produits</th>
				<th>VAS réalisées ' + CAST(@SEASON-1 AS VARCHAR(4)) + ' ($)</th>
				<th>VAS réalisées ' + CAST(@SEASON AS VARCHAR(4)) + ' ($)</th>
				<th>VAS admissibles ' + CAST(@SEASON AS VARCHAR(4)) + ' (acres)</th>
			</tr>
		</thead>
		';

		-- Create a THEAD for the second table
		SET @ML_THEAD2 = '
		<thead>
			<tr>
				<th></th>
				<th>% admissible<OU></th>
				<th>VAS admissibles ' + CAST(@SEASON AS VARCHAR(4)) + ' (acres)</th>
				<th>$/acre</th>
				<th>Récompense ($)</th>
			</tr>
		</thead>
		'
	END
	ELSE
	BEGIN
		SET @ML_THEAD1 = '
		<thead>
			<tr>
				<th>Products</th>
				<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Actual POG $</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Actual POG $</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG Acres</th>
			</tr>
		</thead>
		';

		-- Create a THEAD for the second table
		SET @ML_THEAD2 = '
		<thead>
			<tr>
				<th></th>
				<th>Qualifying<OU> %</th>
				<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG Acres</th>
				<th>$/Acre</th>
				<th>Reward $</th>
			</tr>
		</thead>
		'
	END
		IF @IS_OU = 0
		BEGIN
			SET @ML_THEAD2 = REPLACE(@ML_THEAD2,'<OU>','')
		END
		ELSE 
		BEGIN
			SET @ML_THEAD2 = REPLACE(@ML_THEAD2,'<OU>',' OU')	
		END
			

		-- Use FOR XML PATH to generate HTML from the data from the table
		SET @ML_TBODY1 = CONVERT(NVARCHAR(MAX),(SELECT
					(select RewardBrand as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(QualBrand_Sales_LY,0), '$') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(QualBrand_Sales_CY,0), '$') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Acres,'') as 'td' for xml path(''), type)		
				FROM #TEMP
				FOR XML PATH('tr')	, ELEMENTS, TYPE));
		
		-- Use FOR XML PATH to generate HTML for the totals from the data from the table
		
		SET @ML_TBODY1 += CONVERT(NVARCHAR(MAX),(SELECT top 1	
					(select 'rp_bold' as [td/@class] ,'Total' as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_LY_SUM, '$') as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_CY_SUM, '$') as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Acres_SUM,'') as 'td' for xml path(''), type)
				FROM #TEMP 
				FOR XML PATH('tr')	, ELEMENTS, TYPE));

		SET @ML_TBODY2 = CONVERT(NVARCHAR(MAX),(SELECT top 1	
					(select 'rp_bold' as [td/@class] ,'Total' as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(@QUAL_PERCENTAGE, '%') as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Acres_SUM,'') as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Amount_Tier, '$') as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(Reward_SUM, '$') as 'td' for xml path(''), type)
				FROM #TEMP
				FOR XML PATH('tr'), ELEMENTS, TYPE));
	
		--Generate the HTML for the section if there are transactions
		SET @ML_SECTIONS += '<div class="st_section">
								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD1 +   @ML_TBODY1  + '
									</table>
								</div>

								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD2 +   @ML_TBODY2  + '
									</table>
								</div>
							</div>';
	

	FINAL:	
	-- Set the reward condition text
	IF @LANGUAGE = 'F'
	BEGIN
		IF  @IS_OU = 0
		BEGIN
			SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
					*Pour se qualifier pour cette récompense, les détaillants doivent déclarer plus de 2 000 acres de VAS de produits fongicides récompensés en 2020 et réaliser au moins 100 % des VAS ($) réalisés en 2019.
				</div>
			</div>
			';
		END
		ELSE
		BEGIN
			SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
					*Pour se qualifier pour cette récompense, les détaillants doivent déclarer plus de 2 000 acres de VAS de produits fongicides récompensés en 2020 et réaliser au moins 100 % des VAS ($) réalisés en 2019.
					% Éligible basé sur les totaux des unités opérationnelles
				</div>
			</div>
			';
		END
		-- Set the reward rate table
		SET @REWARDRATETABLE_THEAD = '
		<thead>
			<tr>
				<th>% des VAS ' + CAST(@SEASON-1 AS VARCHAR(4)) + '</th>
				<th>Récompense</th>
			</tr>
		</thead>
		';

		-- Get application rates HTML
		SET @APPLICATIONRATESTABLE_THEAD = 
		'
		<thead>
			<tr>
				<th>Produits</th>
				<th>Dose (acres/boîte)</th>
			</tr>
		</thead>
		';
	END
	ELSE
	BEGIN
	IF  @IS_OU = 0
		BEGIN
			SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
					Retailers must exceed 2,000 acres of 2020 POG Sales of Fungicide Reward Brands and maintain 100% or more of 2019 Actual POG $ to be eligible for this reward.
				</div>
			</div>
			';
		END
	ELSE
		BEGIN
			SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
					Retailers must exceed 2,000 acres of 2020 POG Sales of Fungicide Reward Brands and maintain 100% or more of 2019 Actual POG $ to be eligible for this reward.<br/>
					Qualifying % based on Operating Unit Totals
				</div>
			</div>
			';
		END
		
		-- Set the reward rate table
		SET @REWARDRATETABLE_THEAD = '
		<thead>
			<tr>
				<th>% of ' + CAST(@SEASON-1 AS VARCHAR(4)) + ' POG Sales</th>
				<th>Reward</th>
			</tr>
		</thead>
		';

		-- Get application rates HTML
		SET @APPLICATIONRATESTABLE_THEAD = 
		'
		<thead>
			<tr>
				<th>Products</th>
				<th>Acres/Case Application Rate</th>
			</tr>
		</thead>
		';
	END

	SET @REWARDRATETABLE_TBODY = '
	<tbody>
		<tr>
			<td>100% to 109.99%</td><td>$1.25/acre</td>
		</tr>
		<tr>
			<td>110%+</td><td>$1.75/acre</td>
		</tr>
	</tbody>
	';

	SET @REWARDRATETABLE = '
	<div class="st_static_section">
		<div class = "st_header">
			' + CASE WHEN @LANGUAGE = 'F' THEN '<span>POURCENTAGES DE RÉCOMPENSES</span>'
			ELSE '<span>REWARD RATES</span>' END + '
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"></span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				' + @REWARDRATETABLE_THEAD + @REWARDRATETABLE_TBODY + '
			</table>
		</div>
	</div>
	';

	SET @APPLICATIONRATESTABLE_TBODY =
	'
	<tbody>
		<tr>
			<td>Caramba</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Cotegra</td><td>70 Acres</td>
		</tr>
		<tr>
			<td>Headline AMP</td><td>40 Acres</td>
		</tr>
		<tr>
			<td>Headline</td><td>80 Acres</td>
		</tr>
		<tr>
			<td>Priaxor</td><td>160 Acres</td>
		</tr>
		<tr>
			<td>Twinline</td><td>80 Acres</td>
		</tr>
	</tbody>
	'

	SET @APPLICATIONRATESTABLE = 
	'
	<div class="st_static_section">
		<div class = "st_header">' + 
			CASE WHEN @LANGUAGE = 'F' THEN '<span>TAUX D''APPLICATION</span>'
			ELSE '<span>APPLICATION RATES</span>' END + '
			<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			<span class="st_right"></span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">
				' + @APPLICATIONRATESTABLE_THEAD + '
				' + @APPLICATIONRATESTABLE_TBODY + '
			</table>
		</div>
	</div>
	'

	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @REWARDCONDITIONTEXT;
	SET @HTML_Data += @REWARDRATETABLE;
	SET @HTML_Data += @APPLICATIONRATESTABLE;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json');
END
