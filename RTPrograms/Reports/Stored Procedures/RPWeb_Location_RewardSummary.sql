﻿CREATE PROCEDURE [Reports].[RPWeb_Location_RewardSummary] @STATEMENTCODE INT, @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON INT 
	DECLARE @DATASETCODE VARCHAR(20);

	DECLARE @THEAD NVARCHAR(MAX);
	DECLARE @TBODY NVARCHAR(MAX);

/*	
	SET @DATASETCODE = (
		SELECT 
			CASE
				WHEN VersionType = 'Locked'
					THEN SRC_StatementCode
				ELSE StatementCode
			END
		FROM RPWeb_Statements
		WHERE StatementCode = @STATEMENTCODE
	);
*/
 
	SELECT @SEASON = Season
		,@DATASETCODE =IIF(VersionType = 'Locked', SRC_StatementCode, StatementCode)			
	FROM RPWeb_Statements
	WHERE StatementCode = @STATEMENTCODE
		

	SET @THEAD = '<tr><th>Retailer Name</th><th>Level 2 Name</th><th>Total Reward</th><th>Current Payment</th>';

	SET @TBODY = CONVERT(NVARCHAR(MAX), (
		SELECT
			(SELECT RP.RetailerName + ' (' + RP.RetailerCode + ')' [td] FOR XML PATH(''), TYPE),
			(SELECT ISNULL(RP2.RetailerName, '') [td] FOR XML PATH(''), TYPE),
			(SELECT 'right_align' [td/@class], dbo.SVF_Commify(SUM(RS.TotalReward), '$') [td] FOR XML PATH(''), TYPE),
			(SELECT 'right_align' [td/@class], dbo.SVF_Commify(SUM(RS.CurrentPayment), '$') [td] FOR XML PATH(''), TYPE)
		FROM RPWeb_Rewards_Summary RS
			INNER JOIN RPWeb_Statements ST
			ON RS.StatementCode = ST.StatementCode
			INNER JOIN RetailerProfile RP
			ON ST.RetailerCode = RP.RetailerCode
			INNER JOIN (
				SELECT ST.StatementCode, RP.*
				FROM RPWeb_Statements ST
					LEFT JOIN RPWeb_Statements ST2
					ON ST.DataSetCode = ST2.StatementCode
					LEFT JOIN RetailerProfile RP
					ON ST2.RetailerCode = RP.RetailerCode
				WHERE ST.DataSetCode_L5 = @DATASETCODE
			) RP2
			ON RP2.StatementCode = RS.StatementCode
		WHERE RS.StatementCode IN (SELECT StatementCode FROM RPWeb_Statements WHERE DataSetCode_L5 = @DATASETCODE AND (Region='WEST' OR StatementLevel = 'LEVEL 1'))
		GROUP BY RP.RetailerCode, RS.StatementCode, RP.RetailerName, RP2.RetailerName
		ORDER BY RP.RetailerName
		FOR XML PATH('tr'), TYPE
	));

	SET @HTML_Data = '
	<div class="rp_rewards_tab rp_programs">
		<div class="rp_program_header">
			<h1>LOCATION REWARD SUMMARY</h1>
		</div>
		<table class="rp_report_table">
			<thead>' +
				@THEAD + '
			</thead>
			<tbody>' +
				@TBODY + '
			</tbody>
		</table>
	</div>';

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	
	IF @SEASON <= 2020		
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(REPLACE(@HTML_Data, CHAR(9), ''),CHAR(13),''),CHAR(10),'') , 'json')
	ELSE 
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')

END
