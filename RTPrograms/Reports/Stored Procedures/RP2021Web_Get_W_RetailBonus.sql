﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_RetailBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @ELIGIBILITYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(MAX)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	DECLARE @QUALIFYING_SALES_CY MONEY=0;
	DECLARE @QUALIFYING_SALES_LY MONEY=0;
	DECLARE @QUALIFYING_PERCENTAGE FLOAT =0;
	DECLARE @TOTAL_ELIGIBLE_SALES MONEY = 0;
	DECLARE @CENTURION_SALES MONEY=0;
	DECLARE @CENTURION_REWARD_PERCENTAGE DECIMAL(6,4)=0;
	DECLARE @CENTURION_REWARD MONEY=0;
	DECLARE @LIBERTY_SALES MONEY=0;
	DECLARE @LIBERTY_REWARD_PERCENTAGE DECIMAL(6,4)=0;
	DECLARE @LIBERTY_REWARD MONEY=0;


	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	--FETCH REWARD DISPLAY VALUES
	SELECT @TOTAL_ELIGIBLE_SALES =  SUM(ISNULL(QualBrand_Sales_CY, 0))
		  ,@QUALIFYING_SALES_CY= SUM(ISNULL(Growth_Sales_CY, 0))
		  ,@QUALIFYING_SALES_LY = SUM(ISNULL(Growth_Sales_LY1_INDEX, 0))
		  ,@CENTURION_SALES = SUM(ISNULL(Cent_Reward_Sales, 0))
		  ,@CENTURION_REWARD_PERCENTAGE = MAX(ISNULL(Cent_Reward_Percentage, 0)) 
		  ,@CENTURION_REWARD = SUM(ISNULL(Cent_Reward, 0))
		  ,@LIBERTY_SALES = SUM(ISNULL(Lib_Reward_Sales, 0))
		  ,@LIBERTY_REWARD_PERCENTAGE = MAX(ISNULL(Lib_Reward_Percentage, 0)) 
		  ,@LIBERTY_REWARD = SUM(ISNULL(Lib_Reward, 0))
	From [dbo].[RP2021Web_Rewards_W_RetailBonus]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode


	SET	@QUALIFYING_PERCENTAGE = 
		CASE
			WHEN @QUALIFYING_SALES_CY > 0 AND @QUALIFYING_SALES_LY <= 0 THEN 9.9999
			WHEN @QUALIFYING_SALES_CY <= 0 AND @QUALIFYING_SALES_LY <= 0 THEN 0
			WHEN @QUALIFYING_SALES_CY/@QUALIFYING_SALES_LY > 9.9999 THEN 9.9999
			ELSE @QUALIFYING_SALES_CY/@QUALIFYING_SALES_LY
		END




	/* ############################### ELIGIBILITY TABLE ############################### */
	SET @HEADER = '<th>' + cast(@Season-1 as varchar(4)) + ' Eligible POG $</th> 
				   <th> '+ cast(@Season as varchar(4)) + ' Eligible POG $</th>
				   <th>Growth %</th>
				   <th> '+ cast(@Season as varchar(4)) + ' Total Eligible POG $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_SALES_LY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_SALES_CY,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@TOTAL_ELIGIBLE_SALES,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		
	SET @ELIGIBILITYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### REWARD SUMMARY TABLE ############################### */
	SET @REWARDSUMMARYTABLE = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th>Product</th>
					<th>'+ cast(@Season as varchar(4)) + ' Eligible POG $</th>
					<th>Reward %</th>
					<th class="mw_80p">Reward $</th>
				</tr>
				<tr>
					<td class>Centurion</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@CENTURION_SALES,'$') +'</td>
					<td class="center_align">' + [dbo].[SVF_Commify](@CENTURION_REWARD_PERCENTAGE,'%') +'</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@CENTURION_REWARD,'$') +'</td>
				</tr>	
				<tr>
					<td class>Liberty 150</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@LIBERTY_SALES,'$') +'</td>
					<td class="center_align">' + [dbo].[SVF_Commify](@LIBERTY_REWARD_PERCENTAGE,'%') +'</td>
					<td class="right_align">' + [dbo].[SVF_Commify](@LIBERTY_REWARD,'$') +'</td>
				</tr>	
			</table>
		</div>
	</div>' 



	/* ############################### REWARD PERCENTAGE TABLE ############################### */
	-- CENTURION 
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th colspan="3">Centurion</th>
				</tr>
				<tr>
					<td>Growth Calculation: eligible 2021 POG sales relative to indexed 2020 POG sales</td>
					<td>Greater than $2.0 Million of all eligible 2021 POG sales at SRP value</td>
					<td>Less than $2.0 Million of all eligible 2021 POG sales at SRP value</td>
				</tr>
				<tr>
					<td>0% to 99.9%</td>
					<td class="center_align"> 1.0% </td>
					<td class="center_align"> 1.0% </td>
				</tr>
				
				<tr>
					<td>100% to 104.9%</td>
					<td class="center_align"> 3.0% </td>
					<td class="center_align"> 2.0% </td>
				</tr>						
			</table>
		</div>
	</div>' 


	-- LIBERTY 150  
	SET @REWARDSPERCENTAGEMATRIX += '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				<tr>
					<th colspan="3">Liberty 150</th>
				</tr>
				<tr>
					<td>Growth Calculation: eligible 2021 POG sales relative to indexed 2020 POG sales</td>
					<td>Greater than $2.0 Million of all eligible 2021 POG sales at SRP value</td>
					<td>Less than $2.0 Million of all eligible 2021 POG sales at SRP value</td>
				</tr>
				<tr>
					<td>0% to 99.9%</td>
					<td class="center_align"> 3.0% </td>
					<td class="center_align"> 2.0% </td>
				</tr>
				
				<tr>
					<td>100% to 104.9%</td>
					<td class="center_align"> 6.0% </td>
					<td class="center_align"> 4.0% </td>
				</tr>						
			</table>
		</div>
	</div>' 


	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Growth Calculation Brands:</strong>  Acrobat, Armezon, Basagran, Basagran Forte, Caramba, Centurion, Cevya, Cimegra, Distinct, Dyax, Engenia, Forum, Frontier Max, Heat Complete, Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Insure Cereal, Insure Cereal FX4, Insure Pulse, Nealta, Nexicor, Nodulator brands, Odyssey brands, Outlook, Poast Ultra, Pursuit, Sefina, Sercadis, Solo brands, Teraxxa, Titan, Twinline, and Zidua SC.
			<div class="rprew_subtabletext">
				<strong>2021 Total Eligible POG $ Brands: </strong> All 2021 Eligible Crop Protection and InVigor Reward Brands
			</div>
			<div class="rprew_subtabletext">
				<strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agriculture Regions(s) for your location(s).
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ELIGIBILITYTABLE 
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

