﻿
CREATE PROCEDURE [Reports].[RP2021Web_Get_E_FungicidesSupport] @STATEMENTCODE INT, @PROGRAMCODE VARCHAR(50), @LANGUAGE VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Declare variables used in building HTML
    DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(50);
	DECLARE @SEASON AS INT;
	DECLARE @STATEMENTLEVEL AS VARCHAR(10) = '';
	DECLARE @MARKETLETTERNAME AS VARCHAR(200) = '';
	DECLARE @SEQUENCE AS INT;
	DECLARE @SECTIONHEADER AS VARCHAR(200) = '';

	DECLARE @REWARDSUMMARYSECTION AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_THEAD2 AS NVARCHAR(MAX) = '';
	DECLARE @ML_SECTIONS AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY1 AS NVARCHAR(MAX) = '';
	DECLARE @ML_TBODY2 AS NVARCHAR(MAX) = '';
	DECLARE @REWARDCONDITIONTEXT AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @REWARDRATETABLE_TBODY AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_THEAD AS NVARCHAR(MAX) = '';
	DECLARE @APPLICATIONRATESTABLE_TBODY AS NVARCHAR(MAX) = '';

	DECLARE @QUAL_PERCENTAGE FLOAT=0;
	DECLARE @REWARD_TIER MONEY=0;
	DECLARE @QualBrand_Sales_LY_SUM MONEY=0;
	DECLARE @QualBrand_Sales_CY_SUM MONEY=0;

	DECLARE @OUQUAL_PERCENTAGE FLOAT=0;
	DECLARE @QualBrand_OUSales_LY_SUM MONEY=0;
	DECLARE @QualBrand_OUSales_CY_SUM MONEY=0;

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END
	DECLARE @DECIMAL_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F'  ELSE ' ' END

	DECLARE @IS_OU INT;

	-- Get program title, reward code, season
	SELECT @PROGRAMTITLE=CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END, @REWARDCODE=RewardCode
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE;
	
	SELECT @IS_OU = DatasetCode, @SEASON=Season, @STATEMENTLEVEL=StatementLevel FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE;

	-- If statementLevel = level 1 and DatasetCode <> 0 

	-- Get reward summary
	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @LANGUAGE=@LANGUAGE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	
	DROP TABLE IF EXISTS #TEMP 	

	SELECT	@REWARD_TIER = MAX(ISNULL(RewardPerAcre, 0))
			,@QualBrand_Sales_LY_SUM = SUM(QualBrand_Sales_LY)
			,@QualBrand_Sales_CY_SUM = SUM(QualBrand_Sales_CY)
	FROM	[dbo].[RP2021Web_Rewards_E_FungicidesSupport]
	WHERE	STATEMENTCODE=@STATEMENTCODE

	SELECT	@QualBrand_OUSales_LY_SUM = SUM(QualBrand_Sales_LY)
			,@QualBrand_OUSales_CY_SUM = SUM(QualBrand_Sales_CY)
	FROM	[dbo].[RP2021Web_Rewards_E_FungicidesSupport]
	WHERE	(@IS_OU = 0 and STATEMENTCODE = @STATEMENTCODE) or (@IS_OU <> 0 and STATEMENTCODE = @IS_OU)

	SELECT	T1.RewardBrand
			,ISNULL(QualBrand_Acres_CY, 0) AS QualBrand_Acres_CY
			,ISNULL(RewardPerAcre, @REWARD_TIER) AS RewardPerAcre
			,ISNULL(Reward, 0) AS Reward
			,ISNULL(QualBrand_Acres_CY_SUM, 0) AS QualBrand_Acres_CY_SUM
			,ISNULL(QualBrand_Sales_CY_SUM, 0) AS QualBrand_Sales_CY_SUM
			,ISNULL(QualBrand_Sales_LY_SUM, 0) AS QualBrand_Sales_LY_SUM
			,ISNULL(Reward_Amount_Tier, @REWARD_TIER) AS Reward_Amount_Tier
			,ISNULL(Reward_SUM, 0) AS Reward_SUM
	INTO	#TEMP
	FROM (
		SELECT	DISTINCT ChemicalGroup AS RewardBrand
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('Caramba', 'Headline AMP', 'Cotegra', 'Headline', 'Priaxor')
	) T1
	LEFT JOIN (
		SELECT RewardBrand
			,QualBrand_Acres_CY
			,RewardPerAcre
			,Reward
			,SUM(ISNULL(QualBrand_Acres_CY, 0)) OVER(PARTITION  BY StatementCode) AS QualBrand_Acres_CY_SUM
			,SUM(ISNULL(QualBrand_Sales_CY, 0)) OVER(PARTITION  BY StatementCode) AS QualBrand_Sales_CY_SUM
			,SUM(ISNULL(QualBrand_Sales_LY, 0)) OVER(PARTITION  BY StatementCode) AS QualBrand_Sales_LY_SUM
			,MAX(ISNULL(RewardPerAcre, 0))		OVER(PARTITION  BY StatementCode) AS Reward_Amount_Tier
			,SUM(ISNULL(Reward, 0))				OVER(PARTITION  BY StatementCode) AS Reward_SUM
		FROM  [dbo].[RP2021Web_Rewards_E_FungicidesSupport]
		WHERE STATEMENTCODE=@STATEMENTCODE
	)T2 ON T1.RewardBrand = T2.RewardBrand
	

	SELECT  @QUAL_PERCENTAGE = IIF(@QualBrand_OUSales_LY_SUM > 0, @QualBrand_OUSales_CY_SUM / @QualBrand_OUSales_LY_SUM, IIF(@QualBrand_OUSales_CY_SUM > 0, 9.9999, 0)) FROM #TEMP
	SELECT  @QUAL_PERCENTAGE = IIF(@QUAL_PERCENTAGE > 9.9999, 9.9999, @QUAL_PERCENTAGE)
	
	IF NOT EXISTS(SELECT * FROM #TEMP)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">
							<div class="st_content open">					
						<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</div>					
					</div>
				</div>' 	
		GOTO FINAL
	END


		DECLARE @EligiblePogLYHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+  CAST(@SEASON-1 AS VARCHAR(4))+' ($)' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $' END; -- RP-4183 next 2 lines
		DECLARE @EligiblePogCYHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+  CAST(@SEASON AS VARCHAR(4))+' ($)' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $' END; 
		DECLARE @EligiblePog$Header_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Croissance (%)' ELSE  IIF(@STATEMENTLEVEL = 'Level 1' and @IS_OU <> 0, 'OU Growth %', 'Growth %') END; 

	SET @ML_THEAD1 = '
		<thead>
			<tr>
				<th>' +@EligiblePogLYHeader_FrenchEnglish+'</th>
				<th>'+@EligiblePogCYHeader_FrenchEnglish+'</th>
				<th>' + @EligiblePog$Header_FrenchEnglish+ '</th>
			</tr>
		</thead>
		'

	-- Create a THEAD for the second table

	DECLARE @ProductHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Produits' ELSE 'Products' END; -- RP-4183 next 3 lines
	DECLARE @EligiblePogHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS admissibles '+  CAST(@SEASON AS VARCHAR(4))+' (acres)' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG Acres' END; 
	DECLARE @RewardPerAcreHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Récompense par acre ($)' ELSE 'Reward $ Per Acre' END; 
	DECLARE @RewardHeader_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Récompense ($)' ELSE 'Reward $' END; 


	SET @ML_THEAD2 = '
		<thead>
			<tr>
				<th>'+@ProductHeader_FrenchEnglish+'</th> 
				<th>'+@EligiblePogHeader_FrenchEnglish + '</th> 
				<th>'+@RewardPerAcreHeader_FrenchEnglish +'</th>
				<th>'+@RewardHeader_FrenchEnglish+'</th>
			</tr>
		</thead>
		';


		SET @ML_TBODY1 = CONVERT(NVARCHAR(MAX),(SELECT top 1	
				(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(@QualBrand_Sales_LY_SUM, @CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(@QualBrand_Sales_CY_SUM,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(@QUAL_PERCENTAGE, @PERCENT_FORMAT) as 'td' for xml path(''), type)
				FROM #TEMP
				FOR XML PATH('tr'), ELEMENTS, TYPE));



		-- Use FOR XML PATH to generate HTML from the data from the table
		SET @ML_TBODY2 = CONVERT(NVARCHAR(MAX),(SELECT
				(select RewardBrand as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(QualBrand_Acres_CY,0), @DECIMAL_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(ISNULL(RewardPerAcre, 0), @CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)		
				FROM #TEMP
				FOR XML PATH('tr')	, ELEMENTS, TYPE));
		
		-- Use FOR XML PATH to generate HTML for the totals from the data from the table
		
		SET @ML_TBODY2 += CONVERT(NVARCHAR(MAX),(SELECT top 1	
					(select 'rp_bold' as [td/@class] ,'Total' as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Acres_CY_SUM, @DECIMAL_FORMAT) as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
				,(select 'rp_bold right_align' as [td/@class] ,dbo.SVF_Commify(Reward_SUM,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				FROM (
					SELECT SUM(QualBrand_Acres_CY) AS QualBrand_Acres_CY_SUM, SUM(Reward) AS Reward_SUM
					FROM #TEMP
				)A
				FOR XML PATH('tr')	, ELEMENTS, TYPE));

		
	
		--Generate the HTML for the section if there are transactions
		SET @ML_SECTIONS += '<div class="st_section">
								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD1 +   @ML_TBODY1  + '
									</table>
								</div>

								<div class="st_content open">									
									<table class="rp_report_table">
									' + @ML_THEAD2 +   @ML_TBODY2  + '
									</table>
								</div>
							</div>';
	




	FINAL:	
			DECLARE @PercentPogSales_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN '% de VAS '+ CAST(@SEASON-1 AS VARCHAR(4))  ELSE '% of ' + CAST(@SEASON-1 AS VARCHAR(4)) + ' POG Sales' END; 
			DECLARE @EligibleRewardGT2000_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS produits récompensés admissibles '+ CAST(@SEASON AS VARCHAR(4)) +' >2 000 et <10 000 acres' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible Reward Brand POG Acres > 2,000 and < 10,000' END; 
			DECLARE @EligibleRewardGT10000_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'VAS produits récompensés admissibles '+ CAST(@SEASON AS VARCHAR(4)) +' >= 10 000 acres ' ELSE CAST(@SEASON AS VARCHAR(4)) + ' Eligible Reward Brand POG Acres > 2,000 and < 10,000' END; 

		-- Set the reward rate table
		SET @REWARDRATETABLE_THEAD = '
		<thead>
			<tr>
				<th>'+ @PercentPogSales_FrenchEnglish+'</th>
				<th>'+ @EligibleRewardGT2000_FrenchEnglish+'</th>
				<th>'+ @EligibleRewardGT10000_FrenchEnglish +'</th>
			</tr>
		</thead>
		';
		DECLARE @AcresPerCase_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Acres par boîte' ELSE 'Acres Per Case' END; 

		-- Get application rates HTML
		SET @APPLICATIONRATESTABLE_THEAD = 
		'
		<thead>
			<tr>
				<th>'+ @ProductHeader_FrenchEnglish + '</th>
				<th>'+ @AcresPerCase_FrenchEnglish + '</th>
			</tr>
		</thead>
		';

		DECLARE @HundredTenPlus_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN '110 % et +' ELSE '110%+' END; 
		DECLARE @HundredToHundredTen_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN '100 % à 109,99 %' ELSE '100% to 109.99%' END;
		DECLARE @NinetyNineToHundred_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN '90 % à 99,9 %' ELSE '90-99.9%' END; 
		DECLARE @onePointSevenFive_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN ' 1,75 $ par acre ' ELSE ' $1.75 per acre ' END;
		DECLARE @onePointTwoFive_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN ' 1,25 $ par acre ' ELSE ' $1.25 per acre ' END; 
		DECLARE @NA_FrenchEnglish AS NVARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN ' s.o ' ELSE ' NA ' END; 

	SET @REWARDRATETABLE_TBODY = '
	<tbody>
		<tr>
			<td class = "center_align">'+@HundredTenPlus_FrenchEnglish+'</td>
			<td class = "center_align">'+ @onePointSevenFive_FrenchEnglish +' </td>
			<td class = "center_align"> '+ @onePointSevenFive_FrenchEnglish +' </td>
		</tr>
		
		<tr>
			<td class = "center_align">'+@HundredToHundredTen_FrenchEnglish+'</td>
			<td class = "center_align"> '+@onePointTwoFive_FrenchEnglish+' </td>
			<td class = "center_align"> '+@onePointSevenFive_FrenchEnglish+' </td>
		</tr>
		<tr>
			<td class = "center_align">'+@NinetyNineToHundred_FrenchEnglish+'</td>
			<td class = "center_align">'+@NA_FrenchEnglish+'</td>
			<td class = "center_align">'+@onePointTwoFive_FrenchEnglish+'/td>
		</tr>
	</tbody>
	';

	SET @REWARDRATETABLE = '
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				' + @REWARDRATETABLE_THEAD + @REWARDRATETABLE_TBODY + '
			</table>
		</div>
	</div>
	';

	SET @APPLICATIONRATESTABLE_TBODY =
	'
	<tbody>
		<tr>
			<td>Caramba</td>
			<td class = "center_align">40</td>
		</tr>
		<tr>
			<td>Cotegra</td>
			<td class = "center_align">70</td>
		</tr>
		<tr>
			<td>Headline</td>
			<td class = "center_align">80</td>
		</tr>
		<tr>
			<td>Headline AMP</td>
			<td class = "center_align">40</td>
		</tr>
		<tr>
			<td>Priaxor</td>
			<td class = "center_align">160</td>
		</tr>
	</tbody>
	'

	SET @APPLICATIONRATESTABLE = 
	'
	<div class="st_static_section">
		<div class="st_content open">
			<table class="rp_report_table">
				' + @APPLICATIONRATESTABLE_THEAD + '
				' + @APPLICATIONRATESTABLE_TBODY + '
			</table>
		</div>
	</div>
	'

	SET @REWARDCONDITIONTEXT = '
			<div class="st_static_section">
				<div class="st_content open">
				<p>
					<strong>Qualifying and Reward Brands:</strong> Caramba, Cotegra, Headline, Headline Amp, and Priaxor.
					</br>
				</p>
				<p>
					<strong>Reward Qualification Requirements:</strong> Retails must sell a minimum of 2,000 acres of qualifying fungicide brands to qualify for this reward.
				</p>
				</div>
			</div>
	';
	IF @LANGUAGE ='F'
		BEGIN
			Set @REWARDCONDITIONTEXT = REPLACE(@REWARDCONDITIONTEXT,'Amp, and','AMP et');
			Set @REWARDCONDITIONTEXT = REPLACE(@REWARDCONDITIONTEXT,'Reward Qualification Requirements:','Conditions de qualification pour la récompense :');
			Set @REWARDCONDITIONTEXT = REPLACE(@REWARDCONDITIONTEXT,'Retails must sell a minimum of 2,000 acres of qualifying fungicide brands to qualify for this reward.','Le détaillant doit vendre l''équivalent d''au moins 2 000 acres de produits fongicides qualificatifs.');
		END


	-- Construct HTML structure
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
							<h1>' + @PROGRAMTITLE + '</h1>
							<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
						</div>';

	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @ML_SECTIONS;
	SET @HTML_Data += @REWARDRATETABLE;
	SET @HTML_Data += @APPLICATIONRATESTABLE;
	SET @HTML_Data += @REWARDCONDITIONTEXT;
	SET @HTML_Data += '</div>'; -- rp_rewards_tab closed

	-- Final output
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'');
END
