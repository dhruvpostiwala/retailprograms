﻿

CREATE  PROCEDURE [Reports].[RP2020Web_Get_W_BrandSpecSupport] @STATEMENTCODE INT, @ProgramCode Varchar(50), @HTML_Data NVARCHAR(MAX) OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PROGRAMTITLE as varchar(200);
	DECLARE @REWARDCODE varchar(50);
	DECLARE @SEASON AS INT;
	DECLARE @SEASON_STRING AS VARCHAR(4);
	DECLARE @REGION AS VARCHAR(4)='';
	DECLARE @RETAILERCODE VARCHAR(50);

	Declare @RewardSummarySection nvarchar(max)='';

	DECLARE @SectionHeader AS NVARCHAR(200)='';
	DECLARE @RewardsPercentageMatrix AS NVARCHAR(MAX)='';
	DECLARE @DisclaimerText AS NVARCHAR(MAX)='';

	DECLARE @MARKETLETTERCODE AS VARCHAR(50)=''
	DECLARE @MARKETLETTERNAME AS VARCHAR(200)='';
	DECLARE @SEQUENCE INT;

	DECLARE @QUANTITY AS DECIMAL(16,2)=0;
	DECLARE @REWARD MONEY;

	DECLARE @ML_SECTIONS AS NVARCHAR(MAX);
	DECLARE @ML_THEAD AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS AS NVARCHAR(MAX);
	
	SELECT @PROGRAMTITLE=Title, @REWARDCODE=RewardCode  FROM RP_Config_Programs WHERE ProgramCode=@ProgramCode 
	SELECT @SEASON=Season, @REGION=REGION, @RETAILERCODE=RetailerCode FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	SET @MARKETLETTERCODE='ML' + @SEASON_STRING + '_W_INV'
	EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @ProgramCode, @RewardCode, @RewardSummarySection = @RewardSummarySection OUTPUT
	
	SELECT @MARKETLETTERNAME=Title FROM RP_Config_MarketLetters WHERE MarketLetterCode=@MARKETLETTERCODE
	
	IF NOT EXISTS(SELECT * FROM RP2020Web_Rewards_W_BrandSpecificSupport WHERE Statementcode=@STATEMENTCODE)
	BEGIN
		SET @ML_SECTIONS = '<div class="st_section">	
				<div class = "st_header">
					<span>' + @MARKETLETTERNAME + ' - Reward Summary</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">					
					<div class="rp_tab_message">NO ELIGIBLE POG TRANSACTIONS FOUND</td></div>					
				</div>
			</div>' 

		GOTO FINAL
	END

	IF @RETAILERCODE IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- West Line Companies
		SET @ML_THEAD='<thead><tr>
				<th>2-Year Average<br/>POG$</th>
				<th>' + @SEASON_STRING + ' Eligible<br/>POG $</th>
				<th>Growth %</th>
				<th>' + @SEASON_STRING + ' Eligible InVigor<br/>POG$</th>			
				<th>Reward %</th>
				<th>Reward $</th>
			</tr></thead>'
	ELSE
		SET @ML_THEAD='<thead><tr>
				<th>' + @SEASON_STRING + ' Eligible<br/>POG$</th>
				<th>2-Year Average<br/>POG$</th>
				<th>Growth %</th>
				<th>' + @SEASON_STRING + ' Eligible InVigor<br/>POG$</th>			
				<th>Reward %</th>
				<th>Reward $</th>
			</tr></thead>'

	IF @RETAILERCODE IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- West Line Companies
		SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(Select
								(select 'right_align' as [td/@class] ,dbo.SVF_Commify((QualBrand_Sales_LY2 + QualBrand_Sales_LY1) / 2, '$')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_CY, '$')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(GrowthPercentage,'%')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)						
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage,'%')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
							FROM (
								SELECT RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY2 ,QualBrand_Sales_LY1 ,Reward_Percentage ,Reward
								,CASE WHEN QualBrand_Sales_CY <= 0 THEN 0
										WHEN QualBrand_Sales_LY1 > 0 OR QualBrand_Sales_LY2 > 0 THEN (QualBrand_Sales_CY/((QualBrand_Sales_LY1+QualBrand_Sales_LY2)/2))
										ELSE 0 END AS [GrowthPercentage]
								FROM RP2020Web_Rewards_W_BrandSpecificSupport 
								WHERE STATEMENTCODE =@STATEMENTCODE
							) d						
							FOR XML PATH('tr')	, ELEMENTS, TYPE))
	ELSE
		SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(Select
								(select 'right_align' as [td/@class] ,dbo.SVF_Commify(QualBrand_Sales_CY, '$')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify((QualBrand_Sales_LY2 + QualBrand_Sales_LY1) / 2, '$')  as 'td' for xml path(''), type)							
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(GrowthPercentage, '%')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(RewardBrand_Sales, '$')  as 'td' for xml path(''), type)							
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage,'%')  as 'td' for xml path(''), type)
								,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
							FROM (
								SELECT RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY2 ,QualBrand_Sales_LY1 ,Reward_Percentage ,Reward								
									,CASE WHEN QualBrand_Sales_CY <= 0 THEN 0
										WHEN QualBrand_Sales_LY1 > 0 OR QualBrand_Sales_LY2 > 0 THEN (QualBrand_Sales_CY/((QualBrand_Sales_LY1+QualBrand_Sales_LY2)/2))
										ELSE 0 END AS [GrowthPercentage]
								FROM RP2020Web_Rewards_W_BrandSpecificSupport 
								WHERE STATEMENTCODE =@STATEMENTCODE
							) d						
							FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @ML_SECTIONS = '<div class="st_section">
				<div class = "st_header">
					<span>' + @MARKETLETTERNAME + ' - Reward Summary</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				

				<div class="st_content open">									
					<table class="rp_report_table">
					' + @ML_THEAD +  @ML_TBODY_ROWS + '
					</table>
				</div>
			</div>' 

	

	/* ############################### FINAL OUTPUT ############################### */	

FINAL:
	SET @SectionHeader = 'REBATE PERCENTAGES'
	IF @RETAILERCODE = 'D0000107' -- If Cargill Statement
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class="st_header">
				<span>' + @SectionHeader + '</span>
				<span class="st_right"></span>		
				<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>Growth in 2020 Relative to the 2-year Average</th><th>Reward on 2020 Eligible InVigor POG $</th></tr>
					<tr><td style="text-align: center">110%+</td><td style="text-align: center">3.5%</td></tr>
					<tr><td style="text-align: center">106% to 109.9%</td><td style="text-align: center">3%</td></tr>
					<tr><td style="text-align: center">103% to 105.9%</td><td style="text-align: center">2.5%</td></tr>
					<tr><td style="text-align: center">100% to 102.9%</td><td style="text-align: center">2%</td></tr>
					<tr><td style="text-align: center">95% to 99.9%</td><td style="text-align: center">1.5%</td></tr>
					<tr><td style="text-align: center">90% to 94.9%</td><td style="text-align: center">1%</td></tr>
				</table>		
			</div>
		</div>'
	ELSE IF @RETAILERCODE = 'D520062427' -- If Nutrien Statement (RP-3177)
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class="st_header">
				<span>' + @SectionHeader + '</span>
				<span class="st_right"></span>		
				<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>Growth in 2020 relative to the 2-year average</th><th>Reward on 2020 POG sales of InVigor</th></tr>
					<tr><td style="text-align: center">110%+</td><td style="text-align: center">1.0%</td></tr>
				</table>		
			</div>
		</div>'
	ELSE IF @RETAILERCODE IN ('D0000137', 'D0000244') -- If other West Line Company statement
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class="st_header">
				<span>' + @SectionHeader + '</span>
				<span class="st_right"></span>		
				<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>Growth in 2020 Relative to the 2-year Average</th><th>Reward on 2020 Eligible InVigor POG $</th></tr>
					<tr><td style="text-align: center">110%+</td><td style="text-align: center">4.50%</td></tr>
					<tr><td style="text-align: center">106% to 109.9%</td><td style="text-align: center">4.00%</td></tr>
					<tr><td style="text-align: center">103% to 105.9%</td><td style="text-align: center">3.50%</td></tr>
					<tr><td style="text-align: center">100% to 102.9%</td><td style="text-align: center">3.00%</td></tr>
					<tr><td style="text-align: center">95% to 99.9%</td><td style="text-align: center">2.00%</td></tr>
					<tr><td style="text-align: center">90% to 94.9%</td><td style="text-align: center">1.00%</td></tr>
				</table>		
			</div>
		</div>'
	ELSE
		SET @RewardsPercentageMatrix = '
		<div class="st_static_section">
			<div class="st_header">
				<span>' + @SectionHeader + '</span>
				<span class="st_right"></span>		
				<div class="st_toggle collapse" style="display: none;" onclick="st_toggle(this);"></div>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">
					<tr><th>Growth in 2020 Relative to the 2-year Average</th><th>Reward on 2020 POG $ of InVigor Qualifying Brands</th></tr>
					<tr><td style="text-align: center">110%+</td><td style="text-align: center">4.50%</td></tr>
					<tr><td style="text-align: center">106% to 109.9%</td><td style="text-align: center">4.00%</td></tr>
					<tr><td style="text-align: center">103% to 105.9%</td><td style="text-align: center">3.50%</td></tr>
					<tr><td style="text-align: center">100% to 102.9%</td><td style="text-align: center">3.00%</td></tr>
					<tr><td style="text-align: center">95% to 99.9%</td><td style="text-align: center">2.00%</td></tr>
					<tr><td style="text-align: center">90% to 94.9%</td><td style="text-align: center">1.00%</td></tr>
				</table>		
			</div>
		</div>'	

	IF @RetailerCode IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- If West Line Company
		SET @DisclaimerText='
			<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext"><b>*Qualifying Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Centurion, Cevya, Distinct, Dyax, Engenia, Frontier Max, Forum, Headline, Heat Brands, Insure cereal, Insure Cereal FX4, Insure Pulse, Nealta, Nexicor, Nodulator Brands, Odyssey Brands, Outlook, Poast Ultra, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline Viper ADV, Zampro & Zidua SC.</div>
					<div class="rprew_subtabletext"><b>*InVigor Reward Brands </b>= InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
					<div class="rprew_subtabletext">*2 Year Average is calculated by taking the Average of 2018 Actual POG$ & 2019 Eligible POG$</div>
					<!--<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div>-->
				</div>
			</div>'
	ELSE
		SET @DisclaimerText='
			<div class="st_static_section">
				<div class="st_content open">
					<div class="rprew_subtabletext"><b>*Qualifying Brands</b> = Acrobat, Armezon, Basagran, Basagran Forte, Cabrio, Cabrio Plus, Cantus, Caramba, Centurion, Cevya, Distinct, Dyax, Engenia, Frontier Max, Forum, Headline, Heat Brands, Insure cereal, Insure Cereal FX4, Insure Pulse, Nealta, Nexicor, Nodulator Brands, Odyssey Brands, Outlook, Poast Ultra, Priaxor, Prowl H20, Pursuit, Sefina, Sercadis, Solo, Solo ADV, Solo Ultra, Titan, Twinline Viper ADV, Zampro & Zidua SC.</div>
					<div class="rprew_subtabletext"><b>*InVigor Reward Brands </b>= InVigor L230, InVigor L233P, InVigor L234PC, InVigor L241C, InVigor L252, InVigor L255PC, InVigor L345PC, InVigor L352C, InVigor LR344PC</div>
					<div class="rprew_subtabletext">*2 Year Average is calculated by taking the Average of 2018 Actual POG$ & 2019 Eligible POG$</div>
					<!--<div class="rprew_subtabletext"><span class="highlight_yellow">*Reward calculated at a Retail Location Level</span></div>-->
				</div>
			</div>'

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'

	SET @HTML_Data += @RewardSummarySection
	SET @HTML_Data += @ML_SECTIONS
	SET @HTML_Data += @RewardsPercentageMatrix
	SET @HTML_Data += @DisclaimerText
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>') -- reverting xml encoded characters back to >,<
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	
	--SELECT @HTML_DATA AS [data]
END
