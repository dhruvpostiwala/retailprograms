﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_LoyaltyBonus] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE VARCHAR(MAX)='';
	DECLARE @ORGANIC_DATA VARCHAR(MAX)='';

	DECLARE @L5CODE VARCHAR(100);

	DECLARE @ML_SECTIONS  AS NVARCHAR(MAX)='';
	DECLARE @ML_THEAD_ROW AS NVARCHAR(MAX);
	DECLARE @ML_TBODY_ROWS  AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(1000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE


	SELECT	@L5CODE = Level5Code
	FROM	RPWeb_Statements
	WHERE	StatementCode = @STATEMENTCODE


	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT


	-- FETCH REWARD DISPLAY VALUES

	-- GET SEGMENT A DISPLAY VALUES
	DROP TABLE IF EXISTS #DETAILS
	SELECT	'A' AS Segment, 
			SUM(SegmentA_QualSales_LY_INDEX) AS QualSales_LY, -- GET THE INDEX VALUE			
			MAX(SegmentA_Growth_Percentage) AS Growth_Percentage, 
			SUM(SegmentA_Reward_Sales) AS Reward_Sales, 
			MAX(SegmentA_Reward_Percentage) AS Reward_Percentage, 
			SUM(SegmentA_Reward) AS Reward
	INTO #DETAILS
	FROM RP2021Web_Rewards_W_LoyaltyBonus T1
	WHERE T1.Statementcode=@STATEMENTCODE



	-- GET SEGMENT B VALUES
	INSERT	INTO #DETAILS
	SELECT	'B' AS Segment, 
			SUM(SegmentB_QualSales_LY_INDEX) AS QualSales_LY,  -- GET THE INDEX VALUE
			MAX(SegmentB_Growth_Percentage) AS Growth_Percentage, 
			SUM(SegmentB_Reward_Sales) AS Reward_Sales, 
			MAX(SegmentB_Reward_Percentage) AS Reward_Percentage, 
			SUM(SegmentB_Reward) AS Reward
	FROM RP2021Web_Rewards_W_LoyaltyBonus T1
	WHERE T1.Statementcode=@STATEMENTCODE


	-- GET SEGMENT C AND D VALUES FOR LINE COMPANIES
	IF @L5CODE IN ('D0000107','D0000137','D0000244','D520062427')
	BEGIN
		INSERT	INTO #DETAILS
		SELECT	'C' AS Segment, 
				SUM(SegmentC_QualSales_LY_INDEX) AS QualSales_LY,  -- GET THE INDEX VALUE
				MAX(SegmentC_Growth_Percentage) AS Growth_Percentage, 
				SUM(SegmentC_Reward_Sales) AS Reward_Sales, 
				MAX(SegmentC_Reward_Percentage) AS Reward_Percentage, 
				SUM(SegmentC_Reward) AS Reward
		FROM RP2021Web_Rewards_W_LoyaltyBonus T1
		WHERE T1.Statementcode=@STATEMENTCODE

		INSERT	INTO #DETAILS
		SELECT	'D' AS Segment, 
				SUM(SegmentD_QualSales_LY_INDEX) AS QualSales_LY,  -- GET THE INDEX VALUE
				MAX(SegmentD_Growth_Percentage) AS Growth_Percentage, 
				SUM(SegmentD_Reward_Sales) AS Reward_Sales, 
				MAX(SegmentD_Reward_Percentage) AS Reward_Percentage, 
				SUM(SegmentD_Reward) AS Reward
		FROM RP2021Web_Rewards_W_LoyaltyBonus T1
		WHERE T1.Statementcode=@STATEMENTCODE
	END
	
	/* ############################### PRODUCT DETAIL TABLE ############################### */

	SET @ML_THEAD_ROW='<tr><thead>
			<th>Segment</th>
			<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG $</th>
			<th>Growth %</th>
			<th>Reward %</th>
			<th class="mw_80p">Reward $</th>			
		</thead></tr>'

	SET @ML_TBODY_ROWS = CONVERT(NVARCHAR(MAX),(SELECT 	
		(select 'center_align' as [td/@class], Segment AS 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(QualSales_LY, '$')  as 'td' for xml path(''), type)
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward_Sales, '$')  as 'td' for xml path(''), type)
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Growth_Percentage, '%')  as 'td' for xml path(''), type)	
		,(select 'center_align' as [td/@class] ,dbo.SVF_Commify(Reward_Percentage, '%')  as 'td' for xml path(''), type)	
		,(select 'right_align' as [td/@class] ,dbo.SVF_Commify(Reward, '$')  as 'td' for xml path(''), type)							
	FROM #DETAILS
	ORDER BY Segment ASC
	FOR XML PATH('tr')	, ELEMENTS, TYPE))


	SET @ML_SECTIONS='<div class="st_section">
					<div class="st_content open">					
						<table class="rp_report_table">' + @ML_THEAD_ROW + @ML_TBODY_ROWS  + '</table>		
					</div>
				</div>' 

	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	/*
	SET @ORGANIC_DATA = '{
							"sales":['+cast((SELECT Reward_Sales FROM #DETAILS WHERE Segment='A') as varchar(20))+','+cast((SELECT Reward_Sales FROM #DETAILS WHERE Segment='B') as varchar(20))+'],
							"reward_margins":['+cast((SELECT Reward_Percentage FROM #DETAILS WHERE Segment='A') as varchar(20))+','+cast((SELECT Reward_Percentage FROM #DETAILS WHERE Segment='B') as varchar(20))+'],
							"col_headers":["Segment","'+cast(@Season as varchar(4))+' Eligible POG $"],
							"row_labels":["A","B"]
						}'
	*/
	DECLARE @TEMPLATE_CODE VARCHAR(5)='4C-2R'

		
	IF @L5CODE IN ('D000001','D0000117')
		BEGIN
			DELETE #DETAILS WHERE Segment IN ('C','D')
			SET @TEMPLATE_CODE = '4C-2R'
		END
	ELSE
		SET @TEMPLATE_CODE = '4C-4R' -- '4C-4R'
	
	DECLARE @SALES_ARRAY  NVARCHAR(1000) , @REWARD_MARGINS_ARRAY  NVARCHAR(500) , @ROW_LABELS_ARRAY NVARCHAR(500)
	SET @SALES_ARRAY = ISNULL((REPLACE( REPLACE( (SELECT Reward_Sales FROM #DETAILS ORDER BY Segment FOR JSON PATH),'{"Reward_Sales":','' ),'}','' )),'[]')
	SET @REWARD_MARGINS_ARRAY = ISNULL((REPLACE( REPLACE( (SELECT Reward_Percentage FROM #DETAILS ORDER BY Segment FOR JSON PATH),'{"Reward_Percentage":','' ),'}','' )),'[]')
	--SET @ROW_LABELS_ARRAY = ISNULL((SELECT Segment FROM #DETAILS ORDER BY Segment FOR JSON PATH),'[]')
	SET @ROW_LABELS_ARRAY = ISNULL((REPLACE( REPLACE( (SELECT Segment FROM #DETAILS FOR JSON AUTO),'{"Segment":','' ),'"}','"' )),'[]')
	

	SET @ORGANIC_DATA = '{
							"sales":' + @SALES_ARRAY  +',
							"reward_margins":' + @REWARD_MARGINS_ARRAY + ',
							"col_headers":["Segment","' + cast(@Season as varchar(4)) + ' Eligible POG $"],
							"row_labels":' + @ROW_LABELS_ARRAY + '
						}'
	

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE=@TEMPLATE_CODE, @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT

	IF @L5Code = 'D000001'
	BEGIN
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<div>
				<b>Segment A</b> 
			</div>
			<table class="rp_report_table">
				<tr>
					<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' to ' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG %</th>
					<th>Reward</th>
				</tr>
				<tr>
					<td class="center_align"> 100% + </td>
					<td class="center_align"> 6.25% </td>
				</tr>
				<tr>
					<td class="center_align"> 90% - 99.9% </td>
					<td class="center_align"> 5.75% </td>
				</tr>				
				<tr>
					<td class="center_align"> 80% - 89.9% </td>
					<td class="center_align"> 3.5% </td>
				</tr>	
				<tr>
					<td class="center_align"> < 80% </td>
					<td class="center_align"> 3% </td>
				</tr>	
			</table>
		</div>
	</div>' 


	SET @REWARDSPERCENTAGEMATRIX += '
	<div class="st_static_section">
		<div class="st_content open">
			<div>
				<b>Segment B</b>
			</div>
			<table class="rp_report_table">
				<tr>
					<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' to ' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG %</th>
					<th>Reward</th>
				</tr>
				<tr>
					<td class="center_align"> 100% + </td>
					<td class="center_align"> 12.25% </td>
				</tr>
				<tr>
					<td class="center_align"> 90% - 99.9% </td>
					<td class="center_align"> 10% </td>
				</tr>				
				<tr>
					<td class="center_align"> 80% - 89.9% </td>
					<td class="center_align"> 3.5% </td>
				</tr>	
				<tr>
					<td class="center_align"> < 80% </td>
					<td class="center_align"> 3% </td>
				</tr>	
			</table>
		</div>
	</div>' 
	END
	ELSE IF @L5CODE = 'D0000117'
	BEGIN
	SET @REWARDSPERCENTAGEMATRIX = '
	<div class="st_static_section">
		<div class="st_content open">
			<div>
				<b>Segment A</b> 
			</div>
			<table class="rp_report_table">
				<tr>
					<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' to ' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG %</th>
					<th>Reward</th>
				</tr>
				<tr>
					<td class="center_align"> 100% + </td>
					<td class="center_align"> 4.5% </td>
				</tr>
				<tr>
					<td class="center_align"> 90% - 99.9% </td>
					<td class="center_align"> 3.75% </td>
				</tr>				
				<tr>
					<td class="center_align"> 80% - 89.9% </td>
					<td class="center_align"> 1.5% </td>
				</tr>	
				<tr>
					<td class="center_align"> < 80% </td>
					<td class="center_align"> 1% </td>
				</tr>	
			</table>
		</div>
	</div>' 


	SET @REWARDSPERCENTAGEMATRIX += '
	<div class="st_static_section">
		<div class="st_content open">
			<div>
				<b>Segment B</b>
			</div>
			<table class="rp_report_table">
				<tr>
					<th>' + CAST(@SEASON-1 AS VARCHAR(4)) + ' to ' + CAST(@SEASON AS VARCHAR(4)) + ' Eligible POG %</th>
					<th>Reward</th>
				</tr>
				<tr>
					<td class="center_align"> 100% + </td>
					<td class="center_align"> 9.5% </td>
				</tr>
				<tr>
					<td class="center_align"> 90% - 99.9% </td>
					<td class="center_align"> 7.25% </td>
				</tr>				
				<tr>
					<td class="center_align"> 80% - 89.9% </td>
					<td class="center_align"> 2.5% </td>
				</tr>	
				<tr>
					<td class="center_align"> < 80% </td>
					<td class="center_align"> 2% </td>
				</tr>	
			</table>
		</div>
	</div>' 
	END

	/* ############################### DISCLAIMER TEXT ############################### */
	IF @L5CODE = 'D000001'
		BEGIN
			SET @DISCLAIMERTEXT = 
				'<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">
							<strong>Segment A Qualifying and Reward Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Heat Complete, and Poast Ultra.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment B Qualifying and Reward Brands:</strong> Odyssey brands, Pursuit brands, Solo brands, and Viper brands.
						</div>
						<div class="rprew_subtabletext">
							 <strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agricultural Region(s) for your location(s).
						</div>
					</div>
				 </div>'
		END
	ELSE IF @L5CODE = 'D0000117'
		BEGIN
			SET @DISCLAIMERTEXT = 
				'<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">
							<strong>Segment A Qualifying and Reward Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Heat Complete, and Poast Ultra.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment B Qualifying and Reward Brands:</strong> Odyssey brands, Pursuit brands, Solo brands, and Viper brands.
						</div>
						<div class="rprew_subtabletext">
							<strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agriculture Regions(s) for your location(s).
						</div>
					</div>
				 </div>'
		
		END
	ELSE IF @L5Code = 'D520062427'-- nutrien
		BEGIN
			SET @DISCLAIMERTEXT = 
				'<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">
							<strong>Segment A Qualifying and Reward Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Heat (Pre-Seed), Heat (Pre-Harvest), Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Heat Complete, and Poast Ultra.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment B Qualifying and Reward Brands:</strong> Odyssey brands, Pursuit brands, Solo brands, and Viper brands.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment C Qualifying and Reward Brands:</strong> Liberty 150
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment D Qualifying and Reward Brands:</strong> Centurion
						</div>
						<div class="rprew_subtabletext">
							<strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agriculture Regions(s) for your location(s).
						</div>
					</div>
				 </div>'
		END
	ELSE 
		BEGIN
			SET @DISCLAIMERTEXT = 
				'<div class="st_static_section">
					<div class="st_content open">
						<div class="rprew_subtabletext">
							<strong>Segment A Qualifying and Reward Brands:</strong> Armezon, Basagran, Basagran Forte, Distinct, Heat LQ (Pre-Seed), Heat LQ (Pre-Harvest), Heat Complete, and Poast Ultra.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment B Qualifying and Reward Brands:</strong> Odyssey brands, Pursuit brands, Solo brands, and Viper brands.
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment C Qualifying and Reward Brands:</strong> Liberty 150
						</div>
						<div class="rprew_subtabletext">
							<strong>Segment D Qualifying and Reward Brands:</strong> Centurion
						</div>
						<div class="rprew_subtabletext">
							<strong>Note:</strong> 2020 Eligible POG displayed may be indexed based on AgData results for the Census Agriculture Regions(s) for your location(s).
						</div>
					</div>
				 </div>'
		
		END
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'	
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @ML_SECTIONS 
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END