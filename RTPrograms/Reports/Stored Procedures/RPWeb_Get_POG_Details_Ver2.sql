﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_POG_Details_Ver2] @STATEMENTCODE INT, @HTML_Data VARCHAR(MAX)=NULL OUTPUT

AS
BEGIN
	-----------------
	/*
	Author : Demarey Baker
	April 3 2020
	Retrieves POG Report
	*/
	SET NOCOUNT ON;

	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4);
	DECLARE @CY INT;
	DECLARE @LY INT;
	DECLARE @Region varchar(10);	
	
	DECLARE @SEQUENCE int;
	SET @HTML_Data='';
	DECLARE @Disclaimer_Text nvarchar(max)='';

	DECLARE @MarketLetterCode Varchar(50);
	DECLARE @MarketLetterName Varchar(200);

	DECLARE @STORED_PROCEDURE_NAME VARCHAR(255)

	DECLARE @Summary_Section NVARCHAR(MAX)='';
	DECLARE @Summary_Thead_Row nvarchar(max) = '';
	DECLARE @Summary_Body nvarchar(max) = '';

	DECLARE @ML_Sections NVARCHAR(MAX)='';			
	DECLARE @ML_Thead_Row nvarchar(max) = '';
	DECLARE @ML_Total_Row nvarchar(max) = '';
	DECLARE @ML_Product_Rows nvarchar(max) = '';

	DECLARE @TOTAL_AMOUNT MONEY=0;
	DECLARE @TOTAL_REWARD MONEY=0;
	
	DECLARE @Eligible_MarketLetters TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterName Varchar(200) NOT NULL,
		[Sequence] INT NOT NULL,
		[TotalAmount] MONEY NOT NULL DEFAULT 0,
		[TotalReward] MONEY NOT NULL DEFAULT 0
	)
	
	DECLARE @SKU_LEVEL_SALES AS UDT_RP_SKU_LEVEL_SALES;
	DECLARE @REWARD_MARGINS AS UDT_RP_REWARD_MARGINS;
	DECLARE @REWARDBRAND VARCHAR(50);

	SELECT @SEASON=Season, @CY=Season, @LY=Season-1, @Region=Region FROM RPWeb_Statements where StatementCode = @STATEMENTCODE;
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))


	SET @Disclaimer_Text='
	<div class="rprew_subtabletext">* Non-Reward Brands defined as BASF brands that are not used in the qualification &/or calculation of ' + CAST(@CY AS VARCHAR(4)) + ' rewards programs</div>
	<div class="rprew_subtabletext">* "' + CAST(@LY AS VARCHAR(4)) + ' & ' + CAST(@CY AS VARCHAR(4)) + ' Eligible POG$" includes 150 km radius compliant POG$</div>	
	<br/>'		
	
	INSERT INTO @Eligible_MarketLetters (MarketLetterCode, MarketLetterName, [Sequence])
	SELECT 'NON_REWARD_BRANDS' AS MarketLetterCode, 'Non-Reward Brands' MarketLetterName, 99 as [Sequence]		
		UNION	
	SELECT MarketLetterCode, Title, [Sequence]
	FROM RP_Config_MarketLetters 
	WHERE IncludeInActual='Yes' AND MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE [StatementCode]=@STATEMENTCODE)
	/*********************** DO NOT REMOVE FOLLOWING COMMENTED CODE - BEGIN ***********************/
			/*
					UNION
				SELECT 'NON-EXISTING' AS MarketLetterCode, 'Non-Existing Brands' MarketLetterName, 0 as [Sequence]
			*/
		
			   
			/*	
				-- TRANSACTIONS @ SKU LEVELS
				IF(NOT(OBJECT_ID('tempdb..@SKU_LEVEL_SALES') IS NULL)) DROP TABLE @SKU_LEVEL_SALES 

				IF @SEASON=2020
					SELECT * 
					INTO @SKU_LEVEL_SALES	
					FROM TVF2020_Get_MarketLetter_Sales(@StatementCode)
			*/
	/*********************** DO NOT REMOVE COMMENTED CODE - END ***********************/

	INSERT @SKU_LEVEL_SALES
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE
	   
	-- WE NEED TO DISPLAY SKU LEVEL TRANSACTIONS ONLY IF THEY HAVE TRANSACTION FOR CY OR LY 
	DELETE FROM @SKU_LEVEL_SALES 	WHERE CYQuantity=0 AND LYQuantity=0 
	
	-- IF NO LIBERTY 200 TRANSACTIONS EXIST, DO NOT DISPLAY LIBERTY 200 IN SUMMARY AND DETAILS 
	DELETE EML
	FROM @Eligible_MarketLetters  EML	
		LEFT JOIN (
			SELECT DISTINCT MarketLetterCode FROM @SKU_LEVEL_SALES WHERE MarketLetterCode='ML2020_W_LIBERTY_200'
		) SKU
		ON SKU.MarketLetterCode=EML.MarketLetterCode
	WHERE EML.MarketLetterCode='ML2020_W_LIBERTY_200' AND ISNULL(SKU.MarketLetterCode,'')=''

	UPDATE T1
	SET [TotalAmount]=T2.CYSales
	FROM @Eligible_MarketLetters T1
		INNER JOIN (
			SELECT MarketLetterCode
				,SUM(CYSales) as CYSales
				,SUM(CYEligibleSales) as CYEligibleSales
			FROM @SKU_LEVEL_SALES			
			GROUP BY MarketLetterCode
		) T2
		ON T2.MarketLetterCode=T1.MarketLetterCode

	
	UPDATE T1
	SET [TotalReward]=T2.[TotalReward]
	FROM @Eligible_MarketLetters T1
		INNER JOIN (
			SELECT MarketLetterCode, SUM(TotalReward) AS TotalReward
			FROM RPWeb_Rewards_Summary
			WHERE Statementcode=@Statementcode
			GROUP BY MarketLetterCode
		) T2
		ON T2.MarketLetterCode=T1.MarketLetterCode

	IF @REGION='WEST'
	BEGIN
		SET @STORED_PROCEDURE_NAME = 'Reports.RP' + @SEASON_STRING + '_Get_RewardMargins_' + @REGION

		INSERT @REWARD_MARGINS
		EXEC  @STORED_PROCEDURE_NAME @STATEMENTCODE
	END
	   	
	--Table Headers
	SET @Summary_Thead_Row = '
	<thead>
	<tr>
		<th>Market Letter</th>
		<th>' + CAST(@LY AS VARCHAR(4))  + ' Actual<br/>POG$</th>
		<th>' + CAST(@LY AS VARCHAR(4))  + ' Eligible<br/>POG$</th>
		<th>' + CAST(@CY AS VARCHAR(4))  + ' Actual<br/>POG$</th>
		<th class="mw_80p">' + CAST(@CY AS VARCHAR(4))  + ' Eligible<br/>POG$</th>
	</tr>
	</thead>'

	-- MARKET LETTER - REWARD BRANDS SUB TOTAL ROW

	SELECT @Summary_Body += ISNULL(
								CONVERT(NVARCHAR(MAX), (
									SELECT  'sub_total_row ' as [@class], (SELECT 'Reward Brands Total' AS 'td' for xml path(''), type) 															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
								FROM (
									SELECT SUM(LYSales) as LYSales
										,SUM(LYEligibleSales) as LYEligibleSales
										,SUM(CYSales) as CYSales
										,SUM(CYEligibleSales) as CYEligibleSales
									FROM @SKU_LEVEL_SALES
									WHERE MarketLetterCode <> 'NON_REWARD_BRANDS'																		
								)  D
								FOR XML PATH('tr'), ELEMENTS, TYPE))
							,'<tr class="sub_total_row "><td>Reward Brands Total</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td></tr>')
				
	-- EACH MARKET LETTER TOTALS (MULTIPLE ROWS)
	SELECT @Summary_Body += CONVERT(NVARCHAR(MAX),(SELECT	(SELECT  '' as [td/@class], MarketLetterName as 'td' for xml path(''), type)									
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)									
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)									
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)									
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
									
							FROM (
								SELECT ML.[Sequence], ML.MarketLetterName	
									,SUM(ISNULL(LYSales,0)) as LYSales
									,SUM(ISNULL(LYEligibleSales,0)) as LYEligibleSales
									,SUM(ISNULL(CYSales,0)) as CYSales
									,SUM(ISNULL(CYEligibleSales,0)) as CYEligibleSales
								FROM @Eligible_MarketLetters ML
									LEFT JOIN @SKU_LEVEL_SALES SKU		ON	 SKU.MarketLetterCode=ML.MarketLetterCode
								WHERE ML.MarketLetterCode <> 'NON_REWARD_BRANDS'										
								GROUP BY ML.[Sequence], ML.MarketLetterName								
							)  D
							ORDER BY [Sequence]
							FOR XML RAW('tr'), ELEMENTS, TYPE))
	
	-- NON-REWARD BRANDS SUB TOTAL ROW
	SELECT @Summary_Body += CONVERT(NVARCHAR(MAX), (
									SELECT 'sub_total_row ' as [@class], (SELECT 'Non-Reward Brands Total' AS 'td' for xml path(''), type) 															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
								FROM (
									SELECT ML.[Sequence], ML.MarketLetterName	
										,SUM(ISNULL(LYSales,0)) as LYSales
										,SUM(ISNULL(LYEligibleSales,0)) as LYEligibleSales
										,SUM(ISNULL(CYSales,0)) as CYSales
										,SUM(ISNULL(CYEligibleSales,0)) as CYEligibleSales
									FROM @Eligible_MarketLetters ML
										LEFT JOIN @SKU_LEVEL_SALES SKU		ON	 SKU.MarketLetterCode=ML.MarketLetterCode
									WHERE ML.MarketLetterCode = 'NON_REWARD_BRANDS'										
									GROUP BY ML.[Sequence], ML.MarketLetterName																			
								)  D
								FOR XML PATH('tr'), ELEMENTS, TYPE))
 
	-- SELECT @Summary_Body += '<tr><td colspan=5>Non-Reward Brands defined as BASF brands that are not used in the qualification &/or calculation of ' + CAST(@CY AS VARCHAR(4)) + ' rewards programs</td></tr>'
	--SELECT @Summary_Body += '<tr><td colspan=5></td></tr>'
								

		
	-- GRAND TOTAL ROW
	SELECT @Summary_Body +=  ISNULL(
								CONVERT(NVARCHAR(MAX), (
								SELECT 'total_row ' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)															
									,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
								FROM (
									SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
									FROM @SKU_LEVEL_SALES									
								)  D
								FOR XML PATH('tr'), ELEMENTS, TYPE))
							,'<tr class="total_row "><td>Total</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td></tr>')

	SET @Summary_Section = '<div class="st_section first">
			<div class = "st_header">
				<span>MARKET LETTERS - REWARD SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"></span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @Summary_Thead_Row + @Summary_Body + '
				</table>' +  @Disclaimer_Text + ' 

			</div>
		</div>' 


	-- ############################### MARKET LETTER DETAILS BREAKDOWN ###############################

	SET @ML_Thead_Row='
	<tr>
		<thead>
		<th>Product Name</th>
		<th>' + CAST(@LY AS VARCHAR(4)) + ' Actual POG QTY</th>
		<th>' + CAST(@LY AS VARCHAR(4)) + ' Actual POG$</th>		
		<th>' + CAST(@LY AS VARCHAR(4)) + ' Eligible POG QTY</th>
		<th>' + CAST(@LY AS VARCHAR(4)) + ' Eligible POG$</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual POG QTY</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual POG$</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible POG QTY</th>
		<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible POG$</th>
		<th class="mw_80p">' + CAST(@CY AS VARCHAR(4)) + ' Reward Margin/Brand</th>
		</thead>
	</tr>'

	
	--LOOP THROUGH MARKET LETTERS
	--DECLARE @FirstMarketLetter BIT=1;
	DECLARE @ML_REWARD_MARGIN DECIMAL(6,4)=0;

	DECLARE MARKET_LETTER_LOOP CURSOR FOR 
		SELECT [MarketLetterCode], [MarketLetterName], [Sequence], [TotalAmount], [TotalReward] FROM @Eligible_MarketLetters	ORDER BY [Sequence]
	OPEN MARKET_LETTER_LOOP
	FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE, @TOTAL_AMOUNT, @TOTAL_REWARD
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			IF NOT OBJECT_ID('tempdb..#ML_SKU_DETAILS') IS NULL DROP TABLE #ML_SKU_DETAILS 
			
			IF @TOTAL_AMOUNT=0 	
				SET @ML_REWARD_MARGIN=0
			ELSE 
				SET @ML_REWARD_MARGIN=@TOTAL_REWARD/@TOTAL_AMOUNT;
				
			SELECT MarketLetterName , T1.ChemicalGroup AS RewardBrand ,ProductName
				,LYQuantity ,LYEligibleQty ,LYSales ,LYEligibleSales
				,CYQuantity ,CYSales ,CYEligibleQty ,CYEligibleSales
				,ISNULL(T2.Reward_Margin,0) AS Reward_Margin				
			INTO #ML_SKU_DETAILS
			FROM @SKU_LEVEL_SALES T1
				LEFT JOIN (
					SELECT MarketLetterCode, ChemicalGroup
						,SUM(Overall_Reward_Percentage) AS Reward_Margin
						--,MAX(RewardBrand_Sales) RewardBrand_Sales
						--,SUM(Reward) AS Reward
					FROM @REWARD_MARGINS
					WHERE MarketLetterCode=@MarketLettercode
					GROUP BY MarketLetterCode, ChemicalGroup
				) T2
				ON T2.MarketLetterCode=T1.MarketLettercode AND T2.ChemicalGroup=T1.ChemicalGroup
			WHERE T1.MarketletterCode = @MARKETLETTERCODE 
			ORDER BY ProductName


			IF NOT EXISTS(SELECT * FROM #ML_SKU_DETAILS)
			BEGIN 

			SET @ML_Sections += '<div class="st_section">
				<div class = "st_header">
					<span>' + @MARKETLETTERNAME + '</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				<div class="st_content open">					
					<div class="rp_tab_message">NO TRANSACTIONS FOUND</div>					
				</div>
			</div>' 
				GOTO FETCH_NEXT
			END
			
			SELECT @ML_Product_Rows = ''

			DECLARE REWARD_BRAND_LOOP CURSOR FOR 
				SELECT DISTINCT [RewardBrand] FROM #ML_SKU_DETAILS	ORDER BY [RewardBrand]
			OPEN REWARD_BRAND_LOOP
			FETCH NEXT FROM REWARD_BRAND_LOOP INTO @REWARDBRAND
			WHILE(@@FETCH_STATUS=0)
			BEGIN

				-- REWARD BRAND SUB TOTAL ROW
				SELECT @ML_Product_Rows += CONVERT(NVARCHAR(MAX),(SELECT  'sub_total_row ' as [@class]
							,(SELECT  2 as [td/@colspan], @REWARDBRAND  as 'td' for xml path(''), type)							
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)
							,(SELECT '' as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)
							,(SELECT '' as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)
							,(SELECT '' as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(REWARD_MARGIN,'%') as 'td' for xml path(''), type)																				
					FROM (							
							SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales, MAX(REWARD_MARGIN) AS REWARD_MARGIN
							FROM #ML_SKU_DETAILS 
							WHERE [RewardBrand]=@REWARDBRAND
					)  D											
					FOR XML PATH('tr'), ELEMENTS, TYPE))


				-- MARKET LETTER - PRODUCTS ROWS	
				SELECT @ML_Product_Rows += ISNULL( CONVERT(NVARCHAR(MAX),(SELECT
							(SELECT  '' as [td/@class],  ProductName as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] , dbo.SVF_Commify(LYQuantity,'') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleQty,'') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYQuantity,'') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleQty,'') as 'td' for xml path(''), type)
							,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
							-- ,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(Reward_Margin,'%') as 'td' for xml path(''), type)																				
							,(SELECT '' as 'td' for xml path(''), type)			
					FROM (
							SELECT ProductName
								,LYQuantity ,LYSales ,LYEligibleQty, LYEligibleSales
								,CYQuantity ,CYSales ,CYEligibleQty, CYEligibleSales												
								,REWARD_MARGIN											
							FROM #ML_SKU_DETAILS T1		
							WHERE [RewardBrand]=@REWARDBRAND
					)  D
					ORDER BY ProductName
					FOR XML RAW('tr'), ELEMENTS, TYPE))
				,'<tr><td colspan=10>NO TRANSACTIONS FOUND</td></tr>')
	
				FETCH NEXT FROM REWARD_BRAND_LOOP INTO @REWARDBRAND
			END
			CLOSE REWARD_BRAND_LOOP
			DEALLOCATE REWARD_BRAND_LOOP										
			
			-- MARKET LETTER - TOTAL ROW
			SELECT @ML_Total_Row = CONVERT(NVARCHAR(MAX), (
								SELECT 'total_row ' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 
								,(SELECT 'right_align ' as [td/@class] ,'' as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYSales,'$') as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,'' as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,'$') as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,'' as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYSales,'$') as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,'' as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,'$') as 'td' for xml path(''), type)
								,(SELECT 'right_align ' as [td/@class] ,dbo.SVF_Commify(@ML_REWARD_MARGIN,'%') as 'td' for xml path(''), type)
							FROM (
								SELECT SUM(LYSales) as LYSales ,SUM(LYEligibleSales) as LYEligibleSales ,SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
								FROM #ML_SKU_DETAILS 
							)  D 
							FOR XML PATH('tr'), ELEMENTS, TYPE))

								
			SET @ML_Sections += '<div class="st_section">
				<div class = "st_header">
					<span>' + UPPER(@MARKETLETTERNAME) + ' - REWARD SUMMARY</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"></span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">' + @ML_Thead_Row + @ML_Product_Rows +  @ML_Total_Row + '
					</table>
				</div>
			</div>' 
	
	FETCH_NEXT:	
			FETCH NEXT FROM MARKET_LETTER_LOOP INTO @MARKETLETTERCODE, @MARKETLETTERNAME, @SEQUENCE, @TOTAL_AMOUNT, @TOTAL_REWARD
		END
	CLOSE MARKET_LETTER_LOOP
	DEALLOCATE MARKET_LETTER_LOOP




	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">
		<div class="rp_program_header">
			<h1>POG REPORT VER 2</h1>
			<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
		</div>' +  @Summary_Section +  @ML_Sections + '</div>'

--	 IF(NOT(OBJECT_ID('tempdb..@SKU_LEVEL_SALES') IS NULL)) DROP TABLE #SKU_LEVEL_SALES 
	IF(NOT(OBJECT_ID('tempdb..#ML_SKU_DETAILS') IS NULL)) DROP TABLE #ML_SKU_DETAILS 

	   
	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	
	--SELECT @HTML_DATA AS [data]
END
