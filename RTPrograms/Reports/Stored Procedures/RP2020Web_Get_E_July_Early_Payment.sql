﻿







CREATE  PROCEDURE [Reports].[RP2020Web_Get_E_July_Early_Payment] @STATEMENTCODE int, @REWARDCODE varchar(200), @LANGUAGE AS VARCHAR(1) = 'E', @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	
	DECLARE @SEASON INT = 2020;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @RetailerCode VARCHAR(50);
	
	
	SELECT @PROGRAMTITLE=RewardLabel From RP_Config_Rewards_Summary Where REWARDCODE=@REWARDCODE
	Select @RetailerCode = RetailerCode  from RPWeb_Statements where StatementCode =@STATEMENTCODE
	
		DECLARE @HEADER NVARCHAR(MAX);

		SET @HEADER = '<th> Payment Description</th><th>Status</th><th>Amount</th>'
	
		SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
					(select 'right_align' as [td/@class] ,  PaymentDesc  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , Status  as 'td' for xml path(''), type)
					,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](Amount,'$') as 'td' for xml path(''), type)
					FROM RP_Seeding_OneTimePayments
					WHERE RewardCode = @REWARDCODE AND SEASON = @SEASON AND RetailerCode = @RetailerCode
		FOR XML RAW('tr'), ELEMENTS, TYPE))
	

	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 

	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	--SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
END

