﻿
CREATE PROCEDURE [Reports].[RPWeb_Get_StatementSummary] @STATEMENTCODE INT,  @Language as VARCHAR(1) = 'E', @HTML_DATA NVARCHAR(MAX) = NULL OUTPUT

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4)='';
	DECLARE @CY INT;
	DECLARE @LY INT;
	DECLARE @REGION VARCHAR(4);	
	DECLARE @RETAILERCODE VARCHAR(10);
	DECLARE @LEVEL5CODE VARCHAR(20);
	DECLARE @VERSIONTYPE VARCHAR(50)='';
	DECLARE @SRC_STATEMENTCODE INT=0;
	DECLARE @LOCKED_DATE DATE=''
	DECLARE @SUMMARY VARCHAR(3);

	SET @HTML_DATA='';
	
	DECLARE @POG_SUMMARY_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @POG_SUMMARY_BODY NVARCHAR(MAX)='';
	DECLARE @POG_SUMMARY_SECTION NVARCHAR(MAX)='';

	DECLARE @DISCLAIMER_TEXT NVARCHAR(MAX)='';

	DECLARE @SEQUNECE INT;

	DECLARE @ML_SUMMARY_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @ML_SUMMARY_BODY NVARCHAR(MAX)='';
	DECLARE @ML_SUMMARY_SECTION NVARCHAR(MAX)='';

	DECLARE @OTHER_REWARDS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_BODY NVARCHAR(MAX)='';
	DECLARE @OTHER_REWARDS_SECTION NVARCHAR(MAX)='';


	DECLARE @OTP_REWARDS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @OTP_REWARDS_BODY NVARCHAR(MAX)='';
	DECLARE @OTP_REWARDS_SECTION NVARCHAR(MAX)='';

	
	DECLARE @IEX_REWARDS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @IEX_REWARDS_BODY NVARCHAR(MAX)='';
	DECLARE @IEX_REWARDS_SECTION NVARCHAR(MAX)='';
	

	DECLARE @REWARDS_DETAILS_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @REWARDS_DETAILS_BODY NVARCHAR(MAX)='';
	DECLARE @REWARDS_DETAILS_SECTION NVARCHAR(MAX)='';
		
	DECLARE @TOTAL_REWARD MONEY=0;
	DECLARE @TOTAL_POG_AMOUNT MONEY=0;
	DECLARE @REARD_MARGIN MONEY=0;

	
	DECLARE @ESTIMATES_THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @ESTIMATES_BODY NVARCHAR(MAX)='';
	DECLARE @ESTIMATES_SECTION NVARCHAR(MAX)='';
	

	DECLARE @NET_AMOUNT_SECTION NVARCHAR(MAX)='';

	DECLARE @TOTAL_REWARD_LINE as VARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Total des produits récompensés' ELSE 'Total Reward Brands' END
	DECLARE @GRAND_TOTAL_LINE as VARCHAR(100) = CASE WHEN @LANGUAGE = 'F' THEN 'Grand total' ELSE 'Grand Total' END

	DECLARE @CURRENCY_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F$'  ELSE '$' END -- RP-4183
	DECLARE @PERCENT_FORMAT AS VARCHAR(10)= CASE WHEN @LANGUAGE = 'F' THEN 'F%'  ELSE '%' END

		   
	DECLARE @Eligible_MarketLetters TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterName Varchar(200) NOT NULL,
		[Sequence] INT NOT NULL
	)

	DECLARE @OVERALL_REWARDS_SUMMARY TABLE(
		PaymentMonth VARCHAR(30) NOT NULL, 
		[Sequence] INT,
		LOCKED_DATE DATETIME,
		PaidToDate MONEY,
		CurrentPayment MONEY,
		TotalReward MONEY
	)

	DECLARE @SKU_LEVEL_SALES AS UDT_RP_SKU_LEVEL_SALES;
	
	SELECT @SEASON=Season, @CY=Season, @LY=Season-1, @Region=Region, @VERSIONTYPE=VersionType, @SRC_STATEMENTCODE=SRC_StatementCode, @LOCKED_DATE=Locked_Date, @RETAILERCODE=RetailerCode, @LEVEL5CODE=Level5Code
	FROM RPWeb_Statements 
	WHERE StatementCode = @STATEMENTCODE;

	IF @VERSIONTYPE='UNLOCKED'
		SET @SRC_STATEMENTCODE=@STATEMENTCODE


	SET @SUMMARY = (
		SELECT ISNULL(HO.Summary, 'No')
		FROM RPWeb_Statements ST
			LEFT JOIN RP_Config_HOPaymentLevel HO
			ON HO.Level5Code = ST.RetailerCode AND HO.Season = ST.Season
		WHERE ST.StatementCode = @STATEMENTCODE AND ST.Season = @SEASON AND ST.Status = 'Active' AND ST.StatementType = 'Actual'
	);

	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	-- ELIGIBLE MARKET LETTERS
	INSERT INTO @Eligible_MarketLetters (MarketLetterCode, MarketLetterName, [Sequence])
	SELECT 'NON_REWARD_BRANDS' AS MarketLetterCode
	, CASE WHEN @LANGUAGE = 'F' THEN 'Produits non récompensés' ELSE 'Non-Reward Brands' END AS MarketLetterName
	, 99 as [Sequence]		
		UNION	
	SELECT MarketLetterCode, CASE WHEN @LANGUAGE = 'F' THEN Title_FR ELSE Title END AS Title, [Sequence]
	FROM RP_Config_MarketLetters 
	WHERE Season=@SEASON AND Region=@REGION
		AND MarketLetterCode IN (SELECT DISTINCT MarketLetterCode FROM RPWeb_ML_ELG_Programs WHERE [StatementCode]=@STATEMENTCODE OR [DataSetCode_L5]=@STATEMENTCODE)
		
	-- SKU LEVEL SALES
	INSERT @SKU_LEVEL_SALES
	EXEC [Reports].[RPWeb_Get_SKU_LevelSales] @STATEMENTCODE
	
	-- POG SALES SUMMARY BY MARKET LETTER
	DROP TABLE IF EXISTS #ML_Level_Sales  
	SELECT MarketLetterCode
		,SUM(LYEligibleSales) as LYEligibleSales
		,SUM(Forecast_Sales) AS Forecast_Sales
		,SUM(CYSales) as CYSales
		,SUM(CYEligibleSales) as CYEligibleSales
	INTO #ML_Level_Sales
	FROM @SKU_LEVEL_SALES
	GROUP BY MarketLetterCode
	
	--Table Headers
	IF @LANGUAGE = 'F'
		SET @POG_SUMMARY_THEAD_ROW = '<tr>
			<th>Lettre de commercialisation</th>
			<th>VAS admissibles ' + CAST(@LY AS VARCHAR(4)) + ' ($)</th>
			<th>VAS prévues ' + CAST(@CY AS VARCHAR(4)) + ' ($)</th>
			<th>VAS réalisées ' + CAST(@CY AS VARCHAR(4)) + ' ($)</th>
			<th>VAS admissibles ' + CAST(@CY AS VARCHAR(4)) + ' ($)</th>
		</tr>'
	ELSE
		SET @POG_SUMMARY_THEAD_ROW = '<tr>
			<th>Market Letter</th>
			<th>' + CAST(@LY AS VARCHAR(4)) + ' Eligible<br/>POG</th>
			<th>' + CAST(@CY AS VARCHAR(4)) + ' Plan<br/>POG</th>
			<th>' + CAST(@CY AS VARCHAR(4)) + ' Actual<br/>POG</th>
			<th>' + CAST(@CY AS VARCHAR(4)) + ' Eligible<br/>POG</th>
		</tr>'
	
	-- EACH MARKET LETTER TOTALS (MULTIPLE ROWS)

	--ADDED BY VEDANT. IF THE REWARD IS 0 FOR LIBERTY-200 REMOVE THE ROW RP-2847
	DELETE ML
	FROM @Eligible_MarketLetters ML 
	LEFT JOIN #ML_Level_Sales MLS 
		ON MLS.MarketLetterCode=ML.MarketLetterCode
	WHERE ML.MarketLetterCode = 'ML' + CAST(@SEASON AS VARCHAR(4)) + '_W_LIBERTY_200' AND ISNULL(MLS.CYSales,0) = 0

	--ADDED BY VEDANT. IF THE REWARD IS 0 FOR EAST RETAILERS THEN REMOVE THE InVigor ROW RP-2847
	DELETE ML 
	FROM @Eligible_MarketLetters ML 
	LEFT JOIN #ML_Level_Sales MLS 
		ON MLS.MarketLetterCode=ML.MarketLetterCode
	WHERE ML.MarketLetterCode = 'ML' + CAST(@SEASON AS VARCHAR(4)) + '_E_INV' AND ISNULL(MLS.CYSales,0) = 0 AND ISNULL(MLS.LYEligibleSales,0) = 0
	
	SET @POG_SUMMARY_BODY = CONVERT(NVARCHAR(MAX),(
			SELECT	(SELECT MarketLetterName as 'td' for xml path(''), type)									
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)									
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(POG_PlanSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)									
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)									
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)									
			FROM (
				SELECT ML.[Sequence], ML.MarketLetterName	
					,ISNULL(MLS.LYEligibleSales,0) as LYEligibleSales
					,ISNULL(MLS.Forecast_Sales,0) as POG_PlanSales
					,ISNULL(MLS.CYSales,0) as CYSales
					,ISNULL(MLS.CYEligibleSales,0) as CYEligibleSales
				FROM @Eligible_MarketLetters ML
					LEFT JOIN #ML_Level_Sales MLS		ON	 MLS.MarketLetterCode=ML.MarketLetterCode
				WHERE ML.MarketLetterCode <> 'NON_REWARD_BRANDS'																		
			)  D
			ORDER BY MarketLetterName
	FOR XML RAW('tr'), ELEMENTS, TYPE))

	IF @POG_SUMMARY_BODY IS NULL
		SET @POG_SUMMARY_BODY=''

	IF NOT EXISTS(SELECT * FROM #ML_Level_Sales WHERE MarketLetterCode <> 'NON_REWARD_BRANDS')
		SET @POG_SUMMARY_BODY +='
			<tr class="sub_total_row">
				<td>' + @TOTAL_REWARD_LINE + '</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
			</tr>'
	ELSE

		BEGIN		
		-- MARKET LETTER - REWARD BRANDS - SUB TOTAL ROW
		SET @POG_SUMMARY_BODY += CONVERT(NVARCHAR(MAX), (
				SELECT  'sub_total_row' as [@class], (SELECT @TOTAL_REWARD_LINE AS 'td' for xml path(''), type) 															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(POG_PlanSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)																								
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
			FROM (
				SELECT SUM(LYEligibleSales) as LYEligibleSales
					,SUM(Forecast_Sales) AS POG_PlanSales
					,SUM(CYSales) as CYSales
					,SUM(CYEligibleSales) as CYEligibleSales
				FROM #ML_Level_Sales
				WHERE MarketLetterCode <> 'NON_REWARD_BRANDS'																		
			)  D
			FOR XML PATH('tr'), ELEMENTS, TYPE))	
		END
		
	IF NOT EXISTS(SELECT * FROM #ML_Level_Sales WHERE MarketLetterCode = 'NON_REWARD_BRANDS')
		SET @POG_SUMMARY_BODY +='
			<tr class="sub_total_row">
				<td>'+ CASE WHEN @LANGUAGE = 'F' THEN 'Produits non récompensés' ELSE 'Total Non-Reward Brands' END +'</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
				<td class="right_align reward_amount">$0.00</td>
			</tr>'
	ELSE 
		BEGIN	
		-- NON-REWARD BRANDS SUB TOTAL ROW
		SET @POG_SUMMARY_BODY += CONVERT(NVARCHAR(MAX), (
				SELECT 'sub_total_row' as [@class]
				,(SELECT CASE WHEN @LANGUAGE = 'F' THEN 'Produits non récompensés' ELSE 'Total Non-Reward Brands' END AS 'td' for xml path(''), type) 																								
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(POG_PlanSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
			FROM (
				SELECT ML.MarketLetterName	
					,ISNULL(MLS.LYEligibleSales,0) as LYEligibleSales
					,ISNULL(MLS.Forecast_Sales,0) as POG_PlanSales										
					,ISNULL(MLS.CYSales,0) as CYSales
					,ISNULL(MLS.CYEligibleSales,0) as CYEligibleSales
				FROM @Eligible_MarketLetters ML
					LEFT JOIN #ML_Level_Sales MLS		ON	 MLS.MarketLetterCode=ML.MarketLetterCode
				WHERE ML.MarketLetterCode = 'NON_REWARD_BRANDS'																																		
			)  D
			FOR XML PATH('tr'), ELEMENTS, TYPE))
		END
	
	-- GRAND TOTAL ROW
	SET @POG_SUMMARY_BODY +=  ISNULL(
							CONVERT(NVARCHAR(MAX), (
							SELECT 'total_row' as [@class], (SELECT @GRAND_TOTAL_LINE AS 'td' for xml path(''), type) 																								
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(LYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(POG_PlanSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)															
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CYEligibleSales,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
							FROM (
								SELECT SUM(LYEligibleSales) as LYEligibleSales , SUM(Forecast_Sales) as POG_PlanSales, SUM(CYSales) as CYSales ,SUM(CYEligibleSales) as CYEligibleSales
								FROM #ML_Level_Sales									
							)  D
							FOR XML PATH('tr'), ELEMENTS, TYPE))
						,'<tr class="total_row"><td>Total</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td><td class="right_align">$0.00</td></tr>')

	SET @POG_SUMMARY_SECTION = '<div class="st_section first">
			<div class = "st_header" style="display: none;">
				<span>POG SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @POG_SUMMARY_THEAD_ROW + @POG_SUMMARY_BODY + '
				</table> 

			</div>
		</div>' 
	/***** END OF POG SALES SUMMARY *****/
	

	/***** MARKET LETTER REWARD SUMMARY *****/
	DROP TABLE IF EXISTS #REWARD_SUMMARY --Drop the temp table incase it exists
	SELECT ML.MarketLetterCode, ML.[Sequence] ,ML.MarketLetterName
		,SUM(rs.TotalReward) AS TotalReward
		,SUM(rs.PaidToDate) AS PaidToDate
		,SUM(rs.CurrentPayment) AS CurrentPayment
		,SUM(rs.NextPayment) AS NextPayment
		,MAX(ISNULL(MLS.Eligible_Amount,0)) AS POG_Amount
 	INTO #REWARD_SUMMARY
	FROM @Eligible_MarketLetters ML
		INNER JOIN RPWeb_Rewards_Summary RS			
		ON RS.MarketLetterCode=ML.MarketLetterCode		
		INNER JOIN (
			SELECT RewardCode
			FROM RP_Config_Rewards_Summary 				
			WHERE Season=@Season AND Region=@Region 
				AND (
					(@SEASON >= 2021 AND RewardType IN ('REGULAR','EXCEPTION') )
						OR
					(@SEASON <= 2020 AND RewardType = 'REGULAR')
				)
		) RSC	
		ON RSC.RewardCode=RS.RewardCode
		LEFT JOIN (
			SELECT MarketLetterCode, [CYSales] AS [Actual_Amount], CYEligibleSales AS [Eligible_Amount] 
			FROM #ML_Level_Sales			
		) MLS
		ON MLS.MarketLetterCode=RS.MarketLetterCode
	WHERE RS.StatementCode=@STATEMENTCODE 
	GROUP BY ML.MarketLetterCode ,ML.[Sequence] ,ML.MarketLetterName

	IF @REGION='WEST' --AND @SEASON=2020
	BEGIN
		DELETE T1
		FROM #REWARD_SUMMARY  T1
			LEFT JOIN  #ML_Level_Sales MLS
			ON MLS.MarketLetterCode=T1.MarketLetterCode 
		WHERE T1.MarketLetterCode='ML' + @SEASON_STRING + '_W_LIBERTY_200' AND ISNULL(MLS.[CYSales],0) <= 0
	END 

	-- RP 2927
	IF @REGION = 'EAST' 
	BEGIN
		DELETE T1
		FROM #REWARD_SUMMARY  T1
			LEFT JOIN  #ML_Level_Sales MLS
			ON MLS.MarketLetterCode=T1.MarketLetterCode 
		WHERE T1.MarketLetterCode='ML' + @SEASON_STRING + '_E_INV' AND ISNULL(MLS.[CYSales],0) <= 0
	END
	   
	IF @LANGUAGE = 'F'
		SET @ML_SUMMARY_THEAD_ROW = '
		<tr>
			<th>Récompenses prévues dans la lettre de commercialisation</th>
			<th>Récompense (%)</th>
			<th>Total</th>
			<th>Paiement antérieur</th>
			<th>Paiement actuel</th>
			' + IIF(@VERSIONTYPE IN ('Locked','Archive'),'','<th>Prochain paiement</th>') + '
		</tr>'
	ELSE
		SET @ML_SUMMARY_THEAD_ROW = '
		<tr>
			<th>Market Letter Rewards</th>
			<th>Reward %</th>
			<th>Total</th>
			<th>Previous Payment</th>
			<th>Current Payment</th>
			' + IIF(@VERSIONTYPE IN ('Locked','Archive'),'','<th>Next Payment</th>') + '
		</tr>'

	SELECT @TOTAL_REWARD=SUM(TotalReward) ,@TOTAL_POG_AMOUNT=SUM(POG_Amount)	FROM #REWARD_SUMMARY


	IF @TOTAL_POG_AMOUNT = 0 
		SET @REARD_MARGIN=0		
	ELSE
		SET @REARD_MARGIN=@TOTAL_REWARD/@TOTAL_POG_AMOUNT

	
	-- EACH MARKET LETTER	
	SELECT @ML_SUMMARY_BODY = ISNULL(CONVERT(NVARCHAR(MAX),(
									SELECT	(SELECT MarketLetterName as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(IIF(POG_Amount=0,0,TotalReward/POG_Amount),@PERCENT_FORMAT) as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
							FROM #REWARD_SUMMARY 
							ORDER BY MarketLetterName
							FOR XML RAW('tr'), ELEMENTS, TYPE)) , '')



	-- TOTAL ROW		
	SELECT @ML_SUMMARY_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT 'total_row' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(@REARD_MARGIN,@PERCENT_FORMAT) as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
				FROM (
						SELECT SUM(TotalReward) AS [TotalReward]
							,SUM(PaidToDate) AS [PaidToDate]
							,SUM(CurrentPayment) AS [CurrentPayment]
							,SUM(NextPayment) AS [NextPayment]
							,MAX(POG_Amount) AS POG_Amount
						FROM #REWARD_SUMMARY						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')
				
						
		SET @ML_SUMMARY_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>MARKET LETTER REWARDS SUMMARY</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @ML_SUMMARY_THEAD_ROW + @ML_SUMMARY_BODY  + '
				</table>
			</div>
		</div>' 


	/************************  OTHER REWARD  ***********************************/
	/*
	ROW #1 - Exceptions	
	ROW #2 - Incentives - Applicable for 2020 only. Starting 2021 , Incentives are displayed in its own section
	ROW #3 - Additional Programs 
	*/

	DROP TABLE IF EXISTS #OTHER_REWARDS_SUMMARY 
	SELECT ML.RewardCode, 3 AS [Sequence] ,ML.RewardLabel As MarketLetterName
		,SUM(ISNULL(rs.TotalReward,0)) AS TotalReward
		,SUM(ISNULL(rs.PaidToDate,0)) AS PaidToDate
		,SUM(ISNULL(rs.CurrentPayment,0)) AS CurrentPayment
		,SUM(ISNULL(rs.NextPayment,0)) AS NextPayment
		,MAX(ISNULL(MLS.Eligible_Amount,0)) AS POG_Amount
 	INTO #OTHER_REWARDS_SUMMARY
	FROM RP_Config_Rewards_Summary ML			
		INNER JOIN (
			SELECT *
			FROM RPWeb_Rewards_Summary RS
			WHERE RS.MarketLetterCode ='ML'+ @SEASON_STRING +'_'+IIF(@REGION = 'WEST','W','E') +'_ADD_PROGRAMS'						
		) RS
		ON RS.RewardCode=ML.RewardCode 
		LEFT JOIN (
			SELECT MarketLetterCode, [CYSales] AS [Actual_Amount], CYEligibleSales AS [Eligible_Amount] 
			FROM #ML_Level_Sales			
		) MLS
		ON MLS.MarketLetterCode=RS.MarketLetterCode
	WHERE RS.StatementCode=@STATEMENTCODE AND ML.Season=@SEASON AND ML.Region=@REGION 		
	GROUP BY ML.RewardCode ,ML.RewardLabel

	IF @SEASON >= 2021
		DELETE #OTHER_REWARDS_SUMMARY WHERE RewardCode = 'INCENTIVES_W'

	SELECT @TOTAL_REWARD=SUM(TotalReward) ,@TOTAL_POG_AMOUNT=MAX(POG_Amount)	FROM #OTHER_REWARDs_SUMMARY
	
	IF @TOTAL_POG_AMOUNT = 0 
		SET @REARD_MARGIN=0		
	ELSE
		SET @REARD_MARGIN=@TOTAL_REWARD/@TOTAL_POG_AMOUNT

  	
	IF NOT EXISTS(SELECT * FROM #OTHER_REWARDS_SUMMARY)
		BEGIN			
			SET @OTHER_REWARDS_SECTION=''
		END 
	ELSE
		BEGIN
			IF @LANGUAGE = 'F' 
				SET @OTHER_REWARDS_THEAD_ROW = '
				<tr>
					<th>Programmes supplémentaires</th>' +
					IIF(@RETAILERCODE = 'D0000137', '<th>Récompense (%)</th>', '') +
					'<th>Total</th>
					<th>Paiement antérieur</th>
					<th>Paiement actuel</th>' +
					IIF( (@VERSIONTYPE IN ('Locked','Archive') OR @LEVEL5CODE in ('xD000001')), '', '<th>Prochain paiement</th>') +
				'</tr>'
			ELSE
				SET @OTHER_REWARDS_THEAD_ROW = '
				<tr>
					<th>Additional Programs</th>' +
					IIF(@RETAILERCODE = 'D0000137', '<th>Reward %</th>', '') +
					'<th>Total</th>
					<th>Previous Payment</th>
					<th>Current Payment</th>' +
					IIF( (@VERSIONTYPE IN ('Locked','Archive') OR @LEVEL5CODE in ('xD000001')), '', '<th>Next Payment</th>') +
				'</tr>'

		-- DETATILS ROW	
		SELECT @OTHER_REWARDS_BODY = CONVERT(NVARCHAR(MAX),(
								SELECT	(SELECT  'formatReportMain' as [td/@class], MarketLetterName as 'td' for xml path(''), type)						
								,IIF(@RETAILERCODE = 'D0000137', (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(IIF(POG_Amount=0,0,TotalReward/POG_Amount),@PERCENT_FORMAT) as 'td' for xml path(''), type), NULL)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,IIF(@VERSIONTYPE IN ('Locked','Archive') OR @LEVEL5CODE  in ('xD000001'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
						FROM #OTHER_REWARDS_SUMMARY 
						ORDER BY [Sequence]
						FOR XML RAW('tr'), ELEMENTS, TYPE))


		-- TOTAL ROW
		--@RETAILERCODE = 'D0000137' OR 
		SELECT @OTHER_REWARDS_BODY += CONVERT(NVARCHAR(MAX), (
							SELECT 'total_row' as [@class], (
								SELECT 'Total' AS 'td' for xml path(''), type) 
							--	,(SELECT 'right_align' as [td/@class] ,'' as 'td' for xml path(''), type)
								,IIF(@RETAILERCODE = 'D0000137', (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(@REARD_MARGIN,@PERCENT_FORMAT) as 'td' for xml path(''), type), NULL)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,IIF(@VERSIONTYPE IN ('Locked','Archive') OR @LEVEL5CODE in ('xD000001'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
							FROM (
								SELECT SUM(TotalReward) AS [TotalReward]
									,SUM(PaidToDate) AS [PaidToDate]
									,SUM(CurrentPayment) AS [CurrentPayment]
									,SUM(NextPayment) AS [NextPayment]								
								FROM #OTHER_REWARDS_SUMMARY						
							) D
							FOR XML PATH('tr'), ELEMENTS, TYPE))

			SET @OTHER_REWARDS_SECTION += '<div class="st_section">
				<div class = "st_header" style="display: none;">
					<span>OTHER REWARDS SUMMARY</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">' + @OTHER_REWARDS_THEAD_ROW + @OTHER_REWARDS_BODY  + '
					</table>
				</div>
			</div>' 
		END						
						
	/************* One Time Payment Section ****************/

	DROP TABLE IF EXISTS #OTP_REWARDS_SUMMARY 

	-- Remove OTP for EAST RP-3000
	IF @SEASON=2020 AND @REGION='WEST'
	BEGIN
		SELECT CFG.RewardCode ,CFG.RewardLabel 
			,SUM(ISNULL(rs.PaymentAmount,0)) AS PaymentAmount			
			,SUM(IIF(rs.CurrentPayment=0 AND rs.PaidToDate=0 AND RS.NextPayment=0, rs.PaymentAmount, rs.PaidToDate) ) AS PaidToDate
			,SUM(rs.CurrentPayment) AS CurrentPayment
			,SUM(rs.NextPayment) AS NextPayment 	
		INTO #OTP_REWARDS_SUMMARY
		FROM RP_Config_Rewards_Summary CFG
			INNER JOIN (
				SELECT RewardCode, PaymentAmount, PaidToDate, CurrentPayment, NextPayment
				FROM RPWeb_Rewards_Summary 
				WHERE StatementCode=@STATEMENTCODE
			) RS				
			ON CFG.RewardCode=RS.RewardCode 
		WHERE CFG.Season=@SEASON AND CFG.Region=@REGION AND CFG.RewardType='OTP'		
		GROUP BY CFG.RewardCode ,CFG.RewardLabel 

		IF EXISTS(SELECT * FROM #OTP_REWARDS_SUMMARY)	
		BEGIN

			SET @OTP_REWARDS_THEAD_ROW = '
			<tr>
				<th>One Time Payment</th>
				<th>Payment Amount</th>
				<th>Previous Payment</th>
				<th>Current Payment</th>' +
			--	IIF(@VERSIONTYPE IN ('Locked','Archive'), '', '<th>Next Payment</th>') +
			'</tr>'
		

			-- DETAILS ROW	
			SELECT @OTP_REWARDS_BODY = CONVERT(NVARCHAR(MAX),(
									SELECT	(SELECT  'formatReportMain' as [td/@class], RewardLabel as 'td' for xml path(''), type)						
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaymentAmount,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
				--					,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,'$') as 'td' for xml path(''), type))
							FROM #OTP_REWARDS_SUMMARY 
							-- ORDER BY [Sequence]
							FOR XML RAW('tr'), ELEMENTS, TYPE))

			-- TOTAL ROW
			SELECT @OTP_REWARDS_BODY += CONVERT(NVARCHAR(MAX), (
								SELECT 'total_row' as [@class], (
									SELECT 'Total' AS 'td' for xml path(''), type) 			
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaymentAmount,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
									,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
				--					,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,'$') as 'td' for xml path(''), type))
								FROM (
									SELECT SUM(PaymentAmount) AS [PaymentAmount]
										,SUM(PaidToDate) AS [PaidToDate]
										,SUM(CurrentPayment) AS [CurrentPayment]
										,SUM(NextPayment) AS [NextPayment]								
									FROM #OTP_REWARDS_SUMMARY						
								) D
								FOR XML PATH('tr'), ELEMENTS, TYPE))


			SET @OTP_REWARDS_SECTION += '<div class="st_section">
				<div class = "st_header" style="display: none;">
					<span>ONE TIME PAYMENTS SUMMARY</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">' + @OTP_REWARDS_THEAD_ROW + @OTP_REWARDS_BODY  + '
					</table>
				</div>
			</div>' 

			SET @OTP_REWARDS_SECTION = NULL -- (Remove OTP for EAST RP-3000)
	
		END
	END
	
	/************* Exceptions Section ****************/
	-- STARTING 2021 - SHOW OTHER EXCEPTIONS ONLY
	
	DROP TABLE IF EXISTS #IEX_REWARDS_SUMMARY 

	 ---locked/archive regular statements
	SELECT EX.RewardCode ,EX.Exception_Desc AS RewardLabel,S.VersionType,EX.Exception_Type
		 ,EX.Exception_Value as TotalReward
		,IIF(EX.VersionType='Historical', EX.Exception_Value,0) AS PaidToDate
		,IIF(EX.VersionType='Current', EX.Exception_Value,0) AS CurrentPayment
		,0 AS NextPayment
	INTO #IEX_REWARDS_SUMMARY 
	FROM RPWeb_Exceptions ex
		INNER JOIN (
			SELECT StatementCode, RewardCode, SUM(PaidToDate) As PaidToDate, SUM(NextPayment) AS NextPayment
			FROM RPWeb_Rewards_Summary
			WHERE StatementCode=@StatementCode AND StatementCode < 0
			GROUP BY StatementCode, RewardCode
		) RS
		ON RS.StatementCode=EX.StatementCode and EX.RewardCode = RS.RewardCode
		INNER JOIN RPWeb_Statements S 
		ON S.STatementCode = RS.STatementCode
	WHERE EX.Season = @Season AND EX.Status = 'Approved'  AND @VERSIONTYPE IN ('Locked','Archive')
		AND (@SEASON=2020 OR EX.RewardCode IN ('OTHER_W_X','OTHER_E_X','OTHER_WX','OTHER_EX'))

		UNION ALL

	 --unlocked regular statements
	 SELECT EX.RewardCode,EX.Exception_Desc AS RewardLabel,'Unlocked' AS VersionType,EX.Exception_Type
		 ,EX.Exception_Value as TotalReward
		,IIF(ARC.VersionType='Archive', EX.Exception_Value,0) AS PaidToDate			
		,IIF(ARC.VersionType='Locked', EX.Exception_Value,0) AS CurrentPayment										
		,ISNULL(RS.NextPayment,0) AS NextPayment
	FROM RPWeb_Exceptions ex
		INNER JOIN (
			SELECT StatementCode, RewardCode, SUM(PaidToDate) As PaidToDate, SUM(NextPayment) AS NextPayment
			FROM RPWeb_Rewards_Summary
			WHERE StatementCode = @StatementCode AND StatementCode > 0
			GROUP BY StatementCode, RewardCode
		) RS
	ON RS.StatementCode=EX.SRC_StatementCode and EX.RewardCode = RS.RewardCode
			LEFT JOIN RPWeb_Statements Arc 
			ON ARC.StatementCode = ex.Statementcode AND ARC.[Status] = 'Active' 
	WHERE EX.Show_IN_UL=1 AND EX.[Status]='Approved' AND EX.Season = @Season AND EX.VersionType='Current'
		AND (@SEASON=2020 OR EX.RewardCode IN ('OTHER_W_X','OTHER_E_X','OTHER_WX','OTHER_EX'))


		UNION ALL

	 --locked/archive summary statements

	SELECT EX.RewardCode ,EX.Exception_Desc AS RewardLabel,S.VersionType,EX.Exception_Type
		,SUM(ISNULL(EX.Exception_Value,0)) AS TotalReward
		,SUM(ISNULL(IIF(rs.CurrentPayment=0 AND rs.PaidToDate=0 AND RS.NextPayment=0, rs.TotalReward, rs.PaidToDate),0)) AS PaidToDate		
		,SUM(ISNULL( rs.CurrentPayment,0)) AS CurrentPayment
		,SUM(ISNULL(rs.NextPayment,0)) AS NextPayment			
	FROM RPWeb_Exceptions EX 
		INNER JOIN RPWeb_Rewards_Summary RS  ON EX.RewardCode = RS.RewardCode AND RS.StatementCode = EX.StatementCode AND RS.StatementCode < 0
		INNER JOIN RPWeb_Statements S ON RS.StatementCode = S.StatementCode AND S.DataSetCode_L5=@StatementCode
	 WHERE  @Summary='Yes' AND EX.Season = @Season AND EX.Status = 'Approved'	
		AND (@SEASON=2020 OR EX.RewardCode IN ('OTHER_W_X','OTHER_E_X','OTHER_WX','OTHER_EX'))
	GROUP BY EX.RewardCode ,EX.Exception_Desc, S.VersionType,EX.Exception_Type
		

		UNION ALL
		
	--unlocked summary statements
	 SELECT EX.RewardCode ,EX.Exception_Desc AS RewardLabel ,S.VersionType,EX.Exception_Type 
		,SUM(ISNULL(EX.Exception_Value,0)) AS TotalReward
		,SUM(ISNULL(IIF(rs.CurrentPayment=0 AND rs.PaidToDate=0 AND RS.NextPayment=0, rs.TotalReward, rs.PaidToDate),0)) AS PaidToDate
		,SUM(ISNULL(rs.CurrentPayment,0)) AS CurrentPayment
		,SUM(ISNULL(rs.NextPayment,0)) AS NextPayment			
	FROM RPWeb_Exceptions EX 
		INNER JOIN RPWeb_Rewards_Summary RS ON EX.RewardCode = RS.RewardCode AND RS.StatementCode = EX.SRC_StatementCode AND RS.StatementCode > 0
		INNER JOIN RPWeb_Statements S ON RS.StatementCode = S.StatementCode AND S.DataSetCode_L5=@STATEMENTCODE 
	WHERE @Summary='Yes' AND EX.Season=@Season AND EX.[Status]='Approved' AND ISNULL(EX.Show_IN_UL,0) = 1  AND EX.VersionType='Current'
		AND (@SEASON=2020 OR EX.RewardCode IN ('OTHER_W_X','OTHER_E_X','OTHER_WX','OTHER_EX'))
	GROUP BY EX.RewardCode ,EX.Exception_Desc, S.VersionType,EX.Exception_Type
	
	IF EXISTS(SELECT * FROM #IEX_REWARDS_SUMMARY)
	BEGIN

		IF @LANGUAGE = 'F'
			SET @IEX_REWARDS_THEAD_ROW = '
			<tr>
				<th>Exception(s)</th>
				<th>Total</th>
				<th>Paiement antérieur</th>
				<th>Paiement actuel</th>' +
				IIF(@VERSIONTYPE IN ('Locked','Archive'), '', '<th>Prochain paiement</th>') +
			'</tr>'
		ELSE
			SET @IEX_REWARDS_THEAD_ROW = '
			<tr>
				<th>Exception(s)</th>
				<th>Total</th>
				<th>Previous Payment</th>
				<th>Current Payment</th>' +
				IIF(@VERSIONTYPE IN ('Locked','Archive'), '', '<th>Next Payment</th>') +
			'</tr>'

		-- DETAILS ROW	
		SELECT @IEX_REWARDS_BODY = CONVERT(NVARCHAR(MAX),(
								SELECT	(SELECT  'formatReportMain' as [td/@class], RewardLabel as 'td' for xml path(''), type)						
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
						FROM #IEX_REWARDS_SUMMARY
						WHERE VersionType = @VERSIONTYPE
						-- ORDER BY [Sequence]
						FOR XML RAW('tr'), ELEMENTS, TYPE))

		-- TOTAL ROW
		SELECT @IEX_REWARDS_BODY += CONVERT(NVARCHAR(MAX), (
							SELECT 'total_row' as [@class], (
								SELECT 'Total' AS 'td' for xml path(''), type) 			
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
								,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
							FROM (
								
												SELECT
												 SUM(PaidToDate) AS [PaidToDate]
												,SUM(TotalReward) AS [TotalReward]
												,SUM(CurrentPayment) AS [CurrentPayment]
												,SUM(NextPayment) AS [NextPayment]
											FROM #IEX_REWARDS_SUMMARY
											WHERE VersionType = @VERSIONTYPE
											
										) D
								

							FOR XML PATH('tr'), ELEMENTS, TYPE))


		SET @IEX_REWARDS_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>INTERNAL EXCEPTIONS</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @IEX_REWARDS_THEAD_ROW + @IEX_REWARDS_BODY  + '
				</table>
			</div>
		</div>' 

	END						


	/******************************** ESTIMATES SECTION ********************************/
	IF @SEASON >= 2021 --AND @LEVEL5CODE='D000001' 
	BEGIN		
		DECLARE @INCENTIVES_THEAD_ROW NVARCHAR(MAX)='';
		DECLARE @INCENTIVES_BODY NVARCHAR(MAX)='';
		DECLARE @INCENTIVES_SECTION NVARCHAR(MAX)='';

		DROP TABLE IF EXISTS #INCENTIVES_REWARDS_AMOUNTS
		
		SELECT StatementCode,RewardCode
			--,IIF(RewardCode='EST_OCT_ADJ_INC_W', PaymentAmount, TotalReward) AS TotalReward
			,IIF(RewardCode='INCENTIVES_W', TotalReward, IIF(CurrentPayment> 0,PaymentAmount,0)) AS TotalReward
			,PaidToDate
			,CurrentPayment
			,NextPayment		
			,IIF(RewardCode='EST_OCT_ADJ_INC_W',1,2) AS [RowSequence]			
			,TotalReward AS Temp_TotalReward
		INTO #INCENTIVES_REWARDS_AMOUNTS
		FROM RPWeb_Rewards_Summary
		WHERE StatementCode=@StatementCode	AND RewardCode IN ('EST_OCT_ADJ_INC_W','INCENTIVES_W')
				
		SET @INCENTIVES_THEAD_ROW='
		<thead>
		<tr>
			<th>Incentives</th>			
			<th>Total</th>
			<th>Previous Payment</th>
			<th>Current Payment</th>
			' + IIF(@VERSIONTYPE IN ('Locked','Archive'),'','<th>Next Payment</th>') + '
		</tr>
		</thead>';
		--  'INCENTIVES_W','EST_OCT_ADJ_INC_W','EST_OCT_ADJ_HERB_W'

		SET @INCENTIVES_BODY = CONVERT(NVARCHAR(MAX),(
						SELECT	(SELECT CFG.RewardLabel as 'td' for xml path(''), type)						
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.TotalReward,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.PaidToDate,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.CurrentPayment,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,iif(@VERSIONTYPE IN ('Locked','Archive'),NULL,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.NextPayment,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type)) 
				FROM #INCENTIVES_REWARDS_AMOUNTS RS
					INNER JOIN RP_Config_Rewards_Summary CFG
					ON CFG.RewardCode=RS.RewardCode AND CFG.Season=@SEASON				
				ORDER BY RS.RowSequence
				FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		SELECT @INCENTIVES_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT 'total_row' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 					
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
				FROM (
						SELECT SUM(TotalReward) AS [TotalReward]
							,SUM(PaidToDate) AS [PaidToDate]
							,SUM(CurrentPayment) AS [CurrentPayment]
							,SUM(NextPayment) AS [NextPayment]							
						FROM #INCENTIVES_REWARDS_AMOUNTS						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')

			SET @INCENTIVES_SECTION = '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>INCENTIVES</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @INCENTIVES_THEAD_ROW + @INCENTIVES_BODY  + '
				</table>
			</div>
		</div> '

		-- ESTIAMTE REDUCTION OCTOBER NET AMOUNT OWING
		DROP TABLE IF EXISTS #ESTIMATES_REWARDS

		SELECT StatementCode,RewardCode	,PaymentAmount AS TotalReward,PaidToDate,CurrentPayment,NextPayment					
		INTO #ESTIMATES_REWARDS
		FROM RPWeb_Rewards_Summary
		WHERE StatementCode=@StatementCode	AND RewardCode IN ('EST_OCT_ADJ_HERB_W') -- may be new estimate reward code may come in
			   		 	
						
		SET @ESTIMATES_THEAD_ROW = '
			<thead>
			<tr>
				<th>Estimates Reducing October Net Amount Owing</th>			
				<th>Total</th>
				<th>Previous Payment</th>
				<th>Current Payment</th>
				' + IIF(@VERSIONTYPE IN ('Locked','Archive'),'','<th>Next Payment</th>') + '
			</tr>
			</thead>'

		SET @ESTIMATES_BODY = CONVERT(NVARCHAR(MAX),(
						SELECT	(SELECT CFG.RewardLabel as 'td' for xml path(''), type)						
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.TotalReward,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.PaidToDate,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.CurrentPayment,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type) 
						,iif(@VERSIONTYPE IN ('Locked','Archive'),NULL,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(ISNULL(rs.NextPayment,0),@CURRENCY_FORMAT)  as 'td' for xml path(''), type)) 
				FROM #ESTIMATES_REWARDS RS
					INNER JOIN RP_Config_Rewards_Summary CFG
					ON CFG.RewardCode=RS.RewardCode AND CFG.Season=@SEASON								
				FOR XML RAW('tr'), ELEMENTS, TYPE))
		
		SELECT @ESTIMATES_BODY += ISNULL( CONVERT(NVARCHAR(MAX), (
					SELECT 'total_row' as [@class], (SELECT 'Total' AS 'td' for xml path(''), type) 					
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(PaidToDate,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(CurrentPayment,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
					,IIF(@VERSIONTYPE IN ('Locked','Archive'), NULL, (SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(NextPayment,@CURRENCY_FORMAT) as 'td' for xml path(''), type))
				FROM (
						SELECT SUM(TotalReward) AS [TotalReward]
							,SUM(PaidToDate) AS [PaidToDate]
							,SUM(CurrentPayment) AS [CurrentPayment]
							,SUM(NextPayment) AS [NextPayment]							
						FROM #ESTIMATES_REWARDS						
				) D
				FOR XML PATH('tr'), ELEMENTS, TYPE)), '')

		
		-- THIS CODE IS ALSO REPEATED FOR SEASON <= 2020, NO NEED TO CHANGE THAT IF A REQUEST WAS SUBMITTED TO MAKE SOME CHANGES
		SET @ESTIMATES_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>ESTIMATES REDUCING OCTOBER NET AMOUNT OWING</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @ESTIMATES_THEAD_ROW + @ESTIMATES_BODY  + '
				</table>
			</div>
		</div> '

		
	END -- 2021+



	IF @LEVEL5CODE='D000001' AND @SEASON <= 2020
	BEGIN		
		DROP TABLE IF exists #estimates
		CREATE TABLE #estimates (ID int, Amount money)

		DECLARE @NEXT_PAYMENT MONEY=0,
				@CURRENT_PAYMENT MONEY=0,
				@PAID_TO_DATE MONEY=0;
		DECLARE @ESTIMATE_TOTAL MONEY=0;
		DECLARE @OCT_NET_AMOUNT MONEY=0;
		
		DECLARE  @ESTIMATE_LINE_ITEMS TABLE( 
			[ID] INT NOT NULL,
			[Label] [Varchar](100) NOT NULL			
		)
		
		insert into #estimates
		SELECT [ID], ISNULL(Amount,0) AS Amount
		FROM (				
				SELECT 1 AS [ID], Amount FROM RPWeb_Est_Incentives WHERE Statementcode=@Statementcode AND Amount <> 0
					UNION 
				SELECT 2 AS [ID], Amount FROM RPWeb_Est_HeatDistinctTopUp WHERE Statementcode=@Statementcode AND Amount <> 0
					UNION 
				SELECT 3 AS [ID], Amount FROM RPWeb_Est_Exceptions WHERE Statementcode=@Statementcode AND Amount <> 0
					UNION 
				SELECT 4 AS [ID], Amount FROM RPWeb_Est_MarginTopUp WHERE Statementcode=@Statementcode AND Amount <> 0
			) D

		IF EXISTS(SELECT * FROM #Estimates) 
		BEGIN
			SELECT @NEXT_PAYMENT=SUM(NextPayment) FROM #REWARD_SUMMARY
			SELECT @ESTIMATE_TOTAL=SUM(Amount) FROM #Estimates

			SET @OCT_NET_AMOUNT=ISNULL(@NEXT_PAYMENT,0)+ISNULL(@ESTIMATE_TOTAL,0)

			INSERT INTO @ESTIMATE_LINE_ITEMS([ID], [Label])
			VALUES(1,'Retail Incentive Estimate')
			,(2,'Late Season Herbicide Estimate')
			,(3,'Market Letter Exception Estimate')
			,(4,'October Adjustment Estimate')
									
			SET @ESTIMATES_THEAD_ROW='<thead><tr><th>Estimates Reducing October Net Amount Owing</th><th>Total</th></tr></thead>';
		
			-- DETAILS ROW	
			SELECT @ESTIMATES_BODY = CONVERT(NVARCHAR(MAX),(
					SELECT	(SELECT T1.[Label] as 'td' for xml path(''), type)						
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(T2.Amount,@CURRENCY_FORMAT)  as 'td' for xml path(''), type)
			FROM @ESTIMATE_LINE_ITEMS T1
				INNER JOIN #Estimates  T2	ON T2.ID=T1.ID			
			ORDER BY T1.[ID]
			FOR XML RAW('tr'), ELEMENTS, TYPE))

			-- TOTAL ROW
			SELECT @ESTIMATES_BODY += CONVERT(NVARCHAR(MAX), (
				SELECT 'total_row' as [@class], (
					SELECT 'Total' AS 'td' for xml path(''), type) 								
					,(SELECT 'right_align' as [td/@class] ,dbo.SVF_Commify(TotalAmount,@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				FROM (
					SELECT SUM(Amount) AS [TotalAmount]	FROM #Estimates
				) D
			FOR XML PATH('tr'), ELEMENTS, TYPE))

			SET @ESTIMATES_SECTION += '<div class="st_section">
			<div class = "st_header" style="display: none;">
				<span>ESTIMATES REDUCING OCTOBER NET AMOUNT OWING</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">
				<table class="rp_report_table">' + @ESTIMATES_THEAD_ROW + @ESTIMATES_BODY  + '
				</table>
			</div>
		</div> '
		END

		/*
			<div class="st_section">
				<div class = "st_header" style="display: none;">
					<span>NET AMOUNT</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<tr>
							<td>October Statement Reduction</td>
							<td class="right_align">' + dbo.SVF_Commify(@OCT_NET_AMOUNT,'$')  + '</td>
						</tr>
					</table>
				</div>
			</div>	
		*/
			
	END
	
	   	  
	/* REWARD SUMMARY SECTION */

	/***** FINAL SECTION *****/
	/*
	DECLARE @LOCK_ARCHIVED_STATEMENTS TABLE (
		StatementCode INT NOT NULL
	)

	IF @VERSIONTYPE='UNLOCKED'
		INSERT INTO @LOCK_ARCHIVED_STATEMENTS(StatementCode)
		SELECT StatementCode
		FROM RPWeb_Statements 
		WHERE SRC_StatementCode=@STATEMENTCODE AND [Status]='Active' AND Versiontype IN ('Locked','Archive')
	ELSE
		INSERT INTO @LOCK_ARCHIVED_STATEMENTS(StatementCode)
		SELECT StatementCode
		FROM RPWeb_Statements 
		WHERE SRC_StatementCode=@SRC_STATEMENTCODE AND [Status]='Active' AND Versiontype IN ('Locked','Archive') AND Locked_Date <= @LOCKED_DATE
	*/
	

	DECLARE @PAYMENT_DETAILS TABLE (		
		StatementCode INT NOT NULL,				
		VersionType VARCHAR(50) NOT NULL,
		Locked_Date DATE NULL,
		Row_ID INT NOT NULL,
		PaymentMonth VARCHAR(200) NOT NULL DEFAULT '',				
		TotalReward MONEY NOT NULL DEFAULT 0,
		PaidToDate MONEY NOT NULL DEFAULT 0,
		CurrentPayment MONEY NOT NULL DEFAULT 0,
		NextPayment MONEY NOT NULL  DEFAULT 0		
		
	)

	INSERT INTO @PAYMENT_DETAILS(StatementCode, VersionType, PaymentMonth, Row_ID)
	SELECT @STATEMENTCODE, @VersionType, 'Total' AS PaymentMonth, 999 AS Row_ID
	
	INSERT INTO @PAYMENT_DETAILS(StatementCode, VersionType, Locked_Date, PaymentMonth, Row_ID)
	SELECT S.StatementCode, S.VersionType, S.Locked_Date	
		,ISNULL(CR.Label, DATENAME(MONTH,LOCKED_DATE)  + ' Payment') AS PaymentMonth
		,ROW_NUMBER ( ) OVER ( ORDER BY S.StatementCode)   AS Row_ID
	FROM RPWeb_Statements  S
		LEFT JOIN RP_Config_ChequeRuns CR
		ON CR.ID=S.ChequeRunID
	WHERE S.SRC_StatementCode=@SRC_STATEMENTCODE AND S.[Status]='Active' AND S.Versiontype IN ('Locked','Archive')
		AND (@VERSIONTYPE='UNLOCKED' OR Locked_Date < @LOCKED_DATE)
		

	UPDATE PD
	SET TotalReward = T2.TotalReward
		,PaidToDate = T2.PaidToDate + IIF(PD.VersionType='Unlocked', T2.CurrentPayment, 0)
		,CurrentPayment = T2.CurrentPayment
		,NextPayment = T2.NextPayment
	FROM @PAYMENT_DETAILS PD
		INNER JOIN (
			SELECT T1.StatementCode	
				,SUM(ISNULL(RS.PaymentAmount,RS.TotalReward)) AS TotalReward								
				,SUM(RS.PaidToDate) AS PaidToDate
				,SUM(RS.CurrentPayment) AS CurrentPayment				
				,SUM(RS.NextPayment) AS NextPayment
			FROM @PAYMENT_DETAILS T1
				INNER JOIN RPWeb_Rewards_Summary  RS
				ON RS.StatementCode=T1.StatementCode
			GROUP BY T1.StatementCode
		) T2
		ON T2.StatementCode=PD.StatementCode

		IF @LANGUAGE = 'F'
			SET @REWARDS_DETAILS_THEAD_ROW='
				<thead>
				<tr>
					<th>Détails du paiement</th>				
					<th>Récompense totale</th>
					<th>Paiement(s) antérieur(s)</th>
					<th>' + IIF(@VERSIONTYPE='UNLOCKED', 'Prochain paiement', 'Paiement actuel') + '</th>
				</tr>
				</thead>'
		ELSE
			SET @REWARDS_DETAILS_THEAD_ROW='
			<thead>
			<tr>
				<th>Payment Details</th>				
				<th>Total Reward</th>
				<th>Previous Payment(s)</th>
				<th>' + IIF(@VERSIONTYPE='UNLOCKED', 'Next Payment', 'Current Payment') + '</th>
			</tr>
			</thead>'

		SELECT @REWARDS_DETAILS_BODY = CONVERT(NVARCHAR(MAX),ISNULL(
			(SELECT IIF(StatementCode=@StatementCode, 'total_row', 'sub_total_row') as [@class]
				,(SELECT PaymentMonth  AS 'td' for xml path(''), type) 			
				,(SELECT 'right_align reward_amount' as [td/@class] ,IIF(StatementCode = @StatementCode,dbo.SVF_Commify(TotalReward,@CURRENCY_FORMAT),'') as 'td' for xml path(''), type)
				,(SELECT 'right_align reward_amount' as [td/@class] ,dbo.SVF_Commify(IIF(StatementCode = @StatementCode, PaidToDate, CurrentPayment),@CURRENCY_FORMAT) as 'td' for xml path(''), type)
				,(SELECT 'right_align reward_amount' as [td/@class] ,IIF(StatementCode = @StatementCode,dbo.SVF_Commify(IIF(@VERSIONTYPE='UNLOCKED',NextPayment,CurrentPayment),@CURRENCY_FORMAT),'') as 'td' for xml path(''), type)
		FROM @PAYMENT_DETAILS
		WHERE totalReward > 0 OR PaidToDate > 0 OR CurrentPayment > 0 OR  NextPayment > 0
		ORDER BY ROW_ID  
		FOR XML PATH('tr'), ELEMENTS, TYPE),''))

	


	SET @REWARDS_DETAILS_SECTION += '<div class="st_section">
		<div class = "st_header" style="display: none;">
			<span>PAYMENT DETAILS</span>
			<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
			<span class="st_right"</span>
		</div>
		<div class="st_content open">
			<table class="rp_report_table">' + @REWARDS_DETAILS_THEAD_ROW + @REWARDS_DETAILS_BODY  + '
			</table>
		</div>
	</div>' 


	   

FINAL:

	/***** DISCLAIMER TEXT IN ITS OWN SECTION *****/
	IF @RETAILERCODE IN ('D0000107', 'D0000137', 'D0000244', 'D520062427') -- West Line Companies
		SET @DISCLAIMER_TEXT = '
				<div class="rprew_subtabletext">*Eligible POG$ defined as: All Retail to Grower transactions in ' + CAST(@SEASON AS VARCHAR(4)) + ' Season that meet 150 km Rebate Radius Requirements.</div>
				<div class="rprew_subtabletext">*Non-Reward Brands defined as BASF brands that are not included in the qualification &/or calculation of ' + CAST(@SEASON AS VARCHAR(4)) + ' rewards programs.</div>
				<div class="rprew_subtabletext">*All POG$ are based on ' + CAST(@SEASON AS VARCHAR(4)) + ' invoice pricing.</div>'
	ELSE IF @LEVEL5CODE = 'D0000117'
		SET @DISCLAIMER_TEXT = '
					<div class="rprew_subtabletext">*Eligible POG$ defined as: All Retail to Grower transactions in ' + CAST(@SEASON AS VARCHAR(4)) + ' Season that meet 150 km Rebate Radius Requirements.</div>
					<div class="rprew_subtabletext">*Non-Reward Brands defined as BASF brands that are not included in the qualification &/or calculation of ' + CAST(@SEASON AS VARCHAR(4)) + ' rewards programs.</div>
					<div class="rprew_subtabletext">*All POG $ are based on ' + CAST(@SEASON AS VARCHAR(4)) + ' Suggested Retail Price.</div>'
	ELSE IF @LEVEL5CODE = 'D000001' AND @SEASON = 2021
		SET @DISCLAIMER_TEXT = '					
					<div class="rprew_subtabletext"><strong>Eligible POG $ defined as:</strong> All Retail to Grower transactions in ' + CAST(@SEASON AS VARCHAR(4)) + ' Season that meet 150 km Rebate Radius Requirements.</div>
					<div class="rprew_subtabletext"><strong>Non-Reward Brands defined as:</strong> BASF brands that are not included in the qualification &/or calculation of ' + CAST(@SEASON AS VARCHAR(4)) + ' rewards programs.</div>
					<div class="rprew_subtabletext"><strong>Note:</strong> All POG $ are based on ' + CAST(@SEASON AS VARCHAR(4)) + ' Suggested Dealer Pricing.</div>'
	ELSE
	BEGIN
		IF @LANGUAGE = 'F'
			SET @DISCLAIMER_TEXT = '
							<div class="rprew_subtabletext"><strong>Définition de VAS admissibles :</strong> Toutes les transactions entre détaillant et producteur effectuées durant la saison ' +  CAST(@SEASON AS VARCHAR(4)) + ' et qui respectent la règle du rayon de 150 km.</div>
							<div class="rprew_subtabletext"><strong>Définition de produits non récompensés :</strong> Produits BASF exclus du programme de récompenses ' + CAST(@SEASON AS VARCHAR(4)) + ' 2020 et/ou non considérés aux fins du calcul des récompenses.</div>
							<div class="rprew_subtabletext"><strong>Remarque :</strong> Toutes les VAS  de Protection des cultures sont basées sur les prix au détail suggérés de ' + CAST(@SEASON AS VARCHAR(4)) + '.</div>
							<div class="rprew_subtabletext"><strong>Remarque :</strong> Toutes les VAS d&#39;InVigor sont basées sur les prix au détail suggérés de ' + CAST(@SEASON AS VARCHAR(4)) + '.</div>'
		ELSE	
			SET @DISCLAIMER_TEXT = '
						<div class="rprew_subtabletext"><strong>Eligible POG $ defined as:</strong> All Retail to Grower transactions in ' + CAST(@SEASON AS VARCHAR(4)) + ' Season that meet 150 km Rebate Radius Requirements.</div>
						<div class="rprew_subtabletext"><strong>Non-Reward Brands defined as:</strong> BASF brands that are not included in the qualification &/or calculation of ' + CAST(@SEASON AS VARCHAR(4)) + ' rewards programs.</div>
						<div class="rprew_subtabletext"><Strong>Note:</strong> All Crop Protection POG $ are based on ' + CAST(@SEASON AS VARCHAR(4)) + ' Suggested Dealer Pricing.</div>
						<div class="rprew_subtabletext"><strong>Note:</strong> All InVigor POG $ are based on 2021 Suggested Retail Pricing.</div>'
	END


	SET @HTML_DATA = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_DATA += 	'<div class="rp_program_header">
		<h1>' + CASE WHEN @LANGUAGE = 'F' THEN 'SOMMAIRE DU RELEVÉ' ELSE 'STATEMENT SUMMARY' END + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_DATA += ISNULL(@POG_SUMMARY_SECTION,'')
	SET @HTML_DATA += ISNULL(@ML_SUMMARY_SECTION,'')
	
	--IF @REGION='EAST'
	SET @HTML_DATA += ISNULL(@OTHER_REWARDS_SECTION,'')
	
	IF @LEVEL5CODE='D000001' AND @SEASON <= 2020 
		SET @HTML_DATA += ISNULL(@ESTIMATES_SECTION,'')
	ELSE 
		BEGIN
			SET @HTML_DATA += ISNULL(@INCENTIVES_SECTION,'')
			SET @HTML_DATA += ISNULL(@ESTIMATES_SECTION,'')
		END

	SET @HTML_DATA += ISNULL(@OTP_REWARDS_SECTION,'')
	SET @HTML_DATA += ISNULL(@IEX_REWARDS_SECTION,'')
	SET @HTML_DATA += ISNULL(@REWARDS_DETAILS_SECTION,'')
	SET @HTML_DATA += '<div class="st_section"><div class="st_content open">' +  @DISCLAIMER_TEXT + '</div></div>'
	
	SET @HTML_DATA += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_DATA = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')

	IF @Season <= 2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') 

	DROP TABLE IF EXISTS #SUMMARY_TEMP 
	DROP TABLE IF EXISTS #SUMMARY_TEMP1 
	
END
