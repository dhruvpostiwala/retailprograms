﻿




CREATE PROCEDURE [Reports].[RPWeb_Get_MarginToolData] @SEASON INT, @REGION VARCHAR(4) ,@RETAILER_TYPE VARCHAR(50)
AS
BEGIN

BEGIN TRY

	SET NOCOUNT ON

	DECLARE @JSON_DATA AS NVARCHAR(MAX);
/*
	DECLARE @Item_Code TABLE (
		Item_Code VARCHAR(50) NOT NULL
	)

	INSERT INTO  @Item_Code (Item_Code)
	VALUES	('CY_ML_Margin'),
			('PY_ML_Margin'),
			('CY_INC_Max'),
			('CY_INC_Est'),
			('CY_TOTAL_REWARD'),
			('CY_POG'),
			('CY_POG_PLAN'),
			('PY_POG'),
			('CY_INV_Margin'),
			('CY_INV_POG'),
			('CY_CCP_Margin'),
			('CY_CCP_POG'),
			('CY_ST_INC_Margin'),
			('CY_ST_INC_POG'),
			('CY_CER_PUL_SB_Margin'),
			('CY_CER_PUL_SB_POG'),
			('CY_LIBERTY_200_Margin'),
			('CY_LIBERTY_200_POG'),
			('ADD_PROGRAMS_TOTALREWARD'),
			('OPENING_INV'),
			('SAP_SALES'),
			('EST_RETURNS'),
			('EST_REBILL'),
			('EST_SALES'),
			('CALC_POG'),
			('HEAT_PreHar_Distinct_Est'),
			('Pre_Exceptions_Est');
*/
	DECLARE @LEVEL5CODE VARCHAR(50)='';

	DECLARE @TARGET_STATEMENTS TABLE (
		Statementcode INT,
		LockedStatementCode INT DEFAULT 0,
		RetailerCode VARCHAR(20)
	)

	DECLARE @STATEMENTS TABLE (
		StatementCode INT,
		LockedStatementCode INT,
		RetailerName Varchar(150),
		City VARCHAR(150) NULL,
		Province VARCHAR(150) NULL,
		Item_Code VARCHAR(50)  NULL		
	);

	/**DECLARE @RETAILER_TYPE VARCHAR (50) = 'CO-OP'
	DECLARE @SEASON int = 2020 **/
		
	IF @REGION='WEST' AND  @RETAILER_TYPE='IND'
		SET @LEVEL5CODE='D000001'
	IF @REGION='WEST' AND  @RETAILER_TYPE='FCL'
		SET @LEVEL5CODE='D0000117'
			

	IF @RETAILER_TYPE IN ('IND' , 'FCL')
		INSERT INTO @TARGET_STATEMENTS(StatementCode, RetailerCode)
			SELECT StatementCode, RetailerCode
			FROM RPWeb_Statements st
			WHERE  ST.Season=@SEASON AND ST.Region=@REGION AND ST.[Status]='ACTIVE' AND ST.VersionType='unlocked' AND ST.StatementType='Actual' 
				AND ST.LEVEL5Code=@LEVEL5CODE
		ELSE IF @REGION='WEST' AND @RETAILER_TYPE ='LC'	
			INSERT INTO @TARGET_STATEMENTS(StatementCode, RetailerCode)
			SELECT StatementCode, RetailerCode
			FROM RPWEb_Statements st
			WHERE  ST.Season=@SEASON AND ST.Region=@REGION AND ST.[Status]='ACTIVE' AND ST.VersionType='unlocked' AND ST.StatementType='Actual' 				
				AND ST.Level5Code IN ('D0000244','D520062427','D0000107','D0000137')
				AND ST.StatementLevel='LEVEL 5'

	UPDATE ST
	SET LockedStatementCode=LCK.Statementcode
	FROM @TARGET_Statements ST
		INNER JOIN RPWeb_Statements LCK
		ON LCK.SRC_Statementcode=st.Statementcode AND LCK.VersionType='Locked' AND LCK.Status='Active' AND LCK.ChequeRunName='OCT RECON'
	

	DELETE FROM @TARGET_Statements WHERE LockedStatementCode=0


	INSERT INTO @STATEMENTS(StatementCode, LockedStatementCode, Retailername, City, Province)
	SELECT rs.StatementCode, rs.LockedStatementcode, rp.Retailername, rp.MailingCity, RP.MailingProvince
	FROM @TARGET_STATEMENTS rs
		INNER JOIN RetailerProfile rp 
		on rs.retailercode = rp.retailercode
		
	SET @JSON_DATA =ISNULL(			
		(
			SELECT statementcode, retailername, city, province
				,JSON_QUERY( '{"' + STRING_AGG(item_code + '":"' + CAST(CAST(CAST(item_value AS FLOAT) AS DECIMAL(20,4)) AS nvarchar), '","') + '"}')  AS [metrics]
			FROM (
				Select s.statementcode,  s.retailername, s.city, S.province,LOWER(ms.item_code) as item_code, isnull(ms.item_value,0)  item_value
				From @Statements s
					left join [dbo].[RPWeb_MarginToolData_Summary] ms 
					on ms.statementcode = s.statementcode 
			
					union

				Select s.statementcode,  s.retailername, s.city, S.province,'top_up_amount' item_code, isnull(ms.amount,0) item_value
				From @Statements s
					LEFT JOIN [dbo].[RPWeb_Est_MarginTopUp] ms 			
					ON ms.statementcode = s.LockedStatementCode

					union

				Select s.statementcode,  s.retailername, s.city, S.province,'top_up_percentage' item_code, isnull(ms.Percentage,0) item_value
				From @Statements s
					LEFT JOIN [dbo].[RPWeb_Est_MarginTopUp] ms 			
					ON ms.statementcode = s.LockedStatementCode

			) inner_query
			GROUP BY StatementCode, Retailername, City, Province
			ORDER BY RetailerName
			FOR JSON AUTO
		),'[]')

	END TRY

	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() AS Error_Message
		RETURN;	
	END CATCH

	SELECT 'Success' AS [Status], @JSON_DATA AS Data, '' AS Error_Message
END



















