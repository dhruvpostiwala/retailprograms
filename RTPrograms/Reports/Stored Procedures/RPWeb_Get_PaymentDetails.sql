﻿

CREATE PROCEDURE [Reports].[RPWeb_Get_PaymentDetails] @STATEMENTCODE INT

AS
BEGIN

	SET NOCOUNT ON;

	IF(NOT(OBJECT_ID('tempdb..#SUMMARY_TEMP') IS NULL)) DROP TABLE #SUMMARY_TEMP --Drop the temp table incase it exists
	IF(NOT(OBJECT_ID('tempdb..#SUMMARY_TEMP1') IS NULL)) DROP TABLE #SUMMARY_TEMP1 --Drop the temp table incase it exists

	
	Declare @HTML_Data nvarchar(max);
	Declare @Title varchar(200) = '';
	
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>PAYMENT DETAILS</h1>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'
	SET @HTML_Data += '<div class="st_section">
				<div class = "st_header">
					<span>WORK IN PROGRESS</span>
					<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
					<span class="st_right"</span>
				</div>
				<div class="st_content open">
					<div>CONTENT GOES HERE....</div>
				</div>
			</div>' 

	SET @HTML_Data += '</div>' -- rp_rewards_tab closed

	-- FINAL OUTPUT
	--SELECT ISNULL((SELECT @HTML_DATA AS [data] FOR JSON PATH , WITHOUT_ARRAY_WRAPPER ),'{}')
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	SELECT @HTML_DATA AS [data]
	
	IF(NOT(OBJECT_ID('tempdb..#SUMMARY_TEMP') IS NULL)) DROP TABLE #SUMMARY_TEMP --Drop the temp table incase it exists
	IF(NOT(OBJECT_ID('tempdb..#SUMMARY_TEMP1') IS NULL)) DROP TABLE #SUMMARY_TEMP1 --Drop the temp table incase it exists
END
