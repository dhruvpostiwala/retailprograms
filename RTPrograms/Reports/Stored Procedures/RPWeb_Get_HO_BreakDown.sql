﻿



CREATE   PROCEDURE [Reports].[RPWeb_Get_HO_BreakDown] @STATEMENTCODE INT , @EXCEL INT, @HTML_Data NVARCHAR(MAX)=NULL OUTPUT

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @SEASON INT;
	DECLARE @SEASON_STRING VARCHAR(4);
	DECLARE @Region varchar(10);	
--	DECLARE @EXCEL INT = 0;

	DECLARE @Total_Sales MONEY;
	DECLARE @Total_Eligible_Sales MONEY;
	
	DECLARE @RETAILERS_COUNT INT
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @RETAILER_NAME NVARCHAR(1000);
	DECLARE @RETAILER_CTIY NVARCHAR(1000);
	DECLARE @COUNTER INT=0;

	DECLARE @DYNAMIC_SQL NVARCHAR(MAX)	
	DECLARE @SUMMARY_COLUMN_NAME VARCHAR(20)='';
	DECLARE @RP_COLUMN_NAME VARCHAR(20)='';
	
	DROP TABLE IF EXISTS #SummaryTable

	CREATE TABLE #SummaryTable(		
		ML_Sequence INT  NOT NULL,
		Row_ID INT  NOT NULL,
		Row_Type VARCHAR(50) NOT NULL,		
		MarketLetterCode VARCHAR(50)  NOT NULL,
		ProgramCode VARCHAR(50)  NOT NULL,
		Row_Label NVARCHAR(200) NOT NULL,
		OU_Amount Money  NOT NULL
	)
	
	SELECT @SEASON=Season, @Region=Region  FROM RPWeb_Statements where StatementCode = @STATEMENTCODE;
	SET @SEASON_STRING=CAST(@SEASON AS VARCHAR(4))

	-- RETAILER META DATA	
	DROP TABLE IF EXISTS #RetailerProfile
	SELECT ID = ROW_NUMBER() OVER(ORDER BY RP.RetailerName, RP.MailingCity)
			,RP.RetailerCode, RP.RetailerName, RP.MailingCity AS City		
	INTO #RetailerProfile
	FROM RetailerProfile RP
		INNER JOIN (
			SELECT DISTINCT RetailerCode
			FROM RPWeb_StatementLocations 
			WHERE StatementCode=@Statementcode 			
		) s	
		ON S.RetailerCode=RP.RetailerCode
	
	SELECT @RETAILERS_COUNT=COUNT(*) FROM #RetailerProfile
	
	-- TO ACCOMODATE RETAILER DATA ,  LETS ADD REQUIRED NUMBER OF COLUMNS TO #SUMMARY_TABLE 
	SET @COUNTER=1	
	WHILE @COUNTER <= @RETAILERS_COUNT
	BEGIN  
		SET @SUMMARY_COLUMN_NAME = 'RET_AMOUNT_' + CAST(@COUNTER AS VARCHAR(3))			
		EXECUTE('ALTER TABLE #SummaryTable ADD [' + @SUMMARY_COLUMN_NAME + '] MONEY DEFAULT 0')			
		SET @COUNTER = @COUNTER+1		
	END  
	-- END OF RETAILER PROFILE AND COLUMNS

	-- REWARDS SUMMARY
	DROP TABLE IF EXISTS #REWARDS_SUMMARY
	SELECT RS.MarketLetterCode
		,ISNULL(ML.[Sequence],99) AS [ML_Sequence]
		,ISNULL(ML.Title,'Unknown') AS ML_Title
		,COALESCE(PR.ProgramCode,RSC.RewardCode,'Unknwon') AS Programcode
		,COALESCE(PR.Title,RSC.RewardLabel,'Unknown') AS Program_Title
		,RS.TotalReward AS Amount
	INTO #REWARDS_SUMMARY
	FROM RPWeb_Rewards_Summary RS
		LEFT JOIN RP_Config_MarketLetters ML		ON ML.MarketLetterCode=RS.MarketLetterCode
		LEFT JOIN RP_Config_Rewards_Summary RSC		ON RSC.Season=@SEASON AND RSC.RewardCode=RS.RewardCode AND RSC.RewardType <> 'OTP'
		LEFT JOIN RP_Config_Programs PR				ON PR.Season=@SEASON AND PR.RewardCode=RS.RewardCode
	WHERE RS.StatementCode=@STATEMENTCODE AND RS.MarketLetterCode NOT IN ('Estimate','Estimate_Oct_W')
	
	-- SALES DATA
	/*
	DROP TABLE IF EXISTS #Sales	
	SELECT StatementCode, RetailerCode,
		CAST(SUM(CASE 
			WHEN Pricing_Type='SWP' THEN Price_Q_SWP_CY
			WHEN Pricing_Type='SRP' THEN Price_Q_SRP_CY
			ELSE Price_Q_SDP_CY END) AS MONEY) Sales_Qualifying
		,CAST(SUM(CASE 
			WHEN Pricing_Type='SWP' THEN Price_E_SWP_CY
			WHEN Pricing_Type='SRP' THEN Price_E_SRP_CY
			ELSE Price_E_SDP_CY END) AS MONEY) Sales_Eligible
	INTO #Sales
	FROM RP2020Web_Sales_Consolidated	
	WHERE StatementCode=@Statementcode AND ProgramCode='ELIGIBLE_SUMMARY'
	GROUP BY Statementcode, RetailerCode
	*/

	
	DECLARE @SALES_QUERY VARCHAR(MAX)

	DROP TABLE IF EXISTS #Sales	
	CREATE TABLE #Sales (
		StatementCode INT,
		RetailerCode VARCHAR(20),
		Sales_Qualifying MONEY,
		Sales_Eligible MONEY
	)

	SET @SALES_QUERY = '	
	INSERT INTO #Sales
	SELECT StatementCode, RetailerCode,
		CAST(SUM(CASE 
			WHEN Pricing_Type=''SWP'' THEN Price_Q_SWP_CY
			WHEN Pricing_Type=''SRP'' THEN Price_Q_SRP_CY
			ELSE Price_Q_SDP_CY END) AS MONEY) Sales_Qualifying
		,CAST(SUM(CASE 
			WHEN Pricing_Type=''SWP'' THEN Price_E_SWP_CY
			WHEN Pricing_Type=''SRP'' THEN Price_E_SRP_CY
			ELSE Price_E_SDP_CY END) AS MONEY) Sales_Eligible	
	FROM RP' + CAST(@SEASON AS VARCHAR(4)) + 'Web_Sales_Consolidated	
	WHERE StatementCode=' + CAST(@Statementcode AS VARCHAR) + ' AND ProgramCode=''ELIGIBLE_SUMMARY''		
	GROUP BY Statementcode, RetailerCode '

	EXEC(@SALES_QUERY)
	

	DROP TABLE IF EXISTS #RETAILER_LEVEL_REWARDS
	SELECT RS.MarketLetterCode, RS.ProgramCode, RS.RetailerCode,SUM(RS.Sales) AS Sales,MAX(RS.OU_Sales) AS OU_Sales,MAX(RS.OU_Reward) AS OU_Reward, CAST(0 AS Money) AS Retailer_Reward
	INTO #RETAILER_LEVEL_REWARDS
	FROM RPWeb_HO_BreakDownSummary RS				
	WHERE RS.StatementCode=@STATEMENTCODE
	GROUP BY RS.MarketLetterCode, RS.ProgramCode, RS.RetailerCode

	UPDATE #RETAILER_LEVEL_REWARDS
	SET Retailer_Reward = ROUND(OU_Reward * Sales/OU_Sales,2)
	WHERE OU_Sales <> 0
			
	-- INSERT OU LEVEL DATA INTO #SUMMARYTABLE
	INSERT INTO #SummaryTable(MarketLetterCode, ML_Sequence, Row_Type, Row_ID, ProgramCode, Row_Label, OU_Amount)
	SELECT MarketLetterCode, ML_Sequence, Row_Type, Row_ID,  ProgramCode, Row_Label,  ISNULL(CAST(Amount AS MONEY),0) OU_Amount
	FROM (
		SELECT 'SALES_INFO' AS MarketLetterCode, 0 AS [ML_Sequence], 'CUSTOM' AS [Row_Type], 1 AS [Row_ID] ,'Total_Sales' AS ProgramCode, '<b>Total $</b>' AS [Row_Label] , SUM(Sales_Qualifying) AS Amount 
		FROM #Sales
		
			UNION

		SELECT 'SALES_INFO' AS MarketLetterCode, 0 AS [ML_Sequence], 'CUSTOM' AS [Row_Type], 2 AS [Row_ID], 'Eligible_Sales' AS ProgramCode, '<b>Total Eligible $</b>' AS [Row_Label], SUM(Sales_Eligible) AS Amount 
		FROM #Sales

			UNION

		-- MARKET LETTER LEVEL OU TOTALS 
		SELECT MarketLetterCode, MAX(ML_Sequence) AS [ML_Sequence], 'CUSTOM' AS [Row_Type], 1 AS [Row_ID],  'ML_TOTAL' AS ProgramCode,  MAX([ML_Title]) AS [Row_Label], SUM(Amount) AS Amount
		FROM #REWARDS_SUMMARY
		GROUP BY MarketLetterCode
		
			UNION

		-- PROGRAM LEVEL OU TOTALS
		SELECT MarketLetterCode, ML_Sequence, 'REGULAR REWARDS' AS [Row_Type], 2 AS [Row_ID], ProgramCode,  [Program_Title] AS [Row_Label], Amount AS Amount
		FROM #REWARDS_SUMMARY
					
			UNION

		-- PAYMENT TOTAL @ OU LEVEL
		SELECT 'PAYMENT_INFO' AS  MarketLetterCode, 99 AS ML_Sequence, 'CUSTOM' AS [Row_Type], 1 AS [Row_ID],  'TOTAL_BASF_PAYMENT' AS ProgramCode,  'Total BASF Payment' AS [Row_Label], SUM(Amount) AS Amount
		FROM #REWARDS_SUMMARY
	) D
	
	-- LETS UPDATE RETAILER AMOUNTS   
	DECLARE @RETAILER_TOTAL_SALES AS MONEY;
	DECLARE @RETAILER_ELG_SALES AS MONEY;


	--,' + @RP_COLUMN_NAME + '=''' + @RETAILERCODE + ' ' + @RETAILER_CTIY + '''
	SET @COUNTER=0
	DECLARE RETAILER_LOOP CURSOR FOR 
	SELECT Retailercode, RetailerName, City FROM #RetailerProfile ORDER BY ID ASC -- [RetailerName], [City]
	OPEN RETAILER_LOOP
	FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CTIY
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @COUNTER += 1;
			SET @SUMMARY_COLUMN_NAME = 'RET_AMOUNT_' + CAST(@COUNTER AS VARCHAR)
			SET @RETAILER_TOTAL_SALES=0
			SET @RETAILER_ELG_SALES=0

			SELECT @RETAILER_TOTAL_SALES=Sales_Qualifying, @RETAILER_ELG_SALES=Sales_Eligible 
			FROM #Sales 
			WHERE RetailerCode=@RETAILERCODE
			
						
			SET @DYNAMIC_SQL = '			
			  	UPDATE #SummaryTable
				SET ' + @SUMMARY_COLUMN_NAME + '=' + CAST(@RETAILER_TOTAL_SALES AS VARCHAR) + '
				WHERE MarketLetterCode=''SALES_INFO'' AND [ProgramCode]=''Total_Sales''

				UPDATE #SummaryTable
				SET ' + @SUMMARY_COLUMN_NAME + '=' + CAST(@RETAILER_ELG_SALES AS VARCHAR) + '
				WHERE MarketLetterCode=''SALES_INFO'' AND [ProgramCode]=''Eligible_Sales''

				UPDATE T1
				SET ' + @SUMMARY_COLUMN_NAME + '=ROUND(ISNULL(RS.Amount,0),2)
				FROM #SummaryTable T1
					LEFT JOIN (						
						SELECT MarketLetterCode, SUM(Retailer_Reward) AS Amount
						FROM #RETAILER_LEVEL_REWARDS
						WHERE RetailerCode=''' + @RETAILERCODE + '''
						GROUP BY MarketLetterCode
					) RS
					ON RS.MarketLetterCode=T1.MarketLetterCode 
				WHERE T1.Row_Type = ''CUSTOM'' AND T1.ProgramCode=''ML_TOTAL ''


				UPDATE T1
				SET ' + @SUMMARY_COLUMN_NAME + '=ROUND(ISNULL(RS.Amount,0),2)					
				FROM #SummaryTable T1
					LEFT JOIN (
						SELECT MarketLetterCode, ProgramCode, SUM(Retailer_Reward) AS Amount
						FROM #RETAILER_LEVEL_REWARDS
						WHERE RetailerCode=''' + @RETAILERCODE + '''
						GROUP BY MarketLetterCode, ProgramCode
					) RS
					ON RS.MarketLetterCode=T1.MarketLetterCode AND RS.ProgramCode=T1.ProgramCode 
				WHERE T1.Row_Type =  ''REGULAR REWARDS''

				UPDATE T1
				SET ' + @SUMMARY_COLUMN_NAME + '=ROUND(ISNULL(RS.Amount,0),2)
				FROM #SummaryTable T1
					LEFT JOIN (						
						SELECT ''PAYMENT_INFO'' AS MarketLetterCode, SUM(Retailer_Reward) AS Amount
						FROM #RETAILER_LEVEL_REWARDS
						WHERE RetailerCode=''' + @RETAILERCODE + '''						
					) RS
					ON T1.MarketLetterCode=RS.MarketLetterCode
				WHERE T1.Row_Type = ''CUSTOM'' AND T1.ProgramCode=''TOTAL_BASF_PAYMENT ''
			'
			EXEC(@DYNAMIC_SQL)
		FETCH NEXT FROM RETAILER_LOOP INTO @RETAILERCODE, @RETAILER_NAME, @RETAILER_CTIY
		END
	CLOSE RETAILER_LOOP
	DEALLOCATE RETAILER_LOOP	

	DECLARE @MAX_RET_PER_ROW INT=3, @REMAINING_RET_COUNT INT=0, @X INT=0, @Y INT=0;
	DECLARE @LOOP_COUNTER INT=0, @TOTAL_SPLITS INT=0;
	DECLARE @THEAD_1 NVARCHAR(MAX)='' ,@THEAD_2 NVARCHAR(MAX)='', @THEAD_3 NVARCHAR(MAX)='', @BODY_CONTENT NVARCHAR(MAX)='', @SECTIONS_CONTENT NVARCHAR(MAX)='';
	DECLARE @TD_COLUMN_NAMES VARCHAR(MAX)='';	
	DECLARE @CLASS_FIRST_SECTION VARCHAR(5)='';
	DECLARE @SECTION_LABEL VARCHAR(500)='';
			
	DECLARE @SALES_ROWS_QUERY NVARCHAR(MAX)=''						
	DECLARE @PROGRAMS_ROWS_QUERY NVARCHAR(MAX)=''						
	DECLARE @TEMP_QUERY NVARCHAR(MAX)=''
	
	IF @EXCEL=0
		SET @MAX_RET_PER_ROW=3
	ELSE
		SET @MAX_RET_PER_ROW=@RETAILERS_COUNT

--	SET @MAX_RET_PER_ROW=@RETAILERS_COUNT

	SET @SALES_ROWS_QUERY='
		DELETE #HTML
		INSERT INTO #HTML(Content)
		SELECT  ISNULL((
			CONVERT(NVARCHAR(MAX),(
				SELECT (SELECT Row_Label AS ''td'' FOR XML PATH(''''), type)
					,(SELECT ''right_align'' as [td/@class], dbo.SVF_Commify(OU_Amount,''$'')		AS ''td'' FOR XML PATH(''''), type)
					,<retailer_amount_columns>			
				FROM #SummaryTable	
				WHERE MarketLetterCode=''SALES_INFO''
				ORDER BY ML_Sequence
				FOR XML PATH(''tr''), ELEMENTS, TYPE)
			))
		,'''') AS Content
	'
		
	SET @PROGRAMS_ROWS_QUERY='
		DELETE #HTML
		INSERT INTO #HTML(Content)
		SELECT  ISNULL((
			CONVERT(NVARCHAR(MAX),(
				SELECT 
					CASE WHEN ProgramCode=''ML_TOTAL'' THEN ''sub_total_row''
						WHEN ProgramCode=''TOTAL_BASF_PAYMENT'' THEN ''total_row''
						ELSE NULL END AS [@class]					
					,(SELECT Row_Label	AS ''td'' FOR XML PATH(''''), type)
					,(SELECT ''right_align'' as [td/@class], dbo.SVF_Commify(OU_Amount,''$'')		AS ''td'' FOR XML PATH(''''), type)
					,<retailer_amount_columns>			
				FROM #SummaryTable	
				WHERE MarketLetterCode <> ''SALES_INFO''
				ORDER BY ML_Sequence
				FOR XML PATH(''tr''), ELEMENTS, TYPE)
			))
		,'''') AS Content
	'
	
	DROP TABLE IF EXISTS #HTML
	CREATE TABLE #HTML(Content NVARCHAR(MAX))
	
	DROP TABLE IF EXISTS #Retailer_Columns	
	SELECT CAST(REPLACE(Name,'Ret_Amount_','') AS INT) as ID
		,Name As Col_Name	
		,'(SELECT ''right_align'' as [td/@class], dbo.SVF_Commify(' + Name + ',''$'') AS ''td'' FOR XML PATH(''''), type)' AS col_td		
	INTO #Retailer_Columns
	FROM  tempdb.sys.columns
	WHERE object_id = Object_id('tempdb..#SummaryTable')	
		AND Name Like 'Ret_Amount%' 

	SET @TOTAL_SPLITS = @RETAILERS_COUNT/@MAX_RET_PER_ROW
	IF @RETAILERS_COUNT % @MAX_RET_PER_ROW > 0 
		SET @TOTAL_SPLITS += 1


	SET @HTML_Data='';
	SET @CLASS_FIRST_SECTION='first'
	
	SET @REMAINING_RET_COUNT=@RETAILERS_COUNT
	WHILE @REMAINING_RET_COUNT > 0 
	BEGIN		
		SET @LOOP_COUNTER += 1;
		SET @X=@Y+1
		IF @REMAINING_RET_COUNT > @MAX_RET_PER_ROW
			SET @Y += @MAX_RET_PER_ROW
		ELSE
			SET @Y += @REMAINING_RET_COUNT
		
		-- RETAILERS COLUMNS
		SELECT @TD_COLUMN_NAMES=STRING_AGG(CAST(col_td AS NVARCHAR(MAX)), ',') 
		FROM (
			SELECT TOP (@MAX_RET_PER_ROW) col_td
			FROM #Retailer_Columns	
			ORDER BY ID
		) T		

		-- FIRST ROW - RETAILER INFO
		SELECT @THEAD_1='<tr class="Header_Row">
				<th width=35%>&nbsp;</th>
				<th width=15%>&nbsp;</th>
				<th>' + STRING_AGG(CAST(RetailerName + '<br/>(' + City AS NVARCHAR(MAX))   ,')</th><th>') WITHIN GROUP (ORDER BY ID ASC)  + ')</th>
			</tr>' 
		FROM (
			SELECT TOP (@MAX_RET_PER_ROW) ID, RetailerName, City
			FROM #RetailerProfile
			ORDER BY ID
		) D

		-- ROW #2 and #3 - SALES INFO
		SET @TEMP_QUERY=@SALES_ROWS_QUERY
		SET @TEMP_QUERY=REPLACE(@TEMP_QUERY, '<retailer_amount_columns>', @TD_COLUMN_NAMES)	
		EXEC(@TEMP_QUERY)
		SELECT @THEAD_2=ISNULL(Content,'')	FROM #HTML		
		
		-- ROW #4 - MARKET LETTER TOTAL
		SELECT @THEAD_3='<tr class="Header_Row"><th>Market Letter</th><th>Total Payment</th><th>' + STRING_AGG('&nbsp;','</th><th>')  + '</th></tr>' 
		FROM (
			SELECT TOP (@MAX_RET_PER_ROW) RetailerName, City
			FROM #RetailerProfile
			ORDER BY ID
		) D

		-- PROGRAM LINE ITEMS
		SET @TEMP_QUERY=@PROGRAMS_ROWS_QUERY
		SET @TEMP_QUERY=REPLACE(@TEMP_QUERY, '<retailer_amount_columns>', @TD_COLUMN_NAMES)	
		EXEC(@TEMP_QUERY)
		SELECT @BODY_CONTENT=ISNULL(Content,'')	FROM #HTML
			   

		-- SECTION
		--SET @SECTION_LABEL = '<span>Breakdown ' + CAST(@LOOP_COUNTER AS VARCHAR(2)) + ' OF ' + CAST(@TOTAL_SPLITS AS VARCHAR) + '</span>'
		SET @SECTION_LABEL = 'Retailers ' + CAST(@X AS VARCHAR(3)) + ' to ' +	CAST(@Y AS VARCHAR(3)) + ' of ' + CAST(@RETAILERS_COUNT AS VARCHAR)  
		--SET @SECTIONS_CONTENT += '<div '+ IIF(@LOOP_COUNTER > 1, 'style ="page-break-before:always;"','') + ' class="st_section ' + @CLASS_FIRST_SECTION + '">
		SET @SECTIONS_CONTENT += '<div class="st_section ' + @CLASS_FIRST_SECTION + '" style = "page-break-after: always; float:none;">
			
			<div class = "st_header">
					<span>HEAD OFFICE BREAKDOWN (' +  CAST(@LOOP_COUNTER AS VARCHAR(2)) + ' of ' + CAST(@TOTAL_SPLITS AS VARCHAR(2)) + ')</span>
					<div class="st_toggle collapse" style="display: inline-block;" onclick="st_toggle(this);"></div>
					<span class="st_right">' + @SECTION_LABEL  + '</span>
				</div>
				<div class="st_content open">
					<table class="rp_report_table">
						<thead>' +
							@THEAD_1 + @THEAD_2 + @THEAD_3 + '
						</thead>
						<tbody>' +
							@BODY_CONTENT + '
						</tbody>
					</table>
				</div>
			</div>' 
		

		SET @REMAINING_RET_COUNT -= @MAX_RET_PER_ROW
		SET @CLASS_FIRST_SECTION=''
		DELETE #RetailerProfile WHERE ID <= @Y		
		DELETE #Retailer_Columns WHERE ID <= @Y
	END
	
	

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += 	'<div class="rp_program_header">
		<h1>HEAD OFFICE BREAKDOWN</h1>
		<button type="button" class="main_grey_button Excel" style = "right:140px;" onclick="rp_headOfficeExportToExcel('+CAST(@STATEMENTCODE as varchar(5))+');" >EXPORT TO EXCEL</button>
		<button type="button" class="main_grey_button st_collapseall" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>		
	</div>'

	SET @HTML_Data += @SECTIONS_CONTENT
	SET @HTML_Data += '</div>'

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	IF @SEASON <=2020
		SET @HTML_DATA = STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	ELSE
		SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
	   				   	
--	IF @EXCEL=1
	--BEGIN
		--SET @BODY_CONTENT = '<table border=1>' + 	@THEAD_1 + @THEAD_2 + @THEAD_3 + @BODY_CONTENT + '</table>'
		--SET @BODY_CONTENT =  STRING_ESCAPE(REPLACE(REPLACE(@BODY_CONTENT,CHAR(13),''),CHAR(10),'') , 'json')
		--SELECT  '<style>br {mso-data-placement:same-cell;} mso-pattern:auto;</style>' + @BODY_CONTENT AS [Data]
	--END
END