﻿

CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_Logistics_Support_Reward_Backup] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(2000)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  	
	DECLARE @TABLE_PART_A AS NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS NVARCHAR(MAX)='';
	DECLARE @TABLE_PART_B_DETAILS AS NVARCHAR(MAX)='';
	DECLARE @TABLE_PART_B_SUMMARY AS NVARCHAR(MAX)='';
	DECLARE @DATA NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);

	DECLARE @BASELINE_REWARD_PERCENTAGE DECIMAL(6,4) = 0.01;
	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;
	
	DECLARE @ELIGIBLE_POG_SALES_LY MONEY=0;
	DECLARE @POG_PLAN_CY MONEY=0;
	DECLARE @PLANNED_GROWTH FLOAT=0;
	DECLARE @REWARD_PERCENTAGE_PART_A DECIMAL(6,4)=0;
	DECLARE @REWARD_A MONEY=0;
	DECLARE @REWARD_SALES_A MONEY=0;
	DECLARE @ORGANIC_DATA VARCHAR(MAX)='';
	
	SELECT @PROGRAMTITLE = Title, @REWARDCODE = RewardCode
	FROM RP_Config_Programs
	WHERE ProgramCode=@PROGRAMCODE

	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	
	SELECT @ELIGIBLE_POG_SALES_LY= SUM(ISNULL(LY_Sales, 0))
		  ,@POG_PLAN_CY = SUM(ISNULL(POG_Plan, 0))
		  ,@REWARD_SALES_A = SUM(ISNULL(Segment_Reward_Sales, 0))
	FROM [dbo].[RP2021Web_Rewards_W_LogisticsSupport]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode

	-- GET THE PLANNNED GROWTH PERCENTAGE
	SET @PLANNED_GROWTH = 
		CASE
			WHEN @POG_PLAN_CY > 0 AND @ELIGIBLE_POG_SALES_LY <= 0 THEN 9.9999
			WHEN @POG_PLAN_CY <= 0 AND @ELIGIBLE_POG_SALES_LY <= 0 THEN 0
			WHEN @POG_PLAN_CY/@ELIGIBLE_POG_SALES_LY > 9.9999 THEN 9.9999
			ELSE @POG_PLAN_CY/@ELIGIBLE_POG_SALES_LY
		END

	-- SET THE BASELINE REWARD PERCENTAGE IF APPLICIBLE
	SET @REWARD_PERCENTAGE_PART_A = IIF(@PLANNED_GROWTH + @ROUNDING_PERCENT >= 0.89, @BASELINE_REWARD_PERCENTAGE, 0)

	-- GET THE REWARD AMOUNT 
	SET @REWARD_A = @REWARD_SALES_A*@REWARD_PERCENTAGE_PART_A

	/* ############################### PART A - GROWER DETAIL TABLE ############################### */

	SET @HEADER =  '<th> ' + cast(@Season-1 as varchar(4)) + ' Eligible POG $</th> 
					<th> ' + cast(@Season as varchar(4)) + ' POG Plan</th> 
					<th>Planned Growth %</th>
					<th>Reward %</th>
					<th> ' + cast(@Season as varchar(4)) + ' Eligible POG $</th>
					<th> Reward $</th>'
		
	SELECT @DATA = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@ELIGIBLE_POG_SALES_LY,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@POG_PLAN_CY,'$')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@PLANNED_GROWTH,'%')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENTAGE_PART_A,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES_A,'$')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_A,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
		

	SET @TABLE_PART_A='
	<div class="st_static_section">
		<strong>Part A</strong>
	</div>  
	
	<div class="st_section">
		<div class="st_content open">					
			<table class="rp_report_table">' + @HEADER + @DATA  + '</table>		
		</div>
	</div>' 

	
	/* ############################### EXCEPTION INFORMATION TABLE ############################### */

	SET @ORGANIC_DATA = '{
							"sales":['+cast(@REWARD_SALES_A as varchar(20))+'],
							"reward_margins":['+cast(@REWARD_PERCENTAGE_PART_A as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible POG $"],
							"row_labels":[]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='Default', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT


	/* ############################### PART B - SEGMENT REWARD DETAIL TABLE ############################### */
	
	IF NOT OBJECT_ID('tempdb..#TempLogisticsSupport') IS NULL DROP TABLE #TempLogisticsSupport
	SELECT CASE WHEN BrandSegment = 'LIBERTY' THEN 'LIBERTY 150' ELSE BrandSegment END AS BrandSegment
		   ,CASE BrandSegment
				 WHEN 'INOCULANT & SEED TREATMENT' THEN 1
				 WHEN 'LIBERTY' THEN 2 
				 WHEN 'CENTURION' THEN 3
				 WHEN 'OTHER HERBICIDES' THEN 4
				 WHEN 'FUNGICIDES AND INSECTICIDES' THEN 5
				 WHEN 'FALL HERBICIDES' THEN 6
				 ELSE 7 END AS SortOrder
		   ,CASE WHEN BrandSegment IN ('INOCULANT & SEED TREATMENT','LIBERTY','CENTURION') THEN [dbo].[SVF_Commify](March_Take,'%') ELSE '' END AS March_Take
		   ,CASE WHEN BrandSegment IN ('INOCULANT & SEED TREATMENT','LIBERTY','CENTURION') THEN [dbo].[SVF_Commify](March_Reward,'%') ELSE '' END AS March_Reward
		   ,CASE WHEN BrandSegment IN ('INOCULANT & SEED TREATMENT','LIBERTY','CENTURION','OTHER HERBICIDES') THEN [dbo].[SVF_Commify](April_Take,'%') ELSE '' END AS April_Take
		   ,CASE WHEN BrandSegment IN ('INOCULANT & SEED TREATMENT','LIBERTY','CENTURION','OTHER HERBICIDES') THEN [dbo].[SVF_Commify](April_Reward,'%') ELSE '' END AS April_Reward
		   ,CASE WHEN BrandSegment IN ('LIBERTY','CENTURION','OTHER HERBICIDES','FUNGICIDES AND INSECTICIDES') THEN [dbo].[SVF_Commify](May_Take,'%') ELSE '' END AS May_Take
		   ,CASE WHEN BrandSegment IN ('LIBERTY','CENTURION','OTHER HERBICIDES','FUNGICIDES AND INSECTICIDES') THEN [dbo].[SVF_Commify](May_Reward,'%') ELSE '' END AS May_Reward
		   ,CASE WHEN BrandSegment IN ('FUNGICIDES AND INSECTICIDES') THEN [dbo].[SVF_Commify](June_Take,'%') ELSE '' END AS June_Take
		   ,CASE WHEN BrandSegment IN ('FUNGICIDES AND INSECTICIDES') THEN [dbo].[SVF_Commify](June_Reward,'%') ELSE '' END AS June_Reward
		   ,CASE WHEN BrandSegment IN ('FALL HERBICIDES') THEN [dbo].[SVF_Commify](July_Take,'%') ELSE '' END AS July_Take
		   ,CASE WHEN BrandSegment IN ('FALL HERBICIDES') THEN [dbo].[SVF_Commify](July_Reward,'%') ELSE '' END AS July_Reward
		   ,[dbo].[SVF_Commify](Segment_Reward_Sales,'$') AS Segment_Reward_Sales
		   ,[dbo].[SVF_Commify](Segment_Reward_Percentage,'%') AS Segment_Reward_Percentage
		   ,[dbo].[SVF_Commify](Reward,'$')  AS Reward
	INTO #TempLogisticsSupport
	FROM [RP2021Web_Rewards_W_LogisticsSupport_PartB]
	WHERE StatementCode = @STATEMENTCODE


	   
	SET @HEADER =  '<tr>
						<th rowspan="2">Segment</th>
						<th colspan="2"> March 31, ' + cast(@Season as varchar(4)) + '</th> 
						<th colspan="2"> April 30, ' + cast(@Season as varchar(4)) + '</th> 
						<th colspan="2"> May 31, ' + cast(@Season as varchar(4)) + '</th> 
						<th colspan="2"> June 15, ' + cast(@Season as varchar(4)) + '</th> 
						<th colspan="2"> July 30, ' + cast(@Season as varchar(4)) + '</th> 
						<th rowspan="2">Total Reward %</th>
					</tr>
					<tr>
						<th>Take</th> <th>Reward</th> 
						<th>Take</th> <th>Reward</th> 
						<th>Take</th> <th>Reward</th> 
						<th>Take</th> <th>Reward</th> 
						<th>Take</th> <th>Reward</th> 
					</tr>'


	SELECT @DATA = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'left_align' as [td/@class] , BrandSegment  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , March_Take as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , March_Reward  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , April_Take  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , April_Reward as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , May_Take  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , May_Reward  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , June_Take  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , June_Reward  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , July_Take  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , July_Reward  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , Segment_Reward_Percentage as 'td' for xml path(''), type)
	FROM #TempLogisticsSupport
	ORDER BY SortOrder
	FOR XML RAW('tr'), ELEMENTS, TYPE))
	
	
	SET @TABLE_PART_B_DETAILS='
	<div class="st_static_section">
		<strong>Part B</strong>
	</div>  
	
	<div class="st_section">
		<div class="st_content open">					
			<table class="rp_report_table">' + @HEADER + ISNULL(@DATA,'')  + '</table>		
		</div>
	</div>' 
	

	/* ############################### PART B - SEGMENT REWARD SUMMARY TABLE ############################### */

	SET @HEADER =  '<tr>
						<th>Part B Segment</th>
						<th>2021 Eligible POG $</th> 
						<th>Reward %</th>
						<th>Reward $</th>
					<tr>'
					
	SELECT @DATA = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'left_align' as [td/@class] , BrandSegment  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , Segment_Reward_Sales  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , Segment_Reward_Percentage  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , Reward  as 'td' for xml path(''), type)
	FROM #TempLogisticsSupport
	ORDER BY SortOrder
	FOR XML RAW('tr'), ELEMENTS, TYPE))


	SET @TABLE_PART_B_SUMMARY='
	<div class="st_static_section">
	</div>  
	
	<div class="st_section">
		<div class="st_content open">					
			<table class="rp_report_table">' + @HEADER + ISNULL(@DATA,'')  + '</table>		
		</div>
	</div>' 


	/* ############################### DISCLAIMER TEXT ############################### */
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Inoculant and Seed Treatment Qualifying and Reward Brands:</strong> Insure Cereal, Insure Cereal FX4, Insure Pulse, Nodulator brand, Cimegra, Teraxxa and Titan brands.
			</div>
			<div class="rprew_subtabletext">
				<strong>Liberty 150 Qualifying and Reward Brands:</strong> Liberty 150.
			</div>
			<div class="rprew_subtabletext">
				<strong>Centurion Qualifying and Reward Brands:</strong> Centurion.
			</div>
			<div class="rprew_subtabletext">
				<strong>Other Herbicide Qualifying and Reward Brands:</strong>Armezon, Basagran, Basagran Forte, Engenia, Facet L, Frontier Max, Heat LQ (Pre-Seed), Heat Complete, Liberty 200, Odyssey brands, Poast Ultra, Prowl H20, Pursuit, Solo brands, Viper brands, and Zidua.
			</div>
			<div class="rprew_subtabletext">
				<strong>Fungicides and Insecticide Qualifying and Reward Brands:</strong> Cantus, Caramba, Cevya, Cotegra, Dyax, Forum, Headline, Lance, Lance AG, Nealta, Nexicor, Priaxor, Sefina, Sercadis, and Twinline.
			</div>
			<div class="rprew_subtabletext">
				<strong>Fall Herbicide Qualifying and Reward Brands:</strong> Distinct and Heat LQ (Pre-Harvest).
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */
FINAL:
	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'	
	SET @HTML_Data += @REWARDSUMMARYSECTION
	SET @HTML_Data += @TABLE_PART_A 
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @TABLE_PART_B_DETAILS
	SET @HTML_Data += @TABLE_PART_B_SUMMARY
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

