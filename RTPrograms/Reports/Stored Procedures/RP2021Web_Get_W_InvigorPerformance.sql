﻿




CREATE  PROCEDURE [Reports].[RP2021Web_Get_W_InvigorPerformance] @STATEMENTCODE int, @PROGRAMCODE varchar(200), @HTML_Data NVARCHAR(MAX) OUTPUT

AS
BEGIN

	SET NOCOUNT ON;
	
	
	DECLARE @SEASON INT = 2021;
	DECLARE @PROGRAMTITLE AS VARCHAR(200);
	DECLARE @REWARDCODE AS VARCHAR(200);
	DECLARE @L5CODE AS VARCHAR(10)=''

	DECLARE @REWARDSUMMARYSECTION NVARCHAR(MAX)='';
	DECLARE @REWARDSUMMARYTABLE AS  NVARCHAR(MAX)='';
	DECLARE @EXCEPTION_REWARDTABLE AS NVARCHAR(MAX)='';
	
	DECLARE @DISCLAIMERTEXT AS NVARCHAR(500)='';  
	DECLARE @REWARDSPERCENTAGEMATRIX AS NVARCHAR(MAX)='';  
	
	DECLARE @SUMMARY NVARCHAR(MAX);
	DECLARE @HEADER NVARCHAR(MAX);
	
	DECLARE @BAG_SALES_LY FLOAT=0;
	DECLARE @BAG_SALES_CY FLOAT=0;
	DECLARE @QUALIFYING_PERCENTAGE FLOAT =0;
	DECLARE @REWARD_SALES MONEY=0;
	DECLARE @REWARD_PERCENT DECIMAL(6,4)=0;
	DECLARE @REWARD_TOTAL MONEY=0;
	DECLARE @ORGANIC_DATA VARCHAR(MAX);


	
	SELECT @PROGRAMTITLE = Title, 
		   @REWARDCODE = RewardCode
	From RP_Config_Programs
	Where ProgramCode=@PROGRAMCODE

	SELECT @L5CODE=Level5Code FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE


	EXEC [Reports].[RPWeb_Get_Program_TotalReward] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @REWARDSUMMARYSECTION = @REWARDSUMMARYSECTION OUTPUT

	--FETCH REWARD DISPLAY VALUES
	SELECT @BAG_SALES_LY= SUM(ISNULL(Sales_Target, 0))
		  ,@BAG_SALES_CY = SUM(ISNULL(QualBrand_Qty, 0))
		  ,@REWARD_SALES = SUM(ISNULL(RewardBrand_Sales, 0)) 
		  ,@REWARD_PERCENT = MAX(ISNULL(Reward_Percentage, 0)) 
		  ,@REWARD_TOTAL = SUM(ISNULL(Reward, 0))
		  ,@QUALIFYING_PERCENTAGE = 
			CASE
				WHEN SUM(ISNULL(QualBrand_Qty, 0)) > 0 AND SUM(ISNULL(Sales_Target, 0)) <= 0 THEN 9.9999
				WHEN SUM(ISNULL(QualBrand_Qty, 0)) <= 0 AND SUM(ISNULL(Sales_Target, 0)) <= 0 THEN 0.00
				WHEN SUM(ISNULL(QualBrand_Qty, 0))/SUM(ISNULL(Sales_Target, 0)) > 9.99 THEN 9.9999
				ELSE SUM(ISNULL(QualBrand_Qty, 0))/SUM(ISNULL(Sales_Target, 0))
			END
	From [dbo].[RP2021Web_Rewards_W_INV_PERF]
	WHERE STATEMENTCODE = @STATEMENTCODE
	GROUP BY Statementcode,MarketLetterCode,ProgramCode


	-- Get reward summary
	--EXEC [Reports].[RPWeb_Get_Program_RewardSummary] @STATEMENTCODE, @PROGRAMCODE, @REWARDCODE, @LANGUAGE, @REWARDSUMMARYSECTION OUTPUT;


	/* ############################### REWARD SUMMARY TABLE ############################### */
	IF @L5CODE = 'D520062427'
	BEGIN
	SET @HEADER = '<th> ' + cast(@Season as varchar(4)) + ' Eligible InVigor POG $</th>
				   <th>Reward %</th>
				   <th>Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$') as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENT,'%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_TOTAL,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
	END
	ELSE
	BEGIN
	SET @HEADER = '<th> ' + cast(@Season-1 as varchar(4)) + ' Eligible InVigor Bags</th> 
				   <th> ' + cast(@Season as varchar(4)) + ' Eligible InVigor Bags</th>
				   <th>Qualifying %</th>
				   <th> ' + cast(@Season as varchar(4)) + ' Eligible InVigor POG $</th>
				   <th>Reward %</th>
				   <th>Reward $</th>'
		
	SELECT @SUMMARY = CONVERT(NVARCHAR(MAX),(SELECT	
				(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@BAG_SALES_LY,'N0')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@BAG_SALES_CY,'N0')  as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@QUALIFYING_PERCENTAGE,'%')  as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_SALES,'$') as 'td' for xml path(''), type)
				,(select 'center_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_PERCENT,'%') as 'td' for xml path(''), type)
				,(select 'right_align' as [td/@class] , [dbo].[SVF_Commify](@REWARD_TOTAL,'$') as 'td' for xml path(''), type)
	FOR XML RAW('tr'), ELEMENTS, TYPE))
	END
	SET @REWARDSUMMARYTABLE += '<div class="st_section">
		<div class="st_content open">
			<table class="rp_report_table"> 
				<thead>
				<tr>
						'+ @HEADER +'						
				</tr>
				</thead>
						'+ @SUMMARY + '
			</table>
		</div>
	</div>' 
	
		/* ############################### EXCEPTION INFORMATION TABLE ############################### */

		SET @ORGANIC_DATA = '{
							"sales":['+cast(@REWARD_SALES as varchar(20))+'],
							"reward_margins":['+cast(@REWARD_PERCENT as varchar(20))+'],
							"col_headers":["'+cast(@Season as varchar(4))+' Eligible InVigor POG $"],
							"row_labels":[]
						}'

	EXEC [Reports].[RPWeb_Get_Program_Exception] @STATEMENTCODE =  @StatementCode, @PROGRAMCODE = @PROGRAMCODE, @TEMPLATE_CODE='Default', @ORGANIC_DATA=@ORGANIC_DATA, @EXCEPTION_REWARDTABLE = @EXCEPTION_REWARDTABLE OUTPUT


	/* ############################### REWARD PERCENTAGE TABLE ############################### */
	IF @L5CODE = 'D0000117'
	BEGIN -- WEST COOP REWARRD MATRIX 
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>2021 Eligible POG Growth (in Bags) Relative to Target</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">90% +</td>
						<td class="center_align"> 1.45% </td>
					</tr>
					<tr>
						<td class="center_align">80% to 89.9%</td>
						<td class="center_align"> 1.0% </td>
					</tr>
				
					<tr>
						<td class="center_align">< 80%</td>
						<td class="center_align"> 0.75% </td>
					</tr>						
				</table>
			</div>
		</div>' 
	END
	ELSE IF @L5CODE = 'D000001'
	BEGIN
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>2021 Eligible POG Growth (in Bags) Relative to Target</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">90% +</td>
						<td class="center_align"> 2.0% </td>
					</tr>
					<tr>
						<td class="center_align">80% to 89.9%</td>
						<td class="center_align"> 1.5% </td>
					</tr>
				
					<tr>
						<td class="center_align">< 80%</td>
						<td class="center_align"> 1.0% </td>
					</tr>						
				</table>
			</div>
		</div>' 
	END
	ELSE
	BEGIN
		SET @REWARDSPERCENTAGEMATRIX = '
		<div class="st_static_section">
			<div class="st_content open">
				<table class="rp_report_table">
					<tr>
						<th>2021 Eligible POG Growth (in Bags) Relative to Target</th>
						<th>Reward %</th>
					</tr>
					<tr>
						<td class="center_align">97.5% +</td>
						<td class="center_align"> 3.0% </td>
					</tr>
					<tr>
						<td class="center_align">95% to 97.49%</td>
						<td class="center_align"> 2.5% </td>
					</tr>
				
					<tr>
						<td class="center_align">< 95%</td>
						<td class="center_align"> 2.0% </td>
					</tr>						
				</table>
			</div>
		</div>' 
	END

	/* ############################### DISCLAIMER TEXT ############################### */
	IF @L5CODE = 'D520062427'
	SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Reward Brands:</strong> Eligible 2021 InVigor Reward Brands.
			</div>
		</div>
	 </div>'
	 ELSE
	 SET @DISCLAIMERTEXT = 
	'<div class="st_static_section">
		<div class="st_content open">
			<div class="rprew_subtabletext">
				<strong>Qualifying and Reward Brands:</strong> InVigor L340PC, LR344PC, L345PC, L352C, L357P, L230, L233P, L234PC, L241C, L252, L255PC.
			</div>
			<div class="rprew_subtabletext">
				Note: For the purpose of calculating the Qualifying %, 2020 and 2021 Eligible InVigor Bags does not include any InVigor Reseed amounts
			</div>
		</div>
	 </div>'
		
	/* ############################### FINAL OUTPUT ############################### */

	SET @HTML_Data = '<div class="rp_rewards_tab rp_programs">'
	SET @HTML_Data += '
	<div class="rp_program_header">
		<h1>' + @ProgramTitle + '</h1>
		<button type="button" class="main_grey_button st_collapseall" style="display: none;" onclick="rp_toggle_all(this);">COLLAPSE ALL</button>
	</div>'		
	SET @HTML_Data += @RewardSummarySection;
	SET @HTML_Data += @REWARDSUMMARYTABLE 
	IF @L5CODE != 'D520062427'
	BEGIN
	SET @HTML_Data += @EXCEPTION_REWARDTABLE
	SET @HTML_Data += @REWARDSPERCENTAGEMATRIX
	END
	SET @HTML_Data += @DISCLAIMERTEXT
	
	SET @HTML_Data += '</div>' -- rp_rewards_tab closed
	

	-- FINAL OUTPUT
	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>')
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'')
END

