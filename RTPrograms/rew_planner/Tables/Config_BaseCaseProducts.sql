﻿CREATE TABLE [rew_planner].[Config_BaseCaseProducts] (
    [Season]        INT          NOT NULL,
    [Region]        VARCHAR (4)  NOT NULL,
    [ChemicalGroup] VARCHAR (50) NOT NULL,
    [ProductCode]   VARCHAR (65) NOT NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Region] ASC, [ChemicalGroup] ASC, [ProductCode] ASC)
);

