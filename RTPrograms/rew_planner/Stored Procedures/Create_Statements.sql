﻿CREATE PROCEDURE [rew_planner].[Create_Statements] @SEASON INT, @RETAILERCODE VARCHAR(20), @REWARD_PLANNER_NAME VARCHAR(500), @SCENARIO_DETAILS VARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SOURCE_STATEMENTCODE INT, @NEW_STATEMENTCODE INT
		
	DROP TABLE IF EXISTS #RP_STATEMENTS
		
	BEGIN TRY 
		
		/*
		DECLARE @SEASON INT = 2022;
		DECLARE @RETAILERCODE VARCHAR(20) = 'R770027269';
		DECLARE @REWARD_PLANNER_NAME VARCHAR(500) = 'Reward Plan based on  2020 Projection  on Sep 30 2021'
		DECLARE @SCENARIO_DETAILS VARCHAR(1000) = '{"retailercode":"R770027269","season":2021,"scenario_id":"585","scenario_type":"projection","status":"FINAL","name":"Scenario Name ...","date_modified":"2020-10-26T17:09:27.253"}'
		*/

		-- {"season":"2021","retailercode":"R0000501","scenario_id":"PS-TORO-C69L74","date_modified":"2021-08-26","status":"Projected","scenario_type":"Projection","name":"Aimee R - Reward Planner Testing - 2021 Projections"}

		-- PARSE SCENARIO INFORMATION
		DECLARE @SCENARIO_SEASON INT, @SCENARIO_CODE VARCHAR(20), @SCENARIO_STATUS VARCHAR(20), @DATE_MODIFIED AS DATE, @SCENARIO_TYPE VARCHAR(20)

		SET @SCENARIO_SEASON = JSON_VALUE(@SCENARIO_DETAILS,'$.season')
		SET @SCENARIO_CODE = JSON_VALUE(@SCENARIO_DETAILS,'$.scenario_id')
		SET @SCENARIO_TYPE = JSON_VALUE(@SCENARIO_DETAILS,'$.scenario_type')
		SET @SCENARIO_STATUS = JSON_VALUE(@SCENARIO_DETAILS,'$.status')
		SET @DATE_MODIFIED = JSON_VALUE(@SCENARIO_DETAILS,'$.date_modified')

		SELECT StatementCode, StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,Category
		INTO #RP_STATEMENTS
		FROM RP_Statements
		WHERE VersionType='Unlocked' AND [Status]='Active' 
			AND Season=@SCENARIO_SEASON AND StatementType=@SCENARIO_TYPE 
			AND RetailerCode=@RETAILERCODE 
	
		SELECT @SOURCE_STATEMENTCODE=StatementCode FROM #RP_STATEMENTS

		UPDATE #RP_STATEMENTS
		SET [StatementType]='REWARD PLANNER'			
			,[Category]='REWARD PLANNER'
			,[Season]=@SEASON
		
		-- CREATE A NEW STATEMENTCODE
		INSERT INTO RP_Statements(StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,Category,DataSetCode,DataSetCode_L5)
		SELECT StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,Category,0 as DataSetCode,0 as DataSetCode
		FROM #RP_STATEMENTS
					   
		SELECT @NEW_STATEMENTCODE=StatementCode 		
		FROM RP_Statements		
		WHERE StatementCode=SCOPE_IDENTITY() 

		INSERT INTO RPWeb_StatementInformation (StatementCode, FieldName, FieldValue)
		VALUES(@NEW_STATEMENTCODE, 'Meta Data', '{"title":"' +  @REWARD_PLANNER_NAME + '","date_created":"' + (SELECT convert(varchar, getdate(), 121)) + '","date_modified":"' + (SELECT convert(varchar, getdate(), 121)) + '", "source_type":"' + @SCENARIO_TYPE + '"}')

		/* LETS START DUPLIATING DATA FROM SOURCE STATEMENT TO NEW STATEMENT */
		BEGIN
			-- DUPLICATE STATEMENT MAPPINGS
			INSERT INTO RP_StatementsMappings(StatementCode,RetailerCode,ActiveRetailerCode)
			SELECT @NEW_STATEMENTCODE,RetailerCode,ActiveRetailerCode
			FROM RP_StatementsMappings
			WHERE StatementCode=@SOURCE_STATEMENTCODE

			/*
			-- MAY BE ACCOUNTS TEAM WOULD COME BACK AND ASK TO DUPLICATE EDIT INPUT VALUES AS WELL
			-- IF SUCH IS THE CASE, IT CAN BE IMPLEMENTED FOR THE SAME SEASON (SORUCE FROM SAME SEASON)
			-- AS EDIT INPUT VALUES MIGHT CHANGE SEASON OVER SEASON
			INSERT INTO [dbo].[RPWeb_User_EI_FieldValues](StatementCode,FieldCode,ProgramCode,MarketLetterCode,FieldValue,CalcFieldValue)
			SELECT @NEW_STATEMENTCODE,FieldCode,ProgramCode,MarketLetterCode,FieldValue,CalcFieldValue
			FROM [dbo].[RPWeb_User_EI_FieldValues]
			WHERE [StatementCode]=@SOURCE_STATEMENTCODE
			*/
			
			--  THIS QUITE NOT A DUPLICATION
			-- LINK PASSED IN SCENARIO CODE WITH NEW STATEMENTCODE 
			INSERT INTO RP_DT_Scenarios(StatementCode, RetailerCode, DataSetCode, Season, ScenarioCode, Status, DateModified)
			VALUES(@NEW_STATEMENTCODE,@RETAILERCODE, 0, @SEASON, @SCENARIO_CODE, @SCENARIO_STATUS, @DATE_MODIFIED) 
			--VALUES(@NEW_STATEMENTCODE,@RETAILERCODE, 0, @SCENARIO_SEASON, @SCENARIO_CODE, @SCENARIO_STATUS, @DATE_MODIFIED) 
		
			-- LOGIC IN CASE OF LEVEL 2 REAILERS (LEVEL 2 STATEMENTS)
			-- GETTING SCENARIO PLANS FOR LEVEL 1 LOCATIONS
			INSERT INTO RP_DT_Scenarios(StatementCode, RetailerCode, DataSetCode, Season, ScenarioCode, Status, DateModified)
			SELECT @NEW_STATEMENTCODE,RetailerCode, DataSetCode, Season, ScenarioCode, Status, DateModified
			FROM RP_DT_Scenarios
			WHERE StatementCode=@SOURCE_STATEMENTCODE AND RetailerCode <> @RETAILERCODE
		END
		
		/* LETS REFFRESH SCENARIO TRANSACTIONS ONLY */
		-- REFRESH TRANSACTIONS FOR NEWLY CREATED STATEMENTS
		DECLARE @CONDITION VARCHAR(255)
		SET @CONDITION = 'STATEMENTCODE=' + CAST(@NEW_STATEMENTCODE AS VARCHAR)
		EXEC RP_Agent_Refresh @SEASON, 'REWARD PLANNER', 'TRANSACTIONS', @CONDITION
				
		-- REFRESH REWARD CALCULATIONS 
		EXEC [rew_planner].[RefreshRewards] @NEW_STATEMENTCODE				

		-- CREATE A LOCKED VERSION
		DECLARE @STATEMENTCODES VARCHAR(MAX)=CAST(@NEW_STATEMENTCODE AS VARCHAR)
		EXEC [rew_planner].[LockStatements] @SEASON,  @STATEMENTCODES

		SELECT 'success' AS [status], @NEW_STATEMENTCODE as [statementcode]
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [status], ERROR_MESSAGE() AS [error_message]
		RETURN;	
	END CATCH

END
