﻿CREATE PROCEDURE [rew_planner].[UpdateBrandInputs] @STATEMENTCODE INT, @JSON NVARCHAR(MAX), @VERSION_DETAILS VARCHAR(1000)
AS
BEGIN

	SET NOCOUNT ON;

	/* 
	-- SAMPLE FOR MULTIPLE CHEMICAL GROUPS SUBMITTED AT A TIME
	DECLARE @JSON NVARCHAR(MAX)='{
		"statementcode":1
		,"input_values":[{"chemicalgroup":"Caramba","input_value":125} ,{"chemicalgroup":"Insure Pulse","input_value":80}   ]}'
				
	SELECT a.StatementCode, b.chemicalgroup, CAST(b.input_value/100 AS NUMERIC(8,4)) as PercentageValue
	FROM OPENJSON(@JSON)
		WITH (
			statementcode INT N'$.statementcode',				
			input_values NVARCHAR(MAX) AS JSON
		) AS a
		CROSS APPLY
			OPENJSON(a.input_values)
			WITH (
				chemicalgroup VARCHAR(50)
				,input_value NUMERIC(18,2)					
			) AS b

	-- ONE CHEMCIAL GROUP AT A TIME
	JSON Sample : {"statementcode":1,"chemicalgroup":"Caramba","input_value":125}	

	SELECT a.StatementCode, a.chemicalgroup, CAST(a.PercentageValue/100 AS NUMERIC(8,4)) as PercentageValue
	FROM OPENJSON(@JSON)
		WITH (
			statementcode INT N'$.statementcode',
			chemicalgroup varchar(50) N'$.chemicalgroup',
			PercentageValue numeric(8,4) N'$.input_value'
		) AS a
	*/

	BEGIN TRY

		DECLARE @TEMP_VALUES TABLE( 
			StatementCode INT,
			ChemicalGroup VARCHAR(50),
			PercentageValue NUMERIC(8,4),		
			PRIMARY KEY CLUSTERED (
				[StatementCode] ASC,
				[ChemicalGroup] ASC				
			) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
		)

		DECLARE @VERSION_TITLE VARCHAR(500), @DATE_CREATED AS VARCHAR(20), @SOURCE_TYPE VARCHAR(20)

		SET @VERSION_TITLE = JSON_VALUE(@VERSION_DETAILS,'$.title')
		SET @DATE_CREATED = JSON_VALUE(@VERSION_DETAILS,'$.date_created')
		SET @SOURCE_TYPE = JSON_VALUE(@VERSION_DETAILS,'$.source_type')

	   	INSERT INTO @TEMP_VALUES(StatementCode, ChemicalGroup, PercentageValue)
		SELECT @STATEMENTCODE, a.chemicalgroup, CAST(a.PercentageValue/100 AS NUMERIC(8,4)) as PercentageValue
		FROM OPENJSON(@JSON)
			WITH (
				--statementcode INT N'$.statementcode',
				chemicalgroup varchar(50) N'$.chemicalgroup',
				PercentageValue numeric(8,4) N'$.input_value'
			) AS a
			
		MERGE RPWeb_User_BrandInputs TGT
		USING (
			SELECT StatementCode, ChemicalGroup, PercentageValue
			FROM @TEMP_VALUES
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.ChemicalGroup=TGT.ChemicalGroup
		WHEN MATCHED AND SRC.PercentageValue <> TGT.PercentageValue THEN 
			UPDATE SET TGT.PercentageValue=SRC.PercentageValue
		WHEN NOT MATCHED THEN
			INSERT (StatementCode, ChemicalGroup, PercentageValue)  
			VALUES (SRC.StatementCode, SRC.ChemicalGroup, SRC.PercentageValue);

		UPDATE RPWeb_StatementInformation
		SET FieldValue = '{"title":"' +  @VERSION_TITLE + '","date_created":"' + @DATE_CREATED + '","date_modified":"' + (SELECT convert(varchar, getdate(), 121)) + '", "source_type":"' + @SOURCE_TYPE + '"}'
		WHERE StatementCode=@STATEMENTCODE AND FieldName='Meta Data'

		/*
		-- Update eligible sales
		SELECT @STATEMENT_TYPE = StatementType FROM RP_Statements WHERE StatementCode = @STATEMENTCODE
		DECLARE @STATEMENTS AS UDT_RP_STATEMENT
		INSERT INTO @STATEMENTS VALUES (@STATEMENTCODE)
		EXEC RP_Calc_ML_Eligible_Sales @SEASON, @STATEMENT_TYPE, @STATEMENTS;
		*/

		-- NOW RECALCULATE REWARDS 	
		EXEC [rew_planner].[RefreshRewards] @STATEMENTCODE

		/*
		-- Temporary until Chris can update JS
		DECLARE @TARGET_STATEMENTCODE INT
		SELECT @TARGET_STATEMENTCODE = MAX(StatementCode) FROM @TEMP_VALUES
		EXEC [rew_planner].[RefreshRewards] @TARGET_STATEMENTCODE
		*/

		SELECT 'success' AS [Status]
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

END
