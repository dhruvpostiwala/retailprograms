﻿
CREATE   PROCEDURE [rew_planner].[Get_RewardsSummary] @STATEMENTCODE INT
	,@JSON NVARCHAR(MAX) OUTPUT
	,@EXECUTION_STATUS AS VARCHAR(50) OUTPUT
	,@ERROR_MESSAGE NVARCHAR(2048) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY	

		--DECLARE @STATEMENTCODE INT = 8356;
		DECLARE @STATEMENTCODE_UNLOCKED_TGT INT;
		DECLARE @STATEMENTCODE_LOCKED_SRC INT = 0;
		DECLARE @SalesSummary NVARCHAR(MAX);
		DECLARE @MaximizeRewardMessages NVARCHAR(MAX);
		DECLARE @RewardsDetails NVARCHAR(MAX);

		-- get source and target statements
		SET @STATEMENTCODE_UNLOCKED_TGT = @STATEMENTCODE;
		SET @STATEMENTCODE_LOCKED_SRC = (SELECT StatementCode FROM RPWeb_Statements WHERE SRC_StatementCode = @STATEMENTCODE_UNLOCKED_TGT)
	
		-- Get Sales at the Retail + GroupLabel + ML level
		IF OBJECT_ID('tempdb..#TEMP_Sales') IS NOT NULL DROP TABLE #TEMP_Sales
		SELECT RetailerCode, GroupLabel,
			MAX(MarketLetterCode) AS 'MarketLetterCode',
			MAX(Pricing_Type) AS 'Pricing_Type',
			SUM(ISNULL(Sales_CY_TGT,0)) AS 'Sales_CY_TGT', 
			SUM(ISNULL(Sales_CY_SRC,0)) AS 'Sales_CY_SRC', 
			SUM(ISNULL(Sales_LY1,0)) AS 'Sales_LY1'
		INTO #TEMP_Sales
		FROM (
			SELECT TX.StatementCode, TX.RetailerCode, TX.GroupLabel,
				MAX(TX.MarketLetterCode) AS 'MarketLetterCode',
				MAX(TX.Pricing_Type) AS 'Pricing_Type',
				SUM(IIF(TX.StatementCode = @STATEMENTCODE_UNLOCKED_TGT, TX.Sales_CY * ISNULL(BI.PercentageValue, 1), 0)) AS 'Sales_CY_TGT',
				SUM(IIF(TX.StatementCode = @STATEMENTCODE_LOCKED_SRC, TX.Sales_CY, 0)) AS 'Sales_CY_SRC',
				SUM(IIF(TX.StatementCode = @STATEMENTCODE_UNLOCKED_TGT, TX.Sales_LY1, 0)) AS 'Sales_LY1'
			FROM RPWeb_ML_Elg_Sales TX
			LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = TX.STatementCode AND BI.ChemicalGroup = TX.GroupLabel
			WHERE TX.[StatementCode] IN (@STATEMENTCODE_UNLOCKED_TGT,@STATEMENTCODE_LOCKED_SRC)
			GROUP BY TX.StatementCode, TX.RetailerCode, TX.GroupLabel
		) DATA
		GROUP BY RetailerCode, GroupLabel	

		-- Remember to return All column labels in lowercase
		SELECT @SalesSummary = ISNULL((
			SELECT ML.marketlettercode AS 'ml_code', ML.title AS 'ml_title',
				ISNULL(summary_eligible.Sales_CY_TGT,0) AS 'total_eligible_cy_tgt',
				ISNULL(summary_eligible.Sales_CY_SRC,0) AS 'total_eligible_cy_src',
				ISNULL(summary_eligible.Sales_LY1,0) AS 'total_eligible_ly_tgt'
			FROM RP_Config_MarketLetters ML
			INNER JOIN (
				SELECT DISTINCT MarketLetterCode	
				FROM RPWeb_ML_ELG_Programs 
				WHERE StatementCode = @STATEMENTCODE
			) MLP ON MLP.MarketLetterCode=ML.MarketLetterCode
			LEFT JOIN (
				SELECT MarketLetterCode, SUM(Sales_CY_TGT) AS 'Sales_CY_TGT', SUM(Sales_CY_SRC) AS 'Sales_CY_SRC', SUM(Sales_LY1) AS 'Sales_LY1'
				FROM #TEMP_Sales 
				GROUP BY MarketLetterCode
			) summary_eligible ON Summary_Eligible.MarketLetterCode = MLP.MarketLetterCode
			ORDER BY ML.[Sequence]
			FOR JSON AUTO
		),'[]')

		-- Maximize Reward Messages
		SELECT @MaximizeRewardMessages = ISNULL((
			SELECT ML.Title AS 'ml_title', PRG.Title AS 'program_title', T1.maximizerewardmessage
			FROM RPWeb_MaximizeRewardMessages T1
				INNER JOIN RP_Config_MarketLetters ML 		ON ML.MarketLetterCode=T1.MarketLetterCode
				INNER JOIN RP_Config_Programs PRG			ON PRG.ProgramCode=T1.ProgramCode
			WHERE StatementCode=@Statementcode
			Order By ml.[Sequence], program_title
			FOR JSON PATH
		),'[]')

		-- Get line item details
		SELECT @RewardsDetails = ISNULL((
			SELECT src.RowNumber as row_number, 
				src.rowtype as row_type, 
				src.marketlettercode, 
				src.marketletterpdf,
				src.rewardcode,
				IIF(src.rowlabel='',tgt.rowlabel,src.rowlabel) AS 'rowlabel',
				src.displayinput AS 'src_displayinput', 
				src.inputprefix AS 'src_inputprefix',
				src.inputtext AS 'src_inputtext',
				src.inputsuffix AS 'src_inputsuffix', 
				src.displayqualifier AS 'src_displayqualifier', 
				src.qualifierprefix AS 'src_qualifierprefix',
				src.qualifier AS 'src_qualifier' , 
				src.qualifiersuffix AS 'src_qualifiersuffix', 
				src.reward AS 'src_reward' ,
				src.ProgramDescription AS 'tgt_description', 
				tgt.displayinput AS 'tgt_displayinput', 
				tgt.inputprefix AS 'tgt_inputprefix', 
				tgt.inputtext AS 'tgt_inputtext' , 
				tgt.inputsuffix AS 'tgt_inputsuffix',
				tgt.displayqualifier AS 'tgt_displayqualifier',
				tgt.qualifierprefix AS 'tgt_qualifierprefix', 
				tgt.qualifier AS 'tgt_qualifier', 
				tgt.qualifiersuffix AS 'tgt_qualifiersuffix', 
				tgt.reward AS 'tgt_reward',
				IIF(src.rowtype = 'Program Total','Sample Text','') AS 'maximize_reward_description'
			FROM TVF2022_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_LOCKED_SRC) src 
				LEFT JOIN TVF2022_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_UNLOCKED_TGT) tgt on src.RowNumber = tgt.RowNumber
			WHERE src.[RenderRow] = 'YES'
			ORDER BY src.RowNumber
			FOR JSON PATH
		),'[]') 

		SET @JSON = '{"sales":' + @SalesSummary + ',"max_reward_messages":' + @MaximizeRewardMessages + ',"letters":' + @RewardsDetails + '}'
		SET @EXECUTION_STATUS = 'success'
	END TRY
					
	BEGIN CATCH
		SET @EXECUTION_STATUS='failed'
		SET @ERROR_MESSAGE = ERROR_MESSAGE() + ' in Get_RewardsSummary at Line ' + CAST(ERROR_LINE() AS VARCHAR)
		RETURN;	
	END CATCH

END
