﻿CREATE PROCEDURE [rew_planner].[DeletePlan] @STATEMENTCODE INT, @RETAILERCODE VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY 
		UPDATE RP_Statements SET Status = 'Deleted' WHERE StatementCode=@STATEMENTCODE AND RetailerCode=@RETAILERCODE AND StatementType='REWARD PLANNER'
		SELECT 'success' AS [status]
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [status] ,ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  as [error_message]
		
		RETURN;	
	END CATCH
END
