﻿CREATE PROCEDURE [rew_planner].[Get_RewardPlannerData] @RETAILERCODE VARCHAR(20), @SEASON INT,  @SRC_STATEMENTCODE INT=NULL
AS
BEGIN
	SET NOCOUNT ON;

	/*
		ON FRONT-END, WHEN USER CLICKS BUTTON "REWARD PLANNER", THERE IS NO STATEMENT CODE VALUE TO PASS IN
		HENCE WE WOULD DETERMINE STATEMENT CODE (RECENTLY MODIFIED) AND WILL BE USING THAT STATEMENT CODE TO DEFAULT TO
		OBTAIN BRAND INPUTS, PROGRAM INPUTS AND REWARDS DETAILS
	*/

	BEGIN TRY 
		
		/*
		DECLARE @RETAILERCODE VARCHAR(20) = 'R770027269'
		DECLARE @SEASON INT = 2022
		DECLARE @SRC_STATEMENTCODE INT = 8960
		*/

		DECLARE @EXECUTION_STATUS AS VARCHAR(50) ,@ERROR_MESSAGE NVARCHAR(2048) 
		DECLARE @STATEMENTCODE INT, @REGION VARCHAR(4)
		DECLARE @JSON  NVARCHAR(MAX)
		DECLARE @JSON_VERSIONS NVARCHAR(MAX) = '[]'
		DECLARE @PLAN_TRANSACTIONS NVARCHAR(MAX) = '[]'
		DECLARE @JSON_BRANDINPUTS NVARCHAR(MAX) = '[]'
		DECLARE @JSON_PROGRAM_INPUTS NVARCHAR(MAX) = '{}'
		DECLARE @JSON_REWARD_SUMMARY NVARCHAR(MAX) = '{}'

		DECLARE @TEMP_JSON NVARCHAR(MAX) = '{}'

		-- STEP #1 : GET STATEMENTCODES AND META DATA (IGNORE PASSED IN STATEMENTCODE)
		-- EVEN WHEN STATEMENT CODE IS PASSED IN WE WOULD STILL RUN STEP #1
		DROP TABLE IF EXISTS #TEMP_STATEMENTS
		SELECT statementcode, title, date_created, date_modified, source_type, region,
			IIF(statementcode=@SRC_STATEMENTCODE,'true','false') as selected_statement,
			RANK() OVER (ORDER BY D.date_modified DESC) AS [Rank]  
		INTO #TEMP_STATEMENTS
		FROM (
			SELECT ST.StatementCode	,ST.Region
				,JSON_VALUE(STI.FieldValue,'$.title') AS title
				,JSON_VALUE(STI.FieldValue,'$.date_created')  as date_created
				,JSON_VALUE(STI.FieldValue,'$.date_modified') as date_modified
				,JSON_VALUE(STI.FieldValue,'$.source_type') as source_type
			FROM RP_Statements ST			
				INNER JOIN RPWeb_StatementInformation STI
				ON STI.StatementCode=ST.StatementCode AND STI.FieldName='Meta Data'
			WHERE ST.[StatementType]='Reward Planner' AND ST.[Status]='Active' AND ST.VersionType='Unlocked'
				AND (ST.[Season]=@Season OR ST.[Season]=@Season-1) AND ST.[RetailerCode]=@RetailerCode
		) D
		ORDER BY title

		IF @SRC_STATEMENTCODE IS NULL
			BEGIN
				SELECT TOP 1 @STATEMENTCODE = StatementCode , @Region=Region FROM #TEMP_STATEMENTS WHERE [Rank]=1
				IF @STATEMENTCODE IS NULL GOTO JSON_OUTPUT
			END
		ELSE
			SELECT @STATEMENTCODE=STATEMENTCODE , @REGION=Region FROM RP_Statements WHERE StatementCode=@SRC_STATEMENTCODE

		SET @JSON_VERSIONS = ISNULL((
			SELECT statementcode, title, date_created, date_modified, source_type, CAST(selected_statement AS BIT) AS selected_statement
			FROM #TEMP_STATEMENTS ORDER BY statementcode DESC
			FOR JSON PATH
		),'[]')

		-- STEP #2 - GET BRAND INPUTS
		SET @JSON_BRANDINPUTS = ISNULL((
			SELECT chemicalgroup, percentagevalue
			FROM RPWeb_User_BrandInputs
			WHERE [StatementCode]=@STATEMENTCODE
			FOR JSON PATH
		),'[]')

		-- STEP #3 - GET PROGRAM INPUTS	
		DECLARE @DATA AS TABLE (JSON NVARCHAR(MAX))
		INSERT INTO @DATA
		EXEC [dbo].[RP_Get_EI_PageSetupData]  @STATEMENTCODE
		SELECT @JSON_PROGRAM_INPUTS = JSON FROM @DATA
		SET @JSON_PROGRAM_INPUTS = ISNULL(@JSON_PROGRAM_INPUTS ,'[]')

		/*
		SET @JSON_PROGRAM_INPUTS = ISNULL((
			SELECT fieldcode,programcode,marketlettercode,fieldvalue,calcfieldvalue
			FROM RPWeb_User_EI_FieldValues
			WHERE [StatementCode]=@STATEMENTCODE
			FOR JSON PATH
		),'[]')
		*/
	
		-- STEP #4 - GET SCENARIO / PLAN TRANSACTIONS
		DROP TABLE IF EXISTS #DefaultProducts
		SELECT Category, ChemicalGroup, ProductCode, IIF(@Region='West',ConversionW, ConversionE) AS Conversion, LEUConversion
		INTO #DefaultProducts
		FROM (
			SELECT chemicalGroup, productcode, ConversionW, ConversionE, LEUConversion, Class AS 'Category' 
				,ROW_NUMBER() OVER(PARTITION BY chemicalgroup ORDER BY volume) AS 'priority'
			FROM productreference 
			WHERE Status = 'Active' AND doctype = 10

			UNION ALL

			SELECT 'INVIGOR ' + Hybrid_Name AS 'chemicalGroup', Hybrid_Code AS 'productcode', ConversionW, ConversionE, 1 AS 'LEUConversion', 'INVIGOR' as 'Category', 1  AS 'priority'
			FROM productreference_hybrids
		) Data
		WHERE [priority] = 1

		-- STEP #4A - GET HYBRID PRICING
		DROP TABLE IF EXISTS #HybridReferencePricing
		select prh.Hybrid_Code,
			MAX(prph.srp) as srp,
			MAX(prph.sdp) as sdp,
			MAX(prph.srp/IIF(@REGION='West',ConversionW,ConversionE)) as srp_ppa,
			MAX(prph.sdp/IIF(@REGION='West',ConversionW,ConversionE)) as sdp_ppa
		into #HybridReferencePricing
		from ProductReference_Hybrids prh
		join ProductReferencePricing_Hybrids prph on prh.ID = prph.Hybrid_ID
		where prph.Season = @SEASON
		group by prh.Hybrid_Code

		-- STEP #5 - GET TRANSACTIONS
		SET @PLAN_TRANSACTIONS = ISNULL((
			SELECT d.category, d.brand, d.leu,
				SUM(acres) as acres,
				MAX(srp) as srp,
				MAX(sdp) as sdp,
				MAX(srp_ppa) as srp_ppa,
				MAX(sdp_ppa) as sdp_ppa
			FROM (
				SELECT COALESCE(prh.Category,pr.category,'Unknown') AS category,
					COALESCE(prh.ChemicalGroup,pr.chemicalgroup,tx.ChemicalGroup) AS brand,
					COALESCE(prh.Conversion/prh.LEUConversion,pr.Conversion/pr.LEUConversion,1) AS leu,
					CAST(tx.acres AS DECIMAL(18,2)) AS acres,
					COALESCE(hrp.srp, prp.srp, 0) AS srp,
					COALESCE(hrp.sdp, prp.sdp, 0) AS sdp,
					COALESCE(hrp.srp_ppa, prp.srp/pr.Conversion, 0) AS srp_ppa,
					COALESCE(hrp.sdp_ppa, prp.sdp/pr.Conversion, 0) AS sdp_ppa
				FROM RPWeb_ScenarioTransactions TX				
					LEFT JOIN #DefaultProducts PR			ON PR.ChemicalGroup = TX.ChemicalGroup
					LEFT JOIN #DefaultProducts PRH			ON PRH.ProductCode = TX.HybridCode
					LEFT JOIN ProductReferencePricing PRP	ON PRP.ProductCode = PR.ProductCode AND PRP.Season = @SEASON AND PRP.Region = @REGION 
					LEFT JOIN #HybridReferencePricing HRP	ON HRP.Hybrid_Code = TX.HybridCode  
				WHERE tx.statementcode = @STATEMENTCODE AND tx.Quantity > 0					
			) D
			GROUP BY d.category, d.brand, d.leu
			ORDER BY brand
			FOR JSON PATH
		),'[]')
		
		-- STEP #6 - Get Rewards Summary
		EXEC [rew_planner].[Get_RewardsSummary] @STATEMENTCODE 
			,@JSON = @JSON_REWARD_SUMMARY OUTPUT
			,@EXECUTION_STATUS=@EXECUTION_STATUS  OUTPUT
			,@ERROR_MESSAGE=@ERROR_MESSAGE OUTPUT

	IF @EXECUTION_STATUS='failed'	
		BEGIN
			--SELECT 'failed' AS [status], @ERROR_MESSAGE AS [error_message]
			--RETURN
			THROW 51000,  @ERROR_MESSAGE, 1;  
			
		END 

JSON_OUTPUT:
		SET @JSON = '{
			"versions":' + @JSON_VERSIONS + '
			,"plan_transactions":' + @PLAN_TRANSACTIONS + '
			,"brand_inputs":' + @JSON_BRANDINPUTS + '
			,"program_inputs":' + @JSON_PROGRAM_INPUTS + '
			,"rewards_summary":' + ISNULL(@JSON_REWARD_SUMMARY,'{}') 
		+ '}'			
		SELECT 'success' AS [status], @JSON AS [data] 
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [status] ,ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  as [error_message]		
		RETURN;	
	END CATCH

END
