﻿CREATE PROCEDURE [rew_planner].[LockStatements] @SEASON INT,  @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	
	/* 
		HANDLES LOCKING MULTIPLE STATEMENTS AT A TIME
	*/
	
	SET NOCOUNT ON;
	
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;
	DECLARE @STATEMENTS_TO_LOCK UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @PROCESS_OU_TASKS BIT = 0;
	--DECLARE @PROCESS_HO_TASKS BIT = 0;
	DECLARE @SQL_COMMAND NVARCHAR(MAX);

	/*
	-- Jul-14-2021 - CURRENTLY REWARD PLANNER IS FOR WEST ONLY 
	DECLARE @OU_STATEMENTS TABLE(
		StatementCode INT,
		LockedStatementCode INT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)
	*/
	
	BEGIN TRY
	
		-- OBTAIN A UNIQUE IDENTIFIER FOR THIS JOB
		DECLARE @JOB_ID uniqueidentifier=NEWID();

		INSERT INTO @STATEMENTS(STATEMENTCODE)
		SELECT DISTINCT VALUE AS StatementCode FROM STRING_SPLIT(@STATEMENTCODES, ',')
	   	
		-- WE NEED TO GET ALL STATEMENTS CODES BELONGING TO A FAMILY
		INSERT INTO @STATEMENTS_TO_LOCK(StatementCode, StatementLevel, Retailercode, DataSetCode, DataSetCode_L5 ,Summary)
		SELECT StatementCode,StatementLevel,Retailercode,DataSetCode, DataSetCode_l5, Summary
		FROM TVF_Get_StatementCodesToRefresh(@STATEMENTS) 	

		/*
		-- Jul-14-2021 - CURRENTLY REWARD PLANNER IS FOR WEST ONLY 
		INSERT INTO @OU_STATEMENTS(StatementCode)
		SELECT DISTINCT DataSetCode FROM @STATEMENTS_TO_LOCK WHERE DataSetCode > 0
		

		IF EXISTS(SELECT * FROM @OU_STATEMENTS)
			SET @PROCESS_OU_TASKS=1
		*/
		
	
		/* LETS START LOCKING PROCESS 
			- GENERATE NEW LOCKED STATEMENT CODE (ON NEWLY INSERTED RECORD, SRC_STATEMENTCODE COLUMN VALUE SHOULD BE SET TO UNLOCKED STATEMENT CODE)
			- COPY UNLOCKED DATA FOR LOCKED STATEMENTS
		*/
		DELETE FROM @STATEMENTS		   		 
		INSERT INTO @STATEMENTS(StatementCode)
		SELECT UL.[StatementCode]
		FROM @STATEMENTS_TO_LOCK UL		
		/*
			INNER JOIN RPWeb_StatementInformation sti		
			ON sti.[StatementCode]=UL.[StatementCode]
	*/

		-- CHECK TO MAKE SURE NOT LOCKED STATEMENT EXIST 


		/* INSERT NEW LOCKED STATEMENT INTO WEB STATEMENTS TABLE */	
		/* WEB STATEMENTS TABLE STATEMENTCODE COLUMN IS DEFINED AS IDENTITY COLUMN, SET TO INSERT NEGATIVE STATEMENT CODES */
		INSERT INTO RPWeb_Statements(Locked_Date,Lock_Job_ID, VersionType, StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, SRC_StatementCode, ChequeRunName , ChequeRunID) 	
		SELECT GETDATE(), @JOB_ID, 'Locked', StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, StatementCode	
			,'' AS ChequeRunName 
			,NULL AS ChequeRunID
		FROM RPWeb_Statements ST
		WHERE StatementCode IN (SELECT [StatementCode] FROM @STATEMENTS)

	/*
		-- Jul-14-2021 - CURRENTLY REWARD PLANNER IS FOR WEST ONLY 
		/*
			REGION: EAST
			ON NEWLY CREATED LOCKED STATEMENTS: WE NEED TO UPDATE DATASETCODE, AS COPIED OVER DATASETCODE IS POINTING TO UNLOCKED LEVEL 2 STATEMENTCODE.
			DATASETCODE SHOULD POINT TO LOCKED STATEMENTCODE OF LEVEL 2 STATEMENT WITHIN FAMILY
		*/
		IF @PROCESS_OU_TASKS=1
		UPDATE T1
		SET DataSetCode=T2.StatementCode
		FROM RPWeb_Statements  T1
			INNER JOIN RPWeb_Statements  T2
			ON T2.Lock_Job_ID=T1.Lock_Job_ID AND T2.SRC_StatementCode=T1.DataSetCode
		WHERE T1.Lock_Job_ID=@JOB_ID AND T1.Region='EAST' AND T1.VersionType='Locked' AND T1.[Status]='Active'
	*/


		/* COPY DATA FROM UNLOCKED STATEMENTS TO ASSOCIATED LOCKED STATEMENTS */
		DECLARE @COPY_CMD NVARCHAR(MAX)
		DECLARE @COPY_PARAMS NVARCHAR(50)='@JOB_ID uniqueidentifier'
		DECLARE COPY_LOCKED_DATA CURSOR FOR 
			SELECT 'INSERT INTO '+so.[name]+'([StatementCode],'+STUFF((
				SELECT ',['+[Name]+']'
				FROM syscolumns sc
				/* LETS NOT INCLUDE THESE COLUMNS - WE ARE UPDATING STATEMENT CODE TO BE THE LOCKED STATEMENT CODE, THE REST ARE UNIQUE RECORD IDS */
				WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id] 
				ORDER BY sc.[id]
				FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+')
			SELECT st.[StatementCode],'+STUFF((
				SELECT ',custom_table.['+[Name]+']'
				FROM syscolumns sc
				WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id]
				ORDER BY sc.[id]
				FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+'
			FROM '+so.[name]+' custom_table
				INNER JOIN RPWeb_Statements ST
				ON ST.[SRC_StatementCode]=custom_table.[StatementCode]
			WHERE ST.[Status]=''Active'' AND ST.[Lock_Job_ID]=@JOB_ID AND ST.[VersionType]=''Locked''' AS [CODE]
			FROM sysobjects so
			WHERE so.[xtype]='U' AND so.[name] IN (
				SELECT TableName	
				FROM RP_Config_WebTableNames 
				WHERE Season=@SEASON AND LockStatementAction='Yes'
			)

		OPEN COPY_LOCKED_DATA
		FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
		WHILE(@@FETCH_STATUS=0)
			BEGIN	
				EXEC SP_EXECUTESQL @COPY_CMD,@COPY_PARAMS,@JOB_ID
				FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
			END
		CLOSE COPY_LOCKED_DATA
		DEALLOCATE COPY_LOCKED_DATA	  

		/*
		-- Jul-14-2021 - CURRENTLY REWARD PLANNER IS FOR WEST ONLY 
		-- LETS UPDATE DATASETCODES		
		IF @PROCESS_OU_TASKS=1  -- OR @PROCESS_HO_TASKS=1
		BEGIN 		
			DECLARE UPDATE_DATASETCODES CURSOR FOR 
				SELECT '
					UPDATE T1
					SET [DataSetCode]=T2.[DataSetCode] ,[DataSetCode_L5]=T2.[DataSetCode_L5]
					FROM '+so.[name]+' T1
						INNER JOIN (
							SELECT [StatementCode], [DataSetCode], [DataSetCode_L5]
							FROM RPWEB_Statements 
							WHERE [Lock_Job_ID]=@JOB_ID AND [Status]=''Active''  AND [Versiontype]=''Locked'' AND [StatementLevel]=''LEVEL 1''  
								AND ([DataSetCode] < 0 OR [DataSetCode_L5] < 0)				
						) T2
						ON T2.Statementcode=T1.Statementcode ' AS [Code]
				FROM sysobjects so
				WHERE so.[xtype]='U' AND so.[name] IN (SELECT TableName	FROM RP_Config_WebTableNames WHERE Season=@Season AND UpdateDataSetCodes='Yes')
			   
			OPEN UPDATE_DATASETCODES
			FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
			WHILE(@@FETCH_STATUS=0)
				BEGIN	
					EXEC SP_EXECUTESQL @SQL_COMMAND, N'@SEASON INT, @JOB_ID uniqueidentifier', @SEASON, @JOB_ID
					FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
				END
			CLOSE UPDATE_DATASETCODES
			DEALLOCATE UPDATE_DATASETCODES
		END 
		*/

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:
	SELECT 'success' as [Status]
		,ISNULL(@JOB_ID,'') AS Lock_Job_ID  
		,'' AS Error_Message
		,'' as confirmation_message
		,'' as warning_message

END
