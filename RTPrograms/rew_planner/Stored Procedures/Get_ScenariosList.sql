﻿CREATE PROCEDURE [rew_planner].[Get_ScenariosList] @SEASON INT, @RETAILERCODE VARCHAR(20), @MODULE VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON_VARCHAR VARCHAR(4)=CAST(@SEASON AS VARCHAR(4));
	DECLARE @JSON  NVARCHAR(MAX)

	BEGIN TRY 

		--D0000117 FCL COOPS - BR - RETAIL CONNECT	
		-- option 1: 2021 Projected Plan (most recently saved id) rank
		-- option 2: 2022 POG PLan (final id OR pref id) rank

		-- NEW: RRT-2460 fixed so we have the same options as FCL (2021 and 2022)

		--D000001 IND WEST - RCAMs - RC REP TOOL
		-- option 1: 2022 POG PLan (final id OR pref id)
		-- option 2: 2022 Projected Plan (most recently saved id)

		IF @MODULE='RC'
		BEGIN
			SET @JSON = ISNULL((
				SELECT *
				FROM (
    					SELECT [Season] AS 'season', [RetailerID] AS 'retailercode', [ScenarioID] AS 'scenario_id', CONVERT(date, [Modified]) AS 'date_modified', [Status] AS 'status', [ScenarioType] AS 'scenario_type', [Name] AS 'name'
    					FROM (
        					SELECT [ScenarioID], [RetailerID], [Season], [Modified], [Status], [ScenarioType], [Name], ROW_NUMBER() OVER(PARTITION BY [Season], [RetailerID] ORDER BY [Status] ASC) AS 'plan_rnk'
        					FROM Retail_Plan_Scenarios
						WHERE [RetailerID] = @RETAILERCODE AND [Season] = @SEASON_VARCHAR AND [ScenarioType] = 'Forecast' AND [Status] IN ('Final','Preferred')
    					) pog
    					WHERE pog.plan_rnk = 1
					UNION ALL
    					SELECT [Season] AS 'season', [RetailerID] AS 'retailercode', [ScenarioID] AS 'scenario_id', CONVERT(date, [Modified]) AS 'date_modified', [Status] AS 'status', [ScenarioType] AS 'scenario_type', [Name] AS 'name'
    					FROM (
        					SELECT [ScenarioID], [RetailerID], [Season], [Modified], [Status], [ScenarioType], [Name], ROW_NUMBER() OVER(PARTITION BY [Season], [RetailerID] ORDER BY [Modified] DESC) AS 'plan_rnk'
        					FROM Retail_Plan_Scenarios
						WHERE [RetailerID] = @RETAILERCODE AND [Season] = @SEASON_VARCHAR-1 AND [ScenarioType] = 'Projection'
    					) prj
    					WHERE prj.plan_rnk = 1
				) x
				FOR JSON PATH ),'[]')
		END
			
		IF @MODULE = 'RCSC'
		BEGIN
			SET @JSON = ISNULL((
				SELECT POG.retailercode, [Season] as season, CAST(POG.ID AS VARCHAR(20)) AS scenario_id
					,scenario_type 
					,'FINAL' AS [status] , 'Scenario Name ...' [name]
					,ISNULL(tx.DateModified,POG.SYS_Start) AS date_modified
				FROM RCT_Scenarios POG							
					INNER JOIN (
						SELECT ID, Season, IIF([Type]='PLAN','Forecast',[Type]) as Scenario_Type
						FROM RCT_PlanningModes 
						WHERE [Season] IN (@SEASON,@SEASON-1) AND [Type] IN ('PLAN','PROJECTION') AND [Status]='Active'
					) PM
					ON PM.ID=POG.PlanningModeID 
					LEFT JOIN (
						SELECT ScenarioID as ID, MIN(UserModified_Date) AS DateModified
						FROM RCT_ScenarioDetails 
						GROUP BY ScenarioID
					)	tx
					ON tx.ID=POG.ID			
				WHERE POG.REtailerCode=@RETAILERCODE AND POG.[Primary]=1   
				FOR JSON PATH),'[]')

		END

		SELECT 'success' AS [status], @JSON AS [data], '' as [error_message]
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [status] ,ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  as [error_message]
		
		RETURN;	
	END CATCH

END
