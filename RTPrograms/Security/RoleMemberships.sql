﻿ALTER ROLE [db_owner] ADD MEMBER [THEKENNAGROUP\BASFCA_Admins];


GO
ALTER ROLE [db_owner] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Admin];


GO
ALTER ROLE [db_owner] ADD MEMBER [SQLAdminDev];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [LimaDev];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [THEKENNAGROUP\developer];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [THEKENNAGROUP\technology.dev.coops];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [WebAppDev];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Developers];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Admin];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Kenna_WebAppDev];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Kenna_LimaDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [LimaReadDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [LimaDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [THEKENNAGROUP\developer];


GO
ALTER ROLE [db_datareader] ADD MEMBER [THEKENNAGROUP\technology.dev.coops];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DataLoaderReadDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [WebAppDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [WebAppReadDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Developers];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Admin];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Kenna_WebAppDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Kenna_LimaDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Kenna_LimaReadDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Kenna_WebAppReadDev];


GO
ALTER ROLE [db_datareader] ADD MEMBER [THEKENNAGROUP\BASFCA_Readers];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Kenna_DataLoaderReadDev];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [LimaDev];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [THEKENNAGROUP\developer];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [THEKENNAGROUP\technology.dev.coops];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [WebAppDev];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Developers];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Admin];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [Kenna_WebAppDev];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [Kenna_LimaDev];

