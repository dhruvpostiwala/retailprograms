﻿CREATE ROLE [db_executor]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [db_executor] ADD MEMBER [LimaDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [THEKENNAGROUP\developer];


GO
ALTER ROLE [db_executor] ADD MEMBER [THEKENNAGROUP\technology.dev.coops];


GO
ALTER ROLE [db_executor] ADD MEMBER [DataLoaderReadDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [WebAppDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [WebAppReadDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [CA3BASF-CASQL01\BASFCAN_Developers];


GO
ALTER ROLE [db_executor] ADD MEMBER [Kenna_WebAppDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [Kenna_LimaDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [Kenna_WebAppReadDev];


GO
ALTER ROLE [db_executor] ADD MEMBER [Kenna_DataLoaderReadDev];

