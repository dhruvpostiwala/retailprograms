﻿


CREATE PROCEDURE [Grids].[RP_Statements_GGD_UseCases] @SEASON INT, @USER NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN
	/*
		DEMAREY BAKER
		Returns use case statement in json format

	*/
	SET NOCOUNT ON;
	
	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));
	

	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';
	
	---ACCESS CONTROLS
	IF @USERROLE NOT IN ('KENNA','CALLCENTER','HO')
	BEGIN
		SET @ERROR_STRING = 'You are not authorized to access this grid!';
		GOTO FINAL_EXIT
	END 

	
	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'USE CASE',@SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	
	

	IF @SEARCH_CONDITION <> '' 
		SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;
	

	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		--WILL FIND A BETTER WAY TO DO THIS 
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
		,count(*) OVER() AS total_row_count 	 	  		
		FROM (
		SELECT st.Level5Code + '' '' + st.retailercode + '' '' + sti.FieldValue + '' '' + rp.RetailerName + '' '' + rp.Phone as [searchstring]
			,st.statementcode  as scode, st.status, st.statementlevel ,st.retailercode
			,sti.FieldValue as [use_case_name], sti_dt.FieldValue as [created_date]
			,rp.retailername,rp.mailingcity as city,rp.MailingProvince as province
		FROM RPWeb_Statements ST
			INNER JOIN RPWeb_StatementInformation STI 			ON STI.StatementCode=ST.StatementCode AND STI.FieldName=''UseCaseName''
			INNER JOIN RPWeb_StatementInformation STI_DT 			ON STI_DT.StatementCode=ST.StatementCode AND STI_DT.FieldName=''UseCaseDate''
			INNER JOIN [dbo].[RetailerProfile] rp				ON rp.RetailerCode = st.retailerCode
		WHERE ST.Season='+ @SEASON_STRING + ' AND ST.VersionType=''Use Case'' AND ST.StatementType=''Actual'' AND ST.Status=''Active''			
			 
	) BaseResulteSet ' 
	+ ISNULL(@SEARCH_CONDITION,'') + 
	+ ISNULL(@ORDER_BY,'') + '
	OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	
	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			DROP TABLE IF EXISTS #GRID_DATA
			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
				BEGIN
				  SELECT Top 1 @TotalCount = IsNull([total_row_count], 0 ) FROM #GRID_DATA

				/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]
				END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY

		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS error_msg
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

FINAL_EXIT:
	--we will return error json if access issues etc
	IF @ERROR_STRING = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'error' AS status,@ERROR_STRING AS error_msg

END
