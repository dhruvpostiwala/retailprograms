﻿


CREATE PROCEDURE [Grids].[RP_Statements_Get_Cheque_Runs] @SEASON INT
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TOTALCOUNT INT
		DECLARE @JSON_DATA NVARCHAR(MAX)
		
		IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP
		SELECT ID [id], Season [season], Region [region], ChequeRunName [chequerunname], [Label] as [label], [Status] [status], Comments [comments]
		INTO #TEMP
		FROM RP_Config_ChequeRuns
		WHERE Season = @SEASON
		
		SET @TOTALCOUNT = (
			SELECT COUNT(*) FROM #TEMP
		)
		
		SET @JSON_DATA = ISNULL((
			SELECT * FROM #TEMP ORDER BY ID DESC FOR JSON PATH
		),'[]')
		
		SELECT 'success' as status,@TOTALCOUNT [total_count], @JSON_DATA [json_data]
END
