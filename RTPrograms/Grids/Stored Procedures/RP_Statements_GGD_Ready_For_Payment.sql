﻿



CREATE PROCEDURE [Grids].[RP_Statements_GGD_Ready_For_Payment] @SEASON INT,@GRIDNAME VARCHAR(100),@REGION VARCHAR(4), @USER NVARCHAR(MAX),@TARGET NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @STATUS VARCHAR(25);
	

	SELECT @STATUS = UPPER(JSON_VALUE(@TARGET,'$.status'));
	

	
	
	DECLARE @RETAILER_GROUP VARCHAR(25);
	SELECT @RETAILER_GROUP = UPPER(JSON_VALUE(@TARGET,'$.ret_group'));

	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));
	
	
	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);


	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';
	

	DECLARE @ACL_JOIN NVARCHAR(max) = '';
	
	

	DECLARE @CARGILL VARCHAR(15) = 'D0000107';
	DECLARE @NUTRIEN VARCHAR(15) = 'D520062427'
	DECLARE @RICHARDSON VARCHAR(15) = 'D0000137'
	DECLARE @UFA VARCHAR(15) = 'D0000244'
	DECLARE @WEST_COOP VARCHAR(10) = 'D0000117'
	DECLARE @WEST_INDS VARCHAR(10) = 'D000001'

	
	--RETAILER ACCESS DETERMINATIONS
	--EXEC [Grids].[RP_Statements_Grid_Access] @USERNAME,@USERROLE,@REGION, @GRIDNAME, @ACL_JOIN = @ACL_JOIN OUTPUT, @ERROR_STRING = @ERROR_STRING OUTPUT

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_RC_REP VARCHAR(100) = '';
	DECLARE @FILTER_HORT_REP VARCHAR(100) = '';
	DECLARE @FILTER_SALES_REP VARCHAR(100) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	SET @FILTER_RC_REP = JSON_VALUE(@FILTERS, '$.rcrep');
	SET @FILTER_HORT_REP  = JSON_VALUE(@FILTERS, '$.hortrep');
	SET @FILTER_SALES_REP = JSON_VALUE(@FILTERS, '$.salesrep');
	
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE %' + @SEARCH_CONDITION +'%';

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;

	--------RETAILER GROUPS----------
	IF @RETAILER_GROUP <> 'all' 
	BEGIN
		SET @CONDITION_STRING += 
		CASE WHEN @RETAILER_GROUP = 'east_all'
				THEN ' AND ST.Region=''EAST'' AND ST.Category=''Regular'''
			 WHEN @RETAILER_GROUP = 'west_independents'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_INDS +''' '
			WHEN @RETAILER_GROUP = 'custom_aerial'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Custom Aerial''  '
			WHEN @RETAILER_GROUP = 'west_coops'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_COOP +''' '
			WHEN @RETAILER_GROUP = 'west_lc'
			THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.StatementLevel=''Level 5'' AND ST.RetailerCode IN ('''+ @CARGILL +''', '''+ @NUTRIEN +''','''+ @UFA +''','''+@RICHARDSON+''') '
		END
	END

	--FINAL CONDITION BUILDING
	--IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		
		--SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
		,count(*) OVER() AS total_row_count 	 	  		
		FROM (	
		SELECT  ST.retailercode + '' '' + ST.retailercode + '' '' + ST.level2code +  + '' '' + ST.level5code + '' '' + RP.retailername + '' '' + isnull(RC.firstname,'''') + '' '' + isnull(RC.lastname,'''') as [searchstring]
				,ST.StatementCode [scode],STI.FieldValue [status],st.statementlevel, st.datasetcode
				,ST2.Status AS Unlock_Status
				,ISNULL(LK_STI.[Comments],'''') AS [validation_failed_reason]
				,RP.retailername,RP.MailingCity [city],RP.MailingProvince [province] 			
				,ISNULL([totalreward],0) AS totalreward
				,ISNULL([paidtodate],0) AS paidtodate
				,ISNULL([currentpayment],0) AS currentpayment
				,ISNULL([nextpayment],0) AS nextpayment
		FROM RPWeb_Statements ST		
			INNER JOIN RPWeb_Statements ST2					ON ST2.StatementCode=ST.SRC_StatementCode
			INNER JOIN RPWeb_StatementInformation STI		ON STI.StatementCode = ST.SRC_StatementCode		
			INNER JOIN RetailerProfile RP					ON ST.RetailerCode = RP.RetailerCode	
			LEFT JOIN RetailerContacts RC					ON RC.RetailerCode=RP.RetailerCode AND RC.ISPrimaryContact=''Yes'' AND RC.Status=''Active''
			LEFT JOIN RPWeb_StatementInformation LK_STI		ON LK_STI.StatementCode=ST.StatementCode AND LK_STI.FieldName=''CreateChequeStatus''				
			LEFT JOIN (
				SELECT StatementCode
					,SUM(TotalReward) AS TotalReward
					,SUM(PaidToDate) AS PaidToDate
					,SUM(CurrentPayment) AS CurrentPayment
					,SUM(NextPayment) AS NextPayment
				FROM RPWeb_Rewards_Summary 
				GROUP BY StatementCode 
			)RS ON ST.StatementCode = RS.StatementCode		
			WHERE ST.Season='+ @SEASON_STRING + ' AND ST.Status=''Active'' AND ST.VersionType=''Locked''				
				AND ST.IsPayoutStatement=''Yes''
				AND STI.FieldName=''Status'' AND STI.FieldValue=''Ready for Payment'' 
				AND ISNULL(LK_STI.FieldValue,'''') IN ('''',''Validation Failed'',''Error'')

			<FILTERS_JOIN>
			' + ISNULL(@CONDITION_STRING,'') + '
	) BaseResulteSet ' 
	+ ISNULL(@SEARCH_CONDITION,'') + 
	+ ISNULL(@ORDER_BY,'') + '
	OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	


	IF @ACL_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<ACL_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<ACL_JOIN>',@ACL_JOIN)

	IF @FILTERS_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<FILTERS_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<FILTERS_JOIN>',@FILTERS_JOIN)
	

	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			SELECT Top 1 @TotalCount=IsNull([total_row_count],0) FROM #GRID_DATA

			/* Drop unwanted columns , no need to send this data back to browser*/
			ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]	
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS errormessage
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

	--we will return error json if access issues etc
	IF @ERROR_STRING = ''
		EXECUTE sp_executesql @Query
		
	ELSE
		SELECT 'Error' AS status,@ERROR_STRING AS errormessage

END
