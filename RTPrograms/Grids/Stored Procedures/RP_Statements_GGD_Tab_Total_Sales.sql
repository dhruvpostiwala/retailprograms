﻿



CREATE PROCEDURE [Grids].[RP_Statements_GGD_Tab_Total_Sales] @STATEMENTCODE INT, @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';


	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @STATEMENTCODE_STRING VARCHAR(10)= CONVERT(VARCHAR(10),@STATEMENTCODE);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
		
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		--WILL FIND A BETTER WAY TO DO THIS 
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
					,count(*) OVER() AS total_row_count
					,sum(acres) OVER() AS total_acres
					,sum(sdp) OVER() AS total_sdp
					,sum(srp) OVER() AS total_srp
					,sum(swp) OVER() AS total_swp  		  				
		 	 					
					FROM (
					SELECT
						isnull(rp.retailername,'''') + '' '' + isnull(pr.productname,'''')  + '' '' + tx.farmcode + '' '' + tx.retailercode as [searchstring]
						,tx.statementcode, tx.basfseason, tx.retailercode, tx.invoicenumber
						,CONVERT(char(10), tx.invoicedate,126) as invoicedate
						, tx.productcode, tx.quantity, tx.acres, tx.price_srp as srp, tx.price_sdp as sdp,tx.price_swp as swp
						,isnull(rp.retailername,'''') as retailername, isnull(rp.mailingcity,'''') as city, isnull(rp.mailingprovince,'''') as province
						,isnull(pr.productname,tx.ProductCode) as product
						,IIF(InRadius=1,''Yes'',''No'') as inradius
						,tx.farmcode
					FROM RPWeb_Transactions tx
						left join RetailerProfile RP	on RP.RetailerCode=tx.RetailerCode
						left join ProductReference PR	on PR.ProductCode=tx.Productcode
					WHERE [StatementCode]=' + @STATEMENTCODE_STRING +'
		) BaseResulteSet ' 
		+ ISNULL(@SEARCH_CONDITION,'') + 
		+ ISNULL(@ORDER_BY,'') + '
		OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	
	
	DECLARE @Query NVARCHAR(MAX) = ''
	

	-- Return values
	
	SET @Query = '
			SET NOCOUNT ON;
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			
			DECLARE @TotalCount AS INT;
			DECLARE @JSON_DATA NVARCHAR(MAX);
			DECLARE @FOOTER_JSON_DATA NVARCHAR(MAX);
			
	
			DECLARE @TotalAcres DECIMAL(18,2)=0;
			DECLARE @TotalSDP DECIMAL(18,2)=0;	
			DECLARE @TotalSRP DECIMAL(18,2)=0;
			DECLARE @TotalSWP DECIMAL(18,2)=0;
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
			BEGIN
				-- Total Records Count	
				Select Top 1 @TotalCount=IsNull(total_row_count,0) FROM #GRID_DATA	
				Select Top 1 @TotalAcres=IsNull(total_acres,0) FROM #GRID_DATA	
				Select Top 1 @TotalSDP=IsNull(total_sdp,0) FROM #GRID_DATA
				Select Top 1 @TotalSRP=IsNull(total_srp,0) FROM #GRID_DATA
				Select Top 1 @TotalSWP=IsNull(total_swp,0) FROM #GRID_DATA	

			/* Drop unwanted columns , no need to send this data back to browser*/
			ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring], [total_acres], [total_sdp], [total_srp],[total_swp]
	
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')
			SELECT @FOOTER_JSON_DATA = ISNULL((SELECT @TotalSDP  as total_sdp, @TotalSRP as total_srp,@TotalSWP as total_swp, @TotalAcres as total_acres FOR JSON PATH,WITHOUT_ARRAY_WRAPPER ) ,''{}'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS errormessage
			RETURN;	
		END CATCH
		
		Select ''success'' as status,@TotalCount As totalcount, @JSON_DATA as data, @FOOTER_JSON_DATA as data_footer

		'

	--we will return error json if access issues etc
	IF @ERROR_STRING = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status,@ERROR_STRING AS errormessage

END
