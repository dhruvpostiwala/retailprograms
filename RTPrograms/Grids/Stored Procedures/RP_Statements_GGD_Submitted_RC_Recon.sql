﻿



CREATE PROCEDURE [Grids].[RP_Statements_GGD_Submitted_RC_Recon] @SEASON INT,@GRIDNAME VARCHAR(100), @REGION VARCHAR(4), @USER NVARCHAR(MAX),@TARGET NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF UPPER (@GRIDNAME) <> 'STATEMENTS_SUBMITTED_RC_RECON' RETURN;

	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));

	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';

	

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_RC_REP VARCHAR(100) = '';
	DECLARE @FILTER_HORT_REP VARCHAR(100) = '';
	DECLARE @FILTER_SALES_REP VARCHAR(100) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	SET @FILTER_RC_REP = JSON_VALUE(@FILTERS, '$.rcrep');
	SET @FILTER_HORT_REP  = JSON_VALUE(@FILTERS, '$.hortrep');
	SET @FILTER_SALES_REP = JSON_VALUE(@FILTERS, '$.salesrep');
	
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;
	
	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
			,count(*) OVER() AS total_row_count 	 	  		
			FROM (
			SELECT ISNULL(STI.FieldValue, ''Open'') [status],
				   ST.StatementCode [scode],
				   ST.StatementLevel [statementlevel],
				   RP.RetailerName [retailername],
				   RP.MailingCity [city],
				   RP.MailingProvince [province],
				   SUM(ISNULL(RS.TotalReward, 0)) [totalreward],
				   SUM(ISNULL(RS.PaidToDate, 0)) [paidtodate],
				   SUM(ISNULL(RS.CurrentPayment, 0)) [currentpayment],
				   SUM(ISNULL(RS.NextPayment, 0)) [nextpayment],
				   ''~'' + ST.RetailerCode + '' ''
					   + ST.RetailerCode + '' ''
					   + ST.Level2Code + '' ''
					   + ST.Level5Code + '' ''
					   + RP.RetailerName + '' ''
					   + ISNULL(RC.FirstName,'''') + '' ''
					   + ISNULL(RC.LastName,'''') [searchstring]
			FROM (
				SELECT * FROM RPWeb_Statements ST
				WHERE ST.Season = '+ @SEASON_STRING +' AND ST.Status = ''Active'' AND ST.StatementType = ''Actual'' AND ST.VersionType = ''Locked''
			) ST
				INNER JOIN RetailerProfile RP
				ON ST.RetailerCode = RP.RetailerCode
	
				LEFT JOIN RPWeb_Rewards_Summary RS
				ON ST.StatementCode = RS.StatementCode
	
				LEFT JOIN RetailerContacts RC
				ON RC.RetailerCode=RP.RetailerCode AND RC.ISPrimaryContact=''Yes'' AND RC.Status=''Active''

				INNER JOIN RPWeb_StatementInformation STI
				ON ST.SRC_StatementCode = STI.StatementCode
				   AND STI.FieldName = ''Status''
				   AND STI.FieldValue = ''Submitted For RC Reconciliation''
			' + ISNULL(@CONDITION_STRING,'') + '
			GROUP BY ST.StatementLevel, RP.RetailerName, ST.RetailerCode, ST.Level2Code, ST.Level5Code,
					 RC.FirstName, RC.LastName, RP.MailingCity, RP.MailingProvince, STI.FieldValue, ST.StatementCode		
		) BaseResulteSet ' 
		+ ISNULL(@SEARCH_CONDITION,'') + 
		+ ISNULL(@ORDER_BY,'') + '
		OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	

	IF @FILTERS_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<FILTERS_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<FILTERS_JOIN>',@FILTERS_JOIN)
	

	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
			BEGIN
				SELECT Top 1 @TotalCount=IsNull([total_row_count],0) FROM #GRID_DATA

			/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS errormessage
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

	--we will return error json if access issues etc
	IF @ERROR_STRING = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status,@ERROR_STRING AS errormessage

END
