﻿


CREATE PROCEDURE [Grids].[RP_Statements_GGD_Exception_Main] @SEASON INT, @USER_INFO NVARCHAR(MAX), @TARGET VARCHAR(25), @SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON_STRING VARCHAR(4) = CONVERT(VARCHAR(4), @SEASON)
	DECLARE @USERNAME NVARCHAR(150), @USERROLE VARCHAR(50)
	DECLARE @CONDITION_STRING NVARCHAR(MAX)= '';
	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @ACL_JOIN NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';

	DECLARE @REGION AS VARCHAR(4) 
	DECLARE @GRIDNAME AS VARCHAR(100) ='EXCEPTIONS_ALL'


	SET @USERNAME = JSON_VALUE(@USER_INFO,'$.username')
	SET @USERROLE = JSON_VALUE(@USER_INFO,'$.module_access.role')
	SET @REGION = ISNULL(JSON_VALUE(@USER_INFO,'$.module_access.region'),'')

	--DETERMINE IF USER CAN ACCESS THIS GRID WHILE BUIlDING THE QUERY
	EXEC [Grids].[RP_Statements_Grid_Access] @USERNAME,@USERROLE,@REGION, @GRIDNAME, @ACL_JOIN = @ACL_JOIN OUTPUT, @ERROR_STRING = @ERROR_STRING OUTPUT


	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'EXCEPTIONS', @SORT_STRING = @ORDER_BY OUTPUT
					
	IF UPPER(@TARGET) = 'APPROVAL' 
		
		IF @USERNAME = 'Donald Fisher' OR @USERROLE IN ('HORT RSM','HORTDM')
			SET	@CONDITION_STRING += ' AND REX.Status <> ''Draft'' AND ST.Region = ''EAST'' '
		ELSE IF @USERNAME = 'HOWIE ZANDER' 
			SET	@CONDITION_STRING += ' AND REX.Exception_Value >= 25000 AND REX.Status <> ''Draft'' AND ST.Level5Code=''D000001'' '    --AND REX.Status =  ''PENDING APPROVAL - FINAL'' 
		ELSE IF @USERNAME = 'TIM BODVARSON' --AND @USERROLE='HEADOFFICE'
			SET	@CONDITION_STRING += ' AND REX.Status <> ''Draft'' AND ST.Level5Code=''D000001'''
		ELSE IF @USERNAME = 'DONNELLY MCEWEN' --AND @USERROLE='RC-TL'
			SET	@CONDITION_STRING += ' AND REX.Status <> ''Draft'' AND ST.Level5Code=''D000001'''
		ELSE -- OLD CONDITION 
			SET	@CONDITION_STRING += ' AND REX.Status <> ''Draft'' AND ST.Region = ''EAST'' '
	

	SET @BASEQUERY ='
		DECLARE @TotalCount INT=0;
		DECLARE @JSON_Data NVARCHAR(MAX);		
		
		DROP TABLE IF EXISTS #Temp
				
		SELECT 					
			ID [id],
			REX.StatementCode [StatementCode],
			RP.retailerCode [retailer_code],
			RP.RetailerName [retailer_name],
			RP.MailingCity   [city],
			RP.MailingProvince   [province],
			ST.StatementLevel [statement_level],
			REX.Exception_Desc [exception_desc],
			REX.Exception_Value [exception_value],
			REX.Exception_Type [exception_type],
			REX.Status [exception_status]			
		INTO #Temp
		FROM RPWeb_Exceptions REX
			INNER JOIN RPWeb_Statements ST
			ON ST.statementcode = REX.statementcode AND ST.Status = ''Active''
			INNER JOIN RetailerProfile RP
			ON  RP.RetailerCode = ST.RetailerCode
			<ACL_JOIN>
		WHERE REX.SRC_ID = 0 AND (ST.Region=''WEST'' OR REX.Show_IN_UL = 1)  AND REX.VersionType <> ''Void'' AND REX.Season = ' + @SEASON_STRING  + '
		' +ISNULL(@CONDITION_STRING,'')+ '
				
		SELECT @TotalCount = COUNT(*)
		FROM #Temp
				
		SELECT @JSON_Data = ISNULL((
			SELECT *
			FROM #Temp
			'+ @ORDER_BY + '
			FOR JSON PATH
		), ''[]'');
				
		SELECT ''success'' as status,@JSON_Data [json_data], @TotalCount [totalcount]'

	IF @ACL_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<ACL_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<ACL_JOIN>',@ACL_JOIN)


	--PRINT @BASEQUERY
	EXECUTE sp_executesql @BASEQUERY

END
