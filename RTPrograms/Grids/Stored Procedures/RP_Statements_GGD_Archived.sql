﻿


CREATE PROCEDURE [Grids].[RP_Statements_GGD_Archived] @SEASON INT,@GRIDNAME VARCHAR(100), @REGION VARCHAR(4), @USER NVARCHAR(MAX),@TARGET NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';

	IF UPPER(@GRIDNAME) <> 'STATEMENTS_ARCHIVED'
	BEGIN
		SET @ERROR_STRING = 'UNKNOWN GRID REQUEST'
		GOTO FINAL_EXIT
	END


	
	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));
	DECLARE @STATUS VARCHAR(25);

	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	
	DECLARE @CARGILL VARCHAR(20) =   [dbo].[SVF_GetDistributorCode]('CARGILL') 
	DECLARE @NUTRIEN VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('NUTRIEN') 
	DECLARE @RICHARDSON VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('RICHARDSON') 
	DECLARE @UFA VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('UFA') 
	DECLARE @WEST_COOP VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_COOP')
	DECLARE @WEST_RC VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_RC') 

	--GET ACL JOINS
	DECLARE @ACL_JOIN NVARCHAR(MAX) = '';

	EXEC [Grids].[RP_Statements_Grid_Access] @USERNAME,@USERROLE,@REGION, @GRIDNAME, @ACL_JOIN = @ACL_JOIN OUTPUT, @ERROR_STRING = @ERROR_STRING OUTPUT
	

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_RC_REP VARCHAR(100) = '';
	DECLARE @FILTER_HORT_REP VARCHAR(100) = '';
	DECLARE @FILTER_SALES_REP VARCHAR(100) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	SET @FILTER_RC_REP = JSON_VALUE(@FILTERS, '$.rcrep');
	SET @FILTER_HORT_REP  = JSON_VALUE(@FILTERS, '$.hortrep');
	SET @FILTER_SALES_REP = JSON_VALUE(@FILTERS, '$.salesrep');
	
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;
	
	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		--WILL FIND A BETTER WAY TO DO THIS 
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
			,count(*) OVER() AS total_row_count 	 	  		
			FROM (
			Select rp.retailercode + '' '' + rp.retailername + '' '' + isnull(rc.firstname,'''') + '' '' + isnull(rc.lastname,'''') as [searchstring] 
				,ST.retailercode
				,ST.statementcode as scode, st.statementlevel, st.region			
				,IsNull(STI.[FieldValue],''Open'') as [status]			
				,ISNULL(CR.Label, DATENAME(MONTH,ST.LOCKED_DATE)  + '' Payment'') AS paymentmonth				
				,RP.retailername, RP.mailingcity as city, RP.mailingprovince as province, RP.phone
				,IsNull(RC.FirstName,'''') as  contactfirstname, IsNULL(RC.LastName,'''') as contactlastname
				,IsNull(RS.TotalReward,0) AS totalreward
				,IsNull(RS.PaidToDate,0) as paidtodate
				,IsNull(RS.CurrentPayment,0) as currentpayment
				,IsNull(RS.NextPayment,0) AS nextpayment
			From (
					Select *
					From RPWeb_Statements
					Where [Season]='+ @SEASON_STRING + '  AND [Status]=''Active'' AND [VersionType]=''Archive'' AND [StatementType]=''Actual'' 
				)ST	
				LEFT JOIN RP_Config_ChequeRuns CR	ON CR.ID=ST.ChequeRunID
				<ACL_JOIN>
				INNER JOIN RetailerProfile RP	ON RP.RetailerCode=ST.RetailerCode
				LEFT JOIN RetailerContacts RC	ON RC.RetailerCode=RP.RetailerCode AND RC.ISPrimaryContact=''Yes'' AND RC.Status=''Active''
				LEFT JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=ST.StatementCode AND STI.FieldName=''Payment_Status''
				LEFT JOIN (
					Select StatementCode, Sum(TotalReward) AS TotalReward, Sum(PaidToDate) As PaidToDate, Sum(CurrentPayment) AS CurrentPayment, Sum(NextPayment) as NextPayment
					From RPWeb_Rewards_Summary 
					Group By StatementCode
				) RS	
				ON RS.StatementCode=ST.StatementCode
				<FILTERS_JOIN>
				' + ISNULL(@CONDITION_STRING,'') + '
		) BaseResulteSet ' 
		+ ISNULL(@SEARCH_CONDITION,'') + 
		+ ISNULL(@ORDER_BY,'') + '
		OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	

	--EXEC [Grids].[RP_Statements_Base_Query] @SEASON_STRING,@GRIDNAME, @CONDITION_STRING,@SEARCH_CONDITION, @ORDER_BY, @BASEQUERY  = @BASEQUERY  OUTPUT


	IF @ACL_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<ACL_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<ACL_JOIN>',@ACL_JOIN)

	IF @FILTERS_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<FILTERS_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<FILTERS_JOIN>',@FILTERS_JOIN)
	

	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
			BEGIN
				SELECT Top 1 @TotalCount=IsNull([total_row_count],0) FROM #GRID_DATA

			/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS error_msg
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

FINAL_EXIT:
	--we will return error json if access issues etc
	IF ISNULL(@ERROR_STRING,'') = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status, ISNULL(@ERROR_STRING,'xyz') AS error_msg

END
