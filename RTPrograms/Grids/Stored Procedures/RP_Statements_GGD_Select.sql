﻿


CREATE PROCEDURE [Grids].[RP_Statements_GGD_Select] @SEASON INT,@GRIDNAME VARCHAR(100),@REGION VARCHAR(4), @USER NVARCHAR(MAX),@TARGET NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @STATUS VARCHAR(25);
	

	SELECT @STATUS = UPPER(JSON_VALUE(@TARGET,'$.status'));
	

	IF @STATUS NOT IN ('READY_FOR_PROCESSING','READY_FOR_QA','QA_IN_PROGRESS','SUMMARY_STATEMENTS') RETURN;
	
	DECLARE @RETAILER_GROUP VARCHAR(25);
	SELECT @RETAILER_GROUP = UPPER(JSON_VALUE(@TARGET,'$.ret_group'));

	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));
	
	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);


	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';
	

	DECLARE @ACL_JOIN NVARCHAR(max) = '';
	
	DECLARE @CARGILL VARCHAR(20) =   [dbo].[SVF_GetDistributorCode]('CARGILL') 
	DECLARE @NUTRIEN VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('NUTRIEN') 
	DECLARE @RICHARDSON VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('RICHARDSON') 
	DECLARE @UFA VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('UFA') 
	DECLARE @WEST_COOP VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_COOP')
	DECLARE @WEST_RC VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_RC') 

	
	--RETAILER ACCESS DETERMINATIONS
	EXEC [Grids].[RP_Statements_Grid_Access] @USERNAME,@USERROLE,@REGION, @GRIDNAME, @ACL_JOIN = @ACL_JOIN OUTPUT, @ERROR_STRING = @ERROR_STRING OUTPUT


	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_RC_REP VARCHAR(100) = '';
	DECLARE @FILTER_HORT_REP VARCHAR(100) = '';
	DECLARE @FILTER_SALES_REP VARCHAR(100) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	SET @FILTER_RC_REP = JSON_VALUE(@FILTERS, '$.rcrep');
	SET @FILTER_HORT_REP  = JSON_VALUE(@FILTERS, '$.hortrep');
	SET @FILTER_SALES_REP = JSON_VALUE(@FILTERS, '$.salesrep');
	
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;

	--------------------STATUS-----------------------
	IF @STATUS <> 'ALL'
	BEGIN
		IF @STATUS = 'READY_FOR_PROCESSING'
			SET @CONDITION_STRING += ' AND ISNULL(STI.FieldValue,''Open'')=''Open''' 
		ELSE IF @STATUS = 'READY_FOR_QA'
			SET @CONDITION_STRING += 
				CASE WHEN @RETAILER_GROUP = 'SUMMARY_ALL' 
					THEN ' AND STI.FieldValue=''Open'' '
					ELSE ' AND STI.FieldValue=''Ready for QA'' '  END 
		ELSE IF @STATUS =  'QA_IN_PROGRESS'
			SET @CONDITION_STRING += ' AND IsNull(LOCK_ST.StatementCode,0) < 0  AND STI.FieldValue <> ''Submitted For RC Reconciliation'' '
	END --END STATUS
	
	If @STATUS = 'READY_FOR_PROCESSING' And @RETAILER_GROUP = 'EAST_ALL'  
		SET @CONDITION_STRING += ' AND ST.DataSetCode=0'	
		

	-----------------END STATUS---------------------
	--------RETAILER GROUPS----------
	IF @RETAILER_GROUP <> 'all' 
	BEGIN
		SET @CONDITION_STRING += 
		CASE WHEN @RETAILER_GROUP = 'east_all'
				THEN ' AND ST.Region=''EAST'' AND ST.Category=''Regular'''
			 WHEN @RETAILER_GROUP = 'west_independents'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_RC +''' '
			WHEN @RETAILER_GROUP = 'custom_aerial'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Custom Aerial''  '
			WHEN @RETAILER_GROUP = 'west_coops'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_COOP +''' '
			WHEN @RETAILER_GROUP = 'west_lc'
			THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.StatementLevel=''Level 5'' AND ST.RetailerCode IN ('''+ @CARGILL +''', '''+ @NUTRIEN +''','''+ @UFA +''','''+@RICHARDSON+''') '
		END
	END

	IF  @RETAILER_GROUP ='summary_all' 
		SET @CONDITION_STRING += ' AND ST.Summary=''Yes'' '
	ELSE
		SET @CONDITION_STRING += ' AND ST.Summary=''No'' '
	
			
	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
		,count(*) OVER() AS total_row_count 	 	  		
		FROM (
		Select rp.retailercode + '' '' + st.level2code +  + '' '' + st.level5code + '' '' + rp.retailername + '' '' + isnull(rc.firstname,'''') + '' '' + isnull(rc.lastname,'''') as [searchstring] 
			,ST.statementcode as scode, st.statementlevel, st.region, st.datasetcode	
			,IsNull(STI.[FieldValue],''Open'') as [status]			
			,IsNull(QA.[FieldValue],''Unassigned'') as [assigned_to]
			,RP.retailername, RP.mailingcity as city, RP.mailingprovince as province, RP.phone
			,IsNull(RC.FirstName,'''') as  contactfirstname, IsNULL(RC.LastName,'''') as contactlastname
			,IsNull(RS.TotalReward,0) AS totalreward
			,IsNull(RS.PaidToDate,0) as paidtodate
			,Coalesce(RS_LOCK.CurrentPayment, RS.CurrentPayment,0) as currentpayment
			,IsNull(RS.NextPayment,0) AS nextpayment
			,CASE WHEN IsNull(LOCK_ST.StatementCode,0)=0 THEN ''No'' Else ''Yes'' End  AS [is_locked]
			,IsNull(LOCK_ST.StatementCode,0) AS [locked_scode]
			,CASE WHEN st.level5code = ''' + @WEST_RC + ''' THEN ''West Independent''
				  WHEN st.level5code = '''+ @WEST_COOP + ''' THEN ''West Coop''
				  WHEN st.level5code IN ('''+ @CARGILL +''', '''+ @NUTRIEN +''','''+ @UFA +''','''+@RICHARDSON+''') THEN ''West Line''
				  ELSE ''East''
			END AS model
			, LOCK_ST.Locked_Date AS locked_date				   
		From (
				SELECT S.*
					,ISNULL(HO.Summary,''No'') AS Summary
				FROM RPWeb_Statements s
					LEFT JOIN RP_Config_HOPaymentLevel HO
					ON HO.Level5Code=S.RetailerCode AND HO.Season=S.Season
				WHERE S.[Season]=' + @SEASON_STRING + ' AND S.[Status]=''Active'' AND S.[VersionType]=''Unlocked'' AND S.[StatementType]=''Actual''					
			)ST
			<ACL_JOIN>			
			INNER JOIN RetailerProfile RP	ON RP.RetailerCode=ST.RetailerCode
			LEFT JOIN RetailerContacts RC	ON RC.RetailerCode=RP.RetailerCode AND RC.ISPrimaryContact=''Yes'' AND RC.Status=''Active''
			LEFT JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=ST.StatementCode AND STI.FieldName=''Status''
			LEFT JOIN RPWeb_StatementInformation QA	ON QA.StatementCode=ST.StatementCode AND QA.FieldName=''STMT_QA_ASSIGNEE''			
			LEFT JOIN RPWeb_Statements LOCK_ST	ON LOCK_ST.SRC_StatementCode=ST.StatementCode AND LOCK_ST.Status=''Active'' AND LOCK_ST.VersionType=''Locked''						
			LEFT JOIN (
				Select StatementCode, Sum(TotalReward) AS TotalReward, Sum(PaidToDate) As PaidToDate, Sum(CurrentPayment) AS CurrentPayment, Sum(NextPayment) as NextPayment
				From RPWeb_Rewards_Summary
				WHERE Statementcode > 0 
				Group By StatementCode
			) RS	
			ON RS.StatementCode=ST.StatementCode
			LEFT JOIN (
				Select StatementCode, Sum(TotalReward) AS TotalReward, Sum(PaidToDate) As PaidToDate, Sum(CurrentPayment) AS CurrentPayment, Sum(NextPayment) as NextPayment
				From RPWeb_Rewards_Summary
				WHERE Statementcode < 0 
				Group By StatementCode
			) RS_LOCK	
			ON RS_LOCK.StatementCode=LOCK_ST.StatementCode

			<FILTERS_JOIN>
			' + ISNULL(@CONDITION_STRING,'') + '
	) BaseResulteSet ' 
	+ ISNULL(@SEARCH_CONDITION,'') + 
	+ ISNULL(@ORDER_BY,'') + '
	OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	

	
	IF @ACL_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<ACL_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<ACL_JOIN>',@ACL_JOIN)

	IF @FILTERS_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<FILTERS_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<FILTERS_JOIN>',@FILTERS_JOIN)
	

	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

			IF @@ROWCOUNT > 0
			BEGIN
			/* Total Records Count	*/
				SELECT Top 1 @TotalCount=IsNull([total_row_count],0) FROM #GRID_DATA

			/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS error_msg
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

	--we will return error json if access issues etc	
	IF ISNULL(@ERROR_STRING,'') = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status, @ERROR_STRING AS error_msg

END
