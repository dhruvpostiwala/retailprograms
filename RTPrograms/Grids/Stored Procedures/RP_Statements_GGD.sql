﻿

CREATE PROCEDURE [Grids].[RP_Statements_GGD] @SEASON INT,@GRIDNAME VARCHAR(100),@REGION VARCHAR(4), @USER NVARCHAR(MAX), @TARGET NVARCHAR(MAX), @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	/*
	DEMAREY BAKER
	Returns Statement Grid in JSON Format
	*/

	SET NOCOUNT ON;
	
	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.module_access.role'));
	DECLARE @PERMISSIONS VARCHAR(1050) = JSON_QUERY(@USER, '$.module_access.permissions')
	DECLARE @IS_ADMIN BIT =  ISNULL(CAST(JSON_VALUE(@PERMISSIONS , '$.admin') AS BIT),0)
	DECLARE @CAN_APPROVE BIT =  ISNULL(CAST(JSON_VALUE(@PERMISSIONS , '$.approve') AS BIT),0)
	
	DECLARE @IS_KENNA_ADMIN BIT=0


	DECLARE @STATUS VARCHAR(25);
	DECLARE @RETAILER_GROUP VARCHAR(25);
	

	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @SEASON_STRING VARCHAR(4)= CONVERT(VARCHAR(4),@SEASON);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';

	DECLARE @QA_STATUS VARCHAR(25);

	
	SELECT @STATUS = UPPER(JSON_VALUE(@TARGET,'$.status'));
	SELECT @RETAILER_GROUP = UPPER(JSON_VALUE(@TARGET,'$.ret_group'));
	
	DECLARE @ACL_JOIN NVARCHAR(max) = '';
	
	--Retailer Access Determinations

	DECLARE @CARGILL VARCHAR(20) =   [dbo].[SVF_GetDistributorCode]('CARGILL') 
	DECLARE @NUTRIEN VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('NUTRIEN') 
	DECLARE @RICHARDSON VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('RICHARDSON') 
	DECLARE @UFA VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('UFA') 
	DECLARE @WEST_COOP VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_COOP')
	DECLARE @WEST_RC VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_RC') 

		
	--DETERMINE IF USER CAN ACCESS THIS GRID WHILE BUIlDING THE QUERY
	EXEC [Grids].[RP_Statements_Grid_Access] @USERNAME,@USERROLE,@REGION, @GRIDNAME, @ACL_JOIN = @ACL_JOIN OUTPUT, @ERROR_STRING = @ERROR_STRING OUTPUT

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS',@SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';
	DECLARE @FILTER_STRING NVARCHAR(MAX) = '';
	DECLARE @FILTERS_JOIN NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
	
	
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FILTERS--
	EXEC [Grids].[RP_Statements_GGD_Filters] @FILTERS, @FILTER_STRING = @FILTER_STRING OUTPUT, @FILTERS_JOIN =  @FILTERS_JOIN OUTPUT
	--UPDATE CONDITION STRING WITH FILTER STRING
	SET @CONDITION_STRING += @FILTER_STRING;
	
	--------------------STATUS----------------------- 
	/*PLEASE NOTE THIS WAS TAKEN FROM HOW IT WAS IN NOTES BUT HAVEN'T BEEN TESTED IN SQL YET WITH NEW ACL SETUP YET
	I ALSO THINK WE CAN USE THE SQL ACL TO DO THIS.
	I DIDNT GET TO DO THIS YET -- DEMAREY
	*/
	IF @STATUS = 'pending_approval'
		BEGIN
			IF @USERROLE='KENNA' AND @IS_ADMIN=1 AND @CAN_APPROVE=1 --@APPROVER_TYPE = 'Admin(K) Approver'
				SET @CONDITION_STRING += ' AND STI.FieldValue IN (''Ready for QA'',''Kenna Approval'',''Admin(K) Review Pending'')' 
			ELSE IF @USERROLE='KENNA' AND @CAN_APPROVE=1  -- @APPROVER_TYPE ='Kenna Statement Approver' 
				SET @CONDITION_STRING += ' AND (ST.Region=''WEST'' OR ST.StatementLevel=''LEVEL 1'') AND STI.FieldValue IN (''Ready for QA'',''Kenna Approval'')'
			ELSE IF  @USERROLE = 'RC-TL' -- @APPROVER_TYPE IN ('RC-TL Approver')			
				SET @CONDITION_STRING += ' AND ST.Region=''WEST'' AND STI.FieldValue=''RC Team Lead Approval'' '
			ELSE IF @USERROLE = 'RC' -- @APPROVER_TYPE IN ('RC Approver')	
				SET @CONDITION_STRING += ' AND ST.Region=''WEST'' AND STI.FieldValue=''RC Rep Approval'' '
			ELSE IF @USERROLE IN ('BR', 'HORT') -- @APPROVER_TYPE IN ('BR Approver','HORT Approver')		
				SET @CONDITION_STRING += ' AND (ST.Region=''WEST'' OR ST.StatementLevel=''LEVEL 1'') AND STI.FieldValue=''RR Approval'' '
			ELSE --IF NOT Admin Approver
				BEGIN
					IF @USERROLE='HO' AND @IS_ADMIN=1 AND @CAN_APPROVE=1  --@APPROVER_TYPE = 'BASF Statement Approver' 
						SET @QA_STATUS ='QA Approval'				

					ELSE IF @USERROLE='HO' 
							AND JSON_VALUE(@PERMISSIONS, '$.fc') = 'true'
							AND JSON_VALUE(@PERMISSIONS, '$.approve') = 'true'     --@APPROVER_TYPE = 'Finance Approver' 
						SET @QA_STATUS ='Finance Approval'

					ELSE IF @USERROLE = 'SAM'  -- @APPROVER_TYPE = 'SAM Approver' 
						SET @QA_STATUS  ='SAM Approval'
					ELSE
						SET	@ERROR_STRING = 'You are not authorized to access my work view.' 

			
					SET @CONDITION_STRING += ' AND (ST.Region=''WEST'' OR ST.DataSetCode=0) AND STI.FieldValue=''' + @QA_STATUS + ''''
			
				END
		
		END --end pending_approval
	
	ELSE IF	@STATUS ='my_work'
		BEGIN
			IF  @USERROLE='KENNA' AND @IS_ADMIN=1 AND @CAN_APPROVE=1			
				SET @CONDITION_STRING += ' AND STI.FieldValue IN (''Admin(K) Review Pending'') '			
			ELSE IF @USERROLE='KENNA' AND @CAN_APPROVE=1   -- @APPROVER_TYPE ='Kenna Statement Approver' 
				SET @CONDITION_STRING += ' AND ISNULL(QA.FieldValue,'''')='''+ @USERNAME + ''' AND (ST.Region=''WEST'' OR ST.StatementLevel=''LEVEL 1'') AND STI.FieldValue IN (''Ready for QA'',''Kenna Approval'')'
			ELSE 
				SET	@ERROR_STRING = 'You are not authorized to access my work view.'
		
		END -- end my work
	ELSE IF @STATUS ='qa_in_progress'
		BEGIN
			SET @CONDITION_STRING += ' AND IsNUll(LOCK_ST.StatementCode,0) < 0 '
		END

	
	/****SUMMARY****/	
	IF  @STATUS ='summary_only' 
		SET @CONDITION_STRING += ' AND ST.Summary=''Yes'' '
	ELSE
		SET @CONDITION_STRING += ' AND ST.Summary=''No'' '
			
	-----------------END STATUS---------------------
	
	IF @RETAILER_GROUP <> 'all' 
	BEGIN
		SET @CONDITION_STRING += 
		CASE WHEN @RETAILER_GROUP = 'east_all'
				THEN ' AND ST.Region=''EAST'' AND ST.Category=''Regular'' AND ((ST.StatementLevel = ''Level 1'' AND ST.Level2Code = '''') OR (ST.StatementLevel = ''Level 2'' AND ST.Level2Code <> ''''))'
			 WHEN @RETAILER_GROUP = 'west_independents'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_RC +''' '
			WHEN @RETAILER_GROUP = 'custom_aerial'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Custom Aerial''  '
			WHEN @RETAILER_GROUP = 'west_coops'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.Level5Code='''+@WEST_COOP +''' '
			WHEN @RETAILER_GROUP = 'west_lc'
				THEN ' AND ST.Region=''WEST'' AND ST.Category=''Regular'' AND ST.StatementLevel=''Level 5'' AND ST.RetailerCode IN ('''+ @CARGILL +''', '''+ @NUTRIEN +''','''+ @UFA +''','''+@RICHARDSON+''') '			
		END
	END
	
			
	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
		,count(*) OVER() AS total_row_count 	 	  		
	FROM (
			Select ''~'' + st.retailercode + '' ''  + st.retailercode + '' '' + st.level2code + '' '' + st.level5code + '' '' + rp.retailername + '' '' + isnull(rc.firstname,'''') + '' '' + isnull(rc.lastname,'''') as [searchstring] 
			,ST.statementcode as scode, st.statementlevel, st.region, st.datasetcode
			,IsNull(STI.[FieldValue],''Open'') as [status]	
			,IsNull(QA.[FieldValue],''Unassigned'') as [assigned_to]		
			,RP.retailername, RP.mailingcity as city, RP.mailingprovince as province, RP.phone
			,IsNull(RC.FirstName,'''') as  contactfirstname, IsNULL(RC.LastName,'''') as contactlastname
			,IsNull(RS.TotalReward,0) AS totalreward
			,IsNull(RS.PaidToDate,0) as paidtodate
			,COALESCE(RS_LOCK.CurrentPayment,RS.CurrentPayment,0) as currentpayment
			,IsNull(RS.NextPayment,0) AS nextpayment
			,CASE WHEN IsNull(LOCK_ST.StatementCode,0)=0 THEN ''No'' Else ''Yes'' End  AS [is_locked]
			,IsNull(LOCK_ST.StatementCode,0) AS [locked_scode]
			,CASE WHEN st.level5code = ''D000001'' THEN ''West Independent''
				  WHEN st.level5code = ''D0000117'' THEN ''West Coop''
				  WHEN st.level5code in (''D0000107'',''D0000137'', ''D0000244'', ''D520062427'') THEN ''West Line''
				  ELSE ''East''
			END AS model
			, LOCK_ST.Locked_Date AS locked_date		   
		From (
				Select s.*
					,ISNULL(HO.Summary,''No'') AS Summary
				From RPWeb_Statements s
					LEFT JOIN RP_Config_HOPaymentLevel HO
					ON HO.Level5Code=S.RetailerCode AND HO.Season=S.Season
				Where s.[Season]='+ @SEASON_STRING + ' AND s.[Status]=''Active'' AND s.[VersionType]=''Unlocked'' AND s.[StatementType]=''Actual'' 
			)ST
			<ACL_JOIN>
			INNER JOIN RetailerProfile RP	ON RP.RetailerCode=ST.RetailerCode
			LEFT JOIN  RetailerContacts RC	ON RC.RetailerCode=RP.RetailerCode AND RC.ISPrimaryContact=''Yes'' AND RC.Status=''Active''
			LEFT JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=ST.StatementCode AND STI.FieldName=''Status''			
			LEFT JOIN RPWeb_Statements LOCK_ST	ON LOCK_ST.SRC_StatementCode=ST.StatementCode AND LOCK_ST.Status=''Active'' AND LOCK_ST.VersionType=''Locked''
			LEFT JOIN RPWeb_StatementInformation QA	ON QA.StatementCode=ST.StatementCode AND QA.FieldName=''STMT_QA_ASSIGNEE''						
			LEFT JOIN (
				Select StatementCode, Sum(TotalReward) AS TotalReward, Sum(PaidToDate) As PaidToDate, Sum(CurrentPayment) AS CurrentPayment, Sum(NextPayment) as NextPayment
				From RPWeb_Rewards_Summary
				WHERE Statementcode > 0 
				Group By StatementCode
			) RS	
			ON RS.StatementCode=ST.StatementCode
			LEFT JOIN (
				Select StatementCode, Sum(TotalReward) AS TotalReward, Sum(PaidToDate) As PaidToDate, Sum(CurrentPayment) AS CurrentPayment, Sum(NextPayment) as NextPayment
				From RPWeb_Rewards_Summary
				WHERE Statementcode < 0 
				Group By StatementCode
			) RS_LOCK	
			ON RS_LOCK.StatementCode=LOCK_ST.StatementCode
			<FILTERS_JOIN>
			' + ISNULL(@CONDITION_STRING,'') + '
	) BaseResulteSet ' 
	+ ISNULL(@SEARCH_CONDITION,'') + 
	+ ISNULL(@ORDER_BY,'') + '
	OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	


	IF @ACL_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<ACL_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<ACL_JOIN>',@ACL_JOIN)

	IF @FILTERS_JOIN='' 
		SET @BASEQUERY = REPLACE(@BASEQUERY,'<FILTERS_JOIN>','')
	ELSE
		SET @BASEQUERY=REPLACE(@BASEQUERY,'<FILTERS_JOIN>',@FILTERS_JOIN)
	

	
	DECLARE @Query NVARCHAR(MAX) = ''
		
	SET @Query = '
			DROP TABLE IF EXISTS #GRID_DATA

			DECLARE @TotalCount INT = 0;
			DECLARE @JSON_DATA NVARCHAR(MAX);
		
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
			BEGIN
				SELECT Top 1 @TotalCount=IsNull([total_row_count],0) FROM #GRID_DATA

			/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]	
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS error_msg
			RETURN;	
		END CATCH

		Select ''success'' as status,ISNULL(@TotalCount,0) as totalcount, @JSON_DATA as json_data
		'

		PRINT @Query

	--we will return error json if access issues etc
	IF ISNULL(@ERROR_STRING,'') = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status,@ERROR_STRING AS error_msg

END