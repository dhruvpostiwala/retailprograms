﻿
CREATE PROCEDURE [Grids].[RP_Statements_Grid_Access] 
	@USERNAME VARCHAR(100),
	@USERROLE VARCHAR(50),
	@REGION VARCHAR(4),
	@GRIDNAME VARCHAR(100), 
	@ACL_JOIN NVARCHAR(MAX) OUTPUT, 
	@ERROR_STRING NVARCHAR(MAX) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;


	--Retailer Access Determinations
	DECLARE @SHOULDBEPRIMARY BIT = 1

	--LINE COMPANIES		
	DECLARE @CARGILL VARCHAR(20) =   [dbo].[SVF_GetDistributorCode]('CARGILL') 
	DECLARE @NUTRIEN VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('NUTRIEN') 
	DECLARE @RICHARDSON VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('RICHARDSON') 
	DECLARE @UFA VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('UFA') 
	DECLARE @WEST_COOP VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_COOP')
	DECLARE @WEST_RC VARCHAR(20) = [dbo].[SVF_GetDistributorCode]('WEST_RC') 
	
	IF @GRIDNAME = 'STATEMENTS'
	BEGIN
		IF @USERROLE IN ('KENNA','AGS','HO','NAM')
			BEGIN	
				IF @USERROLE = 'NAM' AND @USERNAME = 'JAMES MOWBRAY'
				BEGIN
					SET @ACL_JOIN = '
					INNER JOIN (
						SELECT RetailerCode
						FROM RetailerAccess
						WHERE Employeename='''+ @USERNAME +''' AND EmployeeRole='''+ @USERROLE +''' <IS_PRIMARY_CNDITIION> 			
					) RA
					ON RA.RetailerCode=ST.RetailerCode AND ST.Level5Code IN ('''+@WEST_COOP+''','''+ @CARGILL +''','''+@RICHARDSON+''','''+@UFA+''','''+@NUTRIEN+''')' 				
				END
		
			END -- END KENNA CALL CENTER HEADOFFICE NAM
		ELSE IF @USERROLE IN('BR','HORT','RM','RC','RC-TL','RCHO','SAM','HORTDM')
		BEGIN

				SET @ACL_JOIN = '
							INNER JOIN (
								SELECT RetailerCode
								FROM RetailerAccess
								WHERE Employeename='''+ @USERNAME +''' AND REPLACE(EmployeeRole,''RCHO'',''RC-TL'')='''+ @USERROLE + ''' <IS_PRIMARY_CNDITIION> 			
							) RA
							ON RA.RetailerCode=ST.RetailerCode'
			
			IF @USERNAME='COURTNEY STEPHENSON' SET @SHOULDBEPRIMARY = 0;
			
			IF @USERROLE IN('RC','RC-TL','RCHO') 
				SET @ACL_JOIN += ' AND ST.Level5Code=''' + @WEST_RC + ''''
			ELSE IF @USERROLE IN ('BR','HORT','RM', 'HORTDM')	
				SET @ACL_JOIN += ' AND ST.Level5Code <> ''' + @WEST_RC + ''' AND ST.StatementLevel <> ''LEVEL 5'' '
			ELSE IF @USERROLE = 'SAM'
			BEGIN
				IF @REGION = 'WEST'
				BEGIN
						IF @USERNAME ='NATHAN HNATIUK' SET @ACL_JOIN += ' AND (ST.StatementLevel=''LEVEL 5'' OR ST.Level5Code=''D0000117'')'					
						ELSE SET @ACL_JOIN += ' AND ST.StatementLevel = ''LEVEL 5'' '
				END --END WEST
				ELSE IF @REGION = 'EAST'
				BEGIN
					IF @USERNAME ='BRUCE IRONS SAM'
					BEGIN 
						SET @SHOULDBEPRIMARY = 0;
						SET @ACL_JOIN += ' AND ST.Region = ''East'' '
					END -- END BRUCE IRONS
					ELSE SET @ACL_JOIN += ' AND ST.StatementLevel IN (''LEVEL 1'',''LEVEL 2'',''LEVEL 5'') '						
				END --END EAST
			END --END USEROLE SAM
		END	--END ('BR','HORT','RM','RC','RC-TL','RCHO','SAM','HORTDM')
		ELSE		
			SET @ERROR_STRING = 'Authentication Error (' + @GRIDNAME + ') - You are not authorized to access this data. ' + @USERNAME + ':' +  @USERROLE	
		
		
	
		--CHECK IS PRIMARY	
		IF @SHOULDBEPRIMARY = 1 
			SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , ' AND IsPrimary=''Yes'' ')
		ELSE
			SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , '')
	
	END
	ELSE IF @GRIDNAME = 'STATEMENTS_ARCHIVED'
	BEGIN
		
		IF @USERROLE IN ('KENNA','AGS','HO','NAM')
		BEGIN	
				IF @USERROLE = 'NAM' AND @USERNAME = 'JAMES MOWBRAY'
				BEGIN
					SET @ACL_JOIN = '
					INNER JOIN (
						SELECT RetailerCode
						FROM RetailerAccess
						WHERE Employeename='''+ @USERNAME +''' AND EmployeeRole='''+ @USERROLE + ''' <IS_PRIMARY_CNDITIION> 			
					) RA
					ON RA.RetailerCode=ST.RetailerCode AND ST.Level5Code IN ('''+@WEST_COOP+''','''+ @CARGILL +''','''+@RICHARDSON+''','''+@UFA+''','''+@NUTRIEN+''')' 				
				END
		
		END -- END KENNA CALL CENTER HEADOFFICE NAM
		ELSE IF @USERROLE IN('BR','HORT','RM','RC','RC-TL','RCHO','SAM')
		BEGIN

					SET @ACL_JOIN = '
								INNER JOIN (
									SELECT RetailerCode
									FROM RetailerAccess
									WHERE Employeename='''+ @USERNAME +''' AND REPLACE(EmployeeRole,''RCHO'',''RC-TL'')='''+ @USERROLE + ''' <IS_PRIMARY_CNDITIION> 			
								) RA
								ON RA.RetailerCode=ST.RetailerCode'
			
				IF @USERNAME='COURTNEY STEPHENSON' SET @SHOULDBEPRIMARY = 0;
			
				IF @USERROLE IN('RC','RC-TL','RCHO') 
					SET @ACL_JOIN += ' AND ST.Level5Code=' + @WEST_RC
				ELSE IF @USERROLE IN ('BR','HORT','RM')	
					SET @ACL_JOIN += ' AND ST.Level5Code <> ' + @WEST_RC + ' AND ST.StatementLevel <> ''LEVEL 5'' '
				ELSE IF @USERROLE = 'SAM'
						SET @ACL_JOIN += ' AND ST.StatementLevel <> ''LEVEL 5'' '
			END			
			ELSE
				SET @ERROR_STRING = 'Authentication Error (' + @GRIDNAME + ') - You are not authorized to access this data. ' + @USERNAME + ':' +  @USERROLE	
		
	
			--CHECK IS PRIMARY	
			IF @SHOULDBEPRIMARY = 1 
				SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , ' AND IsPrimary=''Yes'' ')
			ELSE
				SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , '')
			END
	ELSE IF  @GRIDNAME IN ('STATEMENTS_SELECT','EXCEPTIONS_ALL')
	BEGIN
		IF @USERROLE IN ('KENNA','AGS','HO') --THEY SEE EVERYTHING
			SET @ERROR_STRING = ''
		ELSE IF @USERROLE IN ('BR','HORT','RM','RC','RC-TL','RCHO')
			BEGIN
					SET @ACL_JOIN = '
								INNER JOIN (
									SELECT RetailerCode
									FROM RetailerAccess
									WHERE Employeename='''+ @USERNAME +''' AND REPLACE(EmployeeRole,''RCHO'',''RC-TL'')='''+ @USERROLE + ''' <IS_PRIMARY_CNDITIION> 			
								) RA
								ON RA.RetailerCode=ST.RetailerCode'
			
					IF @USERNAME='COURTNEY STEPHENSON' SET @SHOULDBEPRIMARY = 0;
			
					IF @USERROLE IN('RC','RC-TL','RCHO') 
						SET @ACL_JOIN += ' AND ST.Level5Code=''' + @WEST_RC + ''''
					ELSE 
						SET @ACL_JOIN += ' AND ST.StatementLevel <> ''' + @WEST_RC + ''''
			END			
		ELSE
			SET @ERROR_STRING = 'Authentication Error (' + @GRIDNAME + ') - You are not authorized to access this data. ' + @USERNAME + ':' +  @USERROLE	
			
		
		--CHECK IS PRIMARY	
		IF @SHOULDBEPRIMARY = 1 
			SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , ' AND IsPrimary=''Yes'' ')
		ELSE
			SET @ACL_JOIN = Replace(@ACL_JOIN , '<IS_PRIMARY_CNDITIION>' , '')
				
	
	END --END STATEMENTS_SELECT 
	ELSE
		SET @ERROR_STRING = 'Authentication Error #2 - You are not authorized to access this data. ' + @USERNAME + ':' +  @USERROLE	

END