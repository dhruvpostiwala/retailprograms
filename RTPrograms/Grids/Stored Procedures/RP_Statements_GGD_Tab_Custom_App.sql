﻿




CREATE PROCEDURE [Grids].[RP_Statements_GGD_Tab_Custom_App] @STATEMENTCODE INT, @FILTERS NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ERROR_STRING NVARCHAR(MAX) = '';


	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	DECLARE @STATEMENTCODE_STRING VARCHAR(10)= CONVERT(VARCHAR(10),@STATEMENTCODE);

	DECLARE @CONDITION_STRING NVARCHAR(MAX) = '';
	

	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'STATEMENTS', @SORT_STRING = @ORDER_BY OUTPUT
	
	---------------FILTERS--------------
	DECLARE @SEARCH_CONDITION NVARCHAR(MAX) = '';

	SET @SEARCH_CONDITION  = JSON_VALUE(@FILTERS,'$.searchtxt');
		
	IF @SEARCH_CONDITION <> '' 
	SET @SEARCH_CONDITION = ' WHERE [searchstring] LIKE ' + QUOTENAME('%' + @SEARCH_CONDITION + '%','''') ;

	--FINAL CONDITION BUILDING
	IF @CONDITION_STRING <> '' 
		--REMOVE FIRST "AND" FROM THE CONDITION STRING 				
		--WILL FIND A BETTER WAY TO DO THIS 
	SET @CONDITION_STRING = ' WHERE ' + SUBSTRING(@CONDITION_STRING,5,LEN(@CONDITION_STRING))
	
	SET @BASEQUERY = 'SELECT *
		,count(*) OVER() AS total_row_count			 					
		FROM (
		SELECT
			 isnull(rp.retailername,'''') + '' '' + isnull(pr.productname,'''') as [searchstring]
			,CA.Season as basfseason
			, CA.DocCode
			, isnull(Pr.ProductName,'''') as product
			, rp.retailername +''('' + CA.retailercode + '')'' as retailername
			, CA.quantity
			, CA.packagesize
		FROM  RPWeb_CustomAppProducts CA 
			LEFT JOIN RetailerProfile RP on CA.retailercode = RP.retailercode
			LEFT JOIN ProductReference PR on pr.productcode = ca.productcode
			WHERE CA.[StatementCode]=' + @STATEMENTCODE_STRING + ' OR CA.DataSetCode=' + @STATEMENTCODE_STRING + ' 
		) BaseResulteSet ' 
		+ ISNULL(@SEARCH_CONDITION,'') + 
		+ ISNULL(@ORDER_BY,'') + '
		OFFSET 0 ROWS FETCH NEXT 9000 ROWS ONLY '	
	
	
	DECLARE @QUERY NVARCHAR(MAX) = ''
	
	SET @QUERY = '
			SET NOCOUNT ON;
			IF OBJECT_ID(N''tempdb..#GRID_DATA'') IS NOT NULL DROP TABLE #GRID_DATA
			
			DECLARE @TotalCount AS INT;
			DECLARE @JSON_DATA NVARCHAR(MAX);
					
			BEGIN TRY
			SELECT * 
			Into #GRID_DATA
			From (' + @BASEQUERY + ') t 

		
			/* Total Records Count	*/
			IF @@ROWCOUNT > 0
			BEGIN
				-- Total Records Count	
				Select Top 1 @TotalCount=IsNull(total_row_count,0) FROM #GRID_DATA	
					

			/* Drop unwanted columns , no need to send this data back to browser*/
				ALTER TABLE #GRID_DATA DROP COLUMN [total_row_count], [searchstring]
	
			END
			SELECT @JSON_DATA=ISNULL((Select * FROM #GRID_DATA FOR JSON PATH) ,''[]'')

		END TRY
		BEGIN CATCH
			SELECT ''Error'' AS status, ERROR_MESSAGE() + '' at Line '' + CAST(ERROR_LINE() AS VARCHAR)  AS errormessage
			RETURN;	
		END CATCH
		
		Select ''success'' as status,@TotalCount As totalcount, @JSON_DATA as data

		'

	--we will return error json if access issues etc
	IF @ERROR_STRING = ''
		EXECUTE sp_executesql @Query
	ELSE
		SELECT 'Error' AS status,@ERROR_STRING AS errormessage

END
