﻿



CREATE PROCEDURE [Grids].[RP_Statements_GGD_Exceptions] @STATEMENTCODE INT
AS
BEGIN

	SET NOCOUNT ON;

		DECLARE @TotalCount INT;
		DECLARE @JSON_Data NVARCHAR(MAX);
				
		IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
			DROP TABLE #Temp
				
		SELECT 	ID [id],
			StatementCode [StatementCode],
			versiontype,
			Exception_Desc [exception_desc],
			Exception_Type [exception_type],
			Exception_Value [exception_value],
			Status [exception_status],
			convert(varchar, DateCreated, 107) as [created_date],
			Comments [comments]
		INTO #Temp
		FROM RPWeb_Exceptions
		WHERE StatementCode=@STATEMENTCODE 
						
		SELECT @TotalCount = COUNT(*)
		FROM #Temp
				
		SELECT @JSON_Data = ISNULL((
			SELECT *
			FROM #Temp
			ORDER BY ID DESC
			FOR JSON PATH
		), '[]');
				
		SELECT @JSON_Data [json_data], @TotalCount [totalcount]

END
