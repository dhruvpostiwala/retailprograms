﻿

CREATE PROCEDURE [Grids].[RP_Statements_GGD_Exposure_Report] @SEASON INT, @USER NVARCHAR(MAX),@SORTS NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @USERNAME VARCHAR(100) = UPPER(JSON_VALUE(@USER, '$.username'));
	DECLARE @USERROLE VARCHAR(50) = UPPER(JSON_VALUE(@USER, '$.userrole'));

	--CHECK ACCESS
	IF @USERROLE NOT IN ('KENNA','CALLCENTER','HEADOFFICE')
	RETURN;
	
	DECLARE @BASEQUERY NVARCHAR(MAX) = '';
	
	------------------ORDER BY ------------------
	DECLARE @ORDER_BY NVARCHAR(MAX) = '';
	--UPDATE CONDITION STRING WITH FILTER STRING
	EXEC [Grids].[RP_Statements_GGD_Sorts] @SORTS,'EXPOSURE', @SORT_STRING = @ORDER_BY OUTPUT
		
	SET @BASEQUERY ='
	DECLARE @TotalCount AS INT;
	DECLARE @JSON_DATA NVARCHAR(MAX);
		
	-- Get total records count
	SELECT @TotalCount=IsNull(count(*), 0) FROM RPWeb_Reports_Exposure WHERE SEASON=@SEASON;
		
	-- Get required columns
	SELECT @JSON_DATA=ISNULL((
		SELECT T1.ExposureID
			,FORMAT(t1.ReportDate, ''MMM d yyyy h:mmtt'') AS ReportDate
			,ROW_NUMBER() OVER(ORDER BY t1.ReportDate) AS ROW
			,SUM(ex.AccrualAmount) AS AccrualAmount
			,SUM((ex.AccrualAmount - ex.RewardAmount)) AS AccrualRemaining
			,SUM(ex.RewardAmount) AS RewardAmount
			,SUM(ex.PaidToDate) AS PaidToDate
			,SUM((ex.RewardAmount - ex.PaidToDate)) AS Outstanding 
		FROM RPWeb_Reports_Exposure T1
			INNER JOIN RPWeb_Reports_Exposure_AccrualSummary EX
			ON EX.ExposureID=T1.ExposureID
		WHERE T1.Season=@SEASON
		GROUP BY T1.ExposureID, T1.ReportDate 
		'+ ISNULL(@ORDER_BY,'''') + ' 
		FOR JSON PATH
	), ''[]'');
		
	-- Return the data
	SELECT @TotalCount AS totalcount, @JSON_DATA AS json_data;'
	
	EXECUTE sp_executesql @BASEQUERY, N'@SEASON INT', @SEASON

END

