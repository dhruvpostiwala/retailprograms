﻿



CREATE PROCEDURE [Grids].[RP_Statements_Get_OTP] @SEASON INT
AS
BEGIN

		SET NOCOUNT ON;
				
		DECLARE @TotalCount INT;
		DECLARE @JSON_Data NVARCHAR(MAX);
				
		IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
			DROP TABLE #Temp
				
		SELECT DISTINCT OTP.ID [id],
						RP.Region [model],
						ISNULL(RP.RetailerName,'NO MATCH FOUND') [retailer],
						OTP.RetailerCode [retailer_code],
						OTP.PaymentDesc [payment_desc],
						OTP.Amount [payment_amount],
						OTP.StatementCode [payment_code],
						OTP.[Status] [payment_status],
						OTP.PaymentDate [payment_date],
						OTP.Comments [comments],
						IIF(ISNULL(RP.RetailerCode,'')='','No','Yes') AS [retailer_exists]
		INTO #Temp
		FROM RP_Seeding_OneTimePayments OTP
			LEFT JOIN RetailerProfile RP
			ON OTP.RetailerCode = RP.RetailerCode
		WHERE OTP.Season = @SEASON
				
		SELECT @TotalCount = COUNT(*)
		FROM #Temp
				
		SELECT @JSON_Data = ISNULL((
			SELECT *
			FROM #Temp
			FOR JSON PATH
		), '[]');
				
		SELECT @JSON_Data [json_data], @TotalCount [totalcount]
END
