﻿


CREATE PROCEDURE [Grids].[RP_Statements_GGD_Filters] @FILTERS NVARCHAR(MAX), @FILTER_STRING NVARCHAR(MAX) OUTPUT, @FILTERS_JOIN NVARCHAR(MAX) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;
	
	--{"count":0,"totalreward":"L|~50000|~","level2":"R0002445","rcrep":"Brittany+Yuzik"}


	DECLARE @STATUS VARCHAR(25) = JSON_VALUE(@FILTERS, '$.status');
	DECLARE @REGION VARCHAR(4) = JSON_VALUE(@FILTERS, '$.region');
	DECLARE @TOTAL_REWARD VARCHAR(10) = JSON_VALUE(@FILTERS, '$.totalreward');
	DECLARE @LEVEL5 VARCHAR(15) = JSON_VALUE(@FILTERS, '$.level5');
	DECLARE @LEVEL2 VARCHAR(15) = JSON_VALUE(@FILTERS, '$.level2');
	DECLARE @SALES_REP VARCHAR(100) = JSON_VALUE(@FILTERS, '$.salesrep');
	DECLARE @HORT_REP VARCHAR(100) = JSON_VALUE(@FILTERS, '$.hortrep');
	DECLARE @RC_REP VARCHAR(100) = JSON_VALUE(@FILTERS, '$.rcrep');
	DECLARE @DATE DATETIME = JSON_VALUE(@FILTERS, '$.date');

	
	---------------FILTERS--------------
		
	IF  @REGION <> ''			 										
		SELECT @FILTER_STRING += [dbo].[SVF_AddFilters] ('SINGLE', 'st.[Region]','VARCHAR',@REGION)

	IF  @STATUS <> ''			 										
			IF UPPER(@STATUS) = 'PAID'	
				SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('NUMBER', 'IsNull(RS.PaidToDate,0)', 'FLOAT', 'NE|~0|~')
			ELSE														
				SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('SINGLE', 'IsNull(STI.[FieldValue],''Open'')', 'VARCHAR', @STATUS)
	IF  @TOTAL_REWARD <> ''						
			SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('NUMBER', 'IsNull(RS.TotalReward,0)', 'FLOAT', @TOTAL_REWARD)
	IF  @LEVEL5 <> ''						
			SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('SINGLE', 'RP.Level5Code', 'VARCHAR', @LEVEL5)
	IF  @LEVEL2 <> ''						
			SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('SINGLE', 'RP.Level2Code', 'VARCHAR', @LEVEL2)

	IF  @SALES_REP <> ''						
			SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('SINGLE', 'CA.EmployeeName', 'VARCHAR', @SALES_REP)

	IF  @RC_REP <> ''						
			SELECT @FILTER_STRING += [dbo].[SVF_AddFilters]('SINGLE', 'CA.EmployeeName', 'VARCHAR', @RC_REP)


	---JOINS---
	IF @SALES_REP <> ''
		SET @FILTERS_JOIN = 'INNER JOIN RetailerAccess RA
              				ON RA.RetailerCode=RP.RetailerCode AND RA.isPrimary = ''Yes''
              				INNER JOIN Classificationaccess CA                     
              				ON CA.ClassificationCode=RA.ClassificationCode'
	IF @HORT_REP <> ''
		SET @FILTERS_JOIN = 'INNER JOIN RetailerAccess RA
              				ON RA.RetailerCode=RP.RetailerCode AND RA.isPrimary = ''Yes''
              				INNER JOIN Classificationaccess CA                     
              				ON CA.ClassificationCode=RA.ClassificationCode'
	IF @RC_REP <> ''	
		SET @FILTERS_JOIN = 'INNER JOIN RetailerAccess RA
              				ON RA.RetailerCode=RP.RetailerCode AND RA.isPrimary = ''Yes''
              				INNER JOIN Classificationaccess CA                     
              				ON CA.ClassificationCode=RA.ClassificationCode'


END

