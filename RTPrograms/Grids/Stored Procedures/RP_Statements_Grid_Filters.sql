﻿




CREATE PROCEDURE [Grids].[RP_Statements_Grid_Filters] @GRIDNAME VARCHAR(50)
AS
BEGIN
		/*
		DEMAREY BAKER
		Returns the values needed for the statement filters. 
		Accepts gridname and returns varius filters in json format. 
		*/
		SET NOCOUNT ON;
		
		DECLARE @GRID_JOIN NVARCHAR(MAX) = '';
		DECLARE @QUERY NVARCHAR(MAX) = '';


		IF UPPER(@GRIDNAME) = 'STATEMENTS_ARCHIVED'
			SET @GRID_JOIN = 'INNER JOIN RPWeb_Statements ON RPWeb_Statements.RetailerCode=RetailerProfile.RetailerCode AND RPWeb_Statements.VersionType=''Archive'' '
	
		SELECT @QUERY = '
		DECLARE @Level5 AS NVARCHAR(MAX);
		DECLARE @Level2 AS NVARCHAR(MAX);
		DECLARE @SalesRep AS NVARCHAR(MAX);
		DECLARE @HortRep AS NVARCHAR(MAX);
		DECLARE @RCRep AS NVARCHAR(MAX);
			
		SELECT @Level5=IsNull(
							(SELECT DISTINCT RetailerName, RetailerProfile.Level5Code 
								FROM RetailerProfile
								'+ @GRID_JOIN +'
								WHERE RetailerProfile.Status=''Active'' AND RetailerProfile.RetailerType=''LEVEL 5'' 
								ORDER BY RetailerName 
								FOR JSON PATH), '''');
			
		SELECT @Level2=IsNull(
							(SELECT DISTINCT RetailerName, RetailerProfile.Level2Code 
								FROM RetailerProfile
								'+ @GRID_JOIN +'
								WHERE RetailerProfile.Status=''Active'' AND RetailerProfile.RetailerType=''LEVEL 2'' 
								ORDER BY RetailerName 
								FOR JSON PATH), '''');

		SELECT @SalesRep=IsNull(
							(SELECT OwnerRepName 
								FROM Classifications
								WHERE ClassificationType = ''Territory'' AND OwnerRepName <> ''Vacant'' 
								ORDER BY OwnerRepName 
								FOR JSON PATH), '''');

		SELECT @HortRep=IsNull(
							(SELECT OwnerRepName 
								FROM Classifications
								WHERE ClassificationType = ''Hort Territory'' AND OwnerRepName <> ''Vacant'' 
								ORDER BY OwnerRepName 
								FOR JSON PATH), '''');

		SELECT @RCRep = IsNull(
							(SELECT OwnerRepName 
								FROM Classifications
								WHERE ClassificationType = ''RC Territory'' AND OwnerRepName <> ''Vacant'' 
								ORDER BY OwnerRepName 
								FOR JSON PATH), '''');
			
		SELECT @Level5 AS level_5, @Level2 AS level_2, @SalesRep AS sales_rep, @HortRep AS hort_rep, @RCRep AS rc_rep
		'

		EXECUTE sp_executesql @QUERY
END
