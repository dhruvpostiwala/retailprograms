﻿

CREATE FUNCTION [dbo].[TVF_Get_IndependentPlanTransactions]  (@SEASON INT, @CUT_OFF_DATE DATE, @STATEMENTS AS UDT_RP_STATEMENT READONLY)

RETURNS @T TABLE(
	StatementCode INT,
	ProductCode VARCHAR(65), 
	ChemicalGroup VARCHAR(50), 
	Product VARCHAR(100), 
	ProductName VARCHAR(100), 	
	Hybrid_ID INT,
	SRP FLOAT, 
	SDP FLOAT,
	SWP FLOAT,
	Quantity FLOAT,
	Amount MONEY
)
AS
BEGIN
	
	INSERT INTO @T(Statementcode,ProductCode,Hybrid_ID,ChemicalGroup,Product,ProductName,SRP,SDP,SWP,Quantity,Amount)
	SELECT ST.StatementCode,tx.ProductCode,tx.Hybrid_ID,tx.ChemicalGroup, tx.Product, tx.ProductName, tx.SRP, tx.SDP, tx.SWP, tx.Quantity, tx.Amount as Amount	
	FROM @STATEMENTS ST		
		INNER JOIN RP_DT_Scenarios SCN
		ON SCN.StatementCode=ST.Statementcode						
		INNER JOIN ( 
			SELECT CONVERT(varchar(20),s.ID) ScenarioCode
				,sd.ProductCode
				,PR_H.ID AS Hybrid_ID
				,sd.Quantity
				,sd.usermodified_date
				,sd.UserModified_Amount AS Amount
				,ISNULL(PR.ChemicalGroup, 'InVigor') AS ChemicalGroup
				,ISNULL(PR.Product,PR_H.Hybrid_Name) AS Product
				,ISNULL(PR.ProductName,PR_H.Hybrid_Name) AS ProductName
				,COALESCE(prp.SDP,prp_h.SDP,0) AS SDP
				,COALESCE(prp.SRP,prp_h.SRP,0) AS SRP
				,COALESCE(prp.SWP,prp_h.SWP,0) AS SWP
			FROM RCT_Scenarios s
				JOIN RCT_PlanningModes pm			
				ON pm.ID=s.PlanningModeID
				JOIN   (
					SELECT scenarioid, productcode
						,iif(username like '%system adjustment%','System Adjustment',username) Author
						,UserModified_Amount
						,usermodified_date
						,pogamount-lag(pogamount,1,0) over (partition by scenarioid, productcode order by sys_start) Quantity
					FROM RCT_ScenarioDetails
					FOR System_Time ALL
				) sd
				ON sd.ScenarioID=s.ID	AND sd.Quantity <> 0
				LEFT JOIN ProductReference PR						ON PR.ProductCode=sd.ProductCode
				LEFT JOIN ProductReferencePricing PRP				ON PRP.ProductCode=PR.ProductCode AND PRP.Region='WEST' AND PRP.Season=@SEASON
				LEFT JOIN ProductReference_Hybrids PR_H				ON PR_H.Hybrid_Code=sd.ProductCode
				LEFT JOIN ProductReferencePricing_Hybrids PRP_H		ON PRP_H.Hybrid_ID=PR_H.ID AND PRP_H.Region='WEST' AND PRP_H.Season=@SEASON								
			WHERE s.[Primary]=1  AND pm.[Type]='Plan' AND pm.Season=@SEASON AND CAST(usermodified_date AS DATE) <= @CUT_OFF_DATE																
		) TX		
		ON TX.ScenarioCode=SCN.ScenarioCode

	RETURN
END
