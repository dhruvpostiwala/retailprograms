﻿CREATE FUNCTION [dbo].[TVF2022_Refresh_ForecastRewardsDisplay] (@STATEMENTCODE INT)
RETURNS @RewardsDisplayData TABLE(
	[Season] [int] NOT NULL,
	[Region] [varchar](4) NOT NULL,
	[RowNumber] [numeric](5, 2) NOT NULL,
	[RowType] [varchar](50) NOT NULL,
	[MarketLetterCode] [varchar](50) NOT NULL,
	[MarketLetterPDF] [varchar](100) NULL,
	[RewardCode] [varchar](50) NOT NULL,
	[SubRowCode] [varchar](50) NOT NULL,
	[Label] [varchar](120) NOT NULL,	
	[ProgramCode] [Varchar](50) NOT NULL DEFAULT '',
	[ProgramDescription] [nvarchar](MAX) NOT NULL DEFAULT '',
	[RowLabel] [varchar](120) NOT NULL DEFAULT '',
	[DisplayInput] [varchar](3) NOT NULL,
	[InputPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[InputText] [Numeric](18,2) NOT NULL DEFAULT 0,
	[InputSuffix] [varchar](10) NOT NULL DEFAULT '',	
	[DisplayQualifier] [varchar](3) NOT NULL,
	[QualifierPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[Qualifier] [Numeric](10,2) NOT NULL DEFAULT 0,
	[QualifierSuffix] [varchar](20) NOT NULL DEFAULT '',
	[Reward] [Numeric](18,2) NOT NULL DEFAULT 0,
	[RenderRow] [varchar](3) NOT NULL DEFAULT 'No'
)
AS
BEGIN
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @PROVINCE VARCHAR(2);
	DECLARE @STATEMENT_TYPE VARCHAR(20);
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @LEVEL5CODE VARCHAR(20);
	DECLARE @DATASETCODE INT;
	

	DECLARE @TBL_MarketLetterCodes TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterPDF Varchar(100) NULL,
		Title Varchar(200) NOT NULL,
		ProgramCode Varchar(50) NOT NULL
	)

	DECLARE @TBL_RewardsSummary TABLE (
		MarketLetterCode Varchar(50) NOT NULL,
		RewardCode Varchar(50) NOT NULL,
		RewardLabel Varchar(120) NOT NULL,
		TotalReward Numeric(18,2) NOT NULL
	)

	IF @STATEMENTCODE > 0 -- NEW FOR REWARD PLANNER
		SELECT @RETAILERCODE=RetailerCode, @SEASON=SEASON , @REGION=Region, @STATEMENT_TYPE=StatementType, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@STATEMENTCODE
	ELSE
		SELECT @RETAILERCODE=RetailerCode, @SEASON=SEASON , @REGION=Region, @STATEMENT_TYPE=StatementType, @DATASETCODE=DataSetCode FROM RPWeb_Statements WHERE StatementCode=@STATEMENTCODE

	SELECT @LEVEL5CODE=Level5Code, @PROVINCE=MailingProvince FROM RetailerProfile WHERE RetailerCode=@RETAILERCODE
	
	INSERT INTO @RewardsDisplayData(Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier)
	SELECT Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier
	FROM RP_Config_Forecast_RewardsDisplay
	WHERE Season=@SEASON AND Region=@Region


	INSERT INTO @TBL_MarketLetterCodes(MarketLetterCode, MarketLetterPDF, Title, ProgramCode)
	SELECT T1.MarketLetterCode, ML.PDFFileName, ML.Title, T1.ProgramCode
	FROM RPWeb_ML_ELG_Programs T1
		INNER JOIN RP_Config_MarketLetters ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.StatementCode=@STATEMENTCODE AND ML.Status='Active'



	INSERT INTO @TBL_RewardsSummary(MarketLetterCode, RewardCode, RewardLabel, TotalReward)
	SELECT RS.MarketLetterCode, RS.RewardCode, ISNULL(CRS.RewardLabel,'') AS RewardLabel, RS.TotalReward	
	FROM RPWeb_Rewards_Summary RS
		LEFT JOIN (
			SELECT ROW_NUMBER() OVER(PARTITION BY RewardCode ORDER BY Season DESC) [Rank], * 
			FROM RP_Config_Rewards_Summary 
			WHERE Season <= @SEASON
		) CRS ON CRS.RewardCode=RS.RewardCode AND CRS.[Rank] = 1
	WHERE RS.StatementCode=@STATEMENTCODE AND RS.RewardEligibility='Eligible' AND RS.RewardCode <> 'SG_E'
	
	

	UPDATE T1
	SET ProgramCode=T2.ProgramCode
		,ProgramDescription=IIF(T1.RowType='PROGRAM TOTAL',ISNULL(T3.[Description],''),'')
	FROM @RewardsDisplayData T1		
		INNER JOIN RP_Config_Programs T2	ON T2.Season=T1.Season AND T2.RewardCode=T1.RewardCode
		LEFT JOIN RP_Config_ML_Programs T3	ON T3.MarketLetterCode=T1.MarketLetterCode AND T3.ProgramCode=T2.ProgramCode AND IIF(@REGION='WEST',T3.Level5Code,@LEVEL5CODE)=@LEVEL5CODE
	WHERE T1.RowType IN  ('Program Total','Sub Row') AND T1.RewardCode <> ''
			
	-- UPDATE ROWTYPE = STATEMENT TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label
		,[Reward]=ISNULL(T2.TotalReward,0)
	FROM @RewardsDisplayData T1
		LEFT JOIN (
			SELECT SUM(TotalReward) AS [TotalReward] FROM @TBL_RewardsSummary 			
		) T2
		ON T1.RowType='Statement Total'
	WHERE T1.RowType='Statement Total'


	-- UPDATE ROWTYPE = MARKET LETTER TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=ML.Title
		,[Reward]=ISNULL(RS.TotalReward,0)
		,[MarketLetterPDF]=ML.MarketLetterPDF
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode, Title, MarketLetterPDF FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		LEFT JOIN	(
			SELECT MarketLetterCode, SUM(TotalReward) AS [TotalReward]
			FROM @TBL_RewardsSummary 
			GROUP BY MarketLetterCode
		) RS		
		ON RS.MarketLetterCode=ML.MarketLetterCode
	WHERE T1.RowType='Market Letter Total'

	-- UPDATE ROWTYPE = PROGRAM TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=RS.RewardLabel
		,[Reward]=RS.TotalReward
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		INNER JOIN @TBL_RewardsSummary  RS 
		ON RS.MarketLetterCode=ML.MarketLetterCode AND RS.RewardCode=T1.RewardCode			
	WHERE T1.RowType='Program Total'
	
	IF @REGION='WEST'
	BEGIN
		-- START SUB ROW UPDATES
		-- UPDATE INPUT DATA FOR ROWTYPE = SUB ROW

		-- West InVigor Innovation Bonus Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'INV_INB_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(InVigor300_Percentage) AS Reward_Percentage, SUM(InVigor300_Reward) AS Reward FROM RP2022Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_INB_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(InVigor200_Percentage) AS Reward_Percentage, SUM(InVigor200_Reward) AS Reward FROM RP2022Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_INV_INNOVATION_BONUS'

		-- WEST INVIGOR MARKET LETTER - INVIGOR PERFORMANCE REWARD
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputPrefix]=''
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]=' LEU'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Sales_Target'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage	, Reward		
				FROM RP2022Web_Rewards_W_INV_PERF 
				WHERE StatementCode=@STATEMENTCODE
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_INV_PERFORMANCE' AND T1.SubRowCode='Sales_Target'

	
		-- Base Reward Sub Rows
		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierSuffix] = '%'
			,[Qualifier] = ISNULL(BA.Reward_Percentage * 100, 0)
			,[Reward] = ISNULL(BA.Reward, 0)
		FROM @RewardsDisplayData T1
			LEFT JOIN
			   (SELECT StatementCode, ProgramCode, Level5Code, RewardSegment, SUM(RewardBrand_Sales) AS RewardBrand_Sales
					   ,MAX(Reward_Percentage) AS Reward_Percentage,SUM(Reward) AS Reward
				FROM RP2022_DT_Rewards_W_BaseReward_CPP
				GROUP BY StatementCode, ProgramCode, Level5Code, RewardSegment
				)BA
			ON T1.Label = BA.RewardSegment AND BA.StatementCode = @STATEMENTCODE
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2022_W_BASE_REWARD_CPP'


		-- Logistics Support Reward Sub Rows
		UPDATE T1
		SET [RenderRow] = CASE WHEN @LEVEL5CODE = 'D000001' THEN 'YES' ELSE 'No' END
			,[RowLabel] = T1.Label
			,[QualifierSuffix] = '%'
			,[Qualifier] = ISNULL(LS.Segment_Reward_Percentage * 100, 0)
			,[Reward] = ISNULL(LS.Reward, 0)
		FROM @RewardsDisplayData T1
			LEFT JOIN RP2022Web_Rewards_W_LogisticsSupport LS
			ON T1.Label = LS.BrandSegment AND LS.StatementCode = @STATEMENTCODE
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2022_W_LOGISTICS_SUPPORT'


		-- CANOLA CROP PROTECTION MARKET LETTER - Liberty/ Centurion / Facet L Tank Mix Bonus 
		-- Lib_Cent_Matched_Acres	
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=4
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Centurion'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Cent_Reward) AS Reward
				FROM RP2022Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_1'   

		-- Lib_Facet_Matched_Acres
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=6
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Facet_L'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Facet_Reward) AS Reward
				FROM RP2022Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_2'


		-- Planning_The_Business_Brand sub rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(CPP_Reward_Percentage) AS Reward_Percentage, SUM(CPP_Reward) AS Reward FROM RP2022Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Cent_Reward_Percentage) AS Reward_Percentage, SUM(Cent_Reward) AS Reward FROM RP2022Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_PLANNING_SUPP_BUSINESS'


		-- Loyal Bonus Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'LOYALTY_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(SegmentA_Reward_Percentage) AS Reward_Percentage, SUM(SegmentA_Reward) AS Reward FROM RP2022Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOYALTY_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(SegmentB_Reward_Percentage) AS Reward_Percentage, SUM(SegmentB_Reward) AS Reward FROM RP2022Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2022_W_LOYALTY_BONUS'
		

		-- Custom Seed Treatment Sub Rows
		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[Qualifier] = ISNULL(T2.PartA_Reward_Percentage * 100, CASE @LEVEL5CODE WHEN 'D000001' THEN 16 ELSE 14 END)
			,[QualifierSuffix] = '%'
			,[Reward] = ISNULL(T2.PartA_Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardBrand_Percentage) [PartA_Reward_Percentage], SUM(Reward) [PartA_Reward]
				FROM RP2022Web_Rewards_W_CustomSeed
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand <> 'NODULATOR PRO'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2022_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode = 'Row_1'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[Qualifier] = ISNULL(T2.PartB_Reward_Percentage * 100, 8)
			,[QualifierSuffix] = '%'
			,[Reward] = ISNULL(T2.PartB_Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardBrand_Percentage) [PartB_Reward_Percentage], SUM(Reward) [PartB_Reward]
				FROM RP2022Web_Rewards_W_CustomSeed
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'NODULATOR PRO'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2022_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode = 'Row_2'
		-- END OF SUB ROW UPDATES

		-- LETS UPDATE QUALIFIERS AT PROGRAM TOTAL ROW (NOTHING BUT REWARD PERCENTAGE AND REWARD AMOUNT)
		/*
			ACTIVE PROGRAMS - WEST
			Base Reward						            RP2022_W_BASE_REWARD				    BASE_W				[dbo].[RP2022Web_Rewards_W_INV_BaseReward]
			Brand Specific Support						RP2022_W_BRAND_SPEC_SUPPORT				BSS_W				[dbo].[RP2022Web_Rewards_W_BrandSpecificSupport]
			InVigor & Liberty Loyalty Bonus				RP2022_W_INV_LIB_LOY_BONUS				INV_LLB_W			[dbo].[RP2022Web_Rewards_W_INV_LLB]
			InVigor Innovation Bonus					RP2022_W_INV_INNOVATION_BONUS			INV_INB_W			[dbo].[RP2022Web_Rewards_W_INV_INNOV]
			InVigor Performance							RP2022_W_INV_PERFORMANCE				INV_PER_W			[dbo].[RP2022Web_Rewards_W_INV_PERF]
	*/


		UPDATE T1
		SET [RenderRow]='YES'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'BASE_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_INV_BaseReward WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'BASE_CPP_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_BaseReward_CPP WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'BSS_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_BrandSpecificSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_LLB_W' AS RewardCode, (SELECT MAX(Percentages) FROM (VALUES (MAX(Bonus_Percentage)),(MAX(Addendum_Percentage))) Data(Percentages)) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_INV_LLB WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_INB_W', MAX(IIF(InVigor300_Reward > 0, InVigor300_Percentage, IIF(InVigor200_Reward > 0, InVigor200_Percentage, 0))) As Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_PER_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_INV_PERF WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]				
					UNION
			    SELECT MarketLetterCode, 'PSB_W', 0 AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOG_SUP_W', MAX(Segment_Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_LogisticsSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'GROWTH_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_GrowthBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOYALTY_W', (SELECT MAX(Percentages) FROM (VALUES (MAX(SegmentA_Reward_Percentage)),(MAX(SegmentB_Reward_Percentage))) Data(Percentages)) As Reward_Percentage, SUM(SegmentA_Reward + SegmentB_Reward) As Reward FROM RP2022Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CLL_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_ClearfieldLentils WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]				
				    UNION
				SELECT MarketLetterCode, 'TM_BONUS_W', 0 AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_TankMixBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CST_W', MAX(RewardBrand_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2022Web_Rewards_W_CustomSeed WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL'
	END
	
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label
	FROM @RewardsDisplayData T1
		INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.RowType='DISCLAIMER'


	RETURN
END
