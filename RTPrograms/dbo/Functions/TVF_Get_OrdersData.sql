﻿


CREATE FUNCTION [dbo].[TVF_Get_OrdersData]  (@SEASON INT, @CUT_OFF_DATE DATE, @STATEMENTS AS UDT_RP_STATEMENT READONLY)

RETURNS @T TABLE(
		[StatementCode] INT,
		[TransType] [Varchar](20) NOT NULL,		
		[InvoiceDate] [Date] NOT NULL,
		[Productcode] [varchar](65) NOT NULL,
		[Quantity] [Decimal](18,2) NOT NULL
		
)
AS
BEGIN
	
	INSERT INTO @T(Statementcode, TransType, InvoiceDate, Productcode, Quantity)
	SELECT t1.StatementCode, 'Order' AS TransType, t1.InvoiceDate, t1.ProductCode, t1.Quantity * t1.Conversion AS Quantity 
	FROM RP_DT_Orders  t1
		INNER JOIN @STATEMENTS ST
		ON ST.StatementCode=T1.StatementCode
	WHERE Season=@SEASON AND CAST(InvoiceDate AS DATE) <= @CUT_OFF_DATE --AND InRadius=1 
		

	/*
		UNION

	SELECT StatementCode, 'Pre-Sales' AS TransType, InvoiceDate, ProductCode, Quantity 
	FROM RP_DT_Transactions 
	WHERE BASFSeason=@SEASON  AND InRadius=1 AND CAST(InvoiceDate AS DATE) <= @CUT_OFF_DATE

		UNION

	SELECT StatementCode, 'Post-Sales' AS TransType, InvoiceDate, ProductCode, Quantity 
	FROM RP_DT_Transactions 
	WHERE BASFSeason=@SEASON AND InRadius=1 AND CAST(InvoiceDate AS DATE) > @CUT_OFF_DATE
	*/

	RETURN
END
