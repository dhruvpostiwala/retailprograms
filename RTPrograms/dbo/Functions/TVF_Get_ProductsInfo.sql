﻿


CREATE FUNCTION [dbo].[TVF_Get_ProductsInfo] (@SEASON INT)

RETURNS @T TABLE(
	ProductCode VARCHAR(65), 
	Hybrid_ID INT,
	ProductName VARCHAR(100), 
	Product VARCHAR(100), 
	ChemicalGroup VARCHAR(50), 
	Region VARCHAR(4), 
	ConversionW Float, 
	ConversionE Float,
	Forecasting_W VARCHAR(5),
	Forecasting_E VARCHAR(5),
	SRP FLOAT, 
	SDP FLOAT, 
	SWP FLOAT,
	DocType VARCHAR(10),
	Sector VARCHAR(50),
	Category VARCHAR(50)
)
AS
BEGIN
	
	-- NEW: RRT-748 - include docType = 80 "BASF Forecasting" products

	INSERT INTO @T (ProductCode, Hybrid_ID, ProductName, Product, ChemicalGroup, Region, ConversionW, ConversionE, Forecasting_W, Forecasting_E, SRP, SDP, SWP, DocType, Sector, Category)
	SELECT PR.ProductCode, NULL AS Hybrid_ID, PR.ProductName, PR.Product, PR.ChemicalGroup,ISNULL(PRP.Region,'') AS [Region],PR.ConversionW, PR.ConversionE, PR.Forecasting_W, PR.Forecasting_E		
		, ISNULL(PRP.SRP,0) AS SRP, ISNULL(PRP.SDP,0) AS SDP, ISNULL(PRP.SWP,0) AS SWP
		,PR.DocType ,PR.Sector, PR.Category
	FROM ProductReference PR 
		LEFT JOIN ProductReferencePricing PRP
		ON PRP.ProductCode=PR.ProductCode AND PRP.Season=@SEASON
	WHERE PR.DocType IN (10,80) AND PR.Product NOT LIKE '%(COMMITMENT WITHOUT SALES)%'
	
		UNION

	SELECT PR.Hybrid_Code AS ProductCode, PR.ID AS Hybrid_ID, Hybrid_Name AS ProductName, Hybrid_Name AS Product, 'InVigor' AS ChemicalGroup, ISNULL(PRP.Region,'') AS [Region], PR.ConversionW, PR.ConversionE, PR.Forecasting_W, PR.Forecasting_E
		, ISNULL(PRP.SRP,0) AS SRP, ISNULL(PRP.SDP,0) AS SDP, ISNULL(PRP.SWP,0) AS SWP
		, '' DocType, 'InVigor' AS Sector, 'InVigor' AS Category
	FROM ProductReference_Hybrids PR 
		LEFT JOIN ProductReferencePricing_Hybrids PRP
		ON PRP.Hybrid_ID=PR.ID AND PRP.Season=@SEASON

	RETURN
END
