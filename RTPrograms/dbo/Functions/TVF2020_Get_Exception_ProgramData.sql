﻿


CREATE FUNCTION [dbo].[TVF2020_Get_Exception_ProgramData]  (@STATEMENTS AS UDT_RP_STATEMENT READONLY, @EX_REWARDCODE VARCHAR(50))

RETURNS @T TABLE(
		[StatementCode] INT,
		[CYSales] money NOT NULL DEFAULT 0
)
AS
BEGIN
	
	INSERT INTO @T(StatementCode,CYSales)
	SELECT ST.StatementCode,EX.CYSales 
	FROM 
	@STATEMENTS ST
	INNER JOIN
	(
			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_PlanningTheBusiness]
			GROUP BY StatementCode
			UNION ALL
			SELECT StatementCode,RewardBrand_Sales AS 'CYSales',ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_POGSalesBonus]

			UNION ALL

			SELECT StatementCode, Pursuit_Qty_CY AS 'CYSales',ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport]

			UNION ALL

			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode		
			from [dbo].[RP2020Web_Rewards_E_PortfolioSupport]
			GROUP BY StatementCode

			UNION ALL

			SELECT StatementCode,SUM(RewardBrand_Acres) as 'CYSales',MAX(ProgramCode) ProgramCode			
			FROM [dbo].[RP2020Web_Rewards_E_RowCropFungSupport]
			GROUP BY StatementCode

			UNION ALL

			SELECT StatementCode,SUM(RewardBrand_CY_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode
			from [dbo].[RP2020Web_Rewards_E_SSLB]
			GROUP BY StatementCode
			UNION ALL

			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode)
			FROM [dbo].[RP2020Web_Rewards_E_SupplySales]
			GROUP BY StatementCode
	)EX ON EX.StatementCode = ST.StatementCode
	INNER JOIN  RP_CONFIG_PROGRAMS P ON P.ProgramCode = EX.ProgramCode AND P.RewardCode = LEFT(@EX_REWARDCODE, len(@EX_REWARDCODE)-1)

	RETURN
END