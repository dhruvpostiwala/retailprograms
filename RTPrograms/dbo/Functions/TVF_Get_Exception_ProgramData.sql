﻿




CREATE  FUNCTION [dbo].[TVF_Get_Exception_ProgramData]  (@SEASON INT ,@STATEMENTS AS UDT_RP_STATEMENT READONLY, @EX_REWARDCODE VARCHAR(50))

RETURNS @T TABLE(
		[StatementCode] INT,
		[CYSales] money NOT NULL DEFAULT 0,
		[CurrentReward] money NOT NULL DEFAULT 0
)
AS
BEGIN
	
	--will try to make season more dynamic but this will have to work for now

	

	IF(@SEASON = 2020) 
	BEGIN
		INSERT INTO @T(StatementCode,CYSales, CurrentReward)
		SELECT ST.StatementCode,EX.CYSales,CurrentReward 
		FROM 
		@STATEMENTS ST
		INNER JOIN 	(
				SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode, SUM(Reward) AS 'CurrentReward'
				FROM [dbo].[RP2020Web_Rewards_E_PlanningTheBusiness]
				GROUP BY StatementCode

				UNION ALL

				SELECT StatementCode,RewardBrand_Sales AS 'CYSales',ProgramCode,Reward AS 'CurrentReward'
				FROM [dbo].[RP2020Web_Rewards_E_POGSalesBonus]

				UNION ALL

				SELECT StatementCode, Pursuit_Qty_CY AS 'CYSales',ProgramCode,Reward AS 'CurrentReward'
				FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport]

				UNION ALL

				SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode, SUM(Reward) AS 'CurrentReward'		
				from [dbo].[RP2020Web_Rewards_E_PortfolioSupport]
				GROUP BY StatementCode

				UNION ALL

				SELECT StatementCode,SUM(RewardBrand_Acres) as 'CYSales',MAX(ProgramCode) ProgramCode, SUM(Reward) AS 'CurrentReward'			
				FROM [dbo].[RP2020Web_Rewards_E_RowCropFungSupport]
				GROUP BY StatementCode

				UNION ALL

				SELECT StatementCode,SUM(RewardBrand_CY_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode, SUM(Reward) AS 'CurrentReward'
				from [dbo].[RP2020Web_Rewards_E_SSLB]
				GROUP BY StatementCode
				UNION ALL

				SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode), SUM(Reward) AS 'CurrentReward'
				FROM [dbo].[RP2020Web_Rewards_E_SupplySales]
				GROUP BY StatementCode


				UNION ALL 

				SELECT StatementCode,SUM(RewardBrand_ElgSales_CY) as 'CYSales',MAX(ProgramCode), SUM(Reward) AS 'CurrentReward'
				FROM [dbo].[RP2020Web_Rewards_E_HortiCultureRetailSupport] 
				GROUP BY StatementCode

		)EX 
		ON EX.StatementCode = ST.StatementCode
			INNER JOIN  RP_CONFIG_PROGRAMS P 
			ON P.ProgramCode = EX.ProgramCode AND P.RewardCode = LEFT(@EX_REWARDCODE, len(@EX_REWARDCODE)-1)

	END

	RETURN
END
