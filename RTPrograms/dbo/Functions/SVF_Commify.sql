﻿


-- =============================================
-- Author:		DERMAREY BAKER,
-- Create date: APRIL 3 2020
-- Description:	Display values if negative
--     CONVERT NEGATIVE VALUES WITH BRACKETS AROUND THEM
-- accept float value and ismoney parameters
--and returns varchar 
-- =============================================
CREATE FUNCTION [dbo].[SVF_Commify]
(
	-- Add the parameters for the function here
	@Value MONEY,
	@Unit Char(5) 
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @TEMP_VALUE MONEY=ISNULL(@VALUE,0)
	DECLARE @RESULT VARCHAR(50)

	IF @UNIT='%' OR @UNIT ='F%'
		BEGIN
			SET @TEMP_VALUE = @TEMP_VALUE * 100
		--	SET @RESULT = Convert(varchar(50),CAST(@TEMP_VALUE AS DECIMAL(6,4)),-1) 
			SET @RESULT = Convert(varchar(50),CAST(@TEMP_VALUE AS MONEY),-1) 
		END
	/*ELSE IF @UNIT ='F$'
		BEGIN 
			SET @RESULT  = FORMAT(@VALUE,'C', 'fr-FR'); -- formats it in 1 234,45 € RP4183
		END*/
	ELSE		
		SET @RESULT = Convert(varchar(50),CAST(@TEMP_VALUE AS MONEY),-1) 

	IF @VALUE < 0 
		SET @RESULT = '(' + Replace(@RESULT,'-','') + ')'
	
	IF @UNIT='$'
		SET @RESULT = '$'+ @RESULT 
	ELSE IF @UNIT='%'
		SET @RESULT += '%'
	ELSE IF @UNIT = 'F$'
		BEGIN
			SET @RESULT = REPLACE(@RESULT,'$','')
			SET @RESULT = REPLACE(@RESULT,',',' ')
			SET @RESULT = REPLACE(@RESULT,'.',',')
			SET @RESULT += ' $'
		END
	ELSE IF @UNIT = 'F%'
		BEGIN
			SET @RESULT = REPLACE(@RESULT,'.',',')
			SET @RESULT += ' %'
		END
	ELSE IF @UNIT = 'F'
		BEGIN
			SET @RESULT = REPLACE(@RESULT,',',' ')
			SET @RESULT = REPLACE(@RESULT,'.',',')
		END
		

			   	
	RETURN  @RESULT 
END
