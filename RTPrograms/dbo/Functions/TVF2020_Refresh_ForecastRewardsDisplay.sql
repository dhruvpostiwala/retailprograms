﻿
CREATE FUNCTION [dbo].[TVF2020_Refresh_ForecastRewardsDisplay] (@STATEMENTCODE INT)

RETURNS @RewardsDisplayData TABLE(
	[Season] [int] NOT NULL,
	[Region] [varchar](4) NOT NULL,
	[RowNumber] [numeric](5, 2) NOT NULL,
	[RowType] [varchar](50) NOT NULL,
	[MarketLetterCode] [varchar](50) NOT NULL,
	[MarketLetterPDF] [varchar](100) NULL,
	[RewardCode] [varchar](50) NOT NULL,
	[SubRowCode] [varchar](50) NOT NULL,
	[Label] [varchar](100) NOT NULL,	
	[ProgramCode] [Varchar](50) NOT NULL DEFAULT '',
	[ProgramDescription] [nvarchar](MAX) NOT NULL DEFAULT '',
	[RowLabel] [varchar](100) NOT NULL DEFAULT '',
	[DisplayInput] [varchar](3) NOT NULL,
	[InputPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[InputText] [Numeric](18,2) NOT NULL DEFAULT 0,
	[InputSuffix] [varchar](10) NOT NULL DEFAULT '',	
	[DisplayQualifier] [varchar](3) NOT NULL,	
	[QualifierPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[Qualifier] [Numeric](10,2) NOT NULL DEFAULT 0,
	[QualifierSuffix] [varchar](20) NOT NULL DEFAULT '',
	[Reward] [Numeric](18,2) NOT NULL DEFAULT 0,
	[RenderRow] [varchar](3) NOT NULL DEFAULT 'No'
)
AS
BEGIN
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @STATEMENT_TYPE VARCHAR(20);
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @LEVEL5CODE VARCHAR(20);
	DECLARE @DATASETCODE INT;

	DECLARE @TBL_MarketLetterCodes TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterPDF Varchar(100) NULL,
		Title Varchar(200) NOT NULL,
		ProgramCode Varchar(50) NOT NULL
	)

	DECLARE @TBL_RewardsSummary TABLE (
		MarketLetterCode Varchar(50) NOT NULL,
		RewardCode Varchar(50) NOT NULL,
		RewardLabel Varchar(100) NOT NULL,
		TotalReward Numeric(18,2) NOT NULL
	)

	SELECT @RETAILERCODE=RetailerCode, @SEASON=SEASON , @REGION=Region, @STATEMENT_TYPE=StatementType, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@STATEMENTCODE
	SELECT @LEVEL5CODE=Level5Code FROM RetailerProfile WHERE RetailerCode=@RETAILERCODE
	
	INSERT INTO @RewardsDisplayData(Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier)
	SELECT Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier
	FROM RP_Config_Forecast_RewardsDisplay
	WHERE Season=@SEASON AND Region=@Region
		
	INSERT INTO @TBL_MarketLetterCodes(MarketLetterCode, MarketLetterPDF, Title, ProgramCode)
	SELECT T1.MarketLetterCode, ML.PDFFileName, ML.Title, T1.ProgramCode
	FROM RPWeb_ML_ELG_Programs T1
		INNER JOIN RP_Config_MarketLetters ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.StatementCode=@STATEMENTCODE AND ML.Status='Active'

	INSERT INTO @TBL_RewardsSummary(MarketLetterCode, RewardCode, RewardLabel, TotalReward)
	SELECT RS.MarketLetterCode, RS.RewardCode, ISNULL(CRS.RewardLabel,'') AS RewardLabel, RS.TotalReward	
	FROM RPWeb_Rewards_Summary RS
		LEFT JOIN RP_Config_Rewards_Summary CRS ON CRS.RewardCode=RS.RewardCode
	WHERE RS.StatementCode=@STATEMENTCODE AND RS.RewardEligibility='Eligible' AND RS.RewardCode <> 'SG_E'
	
	UPDATE T1
	SET ProgramCode=T2.ProgramCode
		,ProgramDescription=IIF(T1.RowType='PROGRAM TOTAL',ISNULL(T3.[Description],''),'')
	FROM @RewardsDisplayData T1		
		INNER JOIN RP_Config_Programs T2	ON T2.Season=T1.Season AND T2.RewardCode=T1.RewardCode
		LEFT JOIN RP_Config_ML_Programs T3	ON T3.MarketLetterCode=T1.MarketLetterCode AND T3.ProgramCode=T2.ProgramCode AND IIF(@REGION='WEST',T3.Level5Code,@LEVEL5CODE)=@LEVEL5CODE
	WHERE T1.RowType IN  ('Program Total','Sub Row') AND T1.RewardCode <> ''
			
	-- UPDATE ROWTYPE = STATEMENT TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label
		,[Reward]=ISNULL(T2.TotalReward,0)
	FROM @RewardsDisplayData T1
		LEFT JOIN (
			SELECT SUM(TotalReward) AS [TotalReward] FROM @TBL_RewardsSummary 			
		) T2
		ON T1.RowType='Statement Total'
	WHERE T1.RowType='Statement Total'

	-- UPDATE ROWTYPE = MARKET LETTER TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=ML.Title
		,[Reward]=ISNULL(RS.TotalReward,0)
		,[MarketLetterPDF]=ML.MarketLetterPDF
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode, Title, MarketLetterPDF FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		LEFT JOIN	(
			SELECT MarketLetterCode, SUM(TotalReward) AS [TotalReward]
			FROM @TBL_RewardsSummary 
			GROUP BY MarketLetterCode
		) RS		
		ON RS.MarketLetterCode=ML.MarketLetterCode
	WHERE T1.RowType='Market Letter Total'

	-- UPDATE ROWTYPE = PROGRAM TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=RS.RewardLabel
		,[Reward]=RS.TotalReward
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		INNER JOIN @TBL_RewardsSummary  RS 
		ON RS.MarketLetterCode=ML.MarketLetterCode AND RS.RewardCode=T1.RewardCode			
	WHERE T1.RowType='Program Total'
	
	IF @REGION='WEST'
	BEGIN
		-- START SUB ROW UPDATES
		-- UPDATE INPUT DATA FOR ROWTYPE = SUB ROW 
		-- WEST INVIGOR MARKET LETTER - INVIGOR PERFORMANCE REWARD
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputPrefix]=''
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]=' LEU'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Sales_Target'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage	, Reward		
				FROM RP2020Web_Rewards_W_INV_PERF 
				WHERE StatementCode=@STATEMENTCODE
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_INV_PERFORMANCE' AND T1.SubRowCode='Sales_Target'   

		-- West Efficiency Rebate Total Portfolio Support 
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Support_Reward_Percentage) AS Reward_Percentage, SUM(Support_Reward) AS Reward
				FROM RP2020Web_Rewards_W_EFF_REBATE 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_EFF_REBATE' AND T1.SubRowCode='Row_1'

		-- West Efficiency Rebate Escalator Bonus
		-- NOTE: only display if retail is receiving 1% or 1.75%
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Support_Reward_Percentage) AS Support_Reward_Percentage, MAX(Growth_Reward_Percentage) AS Reward_Percentage, SUM(Growth_Reward) AS Reward
				FROM RP2020Web_Rewards_W_EFF_REBATE 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_EFF_REBATE' AND T1.SubRowCode='Row_2' AND T2.Support_Reward_Percentage IN (0.01, 0.0175)
	
		-- CANOLA CROP PROTECTION MARKET LETTER - Liberty/ Centurion / Facet L Tank Mix Bonus 
		-- Lib_Cent_Matched_Acres	
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=IIF(ISNULL(T2.Reward,0)=0,0,4)
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Centurion'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Cent_Reward) AS Reward
				FROM RP2020Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_1'   

		-- Lib_Facet_Matched_Acres
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=IIF(ISNULL(T2.Reward,0)=0,0,2)
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Facet_L'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Facet_Reward) AS Reward
				FROM RP2020Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_2' 

		-- AgRewards_Bonus_Lead_Closed
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Leads_Per_Closed'
			) EI
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Leads_Closed_Reward) AS Reward
				FROM RP2020Web_Rewards_W_AgRrewardsBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_AGREWARDS_BONUS' AND T1.SubRowCode='Row_1' 

		-- AgRewards_Bonus_No_Lead
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=100-ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Leads_Per_Closed'
			) EI
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Leads_NotClosed_Reward) AS Reward
				FROM RP2020Web_Rewards_W_AgRrewardsBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_AGREWARDS_BONUS' AND T1.SubRowCode='Row_2' 

		-- Planning_The_Business_Brand sub rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(Growth_Reward_Percentage) AS Reward_Percentage, SUM(Growth_Reward) AS Reward FROM RP2020Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Lib_Reward_Percentage) AS Reward_Percentage, SUM(Lib_Reward) AS Reward FROM RP2020Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_3' AS SubRowCode, MAX(Cent_Reward_Percentage) AS Reward_Percentage, SUM(Cent_Reward) AS Reward FROM RP2020Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_PLANNING_SUPP_BUSINESS'

		-- RC-3542: hide Centurion row for Coops since they aren't eligible
		If @LEVEL5CODE = 'D0000117'
		BEGIN
			UPDATE @RewardsDisplayData SET [RenderRow]='NO' WHERE ProgramCode='RP2020_W_PLANNING_SUPP_BUSINESS' AND RowType='Sub Row' AND SubRowCode='Row_3'
		END

		-- Retail_Bonus_Payment sub rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'RET_BON_PAY_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(Cent_Reward_Percentage) AS Reward_Percentage, SUM(Cent_Reward) AS Reward FROM RP2020Web_Rewards_W_RetailBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'RET_BON_PAY_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Lib_Reward_Percentage) AS Reward_Percentage, SUM(Lib_Reward) AS Reward FROM RP2020Web_Rewards_W_RetailBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_RETAIL_BONUS_PAYMENT'

		-- Custom_Seed_Treating Insure_Cereal
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]=''
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Insure_Cereal'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage, Reward
				FROM RP2020Web_Rewards_W_CustomSeed 
				WHERE StatementCode=@STATEMENTCODE AND RewardBrand='INSURE CEREAL'
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode='Row_1'  

		-- Custom_Seed_Treating Insure_Pulse
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]=''
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Insure_Pulse'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage, Reward
				FROM RP2020Web_Rewards_W_CustomSeed 
				WHERE StatementCode=@STATEMENTCODE AND RewardBrand='INSURE PULSE'
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode='Row_2'

		-- Custom_Seed_Treating Nodulator_Pro
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]=''
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Nodulator_Pro'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage, Reward
				FROM RP2020Web_Rewards_W_CustomSeed 
				WHERE StatementCode=@STATEMENTCODE AND RewardBrand='NODULATOR PRO'
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode='Row_3'

		-- Soybean_Pulse_Planning Part A
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[QualifierPrefix]=''
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(PartA_Reward_Percentage) AS Reward_Percentage, SUM(PartA_Reward) AS Reward
				FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
				WHERE StatementCode=@STATEMENTCODE
				GROUP BY MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_1'

		-- Soybean_Pulse_Planning Part B & C only if receiving a reward
		DECLARE @PartBC_Eligible NUMERIC(18,2) = (SELECT [Reward] FROM @RewardsDisplayData WHERE RowType='Sub Row' AND ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND SubRowCode = 'Row_1')
		IF @PartBC_Eligible > 0
		BEGIN

			-- RRT-817: split the part B and C rows into 2 rows each: 1 row for the perc, and one row for the target

			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[QualifierPrefix]=''
				,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
				,[QualifierSuffix]='%'
				,[Reward]=ISNULL(T2.Reward,0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, MAX(PartB_Reward_Percentage) AS Reward_Percentage, SUM(PartB_Reward) AS Reward
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_2'

			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[InputPrefix]='$'
				,[InputText]=ISNULL(T2.Sales_LY * (EI.FieldValue / 100),0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				INNER JOIN (
					SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='PartB_Target'
				) EI
				ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, SUM(PartB_QualSales_LY) AS Sales_LY
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_3'

			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[QualifierPrefix]=''
				,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
				,[QualifierSuffix]='%'
				,[Reward]=ISNULL(T2.Reward,0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, MAX(PartC_Reward_Percentage) AS Reward_Percentage, SUM(PartC_Reward) AS Reward
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_4'

			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[InputPrefix]='$'
				,[InputText]=ISNULL(T2.Sales_LY * (EI.FieldValue / 100),0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				INNER JOIN (
					SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='PartC_Target'
				) EI	
				ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, SUM(PartC_QualSales_LY) AS Sales_LY
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_5'

			/*
			-- Soybean_Pulse_Planning Part B
			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[InputPrefix]='$'
				,[InputText]=ISNULL(T2.Sales_LY * (EI.FieldValue / 100),0)
				,[QualifierPrefix]=''
				,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
				,[QualifierSuffix]='%'
				,[Reward]=ISNULL(T2.Reward,0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				INNER JOIN (
					SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='PartB_Target'
				) EI	
				ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, SUM(PartB_QualSales_LY) AS Sales_LY, MAX(PartB_Reward_Percentage) AS Reward_Percentage, SUM(PartB_Reward) AS Reward
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_2'

			-- Soybean_Pulse_Planning Part C
			UPDATE T1
			SET [RenderRow]='YES'
				,[RowLabel]=T1.Label
				,[InputPrefix]='$'
				,[InputText]=ISNULL(T2.Sales_LY * (EI.FieldValue / 100),0)
				,[QualifierPrefix]=''
				,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
				,[QualifierSuffix]='%'
				,[Reward]=ISNULL(T2.Reward,0)
			FROM @RewardsDisplayData T1
				INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
				INNER JOIN (
					SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='PartC_Target'
				) EI	
				ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
				LEFT JOIN (
					SELECT MarketLetterCode, ProgramCode, SUM(PartC_QualSales_LY) AS Sales_LY, MAX(PartC_Reward_Percentage) AS Reward_Percentage, SUM(PartC_Reward) AS Reward
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE StatementCode=@STATEMENTCODE
					GROUP BY MarketLetterCode, ProgramCode
				) T2
				ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
			WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.SubRowCode='Row_3'
			*/

		END

		-- Clearfield_Lentils_Support $200 commitment row
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]=''	
			,[QualifierPrefix]='$'
			,[Qualifier]=ISNULL(T2.Reward_Percentage,200)
			,[QualifierSuffix]=''
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Early_Commitment'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Commitment_Early_Amount) AS Reward_Percentage, SUM(Commitment_Early_Reward) AS Reward
				FROM RP2020Web_Rewards_W_ClearfieldLentils 
				WHERE StatementCode=@STATEMENTCODE AND RewardBrand = 'COMMITMENT'
				GROUP BY MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CLL_SUPPORT' AND T1.SubRowCode='Row_1'

		-- Clearfield_Lentils_Support $100 commitment row
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]=''	
			,[QualifierPrefix]='$'
			,[Qualifier]=ISNULL(T2.Reward_Percentage,100)
			,[QualifierSuffix]=''
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Late_Commitment'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Commitment_Late_Amount) AS Reward_Percentage, SUM(Commitment_Late_Reward) AS Reward
				FROM RP2020Web_Rewards_W_ClearfieldLentils 
				WHERE StatementCode=@STATEMENTCODE AND RewardBrand = 'COMMITMENT'
				GROUP BY MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CLL_SUPPORT' AND T1.SubRowCode='Row_2'

		-- Clearfield_Lentils_Support Matching Reward row
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[QualifierPrefix]=''
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Herbicide_Reward_Percentage) AS Reward_Percentage, SUM(Herbicide_Reward) AS Reward
				FROM RP2020Web_Rewards_W_ClearfieldLentils 
				WHERE StatementCode=@STATEMENTCODE
				GROUP BY MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_CLL_SUPPORT' AND T1.SubRowCode='Row_3'

		-- END OF SUB ROW UPDATES

		-- LETS UPDATE QUALIFIERS AT PROGRAM TOTAL ROW (NOTHING BUT REWARD PERCENTAGE AND REWARD AMOUNT)
			/*
			ACTIVE PROGRAMS

			InVigor & Liberty Loyalty Bonus		RP2020_W_INV_LIB_LOY_BONUS		INV_LLB_W		[dbo].[RP2020Web_Rewards_W_INV_LLB]
			Efficiency Rebate					RP2020_W_EFF_REBATE				EFF_REB_W		[dbo].[RP2020Web_Rewards_W_EFF_REBATE]
			Brand Specific Support				RP2020_W_BRAND_SPEC_SUPPORT		BSS_W			[dbo].[RP2020Web_Rewards_W_BrandSpecificSupport]	
			InVigor Performance					RP2020_W_INV_PERFORMANCE		INV_PER_W		[dbo].[RP2020Web_Rewards_W_INV_PERF]
			Warehouse Payment					RP2020_W_WAREHOUSE_PAYMENT		WH_PAYMENT_W	[dbo].[RP2020Web_Rewards_W_WarehousePayments]
			Payment on Time 					RP2020_W_PAYMENT_ON_TIME		POT_W			[dbo].[RP2020Web_Rewards_W_PaymentOnTime]

		*/

		UPDATE T1
		SET [RenderRow]='YES'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'INV_LLB_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_INV_LLB WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
 					UNION
				SELECT MarketLetterCode, 'INV_PER_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_INV_PERF WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
					UNION
				SELECT MarketLetterCode, 'EFF_REB_W' AS RewardCode, MAX(Support_Reward_Percentage + Growth_Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward  FROM RP2020Web_Rewards_W_EFF_REBATE WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION 
				SELECT MarketLetterCode, 'BSS_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_BrandSpecificSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
					UNION 
				SELECT MarketLetterCode, 'WH_PAYMENT_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_WarehousePayments WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION 
				SELECT MarketLetterCode, 'POT_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_PaymentOnTime WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION 
				SELECT MarketLetterCode, 'PORT_ACH_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_PortfolioAchievement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION 
				SELECT MarketLetterCode, 'INVENTORY_MGMT_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_InventoryManagement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION 
				SELECT MarketLetterCode, 'PSB_W', MAX(Growth_Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''
	
		-- LETS UPDATE QUALIFIERS AT PROGRAM TOTAL ROW (NOTHING BUT REWARD AMOUNT)
		UPDATE T1
		SET [RenderRow]='YES'				
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'TRUCK_LOAD_W' AS RewardCode, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_TruckloadReward WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
					UNION
				SELECT MarketLetterCode, 'CST_W' AS RewardCode, SUM(Reward) AS Reward FROM RP2020Web_Rewards_W_CustomSeed WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''

		/*
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, GroupLabel, Reward_Percentage	, Reward
				FROM RP2020Web_Rewards_W_EFF_REBATE 
				WHERE StatementCode=@STATEMENTCODE
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode AND T1.Label=T2.GroupLabel
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2020_W_EFF_REBATE' AND T1.SubRowCode IN ('ROW_1','ROW_2')
		*/
	END -- REGION='WEST'

	IF @REGION='EAST'
	BEGIN

		/*
		POG_SALES_BONUS_E		[dbo].[RP2020Web_Rewards_E_POGSalesBonus]
		SSLB_E					[dbo].[RP2020Web_Rewards_E_SSLB]
		SUPPLY_SALES_E			[dbo].[RP2020Web_Rewards_E_SupplySales]
		*/

		-- Pursuit Support Reward
		UPDATE T1
		SET [RenderRow]='YES'
			,[QualifierPrefix]=IIF(ISNULL(T2.Reward,0)=0,'','$')
			,[Qualifier]=IIF(ISNULL(T2.Reward,0)=0,0,Reward_Amount)
			,[QualifierSuffix]=IIF(ISNULL(T2.Reward,0)=0,'','/case')
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'PURS_SUP_E' AS RewardCode, MAX(Pursuit_Reward_Amount) AS Reward_Amount, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_PursuitSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''

		-- Row Crop Fungicide Support Reward
		UPDATE T1
		SET [RenderRow]='YES'
			,[QualifierPrefix]=IIF(ISNULL(T2.Reward,0)=0,'','$')
			,[Qualifier]=IIF(ISNULL(T2.Reward,0)=0,0,Reward_Amount)
			,[QualifierSuffix]=IIF(ISNULL(T2.Reward,0)=0,'','/acre')
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'ROW_CROP_FUNG_E' AS RewardCode, MAX(Reward_Amount) AS Reward_Amount, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_RowCropFungSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''

		UPDATE T1
		SET [RenderRow]='YES'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'POG_SALES_BONUS_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_POGSalesBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
 					UNION
				SELECT MarketLetterCode, 'SSLB_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_SSLB WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SUPPLY_SALES_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_SupplySales WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PORT_SUP_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_PortfolioSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''

		-- LETS UPDATE QUALIFIERS AT PROGRAM TOTAL ROW (NOTHING BUT REWARD AMOUNT)
		UPDATE T1
		SET [RenderRow]='YES'				
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'CGAS_E' AS RewardCode, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_CustomGroundApplication WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
					UNION
				SELECT MarketLetterCode, 'PRX_HEAD_E' AS RewardCode, SUM(Reward) AS Reward FROM RP2020Web_Rewards_E_PriaxorHeadlineSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode] 
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL' AND ISNULL(T2.RewardCode,'') <> ''

		-- Hide Custom App on East level 1's which are part of a family
		If @DATASETCODE > 0
		BEGIN
			UPDATE @RewardsDisplayData SET [RenderRow]='NO' WHERE ProgramCode='RP2020_E_CUSTOM_GROUND_APP_REWARD'
		END
				
	END -- REGION='EAST'
	
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label		
	FROM @RewardsDisplayData T1
		INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.RowType='DISCLAIMER'
	
	RETURN
END
