﻿
CREATE FUNCTION [dbo].[TVF2021_Refresh_ForecastRewardsDisplay] (@STATEMENTCODE INT)
RETURNS @RewardsDisplayData TABLE(
	[Season] [int] NOT NULL,
	[Region] [varchar](4) NOT NULL,
	[RowNumber] [numeric](5, 2) NOT NULL,
	[RowType] [varchar](50) NOT NULL,
	[MarketLetterCode] [varchar](50) NOT NULL,
	[MarketLetterPDF] [varchar](100) NULL,
	[RewardCode] [varchar](50) NOT NULL,
	[SubRowCode] [varchar](50) NOT NULL,
	[Label] [varchar](120) NOT NULL,	
	[ProgramCode] [Varchar](50) NOT NULL DEFAULT '',
	[ProgramDescription] [nvarchar](MAX) NOT NULL DEFAULT '',
	[RowLabel] [varchar](120) NOT NULL DEFAULT '',
	[DisplayInput] [varchar](3) NOT NULL,
	[InputPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[InputText] [Numeric](18,2) NOT NULL DEFAULT 0,
	[InputSuffix] [varchar](10) NOT NULL DEFAULT '',	
	[DisplayQualifier] [varchar](3) NOT NULL,
	[QualifierPrefix] [varchar](5) NOT NULL DEFAULT '',	
	[Qualifier] [Numeric](10,2) NOT NULL DEFAULT 0,
	[QualifierSuffix] [varchar](20) NOT NULL DEFAULT '',
	[Reward] [Numeric](18,2) NOT NULL DEFAULT 0,
	[RenderRow] [varchar](3) NOT NULL DEFAULT 'No'
)
AS
BEGIN
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @PROVINCE VARCHAR(2);
	DECLARE @STATEMENT_TYPE VARCHAR(20);
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @LEVEL5CODE VARCHAR(20);
	DECLARE @DATASETCODE INT;

	DECLARE @TBL_MarketLetterCodes TABLE(
		MarketLetterCode Varchar(50) NOT NULL,
		MarketLetterPDF Varchar(100) NULL,
		Title Varchar(200) NOT NULL,
		ProgramCode Varchar(50) NOT NULL
	)

	DECLARE @TBL_RewardsSummary TABLE (
		MarketLetterCode Varchar(50) NOT NULL,
		RewardCode Varchar(50) NOT NULL,
		RewardLabel Varchar(120) NOT NULL,
		TotalReward Numeric(18,2) NOT NULL
	)

	SELECT @RETAILERCODE=RetailerCode, @SEASON=SEASON , @REGION=Region, @STATEMENT_TYPE=StatementType, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@STATEMENTCODE
	SELECT @LEVEL5CODE=Level5Code, @PROVINCE=MailingProvince FROM RetailerProfile WHERE RetailerCode=@RETAILERCODE
	
	INSERT INTO @RewardsDisplayData(Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier)
	SELECT Season, Region, RowNumber, RowType, MarketLetterCode, RewardCode, SubRowCode, Label, DisplayInput, DisplayQualifier
	FROM RP_Config_Forecast_RewardsDisplay
	WHERE Season=@SEASON AND Region=@Region
		
	INSERT INTO @TBL_MarketLetterCodes(MarketLetterCode, MarketLetterPDF, Title, ProgramCode)
	SELECT T1.MarketLetterCode, ML.PDFFileName, ML.Title, T1.ProgramCode
	FROM RPWeb_ML_ELG_Programs T1
		INNER JOIN RP_Config_MarketLetters ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.StatementCode=@STATEMENTCODE AND ML.Status='Active'

	INSERT INTO @TBL_RewardsSummary(MarketLetterCode, RewardCode, RewardLabel, TotalReward)
	SELECT RS.MarketLetterCode, RS.RewardCode, ISNULL(CRS.RewardLabel,'') AS RewardLabel, RS.TotalReward	
	FROM RPWeb_Rewards_Summary RS
		LEFT JOIN (
			SELECT ROW_NUMBER() OVER(PARTITION BY RewardCode ORDER BY Season DESC) [Rank], * FROM RP_Config_Rewards_Summary WHERE Season <= @SEASON
		) CRS ON CRS.RewardCode=RS.RewardCode AND CRS.[Rank] = 1
	WHERE RS.StatementCode=@STATEMENTCODE AND RS.RewardEligibility='Eligible' AND RS.RewardCode <> 'SG_E'
	
	UPDATE T1
	SET ProgramCode=T2.ProgramCode
		,ProgramDescription=IIF(T1.RowType='PROGRAM TOTAL',ISNULL(T3.[Description],''),'')
	FROM @RewardsDisplayData T1		
		INNER JOIN RP_Config_Programs T2	ON T2.Season=T1.Season AND T2.RewardCode=T1.RewardCode
		LEFT JOIN RP_Config_ML_Programs T3	ON T3.MarketLetterCode=T1.MarketLetterCode AND T3.ProgramCode=T2.ProgramCode AND IIF(@REGION='WEST',T3.Level5Code,@LEVEL5CODE)=@LEVEL5CODE
	WHERE T1.RowType IN  ('Program Total','Sub Row') AND T1.RewardCode <> ''
			
	-- UPDATE ROWTYPE = STATEMENT TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label
		,[Reward]=ISNULL(T2.TotalReward,0)
	FROM @RewardsDisplayData T1
		LEFT JOIN (
			SELECT SUM(TotalReward) AS [TotalReward] FROM @TBL_RewardsSummary 			
		) T2
		ON T1.RowType='Statement Total'
	WHERE T1.RowType='Statement Total'

	-- UPDATE ROWTYPE = MARKET LETTER TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=ML.Title
		,[Reward]=ISNULL(RS.TotalReward,0)
		,[MarketLetterPDF]=ML.MarketLetterPDF
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode, Title, MarketLetterPDF FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		LEFT JOIN	(
			SELECT MarketLetterCode, SUM(TotalReward) AS [TotalReward]
			FROM @TBL_RewardsSummary 
			GROUP BY MarketLetterCode
		) RS		
		ON RS.MarketLetterCode=ML.MarketLetterCode
	WHERE T1.RowType='Market Letter Total'

	-- UPDATE ROWTYPE = PROGRAM TOTAL
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=RS.RewardLabel
		,[Reward]=RS.TotalReward
	FROM @RewardsDisplayData T1
		INNER JOIN (
			SELECT DISTINCT MarketLetterCode FROM  @TBL_MarketLetterCodes
		) ML	
		ON ML.MarketLetterCode=T1.MarketLetterCode
		INNER JOIN @TBL_RewardsSummary  RS 
		ON RS.MarketLetterCode=ML.MarketLetterCode AND RS.RewardCode=T1.RewardCode			
	WHERE T1.RowType='Program Total'
	
	IF @REGION='WEST'
	BEGIN
		-- START SUB ROW UPDATES
		-- UPDATE INPUT DATA FOR ROWTYPE = SUB ROW

		-- West Efficiency Bonus Total Portfolio Support 
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Support_Reward_Percentage) AS Reward_Percentage, SUM(Support_Reward) AS Reward
				FROM RP2021Web_Rewards_W_EFF_BONUS
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_EFF_BONUS' AND T1.SubRowCode='Row_1'

		-- West Efficiency Rebate Escalator Bonus
		-- NOTE: only display if retail is receiving 1% or 1.75%
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, MAX(Support_Reward_Percentage) AS Support_Reward_Percentage, MAX(Growth_Reward_Percentage) AS Reward_Percentage, SUM(Growth_Reward) AS Reward
				FROM RP2021Web_Rewards_W_EFF_BONUS
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_EFF_BONUS_REWARD' AND T1.SubRowCode='Row_2' AND T2.Support_Reward_Percentage IN (0.01, 0.0175)

		-- West InVigor Innovation Bonus Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'INV_INB_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(InVigor300_Percentage) AS Reward_Percentage, SUM(InVigor300_Reward) AS Reward FROM RP2021Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_INB_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(InVigor200_Percentage) AS Reward_Percentage, SUM(InVigor200_Reward) AS Reward FROM RP2021Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_INV_INNOVATION_BONUS'

		-- WEST INVIGOR MARKET LETTER - INVIGOR PERFORMANCE REWARD
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputPrefix]=''
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]=' LEU'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Sales_Target'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, Reward_Percentage	, Reward		
				FROM RP2021Web_Rewards_W_INV_PERF 
				WHERE StatementCode=@STATEMENTCODE
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_INV_PERFORMANCE' AND T1.SubRowCode='Sales_Target'

		-- Logistics Support Reward Sub Rows
		UPDATE T1
		SET [RenderRow] = CASE WHEN @LEVEL5CODE = 'D000001' THEN 'YES' ELSE 'No' END
			,[RowLabel] = T1.Label
			,[QualifierSuffix] = '%'
			,[Qualifier] = ISNULL(LS.Segment_Reward_Percentage * 100, 0)
			,[Reward] = ISNULL(LS.Reward, 0)
		FROM @RewardsDisplayData T1
			LEFT JOIN RP2021Web_Rewards_W_LogisticsSupport LS
			ON T1.Label = LS.BrandSegment AND LS.StatementCode = @STATEMENTCODE
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT'

		-- CANOLA CROP PROTECTION MARKET LETTER - Liberty/ Centurion / Facet L Tank Mix Bonus 
		-- Lib_Cent_Matched_Acres	
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label		
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=4
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Centurion'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Cent_Reward) AS Reward
				FROM RP2021Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_1'   

		-- Lib_Facet_Matched_Acres
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[InputText]=ISNULL(EI.FieldValue,0)
			,[InputSuffix]='%'				
			,[QualifierPrefix]='$'
			,[Qualifier]=6
			,[QualifierSuffix]='/jug'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode  AND ML.ProgramCode=T1.ProgramCode
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, FieldValue From RPWeb_User_EI_FieldValues WHERE StatementCode=@STATEMENTCODE AND FieldCode='Matched_Facet_L'
			) EI	
			ON EI.MarketLetterCode=ML.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode 
			LEFT JOIN (
				SELECT MarketLetterCode, ProgramCode, SUM(Lib_Facet_Reward) AS Reward
				FROM RP2021Web_Rewards_W_TankMixBonus 
				WHERE StatementCode=@STATEMENTCODE
				Group By MarketLetterCode, ProgramCode
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.ProgramCode=ML.ProgramCode 
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_TANK_MIX_BONUS' AND T1.SubRowCode='Row_2'

		-- West Retail Bonus Payment Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'RET_BON_PAY_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(Lib_Reward_Percentage) AS Reward_Percentage, SUM(Lib_Reward) AS Reward FROM RP2021Web_Rewards_W_RetailBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'RET_BON_PAY_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Cent_Reward_Percentage) AS Reward_Percentage, SUM(Cent_Reward) AS Reward FROM RP2021Web_Rewards_W_RetailBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_RETAIL_BONUS_PAYMENT'

		-- Planning_The_Business_Brand sub rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(CPP_Reward_Percentage) AS Reward_Percentage, SUM(CPP_Reward) AS Reward FROM RP2021Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Lib_Reward_Percentage) AS Reward_Percentage, SUM(Lib_Reward) AS Reward FROM RP2021Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_PLANNING_SUPP_BUSINESS'

		-- Loyal Bonus Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'LOYALTY_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(SegmentA_Reward_Percentage) AS Reward_Percentage, SUM(SegmentA_Reward) AS Reward FROM RP2021Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOYALTY_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(SegmentB_Reward_Percentage) AS Reward_Percentage, SUM(SegmentB_Reward) AS Reward FROM RP2021Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_LOYALTY_BONUS'

		-- Inventory Management Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'INVENTORY_MGMT_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(CPP_Percentage) AS Reward_Percentage, SUM(CPP_Reward) AS Reward FROM RP2021Web_Rewards_W_InventoryManagement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INVENTORY_MGMT_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(Liberty_Percentage) AS Reward_Percentage, SUM(Liberty_Reward) AS Reward FROM RP2021Web_Rewards_W_InventoryManagement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INVENTORY_MGMT_W' AS RewardCode, 'Row_3' AS SubRowCode, MAX(Centurion_Percentage) AS Reward_Percentage, SUM(Centurion_Reward) AS Reward FROM RP2021Web_Rewards_W_InventoryManagement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_INVENTORY_MGMT'

		-- Payment On Time Sub Rows
		UPDATE T1
		SET [RenderRow]='YES'
			,[RowLabel]=T1.Label
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'POT_W' AS RewardCode, 'Row_1' AS SubRowCode, MAX(IIF(RewardBrand NOT IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200', 'CENTURION'), Reward_Percentage, 0)) AS Reward_Percentage, SUM(IIF(RewardBrand NOT IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200', 'CENTURION'), Reward, 0)) AS Reward FROM RP2021Web_Rewards_W_PaymentOnTime WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'POT_W' AS RewardCode, 'Row_2' AS SubRowCode, MAX(IIF(RewardBrand IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200'), Reward_Percentage, 0)) AS Reward_Percentage, SUM(IIF(RewardBrand IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200'), Reward, 0)) AS Reward FROM RP2021Web_Rewards_W_PaymentOnTime WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'POT_W' AS RewardCode, 'Row_3' AS SubRowCode, MAX(IIF(RewardBrand = 'CENTURION', Reward_Percentage, 0)) AS Reward_Percentage, SUM(IIF(RewardBrand = 'CENTURION', Reward, 0)) AS Reward FROM RP2021Web_Rewards_W_PaymentOnTime WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_PAYMENT_ON_TIME'

		-- Custom Seed Treatment Sub Rows
		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[Qualifier] = ISNULL(T2.PartA_Reward_Percentage * 100, CASE @LEVEL5CODE WHEN 'D000001' THEN 16 ELSE 14 END)
			,[QualifierSuffix] = '%'
			,[Reward] = ISNULL(T2.PartA_Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardBrand_Percentage) [PartA_Reward_Percentage], SUM(Reward) [PartA_Reward]
				FROM RP2021Web_Rewards_W_CustomSeed
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand <> 'NODULATOR PRO 100'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode = 'Row_1'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[Qualifier] = ISNULL(T2.PartB_Reward_Percentage * 100, 8)
			,[QualifierSuffix] = '%'
			,[Reward] = ISNULL(T2.PartB_Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardBrand_Percentage) [PartB_Reward_Percentage], SUM(Reward) [PartB_Reward]
				FROM RP2021Web_Rewards_W_CustomSeed
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'NODULATOR PRO 100'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_W_CUSTOM_SEED_TREATING_REWARD' AND T1.SubRowCode = 'Row_2'

		-- NEW: RC-4475 and RRT-2204: Fungicide Support Reward
		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = 0.50
			,[QualifierSuffix] = '/Acre'
			,[Reward] = ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode
			LEFT JOIN (
				SELECT MarketLetterCode, 'FUNG_W' AS RewardCode, 'Row_1' AS SubRowCode, SUM(IIF(RewardBrand = 'CARAMBA', Reward, 0)) AS Reward FROM RP2021Web_Rewards_W_Fung_Support WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'FUNG_W' AS RewardCode, 'Row_2' AS SubRowCode, SUM(IIF(RewardBrand = 'COTEGRA', Reward, 0)) AS Reward FROM RP2021Web_Rewards_W_Fung_Support WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode AND T2.SubRowCode=T1.SubRowCode
		WHERE T1.RowType='Sub Row' AND T1.ProgramCode='RP2021_W_FUNG_SUPPORT'

		-- END OF SUB ROW UPDATES

		-- LETS UPDATE QUALIFIERS AT PROGRAM TOTAL ROW (NOTHING BUT REWARD PERCENTAGE AND REWARD AMOUNT)
		/*
			ACTIVE PROGRAMS - WEST

			Brand Specific Support						RP2021_W_BRAND_SPEC_SUPPORT				BSS_W				[dbo].[RP2021Web_Rewards_W_BrandSpecificSupport]
			Efficiency Bonus							RP2021_W_EFF_BONUS_REWARD				EBR_W				[dbo].[RP2021Web_Rewards_W_EFF_BONUS]
			InVigor & Liberty Loyalty Bonus				RP2021_W_INV_LIB_LOY_BONUS				INV_LLB_W			[dbo].[RP2021Web_Rewards_W_INV_LLB]
			InVigor Innovation Bonus					RP2021_W_INV_INNOVATION_BONUS			INV_INB_W			[dbo].[RP2021Web_Rewards_W_INV_INNOV]
			InVigor Performance							RP2021_W_INV_PERFORMANCE				INV_PER_W			[dbo].[RP2021Web_Rewards_W_INV_PERF]
			Warehouse Payment							RP2021_W_WAREHOUSE_PAYMENT				WH_PAYMENT_W		[dbo].[RP2021Web_Rewards_W_WarehousePayments]
			Payment on Time 							RP2021_W_PAYMENT_ON_TIME				POT_W				[dbo].[RP2021Web_Rewards_W_PaymentOnTime]
			Inventory Management						RP2021_W_INVENTORY_MGMT					INVENTORY_MGMT_W	[dbo].[RP2021Web_Rewards_W_InventoryManagement]
			Liberty Centurion Facet L Tank Mix Bonus	RP2021_W_TANK_MIX_BONUS					TM_BONUS_W			[dbo].[RP2021Web_Rewards_W_TankMixBonus]
			Planning & Supporting						RP2021_W_PLANNING_SUPP_BUSINESS			PSB_W				[dbo].[RP2021Web_Rewards_W_PlanningTheBusiness]
			Logistics Support							RP2021_W_LOGISTICS_SUPPORT				LOG_SUP_W			[dbo].[RP2021Web_Rewards_W_LogisticsSupport]
			Growth Bonus								RP2021_W_GROWTH_BONUS					GROWTH_W			[dbo].[RP2021Web_Rewards_W_GrowthBonus]
			Loyalty Bonus								RP2021_W_LOYALTY_BONUS					LOYALTY_W			[dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
			Segment Selling Bonus						RP2021_SEGMENT_SELLING					SEG_SELL_W			[dbo].[RP2021Web_Rewards_W_SegmentSelling]
			Retail Bonus Payment						RP2021_W_RETAIL_BONUS_PAYMENT			RET_BON_PAY_W		[dbo].[RP2021Web_Rewards_W_RetailBonus]
			Custom Seed Treatment						RP2021_W_CUSTOM_SEED_TREATING_REWARD	CST_W				[dbo].[RP2021Web_Rewards_W_CustomSeed]
			Clearfield Lentils Support					RP2021_W_CLL_SUPPORT					CLL_W				[dbo].[RP2021Web_Rewards_W_ClearfieldLentils]
			Dicamba Tolerant Program					RP2021_W_DICAMBA_TOLERANT				DITP_W				[dbo].[RP2021Web_Rewards_W_DicambaTolerant]
			Fungicide Support Reward					RP2021_W_FUNG_SUPPORT					FUNG_W				[dbo].[RP2021Web_Rewards_W_Fung_Support]
		*/

		UPDATE T1
		SET [RenderRow]='YES'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'BSS_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_BrandSpecificSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'EBR_W' AS RewardCode, MAX(Support_Reward_Percentage + Growth_Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward  FROM RP2021Web_Rewards_W_EFF_BONUS WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_LLB_W' AS RewardCode, (SELECT MAX(Percentages) FROM (VALUES (MAX(Bonus_Percentage)),(MAX(Addendum_Percentage))) Data(Percentages)) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_INV_LLB WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_INB_W', MAX(IIF(InVigor300_Reward > 0, InVigor300_Percentage, IIF(InVigor200_Reward > 0, InVigor200_Percentage, 0))) As Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_INV_INNOV WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INV_PER_W' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_INV_PERF WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'INVENTORY_MGMT_W', (SELECT MAX(Percentages) FROM (VALUES (MAX(CPP_Percentage)),(MAX(Centurion_Percentage)),(MAX(Liberty_Percentage))) Data(Percentages)) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_InventoryManagement WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'WH_PAYMENT_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_WarehousePayments WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'POT_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_PaymentOnTime WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_W', (SELECT MAX(Percentages) FROM (VALUES (MAX(CPP_Reward_Percentage)),(MAX(Lib_Reward_Percentage))) Data(Percentages)) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOG_SUP_W', MAX(Segment_Reward_Percentage) As Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_LogisticsSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'GROWTH_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_GrowthBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'LOYALTY_W', (SELECT MAX(Percentages) FROM (VALUES (MAX(SegmentA_Reward_Percentage)),(MAX(SegmentB_Reward_Percentage))) Data(Percentages)) As Reward_Percentage, SUM(SegmentA_Reward + SegmentB_Reward) As Reward FROM RP2021Web_Rewards_W_LoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SEG_SELL_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_SegmentSelling WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'RET_BON_PAY_W', (SELECT MAX(Percentages) FROM (VALUES (MAX(Lib_Reward_Percentage)),(MAX(Cent_Reward_Percentage))) Data(Percentages)) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_RetailBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'TM_BONUS_W', 0 AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_TankMixBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CST_W', MAX(RewardBrand_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_CustomSeed WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CLL_W', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_ClearfieldLentils WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'FUNG_W', 0 AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_W_Fung_Support WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PSB_E', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_E_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PORT_SUP_E', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_E_PortfolioSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CGAS_E', 0 As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_CustomGroundApplication WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PURS_SUP_E', 0 As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_PursuitSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SUPPLY_SALES_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_SupplySales WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'POG_SALES_BONUS_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_POGSalesBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SS_LOYALTY_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_SalesSupportLoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL'
	END
	ELSE
	BEGIN
		-- East Rewards Sub Rows

		-- Custom Ground Application
		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = 1.00
			,[QualifierSuffix] = '/Acre'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_CustomGroundApplication
				WHERE StatementCode = @STATEMENTCODE AND GroupLabel = 'REWARD GROUP 1'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND T1.SubRowCode = 'Row_1'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = 0.50
			,[QualifierSuffix] = '/Acre'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_CustomGroundApplication
				WHERE StatementCode = @STATEMENTCODE AND GroupLabel = 'REWARD GROUP 2'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND T1.SubRowCode = 'Row_2'

		-- Pursuit Support Reward

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'PURSUIT'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_1'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'FRONTIER'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_2'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'HEADLINE'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_3'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'PRIAXOR'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_4'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'CONQUEST LQ'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_5'

		UPDATE T1
		SET [RenderRow] = 'YES'
			,[RowLabel] = T1.Label
			,[QualifierPrefix] = '$'
			,[Qualifier] = ISNULL(T2.RewardPerCase, 0)
			,[QualifierSuffix] = '/Case'
			,[Reward] = ISNULL(T2.Reward, 0)
		FROM @RewardsDisplayData T1
			CROSS JOIN (
				SELECT MAX(RewardPerCase) [RewardPerCase], SUM(Reward) [Reward]
				FROM RP2021Web_Rewards_E_PursuitSupport
				WHERE StatementCode = @STATEMENTCODE AND RewardBrand = 'FORUM'
			) T2
		WHERE T1.RowType = 'Sub Row' AND T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND T1.SubRowCode = 'Row_6'

		/*
			ACTIVE PROGRAMS - EAST

			Planning the Business						RP2021_E_PLANNING_SUPP_BUSINESS			PSB_E				[dbo].[RP2021Web_Rewards_E_PlanningTheBusiness]
			Portfolio Support							RP2021_E_PORTFOLIO_SUPPORT				PORT_SUP_E			[dbo].[RP2021Web_Rewards_E_PortfolioSupport]
			Custom Ground Application					RP2021_E_CUSTOM_GROUND_APP_REWARD		CGAS_E				[dbo].[RP2021Web_Rewards_E_CustomGroundApplication]
			Pursuit Support								RP2021_E_PURSUIT_REWARD					PURS_SUP_E			[dbo].[RP2021Web_Rewards_E_PursuitSupport]
			Fungicides Support							RP2021_E_FUNGICIDES_SUPPORT				FUNG_SUP_E			[dbo].[RP2021Web_Rewards_E_FungicidesSupport]
			Supply of Sales Results						RP2021_E_SUPPLY_SALES					SUPPLY_SALES_E		[dbo].[RP2021Web_Rewards_E_SupplySales]
			POG Sales Bonus								RP2021_E_POG_SALES_BONUS				POG_SALES_BONUS_E	[dbo].[RP2021Web_Rewards_E_POGSalesBonus]
			Sales Support - Loyalty Bonus				RP2021_E_SALES_SUP_LOYALTY_BONUS		SS_LOYALTY_E		[dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
		*/

		UPDATE T1
		SET [RenderRow]='YES'				
			,[Qualifier]=ISNULL(T2.Reward_Percentage * 100,0)
			,[QualifierSuffix]='%'
			,[Reward]=ISNULL(T2.Reward,0)
		FROM @RewardsDisplayData T1
			INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode AND ML.ProgramCode=T1.ProgramCode		
			LEFT JOIN (
				SELECT MarketLetterCode, 'PSB_E' AS RewardCode, MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_E_PlanningTheBusiness WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PORT_SUP_E', MAX(Reward_Percentage) AS Reward_Percentage, SUM(Reward) AS Reward FROM RP2021Web_Rewards_E_PortfolioSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'CGAS_E', 0 As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_CustomGroundApplication WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'PURS_SUP_E', 0 As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_PursuitSupport WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SUPPLY_SALES_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_SupplySales WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'POG_SALES_BONUS_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_POGSalesBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
					UNION
				SELECT MarketLetterCode, 'SS_LOYALTY_E', MAX(Reward_Percentage) As Reward_Percentage, SUM(Reward) As Reward FROM RP2021Web_Rewards_E_SalesSupportLoyaltyBonus WHERE STATEMENTCODE=@STATEMENTCODE GROUP BY [StatementCode],[MarketLetterCode]
			) T2
			ON T2.MarketLetterCode=ML.MarketLetterCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.RowType='PROGRAM TOTAL'
	END

	-- Other line items that don't use percentages as qualifiers

	-- Dicamba Tolerant Program
	UPDATE T1
	SET [RenderRow]='YES'
		,[QualifierPrefix]='$'
		,[Qualifier]=2.00
		,[QualifierSuffix]='/Acre'
		,[Reward]=RS.Reward
	FROM @RewardsDisplayData T1
	INNER JOIN (
		SELECT DISTINCT MarketLetterCode FROM  @TBL_MarketLetterCodes
	) ML	
	ON ML.MarketLetterCode=T1.MarketLetterCode
	INNER JOIN RP2021Web_Rewards_W_DicambaTolerant RS
	ON RS.StatementCode = @STATEMENTCODE		
	WHERE T1.RowType='Program Total' AND T1.RewardCode = 'DITP_W'

	-- Fungicides Support Reward
	UPDATE T1
	SET [RenderRow]='YES'
		,[QualifierPrefix]='$'
		,[Qualifier]=RS.RewardPerAcre
		,[QualifierSuffix]='/Acre'
		,[Reward]=RS.Reward
	FROM @RewardsDisplayData T1
	INNER JOIN (
		SELECT DISTINCT MarketLetterCode FROM  @TBL_MarketLetterCodes
	) ML	
	ON ML.MarketLetterCode=T1.MarketLetterCode
	INNER JOIN (
		SELECT StatementCode,
			   MAX(RewardPerAcre) [RewardPerAcre],
			   SUM(Reward) [Reward]
		FROM RP2021Web_Rewards_E_FungicidesSupport
		GROUP BY StatementCode
	) RS
	ON RS.StatementCode = @STATEMENTCODE		
	WHERE T1.RowType='Program Total' AND T1.RewardCode = 'FUNG_SUP_E'
	
	UPDATE T1
	SET [RenderRow]='YES'
		,[RowLabel]=T1.Label
	FROM @RewardsDisplayData T1
		INNER JOIN @TBL_MarketLetterCodes ML	ON ML.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.RowType='DISCLAIMER'
	
	RETURN
END
