﻿


CREATE FUNCTION [dbo].[TVF_Get_NonIndependent_Scenarios]  (@SEASON INT, @CUT_OFF_DATE DATE, @STATEMENTS AS UDT_RP_STATEMENT READONLY)

RETURNS @T TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,		
		[ScenarioCode] [varchar](20) NOT NULL,
		[ScenarioStatus] [varchar](20) NOT NULL,
		[DateModified] [DATE] NOT NULL,
		PRIMARY KEY CLUSTERED (
			[Statementcode] ASC,
			[RetailerCode] ASC,			
			[ScenarioCode] ASC
		)
)
AS
BEGIN
	
	INSERT INTO @T(Statementcode, Retailercode, Scenariocode, ScenarioStatus, DateModified)
	SELECT ST.Statementcode, Map.ActiveRetailerCode, POG_Plan.ScenarioID, POG_Plan.ScenarioStatus, POG_Plan.DateModified
	FROM @STATEMENTS ST
		INNER JOIN RP_StatementsMappings Map
		ON Map.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT SCN.ScenarioID, SCN.RetailerID AS RetailerCode, SCN.[Status] AS ScenarioStatus,  convert(date,SCN.Created) AS [DateModified]
			FROM (
				SELECT lss.*,
					ROW_NUMBER() OVER (
							PARTITION BY lss.retailerid
							ORDER BY CASE lss.[status] WHEN 'Final' THEN 1 WHEN 'Preferred' THEN 2 WHEN 'Draft' THEN 3 END, lss.created DESC) plan_rnk
				FROM Log_ScenarioStatus lss
					INNER JOIN Retail_Plan_Scenarios rps 
					ON rps.ScenarioID = lss.ScenarioID
				WHERE    lss.[Status] IN ('Preferred', 'Final','Draft')
					AND    lss.season=@SEASON
					AND    rps.[Status]<>'Deleted'
					AND    (rps.Author != 'SYSTEM' AND rps.[Name] != 'SYSTEM-CREATED') 
					AND    convert(date,lss.Created) <= CAST(@CUT_OFF_DATE AS DATE)
			) SCN
			WHERE plan_rnk=1		
		) POG_Plan
		ON POG_Plan.RetailerCode=Map.ActiveRetailerCode		

	RETURN
END
