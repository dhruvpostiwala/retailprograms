﻿
CREATE FUNCTION [dbo].[TVF_Get_StatementCodesToRefresh] (@STATEMENTS AS UDT_RP_STATEMENT READONLY)

RETURNS @T TABLE(
	Season INT,
	RetailerCode Varchar(20),
	StatementCode INT,		
	StatementLevel Varchar(20),
	DataSetCode INT,
	DataSetCode_L5 INT,
	Summary Varchar(3) NOT NULL DEFAULT 'No'
)
AS
BEGIN
	DECLARE @EAST_LEVEL2_STATEMENTS TABLE (Statementcode INT)
	DECLARE @EAST_LEVEL1_DATASETCODES TABLE (DataSetCode INT) -- LEVEL 1s BELONGING TO A FAMILY
	   	 
	-- WEST STATEMENTCODES
	INSERT INTO @T (RS.Season, StatementCode,StatementLevel,RetailerCode,DataSetCode,DataSetCode_L5)
	SELECT RS.Season, ST.StatementCode, RS.StatementLevel, RS.RetailerCode, RS.DataSetCode, RS.DataSetCode_L5 
	FROM @STATEMENTS ST	
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode				
	WHERE RS.Region='WEST' AND RS.[Status]='Active'
	

	-- LETS WORK WITH EAST STATEMENTCODES
	-- COLLECT EAST LEVEL 2 STATEMENTCODES
	INSERT INTO @EAST_LEVEL2_STATEMENTS(StatementCode)
	SELECT ST.StatementCode
	FROM @STATEMENTS ST	
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode		
	WHERE RS.Region='EAST' AND RS.[StatementLevel]='LEVEL 2' AND RS.[Status]='Active'

	-- COLLECT EAST LEVEL 1 DATASETCODES
	INSERT INTO @EAST_LEVEL1_DATASETCODES(DataSetCode)
	SELECT DISTINCT RS.DataSetCode
	FROM @STATEMENTS ST	
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode		
	WHERE RS.Region='EAST' AND RS.[StatementLevel]='LEVEL 1' AND [DataSetCode] > 0 AND RS.[Status]='Active'
	
	INSERT INTO @T (Season, StatementCode,StatementLevel,RetailerCode,DataSetCode,DataSetCode_L5)
	SELECT DISTINCT Season, StatementCode,StatementLevel,RetailerCode,DataSetCode,DataSetCode_L5
	FROM (	
		-- ALL PASSED IN STATEMENT CODES
		SELECT RS.Season, RS.StatementCode, RS.StatementLevel, RS.RetailerCode, RS.DataSetCode, RS.DataSetCode_L5
		FROM RP_Statements RS
			INNER JOIN @STATEMENTS ST ON RS.StatementCode=ST.StatementCode					
		WHERE RS.Region='EAST' AND RS.[Status]='Active'

			UNION	

		-- WHEN A LEVEL 2 STATEMENT CODE IS PASSED IN
		-- INCLUDE ALL LEVEL 1 STATEMENTCODE FROM THE FAMILY
		SELECT RS.Season, RS.StatementCode, RS.StatementLevel, RS.RetailerCode, RS.DataSetCode, RS.DataSetCode_L5
		FROM RP_Statements RS
			INNER JOIN @EAST_LEVEL2_STATEMENTS L2 	ON RS.DataSetCode=L2.StatementCode
		WHERE RS.StatementLevel='LEVEL 1' AND RS.[Status]='Active'
		
			UNION

		-- WHEN A LEVEL 1 STATEMENT CODE FROM A FAMILY IS PASSED IN
			-- #1 INCLUDE ALL LEVEL 1 STATEMENTCODES FROM THE FAMILY
		SELECT RS.Season, RS.StatementCode, RS.StatementLevel, RS.RetailerCode, RS.DataSetCode, RS.DataSetCode_L5
		FROM RP_Statements RS		
			INNER JOIN @EAST_LEVEL1_DATASETCODES L1 	ON RS.DataSetCode=L1.DataSetCode
		WHERE RS.StatementLevel='LEVEL 1' AND RS.[Status]='Active'

			UNION
		
			-- #2 INCLUDE LEVEL2 STATEMENTCODE
		SELECT RS.Season, RS.StatementCode, RS.StatementLevel, RS.RetailerCode, RS.DataSetCode, RS.DataSetCode_L5
		FROM RP_Statements RS		
			INNER JOIN @EAST_LEVEL1_DATASETCODES L1 	ON RS.Statementcode=L1.DataSetCode 
		WHERE RS.[StatementLevel]='LEVEL 2'  AND RS.[Status]='Active'
			   
	) D
	
	UPDATE T1
	SET Summary=HO.Summary
	FROM @T T1
		INNER JOIN RP_Config_HOPaymentLevel HO
		ON HO.Level5Code=T1.RetailerCode AND HO.Season=T1.Season


	RETURN
END
