﻿

-- =============================================
-- Author:		DERMAREY BAKER,
-- Create date: APRIL 3 2020
-- Description:	Display values if negative
--     CONVERT NEGATIVE VALUES WITH BRACKETS AROUND THEM
-- accept float value and ismoney parameters
--and returns varchar 
-- =============================================
CREATE FUNCTION [dbo].[DisplayValuesFinancially]
(
	-- Add the parameters for the function here
	@Value FLOAT,
	@IsMoney BIT
)
RETURNS VARCHAR(50)
AS
BEGIN

	DECLARE @Result VARCHAR(50)

	--Incase value was a quntity then dollar sign is not needed
	DECLARE @DollarSign VARCHAR(1) = '';
	SELECT @DollarSign = IIF(@IsMoney = 1,'$','')
	
	-- Add the T-SQL statements to compute the return value here

	 SELECT @Result =  
		IIF( @VALUE  < 0 
			,'('+ @DollarSign + convert(varchar(50),CAST(CAST(REPLACE(ISNULL(@VALUE,0), '-','') AS FLOAT) AS MONEY), -1) + ')'
			,@DollarSign + convert(varchar(50),CAST(ISNULL(@VALUE,0) AS MONEY),-1) 
		   )
	-- Return the result of the function

	RETURN  @Result

END
