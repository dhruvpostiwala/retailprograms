﻿CREATE TABLE [dbo].[RPWeb_Attachments] (
    [DocCode]       VARCHAR (50)  NULL,
    [ParentDocCode] VARCHAR (50)  NULL,
    [Category]      VARCHAR (50)  NULL,
    [FileName]      VARCHAR (200) NULL,
    [FileType]      VARCHAR (10)  NULL,
    [FileSize]      FLOAT (53)    NULL
);

