﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_Fung_Support] (
    [StatementCode]       INT             NOT NULL,
    [MarketLetterCode]    VARCHAR (50)    NOT NULL,
    [ProgramCode]         VARCHAR (50)    NOT NULL,
    [RewardBrand]         VARCHAR (100)   NOT NULL,
    [ProductName]         VARCHAR (255)   NOT NULL,
    [ProductCode]         VARCHAR (50)    NOT NULL,
    [RewardBrand_QTY]     NUMERIC (18, 2) DEFAULT ((0)) NULL,
    [Reward_Per_Quantity] INT             DEFAULT ((0)) NULL,
    [Reward]              MONEY           DEFAULT ((0)) NOT NULL,
    [RewardBrand_Sales]   MONEY           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC, [ProductName] ASC, [ProductCode] ASC)
);

