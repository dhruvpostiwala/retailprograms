﻿CREATE TABLE [dbo].[RPWeb_Reports_Exposure] (
    [ExposureID] INT      IDENTITY (1, 1) NOT NULL,
    [Season]     INT      NOT NULL,
    [ReportDate] DATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([ExposureID] ASC)
);

