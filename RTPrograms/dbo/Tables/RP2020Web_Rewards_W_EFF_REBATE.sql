﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_EFF_REBATE] (
    [Statementcode]             INT            NOT NULL,
    [MarketLetterCode]          VARCHAR (50)   NOT NULL,
    [ProgramCode]               VARCHAR (50)   NOT NULL,
    [GroupLabel]                VARCHAR (100)  NOT NULL,
    [Support_Reward_Sales]      MONEY          NOT NULL,
    [Support_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Support_Reward]            MONEY          NOT NULL,
    [Growth_Reward_Percentage]  DECIMAL (6, 4) NOT NULL,
    [Growth_Reward]             MONEY          NOT NULL,
    [Reward]                    MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [GroupLabel] ASC)
);

