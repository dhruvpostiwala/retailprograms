﻿CREATE TABLE [dbo].[RP_Config_Rewards_Summary] (
    [Season]            INT           NOT NULL,
    [RewardCode]        VARCHAR (50)  NOT NULL,
    [RewardLabel]       VARCHAR (100) NOT NULL,
    [RewardLabelFrench] VARCHAR (200) NULL,
    [ExposureReport]    VARCHAR (3)   NOT NULL,
    [Accrual]           FLOAT (53)    NULL,
    [ShowRewardMargin]  VARCHAR (3)   NULL,
    [Region]            VARCHAR (4)   NULL,
    [RewardType]        VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [RewardCode] ASC)
);

