﻿CREATE TABLE [dbo].[RP_Config_Groups_ProductSKUs] (
    [GroupID]     VARCHAR (50) NOT NULL,
    [Productcode] VARCHAR (65) NOT NULL,
    PRIMARY KEY CLUSTERED ([GroupID] ASC, [Productcode] ASC)
);

