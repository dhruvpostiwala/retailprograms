﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_PlanningTheBusiness] (
    [Statementcode]            INT            NOT NULL,
    [MarketLetterCode]         VARCHAR (50)   NOT NULL,
    [ProgramCode]              VARCHAR (50)   NOT NULL,
    [GroupLabel]               VARCHAR (100)  NOT NULL,
    [Growth_QualSales_CY]      MONEY          NOT NULL,
    [Growth_QualSales_LY]      MONEY          NOT NULL,
    [Growth_Reward_Sales]      MONEY          NOT NULL,
    [Growth_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Growth_Reward]            MONEY          NOT NULL,
    [Lib_Reward_Sales]         MONEY          NOT NULL,
    [Lib_Reward_Percentage]    DECIMAL (6, 4) NOT NULL,
    [Lib_Reward]               MONEY          NOT NULL,
    [Cent_Reward_Sales]        MONEY          NOT NULL,
    [Cent_Reward_Percentage]   DECIMAL (6, 4) NOT NULL,
    [Cent_Reward]              MONEY          NOT NULL,
    [Reward]                   MONEY          NOT NULL,
    [Early_Order_Percentage]   DECIMAL (6, 4) DEFAULT ((0)) NULL,
    [Take_Delivery_Percentage] DECIMAL (6, 4) DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [GroupLabel] ASC)
);

