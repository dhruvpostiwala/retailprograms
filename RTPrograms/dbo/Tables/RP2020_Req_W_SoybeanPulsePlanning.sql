﻿CREATE TABLE [dbo].[RP2020_Req_W_SoybeanPulsePlanning] (
    [Statementcode] INT   NOT NULL,
    [Plan_Date]     DATE  NULL,
    [Plan_Amount]   MONEY NOT NULL,
    [Order_Amount]  MONEY NOT NULL,
    [Plan_Passed]   BIT   NOT NULL,
    [Orders_Passed] BIT   NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC)
);

