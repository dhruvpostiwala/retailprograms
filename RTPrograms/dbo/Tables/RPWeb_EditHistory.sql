﻿CREATE TABLE [dbo].[RPWeb_EditHistory] (
    [EHCode]   INT           IDENTITY (1, 1) NOT NULL,
    [EHType]   VARCHAR (50)  NOT NULL,
    [EHDate]   DATETIME      NOT NULL,
    [UserName] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([EHCode] ASC)
);

