﻿CREATE TABLE [dbo].[RP2022Web_Rewards_W_BaseReward_CPP] (
    [Statementcode]     INT            NOT NULL,
    [MarketLetterCode]  VARCHAR (50)   NOT NULL,
    [ProgramCode]       VARCHAR (50)   NOT NULL,
    [Level5Code]        VARCHAR (15)   NOT NULL,
    [RewardSegment]     VARCHAR (100)  NOT NULL,
    [RewardBrand]       VARCHAR (100)  NOT NULL,
    [RewardBrand_Sales] MONEY          NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Reward]            MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [Level5Code] ASC, [RewardSegment] ASC, [RewardBrand] ASC)
);

