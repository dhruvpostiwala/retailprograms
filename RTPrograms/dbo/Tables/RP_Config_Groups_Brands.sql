﻿CREATE TABLE [dbo].[RP_Config_Groups_Brands] (
    [GroupID]       INT          NOT NULL,
    [ChemicalGroup] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([GroupID] ASC, [ChemicalGroup] ASC)
);

