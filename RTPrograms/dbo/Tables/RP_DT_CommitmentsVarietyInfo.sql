﻿CREATE TABLE [dbo].[RP_DT_CommitmentsVarietyInfo] (
    [Statementcode]    INT            NOT NULL,
    [RP_WC_ID]         INT            NOT NULL,
    [RP_ID]            INT            NOT NULL,
    [DataSetCode]      INT            NOT NULL,
    [ID]               INT            NOT NULL,
    [BASFSeason]       INT            NOT NULL,
    [ProductCode]      VARCHAR (50)   NOT NULL,
    [Acres]            NUMERIC (8, 2) NOT NULL,
    [RegistrationDate] DATE           NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [RP_WC_ID] ASC, [RP_ID] ASC)
);

