﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_ClearfieldLentils_Actual] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [Retailercode]      VARCHAR (20)    NOT NULL,
    [Farmcode]          VARCHAR (20)    NOT NULL,
    [RewardBrand]       VARCHAR (100)   NOT NULL,
    [MatchingOrder]     INT             NOT NULL,
    [Acres]             DECIMAL (18, 2) NOT NULL,
    [Sales]             DECIMAL (18, 2) NOT NULL,
    [MatchedAcres]      DECIMAL (18, 2) NOT NULL,
    [MatchedSales]      DECIMAL (18, 2) NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4)  NOT NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [Retailercode] ASC, [Farmcode] ASC, [RewardBrand] ASC)
);

