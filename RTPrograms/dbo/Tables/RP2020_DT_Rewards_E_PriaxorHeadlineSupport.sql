﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_E_PriaxorHeadlineSupport] (
    [Statementcode]          INT             NOT NULL,
    [MarketLetterCode]       VARCHAR (50)    NOT NULL,
    [ProgramCode]            VARCHAR (50)    NOT NULL,
    [Priaxor_Acres]          NUMERIC (18, 2) NOT NULL,
    [Priaxor_Sales]          MONEY           NOT NULL,
    [Priaxor_Reward_Amount]  MONEY           NOT NULL,
    [Priaxor_Reward]         MONEY           NOT NULL,
    [Headline_POG_Acres]     NUMERIC (18, 2) NOT NULL,
    [Headline_Acres]         NUMERIC (18, 2) NOT NULL,
    [Headline_Sales]         MONEY           NOT NULL,
    [Headline_Reward_Amount] MONEY           NOT NULL,
    [Headline_Reward]        MONEY           NOT NULL,
    [Reward]                 MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

