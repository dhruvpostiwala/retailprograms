﻿CREATE TABLE [dbo].[RP_Config_Groups_Brands_MP_BKP] (
    [GroupID]       VARCHAR (50) NOT NULL,
    [ChemicalGroup] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([GroupID] ASC, [ChemicalGroup] ASC)
);

