﻿CREATE TABLE [dbo].[RP2022Web_Rewards_W_TankMixBonus] (
    [StatementCode]           INT             NOT NULL,
    [MarketLetterCode]        VARCHAR (50)    NOT NULL,
    [ProgramCode]             VARCHAR (50)    NOT NULL,
    [RetailerCode]            VARCHAR (20)    NOT NULL,
    [FarmCode]                VARCHAR (20)    NOT NULL,
    [Liberty_Acres]           NUMERIC (18, 2) NOT NULL,
    [Centurion_Acres]         NUMERIC (18, 2) NOT NULL,
    [Facet_L_Acres]           NUMERIC (18, 2) NOT NULL,
    [Lib_Cent_Matched_Acres]  NUMERIC (18, 2) NULL,
    [Lib_Facet_Matched_Acres] NUMERIC (18, 2) NULL,
    [Lib_Cent_Reward]         MONEY           NOT NULL,
    [Lib_Facet_Reward]        MONEY           NOT NULL,
    [Reward]                  MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

