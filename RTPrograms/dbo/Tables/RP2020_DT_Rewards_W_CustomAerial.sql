﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_CustomAerial] (
    [StatementCode]            INT             NOT NULL,
    [MarketLetterCode]         VARCHAR (50)    NOT NULL,
    [ProgramCode]              VARCHAR (50)    NOT NULL,
    [ProductName]              VARCHAR (100)   NOT NULL,
    [ProductCode]              VARCHAR (50)    NOT NULL,
    [RewardBrand]              VARCHAR (50)    NOT NULL,
    [RewardBrand_Sales]        MONEY           NOT NULL,
    [Product_Quantity]         NUMERIC (18, 2) NOT NULL,
    [Product_Sales]            MONEY           NOT NULL,
    [Product_CustApp_Quantity] NUMERIC (18, 2) NOT NULL,
    [Product_CustApp_Dollars]  MONEY           NOT NULL,
    [Elig_Dollars]             MONEY           NOT NULL,
    [Reward_Percentage]        DECIMAL (6, 5)  NOT NULL,
    [Reward]                   MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [ProductCode] ASC)
);

