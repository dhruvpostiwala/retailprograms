﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_E_HortiCultureRetailSupport] (
    [Statementcode]           INT            NOT NULL,
    [MarketLetterCode]        VARCHAR (50)   NOT NULL,
    [ProgramCode]             VARCHAR (50)   NOT NULL,
    [RewardBrand_ElgSales_CY] MONEY          DEFAULT ((0)) NOT NULL,
    [RewardBrand_ActSales_CY] MONEY          DEFAULT ((0)) NOT NULL,
    [RewardBrand_ActSales_LY] MONEY          DEFAULT ((0)) NOT NULL,
    [total_basf_sale]         MONEY          DEFAULT ((0)) NOT NULL,
    [Reward_Percentage]       DECIMAL (6, 4) NOT NULL,
    [Reward]                  MONEY          DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

