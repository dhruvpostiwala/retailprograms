﻿CREATE TABLE [dbo].[RPWeb_Est_MarginTopUp] (
    [StatementCode]     INT        NOT NULL,
    [SRC_StatementCode] INT        NOT NULL,
    [Percentage]        FLOAT (53) DEFAULT ((0)) NOT NULL,
    [Amount]            MONEY      DEFAULT ((0)) NOT NULL,
    [DateCreated]       DATETIME   DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME   DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

