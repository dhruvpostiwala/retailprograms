﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_InventoryManagement] (
    [Statementcode]             INT            NOT NULL,
    [MarketLetterCode]          VARCHAR (50)   NOT NULL,
    [ProgramCode]               VARCHAR (50)   NOT NULL,
    [RewardBrand]               VARCHAR (100)  NOT NULL,
    [Reward]                    MONEY          NOT NULL,
    [CPP_Reward]                MONEY          NOT NULL,
    [Centurion_Reward]          MONEY          NOT NULL,
    [CPP_Percentage]            DECIMAL (6, 4) NOT NULL,
    [Liberty_Percentage]        DECIMAL (6, 4) NOT NULL,
    [Centurion_Percentage]      DECIMAL (6, 4) NOT NULL,
    [CPP_Sales]                 MONEY          NOT NULL,
    [Liberty_Sales]             MONEY          NOT NULL,
    [Centurion_Sales]           MONEY          NOT NULL,
    [Liberty_Reward]            MONEY          NOT NULL,
    [InVigor_Sales]             MONEY          NULL,
    [InVigor_Reward_Percentage] DECIMAL (6, 4) NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

