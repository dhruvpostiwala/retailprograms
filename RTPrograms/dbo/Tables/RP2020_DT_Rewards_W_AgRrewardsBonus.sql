﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_AgRrewardsBonus] (
    [StatementCode]                    INT          NOT NULL,
    [MarketLetterCode]                 VARCHAR (50) NOT NULL,
    [ProgramCode]                      VARCHAR (50) NOT NULL,
    [RewardBrand_Sales]                MONEY        NOT NULL,
    [RetailerCode]                     VARCHAR (20) NOT NULL,
    [FarmCode]                         VARCHAR (20) NOT NULL,
    [Leads_Closed_Sales]               MONEY        NOT NULL,
    [Leads_Closed_Inv_Bonus_Reward]    MONEY        NOT NULL,
    [Leads_Closed_Two_Seg_Reward]      MONEY        NOT NULL,
    [Leads_Closed_Three_Seg_Reward]    MONEY        NOT NULL,
    [Leads_Closed_Reward]              MONEY        NOT NULL,
    [Leads_NotClosed_Sales]            MONEY        NOT NULL,
    [Leads_NotClosed_Inv_Bonus_Reward] MONEY        NOT NULL,
    [Leads_NotClosed_Two_Seg_Reward]   MONEY        NOT NULL,
    [Leads_NotClosed_Three_Seg_Reward] MONEY        NOT NULL,
    [Leads_NotClosed_Reward]           MONEY        NOT NULL,
    [Reward]                           MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

