﻿CREATE TABLE [dbo].[RPWeb_User_BrandInputs] (
    [StatementCode]   INT            NOT NULL,
    [ChemicalGroup]   VARCHAR (50)   NOT NULL,
    [Category]        VARCHAR (50)   NULL,
    [PercentageValue] NUMERIC (8, 4) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [ChemicalGroup] ASC)
);

