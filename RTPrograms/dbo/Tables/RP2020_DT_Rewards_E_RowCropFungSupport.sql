﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_E_RowCropFungSupport] (
    [Statementcode]      INT             NOT NULL,
    [MarketLetterCode]   VARCHAR (50)    NOT NULL,
    [ProgramCode]        VARCHAR (50)    NOT NULL,
    [RewardBrand]        VARCHAR (100)   NOT NULL,
    [RewardBrand_Acres]  NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Sales]  MONEY           NOT NULL,
    [QualBrand_Acres_CY] NUMERIC (18, 2) NOT NULL,
    [QualBrand_Acres_LY] NUMERIC (18, 2) NOT NULL,
    [QualBrand_Sales_CY] NUMERIC (18, 2) NOT NULL,
    [QualBrand_Sales_LY] NUMERIC (18, 2) NOT NULL,
    [Reward_Amount]      MONEY           NOT NULL,
    [Reward]             MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

