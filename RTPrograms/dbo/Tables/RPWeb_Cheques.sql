﻿CREATE TABLE [dbo].[RPWeb_Cheques] (
    [ChequeDocCode] VARCHAR (25)  NOT NULL,
    [StatementCode] INT           NOT NULL,
    [RetailerCode]  VARCHAR (20)  NOT NULL,
    [VersionType]   VARCHAR (25)  NOT NULL,
    [PayableToName] VARCHAR (150) NULL,
    [ChequeType]    VARCHAR (25)  NOT NULL,
    [ChequeNumber]  VARCHAR (10)  NULL,
    [ChequeDate]    DATETIME      NULL,
    [ChequeStatus]  VARCHAR (50)  NOT NULL,
    [SendTo]        VARCHAR (75)  NULL,
    [AmtRequested]  MONEY         NOT NULL,
    [AmtReceived]   MONEY         NOT NULL,
    [AmtPaid]       MONEY         NOT NULL,
    [PDFJobID]      INT           NULL,
    [PDFDocCode]    VARCHAR (50)  NULL,
    [ChequeAmount]  MONEY         NULL,
    [InsertedDate]  DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_GPWeb_Cheques] PRIMARY KEY CLUSTERED ([ChequeDocCode] ASC, [StatementCode] ASC)
);

