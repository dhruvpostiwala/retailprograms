﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_Adv_Weed_Control] (
    [StatementCode]    INT             NOT NULL,
    [MarketLetterCode] VARCHAR (50)    NOT NULL,
    [ProgramCode]      VARCHAR (50)    NOT NULL,
    [FarmCode]         VARCHAR (50)    NOT NULL,
    [IMI_Acres]        NUMERIC (18, 2) NOT NULL,
    [Heat_Acres]       NUMERIC (18, 2) NOT NULL,
    [Matched_Acres]    NUMERIC (18, 2) NOT NULL,
    [Reward_Per_Acre]  NUMERIC (5, 2)  NOT NULL,
    [Reward]           MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [FarmCode] ASC)
);

