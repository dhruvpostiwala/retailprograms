﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_EFF_BONUS_QUAL] (
    [Statementcode]       INT             NOT NULL,
    [MarketLetterCode]    VARCHAR (50)    NOT NULL,
    [ProgramCode]         VARCHAR (50)    NOT NULL,
    [All_QualSales_CY]    MONEY           NOT NULL,
    [All_QualSales_LY]    MONEY           NOT NULL,
    [Growth_QualSales_CY] MONEY           NOT NULL,
    [Growth_QualSales_LY] MONEY           NOT NULL,
    [Growth]              NUMERIC (19, 4) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

