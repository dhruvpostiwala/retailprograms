﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_ClearfieldLentils] (
    [StatementCode]               INT             NOT NULL,
    [MarketLetterCode]            VARCHAR (50)    NOT NULL,
    [ProgramCode]                 VARCHAR (50)    NOT NULL,
    [RetailerCode]                VARCHAR (20)    NOT NULL,
    [FarmCode]                    VARCHAR (20)    NOT NULL,
    [RewardBrand]                 VARCHAR (100)   NOT NULL,
    [Commitment_Early]            INT             NOT NULL,
    [Commitment_Early_Amount]     MONEY           NOT NULL,
    [Commitment_Early_Reward]     MONEY           NOT NULL,
    [Commitment_Late]             INT             NOT NULL,
    [Commitment_Late_Amount]      INT             NOT NULL,
    [Commitment_Late_Reward]      MONEY           NOT NULL,
    [Herbicide_Lentil_Acres]      NUMERIC (18, 2) NOT NULL,
    [Herbicide_Brand_Acres]       NUMERIC (18, 2) NOT NULL,
    [Herbicide_Brand_Sales]       MONEY           NOT NULL,
    [Herbicide_Match_Acres]       NUMERIC (18, 2) NOT NULL,
    [Herbicide_Match_Sales]       MONEY           NOT NULL,
    [Herbicide_Reward_Percentage] DECIMAL (18, 2) NOT NULL,
    [Herbicide_Reward]            MONEY           NOT NULL,
    [Reward]                      MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC, [RewardBrand] ASC)
);

