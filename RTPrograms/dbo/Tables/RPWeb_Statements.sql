﻿CREATE TABLE [dbo].[RPWeb_Statements] (
    [StatementCode]        INT              IDENTITY (-1, -1) NOT NULL,
    [StatementType]        VARCHAR (20)     NOT NULL,
    [VersionType]          VARCHAR (20)     NOT NULL,
    [StatementLevel]       VARCHAR (20)     NOT NULL,
    [Status]               VARCHAR (20)     NOT NULL,
    [Season]               VARCHAR (4)      NOT NULL,
    [Region]               VARCHAR (4)      NULL,
    [RetailerCode]         VARCHAR (20)     NOT NULL,
    [RetailerType]         VARCHAR (20)     NOT NULL,
    [Level2Code]           VARCHAR (20)     NOT NULL,
    [Level5Code]           VARCHAR (20)     NOT NULL,
    [ActiveRetailerCode]   VARCHAR (20)     NULL,
    [DataSetCode]          INT              NULL,
    [Lock_Job_ID]          UNIQUEIDENTIFIER NULL,
    [SRC_StatementCode]    INT              NULL,
    [UC_Job_ID]            UNIQUEIDENTIFIER NULL,
    [UC_SRC_StatementCode] INT              NULL,
    [Deleted_Date]         DATE             NULL,
    [Locked_Date]          DATE             NULL,
    [DataSetCode_L5]       INT              DEFAULT ((0)) NULL,
    [IsPayoutStatement]    VARCHAR (3)      DEFAULT ('Yes') NULL,
    [ChequeRunName]        VARCHAR (50)     DEFAULT ('') NOT NULL,
    [Category]             VARCHAR (50)     DEFAULT ('REGUALR') NOT NULL,
    [ChequeRunID]          INT              NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY1]
    ON [dbo].[RPWeb_Statements]([StatementLevel] ASC, [RetailerCode] ASC, [RetailerType] ASC, [Level2Code] ASC, [Level5Code] ASC, [SRC_StatementCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY2]
    ON [dbo].[RPWeb_Statements]([VersionType] ASC)
    INCLUDE([StatementLevel], [RetailerCode], [RetailerType], [Level2Code], [Level5Code], [SRC_StatementCode]);

