﻿CREATE TABLE [dbo].[RP_Error_Log] (
    [StatementCode] INT           NOT NULL,
    [ErrorDate]     DATETIME      DEFAULT (getdate()) NOT NULL,
    [ErrorMessage]  VARCHAR (MAX) NOT NULL,
    [ErrorStatus]   VARCHAR (25)  DEFAULT ('UNRESOLVED') NOT NULL
);

