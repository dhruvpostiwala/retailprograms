﻿CREATE TABLE [dbo].[RP_RetailerReference] (
    [Season]       INT          NOT NULL,
    [Region]       VARCHAR (4)  NULL,
    [RetailerCode] VARCHAR (20) NOT NULL,
    [RetailerType] VARCHAR (20) NOT NULL,
    [Level2Code]   VARCHAR (20) NOT NULL,
    [Level5Code]   VARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [RetailerCode] ASC)
);

