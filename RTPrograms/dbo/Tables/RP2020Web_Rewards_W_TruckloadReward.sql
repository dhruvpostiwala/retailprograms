﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_TruckloadReward] (
    [Statementcode]         INT          NOT NULL,
    [MarketLetterCode]      VARCHAR (50) NOT NULL,
    [ProgramCode]           VARCHAR (50) NOT NULL,
    [SingleBrand_DropCount] INT          NOT NULL,
    [SingleBrand_Reward]    MONEY        NOT NULL,
    [MixedBrand_DropCount]  INT          NOT NULL,
    [MixedBrand_Reward]     MONEY        NOT NULL,
    [Reward]                MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

