﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_E_PortfolioSupport] (
    [Statementcode]       INT            NOT NULL,
    [MarketLetterCode]    VARCHAR (50)   NOT NULL,
    [ProgramCode]         VARCHAR (50)   NOT NULL,
    [RewardBrand]         VARCHAR (100)  NOT NULL,
    [RewardBrand_Sales]   MONEY          NOT NULL,
    [QualBrand_Sales_CY]  MONEY          NOT NULL,
    [QualBrand_Sales_LY1] MONEY          NOT NULL,
    [QualBrand_Sales_LY2] MONEY          NOT NULL,
    [Reward_Percentage]   NUMERIC (6, 4) NOT NULL,
    [Reward]              MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

