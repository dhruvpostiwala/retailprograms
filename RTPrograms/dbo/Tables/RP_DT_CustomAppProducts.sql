﻿CREATE TABLE [dbo].[RP_DT_CustomAppProducts] (
    [Statementcode] INT           NOT NULL,
    [RP_ID]         INT           NOT NULL,
    [DataSetCode]   INT           NOT NULL,
    [Season]        INT           NOT NULL,
    [Retailercode]  VARCHAR (20)  NOT NULL,
    [DocCode]       VARCHAR (30)  NOT NULL,
    [Type]          VARCHAR (50)  NOT NULL,
    [Product]       VARCHAR (200) NOT NULL,
    [Productcode]   VARCHAR (65)  NOT NULL,
    [Quantity]      FLOAT (53)    NOT NULL,
    [PackageSize]   VARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [RP_ID] ASC, [DocCode] ASC, [Type] ASC, [Product] ASC, [Productcode] ASC)
);

