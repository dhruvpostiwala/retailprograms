﻿CREATE TABLE [dbo].[RPWeb_EditHistoryEntry] (
    [EHEntryID]       INT           IDENTITY (1, 1) NOT NULL,
    [EHCode]          INT           NOT NULL,
    [EHDisplay]       VARCHAR (3)   NOT NULL,
    [EHEntryType]     VARCHAR (50)  NOT NULL,
    [EHFieldName]     VARCHAR (50)  NULL,
    [EHOriginalValue] VARCHAR (MAX) NULL,
    [EHNewValue]      VARCHAR (MAX) NULL,
    [EHMessage]       VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([EHEntryID] ASC)
);

