﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_PlanningTheBusiness_Actual] (
    [StatementCode]     INT            NOT NULL,
    [MarketLetterCode]  VARCHAR (50)   NOT NULL,
    [ProgramCode]       VARCHAR (50)   NOT NULL,
    [RewardBrand]       VARCHAR (50)   NOT NULL,
    [LY_Sales]          MONEY          DEFAULT ((0)) NOT NULL,
    [CY_POG_Plan]       MONEY          DEFAULT ((0)) NOT NULL,
    [RewardBrand_Sales] MONEY          DEFAULT ((0)) NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [Reward]            MONEY          DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

