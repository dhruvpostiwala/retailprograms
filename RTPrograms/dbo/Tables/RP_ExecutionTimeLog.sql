﻿CREATE TABLE [dbo].[RP_ExecutionTimeLog] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [StoredProcedureName] VARCHAR (255) NOT NULL,
    [Programcode]         VARCHAR (50)  NOT NULL,
    [StartTime]           DATETIME      NOT NULL,
    [EndTime]             DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

