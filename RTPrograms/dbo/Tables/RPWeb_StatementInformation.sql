﻿CREATE TABLE [dbo].[RPWeb_StatementInformation] (
    [StatementCode] INT           NOT NULL,
    [FieldName]     VARCHAR (50)  NOT NULL,
    [FieldValue]    VARCHAR (500) NULL,
    [Comments]      VARCHAR (300) NULL,
    [FieldCategory] VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [FieldName] ASC)
);

