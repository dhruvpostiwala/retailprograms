﻿CREATE TABLE [dbo].[RPWeb_MarginToolData_Summary] (
    [Statementcode] INT             NOT NULL,
    [Item_Code]     VARCHAR (50)    NOT NULL,
    [Item_Value]    DECIMAL (18, 4) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [Item_Code] ASC)
);

