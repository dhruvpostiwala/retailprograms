﻿CREATE TABLE [dbo].[RP_Config_Accrual] (
    [Season]               INT          NOT NULL,
    [Region]               VARCHAR (4)  NOT NULL,
    [MarketLetterCode]     VARCHAR (50) NOT NULL,
    [RewardCode]           VARCHAR (50) NOT NULL,
    [AccrualAmount]        MONEY        NOT NULL,
    [ALT_MarketLetterCode] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Region] ASC, [MarketLetterCode] ASC, [RewardCode] ASC)
);

