﻿CREATE TABLE [dbo].[RP_Config_Users] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Season]       VARCHAR (4)   NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Role]         VARCHAR (75)  NOT NULL,
    [Email]        VARCHAR (100) NOT NULL,
    [Status]       VARCHAR (20)  NOT NULL,
    [Organization] VARCHAR (100) NULL,
    [DateCreated]  DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Name] ASC, [Role] ASC)
);

