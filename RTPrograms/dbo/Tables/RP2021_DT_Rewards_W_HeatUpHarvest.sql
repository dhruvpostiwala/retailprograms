﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_HeatUpHarvest] (
    [Statementcode]    INT          NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [ProgramCode]      VARCHAR (50) NOT NULL,
    [FarmCode]         VARCHAR (50) NOT NULL,
    [Heat_Acres]       MONEY        NOT NULL,
    [Invigor_Acres]    MONEY        NOT NULL,
    [Matched_Acres]    MONEY        NOT NULL,
    [RewardPerAcre]    MONEY        NOT NULL,
    [Reward]           MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [ProgramCode] ASC, [FarmCode] ASC)
);

