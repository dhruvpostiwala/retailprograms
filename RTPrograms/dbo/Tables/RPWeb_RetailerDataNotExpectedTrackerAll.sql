﻿CREATE TABLE [dbo].[RPWeb_RetailerDataNotExpectedTrackerAll] (
    [StatementCode]     INT          NOT NULL,
    [RP_ID]             INT          NOT NULL,
    [RetailerCode]      VARCHAR (20) NOT NULL,
    [Season]            VARCHAR (4)  NOT NULL,
    [DataType]          VARCHAR (50) NULL,
    [MonthOfSubmission] VARCHAR (20) NULL,
    [Comments]          VARCHAR (50) NULL,
    [DateModified]      DATETIME     NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RP_ID] ASC)
);

