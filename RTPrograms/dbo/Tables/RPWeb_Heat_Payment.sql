﻿CREATE TABLE [dbo].[RPWeb_Heat_Payment] (
    [StatementCode]       INT             NOT NULL,
    [Heat_Proj_Value]     MONEY           NOT NULL,
    [Distinct_Proj_Value] MONEY           NOT NULL,
    [ML_Margin_Rate]      DECIMAL (28, 4) NOT NULL,
    [Amount]              MONEY           NOT NULL
);

