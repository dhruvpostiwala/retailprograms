﻿CREATE TABLE [dbo].[RP_Config_ML_Programs] (
    [MarketLetterCode] VARCHAR (50)   NOT NULL,
    [ProgramCode]      VARCHAR (50)   NOT NULL,
    [Level5Code]       VARCHAR (20)   NOT NULL,
    [Description]      NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    PRIMARY KEY CLUSTERED ([MarketLetterCode] ASC, [ProgramCode] ASC, [Level5Code] ASC)
);

