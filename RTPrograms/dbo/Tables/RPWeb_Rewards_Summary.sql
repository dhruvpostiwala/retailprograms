﻿CREATE TABLE [dbo].[RPWeb_Rewards_Summary] (
    [StatementCode]     INT           NOT NULL,
    [MarketLetterCode]  NVARCHAR (50) NOT NULL,
    [RewardCode]        VARCHAR (50)  NOT NULL,
    [RewardValue]       MONEY         NOT NULL,
    [RewardEligibility] VARCHAR (20)  NOT NULL,
    [RewardMessage]     VARCHAR (250) NULL,
    [TotalReward]       MONEY         NOT NULL,
    [PaidToDate]        MONEY         NOT NULL,
    [CurrentPayment]    MONEY         NOT NULL,
    [NextPayment]       MONEY         NOT NULL,
    [PaymentAmount]     MONEY         NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [RewardCode] ASC)
);

