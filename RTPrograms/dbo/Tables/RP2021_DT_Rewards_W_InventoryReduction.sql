﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_InventoryReduction] (
    [Statementcode]     INT           NOT NULL,
    [MarketLetterCode]  VARCHAR (50)  NOT NULL,
    [ProgramCode]       VARCHAR (50)  NOT NULL,
    [RewardBrand]       VARCHAR (100) NOT NULL,
    [RewardBrand_Sales] MONEY         NOT NULL,
    [Inventory_Sales]   MONEY         NOT NULL,
    [Matched_Sales]     MONEY         NOT NULL,
    [RewardPer]         MONEY         NOT NULL,
    [Reward]            MONEY         NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

