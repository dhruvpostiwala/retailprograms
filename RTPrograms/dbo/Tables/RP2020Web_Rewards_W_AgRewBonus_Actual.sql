﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_AgRewBonus_Actual] (
    [StatementCode]     INT            NOT NULL,
    [MarketLetterCode]  VARCHAR (50)   NOT NULL,
    [ProgramCode]       VARCHAR (50)   NOT NULL,
    [RetailerCode]      VARCHAR (20)   NOT NULL,
    [FarmCode]          VARCHAR (20)   NOT NULL,
    [HasPIPEDA]         VARCHAR (3)    NOT NULL,
    [LeadClosed]        VARCHAR (3)    NOT NULL,
    [SegmentsCount]     INT            NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [RewardBrand_Sales] MONEY          NOT NULL,
    [Reward]            MONEY          NOT NULL,
    [InVigorBonus]      VARCHAR (3)    NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

