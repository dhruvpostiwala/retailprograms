﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_GrowthBonus] (
    [StatementCode]             INT             NOT NULL,
    [MarketLetterCode]          VARCHAR (50)    NOT NULL,
    [ProgramCode]               VARCHAR (50)    NOT NULL,
    [RewardBrand]               VARCHAR (255)   NOT NULL,
    [QualSales_CY]              MONEY           NOT NULL,
    [QualSales_LY_Canola]       MONEY           NOT NULL,
    [QualSales_LY_Canola_INDEX] MONEY           NOT NULL,
    [QualSales_LY_Pulse]        MONEY           NOT NULL,
    [QualSales_LY_Pulse_INDEX]  MONEY           NOT NULL,
    [QualSales_LY_Cereal]       MONEY           NOT NULL,
    [QualSales_LY_Cereal_INDEX] MONEY           NOT NULL,
    [QualSales_LY]              MONEY           DEFAULT ((0)) NOT NULL,
    [QualSales_LY_INDEX]        MONEY           DEFAULT ((0)) NOT NULL,
    [Growth_Percentage]         DECIMAL (19, 4) DEFAULT ((0)) NOT NULL,
    [Reward_Percentage]         DECIMAL (6, 4)  DEFAULT ((0)) NOT NULL,
    [Reward]                    MONEY           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

