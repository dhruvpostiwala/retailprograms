﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_INV_INNOV] (
    [StatementCode]         INT             NOT NULL,
    [RetailerCode]          VARCHAR (50)    NOT NULL,
    [MarketLetterCode]      VARCHAR (50)    NOT NULL,
    [ProgramCode]           VARCHAR (50)    NOT NULL,
    [RewardBrand]           VARCHAR (100)   NOT NULL,
    [InVigor_Bags]          DECIMAL (18)    NOT NULL,
    [Innovation_Bags]       DECIMAL (18)    NOT NULL,
    [RewardBrand_Sales]     MONEY           NOT NULL,
    [Innovation_Sales]      MONEY           DEFAULT ((0)) NOT NULL,
    [InVigor300_Sales]      MONEY           NOT NULL,
    [InVigor200_Sales]      MONEY           NOT NULL,
    [Qualifying_Percentage] DECIMAL (19, 4) DEFAULT ((0)) NOT NULL,
    [InVigor300_Percentage] DECIMAL (6, 4)  DEFAULT ((0)) NOT NULL,
    [InVigor200_Percentage] DECIMAL (6, 4)  DEFAULT ((0)) NOT NULL,
    [InVigor300_Reward]     MONEY           DEFAULT ((0)) NOT NULL,
    [InVigor200_Reward]     MONEY           DEFAULT ((0)) NOT NULL,
    [Reward]                MONEY           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RetailerCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

