﻿CREATE TABLE [dbo].[RPWeb_Display_MarginSummary] (
    [Statementcode] INT             NOT NULL,
    [Item_Code]     VARCHAR (50)    NOT NULL,
    [Item_Label]    VARCHAR (200)   NOT NULL,
    [Item_Value]    DECIMAL (18, 4) NOT NULL
);

