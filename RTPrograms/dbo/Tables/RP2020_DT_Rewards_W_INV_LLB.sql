﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_INV_LLB] (
    [Statementcode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [Liberty_Acres]     NUMERIC (18, 2) NOT NULL,
    [Invigor_Acres]     NUMERIC (18, 2) NOT NULL,
    [RewardBrand]       VARCHAR (100)   NOT NULL,
    [RewardBrand_Sales] MONEY           NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4)  NOT NULL,
    [Reward]            MONEY           NOT NULL,
    [Reseeded_Acres]    NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

