﻿CREATE TABLE [dbo].[RP_Config_EI_Fields] (
    [ProgramCode]    VARCHAR (50)    NOT NULL,
    [FieldCode]      VARCHAR (50)    NOT NULL,
    [GroupTitle]     VARCHAR (300)   NOT NULL,
    [FieldType]      VARCHAR (20)    NOT NULL,
    [Label]          VARCHAR (300)   NOT NULL,
    [Suffix]         VARCHAR (20)    NOT NULL,
    [DisplaySeq]     INT             NOT NULL,
    [HelpInfo]       VARCHAR (MAX)   NOT NULL,
    [Status]         VARCHAR (20)    NOT NULL,
    [DefaultValue]   NUMERIC (16, 2) NOT NULL,
    [CalcFieldValue] VARCHAR (3)     DEFAULT ('NO') NOT NULL,
    [DisplayOnce]    BIT             DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ProgramCode] ASC, [FieldCode] ASC)
);

