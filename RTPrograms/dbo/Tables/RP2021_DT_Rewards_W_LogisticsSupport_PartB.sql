﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_LogisticsSupport_PartB] (
    [Statementcode]             INT            NOT NULL,
    [MarketLetterCode]          VARCHAR (50)   NOT NULL,
    [ProgramCode]               VARCHAR (50)   NOT NULL,
    [RetailerCode]              VARCHAR (50)   NOT NULL,
    [BrandSegment]              VARCHAR (100)  NOT NULL,
    [March_Take]                DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [March_Reward]              DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [April_Take]                DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [April_Reward]              DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [May_Take]                  DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [May_Reward]                DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [June_Take]                 DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [June_Reward]               DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [July_Take]                 DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [July_Reward]               DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [Segment_Reward_Sales]      MONEY          NOT NULL,
    [Segment_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Reward]                    MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [BrandSegment] ASC)
);

