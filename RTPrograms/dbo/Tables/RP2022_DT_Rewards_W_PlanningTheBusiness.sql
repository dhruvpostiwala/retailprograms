﻿CREATE TABLE [dbo].[RP2022_DT_Rewards_W_PlanningTheBusiness] (
    [Statementcode]          INT             NOT NULL,
    [MarketLetterCode]       VARCHAR (50)    NOT NULL,
    [ProgramCode]            VARCHAR (50)    NOT NULL,
    [QualSales_CY]           MONEY           NOT NULL,
    [QualSales_LY]           MONEY           NOT NULL,
    [Growth_Percentage]      DECIMAL (18, 4) NOT NULL,
    [CPP_Reward_Sales]       MONEY           NOT NULL,
    [CPP_Reward_Percentage]  DECIMAL (6, 4)  NOT NULL,
    [CPP_Reward]             MONEY           NOT NULL,
    [Cent_Reward_Sales]      MONEY           NOT NULL,
    [Cent_Reward_Percentage] DECIMAL (6, 4)  NOT NULL,
    [Cent_Reward]            MONEY           NOT NULL,
    [Reward]                 MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

