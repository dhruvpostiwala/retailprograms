﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_E_SalesSupportLoyaltyBonus] (
    [Statementcode]         INT            NOT NULL,
    [MarketLetterCode]      VARCHAR (50)   NOT NULL,
    [ProgramCode]           VARCHAR (50)   NOT NULL,
    [RewardBrand_Sales]     MONEY          NOT NULL,
    [Reward_Percentage]     DECIMAL (6, 4) NOT NULL,
    [Reward]                MONEY          NOT NULL,
    [RewardBrand_POG_Sales] MONEY          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

