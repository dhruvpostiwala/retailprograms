﻿CREATE TABLE [dbo].[RP_DT_ML_ELG_Programs] (
    [StatementCode]    INT          NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [ProgramCode]      VARCHAR (50) NOT NULL,
    [DataSetCode]      INT          DEFAULT ((0)) NOT NULL,
    [DataSetCode_L5]   INT          NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

