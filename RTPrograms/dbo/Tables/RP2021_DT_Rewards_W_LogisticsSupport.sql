﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_LogisticsSupport] (
    [Statementcode]             INT            NOT NULL,
    [MarketLetterCode]          VARCHAR (50)   NOT NULL,
    [ProgramCode]               VARCHAR (50)   NOT NULL,
    [BrandSegment]              VARCHAR (100)  NOT NULL,
    [Segment_Reward_Sales]      MONEY          NOT NULL,
    [LY_Sales]                  MONEY          NOT NULL,
    [POG_Plan]                  MONEY          NOT NULL,
    [Segment_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Reward]                    MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [BrandSegment] ASC)
);

