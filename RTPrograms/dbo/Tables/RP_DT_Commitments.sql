﻿CREATE TABLE [dbo].[RP_DT_Commitments] (
    [Statementcode]  INT          NOT NULL,
    [RP_ID]          INT          NOT NULL,
    [ID]             INT          NOT NULL,
    [DataSetCode]    INT          NOT NULL,
    [BASFSeason]     INT          NOT NULL,
    [RetailerCode]   VARCHAR (20) NOT NULL,
    [FarmCode]       VARCHAR (20) NOT NULL,
    [CommitmentType] VARCHAR (50) NULL,
    [CommitmentDate] DATE         NULL,
    [Status]         VARCHAR (50) NULL,
    [DocCode]        VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [RP_ID] ASC)
);

