﻿CREATE TABLE [dbo].[RPWeb_Leads] (
    [StatementCode]    INT           NOT NULL,
    [RP_ID]            INT           NOT NULL,
    [LeadID]           INT           NOT NULL,
    [Season]           INT           NOT NULL,
    [RetailerCode]     VARCHAR (20)  NOT NULL,
    [FarmCode]         VARCHAR (20)  NOT NULL,
    [InRadius]         BIT           NOT NULL,
    [ProductCode]      VARCHAR (65)  NOT NULL,
    [VarietyCode]      VARCHAR (50)  NOT NULL,
    [Acres]            FLOAT (53)    NOT NULL,
    [AppRate]          INT           NOT NULL,
    [BookType]         VARCHAR (20)  NOT NULL,
    [GeneratingSource] VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [LeadID] ASC, [RP_ID] ASC)
);

