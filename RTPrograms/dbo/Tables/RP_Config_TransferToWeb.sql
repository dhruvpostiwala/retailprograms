﻿CREATE TABLE [dbo].[RP_Config_TransferToWeb] (
    [ID]     INT           IDENTITY (1, 1) NOT NULL,
    [Type]   VARCHAR (20)  NOT NULL,
    [Season] INT           NOT NULL,
    [Action] VARCHAR (255) NOT NULL,
    [Status] VARCHAR (20)  NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

