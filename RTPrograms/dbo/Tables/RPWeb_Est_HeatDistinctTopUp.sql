﻿CREATE TABLE [dbo].[RPWeb_Est_HeatDistinctTopUp] (
    [Statementcode]     INT             NOT NULL,
    [Prj_StatementCode] INT             NOT NULL,
    [Heat_Prj_POG]      MONEY           NOT NULL,
    [Heat_Act_POG]      MONEY           NOT NULL,
    [Distinct_Prj_POG]  MONEY           NOT NULL,
    [Distinct_Act_POG]  MONEY           NOT NULL,
    [ML_Eligible_POG]   MONEY           NOT NULL,
    [ML_Total_Reward]   MONEY           NOT NULL,
    [Margin]            DECIMAL (18, 4) NOT NULL,
    [Heat_Estimate]     MONEY           NOT NULL,
    [Distinct_Estimate] MONEY           NOT NULL,
    [Amount]            MONEY           NOT NULL,
    [SRC_Statementcode] INT             NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC)
);

