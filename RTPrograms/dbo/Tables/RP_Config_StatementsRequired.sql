﻿CREATE TABLE [dbo].[RP_Config_StatementsRequired] (
    [Season]         INT            NOT NULL,
    [StatementType]  VARCHAR (20)   NOT NULL,
    [StatementLevel] VARCHAR (20)   NOT NULL,
    [RetailerCode]   VARCHAR (20)   NOT NULL,
    [Comments]       NVARCHAR (400) NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [StatementType] ASC, [StatementLevel] ASC, [RetailerCode] ASC)
);

