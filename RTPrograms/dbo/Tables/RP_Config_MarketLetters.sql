﻿CREATE TABLE [dbo].[RP_Config_MarketLetters] (
    [Season]              INT           NOT NULL,
    [MarketLetterCode]    VARCHAR (50)  NOT NULL,
    [Region]              VARCHAR (4)   NOT NULL,
    [Sequence]            INT           NOT NULL,
    [Title]               VARCHAR (200) NOT NULL,
    [IncludeInForecast]   VARCHAR (3)   DEFAULT ('NO') NOT NULL,
    [IncludeInActual]     VARCHAR (3)   DEFAULT ('NO') NOT NULL,
    [IncludeInProjection] VARCHAR (3)   DEFAULT ('NO') NOT NULL,
    [PDFFileName]         VARCHAR (100) NULL,
    [PDFFileNameFr]       VARCHAR (100) NULL,
    [Status]              VARCHAR (10)  NULL,
    [FundsRequestGroup]   VARCHAR (50)  NULL,
    [Title_FR]            VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [MarketLetterCode] ASC)
);

