﻿CREATE TABLE [dbo].[RP_Config_Groups] (
    [GroupID]          INT           IDENTITY (1, 1) NOT NULL,
    [ReleaseFlag]      VARCHAR (50)  NOT NULL,
    [MarketLetterCode] VARCHAR (50)  NOT NULL,
    [ProgramCode]      VARCHAR (50)  NOT NULL,
    [GroupLabel]       VARCHAR (100) NOT NULL,
    [GroupType]        VARCHAR (50)  NOT NULL,
    [Comments]         VARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([GroupID] ASC)
);

