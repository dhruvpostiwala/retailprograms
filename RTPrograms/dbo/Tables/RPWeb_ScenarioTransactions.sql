﻿CREATE TABLE [dbo].[RPWeb_ScenarioTransactions] (
    [StatementCode]   INT             NOT NULL,
    [RP_ID]           INT             NOT NULL,
    [ScenarioCode]    VARCHAR (20)    NOT NULL,
    [ProductCode]     VARCHAR (65)    NOT NULL,
    [HybridCode]      VARCHAR (50)    NOT NULL,
    [DataSetCode]     INT             NOT NULL,
    [RetailerCode]    VARCHAR (20)    NOT NULL,
    [ChemicalGroup]   VARCHAR (50)    NOT NULL,
    [Quantity]        FLOAT (53)      NOT NULL,
    [Acres]           FLOAT (53)      NOT NULL,
    [Price_SDP]       NUMERIC (18, 2) NOT NULL,
    [Price_SRP]       NUMERIC (18, 2) NOT NULL,
    [UC_DateModified] DATE            NULL,
    [Price_SWP]       NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [DataSetCode_L5]  INT             NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RP_ID] ASC, [ScenarioCode] ASC, [ProductCode] ASC, [HybridCode] ASC)
);

