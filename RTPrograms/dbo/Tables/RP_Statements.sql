﻿CREATE TABLE [dbo].[RP_Statements] (
    [StatementCode]        INT              IDENTITY (1, 1) NOT NULL,
    [StatementType]        VARCHAR (20)     NOT NULL,
    [VersionType]          VARCHAR (20)     NOT NULL,
    [StatementLevel]       VARCHAR (20)     NOT NULL,
    [Status]               VARCHAR (20)     NOT NULL,
    [Season]               VARCHAR (4)      NOT NULL,
    [Region]               VARCHAR (4)      NULL,
    [RetailerCode]         VARCHAR (20)     NOT NULL,
    [RetailerType]         VARCHAR (20)     NOT NULL,
    [Level2Code]           VARCHAR (20)     NOT NULL,
    [Level5Code]           VARCHAR (20)     NOT NULL,
    [ActiveRetailerCode]   VARCHAR (20)     NULL,
    [DataSetCode]          INT              NULL,
    [UC_Job_ID]            UNIQUEIDENTIFIER NULL,
    [UC_SRC_Statementcode] INT              NULL,
    [DataSetCode_L5]       INT              DEFAULT ((0)) NULL,
    [IsPayoutStatement]    VARCHAR (3)      DEFAULT ('Yes') NULL,
    [Category]             VARCHAR (50)     DEFAULT ('REGUALR') NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY1]
    ON [dbo].[RP_Statements]([StatementType] ASC, [VersionType] ASC, [Region] ASC, [RetailerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY2]
    ON [dbo].[RP_Statements]([StatementType] ASC, [StatementLevel] ASC, [Status] ASC, [Region] ASC, [VersionType] ASC)
    INCLUDE([Season], [Level2Code]);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY3]
    ON [dbo].[RP_Statements]([StatementType] ASC, [VersionType] ASC, [Region] ASC, [RetailerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY4]
    ON [dbo].[RP_Statements]([StatementType] ASC, [Region] ASC, [VersionType] ASC, [Status] ASC)
    INCLUDE([StatementLevel], [Season], [RetailerCode], [RetailerType], [Level2Code], [Level5Code]);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY5]
    ON [dbo].[RP_Statements]([StatementType] ASC, [Region] ASC, [VersionType] ASC)
    INCLUDE([Season]);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY6]
    ON [dbo].[RP_Statements]([StatementType] ASC, [StatementLevel] ASC, [Status] ASC, [Region] ASC, [VersionType] ASC)
    INCLUDE([Season], [RetailerCode]);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY7]
    ON [dbo].[RP_Statements]([StatementType] ASC, [StatementLevel] ASC, [Status] ASC, [Region] ASC, [VersionType] ASC)
    INCLUDE([Season], [Level2Code]);


GO
CREATE NONCLUSTERED INDEX [IDX_KEY8]
    ON [dbo].[RP_Statements]([StatementType] ASC, [StatementLevel] ASC, [Status] ASC, [Region] ASC, [RetailerCode] ASC, [Level2Code] ASC, [VersionType] ASC)
    INCLUDE([Season]);

