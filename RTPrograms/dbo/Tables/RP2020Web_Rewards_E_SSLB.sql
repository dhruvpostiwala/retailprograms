﻿CREATE TABLE [dbo].[RP2020Web_Rewards_E_SSLB] (
    [Statementcode]         INT            NOT NULL,
    [MarketLetterCode]      VARCHAR (50)   NOT NULL,
    [ProgramCode]           VARCHAR (50)   NOT NULL,
    [RewardBrand]           VARCHAR (100)  NOT NULL,
    [Actual_CY_Sales]       MONEY          NOT NULL,
    [RewardBrand_CY_Sales]  MONEY          NOT NULL,
    [RewardBrand_POG_Sales] MONEY          NOT NULL,
    [Reward_Percentage]     DECIMAL (6, 4) NOT NULL,
    [Reward]                MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

