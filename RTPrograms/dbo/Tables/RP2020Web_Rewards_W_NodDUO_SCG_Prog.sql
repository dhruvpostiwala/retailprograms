﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_NodDUO_SCG_Prog] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [RewardBrand]       VARCHAR (50)    NOT NULL,
    [Sales_LY]          NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Sales] MONEY           NOT NULL,
    [Growth]            FLOAT (53)      NOT NULL,
    [Reward_Perc]       NUMERIC (18, 2) NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

