﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_RetailBonus] (
    [Statementcode]          INT            NOT NULL,
    [MarketLetterCode]       VARCHAR (50)   NOT NULL,
    [ProgramCode]            VARCHAR (50)   NOT NULL,
    [QualBrand_Sales_CY]     MONEY          NOT NULL,
    [Growth_Sales_CY]        MONEY          NOT NULL,
    [Growth_Sales_LY1]       MONEY          NOT NULL,
    [Growth_Sales_LY2]       MONEY          NOT NULL,
    [Lib_Reward_Sales]       MONEY          NOT NULL,
    [Lib_Reward_Percentage]  DECIMAL (6, 4) NOT NULL,
    [Lib_Reward]             MONEY          NOT NULL,
    [Cent_Reward_Sales]      MONEY          NOT NULL,
    [Cent_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [Cent_Reward]            MONEY          NOT NULL,
    [Reward]                 MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

