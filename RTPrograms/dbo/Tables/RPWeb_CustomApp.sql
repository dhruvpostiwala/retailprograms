﻿CREATE TABLE [dbo].[RPWeb_CustomApp] (
    [Statementcode] INT          NOT NULL,
    [RP_ID]         INT          NOT NULL,
    [DataSetCode]   INT          NOT NULL,
    [Season]        INT          NOT NULL,
    [DocCode]       VARCHAR (30) NOT NULL,
    [RetailerCode]  VARCHAR (30) NOT NULL,
    [SubmitMethod]  VARCHAR (30) NOT NULL,
    [SubmitDate]    DATE         NOT NULL,
    [SubmitStatus]  VARCHAR (30) NOT NULL,
    [LocationType]  VARCHAR (30) NOT NULL,
    [Region]        VARCHAR (4)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [RP_ID] ASC, [DocCode] ASC)
);

