﻿CREATE TABLE [dbo].[RP_Config_Programs] (
    [ProgramCode]               VARCHAR (50)  NOT NULL,
    [Status]                    VARCHAR (20)  NOT NULL,
    [Season]                    INT           NOT NULL,
    [Region]                    VARCHAR (4)   NOT NULL,
    [Title]                     VARCHAR (200) NOT NULL,
    [RewardCode]                VARCHAR (50)  NOT NULL,
    [StoredProcedureName]       VARCHAR (255) NOT NULL,
    [CalculationOrder]          INT           NOT NULL,
    [EI_PageDisplayOrder]       INT           NOT NULL,
    [TabDisplayOrder]           INT           NOT NULL,
    [Comments]                  VARCHAR (400) NULL,
    [ReportStoredProcedureName] VARCHAR (255) NULL,
    [Title_FR]                  VARCHAR (255) NULL,
    [Exception_Enabled]         BIT           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([ProgramCode] ASC)
);

