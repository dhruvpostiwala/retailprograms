﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_E_Sollio_Growth_Bonus] (
    [StatementCode]       INT            NOT NULL,
    [MarketLetterCode]    VARCHAR (50)   NOT NULL,
    [ProgramCode]         VARCHAR (100)  NOT NULL,
    [RewardBrand]         VARCHAR (100)  NOT NULL,
    [RewardBrand_Sales]   MONEY          NOT NULL,
    [QualBrand_Sales_LY1] MONEY          NOT NULL,
    [QualBrand_Sales_LY2] MONEY          NOT NULL,
    [Reward_Percentage]   DECIMAL (6, 2) DEFAULT ((0)) NOT NULL,
    [Reward]              MONEY          DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

