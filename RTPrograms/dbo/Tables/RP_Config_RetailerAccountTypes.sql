﻿CREATE TABLE [dbo].[RP_Config_RetailerAccountTypes] (
    [Season]      INT           NOT NULL,
    [Region]      NVARCHAR (4)  NOT NULL,
    [AccountType] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Region] ASC, [AccountType] ASC)
);

