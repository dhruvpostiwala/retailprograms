﻿CREATE TABLE [dbo].[RPWeb_Reports_Exposure_L5_Summary] (
    [ExposureID]       INT          NOT NULL,
    [Level5Code]       VARCHAR (20) NOT NULL,
    [RetailerName]     VARCHAR (65) NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [RewardCode]       VARCHAR (50) NOT NULL,
    [ExposureType]     VARCHAR (50) NOT NULL,
    [RewardAmount]     MONEY        NOT NULL,
    [CurrentPayment]   MONEY        NOT NULL,
    [PaidToDate]       MONEY        NOT NULL,
    [Claims]           MONEY        NOT NULL,
    [AveragePayment]   MONEY        NOT NULL,
    [NextPayment]      INT          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([ExposureID] ASC, [Level5Code] ASC, [MarketLetterCode] ASC, [RewardCode] ASC),
    CONSTRAINT [FK_L5_ExposureID] FOREIGN KEY ([ExposureID]) REFERENCES [dbo].[RPWeb_Reports_Exposure] ([ExposureID])
);

