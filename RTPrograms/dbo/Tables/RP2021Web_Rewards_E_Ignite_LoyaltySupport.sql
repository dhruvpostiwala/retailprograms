﻿CREATE TABLE [dbo].[RP2021Web_Rewards_E_Ignite_LoyaltySupport] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [RewardBrand]       VARCHAR (100)   NOT NULL,
    [ProductCode]       VARCHAR (255)   NOT NULL,
    [RewardBrand_QTY]   DECIMAL (19, 4) NOT NULL,
    [RewardPerUnit]     MONEY           DEFAULT ((0)) NOT NULL,
    [Reward]            MONEY           DEFAULT ((0)) NOT NULL,
    [RewardBrand_Sales] MONEY           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC, [ProductCode] ASC)
);

