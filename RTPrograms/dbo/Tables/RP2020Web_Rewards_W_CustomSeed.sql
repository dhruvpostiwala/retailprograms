﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_CustomSeed] (
    [Statementcode]      INT             NOT NULL,
    [MarketLetterCode]   VARCHAR (50)    NOT NULL,
    [ProgramCode]        VARCHAR (50)    NOT NULL,
    [RewardBrand]        VARCHAR (100)   NOT NULL,
    [RewardBrand_Sales]  MONEY           NOT NULL,
    [RewardBrand_Acres]  NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Litres] NUMERIC (18, 2) NOT NULL,
    [CustomApp_Sales]    MONEY           NOT NULL,
    [CustomApp_Acres]    NUMERIC (18, 2) NOT NULL,
    [CustomApp_Litres]   NUMERIC (18, 2) NOT NULL,
    [Rewardable_Acres]   NUMERIC (18, 2) NOT NULL,
    [Rewardable_Litres]  NUMERIC (18, 2) NOT NULL,
    [Rewardable_Sales]   MONEY           NOT NULL,
    [Reward_Percentage]  DECIMAL (3, 2)  NOT NULL,
    [Reward]             MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

