﻿CREATE TABLE [dbo].[RP_Config_Exceptions] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [RetailerCode]  VARCHAR (20)   NOT NULL,
    [SubmittedFrom] INT            NOT NULL,
    [ProgramCode]   VARCHAR (50)   NOT NULL,
    [Name]          VARCHAR (100)  NOT NULL,
    [Author]        VARCHAR (100)  NOT NULL,
    [Status]        VARCHAR (20)   NOT NULL,
    [Description]   VARCHAR (2000) NOT NULL,
    [Comments]      VARCHAR (1000) NOT NULL,
    [Season]        VARCHAR (4)    NOT NULL,
    [ApprovedDate]  DATETIME       NOT NULL,
    [CreatedDate]   DATETIME       NOT NULL,
    [ModifiedDate]  DATETIME       NOT NULL
);

