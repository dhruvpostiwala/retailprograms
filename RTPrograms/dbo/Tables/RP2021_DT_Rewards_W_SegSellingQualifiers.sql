﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_SegSellingQualifiers] (
    [StatementCode]   INT          NOT NULL,
    [RetailerCode]    VARCHAR (50) NOT NULL,
    [FarmCode]        VARCHAR (50) NOT NULL,
    [Segment]         VARCHAR (50) NOT NULL,
    [Ret_Acres_CY]    INT          DEFAULT ((0)) NULL,
    [Ret_Acres_LY]    INT          DEFAULT ((0)) NULL,
    [SP_Acres_CY]     INT          DEFAULT ((0)) NULL,
    [SP_Acres_LY]     INT          DEFAULT ((0)) NULL,
    [Family_Acres_CY] INT          DEFAULT ((0)) NULL,
    [Family_Acres_LY] INT          DEFAULT ((0)) NULL,
    [Flag]            VARCHAR (20) DEFAULT ('') NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RetailerCode] ASC, [FarmCode] ASC, [Segment] ASC)
);

