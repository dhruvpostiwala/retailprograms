﻿CREATE TABLE [dbo].[RP_ActivityLog] (
    [ActivityLogID]         INT            IDENTITY (1, 1) NOT NULL,
    [ActivityID]            FLOAT (53)     NOT NULL,
    [ActivityType]          VARCHAR (50)   NOT NULL,
    [ActivityName]          VARCHAR (50)   NOT NULL,
    [ActivityAction]        VARCHAR (8000) NULL,
    [ActivityStatus]        VARCHAR (50)   NOT NULL,
    [ActivityStatusMessage] VARCHAR (1000) NOT NULL,
    [ActivityDate]          DATETIME       NOT NULL,
    [ActivityEndDateTime]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ActivityLogID] ASC)
);

