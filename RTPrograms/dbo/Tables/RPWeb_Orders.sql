﻿CREATE TABLE [dbo].[RPWeb_Orders] (
    [StatementCode] INT          NOT NULL,
    [RP_ID]         INT          NOT NULL,
    [DataSetCode]   INT          NOT NULL,
    [TransCode]     VARCHAR (20) NOT NULL,
    [Season]        INT          NOT NULL,
    [RetailerCode]  VARCHAR (20) NOT NULL,
    [FarmCode]      VARCHAR (20) NOT NULL,
    [ProductCode]   VARCHAR (65) NOT NULL,
    [Conversion]    FLOAT (53)   NOT NULL,
    [InvoiceDate]   DATETIME     NOT NULL,
    [InvoiceNumber] VARCHAR (50) NOT NULL,
    [Quantity]      FLOAT (53)   NOT NULL,
    [InRadius]      BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RP_ID] ASC, [RetailerCode] ASC, [TransCode] ASC)
);

