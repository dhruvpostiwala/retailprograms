﻿CREATE TABLE [dbo].[RPWeb_StatementLocations] (
    [StatementCode]  INT          NOT NULL,
    [StatementLevel] VARCHAR (20) NOT NULL,
    [RetailerCode]   VARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [StatementLevel] ASC, [RetailerCode] ASC)
);

