﻿CREATE TABLE [dbo].[RPWeb_EditHistoryMappings] (
    [StatementCode] INT           NOT NULL,
    [EHLinkCode]    INT           NOT NULL,
    [EHCode]        INT           NOT NULL,
    [TagData]       VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([EHCode] ASC, [EHLinkCode] ASC)
);

