﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_Cent_AFM] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [RewardBrand]       VARCHAR (50)    NOT NULL,
    [RewardBrand_Sales] MONEY           NOT NULL,
    [Quantity]          NUMERIC (18, 2) NOT NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

