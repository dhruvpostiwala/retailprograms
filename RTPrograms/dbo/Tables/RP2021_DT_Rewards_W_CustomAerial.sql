﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_CustomAerial] (
    [StatementCode]        INT             NOT NULL,
    [MarketLetterCode]     VARCHAR (50)    NOT NULL,
    [ProgramCode]          VARCHAR (50)    NOT NULL,
    [RewardBrand]          VARCHAR (50)    NOT NULL,
    [ProductCode]          VARCHAR (50)    NOT NULL,
    [RewardBrand_Sales]    MONEY           NOT NULL,
    [RewardBrand_Acres]    MONEY           NOT NULL,
    [Cust_App_Quantity]    NUMERIC (18, 2) NOT NULL,
    [Cust_App_Acres]       NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Cust_App] MONEY           NOT NULL,
    [Reward_Percentage]    DECIMAL (6, 5)  NOT NULL,
    [Reward]               MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [ProductCode] ASC)
);

