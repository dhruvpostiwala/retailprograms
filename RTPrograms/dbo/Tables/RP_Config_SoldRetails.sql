﻿CREATE TABLE [dbo].[RP_Config_SoldRetails] (
    [SoldRetailerCode]   VARCHAR (20) NOT NULL,
    [ActiveRetailerCode] VARCHAR (50) NOT NULL,
    [SeasonStart]        DATE         NOT NULL,
    [SeasonEnd]          DATE         NOT NULL,
    [Season]             VARCHAR (4)  NOT NULL
);

