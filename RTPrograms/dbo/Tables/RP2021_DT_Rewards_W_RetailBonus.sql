﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_RetailBonus] (
    [StatementCode]          INT             NOT NULL,
    [MarketLetterCode]       VARCHAR (50)    NOT NULL,
    [ProgramCode]            VARCHAR (50)    NOT NULL,
    [QualBrand_Sales_CY]     MONEY           NOT NULL,
    [Growth_Sales_CY]        MONEY           NOT NULL,
    [Growth_Sales_LY1]       MONEY           DEFAULT ((0)) NOT NULL,
    [Growth_Sales_LY1_INDEX] MONEY           DEFAULT ((0)) NOT NULL,
    [Growth_Sales_LY2]       MONEY           DEFAULT ((0)) NOT NULL,
    [Lib_Reward_Sales]       MONEY           NOT NULL,
    [Lib_Reward_Percentage]  DECIMAL (6, 4)  DEFAULT ((0)) NOT NULL,
    [Lib_Reward]             MONEY           DEFAULT ((0)) NOT NULL,
    [Cent_Reward_Sales]      MONEY           NOT NULL,
    [Cent_Reward_Percentage] DECIMAL (6, 4)  DEFAULT ((0)) NOT NULL,
    [Cent_Reward]            MONEY           DEFAULT ((0)) NOT NULL,
    [Growth_Percentage]      DECIMAL (18, 4) DEFAULT ((0)) NOT NULL,
    [Reward]                 MONEY           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

