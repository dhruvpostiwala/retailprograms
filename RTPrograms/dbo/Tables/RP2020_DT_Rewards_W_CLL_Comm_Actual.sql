﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_CLL_Comm_Actual] (
    [StatementCode]      INT          NOT NULL,
    [MarketLetterCode]   VARCHAR (50) NOT NULL,
    [ProgramCode]        VARCHAR (50) NOT NULL,
    [Retailercode]       VARCHAR (20) NOT NULL,
    [Farmcode]           VARCHAR (20) NOT NULL,
    [CommitmentDate]     DATE         NOT NULL,
    [Req_ReceivedByDate] BIT          NOT NULL,
    [RewardType]         VARCHAR (20) NOT NULL,
    [Reward]             MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [Retailercode] ASC, [Farmcode] ASC)
);

