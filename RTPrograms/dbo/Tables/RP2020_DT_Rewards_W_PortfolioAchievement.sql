﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_PortfolioAchievement] (
    [Statementcode]      INT            NOT NULL,
    [MarketLetterCode]   VARCHAR (50)   NOT NULL,
    [ProgramCode]        VARCHAR (50)   NOT NULL,
    [GroupLabel]         VARCHAR (100)  NOT NULL,
    [RewardBrand_Sales]  MONEY          NOT NULL,
    [QualBrand_Sales_CY] MONEY          NOT NULL,
    [QualBrand_Sales_LY] MONEY          NOT NULL,
    [Reward_Percentage]  DECIMAL (6, 4) NOT NULL,
    [Reward]             MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [GroupLabel] ASC)
);

