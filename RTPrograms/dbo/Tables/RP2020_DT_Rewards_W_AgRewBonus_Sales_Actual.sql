﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_AgRewBonus_Sales_Actual] (
    [Statementcode]    INT           NOT NULL,
    [MarketLetterCode] VARCHAR (50)  NOT NULL,
    [Retailercode]     VARCHAR (20)  NOT NULL,
    [Farmcode]         VARCHAR (20)  NOT NULL,
    [GroupLabel]       VARCHAR (100) NOT NULL,
    [SalesAmount]      MONEY         NOT NULL,
    [Reward]           MONEY         NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [Retailercode] ASC, [Farmcode] ASC, [GroupLabel] ASC)
);

