﻿CREATE TABLE [dbo].[RP2022Web_Rewards_W_CustomSeed] (
    [Statementcode]            INT             NOT NULL,
    [MarketLetterCode]         VARCHAR (50)    NOT NULL,
    [ProgramCode]              VARCHAR (50)    NOT NULL,
    [RewardBrand]              VARCHAR (100)   NOT NULL,
    [Planned_POG_Sales]        MONEY           NOT NULL,
    [Custom_Treated_POG_Sales] MONEY           NOT NULL,
    [RewardBrand_Percentage]   DECIMAL (18, 2) NOT NULL,
    [Reward]                   MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

