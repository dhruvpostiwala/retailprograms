﻿CREATE TABLE [dbo].[RP2022Web_Rewards_W_ClearfieldLentils] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [RetailerCode]      VARCHAR (20)    NOT NULL,
    [FarmCode]          VARCHAR (20)    NOT NULL,
    [CLL_Acres]         DECIMAL (18, 4) NOT NULL,
    [CLL_Sales]         MONEY           NOT NULL,
    [CLL_Herb_Acres]    DECIMAL (18, 4) NOT NULL,
    [CLL_Herb_Sales]    MONEY           NOT NULL,
    [Matched_Acres]     DECIMAL (18, 4) NOT NULL,
    [Reward_Percentage] DECIMAL (18, 4) NOT NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

