﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_SoybeanPulsePlanning] (
    [Statementcode]           INT            NOT NULL,
    [MarketLetterCode]        VARCHAR (50)   NOT NULL,
    [ProgramCode]             VARCHAR (50)   NOT NULL,
    [GroupLabel]              VARCHAR (100)  NOT NULL,
    [PartA_QualSales_CY]      MONEY          NOT NULL,
    [PartA_QualSales_LY]      MONEY          NOT NULL,
    [PartA_Reward_Sales]      MONEY          NOT NULL,
    [PartA_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [PartA_Reward]            MONEY          NOT NULL,
    [PartB_QualSales_CY]      MONEY          NOT NULL,
    [PartB_QualSales_LY]      MONEY          NOT NULL,
    [PartB_Reward_Sales]      MONEY          NOT NULL,
    [PartB_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [PartB_Reward]            MONEY          NOT NULL,
    [PartC_QualSales_CY]      MONEY          NOT NULL,
    [PartC_QualSales_LY]      MONEY          NOT NULL,
    [PartC_Reward_Sales]      MONEY          NOT NULL,
    [PartC_Reward_Percentage] DECIMAL (6, 4) NOT NULL,
    [PartC_Reward]            MONEY          NOT NULL,
    [Reward]                  MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [GroupLabel] ASC)
);

