﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_Lib_SalesForecast] (
    [StatementCode]     INT          NOT NULL,
    [MarketLetterCode]  VARCHAR (50) NOT NULL,
    [ProgramCode]       VARCHAR (50) NOT NULL,
    [RewardBrand]       VARCHAR (50) NOT NULL,
    [RewardBrand_Sales] MONEY        NOT NULL,
    [Reward]            MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

