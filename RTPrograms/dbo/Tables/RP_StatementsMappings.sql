﻿CREATE TABLE [dbo].[RP_StatementsMappings] (
    [StatementCode]      INT          NOT NULL,
    [RetailerCode]       VARCHAR (20) NOT NULL,
    [ActiveRetailerCode] VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RetailerCode] ASC)
);

