﻿CREATE TABLE [dbo].[RPWeb_MaximizeRewardMessages] (
    [StatementCode]         INT            NOT NULL,
    [MarketLetterCode]      NVARCHAR (50)  NOT NULL,
    [ProgramCode]           VARCHAR (50)   NOT NULL,
    [MaximizeRewardMessage] VARCHAR (1000) NULL
);

