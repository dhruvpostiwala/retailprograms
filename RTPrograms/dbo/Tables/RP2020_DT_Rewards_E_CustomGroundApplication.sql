﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_E_CustomGroundApplication] (
    [Statementcode]        INT             NOT NULL,
    [MarketLetterCode]     VARCHAR (50)    NOT NULL,
    [ProgramCode]          VARCHAR (50)    NOT NULL,
    [RewardBrand]          VARCHAR (100)   NOT NULL,
    [POG_Actual_Acres]     NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Acres]    NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Sales]    MONEY           NOT NULL,
    [CustApp_Actual_Acres] NUMERIC (18, 2) NOT NULL,
    [RewardBrand_CustApp]  NUMERIC (18, 2) NOT NULL,
    [Reward_Amount]        MONEY           NOT NULL,
    [Reward]               MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

