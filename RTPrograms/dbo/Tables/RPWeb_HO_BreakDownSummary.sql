﻿CREATE TABLE [dbo].[RPWeb_HO_BreakDownSummary] (
    [StatementCode]    INT          NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [ProgramCode]      VARCHAR (50) NOT NULL,
    [RetailerCode]     VARCHAR (20) NOT NULL,
    [FarmCode]         VARCHAR (20) NOT NULL,
    [Sales]            MONEY        NOT NULL,
    [OU_Sales]         MONEY        DEFAULT ((0)) NOT NULL,
    [OU_Reward]        MONEY        DEFAULT ((0)) NOT NULL,
    [Retailer_Reward]  MONEY        DEFAULT ((0)) NOT NULL,
    [FarmLevelSplit]   BIT          DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

