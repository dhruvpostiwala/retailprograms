﻿CREATE TABLE [dbo].[zMP_RP2021_InventoryAcres] (
    [RetailerCode] NVARCHAR (255)  NULL,
    [ProductCode]  NVARCHAR (255)  NULL,
    [Acres]        DECIMAL (18, 2) NULL
);

