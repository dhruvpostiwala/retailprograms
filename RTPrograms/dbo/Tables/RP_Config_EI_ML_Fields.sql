﻿CREATE TABLE [dbo].[RP_Config_EI_ML_Fields] (
    [ProgramCode]      VARCHAR (50) NOT NULL,
    [FieldCode]        VARCHAR (50) NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([ProgramCode] ASC, [FieldCode] ASC, [MarketLetterCode] ASC)
);

