﻿CREATE TABLE [dbo].[RPWeb_User_EI_FieldValues] (
    [StatementCode]    INT             NOT NULL,
    [FieldCode]        VARCHAR (50)    NOT NULL,
    [ProgramCode]      VARCHAR (50)    NOT NULL,
    [MarketLetterCode] VARCHAR (50)    NOT NULL,
    [FieldValue]       NUMERIC (18, 2) NOT NULL,
    [CalcFieldValue]   VARCHAR (3)     DEFAULT ('No') NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [FieldCode] ASC, [ProgramCode] ASC, [MarketLetterCode] ASC)
);

