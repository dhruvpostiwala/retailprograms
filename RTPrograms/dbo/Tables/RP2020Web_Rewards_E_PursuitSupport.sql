﻿CREATE TABLE [dbo].[RP2020Web_Rewards_E_PursuitSupport] (
    [Statementcode]         INT             NOT NULL,
    [MarketLetterCode]      VARCHAR (50)    NOT NULL,
    [ProgramCode]           VARCHAR (50)    NOT NULL,
    [Pursuit_Qty_CY]        NUMERIC (18, 2) NOT NULL,
    [Pursuit_Qty_LY]        NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Sales]     MONEY           DEFAULT ((0)) NOT NULL,
    [Pursuit_Reward_Amount] MONEY           NOT NULL,
    [Reward]                MONEY           NOT NULL,
    [QualBrand_Sales_CY]    MONEY           DEFAULT ((0)) NOT NULL,
    [QualBrand_Sales_LY]    MONEY           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

