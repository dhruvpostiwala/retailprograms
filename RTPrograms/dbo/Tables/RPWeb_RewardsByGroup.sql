﻿CREATE TABLE [dbo].[RPWeb_RewardsByGroup] (
    [StatementCode]          INT             NOT NULL,
    [MarketLetterCode]       VARCHAR (50)    NOT NULL,
    [ProgramCode]            VARCHAR (50)    NOT NULL,
    [GroupLabel]             VARCHAR (100)   NOT NULL,
    [ExcludedChemicalGroups] VARCHAR (250)   DEFAULT ('') NOT NULL,
    [RewardType]             VARCHAR (20)    NOT NULL,
    [Reward]                 DECIMAL (19, 4) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [GroupLabel] ASC, [ExcludedChemicalGroups] ASC)
);

