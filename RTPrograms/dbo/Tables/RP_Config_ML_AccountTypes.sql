﻿CREATE TABLE [dbo].[RP_Config_ML_AccountTypes] (
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [AccountType]      VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([MarketLetterCode] ASC, [AccountType] ASC)
);

