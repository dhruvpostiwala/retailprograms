﻿CREATE TABLE [dbo].[RP_Config_ChequeRuns] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [Season]        INT           NOT NULL,
    [Region]        VARCHAR (4)   NOT NULL,
    [ChequeRunName] VARCHAR (50)  NOT NULL,
    [Status]        VARCHAR (20)  NOT NULL,
    [Comments]      VARCHAR (300) NULL,
    [Label]         VARCHAR (150) NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Region] ASC, [ChequeRunName] ASC),
    CONSTRAINT [KEY1_Unique] UNIQUE NONCLUSTERED ([Season] ASC, [Region] ASC, [ChequeRunName] ASC)
);

