﻿CREATE TABLE [dbo].[RP2021Web_Rewards_E_PursuitSupport] (
    [Statementcode]      INT             NOT NULL,
    [MarketLetterCode]   VARCHAR (50)    NOT NULL,
    [ProgramCode]        VARCHAR (50)    NOT NULL,
    [RewardBrand]        VARCHAR (100)   NOT NULL,
    [RewardBrand_Qty]    NUMERIC (20, 4) NULL,
    [QualBrand_Sales_LY] MONEY           NOT NULL,
    [QualBrand_Sales_CY] MONEY           NOT NULL,
    [RewardPerCase]      MONEY           NOT NULL,
    [Reward]             MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

