﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_W_Nod_Duo_SCG] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [FarmCode]          VARCHAR (50)    NOT NULL,
    [RewardBrand_Acres] NUMERIC (18, 2) NOT NULL,
    [Reward_Per_Acre]   NUMERIC (18, 2) NULL,
    [Reward]            MONEY           NOT NULL,
    [RewardBrand_Sales] MONEY           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [FarmCode] ASC)
);

