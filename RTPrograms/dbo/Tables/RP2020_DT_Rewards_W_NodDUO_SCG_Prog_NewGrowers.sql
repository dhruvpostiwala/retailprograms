﻿CREATE TABLE [dbo].[RP2020_DT_Rewards_W_NodDUO_SCG_Prog_NewGrowers] (
    [StatementCode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [FarmCode]          VARCHAR (50)    NOT NULL,
    [RewardBrand_Acres] NUMERIC (18, 2) NOT NULL,
    [Reward_Perc]       NUMERIC (18, 2) NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [FarmCode] ASC)
);

