﻿CREATE TABLE [dbo].[RP_ActivityDependencies] (
    [ActivityID]   FLOAT (53) NOT NULL,
    [DependencyID] FLOAT (53) NOT NULL,
    PRIMARY KEY CLUSTERED ([ActivityID] ASC, [DependencyID] ASC)
);

