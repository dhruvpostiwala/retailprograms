﻿CREATE TABLE [dbo].[RPWeb_HTML_PDF] (
    [StatementCode] INT           NOT NULL,
    [HTML_TEXT]     NTEXT         NULL,
    [Comments]      VARCHAR (100) NOT NULL,
    [Status]        VARCHAR (100) NULL,
    [CreatedDate]   DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedDate]  DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_GPWeb_PDF_HTML] PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

