﻿CREATE TABLE [dbo].[RP_DT_Scenarios] (
    [StatementCode] INT          NOT NULL,
    [RetailerCode]  VARCHAR (20) NOT NULL,
    [DataSetCode]   INT          NOT NULL,
    [Season]        INT          NOT NULL,
    [ScenarioCode]  VARCHAR (20) NOT NULL,
    [Status]        VARCHAR (20) NOT NULL,
    [DateModified]  DATETIME     NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RetailerCode] ASC)
);

