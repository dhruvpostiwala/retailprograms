﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_SegmentSelling] (
    [StatementCode]                INT            NOT NULL,
    [MarketLetterCode]             VARCHAR (50)   NOT NULL,
    [ProgramCode]                  VARCHAR (50)   NOT NULL,
    [RetailerCode]                 VARCHAR (50)   NOT NULL,
    [RewardBrand]                  VARCHAR (50)   NOT NULL,
    [RewardBrand_Sales]            MONEY          NOT NULL,
    [Reward_Percentage]            DECIMAL (6, 3) DEFAULT ((0)) NOT NULL,
    [Reward]                       MONEY          DEFAULT ((0)) NOT NULL,
    [SegmentsAVG_CY]               FLOAT (53)     DEFAULT ((0)) NULL,
    [SegmentsAVG_LY]               FLOAT (53)     DEFAULT ((0)) NULL,
    [Growth]                       NUMERIC (6, 3) DEFAULT ((0)) NULL,
    [Reward_Percentage_A]          NUMERIC (6, 3) DEFAULT ((0)) NULL,
    [Reward_A]                     MONEY          DEFAULT ((0)) NULL,
    [Reward_Percentage_B]          NUMERIC (6, 3) DEFAULT ((0)) NULL,
    [Reward_B]                     MONEY          DEFAULT ((0)) NULL,
    [Reward_Percentage_Adjustment] NUMERIC (6, 3) DEFAULT ((0)) NULL,
    [Reward_Adjustment]            MONEY          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [RewardBrand] ASC)
);

