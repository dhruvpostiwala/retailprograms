﻿CREATE TABLE [dbo].[zCLL2019_Sum] (
    [link_code]           NVARCHAR (255) NULL,
    [statement_type]      NVARCHAR (255) NULL,
    [season]              FLOAT (53)     NULL,
    [RetailerCode]        NVARCHAR (255) NULL,
    [Retailer]            NVARCHAR (255) NULL,
    [OperatingUnit]       NVARCHAR (255) NULL,
    [FarmName]            NVARCHAR (255) NULL,
    [FarmCode]            NVARCHAR (255) NULL,
    [Eligible]            NVARCHAR (255) NULL,
    [num_commitments]     FLOAT (53)     NULL,
    [committed_acres]     FLOAT (53)     NULL,
    [herbicide_pog_acres] FLOAT (53)     NULL,
    [matched_acres]       FLOAT (53)     NULL,
    [reward]              FLOAT (53)     NULL,
    [comm_reward]         FLOAT (53)     NULL,
    [Locked Status]       NVARCHAR (255) NULL,
    [last_modified]       DATETIME       NULL
);

