﻿CREATE TABLE [dbo].[RP_Seeding_InventoryAcres] (
    [Season]       INT             NOT NULL,
    [RetailerCode] VARCHAR (20)    NOT NULL,
    [ProductCode]  VARCHAR (50)    NOT NULL,
    [Acres]        DECIMAL (18, 2) NULL,
    [Comments]     VARCHAR (1000)  NULL
);

