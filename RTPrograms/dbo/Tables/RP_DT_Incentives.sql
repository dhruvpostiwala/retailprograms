﻿CREATE TABLE [dbo].[RP_DT_Incentives] (
    [StatementCode] INT          NOT NULL,
    [RetailerCode]  VARCHAR (20) NOT NULL,
    [IncentiveCode] VARCHAR (20) NOT NULL,
    [Amount]        MONEY        NULL,
    [Region]        VARCHAR (4)  NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RetailerCode] ASC, [IncentiveCode] ASC)
);

