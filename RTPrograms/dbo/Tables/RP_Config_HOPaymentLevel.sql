﻿CREATE TABLE [dbo].[RP_Config_HOPaymentLevel] (
    [Level5Code] VARCHAR (30) NOT NULL,
    [Season]     INT          NOT NULL,
    [Summary]    VARCHAR (3)  DEFAULT ('No') NOT NULL,
    PRIMARY KEY CLUSTERED ([Level5Code] ASC, [Season] ASC)
);

