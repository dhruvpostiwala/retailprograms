﻿CREATE TABLE [dbo].[RPWeb_Reports_Exposure_AccrualSummary] (
    [ExposureID]       INT          NOT NULL,
    [Region]           VARCHAR (4)  NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [RewardCode]       VARCHAR (50) NOT NULL,
    [AccrualAmount]    MONEY        NOT NULL,
    [RewardAmount]     MONEY        DEFAULT ((0)) NOT NULL,
    [PaidToDate]       MONEY        DEFAULT ((0)) NOT NULL,
    [CurrentPayment]   MONEY        DEFAULT ((0)) NOT NULL,
    [NextPayment]      INT          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([ExposureID] ASC, [Region] ASC, [MarketLetterCode] ASC, [RewardCode] ASC),
    CONSTRAINT [FK_ExposureID] FOREIGN KEY ([ExposureID]) REFERENCES [dbo].[RPWeb_Reports_Exposure] ([ExposureID])
);

