﻿CREATE TABLE [dbo].[RP_DT_ML_ELG_Sales] (
    [Statementcode]    INT             NOT NULL,
    [RetailerCode]     VARCHAR (20)    NOT NULL,
    [MarketLetterCode] VARCHAR (50)    NOT NULL,
    [GroupLabel]       VARCHAR (100)   NOT NULL,
    [Pricing_Type]     VARCHAR (3)     NOT NULL,
    [Sales_CY]         NUMERIC (18, 2) NOT NULL,
    [Sales_LY1]        NUMERIC (18, 2) NOT NULL,
    [Sales_LY2]        NUMERIC (18, 2) NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [RetailerCode] ASC, [MarketLetterCode] ASC, [GroupLabel] ASC)
);

