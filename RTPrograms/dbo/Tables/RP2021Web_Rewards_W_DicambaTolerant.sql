﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_DicambaTolerant] (
    [StatementCode]           INT             NOT NULL,
    [MarketLetterCode]        VARCHAR (50)    NOT NULL,
    [ProgramCode]             VARCHAR (50)    NOT NULL,
    [RetailerCode]            VARCHAR (20)    NOT NULL,
    [FarmCode]                VARCHAR (20)    NOT NULL,
    [Engenia_Acres]           DECIMAL (18, 4) NOT NULL,
    [ViperADV_Acres]          DECIMAL (18, 4) NOT NULL,
    [Matched_Acres]           DECIMAL (18, 4) NOT NULL,
    [Reward]                  MONEY           NOT NULL,
    [Nodulator_Acres]         DECIMAL (18, 4) DEFAULT ((0)) NOT NULL,
    [Nodulator_Matched_Acres] DECIMAL (18, 4) DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RetailerCode] ASC, [FarmCode] ASC)
);

