﻿CREATE TABLE [dbo].[RP2020Web_Rewards_W_AG_WS_TransitProg] (
    [StatementCode]    INT          NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [ProgramCode]      VARCHAR (50) NOT NULL,
    [Wholesale_Sales]  MONEY        NOT NULL,
    [Reward]           MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC)
);

