﻿CREATE TABLE [dbo].[RP_Config_WebTableNames] (
    [Season]                  INT           NOT NULL,
    [Region]                  VARCHAR (4)   NOT NULL,
    [TableName]               VARCHAR (200) NOT NULL,
    [DT_TableName]            VARCHAR (200) NULL,
    [TransferStatementAction] VARCHAR (3)   NOT NULL,
    [LockStatementAction]     VARCHAR (3)   NOT NULL,
    [CleanupAction]           VARCHAR (3)   NOT NULL,
    [UseCaseCreateAction]     VARCHAR (3)   NOT NULL,
    [Comments]                VARCHAR (500) NULL,
    [UpdateDataSetCodes]      VARCHAR (3)   DEFAULT ('No') NOT NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [TableName] ASC)
);

