﻿CREATE TABLE [dbo].[RP_DT_Transactions] (
    [StatementCode]        INT             NOT NULL,
    [DataSetCode]          INT             NOT NULL,
    [StatementTransNumber] INT             NOT NULL,
    [RetailerCode]         VARCHAR (20)    NOT NULL,
    [FarmCode]             VARCHAR (20)    NOT NULL,
    [TransCode]            VARCHAR (50)    NOT NULL,
    [InvoiceNumber]        VARCHAR (50)    NOT NULL,
    [InvoiceDate]          DATETIME        NOT NULL,
    [ProductCode]          VARCHAR (20)    NOT NULL,
    [Quantity]             FLOAT (53)      NOT NULL,
    [Acres]                FLOAT (53)      NOT NULL,
    [BASFSeason]           INT             NOT NULL,
    [Season]               INT             NOT NULL,
    [ChemicalGroup]        VARCHAR (50)    NOT NULL,
    [Price_SRP]            NUMERIC (18, 2) NOT NULL,
    [Price_SDP]            NUMERIC (18, 2) NOT NULL,
    [InRadius]             BIT             DEFAULT ((0)) NOT NULL,
    [CFStatus]             VARCHAR (50)    NULL,
    [Price_SWP]            NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [DataSetCode_L5]       INT             NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [StatementTransNumber] ASC)
);

