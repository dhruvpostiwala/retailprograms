﻿CREATE TABLE [dbo].[RP_Seeding_APAPayments] (
    [ID]           INT          IDENTITY (1, 1) NOT NULL,
    [RetailerCode] VARCHAR (20) NOT NULL,
    [Season]       INT          NOT NULL,
    [RewardCode]   VARCHAR (50) NOT NULL,
    [Amount]       MONEY        NOT NULL,
    [DateCreated]  DATETIME     DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([RetailerCode] ASC, [Season] DESC, [RewardCode] ASC)
);

