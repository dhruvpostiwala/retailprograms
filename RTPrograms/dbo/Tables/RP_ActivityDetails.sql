﻿CREATE TABLE [dbo].[RP_ActivityDetails] (
    [ActivityID]     FLOAT (53)     NOT NULL,
    [ActivitySeason] INT            NOT NULL,
    [ActivityGroup]  VARCHAR (20)   NOT NULL,
    [ActivityType]   VARCHAR (50)   NOT NULL,
    [ActivityName]   VARCHAR (50)   NOT NULL,
    [ActivityAction] VARCHAR (8000) NULL,
    [ActivityStatus] VARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([ActivityID] ASC)
);

