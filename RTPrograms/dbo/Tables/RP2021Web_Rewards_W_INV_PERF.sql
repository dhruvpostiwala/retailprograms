﻿CREATE TABLE [dbo].[RP2021Web_Rewards_W_INV_PERF] (
    [Statementcode]     INT             NOT NULL,
    [MarketLetterCode]  VARCHAR (50)    NOT NULL,
    [ProgramCode]       VARCHAR (50)    NOT NULL,
    [QualBrand_Qty]     NUMERIC (18, 2) NOT NULL,
    [RewardBrand_Sales] MONEY           NOT NULL,
    [RewardBrand_Qty]   NUMERIC (18, 2) NOT NULL,
    [Sales_Target]      MONEY           NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4)  NOT NULL,
    [Reward]            MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC)
);

