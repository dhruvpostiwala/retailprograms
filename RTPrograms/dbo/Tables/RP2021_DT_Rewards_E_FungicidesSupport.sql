﻿CREATE TABLE [dbo].[RP2021_DT_Rewards_E_FungicidesSupport] (
    [StatementCode]      INT             NOT NULL,
    [MarketLetterCode]   VARCHAR (50)    NOT NULL,
    [ProgramCode]        VARCHAR (50)    NOT NULL,
    [RewardBrand]        VARCHAR (100)   NOT NULL,
    [QualBrand_Sales_CY] MONEY           NOT NULL,
    [QualBrand_Sales_LY] MONEY           NOT NULL,
    [QualBrand_Acres_CY] DECIMAL (20, 4) NOT NULL,
    [RewardPerAcre]      MONEY           NOT NULL,
    [Reward]             MONEY           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

