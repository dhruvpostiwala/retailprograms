﻿CREATE TABLE [dbo].[RPWeb_RetailerDataSubmissionDataAll] (
    [StatementCode]     INT          NOT NULL,
    [RP_ID]             INT          NOT NULL,
    [Season]            VARCHAR (4)  NOT NULL,
    [DocCode]           VARCHAR (20) NOT NULL,
    [RetailerCode]      VARCHAR (20) NOT NULL,
    [DSCode]            VARCHAR (20) NULL,
    [Status]            VARCHAR (20) NULL,
    [DateReceived]      DATETIME     NULL,
    [MultipleRetailers] VARCHAR (3)  NULL,
    [DataType]          VARCHAR (50) NULL,
    [MonthOfSubmission] VARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [RP_ID] ASC)
);

