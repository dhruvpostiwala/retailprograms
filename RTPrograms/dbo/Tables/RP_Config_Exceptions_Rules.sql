﻿CREATE TABLE [dbo].[RP_Config_Exceptions_Rules] (
    [ExcID]       INT           NOT NULL,
    [FieldName]   VARCHAR (100) NOT NULL,
    [FieldNumber] FLOAT (53)    NOT NULL,
    [FieldText]   VARCHAR (100) NOT NULL
);

