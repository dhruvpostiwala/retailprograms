﻿CREATE TABLE [dbo].[RP_Config_Forecast_RewardsDisplay] (
    [Season]           INT            NOT NULL,
    [Region]           VARCHAR (4)    NOT NULL,
    [RowNumber]        NUMERIC (5, 2) NOT NULL,
    [RowType]          VARCHAR (50)   NOT NULL,
    [MarketLetterCode] VARCHAR (50)   NOT NULL,
    [RewardCode]       VARCHAR (50)   NOT NULL,
    [SubRowCode]       VARCHAR (50)   NOT NULL,
    [Label]            VARCHAR (120)  NOT NULL,
    [DisplayInput]     VARCHAR (3)    NOT NULL,
    [DisplayQualifier] VARCHAR (3)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Season] ASC, [Region] ASC, [RowNumber] ASC)
);

