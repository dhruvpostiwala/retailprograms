﻿CREATE TABLE [dbo].[RPWeb_Est_Exceptions] (
    [Statementcode] INT   NOT NULL,
    [Amount]        MONEY NOT NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC)
);

