﻿CREATE TABLE [dbo].[RPWeb_Est_Incentives] (
    [Statementcode]     INT   NOT NULL,
    [Amount]            MONEY NOT NULL,
    [SRC_Statementcode] INT   NULL,
    PRIMARY KEY CLUSTERED ([Statementcode] ASC)
);

