﻿CREATE TABLE [dbo].[RP_Seeding_InternalExceptions] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [RetailerCode]  VARCHAR (20)   NOT NULL,
    [Season]        INT            NOT NULL,
    [RewardCode]    VARCHAR (50)   NOT NULL,
    [Status]        VARCHAR (20)   NOT NULL,
    [Amount]        MONEY          NOT NULL,
    [StatementCode] INT            NULL,
    [PaymentDesc]   VARCHAR (250)  NULL,
    [PaymentDate]   DATE           NULL,
    [PostedDate]    DATE           NULL,
    [Comments]      VARCHAR (1000) NULL,
    [DateCreated]   DATETIME       DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME       NOT NULL,
    [PostedBy]      VARCHAR (150)  NULL,
    PRIMARY KEY CLUSTERED ([RetailerCode] ASC, [Season] DESC, [RewardCode] ASC)
);

