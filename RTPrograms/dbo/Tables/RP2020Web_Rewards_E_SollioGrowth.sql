﻿CREATE TABLE [dbo].[RP2020Web_Rewards_E_SollioGrowth] (
    [StatementCode]     INT            NOT NULL,
    [MarketLetterCode]  VARCHAR (50)   NOT NULL,
    [ProgramCode]       VARCHAR (50)   NOT NULL,
    [LYSales]           MONEY          NOT NULL,
    [CYSales]           MONEY          NOT NULL,
    [RewardBrand]       VARCHAR (100)  NOT NULL,
    [RewardBrand_Sales] MONEY          NOT NULL,
    [Reward_Percentage] NUMERIC (6, 4) NULL,
    [Reward]            MONEY          NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardBrand] ASC)
);

