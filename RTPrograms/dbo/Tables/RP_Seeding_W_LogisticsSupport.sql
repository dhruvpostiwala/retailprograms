﻿CREATE TABLE [dbo].[RP_Seeding_W_LogisticsSupport] (
    [RetailerCode] VARCHAR (50)   NOT NULL,
    [Season]       INT            NOT NULL,
    [BrandSegment] VARCHAR (100)  NOT NULL,
    [March_Take]   NUMERIC (6, 4) NOT NULL,
    [March_Reward] NUMERIC (6, 4) NOT NULL,
    [April_Take]   NUMERIC (6, 4) NOT NULL,
    [April_Reward] NUMERIC (6, 4) NOT NULL,
    [May_Take]     NUMERIC (6, 4) NOT NULL,
    [May_Reward]   NUMERIC (6, 4) NOT NULL,
    [June_Take]    NUMERIC (6, 4) NOT NULL,
    [June_Reward]  NUMERIC (6, 4) NOT NULL,
    [July_Take]    NUMERIC (6, 4) NOT NULL,
    [July_Reward]  NUMERIC (6, 4) NOT NULL,
    [Comments]     VARCHAR (1000) NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([RetailerCode] ASC, [Season] ASC, [BrandSegment] ASC)
);

