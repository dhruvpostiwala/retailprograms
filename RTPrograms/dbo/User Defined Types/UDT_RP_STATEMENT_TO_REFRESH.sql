﻿CREATE TYPE [dbo].[UDT_RP_STATEMENT_TO_REFRESH] AS TABLE (
    [StatementCode]     INT          NOT NULL,
    [StatementLevel]    VARCHAR (20) NOT NULL,
    [Season]            INT          NULL,
    [RetailerCode]      VARCHAR (20) NULL,
    [DataSetCode]       INT          DEFAULT ((0)) NOT NULL,
    [DataSetCode_L5]    INT          DEFAULT ((0)) NOT NULL,
    [SRC_StatementCode] INT          NULL,
    [Summary]           VARCHAR (3)  DEFAULT ('No') NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC));

