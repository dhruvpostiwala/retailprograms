﻿CREATE TYPE [dbo].[UDT_RP_SKU_LEVEL_SALES] AS TABLE (
    [MarketLetterCode] VARCHAR (50)    NOT NULL,
    [MarketLetterName] VARCHAR (200)   NULL,
    [Sequence]         INT             NOT NULL,
    [ChemicalGroup]    VARCHAR (50)    NOT NULL,
    [ProductCode]      VARCHAR (65)    NOT NULL,
    [ProductName]      VARCHAR (200)   NULL,
    [LYQuantity]       DECIMAL (18, 2) NULL,
    [LYSales]          MONEY           NULL,
    [LYEligibleQty]    DECIMAL (18, 2) NULL,
    [LYEligibleSales]  MONEY           NULL,
    [CYQuantity]       DECIMAL (18, 2) NULL,
    [CYSales]          MONEY           NULL,
    [CYEligibleQty]    DECIMAL (18, 2) NULL,
    [CYEligibleSales]  MONEY           NULL,
    [Forecast_Sales]   MONEY           NULL,
    PRIMARY KEY CLUSTERED ([MarketLetterCode] ASC, [ChemicalGroup] ASC, [ProductCode] ASC, [Sequence] ASC));

