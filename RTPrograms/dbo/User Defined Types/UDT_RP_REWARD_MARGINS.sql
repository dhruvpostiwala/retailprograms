﻿CREATE TYPE [dbo].[UDT_RP_REWARD_MARGINS] AS TABLE (
    [StatementCode]             INT            NOT NULL,
    [MarketLetterCode]          VARCHAR (50)   NOT NULL,
    [ProgramCode]               VARCHAR (50)   NOT NULL,
    [ChemicalGroup]             VARCHAR (50)   NOT NULL,
    [RewardBrand_Sales]         MONEY          DEFAULT ((0)) NOT NULL,
    [Rewarded_Sales]            MONEY          DEFAULT ((0)) NOT NULL,
    [Reward]                    MONEY          DEFAULT ((0)) NOT NULL,
    [Reward_Percentage]         NUMERIC (6, 4) DEFAULT ((0)) NOT NULL,
    [Overall_Reward_Percentage] NUMERIC (6, 4) DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [ChemicalGroup] ASC));

