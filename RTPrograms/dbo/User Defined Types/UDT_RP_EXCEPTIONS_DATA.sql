﻿CREATE TYPE [dbo].[UDT_RP_EXCEPTIONS_DATA] AS TABLE (
    [StatementCode]    INT            NOT NULL,
    [MarketLetterCode] VARCHAR (50)   NOT NULL,
    [ProgramCode]      VARCHAR (100)  NOT NULL,
    [RewardCode]       VARCHAR (50)   NOT NULL,
    [Level5Code]       VARCHAR (20)   NOT NULL,
    [cy_sales_A]       MONEY          DEFAULT ((0)) NOT NULL,
    [cy_sales_B]       MONEY          DEFAULT ((0)) NOT NULL,
    [current_margin_A] DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [current_margin_B] DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [current_reward_A] MONEY          DEFAULT ((0)) NOT NULL,
    [current_reward_B] MONEY          DEFAULT ((0)) NOT NULL,
    [next_margin_A]    DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [next_margin_B]    DECIMAL (6, 4) DEFAULT ((0)) NOT NULL,
    [reward_margins_a] NVARCHAR (255) DEFAULT ('[]') NOT NULL,
    [reward_margins_b] NVARCHAR (255) DEFAULT ('[]') NOT NULL,
    [exception_type]   VARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [ProgramCode] ASC, [RewardCode] ASC));

