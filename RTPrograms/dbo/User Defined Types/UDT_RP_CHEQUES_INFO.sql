﻿CREATE TYPE [dbo].[UDT_RP_CHEQUES_INFO] AS TABLE (
    [StatementCode] INT           NOT NULL,
    [ChequeDocCode] VARCHAR (25)  NOT NULL,
    [RetailerCode]  VARCHAR (20)  NOT NULL,
    [VersionType]   VARCHAR (25)  NOT NULL,
    [PayableToName] VARCHAR (150) NOT NULL,
    [ChequeType]    VARCHAR (25)  NOT NULL,
    [ChequeNumber]  VARCHAR (10)  NOT NULL,
    [ChequeDate]    DATETIME      NULL,
    [ChequeAmount]  MONEY         NOT NULL,
    [ChequeStatus]  VARCHAR (50)  NOT NULL,
    [SendTo]        VARCHAR (75)  NOT NULL,
    [AmtRequested]  MONEY         NOT NULL,
    [AmtReceived]   MONEY         NOT NULL,
    [AmtPaid]       MONEY         NOT NULL,
    [Unlocked]      BIT           NOT NULL,
    [Voided]        BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [ChequeDocCode] ASC));

