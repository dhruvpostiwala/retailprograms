﻿CREATE TYPE [dbo].[RP_EDITHISTORY_OPTIMIZED] AS TABLE (
    [EHCode]          INT           NOT NULL,
    [StatementCode]   INT           NOT NULL,
    [EHLinkCode]      INT           NOT NULL,
    [TagData]         VARCHAR (100) NULL,
    [EHDisplay]       VARCHAR (3)   NULL,
    [EHEntryType]     VARCHAR (50)  NULL,
    [EHFieldName]     VARCHAR (50)  NULL,
    [EHOriginalValue] VARCHAR (MAX) NULL,
    [EHNewValue]      VARCHAR (MAX) NULL,
    [EHMessage]       VARCHAR (MAX) NULL,
    [EHGrouping]      INT           NULL);

