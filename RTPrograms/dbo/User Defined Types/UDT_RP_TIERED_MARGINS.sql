﻿CREATE TYPE [dbo].[UDT_RP_TIERED_MARGINS] AS TABLE (
    [Level5Code]        VARCHAR (20)    NOT NULL,
    [Min_Value]         DECIMAL (18, 4) NOT NULL,
    [Max_Value]         DECIMAL (18, 4) NOT NULL,
    [Reward_Percentage] DECIMAL (6, 4)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Level5Code] ASC, [Min_Value] ASC, [Max_Value] ASC, [Reward_Percentage] ASC));

