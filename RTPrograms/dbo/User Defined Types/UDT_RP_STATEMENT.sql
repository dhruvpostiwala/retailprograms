﻿CREATE TYPE [dbo].[UDT_RP_STATEMENT] AS TABLE (
    [StatementCode] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC));

