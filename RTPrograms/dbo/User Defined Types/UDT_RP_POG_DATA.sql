﻿CREATE TYPE [dbo].[UDT_RP_POG_DATA] AS TABLE (
    [DealerCode]          VARCHAR (17)  NOT NULL,
    [Productcode]         VARCHAR (100) NOT NULL,
    [Forecast]            FLOAT (53)    NULL,
    [Projected]           FLOAT (53)    NULL,
    [LastYear]            FLOAT (53)    NULL,
    [TwoYearsAgo]         FLOAT (53)    NULL,
    [EndingInventoryPerc] FLOAT (53)    NULL,
    [ReportedInventory]   FLOAT (53)    NULL,
    [Q4]                  FLOAT (53)    NULL,
    PRIMARY KEY CLUSTERED ([DealerCode] ASC, [Productcode] ASC));

