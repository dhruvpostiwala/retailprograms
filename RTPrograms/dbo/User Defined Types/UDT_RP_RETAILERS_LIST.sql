﻿CREATE TYPE [dbo].[UDT_RP_RETAILERS_LIST] AS TABLE (
    [RetailerCode]       VARCHAR (20) NOT NULL,
    [RetailerType]       VARCHAR (20) NOT NULL,
    [Level2Code]         VARCHAR (20) NOT NULL,
    [Level5Code]         VARCHAR (20) NOT NULL,
    [ActiveRetailerCode] VARCHAR (20) NOT NULL,
    [Status]             VARCHAR (20) NOT NULL,
    [StatementLevel]     VARCHAR (20) NOT NULL,
    [MappingLevel]       VARCHAR (10) NOT NULL,
    [OldLevel2Code]      VARCHAR (20) DEFAULT ('') NOT NULL,
    [OldLevel5Code]      VARCHAR (20) DEFAULT ('') NOT NULL,
    [HierarchyChanged]   BIT          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([RetailerCode] ASC, [StatementLevel] ASC));

