﻿CREATE TYPE [dbo].[UDT_RP_STATEMENT_INFO] AS TABLE (
    [StatementCode] INT          NOT NULL,
    [Season]        INT          NOT NULL,
    [Region]        VARCHAR (4)  NOT NULL,
    [StatementType] VARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [Season] ASC, [Region] ASC, [StatementType] ASC));

