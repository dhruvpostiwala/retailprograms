﻿CREATE TYPE [dbo].[UDT_RP_PAYMENTINFO] AS TABLE (
    [StatementCode]    INT          NOT NULL,
    [MarketLetterCode] VARCHAR (50) NOT NULL,
    [RewardCode]       VARCHAR (50) NOT NULL,
    [RewardAmount]     MONEY        NOT NULL,
    PRIMARY KEY CLUSTERED ([StatementCode] ASC, [MarketLetterCode] ASC, [RewardCode] ASC));

