﻿


CREATE   PROCEDURE [dbo].[RPST_Agent_ReconPaid] @SEASON INT,  @USERNAME VARCHAR(150), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	/* 
		@STATEMENTCODES PARAM CONTAINS LOCKED STATEMENTCODES 
		THIS ACTION WILL BE EXECUTED ON MULTIPLE STATEMENT CODES
	*/


	
	SET NOCOUNT ON;

	DECLARE @EDIT_HISTORY RP_EDITHISTORY;
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;
	DECLARE @UNLOCKED_STATEMENTS AS UDT_RP_STATEMENT;
	
	-- DECLARE @LOCKED_STATEMENTS UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @LOCKED_STATEMENTS TABLE(
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		SRC_StatementCode INT NOT NULL,
		Summary VARCHAR(3) NOT NULL
	)

	BEGIN TRY

	INSERT INTO @STATEMENTS(STATEMENTCODE)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')

	DELETE @STATEMENTS WHERE [StatementCode] NOT IN (SELECT StatementCode FROM RPWeb_Statements WHERE [Season]=@Season AND [Status]='Active' AND [VersionType]='Locked')

	INSERT INTO @LOCKED_STATEMENTS(StatementCode, DataSetCode, SRC_StatementCode,Summary)
	SELECT StatementCode,DataSetCode,SRC_StatementCode,Summary
	FROM TVF_Get_LockedStatementCodesToRefresh(@STATEMENTS) 
	WHERE SUMMARY='No'

	INSERT INTO @UNLOCKED_STATEMENTS(StatementCode)
	SELECT SRC_StatementCode FROM @LOCKED_STATEMENTS

	-- Convert locked Statements to Archive
	UPDATE RPWeb_Statements
	SET VersionType = 'Archive'
	WHERE VersionType='Locked' AND StatementCode IN (SELECT Statementcode FROM @LOCKED_STATEMENTS)
	
	INSERT INTO RPWeb_StatementInformation(StatementCode, FieldName, FieldValue, Comments, FieldCategory)
	SELECT T1.StatementCode, 'Payment_Status', 'Paid', NULL, NULL
	FROM @LOCKED_STATEMENTS T1
		LEFT JOIN RPWeb_StatementInformation STI
		ON STI.Statementcode=T1.Statementcode AND STI.FieldName='Payment_Status'
	WHERE STI.Statementcode IS NULL

	-- Move the Statement back to "Open" status from "Submitted For RC Reconciliation"
	UPDATE RPWEb_StatementInformation
	SET FieldValue = 'Open'
	OUTPUT inserted.[StatementCode],inserted.[StatementCode],'RC RECON Final - Unlock','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	WHERE FieldName='Status' AND FieldValue='Submitted For RC Reconciliation' AND StatementCode IN (SELECT Statementcode FROM @UNLOCKED_STATEMENTS)

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	

	EXEC RP_RefreshPaymentInformation @SEASON, 'ACTUAL', @UNLOCKED_STATEMENTS

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	SELECT 'success' AS [Status], '' AS Error_Message

END




