﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_InventoryManagement] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SEGMENT_A_REWARDS TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		CPP_Sales Money NOT NULL,
		Liberty_Sales Money NOT NULL,
		Centurion_Sales Money NOT NULL,
		CPP_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Liberty_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Centurion_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		CPP_Reward Money NOT NULL DEFAULT 0,
		Liberty_Reward Money NOT NULL DEFAULT 0,
		Centurion_Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @RewardPercentage FLOAT = 0.01;

	-- SEGMENT A
	-- Get sales data for CPP, Liberty, Centurion
	INSERT INTO @SEGMENT_A_REWARDS(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,CPP_Sales,Liberty_Sales,Centurion_Sales)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel,
	SUM(IIF(TX.GroupLabel NOT IN ('LIBERTY 150', 'CENTURION'), TX.Price_CY, 0)) AS CPP_Sales,
	SUM(IIF(TX.GroupLabel = 'LIBERTY 150', TX.Price_CY, 0)) AS Liberty_Sales,
	SUM(IIF(TX.GroupLabel = 'CENTURION', TX.Price_CY, 0)) AS Centurion_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode = ST.StatementCode
	WHERE tx.MarketLetterCode='ML2021_W_CPP' AND  tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ALL_CPP_BRANDS' aND tx.Level5Code <> 'D520062427'
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
	
	-- Update the reward percentages for CPP, Liberty, Centurion
	UPDATE @SEGMENT_A_REWARDS SET CPP_Percentage = @RewardPercentage, CPP_Reward = CPP_Sales * @RewardPercentage
	UPDATE @SEGMENT_A_REWARDS SET Liberty_Percentage = @RewardPercentage, Liberty_Reward = Liberty_Sales * @RewardPercentage
	UPDATE @SEGMENT_A_REWARDS SET Centurion_Percentage = @RewardPercentage, Centurion_Reward = Centurion_Sales * @RewardPercentage

	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[CPP_Reward] = 0, [Centurion_Reward] = 0, [Liberty_Reward] = 0
	FROM	@SEGMENT_A_REWARDS T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(CPP_Reward + Liberty_Reward + Centurion_Reward) AS Statement_Reward
		FROM	@SEGMENT_A_REWARDS
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0


	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')	
	BEGIN
		UPDATE T1
		SET [CPP_Reward] = [CPP_Reward] * ISNULL(EI1.FieldValue/100,1),
			[Liberty_Reward] = [Liberty_Reward] * ISNULL(EI2.FieldValue/100,1),
			[Centurion_Reward] = [Centurion_Reward] * ISNULL(EI3.FieldValue/100,1)
		FROM @SEGMENT_A_REWARDS T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI1
			ON EI1.StatementCode=T1.StatementCode AND EI1.MarketLetterCode=T1.MarketLetterCode  AND EI1.FieldCode='Radius_Percentage_CPP'

			LEFT JOIN RPWeb_User_EI_FieldValues EI2
			ON EI2.StatementCode=T1.StatementCode AND EI2.MarketLetterCode=T1.MarketLetterCode  AND EI2.FieldCode='Radius_Percentage_Liberty'

			LEFT JOIN RPWeb_User_EI_FieldValues EI3
			ON EI3.StatementCode=T1.StatementCode AND EI3.MarketLetterCode=T1.MarketLetterCode  AND EI3.FieldCode='Radius_Percentage_Centurion'
		WHERE [CPP_Reward] > 0 OR [Liberty_Reward] > 0 OR [Centurion_Reward] > 0
	END



	-- SEGMENT B REWARD -- LINE COMPANIES ONLY
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN

		/*
			Cargill (D0000107): 2%
			UFA (D0000244): 2%
			Richardson (D0000137): 2.5%
			Nutrien (D520062427): 2%


		*/

		DROP TABLE IF EXISTS #SEGMENT_B_REWARDS	
		SELECT ST.StatementCode, S.Retailercode, tx.MarketLetterCode, @PROGRAM_CODE AS ProgramCode, 'InVigor' AS RewardBrand,	SUM(TX.Price_CY) AS Sales
			,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
			,CAST(0 AS MONEY) AS Reward
		INTO #SEGMENT_B_REWARDS
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements S						ON S.StatementCode=ST.StatementCode
			INNER JOIN RP2021_DT_Sales_Consolidated TX		ON TX.StatementCode = ST.StatementCode			
		WHERE S.StatementLevel='LEVEL 5' AND S.RetailerCode IN ('D0000107','D0000244','D0000137','D520062427')
			AND tx.MarketLetterCode='ML2021_W_INV' AND tx.ProgramCode=@PROGRAM_CODE AND TX.GroupType='REWARD_BRANDS'				
		GROUP BY ST.StatementCode, S.Retailercode, tx.MarketLetterCode
		--RP2021_W_INVENTORY_MGMT
		
		UPDATE T1
		SET Reward_Percentage =  T2.Reward_Percentage
			,Reward = Sales * T2.Reward_Percentage
		FROM #SEGMENT_B_REWARDS T1
			INNER JOIN (
				SELECT 'D0000107' AS Retailercode, 0.02 AS Reward_Percentage
					UNION
				SELECT 'D0000244' AS Retailercode, 0.02 AS Reward_Percentage
					UNION
				SELECT 'D0000137' AS Retailercode, 0.025 AS Reward_Percentage
					UNION
				SELECT 'D520062427' AS Retailercode, 0.02 AS Reward_Percentage
			) T2
			ON T2.RetailerCode=T1.RetailerCode
	END

	DELETE FROM RP2021_DT_Rewards_W_InventoryManagement WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2021_DT_Rewards_W_InventoryManagement(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,CPP_Sales,Liberty_Sales,Centurion_Sales,CPP_Percentage,Liberty_Percentage,Centurion_Percentage,CPP_Reward,Liberty_Reward,Centurion_Reward,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,CPP_Sales,Liberty_Sales,Centurion_Sales,CPP_Percentage,Liberty_Percentage,Centurion_Percentage,CPP_Reward,Liberty_Reward,Centurion_Reward,(CPP_Reward + Liberty_Reward + Centurion_Reward) [Reward]
	FROM @SEGMENT_A_REWARDS
	   	
	IF @STATEMENT_TYPE = 'ACTUAL'
		INSERT INTO RP2021_DT_Rewards_W_InventoryManagement(StatementCode,MarketLetterCode,ProgramCode,InVigor_Sales,InVigor_Reward_Percentage,Reward
			,RewardBrand,CPP_Sales,Liberty_Sales,Centurion_Sales,CPP_Percentage,Liberty_Percentage,Centurion_Percentage,CPP_Reward,Liberty_Reward,Centurion_Reward)
		SELECT StatementCode,MarketLetterCode,ProgramCode,Sales,Reward_Percentage,Reward
			,RewardBrand
			,0 AS CPP_Sales
			,0 AS Liberty_Sales
			,0 AS Centurion_Sales
			,0 AS CPP_Percentage
			,0 AS Liberty_Percentage
			,0 AS Centurion_Percentage
			,0 AS CPP_Reward
			,0 AS Liberty_Reward
			,0 AS Centurion_Reward
		FROM #SEGMENT_B_REWARDS			   

END
