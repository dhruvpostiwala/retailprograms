﻿
CREATE PROCEDURE [dbo].[RP2021_RefreshMaximizeRewardMessages] @STATEMENT_TYPE VARCHAR(50),  @STATEMENTS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE(
		StatementCode INT NOT NULL,
		Level5Code VARCHAR(50) NOT NULL,
		MarketLetterCode Varchar(50) NOT NULL,
		ProgramCode Varchar(50) NOT NULL	
	)	

	INSERT INTO @TEMP(StatementCode, Level5Code, MarketLetterCode, ProgramCode)
	SELECT ST.StatementCode, ST2.Level5Code, ELG.MarketLetterCode, ELG.ProgramCode
	FROM @STATEMENTS ST			
		INNER JOIN RPWeb_Statements	ST2					ON ST.StatementCode = ST2.StatementCode
		INNER JOIN RPWeb_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode


	DELETE FROM RPWeb_MaximizeRewardMessages	WHERE [StatementCode] IN (SELECT StatementCode From @STATEMENTS)
		
	INSERT INTO RPWeb_MaximizeRewardMessages(StatementCode,MarketLetterCode,ProgramCode,MaximizeRewardMessage)

	-- Brand Specific Support Reward'
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of Crop Protection Brands by ' + dbo.SVF_Commify((T2.QualBrand_Sales_2YearAverage * T2.NextGrowthPercentage) - T2.QualBrand_Sales_CY, '$') + ' to receive an additional ' + dbo.SVF_Commify(T2.NextRewardPercentage - T2.Reward_Percentage, '%') + ' reward on your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of InVigor Hybrid Canola Seed to eligible growers.' [MaximizeRewardMessage]
	FROM @TEMP T1
	INNER JOIN (
		SELECT StatementCode,
			   Reward_Percentage,
			   CASE Reward_Percentage
					WHEN 0.03 THEN 0.04
					WHEN 0.04 THEN 0.045
				END [NextRewardPercentage],
				CASE
					WHEN CASE WHEN ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2) = 0 THEN 0 ELSE (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) END < 0.95 THEN 0.95
					WHEN CASE WHEN ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2) = 0 THEN 0 ELSE (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) END < 1.05 THEN 1.05
				END [NextGrowthPercentage],
				(QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2 [QualBrand_Sales_2YearAverage],
				QualBrand_Sales_CY,
				Reward [OldReward]
		FROM RP2021Web_Rewards_W_BrandSpecificSupport
		WHERE Reward_Percentage < 0.045
	) T2
	ON T1.StatementCode = T2.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_BRAND_SPEC_SUPPORT'

		UNION

	-- Efficiency Bonus Reward
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of BASF portfolio by ' + dbo.SVF_Commify(T2.NextTierDollars - T2.All_QualSales_CY, '$') + ' to receive a ' + dbo.SVF_Commify(T2.NextRewardPercentage, '%') + ' reward margin.' [MaximizeRewardMessage]
	FROM @TEMP T1
	INNER JOIN (
		SELECT RS.StatementCode,
			   RS2.All_QualSales_CY,
			   CASE
					WHEN RS2.All_QualSales_CY < 1000000 THEN 1000000
					WHEN RS2.All_QualSales_CY < 1500000 THEN 1500000
					WHEN RS2.All_QualSales_CY < 2000000 THEN 2000000
			   END [NextTierDollars],
			   CASE Support_Reward_Percentage + Growth_Reward_Percentage
					WHEN 0.01 THEN 0.0175
					WHEN 0.0175 THEN 0.025
					ELSE 0.01
			   END [NextRewardPercentage]
		FROM RP2021Web_Rewards_W_EFF_BONUS RS
			INNER JOIN RP2021Web_Rewards_W_EFF_BONUS_QUAL RS2
			ON RS.StatementCode = RS2.StatementCode
		WHERE (Support_Reward_Percentage + Growth_Reward_Percentage) < 0.025
	) T2
	ON T1.StatementCode = T2.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_EFF_BONUS_REWARD'

		UNION

	-- InVigor & Liberty Loyalty Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Achieve a 1:1 ratio between your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' InVigor Hybrid Canola Seed POG and Liberty 150 POG to qualify for ' + CASE T1.MarketLetterCode WHEN 'ML2021_W_INV' THEN '2' ELSE '5.45' END + '% reward margin.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_INV_LLB RS
		ON T1.StatementCode = RS.StatementCode AND T1.MarketLetterCode = RS.MarketLetterCode
	WHERE T1.ProgramCode = 'RP2021_W_INV_LIB_LOY_BONUS' AND ((RS.Liberty_Acres < RS.InVigor_Acres) OR (RS.Liberty_Acres = 0 AND RS.InVigor_Acres = 0))

		UNION

	-- InVigor Innovation Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of InVigor Innovation Brands by ' + FORMAT((RS.InVigor_Bags * 0.30) - RS.Innovation_Bags, 'N2') + ' bags to receive a ' + CASE T1.Level5Code WHEN 'D000001' THEN '4' ELSE '3.8' END + '% reward margin on 300 series hybrids and 2% on 200 series hybrids.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_INV_INNOV RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_INV_INNOVATION_BONUS' AND Qualifying_Percentage < 0.30 AND InVigor_Bags > 0

		UNION

	-- InVigor Performance Reward
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
		   'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of InVigor Hybrid Canola Seed by ' + FORMAT((RS.Sales_Target * RS.NextQualPercentage) - RS.QualBrand_Qty, 'N2') + ' Bags to receive an additional ' + dbo.SVF_Commify(RS.NextRewardPercentage - RS.Reward_Percentage, '%') + ' on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of InVigor Hybrid Canola.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT RS.StatementCode, RS.Sales_Target, RS.QualBrand_Qty,
			CASE
				WHEN (RS.QualBrand_Qty / RS.Sales_Target) < 0.80 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.015 ELSE 0.01 END
				WHEN (RS.QualBrand_Qty / RS.Sales_Target) < 0.90 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.02 ELSE 0.0145 END
			END [NextRewardPercentage],
			CASE
				WHEN (RS.QualBrand_Qty / RS.Sales_Target) < 0.80 THEN 0.80
				WHEN (RS.QualBrand_Qty / RS.Sales_Target) < 0.90 THEN 0.90
			END [NextQualPercentage],
			RS.Reward_Percentage
			FROM RP2021Web_Rewards_W_INV_PERF RS
				INNER JOIN RPWeb_Statements ST
				ON RS.StatementCode = ST.StatementCode
		) RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_INV_PERFORMANCE' AND ((T1.Level5Code = 'D000001' AND RS.Reward_Percentage < 0.02) OR (T1.Level5Code = 'D0000117' AND RS.Reward_Percentage < 0.0145))  AND RS.Sales_Target > 0

		UNION

	-- Logistics Support Reward
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Receive up to 4.5% reward margin on specific brands groupings (Inoculant & Seed Treatment, Liberty 150, Centurion, Other Herbicides, Fungicides, Insecticides, & Fall Herbicides) when you edit manual inputs for a brand grouping respectively.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT *
			FROM RPWeb_User_EI_FieldValues
			WHERE ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT'
		) EI
		ON T1.StatementCode = EI.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT' AND EI.FieldValue = 0

		UNION

	-- Growth Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	CASE
		WHEN RS.Growth_Percentage < 0.90 THEN
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection POG by ' + dbo.SVF_Commify((RS.QualSales_LY * 0.90) - RS.QualSales_CY, '$') + ' to achieve 90% growth and receive ' + CASE T1.Level5Code WHEN 'D000001' THEN '5' ELSE '2' END + '% reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection Brands.'
		ELSE
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection POG by ' + dbo.SVF_Commify((RS.QualSales_LY * RS.NextQualPercentage) - RS.QualSales_CY, '$') + ' to move to next reward tier & receive an additional ' + dbo.SVF_Commify(RS.NextRewardPercentage - RS.Reward_Percentage, '%') + ' reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection Brands.'
	END [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT RS.StatementCode, RS.QualSales_CY, RS.QualSales_LY,
			CASE
				WHEN (RS.QualSales_CY / RS.QualSales_LY) < 0.90 THEN 0.90
				WHEN (RS.QualSales_CY / RS.QualSales_LY) < 1.00 THEN 1.00
				ELSE 1.05
			END [NextQualPercentage],
			CASE RS.Reward_Percentage
				WHEN 0.03 THEN 0.05
				WHEN 0.05 THEN 0.0575
				WHEN 0.0575 THEN 0.0625
				WHEN 0.01 THEN 0.02
				WHEN 0.02 THEN 0.0375
				WHEN 0.0375 THEN 0.0450
			END [NextRewardPercentage],
			RS.Growth_Percentage,
			RS.Reward_Percentage
			FROM RP2021Web_Rewards_W_GrowthBonus RS
				INNER JOIN RPWeb_Statements ST
				ON RS.StatementCode = ST.StatementCode
		) RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_GROWTH_BONUS' AND RS.QualSales_LY > 0 AND ((T1.Level5Code = 'D000001' AND RS.Reward_Percentage < 0.0625) OR (T1.Level5Code = 'D0000117' AND RS.Reward_Percentage < 0.0450))

	
		UNION

	-- Loyalty Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	CASE WHEN (T1.Level5Code = 'D000001' AND RS.SegmentA_Reward_Percentage < 0.0625) OR (T1.Level5Code = 'D0000117' AND RS.SegmentA_Reward_Percentage < 0.0450) THEN
		CASE WHEN RS.SegmentA_Growth_Percentage < 0.80 THEN
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty Brand A Segment POG by ' + dbo.SVF_Commify((RS.SegmentA_QualSales_LY * 0.80) - RS.SegmentA_Reward_Sales, '$') + ' to achieve 80% growth and receive ' + CASE T1.Level5Code WHEN 'D000001' THEN '3.5' ELSE '1.5' END + '% reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty A Segment Brands.'
		ELSE
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty Brand A Segment POG by ' + dbo.SVF_Commify((RS.SegmentA_QualSales_LY * RS.SegmentA_NextQualPercentage) - RS.SegmentA_Reward_Sales, '$') + ' to move to next reward tier & receive ' + dbo.SVF_Commify(RS.SegmentA_NextRewardPercentage, '%') + ' reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty A Segment Brands.'
		END
	ELSE
		''
	END + CASE WHEN (T1.Level5Code = 'D000001' AND (RS.SegmentA_Reward_Percentage < 0.0625 AND RS.SegmentB_Reward_Percentage < 0.1225)) OR (T1.Level5Code = 'D0000117' AND (RS.SegmentA_Reward_Percentage < 0.0450 AND RS.SegmentB_Reward_Percentage < 0.0950)) THEN '<br/><br/>' ELSE '' END +
	CASE WHEN (T1.Level5Code = 'D000001' AND RS.SegmentB_Reward_Percentage < 0.1225) OR (T1.Level5Code = 'D0000117' AND RS.SegmentB_Reward_Percentage < 0.0950) THEN
		CASE WHEN RS.SegmentB_Growth_Percentage < 0.80 THEN
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty Brand B Segment POG by ' + dbo.SVF_Commify((RS.SegmentB_QualSales_LY * 0.80) - RS.SegmentB_Reward_Sales, '$') + ' to achieve 80% growth and receive ' + CASE T1.Level5Code WHEN 'D000001' THEN '3.5' ELSE '2.5' END + '% reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty B Segment Brands.'
		ELSE
			'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty Brand B Segment POG by ' + dbo.SVF_Commify((RS.SegmentB_QualSales_LY * RS.SegmentB_NextQualPercentage) - RS.SegmentB_Reward_Sales, '$') + ' to move to next reward tier & receive ' + dbo.SVF_Commify(RS.SegmentB_NextRewardPercentage, '%') + ' reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Loyalty B Segment Brands.'
		END
	ELSE
		''
	END [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT RS.StatementCode, RS.SegmentA_Reward_Sales, RS.SegmentA_QualSales_LY, RS.SegmentA_Growth_Percentage, RS.SegmentA_Reward_Percentage, RS.SegmentB_Reward_Sales, RS.SegmentB_QualSales_LY, RS.SegmentB_Growth_Percentage, RS.SegmentB_Reward_Percentage,
			CASE WHEN RS.SegmentA_QualSales_LY > 0 THEN
				CASE
					WHEN (RS.SegmentA_Reward_Sales / RS.SegmentA_QualSales_LY) < 0.80 THEN 0.80
					WHEN (RS.SegmentA_Reward_Sales / RS.SegmentA_QualSales_LY) < 0.90 THEN 0.90
					ELSE 1.00
				END
			ELSE
				0.80
			END [SegmentA_NextQualPercentage],
			CASE WHEN RS.SegmentB_QualSales_LY > 0 THEN
				CASE
					WHEN (RS.SegmentB_Reward_Sales / RS.SegmentB_QualSales_LY) < 0.80 THEN 0.80
					WHEN (RS.SegmentB_Reward_Sales / RS.SegmentB_QualSales_LY) < 0.90 THEN 0.90
					ELSE 1.00
				END
			ELSE
				0.80
			END [SegmentB_NextQualPercentage],
			CASE RS.SegmentA_Reward_Percentage
				WHEN 0.03 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.035 ELSE 0.015 END
				WHEN 0.035 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.0575 ELSE 0.0375 END
				ELSE CASE ST.Level5Code WHEN 'D000001' THEN 0.0625 ELSE 0.0450 END
			END [SegmentA_NextRewardPercentage],
			CASE RS.SegmentB_Reward_Percentage
				WHEN 0.03 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.035 ELSE 0.025 END
				WHEN 0.035 THEN CASE ST.Level5Code WHEN 'D000001' THEN 0.10 ELSE 0.0725 END
				ELSE CASE ST.Level5Code WHEN 'D000001' THEN 0.1225 ELSE 0.0950 END
			END [SegmentB_NextRewardPercentage]
			FROM RP2021Web_Rewards_W_LoyaltyBonus RS
				INNER JOIN RPWeb_Statements ST
				ON RS.StatementCode = ST.StatementCode
		) RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_LOYALTY_BONUS' AND ((T1.Level5Code = 'D000001' AND (RS.SegmentA_Reward_Percentage < 0.0625 OR RS.SegmentB_Reward_Percentage < 0.1225)) OR (T1.Level5Code = 'D0000117' AND (RS.SegmentA_Reward_Percentage < 0.0450 OR RS.SegmentB_Reward_Percentage < 0.0950))) AND (RS.SegmentA_QualSales_LY > 0 OR RS.SegmentB_QualSales_LY > 0)
	
		UNION

	-- Segment Selling Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Update Manual Input for Segment Selling Bonus to reflect expected reward margin, based on retail''s current Average Growers Segment.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT *
			FROM RPWeb_User_EI_FieldValues
			WHERE ProgramCode = 'RP2021_W_SEGMENT_SELLING' AND FieldCode = 'Segment_Selling'
		) RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_SEGMENT_SELLING' AND RS.FieldValue = 0

		UNION

	-- Liberty Centurion Facet L Tank Mix Bonus
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Earn up to $6.00/jug on Liberty when you sell Liberty, Centurion & Facet L to the same grower.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_TankMixBonus RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_TANK_MIX_BONUS' AND (RS.Liberty_Acres > 0 OR RS.Centurion_Acres > 0 OR RS.Facet_L_Acres > 0)

		UNION

	-- Dicamba Tolerant Program
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Earn up to $2.00/acre on matched acres of Engenia & Viper ADV when you sell to the same grower.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_DicambaTolerant RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_DICAMBA_TOLERANT' AND (RS.Engenia_Acres > 0 OR RS.ViperADV_Acres > 0)

		UNION

	-- Custom Seed Treatment
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	CASE WHEN (T1.Level5Code = 'D000001' AND RS.Planned_POG_Sales < 80000) OR (T1.Level5Code = 'D0000117' AND RS.Planned_POG_Sales < 100000) THEN
		'Receive up to ' + CASE T1.Level5Code WHEN 'D000001' THEN '20' ELSE '18' END + '% reward margin on custom treated Insure Cereal and Insure Pulse brands by increasing your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of these brands to at least ' + CASE WHEN T1.Level5Code = 'D000001' THEN '$80,000.' ELSE '$100,000.' END
	ELSE
		''
	END + CASE WHEN (T1.Level5Code = 'D000001' AND RS.Planned_POG_Sales < 80000) OR (T1.Level5Code = 'D0000117' AND RS.Planned_POG_Sales < 100000) THEN '<br/><br/>' ELSE '' END +
	'Receive 8% reward margin on Nodulator Pro 100 when you custom seed treat.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT StatementCode, SUM(Planned_POG_Sales) [Planned_POG_Sales]
			FROM RP2021Web_Rewards_W_CustomSeed
			GROUP BY StatementCode
		) RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_CUSTOM_SEED_TREATING_REWARD' AND RS.Planned_POG_Sales > 0

		UNION

	-- Clearfield Lentils Support
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Receive ' + CASE T1.Level5Code WHEN 'D000001' THEN '3' ELSE '2.75' END + '% reward margin when you match acres of Clearfield Lentil Herbicides (Odyssey & Solo Brands) with Clearfield Lentils in your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projection' ELSE 'POG' END + ' plan.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_ClearfieldLentils RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_CLL_SUPPORT' AND (RS.CLL_Acres > 0 OR RS.CLL_Herb_Acres > 0)

		UNION

	-- Planning & Supporting
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection POG by ' + dbo.SVF_Commify((RS.QualSales_LY * 0.90) - RS.QualSales_CY, '$') + ' to achieve 90% growth and 2% reward margin on ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' Crop Protection Brands.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_PlanningTheBusiness RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_PLANNING_SUPP_BUSINESS' AND RS.QualSales_LY > 0 AND RS.Growth_Percentage < 0.90

		UNION

	-- Retail Bonus Payment
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of qualifying growth brands by ' + dbo.SVF_Commify(((RS.Growth_Sales_LY1 + RS.Growth_Sales_LY2) / 2) - RS.Growth_Sales_CY, '$') + ' to achieve 100% growth and earn up to 2% reward margin on Centurion & 6% reward margin on Liberty 150 ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021Web_Rewards_W_RetailBonus RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_W_RETAIL_BONUS_PAYMENT' AND ((RS.Growth_Sales_LY1 + RS.Growth_Sales_LY2) / 2) > 0 AND (RS.Growth_Sales_CY / ((RS.Growth_Sales_LY1 + RS.Growth_Sales_LY2) / 2)) < 1.00

		UNION

	-- Planning The Business (East)
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Complete your 2021 POG Plan on/before December 11, 2020' [MaximizeRewardMessage]
	FROM @TEMP T1
	WHERE T1.ProgramCode = 'RP2021_E_PLANNING_SUPP_BUSINESS'

		UNION

	-- Portfolio Support (East)
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG$ by ' + dbo.SVF_Commify(CASE WHEN RS.QualBrand_Sales_CY < 100000 THEN 100000 - RS.QualBrand_Sales_CY ELSE (RS.QualBrand_Sales_2YearAverage * RS.NextQualifyingPercentage) - RS.QualBrand_Sales_CY END, '$') + ' to receive ' + dbo.SVF_Commify(RS.NextRewardPercentage, '%') + ' reward.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT StatementCode, MarketLetterCode, Reward_Percentage, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2,
				   CASE
				       WHEN ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) <= 0 AND QualBrand_Sales_CY <= 0) OR (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) < 0.80 THEN 0.80
					   WHEN (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) < 0.90 THEN 0.90
					   WHEN (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) < 0.95 THEN 0.95
					   WHEN (QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2)) < 1.00 THEN 1.00
					END [NextQualifyingPercentage],
					CASE Reward_Percentage
					    WHEN 0 THEN 0.01
						WHEN 0.01 THEN 0.03
						WHEN 0.03 THEN 0.04
						WHEN 0.04 THEN 0.06
					END [NextRewardPercentage],
					(QualBrand_Sales_LY1 + QualBrand_Sales_LY2) / 2 [QualBrand_Sales_2YearAverage]
			FROM (
				SELECT StatementCode, MarketLetterCode,
					   SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY],
					   SUM(QualBrand_Sales_LY1) [QualBrand_Sales_LY1],
					   SUM(QualBrand_Sales_LY2) [QualBrand_Sales_LY2],
					   MAX(Reward_Percentage) [Reward_Percentage]
				FROM RP2021Web_Rewards_E_PortfolioSupport
				GROUP BY StatementCode, MarketLetterCode
			) Data
			WHERE Reward_Percentage < 0.06
		) RS
		ON T1.StatementCode = RS.StatementCode AND T1.MarketLetterCode = RS.MarketLetterCode
	WHERE T1.ProgramCode = 'RP2021_E_PORTFOLIO_SUPPORT'

		UNION

	-- Loyalty Support Reward (East)
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your 2021 ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG of ' + RewardBrand + ' by ' + dbo.SVF_Commify(QualBrand_Sales_LY - QualBrand_Sales_CY, '$') + ' to achieve 100% growth growth and earn ' + dbo.SVF_Commify(RewardPerCase, '$') + '/case.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON T1.StatementCode = RS.StatementCode
	WHERE T1.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.QualBrand_Sales_LY > RS.QualBrand_Sales_CY

		UNION

	-- Fungicide Support Reward (East)
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Increase your ' + CASE @STATEMENT_TYPE WHEN 'PROJECTION' THEN 'projected' ELSE 'planned' END + ' POG$ of fungicide brands by ' + dbo.SVF_Commify((QualBrand_Sales_LY * NextRewardPerc) - QualBrand_Sales_CY, '$') + ' to achieve the next growth tier and earn ' + dbo.SVF_Commify(NextRewardPerAcre,'$') + '/acre.' [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT *,
				CASE
					WHEN QualBrand_Acres_CY < 10000 AND QualBrand_Sales_CY / NULLIF(QualBrand_Sales_LY,0) < 1.00 THEN 1.00
					WHEN QualBrand_Acres_CY < 10000 AND QualBrand_Sales_CY / NULLIF(QualBrand_Sales_LY,0) < 1.10 THEN 1.10
					WHEN QualBrand_Acres_CY >= 10000 AND QualBrand_Sales_CY / NULLIF(QualBrand_Sales_LY,0) < 0.90 THEN 0.90
					WHEN QualBrand_Acres_CY >= 10000 AND QualBrand_Sales_CY / NULLIF(QualBrand_Sales_LY,0) < 1.00 THEN 1.00
				END [NextRewardPerc],
				QualBrand_Sales_CY / NULLIF(QualBrand_Sales_LY,0) AS 'QualifiedPerc',
				CASE RewardPerAcre
					WHEN 0 THEN 1.25
					WHEN 1.25 THEN 1.75
				END [NextRewardPerAcre]
			FROM (
				SELECT StatementCode, MarketLetterCode,
					SUM(QualBrand_Sales_LY) [QualBrand_Sales_LY],
					SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY],
					SUM(QualBrand_Acres_CY) [QualBrand_Acres_CY],
					MAX(RewardPerAcre) [RewardPerAcre]
				FROM RP2021Web_Rewards_E_FungicidesSupport
				GROUP BY StatementCode, MarketLetterCode
			) Data
			WHERE RewardPerAcre < 1.75
		) RS
		ON T1.StatementCode = RS.StatementCode AND T1.MarketLetterCode = RS.MarketLetterCode
	WHERE T1.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT'

		UNION

	-- POG Sales Bonus Reward (East)
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
	'Complete your 2021 POG Plan of InVigor on/before January 30th, 2021' [MaximizeRewardMessage]
	FROM @TEMP T1
	WHERE T1.ProgramCode = 'RP2021_E_POG_SALES_BONUS'
END
