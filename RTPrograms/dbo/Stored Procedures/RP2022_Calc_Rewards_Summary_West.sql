﻿

CREATE PROCEDURE [dbo].[RP2022_Calc_Rewards_Summary_West] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	DELETE REW
	FROM RP_DT_Rewards_Summary REW
	INNER JOIN @STATEMENTS ST
	ON ST.StatementCode=REW.StatementCode

	-- BRAND SPECIFIC SUPPORT
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_BrandSpecificSupport RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_BRAND_SPEC_SUPPORT'

	-- BASE REWARD
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_INV_BaseReward RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_BASE_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- INVIGOR & LIBERTY LOYALTY BONUS
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_INV_LLB RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_INV_LIB_LOY_BONUS'

	-- INVIGOR PERFORMANCE		
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_INV_PERF RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_INV_PERFORMANCE'

	-- INVIGOR INNOVATION
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_INV_INNOV RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_INV_INNOVATION_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Growth Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_GrowthBonus RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_GROWTH_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Loyalty Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN (
			SELECT StatementCode, MarketLetterCode, ProgramCode, SegmentA_Reward + SegmentB_Reward + ISNULL(SegmentC_Reward,0) + ISNULL(SegmentD_Reward,0)  AS Reward
			FROM RP2022_DT_Rewards_W_LoyaltyBonus 			
		) RS		
		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_LOYALTY_BONUS' 
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Clearfield Lentils Support Forecast Projection
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S							ON S.StatementCode = ST.StatementCode 
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_ClearfieldLentils RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE  ELG.ProgramCode='RP2022_W_CLL_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	

	-- Liberty/ Centurion / Facet L Tank Mix Bonus 
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward, 0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_TankMixBonus RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_TANK_MIX_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	

	-- Custom Seed Treatment
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_CustomSeed RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_CUSTOM_SEED_TREATING_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Logistics Support Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_LogisticsSupport RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_LOGISTICS_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	
	-- Planning the Business Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(COALESCE(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG							ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG								ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_PlanningTheBusiness RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_PLANNING_SUPP_BUSINESS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	
	-- Base Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(COALESCE(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG							ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG								ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2022_DT_Rewards_W_BaseReward_CPP RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2022_W_BASE_REWARD_CPP'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	


	-- SUMMARIZE REWARDS AT HEAD OFFICE LEVEL
	-- FCL -- ROLL UP REWARDS TO LEVEL 5 STATEMENT	
	DECLARE @HO_STATEMENTS TABLE(
		StatementCode INT NOT NULL,
		StatementCode_L5 INT NOT NULL
	)

	INSERT INTO @HO_STATEMENTS(StatementCode,StatementCode_L5)
	SELECT DISTINCT ST2.StatementCode, ST2.DataSetCode_L5 AS StatementCode_L5	
	FROM @STATEMENTS ST1
		INNER JOIN RP_Statements ST2				ON ST2.StatementCode=ST1.StatementCode		
		INNER JOIN RP_Config_HOPaymentLevel HO 		ON HO.Level5Code=ST2.Level5Code AND HO.Season=ST2.Season
	WHERE HO.Summary='Yes'

	DELETE REW
	FROM RP_DT_Rewards_Summary REW		
		INNER JOIN (
			SELECT DISTINCT StatementCode_L5 AS Statementcode FROM @HO_STATEMENTS
		) T2
		ON T2.Statementcode=REW.Statementcode
			
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode,  SUM(REW.RewardValue), 'Eligible' , ''
	FROM @HO_STATEMENTS HO
		INNER JOIN RP_DT_Rewards_Summary REW	
		ON REW.StatementCode=HO.StatementCode	
	WHERE RewardEligibility='Eligible'
	GROUP BY HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode

END
