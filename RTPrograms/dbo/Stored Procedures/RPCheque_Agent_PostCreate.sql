﻿


CREATE PROCEDURE [dbo].[RPCheque_Agent_PostCreate] 
	@SEASON INT,  
	@USERNAME VARCHAR(100), 	
	@FieldValue VARCHAR(100),
	@VersionType VARCHAR(25),
	@ChequeCreation VARCHAR(20), 
	@LockedStatementCode INT, 
	@ChequeCode VARCHAR(20),
	@RetailerCode VARCHAR(20),
	@ChequeType VARCHAR(25),
	@ChequeStatus varchar(50),
	@PayableToName varchar(150),
	@ChequeAmount Money,
	@AmountRequested Money,
	@AmountReceived Money,
	@AmountPaid Money,
	@ChequeErrorMessage Varchar(300)	
AS
BEGIN

SET NOCOUNT ON; 
BEGIN TRANSACTION;  

BEGIN TRY
	IF @ChequeCreation='SUCCESS'
	BEGIN
		DELETE FROM RPWeb_Cheques Where [ChequeDocCode]=@ChequeCode
		INSERT INTO RPWeb_Cheques([StatementCode],[ChequeDocCode],[RetailerCode],[VersionType],[ChequeType],[ChequeStatus],[SendTo],[PayableToName],[ChequeAmount],[AmtRequested],[AmtReceived],[AmtPaid])
		VALUES(@LockedStatementCode,@ChequeCode,@RetailerCode,@VersionType,@ChequeType,@ChequeStatus,'',@PayableToName,@ChequeAmount,@AmountRequested,@AmountReceived,@AmountPaid)
	
		DELETE RPWeb_StatementInformation 
		WHERE [StatementCode]=@LockedStatementCode AND [FieldName]='CreateChequeStatus' AND [FieldValue]=@FieldValue 

		INSERT INTO RPWeb_StatementInformation(Statementcode, FieldName, FieldValue)
		VALUES(@LockedStatementCode, 'Payment_Status', @ChequeStatus)
	
		/* UPDATE UNLOCKED STATEMENT STATUS */
		DECLARE @EDIT_HISTORY RP_EDITHISTORY
		UPDATE SI
		SET [FieldValue]='Cheque Created'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Create Cheque/EFT','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation SI
			INNER JOIN RPWeb_Statements ST	
			ON ST.[SRC_StatementCode]=SI.[StatementCode]
		WHERE ST.[Season]=@Season AND ST.[VersionType]='Locked' AND ST.[Status]='Active' 
			AND (ST.StatementCode=@LockedStatementCode OR ST.DataSetCode=@LockedStatementCode OR ST.DataSetCode_L5=@LockedStatementCode)
			AND SI.[FieldName]='Status' AND SI.[FieldValue]='Ready for Payment'
			
	
		EXEC RP_GenerateEditHistory 'Statement',@UserName,@EDIT_HISTORY		
	
		/* SET [VERSIONTYPE]='Archive' on all locked statements belonging to a specific Lock JOB ID*/
		UPDATE RPWeb_Statements 
		SET [VersionType]='Archive'
		WHERE [Season]=@Season AND [VersionType]='Locked' AND [Status]='Active' 
			AND ([StatementCode]=@LockedStatementCode OR [DataSetCode]=@LockedStatementCode OR [DataSetCode_L5]=@LockedStatementCode) 

	END
	
	IF @ChequeCreation='ERROR'
	BEGIN 
		UPDATE RPWeb_StatementInformation
		SET [FieldValue]='Error',[Comments]=@ChequeErrorMessage 
		WHERE [StatementCode]=@LockedStatementCode AND [FieldName]='CreateChequeStatus' AND [FieldValue]=@FieldValue 	
	END 
END TRY

BEGIN CATCH
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;

SELECT  'Failed' AS Status
        ,ERROR_NUMBER() AS ErrorNumber                      
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  	

	RETURN 
END CATCH

IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  

SELECT 'Success' AS Status 
	
END




