﻿

CREATE PROCEDURE [dbo].[RP2020_Exception_E_Program_Data] @SEASON INT,@STATEMENTCODE INT, @REWARDCODE VARCHAR(50)
AS
BEGIN
		SET NOCOUNT ON;
		
		DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		ProgramCode varchar(100) NOT NULL,
		RewardCode varchar(50) NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		cy_sales MONEY NOT NULL DEFAULT 0,
		current_margin DECIMAL(6,4) NOT NULL DEFAULT 0,
		current_reward MONEY NOT NULL DEFAULT 0,
		next_margin DECIMAL(10,4) NOT NULL DEFAULT 0,
		exception_type varchar(50) NULL,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[ProgramCode]  ASC,
			[RewardCode]  ASC,
			[MarketLetterCode] ASC
			)
		)
	
		INSERT into @TEMP(StatementCode,ProgramCode,RewardCode,MarketLetterCode)
		SELECT DISTINCT E.StatementCode,E.ProgramCode,R.RewardCode,E.MarketLetterCode
		FROM [RP_Config_Rewards_Summary] R 
				INNER JOIN RP_Config_programs P ON P.RewardCode = R.RewardCode 
				INNER JOIN RPWeb_ML_ELG_Programs E ON E.ProgramCode = P.ProgramCode 
		WHERE E.StatementCode = @STATEMENTCODE
		AND R.RewardCode NOT IN ('CGAS_E','PRX_HEAD_E')
		
		
		--SupplySales only using on CCP MarketLetter
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2020_E_INV' AND RewardCode = 'SUPPLY_SALES_E'
		
		--These dont get exemptions
		--DELETE FROM @TEMP WHERE RewardCode IN ('CGAS_E','PRX_HEAD_E')
	
	
		UPDATE @TEMP SET exception_type = 'TIER' WHERE RewardCode IN ('PORT_SUP_E','ROW_CROP_FUNG_E','SUPPLY_SALES_E','SSLB_E')
	
		UPDATE @TEMP SET exception_type = 'NON-TIER' WHERE exception_type IS NULL
	
		--Since some programs are flat value we will just set them manually
		--Planning Businnes
		--Pursuit
		--POG Sales Bonus
		--Horticulture Support Reward Added Feb 2021 - Demarey
		UPDATE @TEMP SET next_margin = 0.01 WHERE RewardCode = 'PSB_E'
		UPDATE @TEMP SET next_margin = 0.02 WHERE RewardCode = 'POG_SALES_BONUS_E'
		UPDATE @TEMP SET next_margin = 75 WHERE RewardCode = 'PURS_SUP_E'
	
		UPDATE @TEMP SET next_margin = 0.01 WHERE RewardCode = 'SSLB_E'
		UPDATE @TEMP SET next_margin = 0.01 WHERE RewardCode = 'HORT_RET_SUP_E'
		-- need to fetch current year data for 
		
		--NOn tier values
		UPDATE T1
				SET T1.cy_sales = NTIER.CYSales
		FROM @TEMP T1
		INNER JOIN 
			(
			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales',MAX(ProgramCode) ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_PlanningTheBusiness]
			GROUP BY StatementCode
			UNION ALL
			SELECT StatementCode,RewardBrand_Sales AS 'CYSales',ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_POGSalesBonus]
			UNION ALL
			SELECT StatementCode, Pursuit_QTY_CY AS 'CYSales',ProgramCode
			FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport]
			UNION ALL 
			Select StatementCode,RewardBrand_ElgSales_CY,ProgramCode 
			FROM [dbo].[RP2020Web_Rewards_E_HortiCultureRetailSupport]
			) NTIER	ON NTIER.StatementCode = T1.StatementCode and T1.ProgramCode = NTIER.ProgramCode
			WHERE T1.exception_type = 'NON-TIER'

		--tier values
		UPDATE T1
			SET T1.cy_sales = TIER.CYSales,
				T1.current_margin = TIER.CurrentMargin,
				T1.current_reward = TIER.CurrentReward,
				T1.next_margin = TIER.next_margin
		FROM @TEMP T1
		INNER JOIN 
			(
			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales', MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode,
										MAX(CASE 
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.04  THEN 0.06
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.03  THEN 0.04						
														ELSE 0.03
									END) AS [next_margin]
			from [dbo].[RP2020Web_Rewards_E_PortfolioSupport]
			GROUP BY StatementCode
			UNION ALL
			SELECT StatementCode,SUM(RewardBrand_Acres) as 'CYSales', MAX(Reward_Amount) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode,
									MAX(CASE 
										WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND Reward_Amount = 1.25  THEN 1.75						
										WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND Reward_Amount = 0  THEN 1.25						
												ELSE 0 
										END) AS [next_margin]
			FROM [dbo].[RP2020Web_Rewards_E_RowCropFungSupport]
			GROUP BY StatementCode
			UNION ALL
			SELECT StatementCode,SUM(RewardBrand_CY_Sales) as 'CYSales', MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode,
									MAX(CASE 
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.025  THEN 0.045
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.01  THEN 0.025						
														ELSE 0.01
									END) AS [next_margin]
			from [dbo].[RP2020Web_Rewards_E_SSLB]
			GROUP BY StatementCode
			UNION ALL
			SELECT StatementCode,SUM(RewardBrand_Sales) as 'CYSales', MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode),
									MAX(CASE 
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.0005  THEN 0.0075
											WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.0025  THEN 0.005						
														ELSE 0.0025
									END) AS [next_margin]
									from [dbo].[RP2020Web_Rewards_E_SupplySales]
			GROUP BY StatementCode
			) TIER	ON TIER.StatementCode = T1.StatementCode and T1.ProgramCode = TIER.ProgramCode
			WHERE T1.exception_type = 'TIER'
		
		SELECT ISNULL((
		SELECT cy_sales,current_margin,next_margin,current_reward,RewardCode as reward_code,exception_type from @TEMP Where RewardCode = @REWARDCODE		
		FOR JSON PATH
		) ,'[]')[json_data];
END
