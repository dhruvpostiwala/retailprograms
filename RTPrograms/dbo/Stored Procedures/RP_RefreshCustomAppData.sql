﻿
CREATE PROCEDURE [dbo].[RP_RefreshCustomAppData] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	DELETE T1
	FROM [dbo].[RP_DT_CustomAppProducts] T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'
/*			
	Create table #CAPP_LINK(
		ProductCode varchar(65)
		,Product varchar(65)
		,ActualProduct Varchar(65)
	)

    insert into #CAPP_LINK 
	values('000000000059012874','Insure Cereal','Insure Cereal')
	,('PRD-LCAR-98SQAS','Insure Cereal 120L Drum','Insure Cereal 120 L Drum')
	,('PRD-LCAR-98SQWS','Insure Cereal 450L Tote','Insure Cereal 450 L Tote')
	,('PRD-ENYA-B4XLQV','Insure Cereal FX4','Insure Cereal FX4')
	,('PRD-ENYA-B4XMFC','Insure Cereal FX4 120L Drum','Insure Cereal FX4 120 L Drum')
	,('PRD-ENYA-B4XMKE','Insure Cereal FX4 450L Tote','Insure Cereal FX4 450 L Tote')
	,('PRD-WCHG-9ZZHMQ','Insure Pulse','Insure Pulse')
    ,('PRD-LCAR-9XBKS9','Insure Pulse 120L Drum','Insure Pulse 120 L Drum')
    ,('PRD-LCAR-ADBNBP','Insure Pulse 450L Tote','Insure Pulse 450 L Tote')
    ,('PRD-LCAR-9BEKR5','Nodulator Pro 100','Nodulator Pro 100')
	/*
	,('PRD-ACMC-2C5F8E','TERAXXA F4 120 L DRUM','TERAXXA F4 120 L DRUM')
	,('PRD-ACMC-8D2311','TERAXXA F4 450 L TOTE','TERAXXA F4 450 L TOTE')
	,('PRD-RROE-BRRJ92','TERAXXA F4 2 X 9.8 L CASE','TERAXXA F4 2 X 9.8 L CASE')	
	*/
*/
	IF @SEASON >= 2021
	BEGIN
		INSERT INTO RP_DT_CustomAppProducts(Statementcode,DataSetCode,Season,RetailerCode,DocCode,[Type],Product,Quantity,PackageSize,ProductCode,RP_ID)
		SELECT ST.StatementCode, RS.DataSetcode
			,CAD.Season,CAD.Retailercode,CAD.DocCode,CAD.[Type]
			,CAD.Product
			,CAD.Quantity
			,CAD.PackageSize	
			,COALESCE(PR1.ProductCode,PR2.ProductCode,'') AS ProductCode	
			,ROW_NUMBER() OVER(PARTITION BY ST.Statementcode ORDER BY CAD.Retailercode, CAD.Product) AS [RP_ID]		
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS				ON RS.Statementcode=ST.Statementcode
			INNER JOIN RP_StatementsMappings MAP	ON MAP.Statementcode=ST.StatementCode
			INNER JOIN (
				SELECT CA.RetailerCode, CA.Season, CA.DocCode, CAP.[Type], CAP.Product, CAP.Quantity, CAP.PackageSize,CA.SubmitStatus									
				FROM CustomApp CA
					INNER JOIN CustomAppProducts CAP	
					ON CAP.CustomAppDocCode=CA.Doccode								
				WHERE CA.Season=@SEASON AND CA.SubmitStatus='Completed' AND  CAP.Quantity <> 0
			) CAD
			ON CAD.RetailerCode=MAP.ActiveRetailercode AND CAD.Season=RS.Season		
			LEFT JOIN (
				Select  ProductCode, ProductUOM, Product
				FROM ProductReference 	
				where status = 'Active'
			) PR1
			ON PR1.Product=CAD.Product AND PR1.ProductUOM = CAD.PackageSize			
			LEFT JOIN (
				Select  ProductCode, Product 
				FROM ProductReference 
				WHERE ProductUOM = 'CASES' and status = 'Active'
			) PR2		
			ON PR2.Product=CAD.Product			
	END


	IF @SEASON <= 2020
	BEGIN
		INSERT INTO RP_DT_CustomAppProducts(Statementcode,DataSetCode,Season,RetailerCode,DocCode,[Type],Product,ProductCode,Quantity,PackageSize,RP_ID)
		SELECT ST.StatementCode, RS.DataSetcode
			,CAD.Season,CAD.Retailercode,CAD.DocCode,CAD.[Type]
			,CAD.Product
			,COALESCE(PR1.ProductCode,PR2.ProductCode,'') AS ProductCode		
			,CAD.Quantity,CAD.PackageSize	
			,ROW_NUMBER() OVER(PARTITION BY ST.Statementcode ORDER BY CAD.Retailercode, CAD.Product) AS [RP_ID]		
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS				ON RS.Statementcode=ST.Statementcode
			INNER JOIN RP_StatementsMappings MAP	ON MAP.Statementcode=ST.StatementCode
			INNER JOIN (
				SELECT CA.RetailerCode, CA.Season, CA.DocCode, CAP.[Type], CAP.Product, CAP.Quantity, CAP.PackageSize,CA.SubmitStatus
					,REPLACE(IIF(PRODUCT = 'PROWL H2O TOTE', 'PROWLH2O450LTOTE', CAP.Product),' ','') AS ProductName						-- Exception PROWL H20 TOTES
				FROM CustomApp CA
					INNER JOIN CustomAppProducts CAP	
					ON CAP.CustomAppDocCode=CA.Doccode								
				WHERE CA.Season=@SEASON AND CA.SubmitStatus='Completed' AND  CAP.Quantity <> 0
			) CAD
			ON CAD.RetailerCode=MAP.ActiveRetailercode AND CAD.Season=RS.Season		
			LEFT JOIN (
				Select  ProductCode 
				,REPLACE(ProductName,' ','') AS ProductName
				FROM ProductReference 	
				where status = 'Active'
			) PR1		
			ON PR1.ProductName=CAD.ProductName
			LEFT JOIN (
				Select  ProductCode	,REPLACE(Product,' ','') AS ProductName				
				FROM ProductReference 
				WHERE ProductUOM = 'CASES' and status = 'Active'
			) PR2		
			ON PR2.ProductName=CAD.ProductName
		END

	-- JIRA: RP-3266
	-- Custom App for 8.41750 cases of Insure Cereal goes to Hawk's R520068938 (REMOVE IT FROM BLAIR's STATEMENT)
	IF @SEASON=2020
	DELETE T1
	FROM RP_DT_CustomAppProducts T1
		INNER JOIN RP_Statements RS
		ON RS.StatementCode=T1.StatementCode
	WHERE RS.RetailerCode='R0000839' AND T1.Retailercode='R520068938' AND T1.DocCode='CA-TORO-BSARRW'



END

