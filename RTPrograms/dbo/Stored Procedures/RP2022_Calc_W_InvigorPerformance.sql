﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_InvigorPerformance] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'REWARD PLANNER'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_INV_PERFORMANCE'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (8960)
	*/

	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(10) NOT NULL DEFAULT '',
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Qty NUMERIC(18,2) NOT NULL,
		Sales_Target MONEY NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED  (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- NEW: Apply Reward Planner %s
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Get data for all the statements
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardBrand_Sales, QualBrand_Qty, Sales_Target)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code,
			SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY * ISNULL(EI1.FieldValue,100)/100 ,0) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1)) AS RewardBrand_Sales,
			SUM(IIF(tx.GroupType='QUALIFYING_BRANDS', ((tx.Acres_CY * ISNULL(EI1.FieldValue,100)/100) / 10) ,0) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1)) AS QualBrand_Qty,  -- Converted using CY_acres / 10
			SUM(IIF(tx.GroupType='REWARD_BRANDS',((tx.Acres_LY1 * ISNULL(EI1.FieldValue,100)/100) / 10),0)) AS Sales_Target -- Converted using LY1_acres / 10
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
			INNER JOIN RPWeb_User_EI_FieldValues EI1 ON EI1.StatementCode = ST.StatementCode AND EI1.MarketLetterCode = TX.MarketLetterCode AND EI1.FieldCode = 'Radius_Percentage_InVigor'
			LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = TX.StatementCode AND BI.ChemicalGroup = PR.ChemicalGroup
			LEFT JOIN RPWeb_User_BrandInputs BI2 ON BI2.StatementCode = TX.StatementCode AND BI2.ChemicalGroup = PR.Product
			--LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = TX.StatementCode AND BI.ChemicalGroup = TX.GroupLabel
		WHERE tx.ProgramCode = @PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, TX.Level5Code
	END
	
	--IF @STATEMENT_TYPE IN ('ACTUAL')
	--BEGIN
		--TBD
	--END

	-- Update the reward percentage for all the statements
	UPDATE @TEMP SET [Reward_Percentage] = 0.01 WHERE ([Sales_Target] = 0 OR (QualBrand_Qty / Sales_Target) + @ROUND_PERCENT >= 0.90)
	UPDATE @TEMP SET [Reward_Percentage] = 0.005 WHERE [Reward_Percentage] = 0 AND [Sales_Target] > 0 AND (QualBrand_Qty / Sales_Target) + @ROUND_PERCENT >= 0.80

	-- Set the reward for all the statements based on the reward percentage set previously
	UPDATE @TEMP SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE [RewardBrand_Sales] > 0

	DELETE FROM RP2022_DT_Rewards_W_INV_PERF WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2022_DT_Rewards_W_INV_PERF(StatementCode, MarketLetterCode, ProgramCode, QualBrand_Qty, RewardBrand_Sales, RewardBrand_Qty, Sales_Target, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, QualBrand_Qty, RewardBrand_Sales, QualBrand_Qty, Sales_Target, Reward_Percentage, Reward
	FROM @TEMP

END