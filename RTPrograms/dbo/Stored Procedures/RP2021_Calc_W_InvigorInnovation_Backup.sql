﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_InvigorInnovation_Backup] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @InVigor300_Percentage_Coop Decimal(6,4) = 0.038;
	DECLARE @InVigor300_Percentage_Ind Decimal(6,4) = 0.040;
	DECLARE @InVigor200_Percentage Decimal(6,4) = 0.020;
	DECLARE @Qualifying_Percentage Decimal(6,4) = 0.30;

	DECLARE @TEMP TABLE (
		StatementCode Int,
		Level5Code Varchar(20) NOT NULL,
		RetailerCode Varchar(50) NOT NULL,
		MarketLetterCode Varchar(50) NOT NULL,
		ProgramCode Varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,
		InVigor_Bags Decimal NOT NULL,
		Innovation_Bags Decimal NOT NULL,
		RewardBrand_Sales Money NOT NULL,
		Innovation_Sales Money NOT NULL DEFAULT 0,
		InVigor300_Sales Money NOT NULL,
		InVigor200_Sales Money NOT NULL,
		Qualifying_Percentage Decimal(19,4) NOT NULL DEFAULT 0,
		InVigor300_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		InVigor200_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		InVigor300_Reward Money NOT NULL DEFAULT 0,
		InVigor200_Reward Money NOT NULL DEFAULT 0,
		Reseed_InVigor_Bags Decimal NOT NULL DEFAULT 0,
		Reseed_Innovation_Bags Decimal NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	-- Insert reward data into temporary table variable
	INSERT INTO @TEMP(StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand, InVigor_Bags, Innovation_Bags, RewardBrand_Sales, InVigor300_Sales, InVigor200_Sales, Innovation_Sales)
	SELECT ST.StatementCode, TX.Level5Code, TX.RetailerCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel,
			SUM(TX.Acres_CY / 10) AS InVigor_Bags,
			SUM(IIF(PRH.Hybrid_Name IN ('L234PC', 'L340PC', 'LR344PC', 'L345PC', 'L352C', 'L357P'), TX.Acres_CY / 10, 0)) AS Innovation_Bags,
			SUM(TX.Price_CY) AS RewardBrand_Sales,
			SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][3][0-9]%', TX.Price_CY, 0)) AS InVigor300_Sales,
			SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][2][0-9]%', TX.Price_CY, 0)) AS InVigor200_Sales,
			SUM(IIF(PRH.Hybrid_Name IN ('L234PC', 'L340PC', 'LR344PC', 'L345PC', 'L352C', 'L357P'), TX.Price_CY ,0)) AS Innovation_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode

		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode

		INNER JOIN ProductReference_Hybrids PRH
		ON PR.Hybrid_ID = PRH.ID
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.Level5Code, TX.RetailerCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel


	-- LETS ADJUST 2021 INVIGOR, INNOVATION BAGS BY DEDUCTING RESEEEDED ACRES
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		UPDATE T1
		SET T1.InVigor_Bags = T1.InVigor_Bags - T2.Reseeded_InVigor_Bag
		    ,T1.Innovation_Bags = T1.Innovation_Bags - T2.Reseeded_Innovation_Bag
		FROM @TEMP T1
			INNER JOIN (
				SELECT ST.StatementCode
				       ,SUM(APA.InVigor_ProblemArea/10) AS Reseeded_InVigor_Bag
					   ,SUM(APA.Innovation_ProblemArea/10) AS Reseeded_Innovation_Bag
				FROM @STATEMENTS ST
					INNER JOIN [dbo].[RP_StatementsMappings] MAP
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT C.RetailerCode
							   ,SUM(CB.TotalProblemArea) AS InVigor_ProblemArea
							   ,SUM(IIF(CB.ReseedHybridName IN ('InVigor L234PC', 'InVigor L340PC', 'InVigor LR344PC', 'InVigor L345PC', 'InVigor L352C', 'InVigor L357P'), CB.TotalProblemArea ,0)) AS Innovation_ProblemArea
						FROM Claims C
							LEFT JOIN Claims_BasicInfo CB
							ON C.ID = CB.Claim_ID
						WHERE ISNULL(C.SoftDeleted,'') <> 'Yes' AND C.Season = @SEASON AND C.NOI_Code IN ('W_INV_RESEED','E_INV_RESEED') 
						AND C.Status NOT IN ('Draft','Deleted')  AND ISNULL(C.RetailerCode,'') <> '' AND CB.TotalProblemArea > 0 
						GROUP BY C.RetailerCode
					) APA
					ON APA.RetailerCode=MAP.RetailerCode
				GROUP BY ST.Statementcode				
			) T2
			ON T2.Statementcode=T1.Statementcode
	END


	-- Get the qualifying percentage for each statement
	UPDATE @TEMP
		--SET Qualifying_Percentage = IIF(Innovation_Bags >= 20, Innovation_Bags / InVigor_Bags, 0)
		SET Qualifying_Percentage = IIF(Innovation_Bags >= 20, Innovation_Bags / InVigor_Bags, 0)

	-- Set the reward percentage for InVigor 300 and InVigor 200
	UPDATE @TEMP
		SET InVigor300_Percentage = CASE Level5Code WHEN 'D000001' THEN @InVigor300_Percentage_Ind ELSE @InVigor300_Percentage_Coop END,
			InVigor200_Percentage = @InVigor200_Percentage
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage

	-- Calculate the InVigor 300 and 200 Series reward for each statement
	UPDATE @TEMP
		SET InVigor300_Reward = IIF(InVigor300_Sales > 0, InVigor300_Sales * InVigor300_Percentage, 0),
			InVigor200_Reward = IIF(InVigor200_Sales > 0, InVigor200_Sales * InVigor200_Percentage, 0)
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage

	-- Calculate the total reward as InVigor 300 reward + InVigor 200 reward
	UPDATE @TEMP
		SET Reward = InVigor300_Reward + InVigor200_Reward
	WHERE InVigor300_Reward > 0 OR InVigor200_Reward > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'	
	BEGIN
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage_InVigor'
		WHERE [Reward] > 0
	END

	-- Insert values from temporary table into permanent table
	DELETE FROM RP2021_DT_Rewards_W_INV_INNOV WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_INV_INNOV(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode
	FROM @TEMP
END
