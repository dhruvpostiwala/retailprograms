﻿

CREATE PROCEDURE [dbo].[zMP_Backup_RP2021_Calc_W_LoyaltyBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		RewardBrand VARCHAR(255) NOT NULL,
		SegmentA_QualSales_CY MONEY NOT NULL,
		SegmentA_QualSales_LY_Pulse MONEY NOT NULL,
		SegmentA_QualSales_LY_Pulse_INDEX MONEY NOT NULL,
		SegmentA_QualSales_LY_Cereal MONEY NOT NULL,
		SegmentA_QualSales_LY_Cereal_INDEX MONEY NOT NULL,
		SegmentA_QualSales_LY MONEY NOT NULL DEFAULT 0,
		SegmentA_QualSales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentA_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		SegmentA_Reward_Sales MONEY NOT NULL,
		SegmentA_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentA_Reward MONEY NOT NULL DEFAULT 0,
		SegmentB_QualSales_CY MONEY NOT NULL,
		SegmentB_QualSales_LY MONEY NOT NULL,
		SegmentB_QualSales_LY_INDEX MONEY NOT NULL,
		SegmentB_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		SegmentB_Reward_Sales MONEY NOT NULL,
		SegmentB_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentB_Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			MarketLetterCode ASC,
			ProgramCode ASC,
			RewardBrand ASC
		)
	);

	DECLARE @QUALIFYING TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		SegmentA_QualSales_CY MONEY NOT NULL,
		SegmentA_QualSales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentA_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		SegmentB_QualSales_CY MONEY NOT NULL,
		SegmentB_QualSales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentB_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	);

	DECLARE @CROP_SEGMENT TABLE (
		Crop_Segment VARCHAR(50),
		ChemicalGroup VARCHAR(100),
		PRIMARY KEY CLUSTERED (
			[ChemicalGroup] ASC
		)
	);

	INSERT	INTO @CROP_SEGMENT(Crop_Segment, ChemicalGroup)
	SELECT	IIF(B.ChemicalGroup = 'ARMEZON', 'CEREAL', 'PULSE') AS Crop_Segment,
			B.ChemicalGroup
	FROM	RP_Config_Groups G
			INNER	JOIN RP_Config_Groups_Brands B 
			ON B.GroupID = G.GroupID
	WHERE	ProgramCode = @PROGRAM_CODE AND GroupType = 'QUALIFYING_BRANDS'


	-- Get data from sales consolidated
	/*
	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardBrand, SegmentA_QualSales_CY, SegmentA_QualSales_LY_Pulse, SegmentA_QualSales_LY_Cereal, SegmentA_Reward_Sales, SegmentB_QualSales_CY, SegmentB_QualSales_LY, SegmentB_Reward_Sales, SegmentA_QualSales_LY_Pulse_INDEX, SegmentA_QualSales_LY_Cereal_INDEX, SegmentB_QualSales_LY_INDEX)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup,
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Price_CY END, 0)) [SegmentA_QualSales_CY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', CASE WHEN CS.Crop_Segment = 'PULSE' THEN TX.Price_LY1 ELSE 0 END, 0)) [SegmentA_QualSales_LY_Pulse],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', CASE WHEN CS.Crop_Segment = 'CEREAL'  THEN TX.Price_LY1 ELSE 0 END, 0)) [SegmentA_QualSales_LY_Cereal],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', TX.Price_CY, 0)) [SegmentA_Reward_Sales],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Price_CY END, 0)) [SegmentB_QualSales_CY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1, 0)) [SegmentB_QualSales_LY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_CY, 0)) [SegmentB_Reward_Sales],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', CASE WHEN CS.Crop_Segment = 'PULSE' THEN TX.Price_LY1*ISNULL(TX.YOY_Index, 1) ELSE 0 END, 0)) [SegmentA_QualSales_LY_Pulse_INDEX],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', CASE WHEN CS.Crop_Segment = 'CEREAL'  THEN TX.Price_LY1*ISNULL(TX.YOY_Index, 1) ELSE 0 END, 0)) [SegmentA_QualSales_LY_Cereal_INDEX],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1*ISNULL(TX.YOY_Index, 1), 0)) [SegmentB_QualSales_LY]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
		LEFT JOIN @CROP_SEGMENT CS 
		ON CS.ChemicalGroup = PR.ChemicalGroup
		LEFT JOIN RPWeb_User_EI_FieldValues EI
		ON EI.StatementCode=ST.StatementCode AND EI.MarketLetterCode=TX.MarketLetterCode  AND EI.FieldCode = 'Radius_Percentage_CPP'
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup
	*/

	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardBrand, SegmentA_QualSales_CY, SegmentA_QualSales_LY_Pulse, SegmentA_QualSales_LY_Cereal, SegmentA_Reward_Sales, SegmentB_QualSales_CY, SegmentB_QualSales_LY, SegmentB_Reward_Sales, SegmentA_QualSales_LY_Pulse_INDEX, SegmentA_QualSales_LY_Cereal_INDEX, SegmentB_QualSales_LY_INDEX)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup,
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', TX.Price_CY ,0)) AS [SegmentA_QualSales_CY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'PULSE' , TX.Price_LY1 ,0), 0)) [SegmentA_QualSales_LY_Pulse],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'CEREAL', TX.Price_LY1 ,0), 0)) [SegmentA_QualSales_LY_Cereal],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', TX.Price_CY, 0)) [SegmentA_Reward_Sales],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_CY, 0)) [SegmentB_QualSales_CY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1, 0)) [SegmentB_QualSales_LY],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_CY, 0)) [SegmentB_Reward_Sales],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'PULSE' ,TX.Price_LY1 * ISNULL(TX.YOY_Index, 1) ,0), 0)) [SegmentA_QualSales_LY_Pulse_INDEX],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'CEREAL' ,TX.Price_LY1 * ISNULL(TX.YOY_Index, 1) ,0), 0)) [SegmentA_QualSales_LY_Cereal_INDEX],
		   SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1 * ISNULL(TX.YOY_Index, 1), 0)) [SegmentB_QualSales_LY]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR					ON TX.ProductCode = PR.ProductCode
		LEFT JOIN @CROP_SEGMENT CS						ON CS.ChemicalGroup = PR.ChemicalGroup		
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup
	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		UPDATE T1
		SET SegmentA_QualSales_CY = SegmentA_QualSales_CY * (ISNULL(EI.FieldValue, 100) / 100)
			,SegmentB_QualSales_CY = SegmentB_QualSales_CY * (ISNULL(EI.FieldValue, 100) / 100)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode = 'Radius_Percentage_CPP'
					   			 
		-- Factor in CAR indexing percentages for the different brand groupings
		-- Pulse
		UPDATE T1
		SET SegmentA_QualSales_LY_Pulse = SegmentA_QualSales_LY_Pulse * (EI.FieldValue / 100),
			SegmentB_QualSales_LY = SegmentB_QualSales_LY * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Pulse'

		-- Cereal
		UPDATE T1
		SET SegmentA_QualSales_LY_Cereal = SegmentA_QualSales_LY_Cereal * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Cereal'

		-- SET THE INDEX VALUES IDENTICAL TO THE REGULAR VALUES (FOR FORECAST AND PROJECTIONS)
		-- This is because index values are used for display and calculation purposes (aka growth)
		UPDATE	T1
		SET		T1.SegmentA_QualSales_LY_Pulse_INDEX = T1.SegmentA_QualSales_LY_Pulse
				,T1.SegmentA_QualSales_LY_Cereal_INDEX = T1.SegmentA_QualSales_LY_Cereal
				,T1.SegmentB_QualSales_LY_INDEX =  T1.SegmentB_QualSales_LY
		FROM	@TEMP T1


	END -- END FORECAST PROJECTION

	-- Add together the separate brand groupings to get total qualifying sales for last year
	UPDATE	@TEMP
	SET		SegmentA_QualSales_LY = SegmentA_QualSales_LY_Pulse + SegmentA_QualSales_LY_Cereal
			,SegmentA_QualSales_LY_INDEX = SegmentA_QualSales_LY_Pulse_INDEX + SegmentA_QualSales_LY_Cereal_INDEX

	
	-- INSERT INTO QUALIFYING AGGREGATE NUMBERS
	INSERT	INTO @QUALIFYING(StatementCode,MarketLetterCode,ProgramCode,SegmentA_QualSales_CY,SegmentA_QualSales_LY_INDEX,SegmentB_QualSales_CY,SegmentB_QualSales_LY_INDEX)
	SELECT	StatementCode, MarketLetterCode, ProgramCode
			,SUM(SegmentA_QualSales_CY) AS SegmentA_QualSales_CY
			,SUM(SegmentA_QualSales_LY_INDEX) AS SegmentA_QualSales_LY_INDEX
			,SUM(SegmentB_QualSales_CY) AS SegmentB_QualSales_CY
			,SUM(SegmentB_QualSales_LY_INDEX) AS SegmentB_QualSales_LY_INDEX
	FROM	@TEMP 
	GROUP	BY StatementCode, MarketLetterCode, ProgramCode

	
	-- CALCULATE GROWTH PERCENTAGE IN QUALIFYING TABLE
	UPDATE @QUALIFYING
		SET SegmentA_Growth_Percentage = IIF(SegmentA_QualSales_LY_INDEX > 0, SegmentA_QualSales_CY / SegmentA_QualSales_LY_INDEX, IIF(SegmentA_QualSales_CY > 0, 9.9999, 0)),
			SegmentB_Growth_Percentage = IIF(SegmentB_QualSales_LY_INDEX > 0, SegmentB_QualSales_CY / SegmentB_QualSales_LY_INDEX, IIF(SegmentB_QualSales_CY > 0, 9.9999, 0))
	WHERE SegmentA_QualSales_CY > 0 OR SegmentB_QualSales_CY > 0


	-- INSERT THE GROWTH PERCENTAGE INTO TEMP TABLE
	UPDATE	T1
	SET		T1.SegmentA_Growth_Percentage = T2.SegmentA_Growth_Percentage
			,T1.SegmentB_Growth_Percentage = T2.SegmentB_Growth_Percentage
	FROM	@TEMP T1
	INNER	JOIN @QUALIFYING T2 
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode



	-- Set the reward percentages based on the growth
	-- Segment A
	UPDATE @TEMP SET SegmentA_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0625, 0.0450) WHERE [SegmentA_Growth_Percentage] + @ROUNDING_PERCENT >= 1.00
	UPDATE @TEMP SET SegmentA_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0575, 0.0375) WHERE [SegmentA_Reward_Percentage] = 0 AND [SegmentA_Growth_Percentage] + @ROUNDING_PERCENT >= 0.90
	UPDATE @TEMP SET SegmentA_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0350, 0.0150) WHERE [SegmentA_Reward_Percentage] = 0 AND [SegmentA_Growth_Percentage] + @ROUNDING_PERCENT >= 0.80
	UPDATE @TEMP SET SegmentA_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0300, 0.0100) WHERE [SegmentA_Reward_Percentage] = 0

	-- Segment B
	UPDATE @TEMP SET SegmentB_Reward_Percentage = IIF(Level5Code = 'D000001', 0.1225, 0.0950) WHERE [SegmentB_Growth_Percentage] + @ROUNDING_PERCENT >= 1.00
	UPDATE @TEMP SET SegmentB_Reward_Percentage = IIF(Level5Code = 'D000001', 0.1000, 0.0725) WHERE [SegmentB_Reward_Percentage] = 0 AND [SegmentB_Growth_Percentage] + @ROUNDING_PERCENT >= 0.90
	UPDATE @TEMP SET SegmentB_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0350, 0.0250) WHERE [SegmentB_Reward_Percentage] = 0 AND [SegmentB_Growth_Percentage] + @ROUNDING_PERCENT >= 0.80
	UPDATE @TEMP SET SegmentB_Reward_Percentage = IIF(Level5Code = 'D000001', 0.0300, 0.0200) WHERE [SegmentB_Reward_Percentage] = 0

	--UPDATE @TEMP SET SegmentA_Reward_Percentage = 0.0575 WHERE StatementCode=5880
	--UPDATE @TEMP SET SegmentB_Reward_Percentage = 0.1000 WHERE StatementCode=5880

	--UPDATE @TEMP SET SegmentA_Reward_Sales += 100000 WHERE StatementCode=5880
	--UPDATE @TEMP SET SegmentB_Reward_Sales += 100000 WHERE StatementCode=5880


	-- Set the reward values for each segment
	UPDATE @TEMP
		SET SegmentA_Reward = SegmentA_Reward_Sales * SegmentA_Reward_Percentage,
			SegmentB_Reward = SegmentB_Reward_Sales * SegmentB_Reward_Percentage
	--WHERE SegmentA_Reward_Sales > 0 OR SegmentB_Reward_Sales > 0

	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[SegmentA_Reward] = IIF(T2.Statement_Reward_A < 0, 0, T1.SegmentA_Reward)
			,[SegmentB_Reward] = IIF(T2.Statement_Reward_B < 0, 0, T1.SegmentB_Reward)
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(SegmentA_Reward_Sales) AS Statement_Reward_A, SUM(SegmentB_Reward_Sales) AS Statement_Reward_B
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward_A < 0 OR T2.Statement_Reward_B < 0


	-- Copy the records from the temporary table, into the permanent table
	DELETE FROM RP2021_DT_Rewards_W_LoyaltyBonus WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_LoyaltyBonus(StatementCode, MarketLetterCode, ProgramCode, RewardBrand,SegmentA_QualSales_CY, SegmentA_QualSales_LY, SegmentA_QualSales_LY_INDEX,
												 SegmentA_Growth_Percentage, SegmentA_Reward_Sales, SegmentA_Reward_Percentage, SegmentA_Reward,
												 SegmentB_QualSales_CY, SegmentB_QualSales_LY, SegmentB_QualSales_LY_INDEX ,SegmentB_Growth_Percentage, SegmentB_Reward_Sales,
												 SegmentB_Reward_Percentage, SegmentB_Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, SegmentA_QualSales_CY, SegmentA_QualSales_LY, SegmentA_QualSales_LY_INDEX, SegmentA_Growth_Percentage, SegmentA_Reward_Sales,
		   SegmentA_Reward_Percentage, SegmentA_Reward, SegmentB_QualSales_CY, SegmentB_QualSales_LY, SegmentB_QualSales_LY_INDEX, SegmentB_Growth_Percentage, SegmentB_Reward_Sales,
		   SegmentB_Reward_Percentage, SegmentB_Reward
	FROM @TEMP
END
