﻿



CREATE   PROCEDURE [dbo].[RP2020_Calc_W_AG_WS_TransitProg]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 2354
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_AG_REW_WS_TR'
	
	DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		[RetailerCode]  varchar(50) NOT NULL,		
		[ProductCode]  varchar(50) NOT NULL,
		[Reward_Percentage] DECIMAL(18,4) NOT NULL DEFAULT 0.02,
		[POG_Sales] [numeric](18, 2) NOT NULL DEFAULT 0,
		SAP_INV [numeric](18, 2) NOT NULL DEFAULT 0,
		[wxSales] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC,
		ProductCode ASC
	)
		)
	
	
	/*
	Ag Rewards Wholesaler Transition Program
	Additonal Programs

	Calculation Rules:Qualifying Level: Level 2. For standalone will be at level 1.Calculating Level: Level 2. For standalone will be at level 1.

	This program is to reward retails for purchasing BASF crop protection products from wholesalers.

	Reward Brands: All BASF ML reward brands. InVigor, Liberty & Centurion are NOT included in this reward.

	Calculation: Total Eligible POG$ - Eligible POG$ from Retail Connect = Eligible Wholesale POG$

	2% x Eligible Wholesale POG$

	Reward Opportunity

	Retails can earn 2% reward on their 2020 Eligible POG sales purchased through one of the following wholesalers: AgResource, Univar Canada Ltd., Winfield United Canada.


	******NEW UPDATE AUGUST 27 2020
	Eligible POG – ALL SAP Invoices = Eligible Wholesale POG
	Eligible POG – ALL SAP Invoices = Eligible Wholesale POG

	2% x Eligible Wholesale POG $SDP

	Eligible Wholesale POG must be equal to or greater than 0 (No negative calculations)

	All SAP Invoices = RetailConnect SAP Purchases – Rebill – Returns

	This calculation must be done a SKU level.


	*/

	DECLARE @MARKETLETTERCODE VARCHAR(50) = 'ML2020_W_ADD_PROGRAMS';
	DECLARE @SEASON_START DATE	= CAST ('10/1/2019' AS DATE);
	DECLARE @SEASON_END   DATE = CAST ('09/30/2020' AS DATE);

	--ALL BASF ML REWARD BRANDS  --NEED TESTING OR FIGURE OUT IF THERE IS A NEATER WAY TO DO THIS
	DECLARE @Eligible_Products TABLE (
		 ProductCode VARCHAR (50) NOT NULL
	); 
	--GET ELIGIBLE PRODUCTS
	INSERT INTO @Eligible_Products
	SELECT DISTINCT ProductCode
		FROM ProductReference 
		WHERE ChemicalGroup IN (
	SELECT DISTINCT ChemicalGroup FROM	Rp_Config_Groups GR
			INNER JOIN Rp_Config_Groups_Brands BR on Br.GroupID = GR.GroupID
	WHERE  BR.ChemicalGroup NOT IN ('INVIGOR','LIBERTY 150','LIBERTY 200','CENTURION')  AND GR.GroupType = 'REWARD_BRANDS'  --rewardbrands
			AND GR.MarketLetterCode IN ('ML2020_W_CCP','ML2020_W_CER_PUL_SB','ML2020_W_INV','ML2020_W_LIBERTY_200','ML2020_W_ST_INC') --elgible marketletters
			AND BR.ChemicalGroup NOT LIKE '%ALTITUDE%' AND BR.ChemicalGroup NOT LIKE '%CLEARFIELD%'   --dont want altitude and clearfield lentils from config
	)
	

	--  #TEMP_SALES
	DROP TABLE IF EXISTS #TEMP_SALES
	SELECT ST.StatementCode, RS.StatementLevel,Rs.RetailerCode,tx.RetailerCode AS EffectiveRetailerCode,tx.ProductCode,(PRP.SDP * Quantity) AS POG_Sales  
	INTO #TEMP_SALES
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode
		INNER JOIN @Eligible_Products E			ON E.ProductCode = TX.ProductCode 
		INNER JOIN ProductReferencePricing PRP	ON tx.ProductCode = PRP.ProductCode AND PRP.SEASON = @SEASON AND PRP.REGION ='WEST'
	WHERE RS.Category='Regular' AND TX.BASFSEASON = @SEASON AND tx.INRADIUS = 1 AND RS.Level5Code <> 'D0000117'
	

	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT tx.Statementcode, @MARKETLETTERCODE, @PROGRAM_CODE, tx.EffectiveRetailerCode, '' AS Farmcode,SUM(tx.POG_Sales) AS Sales, 0		
	FROM #TEMP_SALES tx			
	WHERE tx.StatementLevel IN ('LEVEL 2','LEVEL 5')
	GROUP BY tx.Statementcode, tx.EffectiveRetailerCode 

	--LETS INSERT VALID POG SALES
	/*
	INSERT INTO @TEMP(StatementCode,RetailerCode,ProductCode,POG_Sales)
	SELECT ST.StatementCode,Rs.RetailerCode,tx.ProductCode,SUM(PRP.SDP * Quantity) AS wxSales  
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode
		INNER JOIN @Eligible_Products E			ON E.ProductCode = TX.ProductCode 
		INNER JOIN ProductReferencePricing PRP	ON tx.ProductCode = PRP.ProductCode AND PRP.SEASON = @SEASON AND PRP.REGION ='WEST'
	WHERE BASFSEASON = @SEASON AND tx.INRADIUS = 1 AND RS.Level5Code <> 'D0000117'
	GROUP BY ST.StatementCode,Rs.RetailerCode, tx.ProductCode		 
	*/

	INSERT INTO @TEMP(StatementCode,RetailerCode,ProductCode,POG_Sales)
	SELECT StatementCode,RetailerCode,ProductCode,SUM(POG_Sales ) AS POG_Sales
	FROM #TEMP_SALES
	GROUP BY StatementCode,RetailerCode,ProductCode

   	  
	--NOW UPDATING SAP VALUES
	-- updated WholeSale DATA to Invoice Date RP-2911
	UPDATE T1
	SET T1.SAP_INV = SAP.SAP_INV
	FROM @TEMP T1 
		INNER JOIN (
			SELECT ST.StatementCode
					,tx.ProductCode,SUM(ISNULL(Invoice_Quantity,0) * PRP.SDP) AS SAP_INV
			FROM  @STATEMENTS ST
				INNER JOIN [dbo].[RP_StatementsMappings] MAP	
				ON MAP.StatementCode = ST.STatementCode
				INNER JOIN (
					Select Sold_To_Retailercode, coalesce(p.productcode,y.productcode) Productcode , cast(Invoice_quantity as float) Invoice_Quantity
					From (
						SELECT Sold_To_RetailerCode, Productcode, Convert(date,Invoice_Date) AS Invoice_Date,Convert(date,Order_Date) AS Order_Date, ISNULL(Invoice_quantity,0) AS Invoice_quantity, Is_Order, Is_Return, Is_Invoice, Is_Rebill
						FROM [dbo].[YSeed140]
						WHERE 
							(	(Invoice_Date BETWEEN @SEASON_START AND @SEASON_END	AND (Is_Order = 1 OR  Is_Return = 1 OR Is_Invoice = 1 OR Is_Rebill = 1))  --Criteria for determing WholeSale POG
								OR (Is_Rebill = 1 AND Invoice_Date >= CONVERT(DATE, '2020-10-01') AND Invoice_Date < CONVERT(DATE, '2021-02-01'))
							)
							AND Sold_To_RetailerCode IS NOT NULL 
					) y
					Left join (
							SELECT *, Convert(Date, Case Timestart 
												When '10/01' Then '10-01-2019'
												When '07/01' Then '07-01-2020'
											End) Time_Start,
										Convert(Date, Case TimeEnd 
												When '06/30' Then '06-30-2020'
												When '09/30' Then '09-30-2020'
												End) Time_End						    
							FROM dbo.parentproductmapping
							WHERE isnull(parentproductcode,'')  <> ''							
					) p 
					ON p.parentproductcode = y.productcode AND y.Order_Date between p.Time_start and p.Time_End
				) tx 
				ON tx.Sold_To_RetailerCode = MAP.ActiveRetailerCode
				INNER JOIN ProductReferencePricing PRP			
				ON tx.ProductCode = PRP.ProductCode AND PRP.SEASON = @SEASON AND PRP.REGION ='WEST'
			GROUP BY ST.StatementCode, tx.ProductCode 
		) SAP 
		ON SAP.StatementCode = T1.StatementCode AND SAP.ProductCode = T1.ProductCode

	--NOW UPDATE BASED ON RULES AT SKU LEVEL
	/*
	Eligible POG – ALL SAP Invoices = Eligible Wholesale POG
	*/

	UPDATE @TEMP SET SAP_INV = 0 WHERE SAP_INV < 0

	UPDATE @TEMP SET wxSales = IIF(SAP_INV > POG_Sales,0,POG_Sales - SAP_INV)
	
	UPDATE @TEMP SET [Reward_Percentage] = 0.02  WHERE wxSales > 0
	
	-- FOLLOWING RETAILERS WILL GET AN ADDITIONAL 1%
	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.03 
	WHERE wxSales > 0 
		AND [RetailerCode] IN ('R0001329','R0000507','R0000480','R0002476','R0000504','R200003341','R520085939','R0001277','R0002113','R0001653','R0002343','R0000960','R0002352','R0002550','R0001658','R520004642','xR520146531','DLR-LCAR-AW8RQH','R0000711','R0000671')
	
	
	-- FOLLOWING RETAILERS WILL GET AN ADDITIONAL 1% DLR UPDATE FEB 25, 2021
	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.03 
	WHERE wxSales > 0 
		AND [RetailerCode] IN ('DLR-TORO-BGP7VV','DLR-TORO-BY6948','DLR-TORO-BY693L','DLR-TORO-BY6955','DLR-TORO-BGP7VR','DLR-DHOY-942LZH'
		,'DLR-TORO-BY694X','DLR-TORO-BY6946','DLR-TORO-BY694L','DLR-TORO-BY694E','DLR-TORO-BY693Z','DLR-TORO-BY6959','DLR-TORO-BY694G','DLR-TORO-BY693V',
		'DLR-TORO-BY694W')
		

	--UPDATE REWARD DOLLARS
	UPDATE @TEMP SET Reward = wxSales * [Reward_Percentage]  WHERE wxSales > 0 
		
	DELETE FROM RP2020_DT_Rewards_W_AG_WS_TransitProg WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)
	
	INSERT INTO RP2020_DT_Rewards_W_AG_WS_TransitProg(StatementCode,MarketLetterCode,ProgramCode, WholeSale_Sales,Reward)
	SELECT StatementCode,@MARKETLETTERCODE AS MarketLetterCode,@PROGRAM_CODE as ProgramCode,SUM(wxSales) AS WholeSale_Sales, SUM(Reward) AS Reward
	FROM @TEMP
	GROUP BY StatementCode
	
END





