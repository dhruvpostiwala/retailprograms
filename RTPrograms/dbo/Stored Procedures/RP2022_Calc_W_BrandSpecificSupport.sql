﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_BrandSpecificSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'REWARD PLANNER'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_BRAND_SPEC_SUPPORT'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (8866)
	*/

	DECLARE @ROUND_PERCENTAGE NUMERIC(6,4) = 0.005;

	DECLARE @SalesTemp TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,			
		GroupLabel VARCHAR(50) NOT NULL,
		ProductCode VARCHAR(50) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,	
		Level5Code VARCHAR(10) NOT NULL,
		Radius_Percentage DECIMAL(6,3) NOT NULL DEFAULT 100,
		Reward_Percentage DECIMAL(6,3) NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(19,4) NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED  (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC,
			[ProductCode] ASC
		)
    )
	
	DECLARE @Temp TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,			
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,	
		Level5Code VARCHAR(10) NOT NULL,	
		Reward_Percentage DECIMAL(6,3) NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(19,4) NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	INSERT INTO @SalesTemp(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ProductCode, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Level5Code)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.ProductCode,
		SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales,
		SUM(CASE WHEN tx.GroupType='QUALIFYING_BRANDS' THEN	CASE WHEN tx.GroupLabel IN ('COTEGRA', 'CARAMBA', 'DYAX','NEXICOR') THEN (tx.Price_CY * 1.25) ELSE tx.Price_CY END ELSE 0 END) AS QualBrand_Sales_CY,
		SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Price_LY1,0)) AS QualBrand_Sales_LY1,
		SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Price_LY2,0)) AS QualBrand_Sales_LY2,
		MAX(ST2.Level5Code) Level5Code
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
		INNER JOIN RP_Statements ST2 ON ST2.StatementCode = TX.StatementCode
	WHERE tx.ProgramCode = @PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.ProductCode
	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Get editable input radius percentages
		UPDATE T1
		SET Radius_Percentage = CASE T1.GroupLabel WHEN  'INVIGOR' THEN ISNULL(E1.FieldValue,100) WHEN  'LIBERTY' THEN ISNULL(E2.FieldValue,100) WHEN  'CENTURION' THEN ISNULL(E3.FieldValue,100) ELSE  ISNULL(E4.FieldValue,100) END 
		FROM @SalesTemp T1
		INNER JOIN RPWeb_User_EI_FieldValues E1	ON  E1.StatementCode = T1.StatementCode AND E1.MarketLetterCode = T1.MarketLetterCode AND E1.FieldCode = 'Radius_Percentage_InVigor'
		INNER JOIN RPWeb_User_EI_FieldValues E2 ON  E2.StatementCode = T1.StatementCode AND E2.MarketLetterCode = T1.MarketLetterCode AND E2.FieldCode = 'Radius_Percentage_Liberty'
		INNER JOIN RPWeb_User_EI_FieldValues E3	ON  E3.StatementCode = T1.StatementCode AND E3.MarketLetterCode = T1.MarketLetterCode AND E3.FieldCode = 'Radius_Percentage_Centurion'
		INNER JOIN RPWeb_User_EI_FieldValues E4	ON  E4.StatementCode = T1.StatementCode AND E4.MarketLetterCode = T1.MarketLetterCode AND E4.FieldCode = 'Radius_Percentage_CPP'

		-- NEW: Apply Reward Planner %s
		-- Apply radius percentages
	   	UPDATE T1
		SET RewardBrand_Sales = RewardBrand_Sales * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			QualBrand_Sales_CY = QualBrand_Sales_CY * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			QualBrand_Sales_LY1 = QualBrand_Sales_LY1 * (Radius_Percentage/100), 
			QualBrand_Sales_LY2 = QualBrand_Sales_LY2 * (Radius_Percentage/100)
		FROM @SalesTemp T1
		INNER JOIN ProductReference PR ON PR.ProductCode = T1.ProductCode
		LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = PR.ChemicalGroup
		LEFT JOIN RPWeb_User_BrandInputs BI2 ON BI2.StatementCode = T1.StatementCode AND BI2.ChemicalGroup = PR.Product
		--LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = T1.GroupLabel

	END

	--IF @STATEMENT_TYPE IN ('ACTUAL')
	--BEGIN
		--TBD
	--END
		
	-- Summarize temp table without group label
	INSERT INTO @Temp(StatementCode,MarketLetterCode,ProgramCode,RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2,Level5Code)
	SELECT StatementCode, MarketLetterCode, ProgramCode,
		SUM(RewardBrand_Sales),
		SUM(QualBrand_Sales_CY),
		SUM(QualBrand_Sales_LY1),
		SUM(QualBrand_Sales_LY2),
		MAX(Level5Code) 
	FROM @SalesTemp
	GROUP BY StatementCode, MarketLetterCode, ProgramCode
	      	
	UPDATE @Temp
	SET Growth = CASE
					WHEN QualBrand_Sales_LY1 <= 0 AND QualBrand_Sales_LY2 <= 0 THEN CASE WHEN QualBrand_Sales_CY <= 0 THEN 0 ELSE 9.9999 END 
					WHEN QualBrand_Sales_LY1 > 0 AND QualBrand_Sales_LY2 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY1
					WHEN QualBrand_Sales_LY2 > 0 AND QualBrand_Sales_LY1 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY2
					ELSE QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2)
				END

	/*
	Growth in 2022 relative to the 2-year average	Reward on 2022 POG sales of InVigor
	105%+	1.5%
	95% to 104.9%	1.0%
	85% to 94.9%	0.5%
	*/

	-- Set reward tiers
	UPDATE @Temp SET [Reward_Percentage] = 0.015 WHERE ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth + @ROUND_PERCENTAGE >= 1.05)
	UPDATE @Temp SET [Reward_Percentage] = 0.01 WHERE [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.95
	UPDATE @Temp SET [Reward_Percentage] = 0.005 WHERE [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.85

	-- Set reward value based on percentage
	UPDATE @Temp  SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE RewardBrand_Sales > 0

	DELETE FROM RP2022_DT_Rewards_W_BrandSpecificSupport WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2022_DT_Rewards_W_BrandSpecificSupport(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, 'InVigor', RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward
	FROM @TEMP

END
