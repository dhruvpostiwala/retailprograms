﻿
CREATE PROCEDURE [dbo].[RP_GenerateEditHistory_TempTable] @PREVIOUS_EDIT_HISTORY RP_EDITHISTORY READONLY, @EH_TYPE VARCHAR(50), @USERNAME VARCHAR(100), @TABLE_NAME VARCHAR(100), @NEW_FIELDS NVARCHAR(MAX), @OLD_FIELDS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @CMD_PARAMS AS NVARCHAR(100)='@PREVIOUS_EDIT_HISTORY RP_EDITHISTORY READONLY, @EH_TYPE VARCHAR(50), @USERNAME VARCHAR(100)'
	DECLARE @CMD AS NVARCHAR(MAX)='	
DECLARE @EDIT_HISTORY AS RP_EDITHISTORY	

INSERT INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
SELECT COALESCE(new.[StatementCode],old.[StatementCode]),COALESCE(new.[EHLinkCode],old.[EHLinkCode]),''Field'',COALESCE(new.[Field],old.[Field]),[Old_FieldValue],[New_FieldValue]
FROM(
	SELECT [EHLinkCode],[StatementCode],REPLACE([New_Field],''NEW_'','''') AS [Field],[New_FieldValue]
	FROM '+@TABLE_NAME+'
	    UNPIVOT([New_FieldValue] FOR [New_Field] IN ('+@NEW_FIELDS+'))AS unpvt
) new
	FULL OUTER JOIN(
		SELECT [EHLinkCode],[StatementCode],REPLACE([Old_Field],''OLD_'','''') AS [Field],[Old_FieldValue]
		FROM '+@TABLE_NAME+'
			UNPIVOT([Old_FieldValue] FOR [Old_Field] IN ('+@OLD_FIELDS+'))AS unpvt 
	) old
	ON old.[EHLinkCode]=new.[EHLinkCode] AND old.[StatementCode]=new.[StatementCode] AND old.[Field]=new.[Field]
WHERE COALESCE([Old_FieldValue],'''')<>COALESCE([New_FieldValue],'''')

INSERT INTO @EDIT_HISTORY
SELECT *
FROM @PREVIOUS_EDIT_HISTORY
	
EXEC RP_GenerateEditHistory @EH_TYPE,@USERNAME,@EDIT_HISTORY'

	EXEC SP_EXECUTESQL @CMD,@CMD_PARAMS,@PREVIOUS_EDIT_HISTORY,@EH_TYPE,@USERNAME
END





