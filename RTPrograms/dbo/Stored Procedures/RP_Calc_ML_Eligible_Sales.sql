﻿
CREATE PROCEDURE [dbo].[RP_Calc_ML_Eligible_Sales]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	DELETE FROM RP_DT_ML_ELG_Sales WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)

	DECLARE @SQL nvarchar (max);
	DECLARE @Parmdef nvarchar (500);

	SET @parmdef = ' @STATEMENTS UDT_RP_STATEMENT READONLY '

	IF @STATEMENT_TYPE = 'REWARD PLANNER'
		SET @SQL = '
			INSERT INTO RP_DT_ML_ELG_Sales(StatementCode,RetailerCode,MarketLetterCode,GroupLabel,Pricing_Type,Sales_CY,Sales_LY1,Sales_LY2)
			SELECT ST.StatementCode,RetailerCode,MarketLetterCode,IIF(GroupLabel=''INVIGOR'',''INVIGOR ''+prh.Hybrid_Name,GroupLabel),Pricing_Type,SUM(Price_CY),SUM(Price_LY1),SUM(Price_LY2)
			FROM @STATEMENTS ST
				INNER JOIN RP'+CAST(@SEASON AS VARCHAR(4))+'_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode  
				INNER JOIN ProductReference pr on tx.ProductCode = pr.ProductCode
				LEFT JOIN ProductReference_Hybrids prh on pr.Hybrid_ID = prh.ID
			WHERE TX.ProgramCode=''ELIGIBLE_SUMMARY'' AND TX.GroupType=''ELIGIBLE_BRANDS''
			GROUP BY ST.StatementCode,RetailerCode,MarketLetterCode,IIF(GroupLabel=''INVIGOR'',''INVIGOR ''+prh.Hybrid_Name,GroupLabel),Pricing_Type
			'
	ELSE
		SET @SQL = '
			INSERT INTO RP_DT_ML_ELG_Sales(StatementCode,RetailerCode,MarketLetterCode,GroupLabel,Pricing_Type,Sales_CY,Sales_LY1,Sales_LY2)
			SELECT ST.StatementCode,RetailerCode,MarketLetterCode,GroupLabel,Pricing_Type,SUM(Price_CY),SUM(Price_LY1),SUM(Price_LY2)
			FROM @STATEMENTS ST
				INNER JOIN RP'+CAST(@SEASON AS VARCHAR(4))+'_DT_Sales_Consolidated TX		
				ON TX.StatementCode=ST.StatementCode  
			WHERE TX.ProgramCode=''ELIGIBLE_SUMMARY'' AND TX.GroupType=''ELIGIBLE_BRANDS''   
			GROUP BY ST.StatementCode,RetailerCode,MarketLetterCode,GroupLabel,Pricing_Type
			'
		
	EXEC sp_executeSQL @SQL, @Parmdef,  @STATEMENTS
	
END