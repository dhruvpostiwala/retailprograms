﻿

CREATE PROCEDURE [dbo].[RP2021_Calc_Rewards_Summary_West] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SEASON_STRING VARCHAR(4)= '2021'
	DECLARE @INV_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_INV'
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'
	DECLARE @ADDL_PROGRAMS_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_ADD_PROGRAMS'
	
	DELETE REW
	FROM RP_DT_Rewards_Summary REW
	INNER JOIN @STATEMENTS ST
	ON ST.StatementCode=REW.StatementCode

	-- BRAND SPECIFIC SUPPORT
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_BrandSpecificSupport RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_BRAND_SPEC_SUPPORT'

	-- EFFICIENCY REBATE
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_EFF_BONUS RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_EFF_BONUS_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- INVIGOR & LIBERTY LOYALTY BONUS
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_LLB RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_INV_LIB_LOY_BONUS'

	-- INVIGOR PERFORMANCE		
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_PERF RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_INV_PERFORMANCE'

	-- WAREHOUSE PAYMENT
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_WarehousePayments RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_WAREHOUSE_PAYMENT'

	-- PAYMENT ON TIME
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_PaymentOnTime RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_PAYMENT_ON_TIME'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- INVIGOR INNOVATION
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, IIF(SUM(ISNULL(RS.Reward,0)) > 0, SUM(ISNULL(RS.Reward,0)), 0)  AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_INNOV RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_INV_INNOVATION_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Inventory Management
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward, 0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_InventoryManagement RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_INVENTORY_MGMT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Liberty/ Centurion / Facet L Tank Mix Bonus 
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward, 0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_TankMixBonus RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_TANK_MIX_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Planning the Business Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(COALESCE(RS.Reward,RS2.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG							ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG								ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_PlanningTheBusiness RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
		LEFT JOIN RP2021_DT_Rewards_W_PlanningTheBusiness_Actual RS2	ON RS2.StatementCode=ST.StatementCode AND RS2.ProgramCode=ELG.ProgramCode AND RS2.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_PLANNING_SUPP_BUSINESS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Logistics Support Reward
	/*
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_LogisticsSupport RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_LOGISTICS_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	*/

	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN (	
			SELECT StatementCode, MarketLetterCode, ProgramCode, Reward FROM RP2021_DT_Rewards_W_LogisticsSupport
			  UNION ALL
			SELECT StatementCode, MarketLetterCode, ProgramCode, Reward FROM RP2021_DT_Rewards_W_LogisticsSupport_PartB
	    ) RS 
		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_LOGISTICS_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Growth Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_GrowthBonus RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_GROWTH_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Loyalty Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN (
			SELECT StatementCode, MarketLetterCode, ProgramCode, SegmentA_Reward + SegmentB_Reward + ISNULL(SegmentC_Reward,0) + ISNULL(SegmentD_Reward,0)  AS Reward
			FROM RP2021_DT_Rewards_W_LoyaltyBonus 			
		) RS		
		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_LOYALTY_BONUS' 
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	
	
	-- Segment Selling Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_SegmentSelling RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_SEGMENT_SELLING'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Retail Bonus Payment
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_RetailBonus RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_RETAIL_BONUS_PAYMENT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Custom Seed Treatment
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_CustomSeed RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_CUSTOM_SEED_TREATING_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Clearfield Lentils Support Forecast Projection
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S							ON S.StatementCode = ST.StatementCode 
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_ClearfieldLentils RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE StatementType IN ('FORECAST', 'PROJECTION') AND ELG.ProgramCode='RP2021_W_CLL_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Clearfield Lentils Support Actuals
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S							ON S.StatementCode = ST.StatementCode 
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_ClearfieldLentils_Actual RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE S.StatementType = 'ACTUAL' AND ELG.ProgramCode='RP2021_W_CLL_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Dicamba Tolerant Program
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_DicambaTolerant RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_DICAMBA_TOLERANT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- NOdulator DUO SCG Program
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_Nod_Duo_SCG RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_NOD_DUO_SCG'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Fungicide Support Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_Fung_Support RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_FUNG_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Advanced Weed Control Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_Adv_Weed_Control RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_ADVANCED_WEED_CTRL'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Cereal Start to Finish
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_Cereal_SFO RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_CEREAL_START_FINISH'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Custom Aerial App
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_CustomAerial RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_CUSTOM_AER_APP'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Heat up your Harvest
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_HeatUpHarvest RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_HEAT_UP_YOUR_HARVEST'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Inventory Reduction Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_W_InventoryReduction RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_W_INVENTORY_REDUCTION'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	---- Clearfield Wheat Support Reward --Nutrien
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)		
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S						ON S.Statementcode=ST.Statementcode 
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN (
			SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
			FROM RP2021_DT_Rewards_W_CLW_Herb
		) RS	
		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE S.StatementType='Actual' AND ELG.ProgramCode='RP2021_W_CLW_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode


	-- Centurion After Market Payment	
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN  RP2021_DT_Rewards_W_Cent_AFM RS		ON RS.StatementCode=ST.StatementCode 
	WHERE ELG.ProgramCode='RP2021_W_CENT_AFTER_MARKET'

	-- Liberty After Market Payment	
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN  RP2021_DT_Rewards_W_Lib_AFM RS		ON RS.StatementCode=ST.StatementCode 
	WHERE ELG.ProgramCode='RP2021_W_LIB_AFTER_MARKET'


	--Incentive
	
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode
		,@ADDL_PROGRAMS_ML_CODE AS MarketLetterCode
		,'Incentives_W'AS RewardCode
		,ISNULL(SUM(RI.Amount),0) AS RewardValue
		,'Eligible' AS RewardEligibility
		,'' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_Incentives RI							
		ON RI.StatementCode = ST.StatementCode 
	GROUP BY ST.StatementCode


	
		--APA Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, @ADDL_PROGRAMS_ML_CODE AS MarketLetterCode, APA.RewardCode AS RewardCode
		,ISNULL(SUM(APA.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
	INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
	INNER JOIN RP_Seeding_APAPayments APA ON APA.RetailerCode = MAP.RetailerCode AND RS.Season = APA.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'WEST'
	GROUP BY ST.StatementCode, APA.RewardCode
	
	--Over/UnderPayments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, @ADDL_PROGRAMS_ML_CODE AS MarketLetterCode, OUP.RewardCode AS RewardCode
	,ISNULL(SUM(OUP.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
		INNER JOIN RP_Seeding_Over_Under_Payments OUP ON OUP.RetailerCode = MAP.RetailerCode AND RS.Season = OUP.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'WEST'
	GROUP BY ST.StatementCode, OUP.RewardCode


	/****************** HEAD OFFICE ******************/

	-- SUMMARIZE REWARDS AT HEAD OFFICE LEVEL
	-- FCL -- ROLL UP REWARDS TO LEVEL 5 STATEMENT	
	DECLARE @HO_STATEMENTS TABLE(
		StatementCode INT NOT NULL,
		StatementCode_L5 INT NOT NULL
	)

	INSERT INTO @HO_STATEMENTS(StatementCode,StatementCode_L5)
	SELECT DISTINCT ST2.StatementCode, ST2.DataSetCode_L5 AS StatementCode_L5	
	FROM @STATEMENTS ST1
		INNER JOIN RP_Statements ST2				ON ST2.StatementCode=ST1.StatementCode		
		INNER JOIN RP_Config_HOPaymentLevel HO 		ON HO.Level5Code=ST2.Level5Code AND HO.Season=ST2.Season
	WHERE HO.Summary='Yes'

	DELETE REW
	FROM RP_DT_Rewards_Summary REW		
		INNER JOIN (
			SELECT DISTINCT StatementCode_L5 AS Statementcode FROM @HO_STATEMENTS
		) T2
		ON T2.Statementcode=REW.Statementcode
			
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode,  SUM(REW.RewardValue), 'Eligible' , ''
	FROM @HO_STATEMENTS HO
		INNER JOIN RP_DT_Rewards_Summary REW	
		ON REW.StatementCode=HO.StatementCode	
	WHERE RewardEligibility='Eligible'
	GROUP BY HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode




	
END
