﻿

CREATE PROCEDURE [dbo].[RP2021_Refresh_Sales_Consolidated]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SEASON_LY1 INT = @SEASON-1;
	DECLARE @SEASON_LY2 INT = @SEASON-2;

	DECLARE @EAST_STATEMENTS_EXIST BIT=0
	DECLARE @WEST_STATEMENTS_EXIST BIT=0

		
	DROP TABLE IF EXISTS #TEMP_Statements
	SELECT ST.Season, ST.Region, ST.StatementCode, ST.RetailerCode,  ISNULL(ST.DataSetCode,0) AS DataSetCode, ISNULL(ST.DataSetCode_L5,0) AS DataSetCode_L5, ST.Level5Code
	INTO #TEMP_Statements
	FROM RP_STATEMENTS  ST
		INNER JOIN @STATEMENTS T1
		ON T1.Statementcode=ST.StatementCode
	WHERE SEASON=@SEASON

	IF EXISTS(SELECT TOP 1 1 FROM #TEMP_Statements WHERE Region='WEST')	
		SET @WEST_STATEMENTS_EXIST=1

	IF EXISTS(SELECT TOP 1 1 FROM #TEMP_Statements WHERE Region='EAST')
		SET @EAST_STATEMENTS_EXIST=1

	/* Collect all sales information into a temporary table */
	DROP TABLE IF EXISTS #TEMP_Sales
	SELECT RS.StatementCode, RS.Level5Code, tx.RetailerCode, tx.TransType, tx.BASFSeason, tx.ProductCode, tx.ChemicalGroup, tx.Quantity, tx.Acres, tx.Price_SDP, tx.Price_SRP, tx.Price_SWP, tx.InRadius
	INTO #TEMP_Sales
	FROM #TEMP_Statements RS		
		INNER JOIN (
			/* HOUSES CURRENT YEAR POG PLAN TRANSACTIONS ONLY */
			SELECT StatementCode, RetailerCode, 'POG_Plan' AS TransType, @SEASON AS BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, 1 AS InRadius 
			FROM RP_DT_ScenarioTransactions
			
				UNION ALL

			/* IN CASE OF FORECAST/PROJECTION STATEMENTS, HOLDS ONLY LAST TWO YEARS TRANSACTIONS */
			SELECT StatementCode, RetailerCode, 'RET_TRANSACTION' AS TransType, BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, InRadius 
			FROM RP_DT_Transactions 			
			
		) TX
		ON TX.StatementCode=RS.StatementCode


	/********************************************************/
	-- GROUPS CONFIG DATA
	
	DROP TABLE IF EXISTS #CONFIG_GROUPS

	SELECT GR.GroupID,GR.MarketLetterCode,GR.ProgramCode,GR.GroupLabel,GR.GroupType,ISNULL(BR.ChemicalGroup,'N/A')  ChemicalGroup
	INTO #CONFIG_GROUPS
	FROM RP_CONFIG_GROUPS GR
		INNER JOIN RP_Config_MarketLetters ML		ON GR.MarketLetterCode=ML.MarketLetterCode
		LEFT JOIN RP_Config_Groups_Brands BR		ON BR.GroupID = GR.GroupID
	WHERE ML.Season=@SEASON 

	-- NO NEED TO INSERT FOLLOWING  PROGRAM / GROUPTYPE COMIBINATIONS INTO CONSOLIDATED TABLE
	-- BETTER APPROACH WOULD BE TO INTRODUCE A COLUMN IN RP_CONFIG_ GROUPS TO FLAG TO BE EXCLUDED -- EX: ISNULL(GR.ExcludeFromConsolidatedSales,0) = 1
	DELETE FROM #CONFIG_GROUPS 
	WHERE GroupType = 'QUALIFYING_BRANDS' 
		AND ProgramCode IN ('RP2021_W_SEGMENT_SELLING','RP2021_W_CUSTOM_SEED_TREATING_REWARD','RP2021_W_DICAMBA_TOLERANT')
	
	--- NO NEED FOR THIS TO BE PART OF RP_CONFIG_GROUPS
	DELETE FROM #CONFIG_GROUPS WHERE ProgramCode = 'RP2021_W_TANK_MIX_BONUS'

	/***************************************************************************************/
	/* MARKET LETTER ELIGIBLE PROGRAMS LIST */			
	DROP TABLE IF EXISTS #TEMP_ELG_MLP

	SELECT MLP.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode 
	INTO #TEMP_ELG_MLP
	FROM RP_DT_ML_ElG_Programs MLP
		INNER JOIN @STATEMENTS ST
		ON ST.StatementCode=MLP.StatementCode
	GROUP BY MLP.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode 

	
	/* 
		Note: 
			ProgramCode "ELIGIBLE_SUMMARY" is not listed in RP_Config_Programs, hence it is not part of "RP_DT_ML_ElG_Programs" table.
			But, programCode "ELIGIBLE_SUMMARY" is added to table "[RP_Config_Groups]" to keep track of eligible (Reward) brands sales (i.e., reward will paid out on) 
	*/
	INSERT INTO #TEMP_ELG_MLP(StatementCode, MarketLetterCode, ProgramCode)
	SELECT StatementCode, MarketLetterCode, 'ELIGIBLE_SUMMARY' AS ProgramCode 
	FROM #TEMP_ELG_MLP
	GROUP BY StatementCode, MarketLetterCode


	/***************************************************************************************/
	/* Lets consolidate sales to group by market letter, programcode and chemicalgroup, reward/qualifying brand */
	DROP TABLE IF EXISTS #Consolidated_Sales

	/* LETS INSERT TRANSACATION BASED ON CONFIG SETUP */		
	SELECT ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode , MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,'') AS ProgramCode, GR.GroupType						
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			
	
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	INTO #Consolidated_Sales
	FROM @STATEMENTS ST
		INNER JOIN #TEMP_ELG_MLP MLP				ON MLP.StatementCode=ST.StatementCode
		/*
		INNER JOIN RP_Config_Groups GR				ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode						
		INNER JOIN RP_Config_Groups_Brands BR		ON BR.GroupID=GR.GroupID
		*/
		INNER JOIN #CONFIG_GROUPS GR				ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode
		INNER JOIN #TEMP_Sales TX					ON TX.StatementCode=ST.StatementCode AND TX.ChemicalGroup=GR.ChemicalGroup	-- BR.ChemicalGroup	
	GROUP BY ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,''), GR.GroupType
	OPTION  (RECOMPILE)
			
	-- FOLLOWING '#ELIGIBLE_SUMMARY_SALES' DATA SET ASSISTS IN WORKING WITH GROUP TYPES 'ALL_CPP_BRANDS' AND 'ALL_EAST_BRANDS'
	DROP TABLE IF EXISTS #ELIGIBLE_SUMMARY_SALES
	SELECT *
	INTO #ELIGIBLE_SUMMARY_SALES
	FROM #Consolidated_Sales tx
	WHERE TX.ProgramCode = 'ELIGIBLE_SUMMARY' AND TX.GroupType = 'ELIGIBLE_BRANDS' 


	-- ALL CANOLA PROTECTION PRODUCTS BRANDS -- IS USED FOR WEST MARKET LETTER ONLY
	-- LETS INSERT TRANSACTIONS FOR WEST GROUPS WHERE NO CHEMICAL GROUP MAPPING IS PROVIDED
	IF @WEST_STATEMENTS_EXIST = 1			
	BEGIN 		
		/* maintain select column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
		INSERT INTO #Consolidated_Sales
		SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
			,Acres_Forecast
			,SDP_Forecast,SRP_Forecast,SWP_Forecast
			,Acres_Q_CY	,Acres_E_CY				,Acres_Q_LY1,Acres_E_LY1			,Acres_Q_LY2,Acres_E_LY2
			,Price_Q_SDP_CY	,Price_E_SDP_CY		,Price_Q_SDP_LY1,Price_E_SDP_LY1	,Price_Q_SDP_LY2,Price_E_SDP_LY2
			,Price_Q_SRP_CY	,Price_E_SRP_CY		,Price_Q_SRP_LY1,Price_E_SRP_LY1	,Price_Q_SRP_LY2,Price_E_SRP_LY2			
			,Price_Q_SWP_CY,Price_E_SWP_CY		,Price_Q_SWP_LY1,Price_E_SWP_LY1	,Price_Q_SWP_LY2,Price_E_SWP_LY2	
		FROM #ELIGIBLE_SUMMARY_SALES TX
			INNER JOIN (
				SELECT StatementCode, MarketLetterCode, ProgramCode
				FROM #TEMP_ELG_MLP 
				WHERE ProgramCode <> 'ELIGIBLE_SUMMARY'
			) MLP
			ON MLP.StatementCode=TX.StatementCode AND MLP.MarketLetterCode=tx.MarketLetterCode			
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, GroupType
				FROM #CONFIG_GROUPS --RP_Config_Groups  				
				WHERE GroupType='ALL_CPP_BRANDS' -- Do not specify market letter as this group is applicable on both market letters
			) GR
			ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode		
		OPTION  (RECOMPILE)
	END

	-- ALL EAST BRANDS	
	-- -- LETS INSERT TRANSACTIONS FOR EAST GROUPS WHERE NO CHEMICAL GROUP MAPPING IS PROVIDED
	IF @EAST_STATEMENTS_EXIST = 1
	BEGIN 	
		/* maintain select column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
		INSERT INTO #Consolidated_Sales
		SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
			,Acres_Forecast
			,SDP_Forecast,SRP_Forecast,SWP_Forecast
			,Acres_Q_CY	,Acres_E_CY				,Acres_Q_LY1,Acres_E_LY1			,Acres_Q_LY2,Acres_E_LY2
			,Price_Q_SDP_CY	,Price_E_SDP_CY		,Price_Q_SDP_LY1,Price_E_SDP_LY1	,Price_Q_SDP_LY2,Price_E_SDP_LY2
			,Price_Q_SRP_CY	,Price_E_SRP_CY		,Price_Q_SRP_LY1,Price_E_SRP_LY1	,Price_Q_SRP_LY2,Price_E_SRP_LY2			
			,Price_Q_SWP_CY,Price_E_SWP_CY		,Price_Q_SWP_LY1,Price_E_SWP_LY1	,Price_Q_SWP_LY2,Price_E_SWP_LY2	
		FROM #ELIGIBLE_SUMMARY_SALES TX
			INNER JOIN (
				SELECT StatementCode, MarketLetterCode, ProgramCode
				FROM #TEMP_ELG_MLP 
				WHERE ProgramCode <> 'ELIGIBLE_SUMMARY'
			) MLP
			ON MLP.StatementCode=TX.StatementCode AND MLP.MarketLetterCode=tx.MarketLetterCode			
			INNER JOIN (
				SELECT MarketLetterCode, ProgramCode, GroupType
				FROM #CONFIG_GROUPS --RP_Config_Groups  				
				WHERE GroupType='ALL_EAST_BRANDS' 
			) GR
			ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode			
		OPTION  (RECOMPILE)
	END

	/**********************************************/
	
	--CREATE CLUSTERED INDEX idx_cons_sales ON #Consolidated_Sales ([Level5Code],[ProductCode],[MarketLetterCode],[ProgramCode]);

	/************** DATA HYGIENE BEGIN ***********/
	-- BOTH REGIONS (EAST AND WEST)
	BEGIN   	  
		-- Remove InVigor hybrids that have a "DistIncluded" property in the "OtherInfo" column of ProductReference_Hybrids from reward brands, if the retailer's Level5Code isn't in the list for the property.
		DELETE CS
		FROM #Consolidated_Sales CS
			INNER JOIN ProductReference PR
			ON CS.ProductCode = PR.ProductCode
			INNER JOIN ProductReference_Hybrids PRH
			ON PR.Hybrid_ID = PRH.ID
		WHERE JSON_QUERY(PRH.OtherInfo, '$.DistIncluded') IS NOT NULL
		AND CS.Level5Code NOT IN (
			SELECT value [Level5Code]
			FROM OPENJSON((SELECT JSON_QUERY(OtherInfo, '$.DistIncluded') [IncludedDist] FROM ProductReference_Hybrids WHERE ID = PRH.ID))
		)

		DELETE CS
		FROM #Consolidated_Sales CS
			INNER JOIN (
				SELECT ProductCode
				FROM ProductReference 
				WHERE (
					(ChemicalGroup = 'NODULATOR PRO' AND Product <> 'NODULATOR PRO 100')
					OR (ChemicalGroup = 'FRONTIER' AND Product <> 'FRONTIER MAX')
					OR (ChemicalGroup = 'PROWL' AND Product <> 'PROWL H2O')
					OR (ChemicalGroup = 'POAST' AND Product <> 'POAST ULTRA')
				)
			) PR
			ON CS.ProductCode = PR.ProductCode		
	END

	-- WEST DATA HYGIENE
	IF @WEST_STATEMENTS_EXIST = 1
	BEGIN	
		-- RC-4173: Remove Outlook from all west reward qualifications and calculations
		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN (
				SELECT ProductCode
				FROM ProductReference 				
				WHERE (
					ChemicalGroup = 'Outlook'
					OR	Product IN ('PURSUIT HERBICIDE (E)', 'MULTISTAR','DISTINCT (E)')					
				)
			) PR
			ON TX.ProductCode = PR.ProductCode
		WHERE  TX.MarketLetterCode  IN ('ML2021_W_CPP', 'ML2021_W_INV')

			
		-- KM: REMOVE NON REWARD INVIGOR PRODUCTS (WEST MARKET LETTERS)
		DELETE CS
		FROM #Consolidated_Sales CS
			INNER JOIN (
				SELECT ProductCode
				FROM ProductReference 
				WHERE ChemicalGroup = 'INVIGOR'	
					AND Product NOT IN  ('InVigor L340PC','INVIGOR CHOICE LR344PC','InVigor L345PC','InVigor L352C','InVigor L357P','InVigor L230','InVigor L233P','InVigor L234PC','InVigor L241C','InVigor L252','InVigor L255PC')
			) PR 
			ON CS.ProductCode = PR.ProductCode
		WHERE CS.MarketLetterCode IN ('ML2021_W_INV', 'ML2021_W_CPP') 
			
		-- JIRA: https: //kennahome.jira.com/browse/RP-4187
		DELETE CS
		FROM #Consolidated_Sales CS
		WHERE CS.Level5Code IN ('D0000107','D0000137','D0000244','D520062427') 
			AND CS.ProgramCode = 'RP2021_W_BRAND_SPEC_SUPPORT'
			AND CS.GroupLabel IN ('COTEGRA', 'LANCE', 'LANCE AG')

		DELETE CS
		FROM #Consolidated_Sales CS
		WHERE CS.Level5Code IN ('D000001','D0000117') 
			AND CS.ProgramCode = 'RP2021_W_LOYALTY_BONUS'
			AND CS.GroupLabel IN ('SEGMENT C', 'SEGMENT D')
		
	
		-- RP2021_W_LOGISTICS_SUPPORT: Include Heat LQ (Pre Seed) in "Other Herbicides" and Heat LQ (Pre-Harvest) ‘Fall Herbicides' (RP-4013)
		UPDATE TX
		SET GroupLabel = 'OTHER HERBICIDES'
		FROM #Consolidated_Sales TX
			LEFT JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT' AND PR.Product IN ('HEAT LQ (PRE-SEED)', 'HEAT LQ BULK (PRE-SEED)') AND TX.GroupLabel ='FALL HERBICIDES'
	
		DELETE FROM #Consolidated_Sales WHERE ProgramCode = 'RP2021_W_INVENTORY_MGMT' AND GroupLabel LIKE 'NODULATOR%'

		-- D0000137 Richardson Has Nodulator SCG component $2.50/matched acre -- remove it for others
		DELETE FROM #Consolidated_Sales WHERE ProgramCode = 'RP2021_W_DICAMBA_TOLERANT' AND GroupLabel = 'NODULATOR SCG' AND Level5Code <> 'D0000137'
	
					
		DELETE FROM #Consolidated_Sales WHERE ProgramCode = 'RP2021_W_SEGMENT_SELLING' AND GroupLabel IN ('INVIGOR', 'Liberty 150', 'Liberty 200', 'Centurion')

		-- INVENTORY MANAGEMENT 
		-- NUTRIEN DOES NOT HAVE SEGMENT A
		DELETE RP2021_DT_Sales_Consolidated
		WHERE ProgramCode='RP2021_W_INVENTORY_MGMT' AND MarketLetterCode='ML2021_W_CPP' AND Level5Code = 'D520062427'

		-- SEGMENT B APPLICABLE FOR LINE COMPANIES ONLY
		DELETE RP2021_DT_Sales_Consolidated
		WHERE ProgramCode='RP2021_W_INVENTORY_MGMT' AND MarketLetterCode='ML2021_W_INV' AND GroupType='REWARD_BRANDS' AND RetailerCode NOT IN ('D0000107','D0000244','D0000137','D520062427')	
		
		-- NUTRIEN ONLY PRODUCTS
		DELETE T1
		FROM #Consolidated_Sales T1
			INNER JOIN ProductReference PR
			ON PR.ProductCode=T1.ProductCode
		WHERE T1.Level5Code <> 'D520062427'	
			AND  PR.ChemicalGroup IN ('DUET','MIZUNA','ALTITUDE FX','ALTITUDE FX2','ALTITUDE FX 2','ALTITUDE FX3','ALTITUDE FX 3','HEAT') 	  	
	END

	-- EAST DATA HYGIENE
	IF @EAST_STATEMENTS_EXIST = 1
	BEGIN
	   -- KM: REMOVE NON REWARD INVIGOR PRODUCTS (EAST MARKET LETTERS)
		DELETE CS
		FROM #Consolidated_Sales CS
			INNER JOIN ProductReference PR ON CS.ProductCode = PR.ProductCode
		WHERE CS.MarketLetterCode IN ('ML2021_E_CPP', 'ML2021_E_INV')
			AND PR.ChemicalGroup = 'INVIGOR'			
			AND PR.Product NOT IN  ('INVIGOR L345PC', 'INVIGOR L357P', 'INVIGOR L233P', 'INVIGOR L234PC', 'INVIGOR L255PC')


		-- KAM: Remove Specific Products EAST (REMOVE SPECIFIC PRODUCTS WITHIN ELIGIBLE BRANDS)
		DELETE CS
		FROM #Consolidated_Sales CS
			INNER JOIN ProductReference PR
			ON CS.ProductCode = PR.ProductCode
		WHERE CS.MarketLetterCode IN ('ML2021_E_CPP', 'ML2021_E_INV')
			AND (
				 (CS.GroupLabel = 'PROWL' AND PR.Product IN ('PROWL 400'))
			)

		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.ProgramCode = 'RP2021_E_PLANNING_SUPP_BUSINESS' AND PR.ChemicalGroup IN ('KUMULUS', 'INVIGOR')

		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.ProgramCode = 'RP2021_E_PORTFOLIO_SUPPORT' AND 
			(PR.ChemicalGroup = 'INVIGOR' OR (PR.ChemicalGroup = 'PROWL' AND PR.Product = 'PROWL 400'))


		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.MarketLetterCode = 'ML2021_E_CPP' AND TX.ProgramCode = 'RP2021_E_SOLLIO_GROWTH_BONUS' AND PR.ChemicalGroup = 'INVIGOR'

		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.MarketLetterCode = 'ML2021_E_CPP' AND TX.ProgramCode = 'RP2021_E_SUPPLY_SALES' AND PR.ChemicalGroup = 'INVIGOR'

	
		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND PR.ChemicalGroup IN ('FRONTIER') AND PR.Product NOT IN ('FRONTIER MAX')
		

		DELETE TX
		FROM #Consolidated_Sales TX
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
		WHERE TX.MarketLetterCode = 'ML2021_E_CPP' AND TX.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND PR.ChemicalGroup = 'PROWL' AND PR.Product <> 'PROWL H2O'
		
	END


	/************** DATA HYGIENE END ***********/

	/******** NON REWARD BRANDS **********/
	BEGIN
		INSERT INTO #Consolidated_Sales	
		SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode 
			,'NON_REWARD_BRANDS' AS MarketLetterCode , 'NON_REWARD_BRANDS_SUMMARY' ProgramCode , 'NON_REWARD_BRANDS' AS GroupType
			,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
			,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
			,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
			,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

			,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
			,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

			,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

			,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

			,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
			,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

			,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
			,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
			,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
			
		FROM #TEMP_Sales TX		
			LEFT JOIN (
				SELECT DISTINCT StatementCode, Productcode
				FROM #Consolidated_Sales 
				WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS'
			) TX2
			ON TX2.StatementCode=TX.StatementCode AND TX2.ProductCode=TX.Productcode 
		WHERE TX2.ProductCode IS NULL
			-- FOR AUGUST CHEQUE RUN DO NOT INCLUDE NON-REWARD BRANDS DETAILS
			--AND TX.Level5Code NOT IN ('D0000117','D0000107','D0000137','D0000244','D520062427') 		
		GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode
	END

	/* LETS START SETTING DATA VALUES AS NEEDED DEFINED BY BRD */
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')	
	BEGIN
		UPDATE #Consolidated_Sales
		SET Acres_E_CY=Acres_Forecast
			,Acres_E_LY1=Acres_Q_LY1
			,Acres_E_LY2=Acres_Q_LY2

			,Price_E_SDP_CY=SDP_Forecast
			,Price_E_SDP_LY1=Price_Q_SDP_LY1
			,Price_E_SDP_LY2=Price_Q_SDP_LY2

			,Price_E_SRP_CY=SRP_Forecast
			,Price_E_SRP_LY1=Price_Q_SRP_LY1
			,Price_E_SRP_LY2=Price_Q_SRP_LY2

			,Price_E_SWP_CY=SWP_Forecast
			,Price_E_SWP_LY1=Price_Q_SWP_LY1
			,Price_E_SWP_LY2=Price_Q_SWP_LY2
	END

	ALTER TABLE #Consolidated_Sales ADD [Pricing_Type] [VARCHAR](3) NULL 

	IF @WEST_STATEMENTS_EXIST = 1
	BEGIN
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SWP' WHERE Level5Code IN ('D0000107','D0000137','D0000244','D520062427') -- LINE COMPANIES
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SRP' WHERE Level5Code = 'D0000117' -- FCL/COOP
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SDP' WHERE Level5Code = 'D000001' -- INDEPENDENTS / RC

		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SRP' WHERE ProgramCode = 'RP2021_W_CUSTOM_AER_APP'
	END

	IF @EAST_STATEMENTS_EXIST = 1
	BEGIN
		UPDATE CS
		SET [Pricing_Type] =  IIF(PR.ChemicalGroup='INVIGOR','SRP','SDP')
		FROM #Consolidated_Sales CS
			INNER JOIN RP_STATEMENTS RS on CS.StatementCode = RS.StatementCode
			INNER JOIN ProductReference PR on PR.ProductCode = CS.ProductCode
		WHERE RS.Region='EAST'
	END
		
	UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SRP' WHERE Pricing_Type IS NULL

	--  Make sure to insert correct price values based on pricing type  in columns Price_CY, Price_LY1, Price_LY2
	
	DELETE FROM RP2021_DT_Sales_Consolidated WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)
	
	INSERT INTO RP2021_DT_Sales_Consolidated(StatementCode,DataSetCode,DataSetCode_L5,Level5Code,RetailerCode,GroupLabel,ProductCode,MarketLetterCode,ProgramCode,GroupType,Pricing_Type
		,Acres_Forecast
		,Price_Forecast		
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_CY, Acres_LY1 ,Acres_LY2
		,Price_CY, Price_LY1, Price_LY2	)	
	SELECT TX.StatementCode, ST.DataSetCode, ST.DataSetCode_L5, ST.Level5Code ,TX.RetailerCode
		,GroupLabel,ProductCode, MarketLetterCode, ProgramCode, GroupType,Pricing_Type
		,Acres_Forecast
		, CASE WHEN Pricing_Type='SRP' THEN SRP_Forecast
			   WHEN Pricing_Type='SWP' THEN SWP_Forecast
			   ELSE SDP_Forecast 
		  END AS [Price_Forecast]
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_E_CY AS [Acres_CY], Acres_E_LY1 AS [Acres_LY1], Acres_E_LY2  AS [Acres_LY2]
		,CASE 
			WHEN Pricing_Type='SRP' THEN Price_E_SRP_CY
			WHEN Pricing_Type='SWP' THEN Price_E_SWP_CY
			ELSE Price_E_SDP_CY  
		END AS Price_CY
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY1
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY1
			 ELSE  Price_E_SDP_LY1
		  END AS Price_LY1
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY2
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY2
			 ELSE  Price_E_SDP_LY2
		  END AS Price_LY2
	FROM #TEMP_Statements ST
		INNER JOIN #Consolidated_Sales TX		
		ON TX.StatementCode=ST.StatementCode
END


