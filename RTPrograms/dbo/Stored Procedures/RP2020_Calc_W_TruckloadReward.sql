﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_W_TruckloadReward]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	Retails receive $2,000/shipment for Single Brand truckload shipments of one of the eligible brands respectively, provided there are
	20 pallets on the truckload of the same brand and a maximum of 2 drops of inventory within a 200km radius.

	Retails receive $1,500/shipment for Mixed Brand truckload shipments of 2 or more eligible brands collectively, provided there are
	20 pallets on the truckload of both brands a maximum of 2 drops of inventory within a 200km radius.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_TRUCKLOAD_REWARD'
	
	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		SingleBrand_DropCount int NOT NULL,		
		SingleBrand_Reward Money NOT NULL DEFAULT 0,
		MixedBrand_DropCount int NOT NULL,
		MixedBrand_Reward Money NOT NULL DEFAULT 0 ,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @SingleReward INT = 2000;
	DECLARE @MixedReward INT = 1500;

	-- Just give them the reward based on 2 drops
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,SingleBrand_DropCount,MixedBrand_DropCount)
		SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode
			,ISNULL(EI_S.FieldValue,0) AS SingleBrand_DropCount
			,ISNULL(EI_M.FieldValue,0) AS MixedBrand_DropCount
		FROM @STATEMENTS ST
			INNER JOIN RPWeb_ML_ELG_Programs ELG
			ON ST.StatementCode=ELG.StatementCode
			LEFT JOIN RPWeb_User_EI_FieldValues EI_S
			ON EI_S.StatementCode=ELG.StatementCode AND EI_S.MarketLetterCode=ELG.MarketLetterCode AND EI_S.ProgramCode=ELG.ProgramCode AND EI_S.FieldCode='Single_Brand'
			LEFT JOIN RPWeb_User_EI_FieldValues EI_M
			ON EI_M.StatementCode=ELG.StatementCode AND EI_M.MarketLetterCode=ELG.MarketLetterCode AND EI_M.ProgramCode=ELG.ProgramCode AND EI_M.FieldCode='Mixed_Brand'
		WHERE ELG.ProgramCode=@PROGRAM_CODE

		UPDATE @TEMP SET SingleBrand_Reward = SingleBrand_DropCount * @SingleReward, MixedBrand_Reward = MixedBrand_DropCount * @MixedReward
		UPDATE @TEMP SET Reward = SingleBrand_Reward + MixedBrand_Reward

	END

	DELETE FROM RP2020_DT_Rewards_W_TruckloadReward WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_TruckloadReward(Statementcode,MarketLetterCode,ProgramCode,SingleBrand_DropCount,SingleBrand_Reward,MixedBrand_DropCount,MixedBrand_Reward,Reward)
	SELECT Statementcode,MarketLetterCode,ProgramCode,SingleBrand_DropCount,SingleBrand_Reward,MixedBrand_DropCount,MixedBrand_Reward,Reward
	FROM @TEMP

END


