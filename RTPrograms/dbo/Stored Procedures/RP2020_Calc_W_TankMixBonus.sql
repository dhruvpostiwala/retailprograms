﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_TankMixBonus]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	--RetailerCode varchar(20) NOT NULL,
	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,		
		FarmCode varchar(20) NOT NULL,
		Liberty_Acres Float NOT NULL,
		Centurion_Acres Float NOT NULL,		
		Facet_L_Acres Float NOT NULL,
		Lib_Cent_Matched_Acres Float NOT NULL DEFAULT 0,
		Lib_Facet_Matched_Acres Float NOT NULL DEFAULT 0,			
		Lib_Cent_Reward Money NOT NULL DEFAULT 0,
		Lib_Facet_Reward Money NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[FarmCode] ASC
		)
	)

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
		BEGIN
			INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,FarmCode,Liberty_Acres,Centurion_Acres,Facet_L_Acres)
			SELECT ST.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode, 'FXXXXX' AS FarmCode
				,SUM(IIF(tx.ChemicalGroup='LIBERTY 150', Acres, 0)) AS Liberty_Acres
				,SUM(IIF(tx.ChemicalGroup='CENTURION', Acres, 0)) AS Centurion_Acres
				,SUM(IIF(tx.ChemicalGroup='FACET L', Acres, 0)) AS Facet_L_Acres
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_ML_ELG_Programs MLP		ON MLP.StatementCode=ST.StatementCode 
				INNER JOIN RP_DT_ScenarioTransactions TX	ON TX.StatementCode=ST.StatementCode
			WHERE MLP.ProgramCode=@PROGRAM_CODE
				AND tx.ChemicalGroup IN ('LIBERTY 150','CENTURION','FACET L')	
			GROUP BY ST.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode
		END

	ELSE IF @STATEMENT_TYPE='ACTUAL'
		BEGIN
			INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,FarmCode,Liberty_Acres,Centurion_Acres,Facet_L_Acres)
			SELECT ST.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode, tx.FarmCode AS FarmCode
				,SUM(IIF(tx.ChemicalGroup='LIBERTY 150', Acres, 0)) AS Liberty_Acres
				,SUM(IIF(tx.ChemicalGroup='CENTURION', Acres, 0)) AS Centurion_Acres
				,SUM(IIF(tx.ChemicalGroup='FACET L', Acres, 0)) AS Facet_L_Acres
			FROM @STATEMENTS ST				
				INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode 
				INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
			WHERE MLP.ProgramCode=@PROGRAM_CODE
				AND tx.BASFSeason=@SEASON
				AND tx.InRadius=1				
				AND tx.ChemicalGroup IN ('LIBERTY 150','CENTURION','FACET L')	
				AND tx.FarmCode <> 'Adjustment'
			GROUP BY ST.StatementCode, MLP.MarketLetterCode, MLP.ProgramCode, tx.FarmCode

			-- HEAD OFFICE BREAKDOWN			
			DELETE T1
			FROM RP_DT_HO_BreakDownSummary T1
				INNER JOIN @STATEMENTS ST
				ON ST.Statementcode=T1.Statementcode
			WHERE T1.ProgramCode=@PROGRAM_CODE 

			INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
			SELECT ST.StatementCode, MLP.MarketLettercode, MLP.ProgramCode, TX.RetailerCode, TX.FarmCode, SUM(TX.Price_SDP) AS Sales, 1 
			FROM @STATEMENTS ST				
				INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode 
				INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
			WHERE MLP.ProgramCode=@PROGRAM_CODE
				AND tx.BASFSeason=@SEASON
				AND tx.InRadius=1				
				AND tx.ChemicalGroup IN ('LIBERTY 150','CENTURION','FACET L')	
				AND tx.FarmCode <> 'Adjustment'
			GROUP BY ST.StatementCode, MLP.MarketLettercode, MLP.ProgramCode, TX.RetailerCode, TX.FarmCode

		END

	-- DO THE MATCHING
	UPDATE @TEMP
	SET Lib_Cent_Matched_Acres=IIF(Liberty_Acres <= Centurion_Acres, Liberty_Acres, Centurion_Acres)
	WHERE Liberty_Acres > 0 AND Centurion_Acres > 0

	UPDATE @TEMP
	SET Lib_Facet_Matched_Acres=IIF(Lib_Cent_Matched_Acres <= Facet_L_Acres, Lib_Cent_Matched_Acres, Facet_L_Acres)
	WHERE Lib_Cent_Matched_Acres > 0 AND Facet_L_Acres > 0

	-- ADJUST MATCHED ACRES
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET Lib_Cent_Matched_Acres=Lib_Cent_Matched_Acres * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Matched_Centurion'
		WHERE Lib_Cent_Matched_Acres > 0

		UPDATE T1
		SET Lib_Facet_Matched_Acres=Lib_Facet_Matched_Acres * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Matched_Facet_L'
		WHERE Lib_Facet_Matched_Acres > 0
	END
	
	UPDATE @TEMP
	SET Lib_Cent_Reward=(Lib_Cent_Matched_Acres/10) * 4.00
		,Lib_Facet_Reward=(Lib_Facet_Matched_Acres/10) * 2.00 
		,Reward=((Lib_Cent_Matched_Acres/10) * 4.00) + ((Lib_Facet_Matched_Acres/10) * 2.00) 
	WHERE Lib_Cent_Matched_Acres > 0


	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')	
	BEGIN
		UPDATE T1
		SET Lib_Cent_Reward=Lib_Cent_Reward * ISNULL(EI.FieldValue/100,1)
			,Lib_Facet_Reward=Lib_Facet_Reward * ISNULL(EI.FieldValue/100,1)
			,Reward=Reward * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	DELETE FROM RP2020_DT_Rewards_W_TankMixBonus WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_TankMixBonus(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Liberty_Acres,Centurion_Acres,Facet_L_Acres,Lib_Cent_Matched_Acres,Lib_Facet_Matched_Acres,Lib_Cent_Reward,Lib_Facet_Reward,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,'',FarmCode,Liberty_Acres,Centurion_Acres,Facet_L_Acres,Lib_Cent_Matched_Acres,Lib_Facet_Matched_Acres,Lib_Cent_Reward,Lib_Facet_Reward,Reward
	FROM @TEMP

END

