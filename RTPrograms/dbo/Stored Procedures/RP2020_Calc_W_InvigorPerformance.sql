﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_InvigorPerformance]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	-- PROGRAMCODE : RP2020_W_INV_PERFORMANCE
		
	/*
		2020 InVigor POG Plan /  Manual Input InVigor Target @ SDP X Corresponding InVigor % in table above
	*/
	DECLARE @WEST_IND VARCHAR(10) = 'D000001';
	DECLARE @WEST_COOP VARCHAR(10) = 'D0000117';
	DECLARE @WL_COMPANIES TABLE (DCODE VARCHAR(50));
	INSERT INTO @WL_COMPANIES VALUES ('D0000107'),('D0000137'),('D0000244')
	
	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		Level5Code  VARCHAR (10) NOT NULL DEFAULT '',
		RewardBrand_Sales Money NOT NULL,
		QualBrand_Qty Numeric(18,2) NOT NULL,
		Liberty_200_Sales MONEY NOT NULL DEFAULT 0,
		Sales_Target Money NOT NULL DEFAULT 0,
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- RRT-465: updated qualifying brands to be Invigor for BOTH market letters (not Liberty vs target for the CCP ML)
	-- RC-3495: updated to compare total number of bags instead of total Sales (sales target has been changed to past year Invigor Bags, not total sales)

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,QualBrand_Qty,Sales_Target)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
			,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
			,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',(tx.Acres_CY /10),0)) AS QualBrand_Qty  -- Converted using CY_acres / 10
			,MAX(ISNULL(EI.FieldValue,0)) AS Sales_Target
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX
			ON TX.StatementCode=ST.StatementCode
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=ST.StatementCode AND EI.MarketLetterCode=TX.MarketLetterCode AND EI.FieldCode='Sales_Target'
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
	ELSE
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,Liberty_200_Sales,QualBrand_Qty,Sales_Target)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
			,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
			,SUM(IIF(tx.GroupType='REWARD_BRANDS' AND PR.ChemicalGroup = 'LIBERTY 200' ,tx.Price_CY,0)) AS Liberty_200_Sales
			,SUM(IIF(tx.GroupType='REWARD_BRANDS' AND GroupLabel = 'INVIGOR',(tx.Acres_CY /10),0)) AS QualBrand_Qty  -- Converted using CY_acres / 10			
			,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Acres_LY1/10,0)) AS Sales_Target
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX	ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR				ON PR.ProductCode=TX.ProductCode						
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code

		
		--**New DEMAREY COPY OVER ELIGIBLE INVIGOR Quanity for CCP MARKETLETTER
		UPDATE T1 
		SET T1.QualBrand_Qty = T2.QualBrand_Qty
		FROM @TEMP T1
		INNER JOIN @TEMP T2 ON T1.StatementCode = T2.StatementCode
		WHERE T1.MarketLetterCode = 'ML2020_W_CCP' AND T2.MarketLetterCode = 'ML2020_W_INV'

		--Update the BASF InVigor Target (Bags) for west line companies
		-- RP 3145
		-- Nutrien
		UPDATE T1
		SET Sales_Target = 235157
		FROM @TEMP T1
		INNER JOIN RP_STATEMENTS RS
		ON T1.STATEMENTCODE = RS.STATEMENTCODE
		WHERE RS.RETAILERCODE = 'D520062427' AND T1.MarketLetterCode IN ('ML2020_W_CCP', 'ML2020_W_INV')

		--Cargill
		UPDATE T1
		SET Sales_Target = 78752
		FROM @TEMP T1
		INNER JOIN RP_STATEMENTS RS
		ON T1.STATEMENTCODE = RS.STATEMENTCODE
		WHERE RS.RETAILERCODE = 'D0000107' AND T1.MarketLetterCode IN ('ML2020_W_CCP', 'ML2020_W_INV')

		--Richardson
		UPDATE T1
		SET Sales_Target = 304859
		FROM @TEMP T1
		INNER JOIN RP_STATEMENTS RS
		ON T1.STATEMENTCODE = RS.STATEMENTCODE
		WHERE RS.RETAILERCODE = 'D0000137' AND T1.MarketLetterCode IN ('ML2020_W_CCP', 'ML2020_W_INV')

		--UFA
		UPDATE T1
		SET Sales_Target = 25219
		FROM @TEMP T1
		INNER JOIN RP_STATEMENTS RS
		ON T1.STATEMENTCODE = RS.STATEMENTCODE
		WHERE RS.RETAILERCODE = 'D0000244' AND T1.MarketLetterCode IN ('ML2020_W_CCP', 'ML2020_W_INV')

	/* IND
					INVIGOR MARKET LETTER		Canola Crop Protection Market Letter
	
	100% +				4.00%						4.00%   
	97.5 – 99.9%		3.50%						3.00%
	95 – 97.49%			3.00%						2.00%
	92.5 – 94.99%		2.00%						1.50%
	90 – 92.49%			1.50%						1.00%
	85 – 89.99%			1.00%						0.50%
	
	
	WEST LINE

	100%					5%
	97.5% - 99.9%			4.5%
	95 - 97.49%				4%
	92.5 - 94.99%			3%
	90 - 92.49%				2%
	95 - 89.99%				1%

	*/


	UPDATE @TEMP SET Sales_Target=0 WHERE Sales_Target < 0

	--Rounding Rule: 0.5% on Qualifying tiers		
	--IND ETC
	UPDATE @TEMP  SET [Reward_Percentage]=0.04	WHERE [Sales_Target]=0 AND [QualBrand_Qty] > 0 AND [Level5Code]=@WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=0.04	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 1 AND Level5Code = @WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.035,0.03)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.975 AND Level5Code = @WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.03,0.02)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.95 AND Level5Code = @WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.02,0.015)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.925 AND Level5Code = @WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.015,0.01)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.90 AND Level5Code = @WEST_IND
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.01,0.005)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.85 AND Level5Code = @WEST_IND

	--COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.0345	WHERE Sales_Target=0 AND QualBrand_Qty > 0 AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.0345	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 1  AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.0275  	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.975 AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.02  	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.95 AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.015  	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.925 AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.01  	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.90 AND Level5Code = @WEST_COOP
	UPDATE @TEMP  SET [Reward_Percentage]=0.005  	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.85 AND Level5Code = @WEST_COOP


	---
	--westline companies except nutrien
	UPDATE @TEMP  SET [Reward_Percentage]=0.05	WHERE [Sales_Target]=0 AND QualBrand_Qty > 0 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.05	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 1 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.045	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.975 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.04	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.95 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.03	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.925 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.02	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.90 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)
	UPDATE @TEMP  SET [Reward_Percentage]=0.01	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.85 AND Level5Code IN (SELECT DCODE FROM @WL_COMPANIES)

	UPDATE @Temp	SET [Reward]=[RewardBrand_Sales] * [Reward_Percentage] 	WHERE [RewardBrand_Sales] > 0
	
	
	--NUTRIEN ONLY
	--IND ETC Needs to get confirmation and possibly rewrite
	/*
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.06)		WHERE [Sales_Target]=0 AND QualBrand_Qty > 0 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.06)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 1 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.075,0.055)	WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.975 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.065,0.05)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.95 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.055,0.04)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.925 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.04,0.03)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.90 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.03,0.002)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.85 AND Level5Code = 'D520062427'
	*/

	--Rewards percentages updated for Nutrien
	--RP 3065

	--RP 3145 HARDCODE REWARD PERCENTAGE FOR INVIGOR OF NUTRIEN TO 8.5 (11/11/2020)
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.06)		WHERE [Sales_Target]=0 AND QualBrand_Qty > 0 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.06)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 1 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.05)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.975 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.05)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.95 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.05)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.925 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.05)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND ([QualBrand_Qty]/[Sales_Target])+@Roundup_Value >= 0.90 AND Level5Code = 'D520062427'
	UPDATE @TEMP  SET [Reward_Percentage]=IIF([MarketLetterCode]='ML2020_W_INV',0.085,0.0)		WHERE [Reward_Percentage]=0 AND [Sales_Target] > 0 AND Level5Code = 'D520062427'
	
	
	UPDATE @Temp	SET [Reward]=[RewardBrand_Sales] * [Reward_Percentage] 	WHERE [RewardBrand_Sales] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD	
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'		
		WHERE T1.Reward > 0
	END
	
	DELETE FROM RP2020_DT_Rewards_W_INV_PERF WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_INV_PERF(StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, RewardBrand_Qty, Sales_Target, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, QualBrand_Qty, Sales_Target, Reward_Percentage, Reward
	FROM @TEMP
	
END

