﻿




CREATE PROCEDURE [dbo].[RP2021_Calc_W_Cereal_SFO]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
		-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN
		
		
		DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		FarmCode VARCHAR(50) NOT NULL,
		[Segment1_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Segment3_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Matched_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,		
		[Reward_Per_Acre] [numeric](5, 2) DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC,
		[MarketLetterCode] ASC,
		[ProgramCode] ASC,
		[FarmCode] ASC
	)
	)

	DECLARE @MINIMUM_ACRES  INT = 300;  
	DECLARE @REWARD_PER_ACRE  FLOAT = 0.75;
	
	INSERT INTO @TEMP(StatementCode, MarketLetterCode,ProgramCode,FarmCode,Segment1_Acres,Segment3_Acres)
	SELECT ST.StatementCode,GR.MarketLetterCOde, GR.ProgramCode,tx.FarmCode
			,SUM(IIF(GR.GroupLabel = 'SEGMENT 1' ,tx.Acres,0)) AS Segment1_Acres  
			,SUM(IIF(GR.GroupLabel = 'SEGMENT 3',tx.Acres,0)) AS Segment3_Acres
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode = ST.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup = tx.ChemicalGroup
			INNER JOIN RP_Config_Groups GR			ON GR.GroupID = BR.GroupID 	
		WHERE GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'REWARD_BRANDS'
			AND tx.InRadius = 1 AND BASFSeason = @SEASON
			AND TX.FarmCode NOT IN ('F520020402','F520040993')
	GROUP BY ST.StatementCode,GR.MarketLetterCOde, GR.ProgramCode,tx.FarmCode

	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales,FarmLevelSplit)
	SELECT ST.StatementCode,GR.MarketLetterCode, GR.ProgramCode ,tx.RetailerCode ,tx.FarmCode
		,SUM(CASE 
				WHEN S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244') THEN tx.Price_SWP
				WHEN S.Level5Code='D0000117' THEN tx.Price_SRP 
				ELSE tx.Price_SDP 
				END
		) AS Sales 	
		,1 AS FarmLevelSplit
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S				ON S.Statementcode=ST.StatementCode 
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode
		INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup = tx.ChemicalGroup
		INNER JOIN RP_Config_Groups GR			ON GR.GroupID = BR.GroupID 	
	WHERE S.StatementLevel IN ('LEVEL 2','LEVEL 5') AND tx.InRadius = 1 AND BASFSeason = @SEASON
		AND GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'REWARD_BRANDS'
		AND GR.GroupLabel IN ('SEGMENT 1' ,'SEGMENT 3')		
		AND TX.FarmCode NOT IN ('F520020402','F520040993')
	GROUP BY ST.StatementCode,GR.MarketLetterCode, GR.ProgramCode ,tx.RetailerCode ,tx.FarmCode
	-- END OF HEAD OFFICE BREAK DOWN


	UPDATE @TEMP SET [Matched_Acres] = IIF(Segment1_Acres <  Segment3_Acres, Segment1_Acres,Segment3_Acres) 
	WHERE Segment1_Acres >= @MINIMUM_ACRES AND Segment3_Acres >= @MINIMUM_ACRES


	UPDATE @TEMP
	SET Reward_Per_Acre = @REWARD_PER_ACRE
	WHERE [Matched_Acres] > 0
	

	UPDATE @TEMP SET Reward = Matched_Acres *  Reward_Per_Acre WHERE Matched_Acres > 0
	
	
	DELETE FROM [dbo].[RP2021_DT_Rewards_W_Cereal_SFO] WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO [dbo].[RP2021_DT_Rewards_W_Cereal_SFO]([StatementCode],  [MarketLetterCode], [ProgramCode], [FarmCode], [Segment1_Acres],[Segment3_Acres],[Matched_Acres], [Reward_Per_Acre], [Reward])
	SELECT StatementCode,[MarketLetterCode], [ProgramCode], [FarmCode],[Segment1_Acres],[Segment3_Acres],[Matched_Acres], Reward_Per_Acre,[Reward]
	FROM @TEMP

	
END


