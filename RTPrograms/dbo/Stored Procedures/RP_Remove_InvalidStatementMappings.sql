﻿

CREATE PROCEDURE [dbo].[RP_Remove_InvalidStatementMappings] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	-- REMOVE INACTIVE RETAILER CODES FROM MAPPINGS IF THEY HAVE NEITHER TRANSACTIONS AND LEADS FROM CURRENT SEASON
	DROP TABLE IF EXISTS #Inactive_Retailers

--	DELETE MAP
	SELECT MAP.StatementCode, MAP.RetailerCode
	INTO #Inactive_Retailers
	FROM RP_StatementsMappings MAP
		INNER JOIN @STATEMENTS ST			ON ST.Statementcode=MAP.Statementcode		
		INNER JOIN RetailerProfile RP		ON RP.RetailerCode=MAP.RetailerCode
		LEFT JOIN (
			SELECT RetailerCode 
			FROM RP_DT_Transactions 
			WHERE BASFSeason=@SEASON			
			GROUP BY RetailerCode
		) TX
		ON TX.RetailerCode=MAP.RetailerCode
		LEFT JOIN (
			SELECT Retailercode
			FROM RP_DT_Leads
			WHERE Season=@SEASON
			GROUP BY RetailerCode
		) L
		ON L.RetailerCode=MAP.RetailerCode
	WHERE RP.[Status] <> 'Active'  AND TX.RetailerCode IS NULL AND L.RetailerCode IS NULL 

	DELETE T1
	FROM #Inactive_Retailers T1
		INNER JOIN RP_Statements ST
		ON ST.StatementCode=T1.StatementCode
	WHERE ST.RetailerCode=T1.RetailerCode
	   
	DELETE MAP
	FROM RP_StatementsMappings MAP
		INNER JOIN #Inactive_Retailers RET
		ON RET.Statementcode=MAP.Statementcode AND RET.Retailercode=MAP.RetailerCode

	DELETE TX
	FROM RP_DT_Transactions TX
		INNER JOIN #Inactive_Retailers RET
		ON RET.Statementcode=TX.Statementcode AND RET.Retailercode=TX.RetailerCode

END



