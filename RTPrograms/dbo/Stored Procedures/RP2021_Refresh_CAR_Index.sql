﻿


CREATE PROCEDURE [dbo].[RP2021_Refresh_CAR_Index]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @PROG_CODE_GROWTH_BONUS VARCHAR(50)='RP2021_W_GROWTH_BONUS'
	DECLARE @PROG_CODE_LOYALTY_BONUS VARCHAR(50)='RP2021_W_LOYALTY_BONUS'
	DECLARE @PROG_CODE_RET_BONUS_PAYMENT VARCHAR(50)='RP2021_W_RETAIL_BONUS_PAYMENT'

	
	DECLARE @ENV AS VARCHAR(50) = db_name()
	
	--Get the current environment
	/*
		IF RIGHT(@ENV,3) NOT IN ('DEV') 
			RETURN
	*/

			 
	--  ************* RC (WEST INDEPENDENTS) AND COOPS **************
	DROP TABLE IF EXISTS #RC_N_COOP_STATEMENTS
	SELECT ST.StatementCode
	INTO #RC_N_COOP_STATEMENTS
	FROM @STATEMENTS T1
		INNER JOIN RP_STATEMENTS  ST			ON ST.Statementcode=T1.StatementCode
		INNER JOIN RP_DT_ML_ElG_Programs MLP	ON MLP.StatementCode=ST.Statementcode
	WHERE ST.SEASON=@SEASON AND ST.Level5Code IN ('D000001','D0000117')
		AND (
			MLP.ProgramCode IN (@PROG_CODE_GROWTH_BONUS,@PROG_CODE_LOYALTY_BONUS) 
				OR 
			(ST.Level5Code='D0000117' AND  MLP.ProgramCode=@PROG_CODE_RET_BONUS_PAYMENT)
		)
	GROUP BY ST.StatementCode

	IF EXISTS(SELECT TOP 1 1 FROM #RC_N_COOP_STATEMENTS)
	BEGIN				
		DROP TABLE IF EXISTS #RetailerLevel_Index

		SELECT RP.RetailerCode, PCG.ProductCode
			,IIF(T1.YOY_Index < 0.95, T1.YOY_Index, 1) AS YOY_Index
		INTO #RetailerLevel_Index
		FROM [dbo].[TVF_YOY_Index_CAR_Level](@SEASON) T1
			INNER JOIN rp.CARMappingView  T2	ON T2.CARID=T1.CARID
			INNER JOIN RetailerProfile RP		ON RP.RetailerCode=T2.RetailerCode
			INNER JOIN rp.ProductCropGroup PCG	ON PCG.CropGroupID=T1.CropGroupID 
		WHERE PCG.Season=@SEASON -- AND RP.RetailerType='Level 1'

		UPDATE TX
		SET [YOY_Index] = RI.YOY_Index
		FROM RP2021_DT_Sales_Consolidated TX
			INNER JOIN #RC_N_COOP_STATEMENTS ST		ON ST.StatementCode=TX.StatementCode
			INNER JOIN #RetailerLevel_Index RI		ON RI.RetailerCode=TX.RetailerCode AND RI.ProductCode=TX.ProductCode
		WHERE (
				TX.ProgramCode IN (@PROG_CODE_GROWTH_BONUS,@PROG_CODE_LOYALTY_BONUS) 
					OR 
				(tx.Level5Code='D0000117' AND  tx.ProgramCode=@PROG_CODE_RET_BONUS_PAYMENT) 
			)
	END

	/************************** LINE COMPANIES: CARGIL, RICHARDSON, NUTRIEN *********************************************************/
	DROP TABLE IF EXISTS #LineCompanies_Statements
	SELECT ST.StatementCode
	INTO #LineCompanies_Statements
	FROM @STATEMENTS T1
		INNER JOIN RP_STATEMENTS  ST			ON ST.Statementcode=T1.StatementCode		
	WHERE ST.SEASON=@SEASON AND ST.Level5Code IN ('D0000107','D0000137','D520062427')
	GROUP BY ST.StatementCode
	
	IF EXISTS(SELECT TOP 1 1 FROM #LineCompanies_Statements)
	BEGIN		
		DROP TABLE IF EXISTS #WestRegion_Index

		SELECT PCG.ProductCode
			,IIF(T1.YOY_Index < 0.95,T1.YOY_Index, 1) AS YOY_Index
		INTO #WestRegion_Index
		FROM [dbo].[TVF_YOY_Index_Consolidated](@SEASON, 'AB,MB,SK,BC') T1		
			INNER JOIN rp.ProductCropGroup PCG	ON PCG.CropGroupID=T1.CropGroupID 
		WHERE PCG.Season=@SEASON

		UPDATE TX
		SET [YOY_Index] = RI.YOY_Index
		FROM RP2021_DT_Sales_Consolidated TX
			INNER JOIN #LineCompanies_Statements ST		ON ST.StatementCode=TX.StatementCode
			INNER JOIN #WestRegion_Index RI				ON RI.ProductCode=TX.ProductCode
		WHERE TX.ProgramCode IN (@PROG_CODE_GROWTH_BONUS,@PROG_CODE_LOYALTY_BONUS) 
	END

	/***************************** UNITED FARMERS OF ALBERTA (UFA) ******************************************************/
	DROP TABLE IF EXISTS #UFA_Statements

	SELECT ST.StatementCode
	INTO #UFA_Statements
	FROM @STATEMENTS T1
		INNER JOIN RP_STATEMENTS  ST			ON ST.Statementcode=T1.StatementCode	
	WHERE ST.SEASON=@SEASON AND ST.Level5Code = 'D0000244'
	GROUP BY ST.StatementCode
		
	IF EXISTS(SELECT TOP 1 1 FROM #UFA_Statements)
	BEGIN		
		DROP TABLE IF EXISTS #Alberta_Index

		SELECT PCG.ProductCode
			,IIF(T1.YOY_Index < 0.95,T1.YOY_Index,1) AS YOY_Index
		INTO #Alberta_Index
		FROM [dbo].[TVF_YOY_Index_Consolidated](@SEASON, 'AB') T1			
			INNER JOIN rp.ProductCropGroup PCG	ON PCG.CropGroupID=T1.CropGroupID 
		WHERE PCG.Season=@SEASON

		UPDATE TX
		SET [YOY_Index] = RI.YOY_Index
		FROM RP2021_DT_Sales_Consolidated TX
			INNER JOIN #UFA_Statements ST		ON ST.StatementCode=TX.StatementCode
			INNER JOIN #Alberta_Index RI		ON RI.ProductCode=TX.ProductCode
		WHERE TX.ProgramCode IN (@PROG_CODE_GROWTH_BONUS,@PROG_CODE_LOYALTY_BONUS) 
	END

END
