﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_HeatUpHarvest] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CPP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'

	DECLARE @HeatTransStartDate Date = '2021-07-01'
	DECLARE @PricePerAcre MONEY = 0.50
	
	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[MarketLetterCode] VARCHAR(50) NOT NULL,
		[ProgramCode] VARCHAR(50) NOT NULL,
		[FarmCode] VARCHAR(100) NOT NULL,
		[Heat_Acres] MONEY NOT NULL DEFAULT 0,
		[Invigor_Acres] MONEY NOT NULL DEFAULT 0,
		[Matched_Acres] MONEY NOT NULL DEFAULT 0,
		[RewardPerAcre] MONEY NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[FarmCode] ASC
		)
	)


	DROP TABLE IF EXISTS #ProductReference
	SELECT ProductCode, ChemicalGroup, 'All' as Level5Code
	INTO #ProductReference
	FROM ProductReference
	WHERE Product IN ('HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)','INVIGOR L340PC','INVIGOR CHOICE LR344PC','INVIGOR L345PC','INVIGOR L357P','INVIGOR L233P','INVIGOR L234PC','INVIGOR L255PC','INVIGOR L258HPC')

		UNION

	-- https: //kennahome.jira.com/browse/RP-4104
	--D520062427 -- NUTRIEN -- Please adjust code so that Heat (Pre-Seed) and Heat (Pre-Harvest) are both included for Nutrien only.
	SELECT ProductCode, ChemicalGroup, 'D520062427' AS Level5Code
	FROM ProductReference
	WHERE ChemicalGroup = 'Heat' AND Product IN ('HEAT (PRE-HARVEST)','HEAT (PRE-SEED)')


	DROP TABLE IF EXISTS #Temp_Transactions

	SELECT ST.StatementCode, @CPP_ML_CODE AS MarketLetterCode, @PROGRAM_CODE AS ProgramCode
		,ISNULL(T.FarmCode,'') AS FarmCode
		,SUM(IIF(PR.ChemicalGroup IN ('HEAT LQ','HEAT') AND CAST(T.InvoiceDate AS Date) >= @HeatTransStartDate, T.Acres, 0)) AS Heat_Acres
		,SUM(IIF(PR.ChemicalGroup NOT IN ('HEAT LQ','HEAT'), T.Acres, 0)) AS Invigor_Acres
	INTO #Temp_Transactions
	FROM @Statements ST		
		INNER JOIN RP_Statements S			ON S.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Transactions T 	ON T.Statementcode=ST.Statementcode	
		INNER JOIN #ProductReference PR		ON T.ProductCode=PR.ProductCode
	WHERE T.BASFSeason=@SEASON AND T.InRadius=1 AND T.Farmcode NOT IN ('F520020402','F520040993')
		AND (PR.Level5Code='All' OR PR.Level5Code=S.Level5Code)
	GROUP BY ST.StatementCode, T.FarmCode
	
	   
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,FarmCode,Heat_Acres,Invigor_Acres, Matched_Acres)
	SELECT StatementCode, MarketLetterCode, ProgramCode, FarmCode, Heat_Acres,Invigor_Acres	
		,IIF(Heat_Acres <= Invigor_Acres, Heat_Acres, Invigor_Acres) AS Matched_Acres
	FROM #Temp_Transactions TX
	
	UPDATE T
	  SET RewardPerAcre = @PricePerAcre,  
	      Reward = IIF(Matched_Acres <= 0, 0, Matched_Acres* @PricePerAcre)
	FROM @TEMP T
		

	-- Copy records from temporary table into permanent table
	DELETE FROM RP2021_DT_Rewards_W_HeatUpHarvest WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_HeatUpHarvest([Statementcode], [MarketLetterCode], [ProgramCode], [FarmCode], [Heat_Acres], [Invigor_Acres], [Matched_Acres], [RewardPerAcre], [Reward])
	SELECT StatementCode, MarketLetterCode, ProgramCode, FarmCode, Heat_Acres, Invigor_Acres, Matched_Acres, RewardPerAcre, Reward
	FROM @TEMP

END

