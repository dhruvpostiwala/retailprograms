﻿

CREATE PROCEDURE [dbo].[RP2021_Exception_W_Program_Data] @SEASON INT,@STATEMENTCODE INT, @REWARDCODE VARCHAR(50)
AS
BEGIN
		SET NOCOUNT ON;
		
		DECLARE @LEVEL5CODE VARCHAR(20)

		DECLARE @TEMP TABLE (
			StatementCode INT NOT NULL,
			ProgramCode varchar(100) NOT NULL,
			RewardCode varchar(50) NOT NULL,
			MarketLetterCode varchar(50) NOT NULL,
			cy_sales_A MONEY NOT NULL DEFAULT 0,
			cy_sales_B MONEY NOT NULL DEFAULT 0,
			current_margin_A DECIMAL(6,4) NOT NULL DEFAULT 0,
			current_margin_B DECIMAL(6,4) NOT NULL DEFAULT 0,
			current_reward_A MONEY NOT NULL DEFAULT 0,
			current_reward_B MONEY NOT NULL DEFAULT 0,
			next_margin_A DECIMAL(10,4) NOT NULL DEFAULT 0,
			next_margin_B DECIMAL(10,4) NOT NULL DEFAULT 0,
			reward_margins_a nvarchar(255) NOT NULL DEFAULT '[]',
			reward_margins_b nvarchar(255) NOT NULL DEFAULT '[]',
			exception_type varchar(50) NULL ,
			PRIMARY KEY CLUSTERED (
				[StatementCode] ASC,
				[ProgramCode]  ASC,
				[RewardCode]  ASC,
				[MarketLetterCode] ASC
			)
		)

		DECLARE @PROGRAM_CODE VARCHAR(100)
		SELECT @PROGRAM_CODE = ProgramCode FROM RP_Config_Programs WHERE RewardCode=@REWARDCODE AND SEASON=@SEASON
	

		SELECT @LEVEL5CODE = Level5Code FROM [RPWeb_Statements] WHERE StatementCode = @StatementCode

		INSERT into @TEMP(StatementCode,ProgramCode,RewardCode,MarketLetterCode)
		SELECT DISTINCT E.StatementCode,E.ProgramCode,R.RewardCode,E.MarketLetterCode
		FROM [RP_Config_Rewards_Summary] R 
				INNER JOIN RP_Config_programs P 				ON P.RewardCode = R.RewardCode 
				INNER JOIN RPWeb_ML_ELG_Programs E 				ON E.ProgramCode = P.ProgramCode 
		WHERE E.StatementCode = @STATEMENTCODE
			AND R.RewardCode = @REWARDCODE
		
		/*
			AND R.RewardCode NOT IN ('ADV_W_CTRL_W','CEREAL_S_F_W','','CLL_W','CUSTOM_AA_W','DITP_W','FUNG_W'
								 ,'INVENTORY_MGMT_W','NOD_DUO_SCG_W','POT_W','PSB_W','RET_BON_PAY_W','TM_BONUS_W','WH_PAYMENT_W'
								 ,'SEG_SELL_W','INV_INB_W'
								 )
		*/


		--SupplySales only using on CCP MarketLetter
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2021_W_INV' AND RewardCode = 'SUPPLY_SALES_W'
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2021_W_CPP' AND RewardCode = 'INV_LLB_W'
		
		UPDATE @TEMP SET exception_type = 'TIER' WHERE RewardCode IN ('BSS_W','CST_W','EBR_W','GROWTH_W','INV_PER_W','LOYALTY_W')
		UPDATE @TEMP SET exception_type = 'NON-TIER' WHERE exception_type IS NULL
		
		
		
		--Non tier values
		--InVigor and Liberty Loyalty
		IF @REWARDCODE = 'INV_LLB_W'
		BEGIN
			UPDATE T1
			SET cy_sales_A = T2.[cy_sales_A]				
				,next_margin_A = 0.02 -- INVIGOR
				,cy_sales_B = T2.[cy_sales_B]				
				,next_margin_B = 0.05 --LIBERTY
			FROM @TEMP T1
				INNER JOIN (
					SELECT StatementCode, ProgramCode
						,MAX(IIF(MarketLetterCode = 'ML2021_W_INV' , RewardBrand_Sales, 0)) AS [cy_sales_A]						
						,MAX(IIF(MarketLetterCode = 'ML2021_W_CPP' , RewardBrand_Sales, 0)) AS [cy_sales_B]						
					FROM [dbo].[RP2021Web_Rewards_W_INV_LLB] 
					WHERE StatementCode=@StatementCode 
						AND Bonus_Percentage = 0
						AND RewardBrand_Sales > 0
					GROUP BY StatementCode, ProgramCode
				) T2
				ON T2.StatementCode = T1.StatementCode and T2.ProgramCode = T1.ProgramCode

			GOTO FINAL
		END
		
		--Logistics Support Reward
		IF @REWARDCODE='LOG_SUP_W'
		BEGIN
			UPDATE T1
			SET cy_sales_A = T2.cy_sales_a				
				,next_margin_A = 0.01
			FROM @TEMP T1
				INNER JOIN (
					SELECT StatementCode, ProgramCode, SUM(Segment_Reward_Sales) AS cy_sales_a
					FROM [RP2021Web_Rewards_W_LogisticsSupport]
					WHERE StatementCode=@StatementCode AND Segment_Reward_Percentage=0 
					GROUP BY StatementCode, ProgramCode
				) T2
				ON T2.StatementCode = T1.StatementCode and T2.ProgramCode = T1.ProgramCode
			
			GOTO FINAL
		END
		
		UPDATE T1
		SET T1.cy_sales_A = NTIER.CYSales
			,next_margin_A = NTIER.next_margin_A
		FROM @TEMP T1
			INNER JOIN (										
				--Segment Selling Bonus
				SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.02 AS next_margin_A
				FROM [dbo].[RP2021Web_Rewards_W_SegmentSelling]
				WHERE StatementCode=@StatementCode
				GROUP BY StatementCode, ProgramCode
				
				UNION ALL
				
				--InVigor Innovation Bonus
				SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.02 AS next_margin_A
				FROM [dbo].[RP2021Web_Rewards_W_INV_INNOV]
				WHERE StatementCode=@StatementCode
				GROUP BY StatementCode, ProgramCode
			) NTIER	
			ON NTIER.StatementCode = T1.StatementCode and T1.ProgramCode = NTIER.ProgramCode
		WHERE T1.exception_type = 'NON-TIER'

		--tier values
		UPDATE T1
			SET T1.cy_sales_A = TIER.CYSales_A,
				T1.cy_sales_B = TIER.CYSales_B,
				T1.current_margin_A = TIER.CurrentMargin_A,
				T1.current_margin_B = TIER.CurrentMargin_B,
				T1.current_reward_A = TIER.CurrentReward_A,
				T1.current_reward_B = TIER.CurrentReward_B,
				T1.next_margin_A = TIER.next_margin_A,
				T1.next_margin_B = TIER.next_margin_B,
				T1.reward_margins_a = TIER.reward_margins_a,
				T1.reward_margins_b = TIER.reward_margins_b
		FROM @TEMP T1
		INNER JOIN (
			SELECT bss.StatementCode
				,SUM(bss.RewardBrand_Sales) AS 'CYSales_A'
				,0 AS 'CYSales_B'
				,MAX(bss.Reward_Percentage) AS 'CurrentMargin_A'
				,0 AS 'CurrentMargin_B'
				,SUM(bss.Reward) AS 'CurrentReward_A'
				,0 AS 'CurrentReward_B'
				,MAX(bss.ProgramCode) ProgramCode

				,CASE 
					WHEN @LEVEL5CODE IN ('D000001','D0000117') THEN '[0.03,0.04,0.045]'
					WHEN @LEVEL5CODE = 'D0000137' THEN '[0.02,0.03,0.04,0.045]'  					
					WHEN @LEVEL5CODE IN ('D0000107','D0000244') THEN '[0.01,0.02,0.03,0.035]' 										
					WHEN @LEVEL5CODE='D520062427' THEN '[0.005,0.01]' 
					ELSE '[]'
				END AS [Reward_Margins_A]
				,'[]' [Reward_Margins_B]							   
				,MAX(CASE WHEN @LEVEL5CODE IN ('D000001','D0000117') THEN
												CASE
													WHEN bss.Reward_Percentage = 0.045 THEN 0
													WHEN bss.Reward_Percentage = 0.04 THEN 0.045 
													WHEN bss.Reward_Percentage = 0.03 THEN 0.04  																																							
												 ELSE 0.03  
											    END
											 WHEN @LEVEL5CODE = 'D0000137' THEN 
											    CASE 
													WHEN bss.Reward_Percentage = 0.045 THEN 0
													WHEN bss.Reward_Percentage = 0.04 THEN 0.045
													WHEN bss.Reward_Percentage = 0.03 THEN 0.04 
													WHEN bss.Reward_Percentage = 0.02 THEN 0.03 													
												 ELSE 0.02  
											    END
											 WHEN @LEVEL5CODE IN ('D0000107','D0000244') THEN 
											    CASE
													WHEN bss.Reward_Percentage = 0.035 THEN 0
													WHEN bss.Reward_Percentage = 0.03 THEN 0.035 
													WHEN bss.Reward_Percentage = 0.02 THEN 0.03 
													WHEN bss.Reward_Percentage = 0.01 THEN 0.02 
												 ELSE 0.01
											    END											 
											 WHEN @LEVEL5CODE='D520062427' THEN 
											    CASE 													
													WHEN bss.Reward_Percentage = 0.01 THEN 0
													WHEN bss.Reward_Percentage = 0.005 THEN 0.01 
												 ELSE 0.005
											   END
											 ELSE 0
										     END
				) AS [next_margin_A]
				,0 AS [next_margin_B]
			FROM [dbo].[RP2021Web_Rewards_W_BrandSpecificSupport] bss
			WHERE bss.StatementCode=@Statementcode
			GROUP BY bss.StatementCode

			UNION ALL

			SELECT cs.StatementCode
				,SUM(cs.Planned_POG_Sales) AS 'CYSales_A'
				,0 AS 'CYSales_B'
				,MAX(cs.RewardBrand_Percentage) AS 'CurrentMargin_A'
				,0 AS 'CurrentMargin_B'
				,SUM(cs.Reward) AS 'CurrentReward_A'
				,0 AS 'CurrentReward_B'
				,MAX(cs.ProgramCode) ProgramCode
				,CASE 
					WHEN @LEVEL5CODE IN ('D000001','D0000117') THEN '[0.03,0.04,0.05]'
					WHEN @LEVEL5CODE IN ('D0000107','D0000137','D0000244','D520062427') THEN '[0.16,0.20,0.24]'
					ELSE '[]'
				END AS [Reward_Margins_A]
				,'[]' [Reward_Margins_B]
				,MAX(CASE WHEN @LEVEL5CODE IN ('D000001','D0000117') THEN
												CASE
													WHEN cs.RewardBrand_Percentage = 0.045 THEN 0
													WHEN cs.RewardBrand_Percentage = 0.04 THEN 0.045 
													WHEN cs.RewardBrand_Percentage = 0.03 THEN 0.04  												 
													ELSE 0.03 
											    END
											 WHEN @LEVEL5CODE IN ('D0000107','D0000137','D0000244','D520062427') THEN 
											    CASE
													WHEN cs.RewardBrand_Percentage = 0.24 THEN 0
													WHEN cs.RewardBrand_Percentage = 0.20 THEN 0.24 
													WHEN cs.RewardBrand_Percentage = 0.16 THEN 0.20 													
												 ELSE 0.16  
											    END
											 ELSE 0
										     END) AS [next_margin_A],[next_margin_A]=0
		    FROM [dbo].[RP2021Web_Rewards_W_CustomSeed] cs
			WHERE cs.Statementcode=@StatementCode
			GROUP BY cs.StatementCode

			UNION ALL 

			SELECT eb.StatementCode
				,SUM(eb.Support_Reward_Sales) as 'CYSales_A'
				,0 AS 'CYSales_B'
				,MAX(eb.Support_Reward_Percentage) AS 'CurrentMargin_A'
				,0 AS 'CurrentMargin_B'
				,SUM(eb.Reward) as 'CurrentReward_A'
				,0 AS 'CurrentReward_B'
				,MAX(eb.ProgramCode) ProgramCode
				,CASE 
					WHEN @LEVEL5CODE='D000001' THEN '[0.01,0.0175,0.025]'
					ELSE '[]'
					END  AS [Reward_Margins_A]
				,'[]' [Reward_Margins_B]
				,MAX(IIF(@LEVEL5CODE='D000001',CASE 
							WHEN eb.Support_Reward_Percentage = 0.0250 THEN 0
							WHEN eb.Support_Reward_Percentage = 0.0175  THEN 0.025 
							WHEN eb.Support_Reward_Percentage = 0.01  THEN 0.0175							
							ELSE 0.01
		 				END,0)) AS [next_margin_A],[next_margin_B]=0
		    FROM [dbo].[RP2021Web_Rewards_W_EFF_BONUS] eb
			WHERE eb.Statementcode=@statementcode
			GROUP BY eb.StatementCode

			UNION ALL

			SELECT gb.StatementCode
				,SUM(gb.QualSales_CY) as 'CYSales_A'
				,0 AS 'CYSales_B'
				,MAX(gb.Reward_Percentage) AS 'CurrentMargin_A'
				,0 AS 'CurrentMargin_B'
				,SUM(gb.Reward) AS 'CurrentReward_A'
				,0 AS 'CurrentReward_B'
				,MAX(gb.ProgramCode) ProgramCode
				,CASE 
					WHEN  @LEVEL5CODE IN ('D000001','D0000137','D0000244','D0000107','D520062427') THEN '[0.03,0.05,0.0575,0.0625]'
					WHEN @LEVEL5CODE = 'D0000117' THEN '[0.01,0.02,0.0375,0.045]'
					ELSE '[]'

				END AS [Reward_Margins_A]
				,'[]' [Reward_Margins_B]
				,MAX(
					CASE WHEN @LEVEL5CODE IN ('D000001','D0000137','D0000244','D0000107','D520062427') THEN
						CASE 
							WHEN gb.Reward_Percentage = 0.0625 THEN 0
							WHEN gb.Reward_Percentage = 0.0575 THEN 0.0625
							WHEN gb.Reward_Percentage = 0.05 THEN 0.0575
							WHEN gb.Reward_Percentage = 0.03 THEN 0.05  							  							  
							ELSE 0.03  
						 END
					WHEN @LEVEL5CODE = 'D0000117' THEN 
							CASE 
								WHEN gb.Reward_Percentage = 0.0375 THEN 0.045
								WHEN gb.Reward_Percentage = 0.02 THEN 0.0375 
								WHEN gb.Reward_Percentage = 0.01 THEN 0.02 
								ELSE 0.01  
							END
						ELSE 0
					END) AS [next_margin_A]
					,0 AS [next_margin_B]
		    FROM [dbo].[RP2021Web_Rewards_W_GrowthBonus] gb
			WHERE gb.StatementCode=@StatementCode
			GROUP BY gb.StatementCode

			UNION ALL

			SELECT ipr.StatementCode
				,SUM(ipr.RewardBrand_Sales) as 'CYSales_A'
				,0 AS 'CYSales_B'
				,MAX(ipr.Reward_Percentage) AS 'CurrentMargin_A'
				,0 AS 'CurrentMargin_B'
				,SUM(ipr.Reward) as 'CurrentReward_A'
				,0 AS 'CurrentReward_B'
				,MAX(ipr.ProgramCode) ProgramCode
				,CASE 
					WHEN @LEVEL5CODE = 'D000001' THEN '[0.01,0.015,0.02]'
					WHEN @LEVEL5CODE = 'D0000117' THEN '[0.0075,0.01,0.0145]'
					WHEN @LEVEL5CODE IN ('D0000137','D0000244','D0000107') THEN '[0.02,0.025,0.03]'
					WHEN @LEVEL5CODE = 'D520062427' THEN '[0.085]'
				ELSE '[]' END [Reward_Margins_A]
				,'[]' [Reward_Margins_B]	

				,MAX(CASE
					WHEN @LEVEL5CODE = 'D000001' THEN
						CASE 
							WHEN ipr.Reward_Percentage = 0.02 THEN 0
							 WHEN ipr.Reward_Percentage = 0.015 THEN 0.02
							 WHEN ipr.Reward_Percentage = 0.01 THEN 0.015  							 
							ELSE 0.01  
						 END
						
					WHEN @LEVEL5CODE = 'D0000117' THEN 
					    CASE WHEN ipr.Reward_Percentage = 0.0145 THEN 0
							 WHEN ipr.Reward_Percentage = 0.01 THEN 0.0145
							 WHEN ipr.Reward_Percentage = 0.0075 THEN 0.01 							 
							 ELSE 0.0075  
						END
						
					WHEN @LEVEL5CODE IN ('D0000137','D0000244','D0000107') THEN 
					    CASE 
							WHEN ipr.Reward_Percentage = 0.03 THEN 0
							 WHEN ipr.Reward_Percentage = 0.025 THEN 0.03
							 WHEN ipr.Reward_Percentage = 0.02 THEN 0.025 							 
							 ELSE 0.02  
						END
						
					WHEN @LEVEL5CODE = 'D520062427' THEN 
					    CASE WHEN ipr.[MarketLetterCode] = 'ML2021_W_CPP' AND ipr.Reward_Percentage = 0 THEN 0.085 ELSE 0  END
				
					ELSE 0 END) AS [next_margin_A]
					,0 AS [next_margin_B]
		    FROM [dbo].[RP2021Web_Rewards_W_INV_PERF] ipr
			WHERE ipr.StatementCode=@Statementcode
			GROUP BY ipr.StatementCode

			UNION ALL

			SELECT StatementCode
				,SUM(SegmentA_Reward_Sales) as 'CYSales_A'
				,SUM(SegmentB_Reward_Sales) AS 'CYSales_B'
				,MAX(SegmentA_Reward_Percentage) AS 'CurrentMargin_A'
				,MAX(SegmentB_Reward_Percentage) AS 'CurrentMargin_B'
				,SUM(SegmentA_Reward) as 'CurrentReward_A'
				,SUM(SegmentB_Reward) as 'CurrentReward_B'
				,MAX(ProgramCode) ProgramCode
				,MIN(IIF(SegmentA_Reward_Percentage > 0.0575 , '[0]', '[0,0.03,0.035,0.0575,0.0625]')) [Reward_Margins_A]
				,MIN(IIF(SegmentB_Reward_Percentage > 0.10, '[0]','[0,0.03,0.035,0.10,0.1225]')) [Reward_Margins_B]
				,MAX(CASE 
					WHEN SegmentA_Reward_Percentage > 0.0575  THEN 0
					WHEN SegmentA_Reward_Percentage = 0.0575  THEN 0.0625						
					WHEN SegmentA_Reward_Percentage = 0.035  THEN 0.0575	
					WHEN SegmentA_Reward_Percentage = 0.03  THEN 0.035	
					ELSE 0.03 
				END) AS [next_margin_A]
				,MAX(CASE 
					WHEN SegmentB_Reward_Percentage > 0.10  THEN 0
					WHEN SegmentB_Reward_Percentage = 0.10  THEN 0.1225	
					WHEN SegmentB_Reward_Percentage = 0.035  THEN 0.10	
					WHEN SegmentB_Reward_Percentage = 0.03  THEN 0.035	
					ELSE 0.03
				END) AS [next_margin_B]
		   FROM [dbo].[RP2021Web_Rewards_W_LoyaltyBonus]
		   WHERE StatementCode=@StatementCode
		   GROUP BY StatementCode
		) TIER	
		ON TIER.StatementCode = T1.StatementCode and T1.ProgramCode = TIER.ProgramCode
		WHERE T1.exception_type = 'TIER'

FINAL:		
		SELECT ISNULL(
			(
				SELECT exception_type ,RewardCode as reward_code
					,cy_sales_a,cy_sales_b					
					,current_reward_a,current_reward_b	
					,current_margin_a,current_margin_b
					,next_margin_a,next_margin_b									
					,JSON_QUERY(reward_margins_a) AS reward_margins_a
					,JSON_QUERY(reward_margins_b) as reward_margins_b
				FROM @TEMP 
				WHERE RewardCode = @REWARDCODE		
				FOR JSON PATH
			) 
		,'[]')[json_data];
END
