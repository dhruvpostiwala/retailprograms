﻿
CREATE PROCEDURE [dbo].[RP_RefreshLeads] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	
	 --FOR TESTING 
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 904
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'Actual'	

	--FETCH LEADS FROM MASTERLEADS AND RETAILERLEADS TABLE IN APPLICATION DEV AND ATTACH THEM TO APPROPRIATE STATEMENT CODE

	DELETE T1
	FROM RP_DT_Leads T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'
	
	INSERT INTO RP_DT_Leads(StatementCode,LeadID, Season, RetailerCode, FarmCode, ProductCode, VarietyCode, Acres, AppRate, BookType, GeneratingSource, CreatedDate,InRadius, RP_ID )
	SELECT ST.StatementCode,Leads.LeadID,Leads.Season,Leads.RetailerCode,Leads.FarmCode, Leads.ProductCode,Leads.VarietyCode, Leads.Acres, Leads.AppRate, Leads.BookType,Leads.GeneratingSource, Leads.CreatedDate
	   		,Leads.InRadius AS InRadius
			,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY Leads.RetailerCode, Leads.ProductCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT RL.RetailerCode, RL.ID 'LeadID', RL.GeneratingSource, RL.CreatedDate
				,ML.FarmCode,  ML.VarietyCode, ML.ProductCode, ML.Season, ML.BookType
				,COALESCE(RL.Acres,RL.MasterLeadAcres,ML.Acres,0) Acres		
				,COALESCE(RL.AppRate,ML.AppRate,0) AS AppRate
				,ISNULL(Grower.InRadius,0) AS InRadius
			FROM RetailerLeads RL
				INNER JOIN MasterLeads ML			ON RL.MasterLeadID = ML.ID 
				LEFT JOIN (
					SELECT FL.RetailerCode, FL.FarmCode, 1 AS InRadius
					FROM PER_RetailFarmList FL
						INNER JOIN PER_Relationships REL
						ON REL.PerID=FL.PerID
					WHERE REL.IsActive=1 AND REL.Season=@SEASON-1
				) Grower
				ON Grower.FarmCode=ML.FarmCode AND Grower.RetailerCode=RL.RetailerCode
			WHERE ML.Season=@SEASON  AND ML.CropPlanCode <> 'EDI'
					AND RL.[Status] <> 'Deleted'
				--	AND (RL.[Status] IN ('Ordered','Purchased')  OR (RL.[Status]='Open' AND ML.CropIntentionDeleted='No') )
		) Leads 
		ON Leads.RetailerCode = MAP.ActiveRetailerCode --MAP.RetailerCode 
	WHERE RS.VersionType <> 'USE CASE'

	/*
	UPDATE T1
	SET FinalAcres=0
	FROM RP_DT_Leads T1
		INNER JOIN @STATEMENTS ST		ON ST.StatementCode=T1.StatementCode
		LEFT JOIN (
			SELECT distinct gbpcc.CropCode, lgcc.ProductCode, lgcc.HortConversionRewards as [Conversion]
			FROM LinkedGrowerConnectCrops lgcc
				left join ProductReferenceLinkedDocuments prld  			
				on lgcc.LinkedDocCode = prld.LinkedDocCode
				left join GBPCropConfig gbpcc 			
				on gbpcc.CropCode = prld.SourceDocCode
			where prld.[ApplicationType]='Grower Connect' AND HortConversionRewards <> '-'
		)hort
		on hort.cropcode=T1.VarietyCode AND hort.productcode=T1.ProductCode		
	*/



END
