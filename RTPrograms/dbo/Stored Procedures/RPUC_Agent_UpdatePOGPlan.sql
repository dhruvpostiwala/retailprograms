﻿

CREATE     PROCEDURE [dbo].[RPUC_Agent_UpdatePOGPlan] @STATEMENTCODE INT, @PlanDateCreated datetime 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	DECLARE @Retailercode VARCHAR(20);
	DECLARE @BASFSeason INT;
	DECLARE @PlanStatus VARCHAR(20)='Final'
	
	BEGIN TRY

		SELECT @Retailercode=Retailercode, @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode
		IF MONTH(@PlanDateCreated) IN (10,11,12)
			SET @BASFSeason=YEAR(@PlanDateCreated)+1
		ELSE
			SET @BASFSeason=YEAR(@PlanDateCreated)


		DELETE RP_DT_Scenarios WHERE StatementCode = @STATEMENTCODE
		
		INSERT INTO RP_DT_Scenarios(StatementCode,RetailerCode,DataSetCode,Season,ScenarioCode,Status,DateModified)
		VALUES(@STATEMENTCODE,@Retailercode,@DATASETCODE,@BASFSeason,'USE CASE',@PlanStatus,@PlanDateCreated)
				
		IF @REGION='EAST' AND @DATASETCODE > 0 
		BEGIN
			
			DELETE RP_DT_Scenarios WHERE DataSetCode=@DataSetCode AND RetailerCode=@Retailercode
		
			INSERT INTO RP_DT_Scenarios(StatementCode,DataSetCode,RetailerCode,Season,ScenarioCode,Status,DateModified)
			VALUES(@DataSetCode,0,@Retailercode,@BASFSeason,'USE CASE',@PlanStatus,@PlanDateCreated)
		END
		
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

