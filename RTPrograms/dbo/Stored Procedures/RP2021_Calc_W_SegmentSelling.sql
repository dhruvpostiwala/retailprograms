﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_SegmentSelling] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	/*

	DECLARE @SEASON INT = 2021
	DECLARE @STATEMENT_TYPE [VARCHAR](20) = 'Actual'
	DECLARE @PROGRAM_CODE [VARCHAR](50) = 'RP2021_W_SEGMENT_SELLING'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT 

	INSERT INTO @STATEMENTS (StatementCode)
	VALUES(5786)

	EXEC [dbo].[RP2021_Calc_W_SegmentSelling] @SEASON, @STATEMENT_TYPE,  @PROGRAM_CODE, @STATEMENTS 	
	
	*/

	/*


	 MP: Code review on Sep/06/2021
		 This program goes on to single market letter i.e CCP
		 There is no point maintaing market letter code and program code in temp tables, waste of additional joins.
		
		I am removing market letter code and program code columns from Temp tables. If required pull a back up before Sep/6/2021 for comparison.
		By removing market letter code and program code from temp table, procedure would run faster.
		Same needs to be done in other programs as well
	*/

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'
	

	DECLARE @ROUNDING_PERCENT NUMERIC(6,4) = 0.005
	--DECLARE @ROUNDING_DECIMAL NUMERIC(6,4) = 0.05
	DECLARE @ROUNDING_DECIMAL NUMERIC(6,4) = 0.025
	

	DECLARE @MINIMUM_REWARD_PERCENTAGE NUMERIC(6,4) = 0.035
	DECLARE @LY_SEASON INT  = @SEASON - 1;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,3) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		SegmentsAVG_CY FLOAT DEFAULT 0,
		SegmentsAVG_LY FLOAT DEFAULT 0,
		Growth FLOAT DEFAULT 0,
		Reward_Percentage_A NUMERIC(6,3) DEFAULT 0,
		Reward_A MONEY DEFAULT 0,
		Reward_Percentage_B NUMERIC(6,3) DEFAULT 0,
		Reward_B MONEY DEFAULT 0,
		Reward_Percentage_Adjustment NUMERIC(6,3) DEFAULT 0,
		Reward_Adjustment MONEY DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			RetailerCode ASC,
			RewardBrand ASC
		)
	);

	DECLARE @SEGMENT_SALES_TABLE TABLE (
		StatementCode INT NOT NULL,		
		RetailerCode VARCHAR(50) NOT NULL,		
		FarmCode VARCHAR(50) NOT NULL,
		Segment VARCHAR(50) NOT NULL,		
		Ret_Acres_CY INT DEFAULT 0,
		Ret_Acres_LY INT DEFAULT 0,		
		SP_Acres_CY INT DEFAULT 0,
		SP_Acres_LY INT DEFAULT 0,
		Family_Acres_CY INT DEFAULT 0,
		Family_Acres_LY INT DEFAULT 0,
		Flag VARCHAR(20) NOT NULL DEFAULT '',
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,						
			RetailerCode ASC,
			FarmCode ASC,
			Segment ASC						
		)
	);
	
	DECLARE @SEGMENT_RET_FARM_TABLE TABLE (
		StatementCode INT NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		FarmCode VARCHAR(50) NOT NULL,
		Segments_CY INT,
		Segments_LY INT,	
		Has_Sales_CY INT,
		Has_Sales_LY INT,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			RetailerCode ASC,
			FarmCode ASC
		)
	);

	DECLARE @QUALIFYING_TABLE TABLE (
		StatementCode INT NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		SegmentsAVG_CY FLOAT DEFAULT 0,
		SegmentsAVG_LY FLOAT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			RetailerCode ASC
		)
	);
	   	 	
	INSERT INTO @TEMP(StatementCode,RetailerCode, RewardBrand,RewardBrand_Sales)
	SELECT TX.StatementCode, TX.RetailerCode, TX.GroupLabel AS RewardBrand, SUM(TX.Price_CY) [RewardBrand_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
	WHERE tx.ProgramCode = @PROGRAM_CODE AND tx.GroupType = 'ALL_CPP_BRANDS' 
		--AND TX.GroupLabel NOT IN ('Liberty 150', 'Liberty 200', 'Centurion') -- taken care in refersh sales conoslidated for the season
	GROUP BY TX.StatementCode, TX.RetailerCode, TX.GroupLabel	

	-- Set the reward percentage
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
			SET T1.Reward_Percentage = ISNULL(EI.FieldValue / 100, 0)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND EI.MarketLetterCode=@CCP_ML_CODE AND EI.FieldCode = 'Segment_Selling'
		WHERE T1.RewardBrand_Sales > 0
	END

	-- ACTUAL CALCULATIONS
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN

		DROP TABLE IF EXISTS #ProductReference
		
		SELECT GR.GroupLabel AS Segment, BR.ChemicalGroup,'All' as Level5Code
		INTO #ProductReference
		FROM RP_Config_Groups_Brands BR	
			INNER JOIN RP_Config_Groups GR			
			ON GR.GroupID = BR.GroupID 							
		WHERE GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'QUALIFYING_BRANDS'
		
			UNION

		--D520062427 -- NUTRIEN -- Please adjust code so that Heat (Pre-Seed) and Heat (Pre-Harvest) are both included for Nutrien only.
		SELECT 'SEGMENT_HERB' AS Segment, 'Heat' AS  ChemicalGroup, 'D520062427' AS Level5Code

		
		-- GET THE RETAILER - GROWER SALES FROM ALL RETAILER LOCATIONS IN THE FAMILY
		/*
		INSERT INTO @SEGMENT_SALES_TABLE (StatementCode, Segment, RetailerCode, FarmCode, Ret_Acres_CY, Ret_Acres_LY)
		SELECT	TX.StatementCode, GR.GroupLabel, TX.RetailerCode, TX.FarmCode
				,SUM(IIF(TX.BASFSeason = @SEASON, Acres, 0)) AS Ret_Acres_CY
				,SUM(IIF(TX.BASFSeason = @LY_SEASON, Acres, 0)) AS Ret_Acres_LY				
		FROM @STATEMENTS ST						
			INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode			
			INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup = tx.ChemicalGroup
			INNER JOIN RP_Config_Groups GR			ON GR.GroupID = BR.GroupID 							
		WHERE GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'QUALIFYING_BRANDS' 
			AND tx.InRadius = 1 AND TX.BASFSeason IN (@SEASON, @LY_SEASON)					
		GROUP BY TX.StatementCode, GR.GroupLabel, TX.RetailerCode, TX.FarmCode
		*/

		INSERT INTO @SEGMENT_SALES_TABLE (StatementCode, Segment, RetailerCode, FarmCode, Ret_Acres_CY, Ret_Acres_LY)
		SELECT	TX.StatementCode, PR.Segment, TX.RetailerCode, TX.FarmCode
				,SUM(IIF(TX.BASFSeason = @SEASON, Acres, 0)) AS Ret_Acres_CY
				,SUM(IIF(TX.BASFSeason = @LY_SEASON, Acres, 0)) AS Ret_Acres_LY				
		FROM @STATEMENTS ST			
			INNER JOIN RP_Statements S				ON S.StatementCode = ST.StatementCode
			INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode			
			INNER JOIN #ProductReference PR			ON PR.ChemicalGroup = TX.ChemicalGroup					
		WHERE tx.InRadius = 1 AND TX.BASFSeason IN (@SEASON, @LY_SEASON)					
				AND (PR.Level5Code = 'All' OR PR.Level5Code = S.Level5Code)
		GROUP BY TX.StatementCode, PR.Segment, TX.RetailerCode, TX.FarmCode
			

		
		BEGIN
			/*
			Standalone Seed Treater/Processor Sales Qualifications
			·	Retail programs will leverage the business rules for the Seed Treater/Processor data model (managed in a separate requirement document) to apply the qualifying product to the originating retailers. 
			·	By way of this model, in any grower level matching qualification rewards, the sales of custom applied product made by standalone seed treaters/processors will be traced back to the originating authorized BASF retailer 
			·	For example: 
			·	Retail 1 sells Farmer John Herbicides and Fungicides
			·	Retail 1 sells ABC Seed Processor Seed Treatments
			·	ABC Seed Processor sells Farmer John his Seed Treatment
			·	BASF will assess Retail 1 qualification in Segment Selling as having sold the grower 3 segments: Seed Treatments, Herbicide, and Fungicides (but will only pay on the Herbicides & Fungicides)

			*/

			/*
					From: Jennifer Charter <jcharter@kenna.ca>
					Date: Tuesday, November 2, 2021 at 9:42 AM
					To: Mahesh Pantangi <mpantangi@kenna.ca>, Aimee Richard <arichard@kenna.ca>
					Cc: Mark Irons <mirons@kenna.ca>, Mellisa Sinclair <msinclair@kenna.ca>, Ann LaFontaine <alafontaine@kenna.ca>, Jeremie Mayer <JMayer@kenna.ca>
					Subject: RE: Rule Confirmation: Seed Processors and Reward Qualification
					Hi:

					Here are all of the Seed Processors we have activated for the 2022 season so far:
					Dundas Farms Ltd  R520172180 / F0029284.
					WOOLIAMS FARMS R520172208 / F0000557
					Airth Farms R520172455 / F520054221
					Fabian Seed Farms R520172543 / F0112478
					Pincher Creek Seed Cleaning Co-op (reactivated) R520003678 (no grower code)
					Peturic Seed Farm R520172556 / F0003054

					Thanks,
					Jen
			*/

			-- Delete Seed Treatment / Inoculant sales made to a seed processor
			DELETE T1
			FROM @SEGMENT_SALES_TABLE T1
				INNER JOIN RetailerProfile RP
				ON RP.LinkedFarmCode=T1.FarmCode
			WHERE T1.Segment = 'SEGMENT_INOC_STT' AND RP.LinkedFarmCode NOT IN ('F0029284','F0000557','F520054221','F0112478','F0003054')
					   
			drop table if exists #eligible_retails

			select	rp.retailercode
			into	#eligible_retails
			from	RetailerProfile rp		
				inner join retailerprofileaccounttype rt		
				on rt.retailercode=rp.retailercode
				inner join (
					select AccountType 
					from [dbo].[RP_Config_ML_AccountTypes] 
					where marketlettercode='ML2021_W_CPP'
				) mat
				on mat.accounttype=rt.accounttype	
			where	rp.Region = 'West'  and rp.level5code in ('D000001','D0000117','D520062427','D0000107','D0000137','D0000244')
			group by rp.retailercode

   
			-- SEED PROCESSOR RETAILERS LIST. EXCLUDING WHO IS ALREADY IN THE ELIGIBLE RETAILERS LIST --
			drop table if exists #seed_processor_retail
			Select rp.retailercode, rp.linkedfarmcode
			into	#seed_processor_retail
			From retailerprofile rp
				inner join (
					Select	RetailerCode
					from	retailerprofileaccounttype
					where	accounttype='seed processor' 
						AND Retailercode NOT IN ('R520172180','R520172208','R520172455','R520172543','R520003678','R520172556')
				) sp 
				on sp.retailercode = rp.retailercode
				left join #eligible_retails elg
				on elg.retailercode=rp.retailercode
			where rp.linkedfarmcode <> ''  and elg.retailercode is null

			-- SEGMENT SELLING QUALIFYING PRODUCTS  LIST --
			drop	table if exists #eligible_products
	
			SELECT PR.ProductCode, PR.ChemicalGroup, pr.product, pr.ProductName, pr.conversionw, GR.GroupLabel AS [Segment]
			INTO #eligible_products
			FROM ProductReference PR
				INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=PR.ChemicalGroup
				INNER JOIN RP_Config_Groups GR			ON GR.GroupID = BR.GroupID		
			WHERE GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'QUALIFYING_BRANDS'

			DELETE #eligible_products
			WHERE ChemicalGroup = 'NODULATOR PRO' AND product <> 'NODULATOR PRO 100'
			
			-- QUALIFYING SEGMENT TRANSACTIONS FOR CURRENT AND LAST YEAR --	
			drop table if exists #transactions

			select tx.basfseason, tx.retailercode, tx.farmcode, pr.chemicalgroup, pr.segment, CAST(SUM(tx.quantity * pr.conversionw) AS Decimal(18,2)) as acres
			into #transactions
			from GrowerTransactionsBASFDetails tx
				inner join #eligible_products pr
				on pr.productcode=tx.productcode
			where tx.basfseason in (@season,@LY_SEASON) 
			group by tx.basfseason, tx.retailercode, tx.farmcode, pr.chemicalgroup, pr.segment
	 
	 
			-- SEED PROCESSOR SOLD SEED TREATMENTS/INOCULANT PRODUCTS TO GROWER
			drop table if exists #trans_sp_to_grower   

			select tx.BASFSeason, tx.retailercode as seed_retailercode, tx.farmcode, tx.chemicalgroup, tx.segment, acres
			into #trans_sp_to_grower
			from #transactions tx
				inner join #seed_processor_retail rp		
				on rp.retailercode=tx.retailercode		
			where tx.Segment='SEGMENT_INOC_STT'  
	 
			-- RETAILER SOLD SEED TREATMENTS/INOCULANT PRODUCTS TO SEED PROCESSOR (LinkedFarmCode)
			drop table if exists #trans_ret_to_sp 

			select tx.BASFSeason, tx.retailercode, sp.retailercode as seed_retailercode, tx.chemicalgroup, tx.segment, sum(acres) AS acres
			into #trans_ret_to_sp
			from #transactions tx
				inner join #eligible_retails rp				on rp.retailercode = tx.retailercode		
				inner join #seed_processor_retail sp		on sp.linkedfarmcode = tx.farmcode		
			where tx.Segment = 'SEGMENT_INOC_STT' 
			group by tx.BASFSeason, tx.retailercode, sp.retailercode, tx.chemicalgroup, tx.segment
	
			-- RETAILER SOLD HERB/FUNG/LIB/CENT PRODUCTS TO GROWERS , EXCLUDING SEED PROCESSOR LINKEDFARMCODE
			drop table if exists #trans_ret_to_grower 

			select tx.BASFSeason, tx.retailercode, tx.farmcode, tx.ChemicalGroup, tx.segment, sum(acres) as acres
			into #trans_ret_to_grower
			from #transactions tx
				inner join #eligible_retails rp			on rp.retailercode = tx.retailercode		
				--left join #seed_processor_retail sp		on sp.linkedfarmcode = tx.farmcode		
			where tx.Segment <> 'SEGMENT_INOC_STT'	--and sp.linkedfarmcode is null
			group by tx.BASFSeason, tx.retailercode, tx.farmcode, tx.ChemicalGroup, tx.segment
			having sum(acres) > 0

			-- LETS MAP SALES BACK TO ORIGINAL RETAILER (i.e bought by Seed Processor and sold to a grower)
			
			IF USER_NAME() = 'THEKENNAGROUP\mpantangi'
			BEGIN
				DROP TABLE [THEKENNAGROUP\mpantangi].[zMP_SeedRetailer_To_Grower_Sales]

				SELECT stg.seed_retailercode, rts.RetailerCode, stg.Farmcode, stg.Segment
					,SUM(IIF(stg.BASFSeason = @SEASON, stg.Acres, 0)) Acres_CY
					,SUM(IIF(stg.BASFSeason = @LY_SEASON, stg.Acres, 0)) Acres_LY	
				INTO zMP_SeedRetailer_To_Grower_Sales
				FROM #trans_sp_to_grower stg
					inner join #trans_ret_to_sp rts
					on rts.seed_retailercode = stg.seed_retailercode and stg.chemicalgroup=rts.chemicalgroup and rts.BASFSeason=stg.BASFSeason
					inner join (
						select retailercode, farmcode, basfseason
						from #trans_ret_to_grower
						group by retailercode, farmcode, basfseason
					) tt
					on tt.farmcode = stg.farmcode and tt.basfseason=stg.basfseason and tt.basfseason=rts.basfseason and tt.retailercode=rts.retailercode						
				GROUP BY stg.seed_retailercode, rts.RetailerCode, stg.Farmcode, stg.Segment
				OPTION (RECOMPILE)
			END
			
			
			drop table if exists #Final_Data
			
			SELECT rts.RetailerCode, stg.Farmcode, stg.Segment
				,SUM(IIF(stg.BASFSeason = @SEASON, stg.Acres, 0)) Acres_CY
				,SUM(IIF(stg.BASFSeason = @LY_SEASON, stg.Acres, 0)) Acres_LY	
			INTO #Final_Data 
			FROM #trans_sp_to_grower stg
				inner join #trans_ret_to_sp rts
				on rts.seed_retailercode = stg.seed_retailercode and stg.chemicalgroup=rts.chemicalgroup and rts.BASFSeason=stg.BASFSeason
				inner join (
						select retailercode, farmcode, basfseason
						from #trans_ret_to_grower
						group by retailercode, farmcode, basfseason
				) tt
				on tt.farmcode = stg.farmcode and tt.basfseason=stg.basfseason and tt.basfseason=rts.basfseason and tt.retailercode=rts.retailercode
			GROUP BY rts.RetailerCode, stg.Farmcode, stg.Segment
			OPTION (RECOMPILE)			
			

			MERGE @SEGMENT_SALES_TABLE AS  TGT
			USING (
				SELECT ST.StatementCode,TX.Segment,TX.RetailerCode,TX.Farmcode ,TX.Acres_CY ,TX.Acres_LY
				FROM @STATEMENTS ST
					INNER JOIN RP_StatementsMappings MAP		ON MAP.StatementCode=ST.StatementCode
					INNER JOIN #Final_Data TX					ON TX.Retailercode=MAP.RetailerCode				
			) SRC
			ON SRC.Retailercode=TGT.Retailercode AND SRC.FarmCode=TGT.Farmcode AND SRC.Segment=TGT.Segment
			WHEN MATCHED THEN
				UPDATE 
				SET Flag='Updated'
					,SP_Acres_CY = SRC.Acres_CY
					,SP_Acres_LY = SRC.Acres_LY 
			WHEN NOT MATCHED THEN
				INSERT (StatementCode, Segment, RetailerCode, FarmCode, Flag,SP_Acres_CY,SP_Acres_LY)
				VALUES(SRC.StatementCode, SRC.Segment, SRC.RetailerCode, SRC.FarmCode, 'Inserted', SRC.Acres_CY, SRC.Acres_LY) 
			OPTION (RECOMPILE);
		END			

		/***********************************************************/

		DELETE @SEGMENT_SALES_TABLE WHERE FarmCode IN ('F520040993','F520020402')
		
		/************* FARM LEVEL AGGREGATION BEGIN *********************/
		BEGIN
			DROP TABLE IF EXISTS #FARM_SEGMENT_ACRES
		
			SELECT StatementCode, FarmCode, Segment,  SUM(Ret_Acres_CY+SP_Acres_CY) Family_Acres_CY, SUM(Ret_Acres_LY+SP_Acres_LY) Family_Acres_LY
			INTO #FARM_SEGMENT_ACRES
			FROM @SEGMENT_SALES_TABLE
			GROUP BY StatementCode, FarmCode, Segment

			DROP TABLE IF EXISTS #FINAL_SALES_DATA

			select StatementCode, RetailerCode, FarmCode, Segment,
					max(iif([year]='cy',family_acres,0)) Family_Acres_CY,
					max(iif([year]='ly',family_acres,0)) Family_Acres_LY
			INTO #FINAL_SALES_DATA
			FROM (
				select ss.*, fs.segment, fs.family_acres_cy family_acres, 'CY' [year]
				from (
					select ss.statementcode, ss.retailercode, ss.farmcode
					from @segment_sales_table ss
					where ret_acres_cy>0
					group by ss.statementcode, ss.retailercode, ss.farmcode
				) ss
				left join #farm_segment_acres fs
				on fs.farmcode=ss.farmcode	and fs.statementcode=ss.statementcode

					union all

			select ss.*, fs.segment, fs.family_acres_ly family_acres, 'LY' [year]
			from (
				select ss.statementcode, ss.retailercode, ss.farmcode
				from @segment_sales_table ss
				where ret_acres_ly>0
				group by ss.statementcode, ss.retailercode, ss.farmcode
			) ss
				left join #farm_segment_acres fs
				on fs.farmcode=ss.farmcode		and fs.statementcode=ss.statementcode
			) ss
			group by StatementCode, RetailerCode, FarmCode, Segment

			MERGE @SEGMENT_SALES_TABLE AS  TGT
			USING (				
				select StatementCode, RetailerCode, FarmCode, Segment,Family_Acres_CY,Family_Acres_LY
				FROM #FINAL_SALES_DATA ST					
			) SRC
			ON SRC.Retailercode=TGT.Retailercode AND SRC.FarmCode=TGT.Farmcode AND SRC.Segment=TGT.Segment
			/*
			WHEN MATCHED THEN
				UPDATE 
				SET Family_Acres_CY = SRC.Family_Acres_CY
					,Family_Acres_LY = SRC.Family_Acres_LY
			*/
			WHEN NOT MATCHED THEN
				INSERT (StatementCode, Segment, RetailerCode, FarmCode, Ret_Acres_CY, Ret_Acres_LY)
				VALUES(SRC.StatementCode, SRC.Segment, SRC.RetailerCode, SRC.FarmCode, SRC.Family_Acres_CY, SRC.Family_Acres_LY) 
			OPTION (RECOMPILE);


			/*
			UPDATE T1
			SET Family_Acres_CY = T2.Family_Acres_CY
			FROM @SEGMENT_SALES_TABLE T1
				INNER JOIN #FARM_SEGMENT_ACRES  T2
				ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.FarmCode AND  T1.Segment = T2.Segment
			WHERE T1.Ret_Acres_CY > 0 OR  T1.SP_Acres_CY > 0  
		

			UPDATE T1
			SET Family_Acres_LY = T2.Family_Acres_LY
			FROM @SEGMENT_SALES_TABLE T1
				INNER JOIN #FARM_SEGMENT_ACRES  T2
				ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.FarmCode AND T1.Segment = T2.Segment
			WHERE T1.Ret_Acres_LY > 0 OR  T1.SP_Acres_LY > 0  
			*/
		
			DROP TABLE IF EXISTS #FARM_SEGMENTS		
			SELECT StatementCode, FarmCode
				,COUNT(DISTINCT(Segment_CY)) AS Segments_CY
				,COUNT(DISTINCT(Segment_LY)) AS Segments_LY
			INTO #FARM_SEGMENTS
			FROM (
				SELECT StatementCode, FarmCode
						,IIF(FS.Family_Acres_CY >= 300, Segment, NULL) AS Segment_CY
						,IIF(FS.Family_Acres_LY >= 300, Segment, NULL) AS Segment_LY							
				FROM #FARM_SEGMENT_ACRES FS
			) D
			GROUP BY StatementCode, FarmCode
		
		END
		/************* FARM LEVEL AGGREGATION END *********************/

				
		INSERT	INTO @SEGMENT_RET_FARM_TABLE(StatementCode, RetailerCode, FarmCode, Segments_CY, Segments_LY, Has_Sales_CY, Has_Sales_LY)
		SELECT StatementCode, RetailerCode, FarmCode, 0 Segments_CY, 0 AS Segments_LY
			,MAX(IIF(Ret_Acres_CY > 0, 1,0)) AS Has_Sales_CY 
			,MAX(IIF(Ret_Acres_LY > 0, 1,0)) AS Has_Sales_LY 			
		FROM @SEGMENT_SALES_TABLE 
		GROUP BY StatementCode, RetailerCode, FarmCode
			   
		UPDATE	T1
		SET		[Segments_CY] = T2.Segments_CY 				
		FROM	@SEGMENT_RET_FARM_TABLE T1
			INNER JOIN #FARM_SEGMENTS T2
			ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.FarmCode
		WHERE T1.Has_Sales_CY = 1 
				
		UPDATE	T1
		SET		[Segments_LY] = T2.Segments_LY 
		FROM	@SEGMENT_RET_FARM_TABLE T1
			INNER JOIN #FARM_SEGMENTS T2
			ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.FarmCode
		WHERE T1.Has_Sales_LY = 1

		-- CALCULATE THE AVERAGE SEGMENTS FOR EACH RETAIL LOCATION 
		INSERT	INTO @QUALIFYING_TABLE (StatementCode, RetailerCode, SegmentsAVG_CY, SegmentsAVG_LY) 
		SELECT	T1.StatementCode, T1.RetailerCode
				,ISNULL(T2.SegmentsAVG_CY, 0) AS SegmentsAVG_CY
				,ISNULL(T3.SegmentsAVG_LY, 0) AS SegmentsAVG_LY
		FROM	(
			SELECT	StatementCode, RetailerCode
			FROM	@SEGMENT_RET_FARM_TABLE
			GROUP BY StatementCode, RetailerCode
		)T1
		LEFT	JOIN (
			SELECT	StatementCode, RetailerCode
					,AVG(CONVERT(FLOAT, Segments_CY)) AS SegmentsAVG_CY
			FROM	@SEGMENT_RET_FARM_TABLE
			WHERE	Segments_CY > 0
			GROUP	BY StatementCode, RetailerCode
		)T2
			ON T1.StatementCode = T2.StatementCode AND T1.RetailerCode = T2.RetailerCode
		LEFT	JOIN (
			SELECT	StatementCode, RetailerCode
					,AVG(CONVERT(FLOAT, Segments_LY)) AS SegmentsAVG_LY
			FROM	@SEGMENT_RET_FARM_TABLE
			WHERE	Segments_LY > 0
			GROUP	BY StatementCode, RetailerCode
		)T3
			ON T1.StatementCode = T3.StatementCode AND T1.RetailerCode = T3.RetailerCode			
	/*
		IF USER_NAME() = 'THEKENNAGROUP\mpantangi'
		BEGIN						
			DROP TABLE [THEKENNAGROUP\mpantangi].[zMP_Segment_Sales]
			SELECT *
			INTO zMP_Segment_Sales
			FROM @SEGMENT_SALES_TABLE	

			DROP TABLE [THEKENNAGROUP\mpantangi].[zMP_Seg_Ret_Farm_Table]
			SELECT * --StatementCode,RetailerCode,FarmCode,Segments_CY,Segments_LY
			INTO zMP_Seg_Ret_Farm_Table
			FROM @SEGMENT_RET_FARM_TABLE			
		END 
	*/
		-- INSERT QUALIFYING VALUES INTO TEMP
		UPDATE	T1
		SET		SegmentsAVG_CY = T2.SegmentsAVG_CY
				,SegmentsAVG_LY = T2.SegmentsAVG_LY
		FROM	@TEMP T1
		INNER	JOIN @QUALIFYING_TABLE T2 
		ON T1.StatementCode = T2.StatementCode AND T1.RetailerCode = T2.RetailerCode
			   				


/*
AVERAGING_SHORT_METHOD:


		UPDATE	T1
		SET		SegmentsAVG_CY = ISNULL(CY.avg_segments,0)
				,SegmentsAVG_LY = ISNULL(LY.avg_segments,0)
		FROM	@TEMP T1
			LEFT JOIN (
				select statementcode, retailercode
					,count(distinct farmcode) farms, count(segment) segments
					,count(segment)*1.0/count(distinct farmcode) avg_segments
				from @SEGMENT_SALES_TABLE 
				where family_acres_cy>=300
				group by statementcode, retailercode			
			) CY
			ON CY.StatementCode = T1.StatementCode AND CY.RetailerCode = T1.RetailerCode
			LEFT JOIN (
				select statementcode, retailercode
				,count(distinct farmcode) farms, count(segment) segments
				,count(segment)*1.0/count(distinct farmcode) avg_segments
				from @SEGMENT_SALES_TABLE 
				where family_acres_ly>=300
				group by statementcode, retailercode			
			) LY
			ON LY.StatementCode = T1.StatementCode AND LY.RetailerCode = T1.RetailerCode
*/
		
		-- SET REWARD PERCENT FOR PART A:
		-- THIS REWARD PERCENTAGES IS SAME FOR ALL RETAILERS (RC/IND,FCL,Line Companies)
		UPDATE	@TEMP SET [Reward_Percentage_A] = 0.02 WHERE [SegmentsAVG_CY] + @ROUNDING_DECIMAL >= 1.65


		-- CALCULATE GROWTH:
		UPDATE	@TEMP 
		SET		[Growth] =	CASE
								WHEN SegmentsAVG_CY <= 0 AND SegmentsAVG_LY <= 0 THEN 0.00
								WHEN SegmentsAVG_CY >= 0 AND SegmentsAVG_LY <= 0 THEN 9.9999								
								WHEN SegmentsAVG_CY/SegmentsAVG_LY >= 9.9999 THEN 9.99
								ELSE SegmentsAVG_CY/SegmentsAVG_LY
							END


		-- SET REWARD PERCENT FOR PART B: 

		/*
			Reward Percentage tiers are same for all retailers (RC/IND, FCL, Line Companies)
				2020 to 2021 Average Segments per Grower Growth	Reward
					105% +			2.5%
					100% - 104.9%	2%
					90% - 99.9%		1.5%
					0% - 89.9%		0%

		*/

		UPDATE	@TEMP SET [Reward_Percentage_B] = 0.025 WHERE [Growth] + @ROUNDING_PERCENT >= 1.05
		UPDATE	@TEMP SET [Reward_Percentage_B] = 0.020 WHERE [Reward_Percentage_B] = 0 AND [Growth] + @ROUNDING_PERCENT >= 1.00
		UPDATE	@TEMP SET [Reward_Percentage_B] = 0.015 WHERE [Reward_Percentage_B] = 0 AND [Growth] + @ROUNDING_PERCENT >= 0.90


		-- SET REWARD PERCENT FOR ADJUSTMENT:
		UPDATE	@TEMP 
		SET		[Reward_Percentage_Adjustment] =	
					CASE
						WHEN [Reward_Percentage_A] + [Reward_Percentage_B] >= @MINIMUM_REWARD_PERCENTAGE THEN 0
						ELSE @MINIMUM_REWARD_PERCENTAGE - ([Reward_Percentage_A] + [Reward_Percentage_B])
					END

		-- SET TOTAL REWARD PERCENTAGE (INCLUDING ADJUSTMENT)
		UPDATE	@TEMP SET [Reward_Percentage] = [Reward_Percentage_A] + [Reward_Percentage_B] + [Reward_Percentage_Adjustment]			   
	END

	-- END OF ACTUAL CALCULATIONS

	-- SET REWARD AMOUNT
	UPDATE	@TEMP 
	SET		[Reward] = [RewardBrand_Sales]*[Reward_Percentage]
			,[Reward_A] = [RewardBrand_Sales]*[Reward_Percentage_A]
			,[Reward_B] = [RewardBrand_Sales]*[Reward_Percentage_B]
			,[Reward_Adjustment] = [RewardBrand_Sales]*[Reward_Percentage_Adjustment]


	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[Reward] = 0, [Reward_A] = 0, [Reward_B] = 0, [Reward_Adjustment] = 0
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, RetailerCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP
		GROUP	BY StatementCode, RetailerCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.RetailerCode = T2.RetailerCode
	WHERE	T2.Statement_Reward < 0


	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=@CCP_ML_CODE  AND EI.FieldCode = 'Radius_Percentage_CPP'
		WHERE [Reward] > 0
	END

	-- Copy the records from the temporary table into the permanent table
	DELETE FROM [dbo].[RP2021_DT_Rewards_W_SegSellingQualifiers] WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO [dbo].[RP2021_DT_Rewards_W_SegSellingQualifiers](StatementCode,RetailerCode,FarmCode,Segment,Ret_Acres_CY,Ret_Acres_LY,SP_Acres_CY,SP_Acres_LY,Family_Acres_CY,Family_Acres_LY,Flag)
	SELECT StatementCode,RetailerCode,FarmCode,Segment,Ret_Acres_CY,Ret_Acres_LY,SP_Acres_CY,SP_Acres_LY,Family_Acres_CY,Family_Acres_LY,Flag
	FROM @SEGMENT_SALES_TABLE


	DELETE FROM RP2021_DT_Rewards_W_SegmentSelling WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_SegmentSelling(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward,SegmentsAVG_CY,SegmentsAVG_LY,Growth,Reward_Percentage_A,Reward_A,Reward_Percentage_B,Reward_B,Reward_Percentage_Adjustment,Reward_Adjustment)
	SELECT StatementCode,@CCP_ML_CODE as MarketLetterCode,@PROGRAM_CODE as ProgramCode,RetailerCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward,SegmentsAVG_CY,SegmentsAVG_LY,Growth,Reward_Percentage_A,Reward_A,Reward_Percentage_B,Reward_B,Reward_Percentage_Adjustment,Reward_Adjustment
	FROM @TEMP
END
