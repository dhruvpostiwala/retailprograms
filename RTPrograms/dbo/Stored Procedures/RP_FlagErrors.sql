﻿

CREATE PROCEDURE [dbo].[RP_FlagErrors] @Season INT
AS
BEGIN
	SET NOCOUNT ON;
	
	RETURN;

	DECLARE @ERRORED_STATEMENTS AS TABLE(
		[StatementCode] INT NOT NULL,
		[Status] VARCHAR(50) NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[ErrorMessage] VARCHAR(MAX) NULL,
	PRIMARY KEY CLUSTERED(
		[StatementCode] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY])

	/* LETS CHECK ALL UNLOCKED STATEMENTS */
	INSERT INTO @ERRORED_STATEMENTS([StatementCode],[Status],[Level5Code])
	SELECT [StatementCode],[Status],[Level5Code]
	FROM RP_Statements
	WHERE [Season]=@SEASON AND [VersionType]='Unlocked' AND [StatementType]='Actual'
			
	/* REWARD SUMMARY BREAKDOWNS VS CHEQUE AMOUNTS */
	UPDATE T1
	SET [Status]='ERROR',[ErrorMessage]='The AmtPaid on a cheque for this statement does not match the Paid to Date breakdown on the associated archive statement.'
	FROM @ERRORED_STATEMENTS T1
		LEFT JOIN(
			SELECT [StatementCode],SUM([PaidToDate]) AS [Total]
			FROM RPWeb_Rewards_Summary
			GROUP BY [StatementCode]
		) rs
		ON T1.[StatementCode]=rs.[StatementCode]
		LEFT JOIN(
			SELECT st.[SRC_StatementCode] 
				,SUM(IIF(chq.[ChequeStatus] IN ('Sent','Cashed'),chq.[AmtPaid],0)) AS [CHQTotal]		
			FROM RPWeb_Statements st
				INNER JOIN RPWeb_Cheques chq
				ON chq.[StatementCode]=st.[StatementCode] 
			WHERE chq.[ChequeType]='Cheque' AND chq.[VersionType]='Current' 
			GROUP BY [SRC_StatementCode]
		) chq
		ON chq.[SRC_StatementCode]=T1.[StatementCode]
	WHERE T1.Level5Code <> 'D000001' AND  ABS(COALESCE(rs.[Total],0)-COALESCE(chq.[CHQTotal],0))>0.01
	 				
	UPDATE t1
	SET [ErrorStatus]='RESOLVED'
	FROM RP_Error_Log T1 
		LEFT JOIN @ERRORED_STATEMENTS T2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.[ErrorMessage]=t2.[ErrorMessage]
	WHERE COALESCE(t2.[Status],'Active')='Active'
	

	INSERT INTO RP_Error_Log([StatementCode],[ErrorDate],[ErrorMessage],[ErrorStatus])
	SELECT t1.[StatementCode],GETDATE(),t1.[ErrorMessage],'UNRESOLVED'
	FROM @ERRORED_STATEMENTS t1
		LEFT JOIN RP_Error_Log t2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.[ErrorMessage]=t2.[ErrorMessage] AND t2.[ErrorStatus]='UNRESOLVED'
	WHERE t2.[StatementCode] IS NULL AND t1.[Status]='ERROR' AND ISNULL(t1.[ErrorMessage],'') <> ''
		
	DECLARE @EDIT_HISTORY RP_EDITHISTORY
	MERGE RPWeb_Statements dest
	USING(
		SELECT t1.[StatementCode],t1.[Status],t1.[ErrorMessage]
		FROM @ERRORED_STATEMENTS t1
			INNER JOIN RPWeb_Statements t2
			ON t1.[StatementCode]=t2.[StatementCode]
		WHERE t1.[Status]<>t2.[Status]
	) src ON src.[StatementCode]=dest.[StatementCode]
	WHEN MATCHED THEN
		UPDATE SET [Status]=src.[Status]
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Message',CASE WHEN src.[Status]='Active' THEN 'Statement status returned to Active' ELSE 'Statement status set as errored. ('+src.[ErrorMessage]+')' END
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[EHEntryType],[EHMessage]);
	
	EXEC RP_GenerateEditHistory 'Statement','Nightly Error Validation',@EDIT_HISTORY

END













