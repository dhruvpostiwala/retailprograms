﻿

CREATE PROCEDURE [dbo].[RPST_Agent_Print_GetData_IAS] @Season INT, @StatementCode INT, @ReportHTML NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET ANSI_WARNINGS OFF;
	SET NOCOUNT ON;

	DECLARE @REGION VARCHAR(4);
	DECLARE @TEMP NVARCHAR(MAX) = '';
	DECLARE @RETAILERINFOHTML NVARCHAR(MAX) = '';
	DECLARE @StatementHTML NVARCHAR(MAX) = '';
	DECLARE @PROGRAM_TITLE VARCHAR(200)='';
	DECLARE @Language VARCHAR(1) = 'E';

	SELECT @REGION=REGION FROM RPWeb_Statements WHERE Statementcode=@STATEMENTCODE

	--ADD STATEMENT SUMMARY, ML SUMMARY, POG REPORT TO THE PRINT MODE PAGE
	--HEADER ROW ON ALL PAGES
	EXEC [Reports].[RPWeb_Get_StatementSummary] @StatementCode, @Language, @temp OUTPUT;
	SET @StatementHTML += ISNULL(@temp,'');

	EXEC [Reports].[RPWeb_Get_ML_RewardSummary] @StatementCode, @Language, @temp OUTPUT;
	SET @StatementHTML += ISNULL(@temp,'');

	EXEC [Reports].[RPWeb_Get_POG_Details] @StatementCode, @Language, @temp OUTPUT;
	SET @StatementHTML += ISNULL(@temp,'');
	
	--Add applicable programs to the print mode page
	DECLARE @ProcedureName VARCHAR(255);
	DECLARE @ProgramCode VARCHAR(200);

	DECLARE ReportProcedure_Loop CURSOR FOR
		SELECT DISTINCT ProgramCode, Title, ReportStoredProcedureName
		FROM RP_Config_Programs P
			INNER JOIN [RPWeb_Rewards_Summary] R		
			ON P.RewardCode = R.RewardCode
		WHERE Season=@Season AND Region=@Region 
			AND R.StatementCode = @STATEMENTCODE
			AND ReportStoredProcedureName IS NOT NULL AND ReportStoredProcedureName <> ''
		ORDER BY P.Title;
	OPEN ReportProcedure_Loop;
	FETCH NEXT FROM ReportProcedure_Loop INTO @ProgramCode, @PROGRAM_TITLE, @ProcedureName;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		EXEC @ProcedureName @StatementCode, @ProgramCode, @temp OUTPUT;
		SET @StatementHTML += ISNULL(@temp,'');
		FETCH NEXT FROM ReportProcedure_Loop INTO @ProgramCode, @PROGRAM_TITLE, @ProcedureName;
	END;
	CLOSE ReportProcedure_Loop;
	DEALLOCATE ReportProcedure_Loop;

	SET @ReportHTML = ISNULL(@StatementHTML,'')

END
