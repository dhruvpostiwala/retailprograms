﻿
CREATE PROCEDURE [dbo].[RP2020_Refresh_EI_FieldValues] @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT=2020;
	DECLARE @FORECAST_STATEMENTS AS UDT_RP_STATEMENT

	/*
	DELETE T1
	FROM RPWeb_User_EI_FieldValues T1
		LEFT JOIN RP_DT_ML_ELG_Programs MLP
		ON MLP.MarketLetterCode=T1.MarketLetterCode AND MLP.ProgramCode=T1.ProgramCode
	WHERE ISNULL(MLP.MarketLetterCode,'')=''
	*/	

	-- IN CASE OF PROJECTION STATEMENT , JUST COPY THE INFORMATION ENTERED FOR RETAILERS FORECAST STATEMENT
	IF @STATEMENT_TYPE='PROJECTION'
	BEGIN
		INSERT INTO RPWeb_User_EI_FieldValues(StatementCode,FieldCode,ProgramCode,MarketLetterCode,FieldValue,CalcFieldValue)
		SELECT RS.StatementCode_P, EI_F.FieldCode, EI_F.ProgramCode, EI_F.MarketLetterCode, EI_F.FieldValue, EI_F.CalcFieldValue
		FROM (
			SELECT F.StatementCode AS [StatementCode_F], P.StatementCode AS [StatementCode_P]
			FROM RP_Statements F
				INNER JOIN (
					SELECT StatementCode, Season, RetailerCode, StatementLevel
					FROM RP_Statements
					WHERE [StatementCode] IN (SELECT StatementCode From @Statements)						
				) P
				ON P.Season=F.Season AND P.RetailerCode=F.RetailerCode AND P.StatementLevel=F.StatementLevel 
			WHERE F.[Status]='Active' AND F.StatementType='Forecast' AND F.VersionType='Final-Preferred'			
		) RS
			INNER JOIN RPWeb_User_EI_FieldValues EI_F		
			ON EI_F.StatementCode=RS.StatementCode_F
			LEFT JOIN RPWeb_User_EI_FieldValues  EI_P		
			ON EI_P.StatementCode=RS.StatementCode_P AND  EI_P.ProgramCode=EI_F.ProgramCode AND EI_P.MarketLetterCode=EI_F.MarketLetterCode AND  EI_P.FieldCode=EI_F.FieldCode
		WHERE ISNULL(EI_P.StatementCode,0)=0
				
		RETURN
	END 

	-- LETS TAKE CARE OF FORECAST STATEMENTS

	MERGE RPWeb_User_EI_FieldValues AS TGT
	USING (
		SELECT ST.StatementCode, EI.FieldCode, EI.ProgramCode, EI.MarketLetterCode, EI.DefaultValue, EI.CalcFieldValue
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS
			ON RS.StatementCode=ST.StatementCode 
			INNER JOIN (
				SELECT EIF.FieldCode, EIF.ProgramCode, EIML.MarketLetterCode, EIF.DefaultValue, EIF.CalcFieldValue
				FROM RP_Config_Programs PRG
					INNER JOIN RP_Config_EI_Fields EIF	ON EIF.ProgramCode=PRG.ProgramCode
					LEFT JOIN RP_Config_EI_ML_Fields EIML	ON EIML.ProgramCode=EIF.ProgramCode
				WHERE PRG.[Season]=@SEASON AND PRG.[Status]='Active' AND EIF.ProgramCode IN ('RP2020_W_ALL_PROGRAMS','RP2020_E_ALL_PROGRAMS') AND EIF.[DisplayOnce]=0 
			) EI
			ON 1=1		
		WHERE (RS.Region='WEST' AND EI.ProgramCode='RP2020_W_ALL_PROGRAMS') OR (RS.Region='EAST' AND EI.ProgramCode='RP2020_E_ALL_PROGRAMS'   /*  AND RS.StatementLevel='LEVEL 1' */)
		     
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.MarketLetterCode=TGT.MarketLetterCode
	WHEN NOT MATCHED THEN
		INSERT (StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)	
		VALUES (SRC.StatementCode, SRC.FieldCode, SRC.ProgramCode, SRC.MarketLetterCode, SRC.DefaultValue, SRC.CalcFieldValue);

	-- FOLLOWING INPUTS SHOULD BE DISPLAYED ONLY ONCE FOR USER
	MERGE RPWeb_User_EI_FieldValues AS TGT
	USING (
		SELECT ST.StatementCode, EIF.FieldCode, MLP.ProgramCode, MLP.MarketLetterCode,  EIF.DefaultValue, EIF.CalcFieldValue
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode		
			INNER JOIN RP_Config_EI_Fields EIF		ON EIF.ProgramCode=MLP.ProgramCode	
		WHERE EIF.DisplayOnce=1			
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode  AND SRC.MarketLetterCode=TGT.MarketLetterCode		 
	WHEN NOT MATCHED THEN
		INSERT (StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)	
		VALUES (SRC.StatementCode, SRC.FieldCode, SRC.ProgramCode, SRC.MarketLetterCode, SRC.DefaultValue, SRC.CalcFieldValue);
	
	-- Update movable targets

	-- RRT-427 - the invigor target should always be calculated UNLESS the user has manually updated it (this is true for ALL targets)
	-- RC-3498 - this should actually be LY1 Bags of Invigor
	UPDATE T1
	SET FieldValue=ISNULL(T2.Bags,0)
		,CalcFieldValue='Yes'
	FROM RPWeb_User_EI_FieldValues T1
		LEFT JOIN (
			/*
			SELECT ST.StatementCode, SUM(IIF(RP.Level5Code = 'D0000117', TX.Price_SRP, TX.Price_SDP)) AS Price				
			FROM @STATEMENTS ST												
				INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
				INNER JOIN RetailerProfile RP		ON RP.RetailerCode=TX.RetailerCode
			WHERE TX.BASFSeason=@SEASON-1
			GROUP BY ST.StatementCode
			*/
			SELECT ST.StatementCode, SUM(TX.Quantity * IIF(PR.Conversion = 200, 20, 1)) AS 'Bags'
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
				INNER JOIN RetailerProfile RP		ON RP.RetailerCode=TX.RetailerCode
				INNER JOIN ProductReference PR		ON TX.ProductCode=PR.ProductCode
			WHERE TX.BASFSeason=@SEASON-1 AND TX.ChemicalGroup = 'INVIGOR'
			GROUP BY ST.StatementCode
		) T2
		ON T2.StatementCode=T1.StatementCode 
	WHERE T1.CalcFieldValue='Yes' AND T1.ProgramCode='RP2020_W_INV_PERFORMANCE' AND T1.FieldCode='Sales_Target' --AND ISNULL(T1.FieldValue,0)=0

	-- NEW: RRT-720 - For Soybean Pulse Reward, do not auto calculate anymore

	/*
	-- NEW: RRT-616 - For Soybean Pulse Reward, we must refresh the following:
	-- Heat LQ/Heat Complete Target
	-- Priaxor/Dyax Bonus Target

	-- Default to LY1 POG
	UPDATE T1
	SET FieldValue=ISNULL(T2.Acres,0)
		,CalcFieldValue='Yes'
	FROM RPWeb_User_EI_FieldValues T1
		LEFT JOIN (
			SELECT ST.StatementCode, SUM(TX.Acres) AS 'Acres'
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
				INNER JOIN RetailerProfile RP		ON RP.RetailerCode=TX.RetailerCode
				INNER JOIN ProductReference PR		ON TX.ProductCode=PR.ProductCode
			WHERE TX.BASFSeason=@SEASON-1 AND TX.ChemicalGroup IN ('HEAT','HEAT LQ','HEAT COMPLETE')
			GROUP BY ST.StatementCode
		) T2
		ON T2.StatementCode=T1.StatementCode 
	WHERE T1.CalcFieldValue='Yes' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.FieldCode='PartB_Target' --AND ISNULL(T1.FieldValue,0)=0
	
	UPDATE T1
	SET FieldValue=ISNULL(T2.Acres,0)
		,CalcFieldValue='Yes'
	FROM RPWeb_User_EI_FieldValues T1
		LEFT JOIN (
			SELECT ST.StatementCode, SUM(TX.Acres) AS 'Acres'
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
				INNER JOIN RetailerProfile RP		ON RP.RetailerCode=TX.RetailerCode
				INNER JOIN ProductReference PR		ON TX.ProductCode=PR.ProductCode
			WHERE TX.BASFSeason=@SEASON-1 AND TX.ChemicalGroup IN ('PRIAXOR','DYAX')
			GROUP BY ST.StatementCode
		) T2
		ON T2.StatementCode=T1.StatementCode 
	WHERE T1.CalcFieldValue='Yes' AND T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' AND T1.FieldCode='PartC_Target' --AND ISNULL(T1.FieldValue,0)=0
	*/

END