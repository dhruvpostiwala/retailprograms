﻿CREATE PROCEDURE [dbo].[RP_Pre_RewardCalc_Setup]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	IF @SEASON=2020
	BEGIN
		-- ORDER OF EXECUTION IS IMPORTANT
		--PRINT N' RP_Calc_ML_Eligible_Programs - ' + cast(getdate() as varchar)
		
		EXEC RP_Calc_ML_Eligible_Programs @SEASON, @STATEMENT_TYPE, @STATEMENTS;
		
		IF @STATEMENT_TYPE = 'ACTUAL'
			EXEC [RP_Calc_ML_Eligible_Programs_Cleanup] @SEASON, @STATEMENT_TYPE, @STATEMENTS;
						

		--PRINT N' RP2020_Refresh_EI_FieldValues - ' + cast(getdate() as varchar)
		IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION') EXEC RP2020_Refresh_EI_FieldValues @STATEMENT_TYPE, @STATEMENTS;	

		--PRINT N' RP2020_Refresh_Sales_Consolidated - ' + cast(getdate() as varchar)
		EXEC RP2020_Refresh_Sales_Consolidated @SEASON, @STATEMENT_TYPE, @STATEMENTS;
		

		--PRINT N' RP_Calc_ML_Eligible_Sales - ' + cast(getdate() as varchar)
		EXEC RP_Calc_ML_Eligible_Sales @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' END - ' + cast(getdate() as varchar)

	END
	ELSE IF @SEASON=2021
	BEGIN
		-- ORDER OF EXECUTION IS IMPORTANT
		--PRINT N' RP_Calc_ML_Eligible_Programs - ' + cast(getdate() as varchar)
		EXEC RP_Calc_ML_Eligible_Programs @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		EXEC [RP_Calc_ML_Eligible_Programs_Cleanup] @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' RP2020_Refresh_EI_FieldValues - ' + cast(getdate() as varchar)
		IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION') EXEC RP2021_Refresh_EI_FieldValues @STATEMENT_TYPE, @STATEMENTS;	

		
		EXEC RP2021_Refresh_Sales_Consolidated @SEASON, @STATEMENT_TYPE, @STATEMENTS;
		EXEC RP2021_Refresh_CAR_Index  @SEASON, @STATEMENT_TYPE, @STATEMENTS;


		--PRINT N' RP_Calc_ML_Eligible_Sales - ' + cast(getdate() as varchar)
		EXEC RP_Calc_ML_Eligible_Sales @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' END - ' + cast(getdate() as varchar)

	END 

	ELSE IF @SEASON=2022
	BEGIN
		-- ORDER OF EXECUTION IS IMPORTANT
		--PRINT N' RP_Calc_ML_Eligible_Programs - ' + cast(getdate() as varchar)
		EXEC RP_Calc_ML_Eligible_Programs @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		EXEC [RP_Calc_ML_Eligible_Programs_Cleanup] @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' RP2022_Refresh_EI_FieldValues - ' + cast(getdate() as varchar)
		IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION','REWARD PLANNER') EXEC RP2022_Refresh_EI_FieldValues @STATEMENT_TYPE, @STATEMENTS;	

		--PRINT N' RP2022_Refresh_Sales_Consolidated - ' + cast(getdate() as varchar)
		EXEC RP2022_Refresh_Sales_Consolidated @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' RP_Calc_ML_Eligible_Sales - ' + cast(getdate() as varchar)
		EXEC RP_Calc_ML_Eligible_Sales @SEASON, @STATEMENT_TYPE, @STATEMENTS;

		--PRINT N' END - ' + cast(getdate() as varchar)

	END 
	
END
