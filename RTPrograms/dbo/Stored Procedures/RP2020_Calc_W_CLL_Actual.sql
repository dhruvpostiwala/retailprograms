﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_W_CLL_Actual]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	Clearfield Commitment Reward
	Retailers can earn $200/ grower, for newly signed evergreen Clearfield Commitments for Lentils, signed and submitted on or before February 28, 2020.
	Retailers can earn $100/ grower, for new Commitments for Lentils signed and submitted between March 1, 2020 and October 8, 2020

	Matching Acres Reward
	•	Sell matching acres of Clearfield herbicide on a grower’s matching committed or registered Clearfield lentil acres.
	•	Retailers can earn 3% reward on registered, matching acres of Clearfield lentil herbicides including, Odyssey NXT herbicide, Odyssey Ultra NXT, Solo ADV herbicide, Solo Ultra herbicides (and all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT)

	*/
/*
	DECLARE @SEASON INT = 2020
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_CLL_SUPPORT'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT

	INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode in (3116)
	EXEC [dbo].[RP2020_Calc_W_CLL_Actual]  @SEASON, 'Actual', @PROGRAM_CODE,  @STATEMENTS 
*/
	
	DECLARE @CommitmentReward AS TABLE (
		Statementcode INT NOT NULL,
		MarketLetterCode VARCHAR(50),
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		CommitmentDate  Date not null,
		Req_ReceivedByDate BIT NOT NULL DEFAULT 1,
		RewardType varchar(50) NOT NULL DEFAULT '',
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC,
			Retailercode ASC,
			Farmcode ASC
		)
	)
		
	DECLARE @FARMCODES TABLE (
		Farmcode VARCHAR(20),
		PRIMARY KEY CLUSTERED (
			Farmcode ASC
		)
	)

	DECLARE @REWARD_PRECENTAGE FLOAT=0.03;
	DECLARE @CUT_OFF_DATE DATE = CAST('2020-02-29' AS DATE)

	-- COMMITMENT REWARD
	-- USE CASE	
	INSERT INTO @CommitmentReward(Statementcode,MarketLetterCode,RetailerCode,Farmcode,CommitmentDate)
	SELECT ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode
		,MIN(CAST(C.CommitmentDate AS DATE)) AS CommitmentDate			
	FROM @STATEMENTS ST
		INNER JOIN (
			SELECT StatementCode 
			FROM RP_Statements 
			WHERE Season=@SEASON AND VersionType='USE CASE' AND StatementType=@STATEMENT_TYPE
		) RS
		ON RS.Statementcode=ST.Statementcode
		INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Commitments C 			ON ST.StatementCode=C.StatementCode 		
		LEFT JOIN (
			SELECT Statementcode, Farmcode
			FROM RP_DT_Commitments 
			WHERE CommitmentType='Lentils' AND [Status]='Complete' AND BASFSeason BETWEEN 2015 AND @SEASON-1 
			GROUP BY Statementcode, Farmcode
		) CLY 		
		ON ST.StatementCode=CLY.StatementCode  AND CLY.Farmcode=C.Farmcode
	WHERE MLP.ProgramCode=@PROGRAM_CODE AND c.[Status]='Complete' AND C.BASFSeason=@SEASON AND c.CommitmentType='Lentils'  
		AND CLY.StatementCode IS NULL		
	GROUP BY ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode


	-- UNLOCKED	(NON USE-CASE)
	INSERT INTO @CommitmentReward(Statementcode,MarketLetterCode,RetailerCode,Farmcode,CommitmentDate)
	SELECT ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode
		,MIN(CAST(C.CommitmentDate AS DATE)) AS CommitmentDate			
	FROM @STATEMENTS ST
		INNER JOIN (
			SELECT StatementCode 
			FROM RP_Statements 
			WHERE Season=@SEASON AND VersionType='UNLOCKED' AND StatementType=@STATEMENT_TYPE
		) RS
		ON RS.Statementcode=ST.Statementcode
		INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Commitments C 			ON ST.StatementCode=C.StatementCode 		
		LEFT JOIN (
			SELECT DISTINCT WC.[Parentcode] AS Farmcode 			
			FROM WebCommitments WC							
			WHERE WC.CommitmentType='Lentils' AND WC.[Status]='Complete' AND WC.Season BETWEEN 2015 AND @SEASON-1 
				AND WC.CommitmentDate IS NOT NULL														
		) CLY 		
		ON CLY.Farmcode=C.Farmcode
	WHERE MLP.ProgramCode=@PROGRAM_CODE AND c.[Status]='Complete' AND C.BASFSeason=@SEASON AND c.CommitmentType='Lentils'  
		AND CLY.Farmcode IS NULL		
	GROUP BY ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode
	   
	UPDATE @CommitmentReward	
	SET RewardType=IIF(CommitmentDate <= @CUT_OFF_DATE,'Early Commitment','Late Commitment')

	UPDATE @CommitmentReward	
	SET Reward=IIF(CommitmentDate <= @CUT_OFF_DATE,200,100)
--	WHERE Req_ReceivedByDate=1  -- RP-2292 
		

	-- MATCHING CLEARFIELD HERBICIDES REWARD
	/*
	-- these are taken from 2019 prices, i'll just assume for now it's the same order for 2020
	SOLO ULTRA			$20.63825
	ODYSSEY ULTRA NXT	$20.07975
	ODYSSEY ULTRA		$18.92875
	SOLO ADV			$17.483
	ODYSSEY NXT			$16.581625
	SOLO				$15.4575
	ODYSSEY				$14.935	


		---------------------------------
	2020
	20.01125	20.63	SOLO ULTRA	SOLO ULTRA
	19.47	20.0725	ODYSSEY ULTRA NXT	ODYSSEY ULTRA NXT
	17.99375	18.55	ODYSSEY ULTRA	ODYSSEY ULTRA
	16.97	16.97	SOLO ADV	SOLO ADV
	15.6025	16.085	SOLO ADV	SOLO ADV
	14.7975	15.255	ODYSSEY NXT	ODYSSEY NXT
	14.694375	15.14875	SOLO	SOLO
	14.694375	15.14875	MIZUNA	MIZUNA
	14.196875	14.63625	ODYSSEY 160 Acre Carton	ODYSSEY
	14.196875	14.63625	DUET	DUET


	*/

	-- TEMP HERBICIDE SALES
	DROP TABLE IF EXISTS #TEMP_HERB_SALES

	SELECT ST.Statementcode, S.VersionType, S.StatementLevel, S.Level5Code, GR.MarketLetterCode, S.Retailercode, tx.FarmCode, tx.ChemicalGroup ,tx.Acres AS [Acres]		
		,IIF(S.StatementLevel='LEVEL 5',tx.Price_SWP,tx.Price_SDP) AS [Sales]										
		,CAST(CASE WHEN PR.ConversionW=0 THEN 0	ELSE IIF(S.StatementLevel='LEVEL 5',PRP.SWP,PRP.SDP)/PR.ConversionW END AS MONEY) AS [PPA]
		,TX.RetailerCode AS EffectiveRetailerCode
	INTO #TEMP_HERB_SALES
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements S				ON TX.StatementCode=S.StatementCode
		INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
		INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID 			
		INNER JOIN ProductReference	PR			ON PR.ProductCode=TX.ProductCode
		INNER JOIN ProductReferencePricing PRP	ON TX.ProductCode=PRP.ProductCode AND PRP.Season=@SEASON AND PRP.Region='WEST'
	WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD LENTILS'		

	DELETE FROM #TEMP_HERB_SALES WHERE Level5Code <> 'D520062427' AND ChemicalGroup IN ('DUET','MIZUNA')


	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT tx.Statementcode, tx.MarketLetterCode, @PROGRAM_CODE, tx.EffectiveRetailerCode, tx.Farmcode,SUM(tx.Sales) AS Sales, 1		
	FROM #TEMP_HERB_SALES tx					
	WHERE tx.StatementLevel IN ('LEVEL 2','LEVEL 5')
	GROUP BY tx.Statementcode, tx.MarketLetterCode, tx.EffectiveRetailerCode ,tx.Farmcode


	DROP TABLE IF EXISTS #Herbicides

	SELECT Statementcode, VersionType, Level5Code, MarketLetterCode, Retailercode, FarmCode, ChemicalGroup ,PPA		
		,SUM(Sales) AS [Sales]						
		,SUM(Acres) AS [Acres]			
		,CAST(0 AS DECIMAL(18,2)) AS MatchedAcres
		,CAST(0 AS MONEY) AS MatchedSales
		,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
		,CAST(0 AS MONEY) AS Reward
		,ROW_NUMBER() OVER(PARTITION BY [StatementCode] ORDER BY [PPA] DESC) AS [MatchingOrder]	
	INTO #Herbicides
	FROM #TEMP_HERB_SALES D
	/*
	FROM (
		SELECT ST.Statementcode, S.VersionType, S.Level5Code, GR.MarketLetterCode, S.Retailercode, tx.FarmCode, tx.ChemicalGroup ,tx.Acres AS [Acres]		
			,IIF(S.StatementLevel='LEVEL 5',tx.Price_SWP,tx.Price_SDP) AS [Sales]										
			,CAST(CASE WHEN PR.ConversionW=0 THEN 0	ELSE IIF(S.StatementLevel='LEVEL 5',PRP.SWP,PRP.SDP)/PR.ConversionW END AS MONEY) AS [PPA]
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements S				ON TX.StatementCode=S.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
			INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID 			
			INNER JOIN ProductReference	PR			ON PR.ProductCode=TX.ProductCode
			INNER JOIN ProductReferencePricing PRP	ON TX.ProductCode=PRP.ProductCode AND PRP.Season=@SEASON AND PRP.Region='WEST'
		WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD LENTILS'					
	) D	
	*/
	GROUP BY Statementcode, VersionType, Level5Code, MarketLetterCode, Retailercode, FarmCode, ChemicalGroup, PPA   
	   	 
	INSERT INTO @FARMCODES(Farmcode)
	SELECT DISTINCT Farmcode FROM #Herbicides

	-- CLEARFIELD LENTIL COMMITED ACRES	
	-- Statementcode INT,
	 DECLARE @Lentils TABLE (		
		Farmcode VARCHAR(20) NOT NULL,
		Acres Decimal(18,2) NOT NULL
	 )

	-- use case we refresh one at a time	
	IF EXISTS(SELECT * FROM #Herbicides WHERE VERSIONTYPE='USE CASE')	
		INSERT INTO @Lentils(Farmcode, Acres)
		SELECT Farmcode, SUM(Acres) Acres
		FROM (
			SELECT Farmcode, ProductCode, MAX(Acres) Acres
			FROM (
				SELECT WC.Farmcode, WCV.ProductCode, WCV.Acres ,WCV.RegistrationDate
				FROM @STATEMENTS ST			
					INNER JOIN RP_DT_Commitments WC					ON WC.StatementCode=ST.Statementcode 
					INNER JOIN RP_DT_CommitmentsVarietyInfo WCV 	ON WCV.StatementCode=ST.Statementcode AND WCV.RP_WC_ID=WC.RP_ID
					INNER JOIN @Farmcodes F							ON WC.Farmcode=F.Farmcode												
				WHERE WC.CommitmentType='Lentils'  AND WC.[Status]='Complete' AND WCV.BASFSeason=@SEASON 
			) CL
			WHERE  CAST(RegistrationDate AS DATE) <= CAST('2020-07-09' AS DATE)
			GROUP BY Farmcode, ProductCode		
		) D
		GROUP BY Farmcode		
	ELSE
		INSERT INTO @Lentils(Farmcode, Acres)
		SELECT Farmcode, SUM(Acres) Acres
		FROM (
			SELECT Farmcode, ProductCode, MAX(Acres) Acres
			FROM (
				SELECT WC.[Parentcode] AS Farmcode, WCV.ProductCode, WCV.Acres 					
					,IIF(ISDATE(WCV.RegistrationDate)=1,WCV.RegistrationDate,NULL) AS RegistrationDate
				FROM @Farmcodes F
					INNER JOIN WebCommitments WC				ON WC.Parentcode=F.Farmcode
					INNER JOIN WebCommitmentsVarietyInfo WCV 	ON WCV.DocCode=WC.DocCode
				WHERE WC.CommitmentType='Lentils' AND WC.[Status]='Complete' AND WCV.Season=@SEASON AND WCV.RegistrationDate IS NOT NULL 
			) CL
			WHERE CAST(RegistrationDate AS DATE) <= CAST('2020-07-09' AS DATE)
			GROUP BY Farmcode, ProductCode
		) D
		GROUP BY Farmcode
		
	-- DUET AND MIZUNA ARE APPLICABLE FOR NUTRIEN ONLY  D520062427
	-- Taken care earlier
	--	DELETE FROM #Herbicides WHERE Level5Code <> 'D520062427' AND ChemicalGroup IN ('DUET','MIZUNA')
	
	   	  
	-- LETS DO HERBICIDE MATCHING
	UPDATE T1
	SET MatchedAcres=IIF(T2.[RunningTotal] <= CLL.Acres, T1.Acres,T1.Acres+(CLL.[Acres]-T2.[RunningTotal]))
	FROM #Herbicides T1
		INNER JOIN @Lentils CLL		
		ON CLL.FarmCode=T1.FarmCode 
		INNER JOIN (
			SELECT [StatementCode],[RetailerCode],[Farmcode],[ChemicalGroup],[MatchingOrder]
				,SUM(Acres) OVER(PARTITION BY [StatementCode],[RetailerCode],[Farmcode] ORDER BY [MatchingOrder] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
			FROM #Herbicides
		) T2
		ON T2.Statementcode=T1.StatementCode AND T2.RetailerCode=T1.RetailerCode AND T2.FarmCode=T1.FarmCode AND T2.ChemicalGroup=T1.ChemicalGroup AND T2.MatchingOrder=T1.MatchingOrder
	WHERE (T2.[RunningTotal] <= CLL.Acres  OR T1.Acres+(CLL.[Acres]-T2.[RunningTotal]) > 0)
				
	-- UPDATE MATCHING SALES, REWARD PERCENTAGE AND REWARD
	UPDATE #Herbicides SET [MatchedSales]=[Sales] * ([MatchedAcres]/[Acres]) WHERE [Acres] > 0
	UPDATE #Herbicides SET [Reward_Percentage]=@REWARD_PRECENTAGE,  [Reward]=[MatchedSales] * @REWARD_PRECENTAGE	WHERE [MatchedSales] > 0

	-- LETS INSERT DATA INTO TABLES FROM TEMP TABLES		
	DELETE FROM RP2020_DT_Rewards_W_CLL_Comm_Actual WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2020_DT_Rewards_W_CLL_Comm_Actual(Statementcode, MarketLetterCode, ProgramCode, Retailercode, Farmcode, CommitmentDate, Req_ReceivedByDate, RewardType, Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode,  CommitmentDate, Req_ReceivedByDate, RewardType, Reward
	FROM @CommitmentReward

	DELETE FROM RP2020_DT_Rewards_W_CLL_Herb_Actual WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CLL_Herb_Actual(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,MatchingOrder,Acres,Sales,MatchedAcres,MatchedSales,Reward_Percentage,Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode, ChemicalGroup
		,MIN(MatchingOrder) AS MatchingOrder
		,SUM(Acres) AS Acres
		,SUM(Sales) AS Sales
		,SUM(MatchedAcres) AS MatchedAcres
		,SUM(MatchedSales) AS MatchedSales
		,MAX(Reward_Percentage) Reward_Percentage
		,SUM(Reward) AS Reward
	FROM #Herbicides
	GROUP BY StatementCode, MarketLetterCode, RetailerCode, FarmCode, ChemicalGroup
	
		UNION

	SELECT T2.Statementcode, T2.MarketLetterCode, @PROGRAM_CODE,  T2.RetailerCode, T1.FarmCode,'CLEARFIELD LENTILS' AS RewardBrand, 0 AS MatchingOrder, Acres, 0 as Sales, 0 as MatchedAcres, 0 as MatchedSales, 0 as Reward_Percentage, 0 as Reward
	FROM @Lentils T1
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, RetailerCode, FarmCode FROM #Herbicides 
		) T2
		ON T2.FarmCode=T1.FarmCode





	   
END
