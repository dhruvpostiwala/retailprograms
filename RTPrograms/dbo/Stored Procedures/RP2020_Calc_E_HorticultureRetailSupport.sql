﻿


CREATE   PROCEDURE [dbo].[RP2020_Calc_E_HorticultureRetailSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
		Planned Growth: (%) 2020 POG Plan ($) relative to 2019/2018 2-Year Avg POG Sales ($)
		Qualify on a whole bunch of brands
		Reward on Centurion - above 2M qualifies <=100% = 1%, >100% = 3%, below 2M qualifies <=100% = 1%, >100% = 2%
		Reward on Liberty   - above 2M qualifies <=100% = 1%, >100% = 3%, below 2M qualifies <=100% = 1%, >100% = 2%
	*/

	IF @STATEMENT_TYPE <> 'ACTUAL'
	RETURN;

	DECLARE @EPSILON DECIMAL(4,4)=dbo.SVF_GetEpsilon();
	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	
	DECLARE @TEMP TABLE (
		StatementCode INT,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand_ElgSales_CY Money NOT NULL,
		RewardBrand_ActSales_CY Money NOT NULL,
		RewardBrand_ActSales_LY Money NOT NULL,
		total_basf_sale Money NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Reward_Percentage] DECIMAL(18,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand_ElgSales_CY Money NOT NULL,
		RewardBrand_ActSales_CY Money NOT NULL,
		RewardBrand_ActSales_LY Money NOT NULL,
		total_basf_sale Money NOT NULL DEFAULT 0,
		Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Reward_Percentage] DECIMAL(18,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_ElgSales_CY,RewardBrand_ActSales_CY,RewardBrand_ActSales_LY)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode
	,SUM(Price_E_SRP_CY) as RewardBrand_ElgSales_CY
	,SUM(Price_Q_SRP_CY) as RewardBrand_ActSales_CY
	,SUM(Price_Q_SRP_LY1) as RewardBrand_ActSales_LY
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode

	UPDATE T1
		SET total_basf_sale = sales.Price_SRP
		FROM @TEMP T1
		INNER JOIN
			(
				SELECT st.STATEMENTCODE, SUM(Price_SRP) as Price_SRP
				FROM RP_DT_TRANSACTIONS RS
				INNER JOIN @STATEMENTS ST
				ON ST.STATEMENTCODE = RS.STATEMENTCODE
				WHERE BasfSEASON = @SEASON
				GROUP BY st.STATEMENTCODE 
			) sales
		ON sales.STATEMENTCODE = T1.STATEMENTCODE

		-- Determine growth based on the sum of pursuit acres at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_ElgSales_CY,RewardBrand_ActSales_CY, RewardBrand_ActSales_LY,total_basf_sale)
	SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode
		,SUM(RewardBrand_ElgSales_CY) AS RewardBrand_ElgSales_CY
		,SUM(RewardBrand_ActSales_CY) AS RewardBrand_ActSales_CY
		,SUM(RewardBrand_ActSales_LY) AS RewardBrand_ActSales_LY
		,SUM(total_basf_sale) AS total_basf_sale
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	UPDATE @TEMP_TOTALS
	SET Growth_Percentage = IIF(RewardBrand_ActSales_LY > 0, RewardBrand_ActSales_CY / RewardBrand_ActSales_LY, 9.999)

	-- Update the reward %
	DECLARE @totalSalesThreshold INT = 100000
	DECLARE @totalRewardBrandSalesThreshold INT = 75000

	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.01 
	WHERE total_basf_sale >= @totalSalesThreshold 
		AND RewardBrand_ActSales_CY >= @totalRewardBrandSalesThreshold 
		AND (Growth_Percentage + @ROUNDUP_VALUE) >= 1.05

	-- Update the growth % and reward % back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth_Percentage = T2.Growth_Percentage, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward % back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth_Percentage = T2.Growth_Percentage, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0

	UPDATE T1
	SET T1.Reward = RewardBrand_ElgSales_CY * T1.Reward_Percentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode


	DELETE FROM [RP2020_DT_Rewards_E_HortiCultureRetailSupport] WHERE StatementCode IN (Select StatementCode From @Statements)

	DELETE FROM @TEMP WHERE Reward = 0

	INSERT INTO [RP2020_DT_Rewards_E_HortiCultureRetailSupport](Statementcode, MarketLetterCode, ProgramCode, RewardBrand_ElgSales_CY, RewardBrand_ActSales_CY, RewardBrand_ActSales_LY, total_basf_sale, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand_ElgSales_CY, RewardBrand_ActSales_CY, RewardBrand_ActSales_LY, total_basf_sale, Reward_Percentage, Reward
	FROM @TEMP
	UNION
	SELECT DataSetCode,MarketLetterCode,ProgramCode
		,SUM(RewardBrand_ElgSales_CY) AS RewardBrand_ElgSales_CY
		,SUM(RewardBrand_ActSales_CY) AS RewardBrand_ActSales_CY
		,SUM(RewardBrand_ActSales_LY) AS RewardBrand_ActSales_LY
		,SUM(total_basf_sale) AS total_basf_sale
		,MAX(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0 --AND Reward > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode
END


