﻿

CREATE PROCEDURE [dbo].[RPUC_Agent_UpdateCommitmentVarities] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	/*
		BATCH ACTION:
		{"created":[]
		,"updated":[{"rp_id":113661,"rp_wc_id":891496,"basfseason":2015,"productcode":"PRD-MMAN-7SHQ46","acres":500,"product":{"productcode":"PRD-MMAN-7SHQ46","productname":"CLL CDC IMIGREEN (IBC-145) ACRES"},"registrationdate":null}]
		,"destroyed":[]}

		{
			"rp_id":113661,
			"rp_wc_id":891496,
			"basfseason":2015,
			"productcode":"PRD-MMAN-7SHQ46",
			"acres":500,
			"product":{"productcode":"PRD-MMAN-7SHQ46","productname":"CLL CDC IMIGREEN (IBC-145) ACRES"},
			"registrationdate":null
		}

	*/
		
	BEGIN TRY

		IF OBJECT_ID('tempdb..#Temp_ToDelete') IS NOT NULL DROP TABLE #Temp_ToDelete
		IF OBJECT_ID('tempdb..#Temp_ToInsert') IS NOT NULL DROP TABLE #Temp_ToInsert
		IF OBJECT_ID('tempdb..#Final_DataToInsert') IS NOT NULL DROP TABLE #Final_DataToInsert
		
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @StatementCode AS Statementcode, t1.RP_ID ,t1.RP_WC_ID
		INTO #Temp_ToDelete
		FROM (
			SELECT b.rp_id, b.rp_wc_id
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						rp_id INT N'$.rp_id',					
						rp_wc_id INT N'$.rp_wc_id'			
					) as b	

				UNION ALL

			SELECT b.rp_id, b.rp_wc_id
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						rp_id INT N'$.rp_id',					
						rp_wc_id INT N'$.rp_wc_id'			
					) as b		
		) T1			


		-- ALL STATEMENTS TO BE INSERTED (INCLUDING UPDATED ONES FROM JSON)
		SELECT @StatementCode AS Statementcode ,t1.*
		INTO #Temp_ToInsert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (																			
							RP_WC_ID int N'$.rp_wc_id',
							BASFSeason int N'$.basfseason',
							Productcode varchar(50) N'$.product.productcode',
							Acres numeric(18,2) N'$.acres',
							RegistrationDate datetime N'$.registrationdate'			
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								RP_WC_ID int N'$.rp_wc_id',
								BASFSeason int N'$.basfseason',
								Productcode varchar(50) N'$.product.productcode',
								Acres numeric(18,2) N'$.acres',
								RegistrationDate datetime N'$.registrationdate'						
							) as b
		) T1
	

		IF EXISTS(SELECT * FROM #Temp_ToDelete)
		BEGIN
			DELETE T1
			FROM RP_DT_CommitmentsVarietyInfo T1
				INNER JOIN #Temp_ToDelete T2
				ON T2.StatementCode=T1.StatementCode AND T2.RP_ID=T1.RP_ID AND T2.RP_WC_ID=T1.RP_WC_ID
		END 

		IF NOT EXISTS(SELECT * FROM #Temp_ToInsert)
			GOTO FINAL
			
		
		/*  
			Note: PRIMARY KEY (UNIQUE)			
			[Statementcode]
			[RP_WC_ID] [int]
			[RP_ID] [int]
			
			Lets Regenerate value for column: [RP_ID]
			
			STEP #1: COLLECT DATA FROM ACTUAL TABLE INTO TEMPORARY TABLE. SINCE WE ARE GOING TO UPDATE 'ID' COLUMN, NO NEED TO FETCH IT 				
			STEP #2: DELETE RECORDS FROM ACTUAL TABLE WHERE STATEMENTCODE=@STATEMENTCODE
			STEP #3: INSERT RECORDS BACK INTO ACTUAL TABLE FROM TEMPORARY TABLE. USE 'ROW_NUMBER()' FUNCTION TO INSERT VALUE INTO 'ID' COLUMN
		*/

				
		-- STEP #1: Collect data into temporary table #ALLSales_To_Insert
		SELECT Statementcode,RP_WC_ID,BASFSeason,ProductCode,Acres,RegistrationDate
			,ROW_NUMBER() OVER(ORDER BY BASFSeason, RegistrationDate) AS RP_ID
		INTO #Final_DataToInsert
		FROM (
			SELECT Statementcode,RP_WC_ID,BASFSeason,ProductCode,Acres,RegistrationDate
			FROM RP_DT_CommitmentsVarietyInfo 
			WHERE StatementCode=@StatementCode AND RP_WC_ID IN (SELECT RP_WC_ID FROM #Temp_ToInsert)
		
				UNION ALL

			SELECT Statementcode,RP_WC_ID,BASFSeason,ProductCode,Acres,RegistrationDate				
			FROM #Temp_ToInsert 


		) T1	
					

		-- STEP #2: DELETE RECORDS FROM  RP_DT_TRANSACTIONS
		DELETE RP_DT_CommitmentsVarietyInfo 
		WHERE StatementCode=@StatementCode AND RP_WC_ID IN (SELECT RP_WC_ID FROM #Temp_ToInsert)
		
		-- STEP #3: 
		INSERT INTO RP_DT_CommitmentsVarietyInfo(Statementcode,DataSetCode,RP_ID,RP_WC_ID,BASFSeason,ProductCode,Acres,RegistrationDate, ID)
		SELECT Statementcode,0 as DataSetCode,RP_ID,RP_WC_ID,BASFSeason,ProductCode,Acres,RegistrationDate
			,0 AS ID -- since this for use case
		FROM #Final_DataToInsert		
		
		
		-- NO COMMIMTMENTS IN THE EAST. NO NEED TO UPDATE LEVEL 2 STATEMENT
		
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'success' AS [Status], '' AS Error_Message
		
END

