﻿

CREATE PROCEDURE [dbo].[RP_RefreshEstimates] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @WEST_REGION BIT=0;
	

	DECLARE  @TARGET_STATEMENTS AS UDT_RP_STATEMENT;
	INSERT INTO @TARGET_STATEMENTS(Statementcode)
	SELECT Statementcode
	FROM RPWeb_Statements ST
	WHERE ST.[Season]=@SEASON AND ST.[Region]='WEST' AND ST.Level5Code='D000001' AND ST.[Statementcode] IN (SELECT Statementcode FROM @Statements)

	IF NOT EXISTS(SELECT *	FROM @TARGET_STATEMENTS)
		RETURN

	
	IF @SEASON=2021
	BEGIN
	
		EXEC [dbo].[RP_RefreshEstimate_W_HeatDistinctAmounts]  @SEASON, @TARGET_STATEMENTS
		EXEC [dbo].[RP_RefreshEstimate_W_IncentiveAmounts] @SEASON, @TARGET_STATEMENTS
		GOTO FINAL
	END
	
	IF @SEASON=2020 
	BEGIN 		
		EXEC [dbo].[RP_RefreshEstimateExceptions]  @SEASON, @TARGET_STATEMENTS
		-- EXEC [dbo].[RP2020_Estimate_W_HeatDistinctAmounts] @SEASON, @TARGET_STATEMENTS
		EXEC [dbo].[RP_RefreshEstimate_W_HeatDistinctAmounts]  @SEASON, @TARGET_STATEMENTS
		EXEC [dbo].[RP2020_Estimate_W_IncentiveAmounts] @SEASON, @TARGET_STATEMENTS
		EXEC [dbo].[RP2020_Estimate_W_MarginToolsMetrics] @SEASON, @TARGET_STATEMENTS	
		
		GOTO FINAL
	END
	

FINAL:
	EXEC [dbo].[RP_RefreshEstimatesRewardsSummary] @SEASON, @STATEMENTS 

END







