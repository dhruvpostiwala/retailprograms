﻿




CREATE PROCEDURE [dbo].[RP_Agent_UpdateRetailerReference] @SEASON INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM RP_RetailerReference WHERE Season=@Season
			
	INSERT INTO RP_RetailerReference(Season, Region, RetailerCode, RetailerType, Level2Code, Level5Code)
	SELECT @Season, Region, RetailerCode, RetailerType, Level2Code, Level5Code
	FROM RetailerProfile 
	WHERE Status IN ('Active','Inactive','Sold') AND RetailerType IN ('LEVEL 1','LEVEL 2') -- may be, include Level 5 in future
	
END
