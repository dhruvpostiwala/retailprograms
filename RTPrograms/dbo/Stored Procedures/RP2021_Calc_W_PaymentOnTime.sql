﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_PaymentOnTime] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales Money NOT NULL,
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @RewardPercentage FLOAT = 0.0075;

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel,SUM(tx.Price_CY) AS RewardBrand_Sales,SUM(tx.Price_CY * @RewardPercentage) AS Reward
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS', 'ALL_CPP_BRANDS')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel

	UPDATE @TEMP
	SET Reward_Percentage = @RewardPercentage


	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[Reward] = 0
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0


	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode
			AND EI.FieldCode= IIF(T1.MarketLetterCode = 'ML2021_W_INV', 'Radius_Percentage_InVigor', 'Radius_Percentage_CPP')
		WHERE [Reward] > 0
	END

	DELETE FROM RP2021_DT_Rewards_W_PaymentOnTime WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)
	
	INSERT INTO RP2021_DT_Rewards_W_PaymentOnTime(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward
	FROM @TEMP
END
