﻿



CREATE PROCEDURE [dbo].[RP2020_Estimate_W_UpdateTopUpAmount] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	

	/* UNLOCKED STATEMENTCODES ARE PASSED IN */

	DECLARE @TARGET_STATEMENTS AS TABLE(
		Statementcode INT NOT NULL,
		Locked_Statementcode INT NOT NULL,
		POG_Amount MONEY NOT NULL DEFAULT 0,
		Reward_Amount MONEY NOT NULL DEFAULT 0,
		Margin DECIMAL(18,4) NOT NULL DEFAULT 0
	)

	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	INSERT INTO @TARGET_STATEMENTS(Statementcode, Locked_Statementcode, POG_Amount, Reward_Amount)
	SELECT ST.Statementcode, LCK.Statementcode, ROUND(ISNULL(TX.POG_Amount,0),2), Round(ISNULL(Rew.Reward_Amount,0),2)
	FROM @STATEMENTS ST
		INNER JOIN RPWeb_Statements LCK 
		ON LCK.SRC_Statementcode=ST.statementcode				
		LEFT JOIN (
			SELECT Statementcode, Sum(Price_CY) POG_Amount  
			FROM RP2020Web_Sales_Consolidated 
			WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS' AND StatementCode < 0
			GROUP BY Statementcode
		) TX
		ON TX.StatementCode=LCK.Statementcode
		LEFT JOIN (
			SELECT Statementcode, SUM(ISNULL(Amount,0)) AS Reward_Amount
			FROM (
				SELECT RS.StatementCode, TotalReward AS Amount
				FROM RPWEB_Rewards_Summary  RS
					INNER JOIN RP_Config_Rewards_Summary cfg
					ON CFG.RewardCode=RS.RewardCode
				WHERE cfg.Season=@SEASON AND cfg.RewardType='Regular'  AND StatementCode < 0								
					UNION ALL
				SELECT Statementcode, Amount FROM RPWeb_Est_Incentives 
					UNION ALL				 
				SELECT Statementcode, Amount FROM RPWeb_Est_HeatDistinctTopUp
					UNION ALL				 
				SELECT Statementcode, Amount FROM RPWeb_Est_Exceptions
			) D
			GROUP BY Statementcode
		) REW
		ON REW.StatementCode=LCK.Statementcode 
	WHERE LCK.Season=@Season AND LCK.[Status]='Active'  AND LCK.StatementType='Actual' and  LCK.VersionType='Locked' 
		AND LCK.ChequeRunName='OCT RECON'
		AND LCK.Level5Code='D000001'

	UPDATE @TARGET_STATEMENTS	
	SET Margin = (ROUND(Convert(Decimal,Reward_Amount),4)/ROUND(Convert(Decimal,POG_Amount),4)) 
	WHERE POG_Amount > 0

	/*   	 
	UPDATE TU
	SET Amount = IIF(TU.[Percentage] <= ML.Margin , 0, (TU.[Percentage]-ML.Margin) * ML.POG_Amount)
	FROM RPWeb_Est_MarginTopUp TU
		INNER JOIN @TARGET_STATEMENTS ML
		ON ML.Locked_Statementcode=TU.Statementcode
	*/	
	

	UPDATE TU
	SET	StatementCode=ML.Locked_Statementcode
			,Amount = IIF(TU.[Percentage] <= ML.Margin , 0, ROUND((TU.[Percentage]-ML.Margin) * ML.POG_Amount,2))   
	OUTPUT inserted.SRC_StatementCode,inserted.SRC_StatementCode,'Margin Top Up Amount Calculated','Field','Amount',deleted.[Amount],inserted.[Amount] 
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	FROM RPWeb_Est_MarginTopUp TU
		INNER JOIN @TARGET_STATEMENTS ML
		ON ML.Statementcode=TU.SRC_StatementCode
	WHERE Amount <> IIF(TU.[Percentage] <= ML.Margin , 0, ROUND((TU.[Percentage]-ML.Margin) * ML.POG_Amount,2))   
	
	EXEC RP_GenerateEditHistory 'Statement', 'Margin Top Up Payment Amount Update', @EDIT_HISTORY 

END

