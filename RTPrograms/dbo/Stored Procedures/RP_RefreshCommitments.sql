﻿
CREATE PROCEDURE [dbo].[RP_RefreshCommitments] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	/*
	--FOR TESTING 
		DECLARE @SEASON INT = 2020	
		DECLARE @STATEMENTS AS UDT_RP_STATEMENT
		DECLARE @STATEMENT_TYPE VARCHAR(20) = 'Actual'
		INSERT INTO @STATEMENTS  SELECT StatementCode FROM RP_Statements WHERE VersionType='Unlocked' and StatementType = @STATEMENT_TYPE and Season = @SEASON
		EXEC [dbo].[RP_RefreshCommitments] @SEASON, @STATEMENT_TYPE, @STATEMENTS
	*/


	--DECLARE @evergreenStartYr INT  = 2015
	DECLARE @LENTILS_BUFFER_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-08' AS DATE)
	DECLARE @WHEAT_BUFFER_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-08' AS DATE)

	DECLARE @COMMITMENTS TABLE(
		[Statementcode] [int] NOT NULL,
		[DataSetCode]  [int] NOT NULL,
		[RP_WC_ID] [int] NOT NULL,
		[WC_ID] [int] NOT NULL,
		[WC_BASFSeason] [int] NOT NULL,
		[RetailerCode] [varchar](20) NOT NULL,   
		[FarmCode] [varchar](20) NOT NULL,   
		[CommitmentType] [varchar](50) NULL,
		[CommitmentDate] [date] NULL,
		[Status] [varchar](50) NULL,
		[DocCode] [varchar](20) NULL,		
		[WCV_ID] [int] NULL,		
		[WCV_BASFSeason] [int] NULL,
		[ProductCode] [varchar](50) NULL,
		[Acres] [numeric](8, 2) NULL,
		[RegistrationDate] [date] NULL
	)
		


	/*
	INSERT INTO @COMMITMENTS(Statementcode,DataSetCode,WC_ID,WC_BASFSeason,RetailerCode,Farmcode,CommitmentType,CommitmentDate,Status,DocCode,WCV_ID,WCV_BASFSeason,ProductCode,Acres,RegistrationDate, RP_WC_ID)
	SELECT RS.StatementCode, RS.DataSetCode, SRC.WC_ID, SRC.WC_BASFSeason, SRC.RetailerCode, SRC.Farmcode, SRC.CommitmentType, SRC.CommitmentDate, SRC.[Status], SRC.[DocCode]
		,SRC.WCV_ID ,SRC.WCV_BASFSeason, SRC.ProductCode, ISNULL(SRC.Acres,0) AS Acres, SRC.RegistrationDate	
		,DENSE_RANK() OVER(PARTITION BY RS.StatementCode ORDER BY  SRC.WC_ID) AS RP_WC_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT WC.WebCommitmentsID AS [WC_ID] ,WC.Season AS [WC_BASFSeason]	,WC.DealerCode AS [RetailerCode], WC.[Parentcode] AS Farmcode ,WC.CommitmentType 				
				,IIF(ISDATE(WC.CommitmentDate )=1,WC.CommitmentDate ,NULL)  CommitmentDate 
				,WC.[Status] ,WC.[DocCode]	,WCV.ID AS [WCV_ID]	,WCV.Season AS [WCV_BASFSeason]	,WCV.ProductCode ,WCV.Acres	
				,IIF(ISDATE(WCV.RegistrationDate)=1,WCV.RegistrationDate,NULL) AS RegistrationDate
			FROM WebCommitments WC
				INNER JOIN WebCommitmentsVarietyInfo WCV 	
				ON WCV.DocCode=WC.DocCode			
			WHERE WC.[Status]='Complete' AND WC.CommitmentDate IS NOT NULL			
				AND (
					 (WC.CommitmentType='Lentils' AND  WC.Season IN (@SEASON,@SEASON+1) )
						OR
					(WC.CommitmentType='Wheat' AND WC.Season=@SEASON)
				)				
		) SRC 
		ON SRC.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.Region='West' AND RS.VersionType <> 'USE CASE'	AND  CAST(SRC.CommitmentDate AS DATE) <=  @LENTILS_BUFFER_DATE --CAST('2020-10-08' AS DATE)	
	*/

	-- LENTILS COMMITMENTS
	INSERT INTO @COMMITMENTS(Statementcode,DataSetCode,WC_ID,WC_BASFSeason,RetailerCode,Farmcode,CommitmentType,CommitmentDate,Status,DocCode,WCV_ID,WCV_BASFSeason,ProductCode,Acres,RegistrationDate, RP_WC_ID)
	SELECT RS.StatementCode, RS.DataSetCode, SRC.WC_ID, SRC.WC_BASFSeason, SRC.RetailerCode, SRC.Farmcode, SRC.CommitmentType, SRC.CommitmentDate, SRC.[Status], SRC.[DocCode]
		,SRC.WCV_ID ,SRC.WCV_BASFSeason, SRC.ProductCode, ISNULL(SRC.Acres,0) AS Acres, SRC.RegistrationDate	
		-- ,DENSE_RANK() OVER(PARTITION BY RS.StatementCode ORDER BY  SRC.WC_ID) AS RP_WC_ID
		,0 AS RP_WC_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT WC.WebCommitmentsID AS [WC_ID] ,WC.Season AS [WC_BASFSeason]	,WC.DealerCode AS [RetailerCode], WC.[Parentcode] AS Farmcode ,WC.CommitmentType 				
				,IIF(ISDATE(WC.CommitmentDate )=1,WC.CommitmentDate ,NULL)  CommitmentDate 
				,WC.[Status] ,WC.[DocCode]	,WCV.ID AS [WCV_ID]	,WCV.Season AS [WCV_BASFSeason]	,WCV.ProductCode ,WCV.Acres	
				,IIF(ISDATE(WCV.RegistrationDate)=1,WCV.RegistrationDate,NULL) AS RegistrationDate
			FROM WebCommitments WC
				INNER JOIN WebCommitmentsVarietyInfo WCV 	
				ON WCV.DocCode=WC.DocCode			
			WHERE WC.[Status]='Complete' AND WC.CommitmentType='Lentils' AND WC.Season IN (@SEASON,@SEASON+1) AND WC.CommitmentDate IS NOT NULL 
		) SRC 
		ON SRC.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.Region='West' AND RS.VersionType <> 'USE CASE'	AND CAST(SRC.CommitmentDate AS DATE) <=  @LENTILS_BUFFER_DATE --CAST('2020-10-08' AS DATE)

	--WHEAT COMMITMENTS -- THERE IS NO VARIETY INFORMATION
	INSERT INTO @COMMITMENTS(Statementcode,DataSetCode,WC_ID,WC_BASFSeason,RetailerCode,Farmcode,CommitmentType,CommitmentDate,Status,DocCode,WCV_ID,WCV_BASFSeason,ProductCode,Acres,RegistrationDate, RP_WC_ID)
	SELECT RS.StatementCode, RS.DataSetCode, SRC.WC_ID, SRC.WC_BASFSeason, SRC.RetailerCode, SRC.Farmcode, SRC.CommitmentType, SRC.CommitmentDate, SRC.[Status], SRC.[DocCode]
		,NULL AS WCV_ID ,NULL AS WCV_BASFSeason, NULL AS ProductCode, NULL AS Acres, NULL AS RegistrationDate	
		--,DENSE_RANK() OVER(PARTITION BY RS.StatementCode ORDER BY  SRC.WC_ID) AS RP_WC_ID
		,0 AS RP_WC_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT WC.WebCommitmentsID AS [WC_ID] ,WC.Season AS [WC_BASFSeason]	,WC.DealerCode AS [RetailerCode], WC.[Parentcode] AS Farmcode ,WC.CommitmentType 				
				,IIF(ISDATE(WC.CommitmentDate )=1,WC.CommitmentDate ,NULL)  CommitmentDate ,WC.[Status] ,WC.[DocCode]					
			FROM WebCommitments WC				
			WHERE WC.[Status]='Complete' AND WC.CommitmentType='Wheat' AND WC.Season IN (@SEASON,@SEASON+1) AND WC.CommitmentDate IS NOT NULL 							
		) SRC 
		ON SRC.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.Region='West' AND RS.VersionType <> 'USE CASE'	AND CAST(SRC.CommitmentDate AS DATE) <=  @WHEAT_BUFFER_DATE 
	
	UPDATE T1
	SET RP_WC_ID=T2.RP_WC_ID
	FROM @COMMITMENTS T1
		INNER JOIN (
			SELECT StatementCode, WC_ID, CommitmentType
				,DENSE_RANK() OVER(PARTITION BY StatementCode ORDER BY  CommitmentType, WC_ID) AS RP_WC_ID
			FROM @COMMITMENTS
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.WC_ID=T1.WC_ID
		
	DELETE T1
	FROM RP_DT_Commitments T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'	

	DELETE T1
	FROM RP_DT_CommitmentsVarietyInfo T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'	

	   	 
	INSERT INTO [RP_DT_Commitments](Statementcode, DataSetCode, RP_ID, ID, BASFSeason, RetailerCode, Farmcode, CommitmentType, CommitmentDate, Status, DocCode)
	SELECT DISTINCT Statementcode, DataSetCode, RP_WC_ID, WC_ID, WC_BASFSeason, Retailercode,  Farmcode, CommitmentType, CommitmentDate, Status, DocCode 	
	FROM @COMMITMENTS
	
	INSERT INTO RP_DT_CommitmentsVarietyInfo(Statementcode, DataSetCode, RP_WC_ID, ID,BASFSeason,ProductCode,Acres,RegistrationDate, RP_ID)
	SELECT Statementcode, DataSetCode, RP_WC_ID, WCV_ID,WCV_BASFSeason,ProductCode,Acres,RegistrationDate
		,DENSE_RANK() OVER(PARTITION BY STATEMENTCODE, RP_WC_ID ORDER BY WCV_ID) AS RP_ID
	FROM @COMMITMENTS
	WHERE CommitmentType='LENTILS'

END
