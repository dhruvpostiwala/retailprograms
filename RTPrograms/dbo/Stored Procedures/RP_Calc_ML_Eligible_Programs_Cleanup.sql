﻿
CREATE PROCEDURE [dbo].[RP_Calc_ML_Eligible_Programs_Cleanup] @SEASON INT,  @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))

	-- SEASON 2020 , STATEMENT TYPE = ACTUAL
	-- TEMPORARY EXCEPTION FOR AUGUST CHEQUE RUN
	-- APPLICABLE TO WEST FCL/COOP AND LINE COMPANIES

	DECLARE @ENV AS VARCHAR(50) = db_name()
	
	--Get the current environment
	/*
	IF RIGHT(@ENV,3)='PRD' AND  @STATEMENT_TYPE = 'ACTUAL' AND  @SEASON = 2021
	BEGIN
		-- TEMPORARY EXCEPTION FOR AUGUST CHEQUE RUN
		-- APPLICABLE TO WEST FCL/COOP AND LINE COMPANIES
		DELETE T1
		FROM RP_DT_ML_ElG_Programs  T1
			INNER JOIN RP_Statements S
			ON S.StatementCode = T1.StatementCode
		WHERE S.Season=@SEASON AND S.StatementType='ACTUAL'  AND S.VersionType IN ('Unlocked','Use Case') 
			AND S.Level5Code <> 'D000001'
			AND S.Region = 'WEST'
			AND T1.Programcode <> 'RP' + @SEASON_STRING + '_W_CUSTOM_SEED_TREATING_REWARD' 			
	END
	 */ 

	IF @SEASON=2021
	BEGIN
		/*
		 For the Dicamba Tolerant Program
		 REQUIREMENT: 
		 LEVEL 1 STATEMENT - RETAILER SHOULD BE FROM MANITOBA 
		 LEVEL 2 STATEMENT - AT LEAST ONE RETAILER FROM THE OPERATION UNIT SHOULD BE FROM MANITOBA
		 OTHERWISE, LETS GET RID OF THIS PROGRAM FROM RP_DT_ML_ELG_Programs 
		*/
		
		DELETE MLP
		FROM RP_DT_ML_ELG_Programs MLP	
		WHERE MLP.ProgramCode = 'RP2021_W_DICAMBA_TOLERANT'  AND MLP.StatementCode NOT IN (
			SELECT ST.StatementCode
			FROM @STATEMENTS ST
				INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode = ST.StatementCode	
				INNER JOIN RetailerProfile RP			ON RP.Retailercode=MAP.RetailerCode
			WHERE RP.MailingProvince = 'MB'
			GROUP BY ST.StatementCode
		)
		
		/* ENSURE ONLY SOLLIO RETAILERS GET THIS REWARD */
		DELETE MLP
			FROM RP_DT_ML_ElG_Programs MLP
				INNER JOIN @STATEMENTS ST		ON ST.Statementcode = MLP.StatementCode
				INNER JOIN RP_STATEMENTS RS		ON RS.Statementcode = ST.StatementCode
				INNER JOIN RetailerProfile RP	ON RP.Retailercode = RS.RetailerCode
			WHERE RS.Region='East' AND MLP.ProgramCode='RP2021_E_SOLLIO_GROWTH_BONUS' AND MLP.MarketLetterCode = 'ML2021_E_CPP' 					
					AND (RS.Level5Code <> 'D0000111' OR RP.Level4Code <> 'D520169710' OR RP.MailingProvince <> 'QC')
	
	
		/* ONLY BC RETAILS GET THIS REWARD */
		DELETE MLP
			FROM RP_DT_ML_ElG_Programs MLP
				INNER JOIN @STATEMENTS ST		ON ST.Statementcode = MLP.StatementCode
				INNER JOIN RP_STATEMENTS RS		ON RS.Statementcode = ST.StatementCode
				INNER JOIN RetailerProfile RP	ON RP.RetailerCode = RS.RetailerCode
			WHERE RS.Region='East' AND MLP.ProgramCode='RP2021_E_IGNITE_LOYAL_SUPPORT' AND MLP.MarketLetterCode = 'ML2021_E_CPP' 
					AND RP.MailingProvince <> 'BC'
	

		--REMOVE RETAILERS WHO ARE NOT CUSTOM AERIAL APPLICATORS
		DELETE MLP
		FROM RP_DT_ML_ElG_Programs MLP		
			INNER JOIN @STATEMENTS S		ON S.StatementCode=MLP.StatementCode	
			--INNER JOIN RP_Statements RS		ON RS.StatementCode=S.StatementCode
			LEFT JOIN (
				SELECT ST.StatementCode
				FROM @STATEMENTS ST
					INNER JOIN RP_StatementsMappings MAP		
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT RetailerCode 
						FROM RetailerProfileAccountType 
						WHERE AccountType IN ('Custom Aerial App','Custom Aerial Applicator','Third Party Custom Aerial App')
						GROUP BY RetailerCode 
					) RAT
					ON RAT.RetailerCode=MAP.ActiveRetailerCode
				GROUP BY ST.StatementCode
			) CA
			ON CA.StatementCode=S.StatementCode
		WHERE MLP.PROGRAMCODE = 'RP2021_W_CUSTOM_AER_APP' AND MLP.MarketLetterCode = 'ML2021_W_CPP' 
			AND CA.StatementCode IS NULL
			--AND RS.StatementLevel <> 'LEVEL 5' 
				
			DELETE MLP
			FROM RP_DT_ML_ElG_Programs MLP
				INNER JOIN @STATEMENTS ST		ON ST.Statementcode = MLP.StatementCode
				INNER JOIN RP_STATEMENTS RS		ON RS.Statementcode = ST.StatementCode
			WHERE RS.Category='CUSTOM AERIAL' AND MLP.ProgramCode <> 'RP2021_W_CUSTOM_AER_APP'		

		
	END
	
	IF @SEASON = 2020 
	BEGIN
		/*			
		IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')				
		BEGIN
			-- DUE TO RP-2595,  REVERTING CONFIG MAPPING BETWEEN PROGRAMCODE AND MARKETLETETR TO THE STATE WHAT IT WAS AT THE TIME OF BUILDING FORECAST REWARDS
			DELETE RP_DT_ML_ElG_Programs
			WHERE Programcode='RP2020_W_EFF_REBATE' AND MarketLetterCode IN ('ML2020_W_ST_INC','ML2020_W_CER_PUL_SB')
		END
		*/	

		IF @STATEMENT_TYPE = 'ACTUAL'
		BEGIN
			--Rp-2454 This is now being rewarded on based on Mark Irons comments
			--DELETE RP_DT_ML_ElG_Programs
			--WHERE Programcode='RP2020_W_EFF_REBATE' AND MarketLetterCode='ML2020_W_INV'
					   
			DELETE MLP
			FROM RP_DT_ML_ElG_Programs MLP
				INNER JOIN @STATEMENTS ST		ON ST.Statementcode = MLP.StatementCode
				INNER JOIN RP_STATEMENTS RS		ON RS.Statementcode = ST.StatementCode
			WHERE RS.Region='East' AND MLP.ProgramCode='RP2020_E_SOLLIO_GROWTH_REWARD' AND MLP.MarketLetterCode = 'ML2020_E_CCP' 
					AND RS.Level5Code <> 'D0000111'

			--DELETE INVIGOR PROGRAMS WHEN THERE IS NO POG
			--RP-3112
			DELETE T1
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_ML_ELG_Programs T1 
				ON T1.StatementCode = ST.StatementCode
				LEFT JOIN (
					SELECT Statementcode
					FROM RP_DT_Transactions
					WHERE BASFSeason=@SEASON AND ChemicalGroup='INVIGOR'					
					GROUP BY StatementCode
				) TX
				ON TX.StatementCode=T1.StatementCode
			WHERE TX.Statementcode IS NULL AND T1.MarketLetterCode = 'ML2020_E_INV'

			DELETE MLP
			FROM RP_DT_ML_ElG_Programs MLP
				INNER JOIN @STATEMENTS ST		ON ST.Statementcode = MLP.StatementCode
				INNER JOIN RP_STATEMENTS RS		ON RS.Statementcode = ST.StatementCode
			WHERE RS.Category='CUSTOM AERIAL' AND MLP.ProgramCode <> 'RP2020_W_CUSTOM_AERIAL_APP_REW'			
		END
	END

END


