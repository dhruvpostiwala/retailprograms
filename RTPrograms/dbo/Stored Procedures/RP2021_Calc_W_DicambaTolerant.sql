﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_DicambaTolerant] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[MarketLetterCode] VARCHAR(50) NOT NULL,
		[ProgramCode] VARCHAR(50) NOT NULL,
		[RetailerCode] VARCHAR(20) NOT NULL,
		[FarmCode] VARCHAR(20) NOT NULL,
		[Engenia_Acres] DECIMAL(18,4) NOT NULL,
		[ViperADV_Acres] DECIMAL(18,4) NOT NULL,
		[EV_Matched_Acres] DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Nodulator_Acres] DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Nodulator_Matched_Acres] DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC
		)
	)

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		BEGIN
			-- Get data from sales consolidated
			--cant use for actual no farmcode 
			INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, Engenia_Acres, ViperADV_Acres)
			SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode, 'FXXXXX' [FarmCode],
				   SUM(CASE TX.GroupLabel WHEN 'ENGENIA' THEN CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Acres_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Acres_CY END ELSE 0 END) [Engenia_Acres],
				   SUM(CASE TX.GroupLabel WHEN 'VIPER ADV' THEN CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Acres_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Acres_CY END ELSE 0 END) [ViperADV_Acres]
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
				INNER JOIN RetailerProfile RP ON TX.RetailerCode = RP.RetailerCode
				LEFT JOIN RPWeb_User_EI_FieldValues EI	ON ST.StatementCode = EI.StatementCode AND TX.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'Radius_Percentage_CPP'
			WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS' AND RP.MailingProvince = 'MB'
			GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode

		END
	ELSE IF @STATEMENT_TYPE = 'ACTUAL'
		BEGIN
			INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, Engenia_Acres, ViperADV_Acres,Nodulator_Acres)
			SELECT ST.StatementCode, GR.MarketLetterCode, GR.ProgramCode, TX.RetailerCode AS RetailerCode, tx.[FarmCode]  
				   ,SUM(IIF(GR.GroupLabel = 'ENGENIA',TX.Acres,0)) AS [Engenia_Acres]
				   ,SUM(IIF(GR.GroupLabel = 'VIPER ADV',TX.Acres,0)) AS [ViperADV_Acres]
				   ,SUM(IIF(RP.Level5Code='D0000137' AND GR.GroupLabel = 'NODULATOR SCG',TX.Acres,0)) AS [Nodulator_Acres]
			FROM @STATEMENTS ST				
				INNER JOIN RP_DT_Transactions TX		ON ST.StatementCode = TX.StatementCode
				INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
				INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID 			
				INNER JOIN RetailerProfile RP			ON TX.RetailerCode = RP.RetailerCode
			WHERE GR.ProgramCode = @PROGRAM_CODE AND tx.BASFSeason=@SEASON AND GR.GroupType = 'REWARD_BRANDS' 
				AND RP.MailingProvince = 'MB' AND tx.InRadius = 1
				AND TX.FarmCode NOT IN ('F520020402','F520040993')
			GROUP BY ST.StatementCode, GR.MarketLetterCode, GR.ProgramCode, tx.RetailerCode, tx.[FarmCode]  

			-- OU AND HEAD OFFICE BREAK DOWN	
			DELETE T1
			FROM RP_DT_HO_BreakDownSummary T1
				INNER JOIN @STATEMENTS ST
				ON ST.Statementcode=T1.Statementcode
			WHERE T1.ProgramCode=@PROGRAM_CODE 

			INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales,FarmLevelSplit)
			SELECT ST.StatementCode, GR.MarketLetterCode, GR.ProgramCode, Tx.RetailerCode, tx.[FarmCode]   	
				   	,SUM(CASE 
						WHEN GR.GroupLabel='NODULATOR DUO SCG' AND S.Level5Code='D0000137' THEN tx.Price_SWP						
						WHEN GR.GroupLabel IN ('ENGENIA','VIPER ADV') THEN
							CASE 
								WHEN S.Level5Code IN ('D0000107','D520062427','D0000244') THEN tx.Price_SWP
								WHEN S.Level5Code='D0000117'THEN tx.Price_SRP 
								ELSE tx.Price_SDP 
							END						
						ELSE 0
						END
				) AS Sales 	
				,1 AS FarmLevelSplit
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements S				ON S.Statementcode=ST.StatementCode 
				INNER JOIN RP_DT_Transactions TX		ON ST.StatementCode = TX.StatementCode
				INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
				INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID 			
				INNER JOIN RetailerProfile RP			ON TX.RetailerCode = RP.RetailerCode
			WHERE S.StatementLevel IN ('LEVEL 2','LEVEL 5') AND tx.InRadius = 1 AND TX.BASFSeason = @SEASON
				AND GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType = 'REWARD_BRANDS'
				AND RP.MailingProvince = 'MB' 	
				AND TX.FarmCode NOT IN ('F520020402','F520040993')
			GROUP BY ST.StatementCode,GR.MarketLetterCode, GR.ProgramCode ,tx.RetailerCode ,tx.FarmCode
			-- END OF HEAD OFFICE BREAK DOWN
		END

	-- Get matched acres
	UPDATE @TEMP
	SET EV_Matched_Acres = IIF(Engenia_Acres < ViperADV_Acres,Engenia_Acres,ViperADV_Acres)

	UPDATE @TEMP
	SET Nodulator_Matched_Acres = IIF(EV_Matched_Acres <= Nodulator_Acres,EV_Matched_Acres,Nodulator_Acres)

	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- Factor in the manual input for the line item
		UPDATE T1
		SET T1.EV_Matched_Acres = T1.EV_Matched_Acres * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.ProgramCode = EI.ProgramCode AND EI.FieldCode = 'Matched_Engenia'
	END

	-- Calculate the reward for each statement ($2.00/acre)
	--D0000137 Richardson Has Nodulator SCG component $0.50/matched acre https: //kennahome.jira.com/browse/RP-4103
	UPDATE @TEMP
	SET Reward = (EV_Matched_Acres * 2.00) + (Nodulator_Matched_Acres * 0.50)
	WHERE EV_Matched_Acres > 0

	-- Copy the data from the temporary table into the permanent table
	DELETE FROM RP2021_DT_Rewards_W_DicambaTolerant WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_DicambaTolerant(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, Engenia_Acres, ViperADV_Acres, Matched_Acres, Nodulator_Acres, Nodulator_Matched_Acres,  Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, Engenia_Acres, ViperADV_Acres, EV_Matched_Acres, Nodulator_Acres, Nodulator_Matched_Acres, Reward
	FROM @TEMP

END






