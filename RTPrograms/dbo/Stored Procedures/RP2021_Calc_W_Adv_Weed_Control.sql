﻿

CREATE PROCEDURE [dbo].[RP2021_Calc_W_Adv_Weed_Control]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	-- APPLICABLE ONLY ON ACTUAL STATEMENTS
	IF @STATEMENT_TYPE <> 'ACTUAL' 
	RETURN

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'				

	DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		FarmCode VARCHAR(50) NOT NULL,
		[IMI_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Heat_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Matched_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,		
		[Reward_Per_Acre] [numeric](5, 2) DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0
	PRIMARY KEY CLUSTERED 	(
		[StatementCode] ASC,
		[MarketLetterCode] ASC,
		[ProgramCode] ASC,
		[FarmCode] ASC
		)
	)

	DECLARE @MINIMUM_ACRES  INT = 300;  
	DECLARE @REWARD_PER_ACRE  FLOAT = 1.25;

	DROP TABLE IF EXISTS #Config_Groups
	SELECT ChemicalGroup, ProductCode
		, IIF(ChemicalGroup IN ('HEAT','HEAT COMPLETE','HEAT LQ'),'REWARD_BRANDS','QUALIFYING_BRANDS') AS GroupType
	INTO #Config_Groups
	FROM ProductReference
	WHERE DocType=10 AND ChemicalGroup IN ('HEAT','HEAT COMPLETE','HEAT LQ','ODYSSEY NXT','ODYSSEY ULTRA','ODYSSEY ULTRA NXT','SOLO ADV','SOLO ULTRA','VIPER ADV')
		AND Product NOT IN ('HEAT LQ (PRE-HARVEST)', 'HEAT LQ BULK (PRE-HARVEST)','HEAT (PRE-HARVEST)')
	
	-- ONLY HEAT* PRE-SEED AND HEAT COMPLETE GETS REWARDED	
--	DELETE #Config_Groups WHERE  Product LIKE '%PRE-HARVEST%'

	-- https: //kennahome.jira.com/browse/RP-4104 -- NUTRIEN D520062427
	INSERT INTO @TEMP(StatementCode, MarketLetterCode,ProgramCode,FarmCode,IMI_Acres,Heat_Acres)
	SELECT ST.StatementCode,@CCP_ML_CODE,@PROGRAM_CODE,tx.FarmCode
		,SUM(IIF(GR.GroupType = 'QUALIFYING_BRANDS' ,tx.Acres,0)) AS [IMI_Acres]  
		,SUM(IIF(GR.GroupType = 'REWARD_BRANDS',tx.Acres,0)) AS [Heat_Acres]
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S				ON S.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode
		INNER JOIN #Config_Groups GR			ON GR.ProductCode = TX.ProductCode 		
	WHERE tx.InRadius = 1 AND TX.BASFSeason = @SEASON
		AND TX.FarmCode NOT IN ('F520020402','F520040993')
		AND (S.Level5Code = 'D520062427' OR GR.ChemicalGroup <> 'HEAT')				
	GROUP BY ST.StatementCode,tx.FarmCode

	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 
	
	-- ONLY HEAT* PRE-SEED AND HEAT COMPLETE GETS REWARDED	
	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales,FarmLevelSplit)
	SELECT ST.StatementCode,@CCP_ML_CODE,@PROGRAM_CODE, tx.RetailerCode ,tx.FarmCode		
		,[Sales] =SUM(	CASE 
				WHEN S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244') THEN tx.Price_SWP
				WHEN S.Level5Code='D0000117' THEN tx.Price_SRP 
				ELSE tx.Price_SDP 
			END)
		,[FarmLevelSplit] = 1
	FROM @STATEMENTS ST 
		INNER JOIN RP_Statements S				ON S.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode 
		INNER JOIN #Config_Groups GR			ON GR.ProductCode = TX.ProductCode 
	WHERE S.StatementLevel IN ('LEVEL 2','LEVEL 5') AND tx.InRadius = 1 AND tx.BASFSeason = @SEASON
		AND GR.GroupType= 'REWARD_BRANDS' 		
		AND TX.FarmCode NOT IN ('F520020402','F520040993')
		AND (S.Level5Code = 'D520062427' OR GR.ChemicalGroup <> 'HEAT')		
	GROUP BY ST.StatementCode,  tx.RetailerCode ,tx.FarmCode
	-- END OF HEAD OFFICE BREAKDOWN
		
	UPDATE @TEMP SET [Matched_Acres] = IIF(IMI_Acres <  [Heat_Acres], IMI_Acres,[Heat_Acres]) 
	--WHERE [IMI_Acres] >= @MINIMUM_ACRES AND [Heat_Acres] >= @MINIMUM_ACRES
	
	UPDATE @TEMP
	SET Reward_Per_Acre = @REWARD_PER_ACRE
	WHERE [Matched_Acres] > 0
	
	UPDATE @TEMP SET Reward = Matched_Acres *  Reward_Per_Acre WHERE Matched_Acres >= @MINIMUM_ACRES
		
	DELETE FROM [dbo].[RP2021_DT_Rewards_W_Adv_Weed_Control] WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO [dbo].[RP2021_DT_Rewards_W_Adv_Weed_Control]([StatementCode],  [MarketLetterCode], [ProgramCode], [FarmCode], [IMI_Acres],[Heat_Acres],[Matched_Acres], [Reward_Per_Acre], [Reward])
	SELECT StatementCode,[MarketLetterCode], [ProgramCode], [FarmCode],[IMI_Acres],[Heat_Acres],[Matched_Acres], Reward_Per_Acre,[Reward]
	FROM @TEMP
	
END

