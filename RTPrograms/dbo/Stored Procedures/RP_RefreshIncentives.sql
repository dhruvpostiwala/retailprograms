﻿

CREATE PROCEDURE [dbo].[RP_RefreshIncentives] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	
	DELETE T1
	FROM [dbo].[RP_DT_Incentives] T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'
	

	INSERT INTO [RP_DT_Incentives](StatementCode,RetailerCode, IncentiveCode, Amount, Region)
	SELECT ST.StatementCode, Incentives.RetailerCode, Incentives.IncentiveCode, Incentives.Amount, RS.Region
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN(
			SELECT IncentiveCode, payabletocode AS RetailerCode, SUM(Payment) AS Amount
			FROM dbo.incentivepayment p
				INNER JOIN dbo.incentive i			
				ON i.IncentiveCode=p.LinkToCode
			WHERE i.Season=@SEASON AND p.status = 'Paid to Retail Statement' and p.paymentmethod = 'Paid by Retail Statement'
			GROUP BY PayableToCode, i.IncentiveCode
		) Incentives
		ON Incentives.RetailerCode = MAP.ActiveRetailerCode
	WHERE RS.VersionType <> 'USE CASE'
END
