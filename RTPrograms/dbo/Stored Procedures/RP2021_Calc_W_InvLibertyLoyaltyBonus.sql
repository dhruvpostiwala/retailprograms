﻿

CREATE PROCEDURE [dbo].[RP2021_Calc_W_InvLibertyLoyaltyBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SEASON_STRING VARCHAR(4)=CAST(@Season AS VARCHAR(4))
	DECLARE @INV_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_INV'
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'

	DECLARE @TEMP TABLE (
		StatementCode int,
		Level5Code VARCHAR(50) NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,		
		Liberty_Acres NUMERIC(18,2) NOT NULL,
		Invigor_Acres NUMERIC(18,2) NOT NULL,
		Total_Invigor_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		Total_Reseeded_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		RewardBrand Varchar(100) NOT NULL,				
		RewardBrand_Sales MONEY NOT NULL,
		Bonus_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Bonus_Reward MONEY NOT NULL DEFAULT 0,
		Addendum_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Addendum_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Qualified BIT DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @HEATADDENDUM_QUALIFY TABLE (
		StatementCode INT NOT NULL,
		Part1 BIT NOT NULL DEFAULT 1,
		Part2 BIT NOT NULL DEFAULT 1,
		AddendumOn BIT NOT NULL DEFAULT 0
	)

	/*
		DROP TABLE IF EXISTS #SUMMARY
		SELECT ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode,TX.Level5Code,RP.StatementLevel
			,SUM(IIF(PR.ChemicalGroup = 'LIBERTY 150',IIF(@STATEMENT_TYPE IN ('FORECAST', 'PROJECTION'), tx.Acres_CY * (ISNULL(EI1.FieldValue, 100) / 100), tx.Acres_CY),0)) AS Liberty_Acres
			,SUM(IIF(PR.ChemicalGroup = 'LIBERTY 150',tx.Price_CY,0)) AS Liberty_Sales
			,SUM(IIF(PR.ChemicalGroup = 'INVIGOR',IIF(@STATEMENT_TYPE IN ('FORECAST', 'PROJECTION'), tx.Acres_CY * (ISNULL(EI2.FieldValue, 100) / 100), tx.Acres_CY),0)) AS Invigor_Acres
			,SUM(IIF(PR.ChemicalGroup = 'INVIGOR',tx.Price_CY,0)) AS Invigor_Sales
			,MAX(tx.GroupLabel) RewardBrand		
			-- Following two are need for Forecast and Projection
			,SUM(IIF(PR.ChemicalGroup = 'HEAT LQ',IIF(@STATEMENT_TYPE IN ('FORECAST', 'PROJECTION'), tx.Price_CY * (ISNULL(EI3.FieldValue, 100) / 100), tx.Price_CY),0)) AS Heat_CY_Sales
			,SUM(IIF(PR.ChemicalGroup = 'HEAT LQ',tx.Price_LY1,0)) AS Heat_LY_Sales
		INTO #SUMMARY
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_STATEMENTS RP						ON TX.StatementCode= RP.StatementCode
			INNER JOIN ProductReference PR on PR.ProductCode = tx.ProductCode
			LEFT JOIN RPWeb_User_EI_FieldValues EI1
			ON EI1.StatementCode=ST.StatementCode AND EI1.MarketLetterCode=TX.MarketLetterCode AND EI1.FieldCode = 'Radius_Percentage_Liberty'
			LEFT JOIN RPWeb_User_EI_FieldValues EI2
			ON EI2.StatementCode=ST.StatementCode AND EI2.MarketLetterCode=TX.MarketLetterCode AND EI2.FieldCode = 'Radius_Percentage_InVigor'
			LEFT JOIN RPWeb_User_EI_FieldValues EI3
			ON EI3.StatementCode=ST.StatementCode AND EI3.MarketLetterCode=TX.MarketLetterCode AND EI3.FieldCode = 'Radius_Percentage_CPP'
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='QUALIFYING_BRANDS'
		GROUP BY ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode,TX.Level5Code,RP.StatementLevel		
	*/


	DROP TABLE IF EXISTS #SUMMARY
	SELECT ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode,TX.Level5Code,RP.StatementLevel
		,SUM(IIF(PR.ChemicalGroup = 'LIBERTY 150',tx.Acres_CY,0)) AS Liberty_Acres
		,SUM(IIF(PR.ChemicalGroup = 'LIBERTY 150',tx.Price_CY,0)) AS Liberty_Sales
		,SUM(IIF(PR.ChemicalGroup = 'INVIGOR',tx.Acres_CY,0)) AS Invigor_Acres
		,SUM(IIF(PR.ChemicalGroup = 'INVIGOR',tx.Price_CY,0)) AS Invigor_Sales
		,MAX(tx.GroupLabel) RewardBrand		
		-- Following two are need for Forecast and Projection
		,SUM(IIF(PR.ChemicalGroup = 'HEAT LQ',tx.Price_CY,0)) AS Heat_CY_Sales
		,SUM(IIF(PR.ChemicalGroup = 'HEAT LQ',tx.Price_LY1,0)) AS Heat_LY_Sales
	INTO #SUMMARY
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_STATEMENTS RP						ON TX.StatementCode= RP.StatementCode
		INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode,TX.Level5Code,RP.StatementLevel

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		UPDATE T1
		SET Liberty_Acres = Liberty_Acres * (ISNULL(EI1.FieldValue, 100) / 100)
			,Invigor_Acres = Invigor_Acres * (ISNULL(EI2.FieldValue, 100) / 100)
			,Heat_CY_Sales = Heat_CY_Sales * (ISNULL(EI3.FieldValue, 100) / 100)
		FROM #SUMMARY T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI1
			ON EI1.StatementCode=T1.StatementCode AND EI1.MarketLetterCode=T1.MarketLetterCode AND EI1.FieldCode = 'Radius_Percentage_Liberty'
			LEFT JOIN RPWeb_User_EI_FieldValues EI2
			ON EI2.StatementCode=T1.StatementCode AND EI2.MarketLetterCode=T1.MarketLetterCode AND EI2.FieldCode = 'Radius_Percentage_InVigor'
			LEFT JOIN RPWeb_User_EI_FieldValues EI3
			ON EI3.StatementCode=T1.StatementCode AND EI3.MarketLetterCode=T1.MarketLetterCode AND EI3.FieldCode = 'Radius_Percentage_CPP'
	END

	INSERT INTO @TEMP(StatementCode,Level5Code,MarketLetterCode,ProgramCode,Liberty_Acres, Invigor_Acres,RewardBrand,RewardBrand_Sales)	
	SELECT ST.StatementCode, S.Level5Code, Elg.MarketLetterCode, Elg.ProgramCode as ProgramCode
		,MAX(S.Liberty_Acres) AS Liberty_Acres
		,MAX(S.Invigor_Acres) AS Invigor_Acres		
		,IIF(ELG.MarketLetterCode = @INV_ML_CODE, 'INVIGOR', 'LIBERTY 150') AS RewardBrand
		,MAX(IIF(ELG.MarketLetterCode = @INV_ML_CODE, S.Invigor_Sales, S.Liberty_Sales)) AS RewardBrand_Sales
	FROM @STATEMENTS ST 
		INNER JOIN RP_DT_ML_Elg_Programs ELG	ON ELG.Statementcode=ST.Statementcode
		INNER JOIN #SUMMARY S	ON S.Statementcode=ST.Statementcode
	WHERE ELG.ProgramCode=@PROGRAM_CODE
	GROUP BY ST.StatementCode, S.Level5Code, ELG.MarketLetterCode, Elg.ProgramCode, Level5Code, StatementLevel

	-- LETS ADJUST INVIGOR ACRES BY DEDUCTING RESEEEDED ACRES
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		UPDATE T1
		SET T1.Total_Reseeded_Acres = T2.Reseeded_Acres
			,T1.Total_Invigor_Acres = T1.Invigor_Acres
			,T1.Invigor_Acres = T1.Invigor_Acres-T2.Reseeded_Acres
		FROM @TEMP T1
			INNER JOIN (
				SELECT ST.StatementCode, SUM(APA.ProblemAcres) AS Reseeded_Acres
				FROM @STATEMENTS ST
					INNER JOIN [dbo].[RP_StatementsMappings] MAP
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT C.RetailerCode, SUM(CB.TotalProblemArea) AS ProblemAcres
						FROM Claims C
							LEFT JOIN Claims_BasicInfo CB
							ON C.ID = CB.Claim_ID
						WHERE ISNULL(C.SoftDeleted,'') <> 'Yes' AND C.Season=@SEASON AND C.NOI_Code IN ('W_INV_RESEED','E_INV_RESEED') 
						AND C.Status <> 'Draft' AND ISNULL(C.RetailerCode,'') <> '' AND CB.TotalProblemArea > 0    
						GROUP BY C.RetailerCode	
					) APA
					ON APA.RetailerCode=MAP.RetailerCode
				GROUP BY ST.Statementcode				
			) T2
			ON T2.Statementcode=T1.Statementcode
	END
	
	-- Update reward percentage if qualified
	UPDATE @TEMP 
	SET Qualified = 1
	WHERE Liberty_Acres > 0 AND Invigor_Acres > 0 AND Liberty_Acres >= Invigor_Acres

	-- INVIGOR REWARD PRECENTAGE for All Retailers
	UPDATE @TEMP 
	SET [Bonus_Percentage] = 0.02
	WHERE MarketLetterCode = @INV_ML_CODE AND Qualified = 1 
		
	-- Liberty Reward Percentage for RC/IND
	UPDATE @TEMP 
	SET [Bonus_Percentage] = 0.05
	WHERE MarketLetterCode = @CCP_ML_CODE AND Level5Code = 'D000001'  AND Qualified = 1 
					
	-- Liberty Reward Percentage for  FCL	
	UPDATE @TEMP 
	SET [Bonus_Percentage] = 0.0545
	WHERE MarketLetterCode = @CCP_ML_CODE AND Level5Code = 'D0000117'  AND Qualified = 1 
		
	-- Liberty Reward Percentage for  LINE COMPANIES	
	UPDATE @TEMP 
	SET [Bonus_Percentage] = 0.02
	WHERE MarketLetterCode = @CCP_ML_CODE AND Level5Code IN ('D0000107','D0000137','D0000244','D520062427')  AND Qualified = 1 
		
	UPDATE @TEMP 
	SET [Bonus_Reward]=[RewardBrand_Sales] * [Bonus_Percentage]
	WHERE [Bonus_Percentage] > 0
	   
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- Check whether each statement qualifies for the heat addendum (update "AddendumOn" to check retailer against filter once filter is available)
		INSERT INTO @HEATADDENDUM_QUALIFY(StatementCode, Part1, Part2)
		SELECT StatementCode,
			   IIF(SUM(Heat_LY_Sales) > 0, IIF(SUM(Heat_CY_Sales) / (SUM(Heat_LY_Sales)) >= 1.075, 0, 1), IIF(SUM(Heat_CY_Sales) > 0, 0, 1)) [Part1],
			   IIF(SUM(Liberty_Acres) >= (0.75 * SUM(InVigor_Acres)), 0, 1) [Part2]
		FROM #Summary
		GROUP BY StatementCode

		UPDATE HAQ
			SET AddendumOn = IIF(EI.FieldValue = 100, 1, 0)
		FROM @HEATADDENDUM_QUALIFY HAQ
			INNER JOIN RPWeb_user_EI_FieldValues EI
			ON HAQ.StatementCode = EI.StatementCode AND EI.FieldCode = 'Heat_Addendum'

		-- If the heat addendum is enabled for a retailer, and the retailer does not currently qualify for a reward, qualify the retailer for the reward
		UPDATE T1
		SET [Addendum_Percentage] = IIF(MarketLetterCode = @INV_ML_CODE, 0.02, IIF(Level5Code = 'D000001', 0.0500, 0.0545))
			,[Addendum_Reward] = [RewardBrand_Sales] * IIF(MarketLetterCode = @INV_ML_CODE, 0.02, IIF(Level5Code = 'D000001', 0.0500, 0.0545))
		FROM @TEMP T1
			INNER JOIN @HEATADDENDUM_QUALIFY T2
			ON T1.StatementCode = T2.StatementCode
		WHERE T2.Part1 = 0 AND T2.Part2 = 0 AND T2.AddendumOn = 1 AND T1.Bonus_Reward = 0
	END --END FORECAST PROJECTION


	-- Set the final reward to the sum of the bonus and addendum rewards
	UPDATE @TEMP
	SET [Reward] = [Bonus_Reward] + [Addendum_Reward]

	DELETE FROM RP2021_DT_Rewards_W_INV_LLB WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2021_DT_Rewards_W_INV_LLB(StatementCode,MarketLetterCode,ProgramCode,Liberty_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Bonus_Percentage,Bonus_Reward,Addendum_Percentage,Addendum_Reward,Reward,Total_Invigor_Acres,Total_Reseeded_Acres)
	SELECT StatementCode,MarketLetterCode,ProgramCode,Liberty_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Bonus_Percentage,Bonus_Reward,Addendum_Percentage,Addendum_Reward,Reward,Total_Invigor_Acres,Total_Reseeded_Acres
	FROM @TEMP
END


