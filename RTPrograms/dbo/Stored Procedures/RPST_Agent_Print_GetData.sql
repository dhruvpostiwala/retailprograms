﻿

CREATE PROCEDURE [dbo].[RPST_Agent_Print_GetData] @Season INT, @StatementCode INT,  @Language as VARCHAR(1) = NULL, @ReportHTML NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET ANSI_WARNINGS OFF;
	SET NOCOUNT ON;

	DECLARE @REGION VARCHAR(4);
	DECLARE @TEMP NVARCHAR(MAX) = '';
	DECLARE @RETAILERINFOHTML NVARCHAR(MAX) = '';
	DECLARE @StatementHTML NVARCHAR(MAX) = '';
	DECLARE @PROGRAM_TITLE VARCHAR(200)='';
	DECLARE @StatementLevel VARCHAR(30)='';
	DECLARE @LEVEL5CODE VARCHAR(20)='';
	DECLARE @CATEGORY VARCHAR(50)='';
	

	SELECT @REGION=REGION, @StatementLevel=StatementLevel , @LEVEL5CODE=Level5Code, @CATEGORY=Category
	FROM RPWeb_Statements 
	WHERE Statementcode=@STATEMENTCODE

	IF @Language = NULL OR @Language = ''
		SELECT @Language = IIF(RETAILERLANGUAGE='B', 'F', RETAILERLANGUAGE)
		FROM RetailerProfile RP
			INNER JOIN RPWEB_STATEMENTS RS
			ON RS.RETAILERCODE = RP.RETAILERCODE
		WHERE RS.STATEMENTCODE = @StatementCode

		---NEED TO BE ON EVERYPAGE
	DECLARE @HEADER_ROW NVARCHAR(MAX)='
		<table class = "rp_header_table">
				<tr class ="rp_bold">
					<td class="rp_prtint_title_td" style="text-align: left;">
						' + CASE WHEN @LANGUAGE = 'F' THEN 'Relevé des programmes pour les détaillants ' + CAST(@Season AS VARCHAR) ELSE CAST(@Season AS VARCHAR) + ' Retail Programs Statement' END + '
					</td>
					<td class="rp_prtint_title_td" style="text-align: right;">
							<img src="BASFLogoChemistry.jpg"/>
					</td>
				</tr>
		</table>
		'
	
	--RETAILER HEADER
	SELECT @RETAILERINFOHTML = (
		SELECT '<table class="rp_print_retailerinfo">
					<tbody>
						<tr class="rp_bold">
							<td>' + RetailerName + ' (' + ST.RetailerCode + ')' + '</td>' + --'<td class="rp_print_retailerinfo_rightcell">Phone: ' + Phone + '</td>
						'</tr>
						<tr class="rp_bold">
							<td>' + MailingAddress1 + '</td>' + --'<td class="rp_print_retailerinfo_rightcell">Fax: ' + Fax + '</td>
						'</tr>
						<tr class="rp_bold">
							<td>' + MailingCity + ', ' + MailingProvince + ' ' + MailingPostalCode + '</td>
						</tr>
					</tbody>
				</table>'
		FROM RPWeb_Statements ST
			INNER JOIN RetailerProfile RP
			ON RP.RetailerCode = ST.RetailerCode
		WHERE ST.StatementCode = @StatementCode
	);

	
	 SET @HEADER_ROW += @RETAILERINFOHTML
	

	--ADD STATEMENT SUMMARY, ML SUMMARY, POG REPORT, HEAD OFFICE BREAKDOWN TO THE PRINT MODE PAGE
	--HEADER ROW ON ALL PAGES
	EXEC [Reports].[RPWeb_Get_StatementSummary] @StatementCode, @Language, @temp OUTPUT;
	--SET @StatementHTML += @HEADER_ROW
	SET @StatementHTML += ISNULL(@temp,'');

	EXEC [Reports].[RPWeb_Get_ML_RewardSummary] @StatementCode, @Language, @temp OUTPUT;
	SET @StatementHTML += @HEADER_ROW
	SET @StatementHTML += ISNULL(@temp,'');

	EXEC [Reports].[RPWeb_Get_POG_Details] @StatementCode, @Language, @temp OUTPUT;
	SET @StatementHTML += @HEADER_ROW
	SET @StatementHTML += ISNULL(@temp,'');

	
	-- JIRA: RP-3315
	/*
	IF @LEVEL5CODE='D000001' AND @CATEGORY='REGULAR' AND @REGION='WEST' AND  @StatementLevel='LEVEL 2'
	BEGIN
		EXEC [Reports].[RPWeb_Get_HO_BreakDown] @StatementCode, @temp OUTPUT;
		SET @StatementHTML += @HEADER_ROW
		SET @StatementHTML += ISNULL(@temp,'');
	END
	*/
	

	--Add applicable programs to the print mode page
	--EACh Program has header row
	DECLARE @ProcedureName VARCHAR(255);
	DECLARE @ProgramCode VARCHAR(200);

	DECLARE ReportProcedure_Loop CURSOR FOR
		SELECT DISTINCT ProgramCode, Title, ReportStoredProcedureName
		FROM RP_Config_Programs P
			INNER JOIN [RPWeb_Rewards_Summary] R		
			ON P.RewardCode = R.RewardCode
		WHERE Season=@Season AND Region=@Region 
			AND R.StatementCode = @STATEMENTCODE
			AND ReportStoredProcedureName IS NOT NULL AND ReportStoredProcedureName <> ''
		ORDER BY P.Title;
	OPEN ReportProcedure_Loop;
	FETCH NEXT FROM ReportProcedure_Loop INTO @ProgramCode, @PROGRAM_TITLE, @ProcedureName;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @REGION = 'EAST'
			EXEC @ProcedureName @StatementCode, @ProgramCode, @Language, @temp OUTPUT;
		ELSE
			EXEC @ProcedureName @StatementCode, @ProgramCode, @temp OUTPUT;
		SET @StatementHTML += @HEADER_ROW
		SET @StatementHTML += ISNULL(@temp,'');
		FETCH NEXT FROM ReportProcedure_Loop INTO @ProgramCode, @PROGRAM_TITLE, @ProcedureName;
	END;
	CLOSE ReportProcedure_Loop;
	DEALLOCATE ReportProcedure_Loop;

	--IF @SEASON >= 2021
		SET @StatementHTML = STRING_ESCAPE(REPLACE(REPLACE(@StatementHTML,CHAR(13),''),CHAR(10),'') , 'json');

	SET @ReportHTML = ISNULL(@StatementHTML,'')

END
