﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_BrandSpecificSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	DECLARE @SEASON INT = 2021;
	DECLARE @STATEMENT_TYPE VARCHAR(50)
	DECLARE @PROGRAM_CODE VARCHAR(50)
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	*/

	DECLARE @ROUND_PERCENTAGE NUMERIC(6,4) = 0.005;


	DECLARE @Temp TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,						
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,	
		Level5Code Varchar(10) NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(19,4) NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		INSERT INTO @Temp(StatementCode,MarketLetterCode,ProgramCode,RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2,Level5Code)
		SELECT ST.StatementCode, tx.MarketLetterCode,tx.ProgramCode
			,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
			,SUM(CASE
					WHEN tx.GroupType='QUALIFYING_BRANDS' THEN
						CASE
							WHEN tx.GroupLabel IN ('COTEGRA', 'LANCE', 'LANCE AG') THEN (tx.Price_CY * 0.50)
							WHEN tx.GroupLabel = 'CARAMBA' THEN (tx.Price_CY * 1.25)
							ELSE tx.Price_CY
						END *
						CASE
							WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN
								CASE
									WHEN tx.GroupLabel <> 'CENTURION' THEN ISNULL(EI1.FieldValue, 100) / 100
									ELSE ISNULL(EI2.FieldValue, 100) / 100
								END
							ELSE 1
						END
					ELSE 0
				 END) AS QualBrand_Sales_CY
			,SUM(CASE
					WHEN tx.GroupType='QUALIFYING_BRANDS' THEN
						CASE
							WHEN tx.GroupLabel NOT IN ('COTEGRA', 'LANCE', 'LANCE AG') THEN tx.Price_LY1
						END
					ELSE 0
				 END) AS QualBrand_Sales_LY1
			,SUM(CASE
					WHEN tx.GroupType='QUALIFYING_BRANDS' THEN
						CASE
							WHEN tx.GroupLabel NOT IN ('COTEGRA', 'LANCE', 'LANCE AG') THEN tx.Price_LY2
						END
					ELSE 0
				 END) AS QualBrand_Sales_LY2
			,MAX(ST2.Level5Code) Level5Code
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements ST2 ON ST2.StatementCode = TX.StatementCode
			LEFT JOIN RPWeb_User_EI_FieldValues EI1
			ON EI1.StatementCode=ST.StatementCode AND EI1.MarketLetterCode=TX.MarketLetterCode AND EI1.FieldCode='Radius_Percentage_CPP'
			LEFT JOIN RPWeb_User_EI_FieldValues EI2
			ON EI2.StatementCode=ST.StatementCode AND EI2.MarketLetterCode=TX.MarketLetterCode AND EI2.FieldCode='Radius_Percentage_Centurion'
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN  ('REWARD_BRANDS','QUALIFYING_BRANDS')
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode
	END

	
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
	
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2,Level5Code)
		SELECT ST.StatementCode, tx.MarketLetterCode,tx.ProgramCode
			,SUM(IIF(tx.GroupType = 'REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
			,SUM(CASE
					WHEN tx.GroupType='QUALIFYING_BRANDS' THEN
						CASE
							WHEN tx.Level5Code IN ('D000001','D0000117') AND tx.GroupLabel IN ('COTEGRA', 'LANCE', 'LANCE AG') THEN (tx.Price_CY * 0.50)
							WHEN tx.Level5Code IN ('D000001','D0000117') AND tx.GroupLabel = 'CARAMBA' THEN (tx.Price_CY * 1.25)
							ELSE tx.Price_CY
						END
					ELSE 0
				END) AS QualBrand_Sales_CY
			,SUM(IIF(tx.GroupType = 'QUALIFYING_BRANDS'
				AND GroupLabel NOT IN ('CANTUS','COTEGRA','FACET L','HEADLINE','LANCE','LANCE AG','LIBERTY 150','LIBERTY 200','PRIAXOR','PROWL H20','ZAMPRO') 
			,tx.Price_LY1,0)) AS QualBrand_Sales_LY1
			,SUM(IIF(tx.GroupType = 'QUALIFYING_BRANDS'
				AND GroupLabel NOT IN ('CANTUS','COTEGRA','FACET L','HEADLINE','LANCE','LANCE AG','LIBERTY 150','LIBERTY 200','PRIAXOR','PROWL H20','ZAMPRO') 
			,tx.Price_LY2,0)) AS QualBrand_Sales_LY2
			,Level5Code
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON TX.StatementCode=ST.StatementCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('QUALIFYING_BRANDS','REWARD_BRANDS') 
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, Level5Code

	END --END ACTUAL
	

	UPDATE @Temp
	SET Growth =
			CASE 
				WHEN QualBrand_Sales_LY1 <= 0 AND QualBrand_Sales_LY2 <= 0 THEN 
					CASE 
						WHEN QualBrand_Sales_CY <= 0 THEN 0 
						ELSE 9.9999 
					END 
				WHEN QualBrand_Sales_LY1 > 0 AND QualBrand_Sales_LY2 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY1
				WHEN QualBrand_Sales_LY2 > 0 AND QualBrand_Sales_LY1 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY2
				ELSE QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2)
			END

	/*
	Growth in 2021 relative to the 2-year average	Reward on 2021 POG sales of InVigor
	105% +			4.5%
	95% to 104.9%	4%
	Less Than 95%	3%
	*/

	-- Set reward tiers
	-- PLEASE SEE JIRA:RP-4055 as Tiers changed for RC
	
	-- IND/RC, FCL
	UPDATE @Temp SET [Reward_Percentage] = 0.045 WHERE	Level5Code IN ('D000001','D0000117') AND ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth + @ROUND_PERCENTAGE >= 1.05)
	UPDATE @Temp SET [Reward_Percentage] = 0.04 WHERE	Level5Code IN ('D000001','D0000117') AND [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.90
	UPDATE @Temp SET [Reward_Percentage] = 0.03 WHERE	Level5Code IN ('D000001','D0000117') AND [Reward_Percentage] = 0 AND [RewardBrand_Sales] > 0
	
	/*
	Line Companies:
		RetailerCode	RetailerName
		D0000107	CARGILL LIMITED - RETAIL WEST
		D0000137	RICHARDSON PIONEER LTD
		D0000244	UNITED FARMERS OF ALBERTA
		D520062427	NUTRIEN AG SOLUTIONS CANADA

		Cargil, Richardson, UFA
		105% +			3.5%
		95% - 104.9%	3%
		85% - 94.9%		2%
		< 85%			1%		

		Richardson
		105% +			4.5%
		95% - 104.9%	4%
		85% - 94.9%		3%
		< 85%			2%
	*/
	
	UPDATE @Temp SET [Reward_Percentage] = 0.035 WHERE	Level5Code IN ('D0000107','D0000137','D0000244') AND ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth + @ROUND_PERCENTAGE >= 1.05)
	UPDATE @Temp SET [Reward_Percentage] = 0.03  WHERE	Level5Code IN ('D0000107','D0000137','D0000244') AND [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.95
	UPDATE @Temp SET [Reward_Percentage] = 0.02  WHERE	Level5Code IN ('D0000107','D0000137','D0000244') AND [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.85
	UPDATE @Temp SET [Reward_Percentage] = 0.01  WHERE	Level5Code IN ('D0000107','D0000137','D0000244') AND [Reward_Percentage] = 0 AND [RewardBrand_Sales] > 0
	
	-- Ricahrdson gets 1% extra compared to others
	UPDATE @Temp SET [Reward_Percentage] += 0.01 WHERE  Level5Code = 'D0000137' AND [Reward_Percentage] > 0


	/*
		NURTRIEN - D520062427:
		105% +			1.0%
		95% - 104.9%	0.5%
	*/
	
	UPDATE @Temp SET [Reward_Percentage] = 0.01   WHERE	Level5Code ='D520062427' AND ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth + @ROUND_PERCENTAGE >= 1.05)
	UPDATE @Temp SET [Reward_Percentage] = 0.005  WHERE	Level5Code ='D520062427' AND [Reward_Percentage] = 0 AND Growth + @ROUND_PERCENTAGE >= 0.95 


	-- Set reward value based on percentage

	UPDATE @Temp  SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE RewardBrand_Sales > 0

	DELETE FROM RP2021_DT_Rewards_W_BrandSpecificSupport WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2021_DT_Rewards_W_BrandSpecificSupport(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, 'InVigor', RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward
	FROM @TEMP
END
