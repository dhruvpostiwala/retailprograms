﻿

CREATE PROCEDURE [dbo].[RP2021_Calc_W_Nod_Duo_SCG]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
		-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
	IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN
		
	/*
	 MP: Code review on Sep/06/2021
		 This program goes on to single market letter i.e CCP
		 There is no point maintaing market letter code and program code in temp tables, waste of additional joins.
		
		I am removing market letter code and program code columns from Temp tables. If required pull a back up before Sep/6/2021 for comparison.
		By removing market letter code and program code from temp table, procedure would run faster.
		Same needs to be done in other programs as well
	*/

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'
		
	DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		FarmCode VARCHAR(50) NOT NULL,
		[RewardBrand_Sales] MONEY NOT NULL DEFAULT 0,
		[RewardBrand_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Acres_LY] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Acres_LY2] [numeric](18, 2) NOT NULL DEFAULT 0,
		[IMI_Acres] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Reward_Per_Acre] [numeric](5, 2) DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[FarmCode] ASC
		)
	)

	/*
		Reward opportunity

		Retails will be rewarded $0.50 per acre of Nodulator DUO SCG sold to a Pulse Prospect Grower.

	·	Grower must have purchased Nodulator DUO SCG in 2018 and/or 2019 but NOT in 2020.
	·	Grower must have purchased an IMI product (Odyssey, Solo and/or Viper) in 2020 but NOT Nodulator DUO SCG in 2020.
	·	A minimum of 300 acres of Nodulator DUO SCG POG must be purchased by a grower to qualify.

	*/

	DECLARE @MINIMUM_ACRES  int = 300;  
	DECLARE @REWARD_PER_ACRE  float = 0.5;
	DECLARE @SEASON_LY INT = @SEASON - 1;
	DECLARE @SEASON_LY2 INT = @SEASON - 2;
	DECLARE @SEASON_LY3 INT = @SEASON - 3;

	INSERT INTO @TEMP(StatementCode, FarmCode,RewardBrand_Sales,RewardBrand_Acres,Acres_LY,Acres_LY2,IMI_Acres)
	SELECT ST.StatementCode,tx.FarmCode
			,SUM(IIF(BASFSeason = @SEASON AND GR.GroupType = 'REWARD_BRANDS',
					CASE 
						WHEN S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244') THEN tx.Price_SWP
						WHEN S.Level5Code='D0000117' THEN tx.Price_SRP 
						ELSE tx.Price_SDP
					END
			,0)) AS RewardBrand_Sales  
			,SUM(IIF(BASFSeason = @SEASON AND GR.GroupType = 'REWARD_BRANDS',tx.Acres,0)) AS RewardBrand_Acres  
			,SUM(IIF(BASFSeason = @SEASON_LY AND GR.GroupType = 'REWARD_BRANDS' ,tx.Acres,0)) AS Acres_LY 
			,SUM(IIF(BASFSeason <= @SEASON_LY2 AND GR.GroupType = 'REWARD_BRANDS' ,tx.Acres,0)) AS Acres_LY2  -- 2019 and 2018
			,SUM(IIF(BASFSeason = @SEASON_LY AND GR.GroupType = 'QUALIFYING_BRANDS' ,tx.Acres,0)) AS [IMI_Acres]			
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements S			ON S.StatementCode = ST.StatementCode
			INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode = ST.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup = tx.ChemicalGroup
			INNER JOIN RP_Config_Groups GR			ON GR.GroupID = BR.GroupID 	
		WHERE GR.ProgramCode = @PROGRAM_CODE AND GR.GroupType IN ('QUALIFYING_BRANDS','REWARD_BRANDS') AND tx.InRadius = 1 
			AND TX.FarmCode NOT IN ('F520020402','F520040993')
	GROUP BY ST.StatementCode,tx.FarmCode

	-- HEAD OFFICE BREAKDOWN
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT ST.StatementCode, @CCP_ML_CODE, @PROGRAM_CODE, TX.RetailerCode, TX.FarmCode
		,SUM(IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,IIF(S.Level5Code='D0000117',tx.Price_SRP,tx.Price_SDP))) AS Sales
		,1 AS FarmLevelSplit
	FROM @STATEMENTS ST			
		INNER JOIN RP_Statements S			ON S.StatementCode = ST.StatementCode
		INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
	WHERE tx.BASFSeason=@SEASON
		AND tx.InRadius=1				
		AND tx.ChemicalGroup ='NODULATOR DUO SCG'			
		AND TX.FarmCode NOT IN ('F520020402','F520040993')
	GROUP BY ST.StatementCode,  TX.RetailerCode, TX.FarmCode
	/***************************************/

	UPDATE @TEMP
	SET Reward_Per_Acre = @REWARD_PER_ACRE
	WHERE RewardBrand_Acres >= @MINIMUM_ACRES 
	      AND Acres_LY <= 0
		  AND ([IMI_Acres] > 0 OR [Acres_LY2] > 0)

	--Delete non qualifying growers
	DELETE FROM @TEMP
	WHERE Reward_Per_Acre <= 0

	UPDATE @TEMP SET Reward = RewardBrand_Acres *  Reward_Per_Acre WHERE RewardBrand_Acres >= @MINIMUM_ACRES
		
	DELETE FROM [dbo].[RP2021_DT_Rewards_W_Nod_Duo_SCG] WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO [dbo].[RP2021_DT_Rewards_W_Nod_Duo_SCG]([StatementCode],  [MarketLetterCode], [ProgramCode], [FarmCode], [RewardBrand_Acres], [Reward_Per_Acre], [Reward], RewardBrand_Sales)
	SELECT StatementCode,@CCP_ML_CODE AS MarketLetterCode, @PROGRAM_CODE [ProgramCode], [FarmCode],RewardBrand_Acres, Reward_Per_Acre,[Reward], RewardBrand_Sales
	FROM @TEMP

END

