﻿
CREATE PROCEDURE [dbo].[RP2021_Exceptions_E_GetRewardsData] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;		
	DECLARE @SEASON INT=2021
	DECLARE @REGION VARCHAR(4)='East'

	/*		
		We are expecting receive Locked statement code or unlocked statementcodes
		-- Locked Statement code from front-end
		-- Unlocked Statement codes from nightly refresh		
	*/

	DECLARE @TEMP AS [dbo].[UDT_RP_EXCEPTIONS_DATA]

	DECLARE @REWARD_MARGINS TABLE (
		ProgramCode [Varchar](50) NOT NULL,
		Level5Code  [Varchar](20) NOT NULL,
		Margin_A DECIMAL(6,4) NOT NULL DEFAULT 0,
		Margin_B DECIMAL(6,4) NULL ,
		PRIMARY KEY CLUSTERED 
		(
			ProgramCode ASC,
			Level5Code ASC,
			Margin_A ASC
		)WITH (IGNORE_DUP_KEY = OFF)
	)

	INSERT INTO @REWARD_MARGINS(ProgramCode, Level5Code, Margin_A, Margin_B)
	VALUES('RP2021_E_PORTFOLIO_SUPPORT','',0.01,NULL)
	,('RP2021_E_PORTFOLIO_SUPPORT','',0.03,NULL)
	,('RP2021_E_PORTFOLIO_SUPPORT','',0.04,NULL)
	,('RP2021_E_PORTFOLIO_SUPPORT','',0.06,NULL)
	,('RP2021_E_SALES_SUP_LOYALTY_BONUS','',0.01,NULL)
	,('RP2021_E_SALES_SUP_LOYALTY_BONUS','',0.025,NULL)
	,('RP2021_E_SALES_SUP_LOYALTY_BONUS','',0.0425,NULL)
	,('RP2021_E_SOLLIO_GROWTH_BONUS','',0.03,NULL)
	,('RP2021_E_SOLLIO_GROWTH_BONUS','',0.04,NULL)
	,('RP2021_E_SOLLIO_GROWTH_BONUS','',0.05,NULL)
	,('RP2021_E_SUPPLY_SALES','',0.025,NULL)
	,('RP2021_E_SUPPLY_SALES','',0.050,NULL)
	,('RP2021_E_SUPPLY_SALES','',0.075,NULL)

	INSERT INTO @TEMP(StatementCode,Level5Code,ProgramCode,RewardCode,MarketLetterCode,NEXT_MARGIN_A,NEXT_MARGIN_B,reward_margins_a,reward_margins_b)
	SELECT UL.StatementCode,UL.Level5Code,P.ProgramCode,P.RewardCode,E.MarketLetterCode,0,0,'[0]','[0]'
	FROM @STATEMENTS ST
		INNER JOIN RPWeb_Statements UL			ON UL.StatementCode=ST.StatementCode
		INNER JOIN RPWeb_ML_ELG_Programs E		ON E.StatementCode = ST.StatementCode
		INNER JOIN RP_Config_programs P 		ON P.ProgramCode = E.ProgramCode			
	WHERE P.Season=@SEASON AND P.Region=@Region AND ISNULL(P.Exception_Enabled,0) = 1							
		
	DELETE FROM @TEMP WHERE MarketLetterCode = 'ML2021_E_INV' AND RewardCode = 'SUPPLY_SALES_E' -- Supply Sales only using on CCP MarketLetter
	UPDATE @TEMP SET exception_type = 'TIER' WHERE RewardCode IN ('PORT_SUP_E','SS_LOYALTY_E','SG_E','SUPPLY_SALES_E')	
	UPDATE @TEMP SET exception_type = 'NON-TIER' WHERE exception_type IS NULL
	
/*
	Title									RewardCode			ProgramCode
	Fungicide Support Reward				FUNG_SUP_E			RP2021_E_FUNGICIDES_SUPPORT				--NEEDS TO BE DISCUSSED

	TIERED - EXCEPTIONS
	Portfolio Support Reward				PORT_SUP_E			RP2021_E_PORTFOLIO_SUPPORT
	Sales Support - Loyalty Bonus Reward	SS_LOYALTY_E		RP2021_E_SALES_SUP_LOYALTY_BONUS
	Sollio Growth Bonus						SG_E				RP2021_E_SOLLIO_GROWTH_BONUS
	Supply of Sales Results Reward			SUPPLY_SALES_E		RP2021_E_SUPPLY_SALES
*/

	-- NOT-TIERED REWARDS
	UPDATE T1
	SET T1.cy_sales_A = NTIER.CYSales
		,next_margin_A = NTIER.next_margin_A
	FROM @TEMP T1
		INNER JOIN (										
			-- Planning Businnes
			SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.01 AS next_margin_A
			FROM  [dbo].[RP2021Web_Rewards_E_PlanningTheBusiness]
			WHERE [Reward_Percentage]=0
			GROUP BY StatementCode, ProgramCode
			HAVING SUM(RewardBrand_Sales) > 0
			
				UNION ALL

			-- POG Sales Bonus
			SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.02 AS next_margin_A
			FROM [dbo].[RP2021Web_Rewards_E_POGSalesBonus]  
			WHERE [Reward_Percentage]=0 
			GROUP BY StatementCode, ProgramCode		
			HAVING SUM(RewardBrand_Sales) > 0

--				UNION ALL

			-- FUNGICIDE SUPPORT REWARD


		) NTIER	
		ON NTIER.StatementCode = T1.StatementCode AND T1.ProgramCode = NTIER.ProgramCode
	WHERE T1.exception_type = 'NON-TIER' 		
		
	-- TIERED REWARDS
	UPDATE T1
	SET T1.cy_sales_a = TIER.CYSales
		,T1.current_margin_a = TIER.CurrentMargin
		,T1.current_reward_a = TIER.CurrentReward			
	FROM @TEMP T1
		INNER JOIN (
			-- CPP Market Letter Only Reward
			SELECT StatementCode, ProgramCode
				,SUM(RewardBrand_Sales) AS 'CYSales'
				,MAX(Reward_Percentage) AS 'CurrentMargin'
				,SUM(Reward) AS 'CurrentReward'					
			FROM [dbo].[RP2021Web_Rewards_E_PortfolioSupport]
			GROUP BY StatementCode, ProgramCode

				UNION ALL

			-- INV Market Letter Only Reward
			SELECT StatementCode, ProgramCode
				,SUM(RewardBrand_POG_Sales) AS 'CYSales'
				,MAX(Reward_Percentage) AS 'CurrentMargin'
				,SUM(Reward) AS 'CurrentReward'					
			FROM [dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]			
			GROUP BY StatementCode, ProgramCode

				UNION ALL 

			-- CPP Market Letter Only Reward
			SELECT StatementCode, ProgramCode
				,SUM(RewardBrand_Sales) as 'CYSales'
				,MAX(Reward_Percentage) AS 'CurrentMargin'
				,SUM(Reward) AS 'CurrentReward'										
			FROM [dbo].[RP2021Web_Rewards_E_Sollio_Growth_Bonus]
			GROUP BY StatementCode, ProgramCode

				UNION ALL

			-- Even though this reward is applicable on both market letters
			-- Exception is applicable only for CPP market letter
			SELECT StatementCode, ProgramCode
				,SUM(RewardBrand_Sales) AS 'CYSales'
				,MAX(Reward_Percentage) AS 'CurrentMargin'
				,SUM(Reward) AS 'CurrentReward'										
			FROM [dbo].[RP2021Web_Rewards_E_SupplySales]
			WHERE [MarketLetterCode] = 'ML2021_E_CPP'
			GROUP BY StatementCode, ProgramCode		
		) TIER	
		ON TIER.StatementCode = T1.StatementCode AND T1.ProgramCode = TIER.ProgramCode
	WHERE T1.exception_type = 'TIER'
			
	UPDATE @TEMP 		SET Current_Margin_A = 0 		WHERE cy_sales_A <= 0
	UPDATE @TEMP 		SET Current_Margin_B = 0 		WHERE cy_sales_B <= 0
		
	/************ SEGEMENT A NEXT REWARD MARGIN TO BE APPLIED ******************/		
	UPDATE TGT
	SET Next_Margin_A = SRC.Margin_A		
	FROM @TEMP TGT
		INNER JOIN (
		SELECT T1.StatementCode, T1.ProgramCode, T2.Margin_A
			,RANK() OVER(PARTITION BY T1.StatementCode, T1.ProgramCode ORDER BY T2.Margin_A ) AS [Rank]				
		FROM @TEMP T1
			INNER JOIN @REWARD_MARGINS T2
			ON T2.ProgramCode=T1.ProgramCode AND T2.Margin_A > T1.Current_Margin_A			 			  
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.[Rank]=1

	/************ SEGEMENT A REWARD MARGINS ******************/		
	UPDATE TGT
	SET Reward_Margins_A = '[' + ISNULL(SRC.Reward_Margins,'0') + ']'				
	FROM @TEMP TGT
		INNER JOIN (
		SELECT T1.StatementCode, T1.ProgramCode, STRING_AGG(T2.Margin_A, ',') AS Reward_Margins
		FROM @TEMP T1
			INNER JOIN @REWARD_MARGINS T2
			ON T2.ProgramCode=T1.ProgramCode AND T2.Margin_A > T1.Current_Margin_A			
		GROUP BY T1.StatementCode, T1.ProgramCode
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode = TGT.ProgramCode 
		
FINAL:		
	SELECT * FROM @TEMP
END

