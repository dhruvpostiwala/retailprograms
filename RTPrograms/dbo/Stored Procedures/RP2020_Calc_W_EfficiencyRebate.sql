﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_W_EfficiencyRebate]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	/*
	NEW: Nov 19, 2019 ilan - RRT-657 - reward is changing to include an escalator bonus
	Within the InVigor and CCP market letter accordions, under the Efficiency Bonus program line item, please add a sub-row for Escalator Bonus.
	-- If retailers POG Plan is between Tier 1 and Tier 2, and meet requirements of Escalator Bonus, we will show the difference in % (i.e. Efficiency Rebate would display 1.00%, and Escalator Bonus would show 0.75%)
	-- If retailers POG Plan is between Tier 2 and Tier 3, and meet requirements of Escalator Bonus, we will show the difference in % (i.e. Efficiency Rebate would display 1.75%, and Escalator Bonus would show 0.75%)
	-- If retailers POG Plan is between greater than $2M (i.e. Tier 3) Retailer would not be eligible for Escalator Bonus. Please hide row.

	In summary:

	1) Qualifiers:
		- Portfolio Support = ALL brands (whatever that means)
		- Escalator Bonus = ALL brands (whatever that means) MINUS Liberty and InVigor
	2) Reward is paid on:
		- InVigor Market Letter = InVigor
		- CCP Market Letter = Liberty + Centurion
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 179
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_EFF_REBATE'

	DECLARE @EPSILON DECIMAL(4,4)=dbo.SVF_GetEpsilon()

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,		
		GroupLabel varchar(100) NOT NULL,
		Support_Reward_Sales MONEY NOT NULL,		
		Support_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Support_Reward MONEY NOT NULL DEFAULT 0,		
		Growth_Reward_Percentage MONEY NOT NULL DEFAULT 0,
		Growth_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,			
			[GroupLabel] ASC			
		)
	)

	-- QUALIFIERS ARE AT STATEMENT LEVEL
	DECLARE @QUALIFIERS TABLE (
		StatementCode INT,
		All_QualSales_CY  MONEY NOT NULL DEFAULT 0,
		Growth_QualSales_CY MONEY NOT NULL DEFAULT 0,
		Growth_QualSales_LY MONEY NOT NULL DEFAULT 0,		
		Support_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Growth_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Growth DECIMAL(7,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC
		)
	)

	-- Qualifiers Information
	INSERT INTO @QUALIFIERS(StatementCode,All_QualSales_CY,Growth_QualSales_CY,Growth_QualSales_LY)
	SELECT ST.StatementCode
		,SUM(tx.Price_CY) AS All_QualSales_CY		
		,SUM(IIF(TX.GroupLabel IN ('INVIGOR','LIBERTY','LIBERTY 150','LIBERTY 200'),0 ,tx.Price_CY)) AS Growth_QualSales_CY
		,SUM(IIF(TX.GroupLabel IN ('INVIGOR','LIBERTY','LIBERTY 150','LIBERTY 200'),0 ,tx.Price_LY1)) AS Growth_QualSales_LY	
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode='ELIGIBLE_SUMMARY' AND tx.GroupType='ELIGIBLE_BRANDS' 
	GROUP BY ST.StatementCode
	   
	-- Update the support reward %
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.025	WHERE All_QualSales_CY + @EPSILON >= 2000000
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.0175 WHERE Support_Reward_Percentage=0 AND All_QualSales_CY + @EPSILON >= 1500000
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.01	WHERE Support_Reward_Percentage=0 AND All_QualSales_CY + @EPSILON >= 1000000

	-- Determine growth on total sales even though we are storing the data at brand level sales
	UPDATE @QUALIFIERS
	SET [Growth] = [Growth_QualSales_CY] / [Growth_QualSales_LY]
	WHERE [Growth_QualSales_LY] > 0 

	-- Update the growth reward % but only if the support reward % is 1% or 1.75%
	UPDATE @QUALIFIERS 
	SET [Growth_Reward_Percentage] = 0.0075 -- Bascially this the difference between reward tiers 
	WHERE [Support_Reward_Percentage] IN (0.01, 0.0175) AND (([Growth] = 0 AND [Growth_QualSales_CY] > 0) OR [Growth]+0.005 >= 1.15)
		
	/*	
		Buffer Rule: Include buffer on reward tier qualification of $5000. This includes minimum qualification i.e. $1M with buffer included = $995,000.
		Note: Buffer Rule should only apply if the retail does not qualify for the escalator bonus reward opportunity.
	*/
	IF @STATEMENT_TYPE='ACTUAL'
		UPDATE @QUALIFIERS 
		SET [Support_Reward_Percentage] = 0.01 
		WHERE [Support_Reward_Percentage]=0 AND [Growth_Reward_Percentage]=0 AND [All_QualSales_CY] + @EPSILON >= 995000 
					   
	-- LETS CALCULATE REWARDS ON ELIGIBLE SALES BY APPLY REWARD PERCENTAGES		
	-- Determine sales at the brand level
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,GroupLabel,Support_Reward_Sales) 
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.GroupLabel, SUM(tx.Price_CY) AS Support_Reward_Sales	   
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.GroupLabel
	
	-- Update the support reward % and growth reward %  and reward values
	UPDATE T1
	SET [Support_Reward_Percentage] = T2.[Support_Reward_Percentage]
		,[Support_Reward] = [Support_Reward_Sales] * T2.[Support_Reward_Percentage]
		,[Growth_Reward_Percentage] = T2.[Growth_Reward_Percentage]
		,[Growth_Reward] = [Support_Reward_Sales] * T2.[Growth_Reward_Percentage]
	FROM @TEMP T1
		INNER JOIN @QUALIFIERS T2 
	ON T1.[StatementCode] = T2.[StatementCode] -- AND T1.[MarketLetterCode] = T2.[MarketLetterCode] 
	WHERE [Support_Reward_Sales] > 0

	-- Update final reward
	UPDATE @TEMP SET [Reward] = [Support_Reward] + [Growth_Reward] WHERE [Support_Reward] + [Growth_Reward] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END

	-- LETS INSERT QUALIFIERS META DATA INTO ACTUAL TABLES FROM TEMP TABLES
	DELETE FROM RP2020_DT_Rewards_W_EFF_REBATE_QUAL WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)

	INSERT INTO RP2020_DT_Rewards_W_EFF_REBATE_QUAL(Statementcode, MarketLetterCode, ProgramCode, All_QualSales_CY, All_QualSales_LY, Growth_QualSales_CY, Growth_QualSales_LY, Growth)
	SELECT Statementcode, 'ALL' AS MarketLetterCode, @PROGRAM_CODE, All_QualSales_CY, 0 AS All_QualSales_LY, Growth_QualSales_CY, Growth_QualSales_LY, Growth
	FROM @QUALIFIERS


	-- LETS INSERT ELIIGIBLE SALES & REWARDS META DATA INTO ACTUAL TABLES FROM TEMP TABLES
	DELETE FROM RP2020_DT_Rewards_W_EFF_REBATE WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)
	
	INSERT INTO RP2020_DT_Rewards_W_EFF_REBATE(Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Support_Reward_Sales, Support_Reward_Percentage, Support_Reward, Growth_Reward_Percentage, Growth_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, @PROGRAM_CODE, GroupLabel, Support_Reward_Sales, Support_Reward_Percentage, Support_Reward, Growth_Reward_Percentage, Growth_Reward, Reward
	FROM @TEMP
	


END
 