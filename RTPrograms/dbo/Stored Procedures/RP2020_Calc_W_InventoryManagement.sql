﻿CREATE PROCEDURE [dbo].[RP2020_Calc_W_InventoryManagement]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	Retails receive a 1.0% payment on 2020 POG sales of Cotegra, Lance, Lance AG, and Facet L, valued at invoice pricing,
	for which delivery is taken prior to September 30, 2020.  
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_PORFOLIO_ACHIEVEMENT'
	
	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		StatementLevel varchar(50) NOT NULL,
		Level5Code varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales Money NOT NULL,
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @RewardPercentage Float=0.01;
	DECLARE @RewardPercentage_nutrienCargill Float=0.02;

	--
	
	DECLARE @Inventory_Temp Float = 10;

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,StatementLevel,Level5Code,RewardBrand,RewardBrand_Sales)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,MAX(S.StatementLevel) StatementLevel,MAX(tx.Level5Code) Level5Code,tx.GroupLabel
	,SUM(tx.Price_CY) AS RewardBrand_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		INNER JOIN RP_Statements S on tx.StatementCode = S.StatementCode
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' 
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel

	--ALL
	UPDATE @TEMP SET Reward_Percentage  = @RewardPercentage, Reward = RewardBrand_Sales * @RewardPercentage WHERE StatementLevel <> 'LEVEL 5' and @Inventory_Temp <= 15;

	--Distributors
	UPDATE @TEMP SET Reward_Percentage  = IIF([MarketLetterCode]='ML2020_W_INV',0.02,0.01) WHERE StatementLevel ='LEVEL 5' and @Inventory_Temp <= 10;

	--Nutrien and Cargill
	UPDATE @TEMP SET Reward_Percentage  = @RewardPercentage_nutrienCargill, Reward = RewardBrand_Sales * @RewardPercentage  WHERE StatementLevel ='LEVEL 5' and Level5Code in ('D520062427', 'D0000107', 'D0000137') and [MarketLetterCode]='ML2020_W_INV' and @Inventory_Temp <= 10 ;

	-- Richardson and UFA
	UPDATE @TEMP SET Reward_Percentage  = @RewardPercentage, Reward = RewardBrand_Sales * @RewardPercentage  WHERE StatementLevel ='LEVEL 5' and Level5Code in ('D0000244') and [MarketLetterCode]='ML2020_W_INV' and @Inventory_Temp <= 10 ;

	--UPDATE @TEMP SET Reward  = Reward_Percentage * RewardBrand_Sales WHERE StatementLevel ='LEVEL 5' and Level5Code <> 'D520062427' and @Inventory_Temp <= 10;
	UPDATE @TEMP SET Reward  = Reward_Percentage * RewardBrand_Sales WHERE StatementLevel ='LEVEL 5' and @Inventory_Temp <= 10;

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'	
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	DELETE FROM RP2020_DT_Rewards_W_InventoryManagement WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_InventoryManagement(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward
	FROM @TEMP

END

