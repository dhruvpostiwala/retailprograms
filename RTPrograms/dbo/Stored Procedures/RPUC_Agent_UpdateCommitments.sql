﻿

CREATE PROCEDURE [dbo].[RPUC_Agent_UpdateCommitments] @STATEMENTCODE INT, @ACTION AS VARCHAR(20), @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;	
	DECLARE @CommitmentID INT;
	DECLARE @NextID INT=1;
	
	BEGIN TRY
		-- SINGLE RECORD UPDATE
		-- @JSON = '{"rp_id":891496,"basfseason":2015,"retailercode":"R0000504","commitmenttype":"Lentils","commitmentdate":"2015-07-08T04:00:00.000Z","status":"Complete"}'

		SET @CommitmentID=JSON_VALUE(@JSON, '$.rp_id')
		IF @ACTION='DELETE'
			BEGIN				
				DELETE RP_DT_Commitments WHERE [StatementCode]=@STATEMENTCODE AND RP_ID=@CommitmentID
				DELETE RP_DT_CommitmentsVarietyInfo WHERE [StatementCode]=@STATEMENTCODE AND RP_WC_ID=@CommitmentID
			END
		ELSE
			BEGIN
				-- INSERT, UPDATE			
				IF @ACTION='INSERT'					
					BEGIN
						SELECT @NextID=MAX(RP_ID)+1 FROM RP_DT_Commitments 	WHERE [StatementCode]=@Statementcode
						IF @NextID IS NULL
							SET @NextID=1
					END 
			
				DELETE RP_DT_Commitments WHERE [StatementCode]=@STATEMENTCODE AND RP_ID=@CommitmentID

				INSERT INTO RP_DT_Commitments(StatementCode,DataSetCode,RP_ID,BASFSeason,RetailerCode,FarmCode,CommitmentType,CommitmentDate,[Status],DocCode,ID)
				SELECT @Statementcode as Statementcode
					,0 AS DataSetCode
					,IIF(@ACTION='INSERT',ISNULL(@NextID,1),@CommitmentID) AS rp_id
					,JSON_VALUE(@JSON, '$.basfseason') as basfseason
					,JSON_VALUE(@JSON, '$.retailercode') as retailercode
					,JSON_VALUE(@JSON, '$.farm.farmcode') as farmcode
					,JSON_VALUE(@JSON, '$.commitmenttype') as commitmenttype
					,JSON_VALUE(@JSON, '$.commitmentdate') as commitmentdate
					,JSON_VALUE(@JSON, '$.status') as [status]
					,'UseCase ' + CAST(ISNULL(@NextID,@CommitmentID) AS VARCHAR) AS [DocCode]
					,0 AS ID -- since this from UseCase
			END	

			--THERE ARE NO COMMITMENS IN THE EAST , NO NEED TO ROLL UP TO LEVEL 2 STATEMENT

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

	SELECT 'success' AS [Status], '' AS Error_Message		
END

