﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_WarehousePayments]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales Money NOT NULL,
		Reward_Percentage Decimal(6,4) NOT NULL,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @RewardPercentage Float=0.01;

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales, Reward_Percentage,Reward)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel,SUM(tx.Price_CY) AS RewardBrand_Sales,@RewardPercentage,SUM(tx.Price_CY * @RewardPercentage) AS Reward
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' AND tx.GroupLabel='INVIGOR'
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
	
	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'	
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	DELETE FROM RP2020_DT_Rewards_W_WarehousePayments WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_WarehousePayments(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward
	FROM @TEMP

END
