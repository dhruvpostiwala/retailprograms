﻿

CREATE PROCEDURE [dbo].[RPST_Agent_ForceUpdateStatus] @SEASON INT,  @USERNAME VARCHAR(150), @Status VARCHAR(100), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @STATEMENTS UDT_RP_STATEMENT;
	DECLARE @TARGET_STATEMENTS UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @OLD_STATUS VARCHAR(100);
	
	-- PASS IN LOCKED STATEMENT CODES	
	BEGIN TRY

		IF @STATUS = 'Ready For Payment'	
		BEGIN

			-- PASSED IN STATEMENTS (UNLOCKED STATEMENTCODES)
			INSERT INTO @STATEMENTS(StatementCode)
			SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')


			--TVF_Get_LockedStatementCodesToRefresh ACCEPTS LOCKED STATEMENTS
			--DEMAREY BAKER APRIl 20 21
			UPDATE S
			SET StatementCode = LK.StatementCode
			FROM @STATEMENTS S 
			INNER JOIN RPWeb_Statements LK ON S.StatementCode = LK.SRC_StatementCode AND Status = 'Active'
			WHERE S.StatementCode > 0


			-- ALL STATEMENTS CODES TO BE INCLUDED FROM FAMILY
			INSERT INTO @TARGET_STATEMENTS(StatementCode, StatementLevel, DataSetCode, SRC_Statementcode)
			SELECT StatementCode,StatementLevel,DataSetCode, SRC_Statementcode
			FROM TVF_Get_LockedStatementCodesToRefresh(@STATEMENTS) 
	
			UPDATE STI
			SET [FieldValue]=@Status
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Force Update Status - Update','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RPWeb_StatementInformation STI		
				INNER JOIN @TARGET_STATEMENTS TS	
				ON TS.SRC_StatementCode=STI.StatementCode				
			WHERE STI.[FieldName]='Status'  AND STI.FieldValue <> 'Submitted For RC Reconciliation'
		
			EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	
		END
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message

END




