﻿
CREATE PROCEDURE [dbo].[RPST_Agent_Approve_Exceptions]  @STATEMENTCODE INT,  @USERNAME VARCHAR(150), @USERROLE VARCHAR(50), @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @REWARD_CODE VARCHAR(20);
	DECLARE @UNLOCKED_STATEMENTCODE INT;
	DECLARE @SRC_STATEMENTCODE INT;
	DECLARE @EXCEPTIONLEVEL VARCHAR(10);
	DECLARE @EXCEPTION_ID INT = JSON_VALUE(@JSON, '$.id') --@EXCEPTION_ID
	DECLARE @VERSION_TYPE VARCHAR(20)
	DECLARE @STATEMENT_IS_ACTIVE BIT=0
	DECLARE @SEASON_STRING VARCHAR(4)
	DECLARE @OTHER_REWARD_CODE VARCHAR(20) 
	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;

	DECLARE @REFRESH_STATEMENTS AS UDT_RP_STATEMENT

	DECLARE @Exception TABLE (
		ID INT,		
		StatementCode INT NOT NULL DEFAULT 0,
		Src_ID INT NOT NULL DEFAULT 0,			
		RewardCode VARCHAR(50) NULL,
		Amount Money NULL DEFAULT 0,		
		MarketLetterCode VARCHAR(50) NULL,
		Exception_Type VARCHAR(10)  NULL,
		ValidationStatus VARCHAR(20) NULL DEFAULT 'PASSED'
	)

	DECLARE @APPROVED_EXCEPTIONS TABLE (
		StatementCode INT,
		RewardCode VARCHAR(50),
		MarketLetterCode VARCHAR(50),
		Amount Decimal(18,2) DEFAULT 0
	)

	SELECT @REGION = UPPER(Region),@SEASON = Season,@SRC_STATEMENTCODE = SRC_StatementCode
		,@UNLOCKED_STATEMENTCODE = IIF(VersionType='Locked',Src_StatementCode,StatementCode)
		,@VERSION_TYPE = VersionType
		,@STATEMENT_IS_ACTIVE = IIF([Status]='Active', 1,0)
	FROM RPWeb_Statements 
	WHERE StatementCode = @STATEMENTCODE;

	SET @SEASON_STRING = CAST(@SEASON AS VARCHAR(4))
	
	DECLARE @MARKET_LETTER_CODE VARCHAR(20) = 'ML'  + @SEASON_STRING + '_' + IIF(@REGION = 'EAST','E','W')+'_EXCEPTION';
	
	IF @SEASON >= 2021
		SET @OTHER_REWARD_CODE = 'OTHER_'+ IIF(@REGION = 'EAST','EX','WX');
	ELSE	
		SET @OTHER_REWARD_CODE = 'OTHER_'+ IIF(@REGION = 'EAST','E_X','W_X');
		
	SELECT @REWARD_CODE = RewardCode FROM RPWeb_Exceptions WHERE ID = @EXCEPTION_ID;
	   
	SET @EXCEPTIONLEVEL = JSON_VALUE(@JSON, '$.exception_level')
	
	IF  @REGION='EAST' AND @EXCEPTIONLEVEL = 'LEVEL 2'
		INSERT INTO @Exception(ID,StatementCode)
		SELECT ID,StatementCode 
		FROM RPWeb_Exceptions 
		WHERE (ID = @EXCEPTION_ID OR SRC_ID = @EXCEPTION_ID)  AND Status = 'Submitted' AND Exception_Type <> 'OTHER'	
	ELSE -- EAST LEVEL 1 STATEMENTS  -- WEST STATEMENTS (LEVEL 1 AND LEVEL 2)
		INSERT INTO @Exception(ID,StatementCode)
		SELECT ID,StatementCode 
		FROM RPWeb_Exceptions 
		WHERE ID = @EXCEPTION_ID  AND [Status] IN ('Submitted','Pending Approval - HO','Pending Approval - FINAL')
	
	UPDATE T1
	SET  T1.StatementCode = E.StatementCode
		,T1.SRC_ID = E.Src_ID
		,T1.RewardCode = E.RewardCode
		,T1.Exception_Type = E.Exception_Type
		,T1.Amount = ISNULL(E.Exception_Value,0)				
	FROM @Exception T1
		INNER JOIN RPWeb_Exceptions E ON E.ID = T1.ID
		--WHERE E.[Status] = 'Submitted' AND E.Season = @SEASON 

	UPDATE @Exception 
	SET ValidationStatus = 'FAILED'
	WHERE Amount=0 OR RewardCode='' OR StatementCode = 0 OR Exception_Type = ''

	IF @REGION='EAST' 
	BEGIN 
		BEGIN TRY
			BEGIN TRANSACTION

				DELETE FROM @EDIT_HISTORY

				-- UPDATE STATUS APPROVAL  EXCEPTIONS TABLE
				UPDATE T1
				SET [Status] = 'Approved'
					,Show_In_UL = 1
					,DateModified = GetDate()
					,PostedBy = @USERNAME
				OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RPWeb_Exceptions T1
					INNER JOIN @Exception T2
					ON T2.ID=T1.ID 
				WHERE T2.validationStatus = 'PASSED';
			
				---MERGE RB_WEB_Rewards_SUMMARY TABLE LOCKED STATEMENTS 
				MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
				USING (
					SELECT E.StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
					FROM RPWeb_Exceptions E
						INNER JOIN RPWeb_Statements S 
						ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType = 'Locked' AND S.Status = 'Active'
					WHERE E.Status = 'Approved' 
						AND Show_IN_UL  = 1
						AND E.VersionType = 'Current'
						AND E.Exception_type <> 'OTHER'
						AND RewardCode = @REWARD_CODE
						AND E.Season = @SEASON
						AND (E.ID = @EXCEPTION_ID OR E.SRC_ID = @EXCEPTION_ID)
					GROUP BY E.StatementCode,E.RewardCode
					
					UNION

					--OTHER LEVEL's DONT REMEMBER BY I IUSED THIS CONDIITON (E.ID = @EXCEPTION_ID OR E.SRC_ID = @EXCEPTION_ID) WILL CHECK IT OUT AND TEST
					SELECT E.StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
					FROM RPWeb_Exceptions E
						INNER JOIN RPWeb_Statements S 
						ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType = 'Locked' AND S.Status = 'Active'
					WHERE E.Status = 'Approved' 
						AND Show_IN_UL  = 1
						AND E.VersionType = 'Current'
						AND E.Exception_type = 'OTHER'
						AND RewardCode = @REWARD_CODE
						AND E.Season = @SEASON
						AND E.StatementCode = @STATEMENTCODE
					GROUP BY E.StatementCode,E.RewardCode


					UNION

					--RP3107
					--LEVEL 2 OTHER  UPDATE ONCE WE MAKE CHNAGE TO OTHER
					SELECT  StatementCode,RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(Exception_Value) AS Amount
					FROM (
						SELECT S.DatasetCode AS StatementCode,E.RewardCode,E.Exception_Value
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements S 
							ON S.StatementCode = E.StatementCode AND DataSetCode <> 0 AND Region = @REGION
						WHERE S.VersionType = 'Locked' AND S.Status = 'Active'
							AND RewardCode = @REWARD_CODE
							AND Show_IN_UL  = 1
							AND E.VersionType = 'Current'
							AND E.Exception_type = 'OTHER'
							AND E.Status = 'Approved'
							AND E.Season = @SEASON
					)L2
					WHERE StatementCode = (SELECT DISTINCT DatasetCode FROM RPWeb_Statements WHERE StatementCode = @StatementCode)
					GROUP BY L2.StatementCode,L2.RewardCode
				) AS SRC
				ON TGT.StatementCode = SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND SRC.MarketLetterCode = TGT.MarketLetterCode    
			  WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
					INSERT  (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment)  
					VALUES (SRC.StatementCode,SRC.MarketLetterCode,SRC.RewardCode,SRC.Amount,'Eligible','',SRC.Amount,0,SRC.Amount,0) 
			   WHEN MATCHED AND  SRC.RewardCode = @OTHER_REWARD_CODE THEN
					UPDATE  SET TGT.RewardValue = SRC.Amount,TGT.TotalReward = SRC.Amount,TGT.CurrentPayment = SRC.Amount,TGT.NextPayment = 0
				;
			
			
				---MERGE RB_WEB_Rewards_SUMMARY TABLE UNLOCKED STATEMENTS 
				MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
				USING (
					SELECT E.Src_StatementCode AS StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
					FROM RPWeb_Exceptions E
					INNER JOIN RPWeb_Statements S ON S.StatementCode = E.SRC_StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
					WHERE E.Status = 'Approved' 
						AND Show_IN_UL  = 1
						AND E.VersionType = 'Current'
						AND RewardCode = @REWARD_CODE
						AND E.Exception_type <> 'OTHER'
						AND E.Season = @SEASON
						AND (E.ID = @EXCEPTION_ID OR E.SRC_ID = @EXCEPTION_ID)
					GROUP BY E.Src_StatementCode,E.RewardCode


					--OTHER LEVEL 1s 'OTHER'
					UNION

					SELECT E.Src_StatementCode AS StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
					FROM RPWeb_Exceptions E
					INNER JOIN RPWeb_Statements S ON S.StatementCode = E.SRC_StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
					WHERE E.Status = 'Approved' 
						AND Show_IN_UL  = 1
						AND E.VersionType = 'Current'
						AND RewardCode = @REWARD_CODE
						AND E.Exception_type = 'OTHER'
						AND E.Season = @SEASON
						AND E.StatementCode = @STATEMENTCODE
					GROUP BY E.Src_StatementCode,E.RewardCode


					UNION 

					--RP3107
					--LEVEL 2 OTHER  UPDATE ONCE WE MAKE CHNAGE TO OTHER
					SELECT  StatementCode,RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(Exception_Value) AS Amount
					FROM (
						SELECT S.DatasetCode AS StatementCode,E.RewardCode,E.Exception_Value
						FROM RPWeb_Exceptions E
						INNER JOIN RPWeb_Statements S ON S.StatementCode =  E.Src_StatementCode AND DataSetCode <> 0 AND Region = @REGION
						WHERE S.VersionType = 'Unlocked' and S.Status = 'Active'
							AND E.Exception_type = 'OTHER'
							AND E.VersionType = 'Current'
							AND Show_IN_UL  = 1
							AND E.Status = 'Approved'
							AND E.Season = @SEASON
					)L2
					WHERE StatementCode = (Select DISTINCT DatasetCode FROM RPWeb_Statements WHERE StatementCode = @SRC_STATEMENTCODE)
					GROUP BY L2.StatementCode,L2.RewardCode
				) AS SRC
				ON TGT.StatementCode = SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND SRC.MarketLetterCode = TGT.MarketLetterCode        
			   WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
					INSERT  (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment)  
					VALUES (SRC.StatementCode,SRC.MarketLetterCode,SRC.RewardCode,SRC.Amount,'Eligible','',SRC.Amount,0,SRC.Amount,0) 
			   WHEN MATCHED AND SRC.RewardCode = @OTHER_REWARD_CODE THEN
					UPDATE  SET TGT.RewardValue = SRC.Amount,TGT.TotalReward = SRC.Amount,TGT.CurrentPayment = SRC.Amount,TGT.NextPayment = 0
				;

				--
				--https://kennahome.jira.com/browse/RP-3517
				--Added versiontype to ensure it doesnt sum and overwrite historial values
				--MERGE Exceptions table For other only
				MERGE [dbo].[RPWeb_Exceptions] AS TGT
				USING (
					SELECT R.StatementCode,S.Src_StatementCode,S.RetailerCode, R.RewardCode,R.TotalReward AS Amount
					FROM RPWeb_Rewards_Summary R
					INNER JOIN RPWeb_Statements S ON S.StatementCode = R.StatementCode AND S.REGION =  @REGION
					WHERE R.MarketLetterCode = @MARKET_LETTER_CODE AND R.RewardCode = @OTHER_REWARD_CODE
						  AND S.VersionType = 'Locked' AND S.Status = 'Active'
						  AND StatementLevel = 'LEVEL 2'
				) AS SRC
				ON TGT.StatementCode = SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND TGT.Status = 'Approved' AND VersionType = 'Current'   
			   WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
					INSERT  ([RetailerCode], [StatementCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy],Show_IN_UL)  
					VALUES (SRC.RetailerCode,SRC.StatementCode,@Season,SRC.RewardCode,'LEVEL 2','Other Exceptions','OTHER',SRC.Amount,0,'Approved',SRC.Src_StatementCode,0,'OU OTHER EXCEPTION ROLLED UP',GETDATE(),GETDATE(),@USERNAME,1)
				WHEN MATCHED AND SRC.RewardCode = @OTHER_REWARD_CODE AND SRC.Amount > 0 THEN
					UPDATE  SET TGT.Exception_Value = SRC.Amount, Show_IN_UL  = 1
				;

				EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY
				
				COMMIT TRANSACTION
				GOTO FINAL
		END TRY
	
		BEGIN CATCH
			SELECT 'failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
			ROLLBACK TRANSACTION			
		END CATCH
	END 
	   
	IF @REGION='WEST'
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @NEXT_STATUS VARCHAR(50)='UNKNOWN'
				DECLARE @CURRENT_STATUS VARCHAR(50)='UNKNOWN'
				DECLARE @FINAL_APPROVAL_AMOUNT MONEY=25000

				IF @USERNAME='HOWIE ZANDER' -- OR @USERROLE = 'KENNA' 
					BEGIN
						SET @CURRENT_STATUS = 'PENDING APPROVAL - FINAL'
						SET @NEXT_STATUS = 'APPROVED'
					END
				ELSE IF @USERROLE = 'RC-TL' AND @USERNAME='DONNELLY MCEWEN'
					BEGIN
						SET @CURRENT_STATUS = 'SUBMITTED'
						SET @NEXT_STATUS = 'PENDING APPROVAL - HO'
					END
				ELSE IF @USERROLE='HEADOFFICE' AND @USERNAME='TIM BODVARSON'
					BEGIN
						SET @CURRENT_STATUS = 'PENDING APPROVAL - HO'
						SET @NEXT_STATUS = 'UNKNOWN'
					END 
							
				DELETE FROM @EDIT_HISTORY

				-- UPDATE STATUS APPROVAL  EXCEPTIONS TABLE
				IF  @NEXT_STATUS = 'UNKNOWN'
					UPDATE T1
					SET [Status] =  IIF(T1.exception_value >= @FINAL_APPROVAL_AMOUNT, 'PENDING APPROVAL - FINAL', 'APPROVED') 
						,Show_In_UL = 1 
						,DateModified = GetDate()
						,PostedBy = @USERNAME
					OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
					FROM RPWeb_Exceptions T1
						INNER JOIN @Exception T2
						ON T2.ID=T1.ID 					
					WHERE T1.[Status]=@CURRENT_STATUS AND T2.validationStatus = 'PASSED'
				ELSE
					UPDATE T1
					SET [Status] = @NEXT_STATUS			
						,Show_In_UL =  1 
						,DateModified = GetDate()
						,PostedBy = @USERNAME
					OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
					FROM RPWeb_Exceptions T1
						INNER JOIN @Exception T2
						ON T2.ID=T1.ID 
					WHERE T1.[Status]=@CURRENT_STATUS AND T2.validationStatus = 'PASSED'

						
				---MERGE RB_WEB_Rewards_SUMMARY TABLE LOCKED STATEMENTS 
				-- APPLICABLE FOR SEASON 2020
				IF @SEASON=2020
				BEGIN
					MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
					USING (
				
						SELECT E.StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements S 
							ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType = 'Locked' AND S.Status = 'Active'
						WHERE E.Status = 'Approved' 
							AND Show_IN_UL  = 1
							AND E.VersionType = 'Current'
							AND RewardCode = @REWARD_CODE
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.StatementCode,E.RewardCode
				
						UNION
				
				
						SELECT E.StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements S 
							ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType = 'Locked' AND S.Status = 'Active'
						WHERE E.Status = 'Approved' 
							AND Show_IN_UL  = 1
							AND E.VersionType = 'Current'
							AND RewardCode = @OTHER_REWARD_CODE
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.StatementCode,E.RewardCode
					) AS SRC
					ON TGT.StatementCode = SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND SRC.MarketLetterCode = TGT.MarketLetterCode    
				  WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
						INSERT  (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment)  
						VALUES (SRC.StatementCode,SRC.MarketLetterCode,SRC.RewardCode,SRC.Amount,'Eligible','',SRC.Amount,0,SRC.Amount,0) 
				   WHEN MATCHED AND SRC.RewardCode = @OTHER_REWARD_CODE THEN
						UPDATE  SET TGT.RewardValue = SRC.Amount,TGT.TotalReward = SRC.Amount,TGT.CurrentPayment = SRC.Amount,TGT.NextPayment = 0
					;
										
					---MERGE RBWEB_Rewards_SUMMARY TABLE UNLOCKED STATEMENTS 
		
					MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
					USING (				
						SELECT E.Src_StatementCode AS StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements S 
							ON S.StatementCode = E.SRC_StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
						WHERE E.Status = 'Approved'
							AND E.VersionType = 'Current'
							AND Show_IN_UL  = 1
							AND RewardCode = @REWARD_CODE
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.Src_StatementCode,E.RewardCode

						UNION
				
						SELECT E.Src_StatementCode AS StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements S 
							ON S.StatementCode = E.SRC_StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
						WHERE E.Status = 'Approved' 
							AND Show_IN_UL  = 1
							AND E.VersionType = 'Current'
							AND RewardCode = @OTHER_REWARD_CODE
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.Src_StatementCode,E.RewardCode
					) AS SRC
					ON TGT.StatementCode = SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND SRC.MarketLetterCode = TGT.MarketLetterCode        
				   WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
						INSERT  (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment)  
						VALUES (SRC.StatementCode,SRC.MarketLetterCode,SRC.RewardCode,SRC.Amount,'Eligible','',SRC.Amount,0,SRC.Amount,0) 
				   WHEN MATCHED AND SRC.RewardCode = @OTHER_REWARD_CODE THEN
						UPDATE  SET TGT.RewardValue = SRC.Amount,TGT.TotalReward = SRC.Amount,TGT.CurrentPayment = SRC.Amount,TGT.NextPayment = 0
					;
				END -- END OF SEASON 2020

				-- NEW LOGIC STARTING 2021 - BEGIN
				IF @SEASON >= 2021
				BEGIN					
					IF @REWARD_CODE = 'INV_LLB_WX'
						INSERT INTO @APPROVED_EXCEPTIONS(StatementCode,RewardCode,MarketLetterCode,Amount)
						SELECT E.StatementCode,E.RewardCode, 'ML' + @SEASON_STRING + '_W_INV' AS MarketLetterCode, SUM(E.Exception_Value_A) AS Amount					
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements			S 	
							ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
						WHERE E.Status = 'Approved'
							AND E.VersionType = 'Current'
							AND Show_IN_UL  = 1
							AND RewardCode = 'INV_LLB_WX'
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.StatementCode,E.RewardCode

							UNION ALL

						SELECT E.StatementCode,E.RewardCode, 'ML' + @SEASON_STRING +  '_W_CPP' AS MarketLetterCode, SUM(E.Exception_Value_B) AS Amount					
						FROM RPWeb_Exceptions E
							INNER JOIN RPWeb_Statements			S 	
							ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
						WHERE E.Status = 'Approved'
							AND E.VersionType = 'Current'
							AND Show_IN_UL  = 1
							AND RewardCode = 'INV_LLB_WX'
							AND E.Season = @SEASON
							AND E.StatementCode = @STATEMENTCODE
						GROUP BY E.StatementCode,E.RewardCode
					ELSE
						BEGIN					
							INSERT INTO @APPROVED_EXCEPTIONS(StatementCode,RewardCode,MarketLetterCode,Amount)
							SELECT E.StatementCode,E.RewardCode, @MARKET_LETTER_CODE AS MarketLetterCode, SUM(E.Exception_Value) AS Amount					
							FROM RPWeb_Exceptions E
								INNER JOIN RPWeb_Statements			S 	
								ON S.StatementCode = E.StatementCode and S.Region = @REGION AND S.VersionType= 'Unlocked' and S.Status = 'Active'
							WHERE E.Status = 'Approved'
								AND E.VersionType = 'Current'
								AND Show_IN_UL  = 1
								AND RewardCode IN (@REWARD_CODE,@OTHER_REWARD_CODE)
								AND E.Season = @SEASON
								AND E.StatementCode = @STATEMENTCODE
							GROUP BY E.StatementCode,E.RewardCode

							-- REWARDS WHICH GOES ON TO SINGLE MARKET LETTER ONLY
							IF @REWARD_CODE NOT IN ('OTHER','OTHER_W_X','Other_WX')
								UPDATE T1
								SET MarketLetterCode = ML.MArketLetterCode
								FROM @APPROVED_EXCEPTIONS T1
									INNER JOIN RPWeb_ML_ELG_Programs ML 									
									ON ML.StatementCode = T1.StatementCode							
									INNER JOIN (
										SELECT ProgramCode, RewardCode + 'X' AS RewardCode
										FROM RP_Config_Programs	
										WHERE Season=@SEASON AND Region='West' AND ISNULL(RewardCode,'') <> ''
									) P	
									ON P.ProgramCode=ML.ProgramCode AND P.RewardCode=T1.RewardCode 
						END						

						MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
						USING @APPROVED_EXCEPTIONS AS SRC
						ON TGT.StatementCode=SRC.StatementCode AND SRC.RewardCode=TGT.RewardCode AND SRC.MarketLetterCode=TGT.MarketLetterCode        
							WHEN NOT MATCHED AND SRC.Amount > 0 THEN  
							INSERT (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment)  
							VALUES (SRC.StatementCode,SRC.MarketLetterCode,SRC.RewardCode,SRC.Amount,'Eligible','',SRC.Amount,0,0,SRC.Amount) 
						WHEN MATCHED AND SRC.RewardCode = @OTHER_REWARD_CODE THEN
							UPDATE SET TGT.RewardValue=SRC.Amount ,TGT.TotalReward=SRC.Amount ,TGT.PaidToDate=0 ,TGT.CurrentPayment=0 ,TGT.NextPayment=SRC.Amount
						;													
				END	-- NEW LOGIC STARTING 2021 - ENDS
				
				EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY

				/*
				INSERT INTO @REFRESH_STATEMENTS(Statements)
				VALUES(@STATEMENTCODE)

				EXEC RP_RefreshPaymentInformation @SEASON, 'Actual', @REFRESH_STATEMENTS
				*/

				COMMIT TRANSACTION
		END TRY
					
		BEGIN CATCH
			SELECT 'failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
			ROLLBACK TRANSACTION			
		END CATCH
	END --END WEST

FINAL:
	SELECT 'success' AS Status, '' AS ErrorMessage, '' as confirmation_message, '' as warning_message
END

