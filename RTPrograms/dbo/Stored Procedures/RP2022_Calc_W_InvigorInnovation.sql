﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_InvigorInnovation] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'REWARD PLANNER'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_INV_INNOVATION_BONUS'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (8956)
	*/

	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;
	DECLARE @InVigor300_Percentage_Coop DECIMAL(6,4) = 0.028;
	DECLARE @InVigor200_Percentage_Coop DECIMAL(6,4) = 0.010;
	DECLARE @InVigor300_Percentage_Ind DECIMAL(6,4) = 0.030;
	DECLARE @InVigor200_Percentage_Ind DECIMAL(6,4) = 0.010;
	DECLARE @Qualifying_Percentage DECIMAL(6,4) = 0.35;

	DECLARE @TEMP TABLE (
		StatementCode INT,
		Level5Code VARCHAR(20) NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		ProductCode VARCHAR(50) NOT NULL,
		InVigor_Bags DECIMAL NOT NULL,
		Innovation_Bags DECIMAL NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Innovation_Sales MONEY NOT NULL DEFAULT 0,
		InVigor300_Sales MONEY NOT NULL,
		InVigor200_Sales MONEY NOT NULL,
		Qualifying_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		InVigor300_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		InVigor200_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		InVigor300_Reward MONEY NOT NULL DEFAULT 0,
		InVigor200_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED  (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC,
			[ProductCode] ASC
		)
	)

	DECLARE @TOTAL_TEMP TABLE (
		StatementCode INT,
		Level5Code VARCHAR(20) NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		InVigor_Bags DECIMAL NOT NULL,
		Innovation_Bags DECIMAL NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Innovation_Sales MONEY NOT NULL DEFAULT 0,
		InVigor300_Sales MONEY NOT NULL,
		InVigor200_Sales MONEY NOT NULL,
		Qualifying_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		InVigor300_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		InVigor200_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		InVigor300_Reward MONEY NOT NULL DEFAULT 0,
		InVigor200_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED  (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	-- NEW: RRT-2425 - I think the qualifiying percent needs to be calculated at the statement level not retail level
	DECLARE @QUALIFYING_PERC TABLE (
		StatementCode INT,
		Innovation_Bags DECIMAL NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Innovation_Sales MONEY NOT NULL,
		Qualifying_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0
	)

	-- Insert reward data into temporary table variable
	INSERT INTO @TEMP(StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand, ProductCode, InVigor_Bags, Innovation_Bags, RewardBrand_Sales, InVigor300_Sales, InVigor200_Sales, Innovation_Sales)
	SELECT ST.StatementCode, TX.Level5Code, TX.RetailerCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode,
		SUM(TX.Acres_CY / 10) AS InVigor_Bags,
		SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][3][0-9]%' OR PRH.Hybrid_Name = 'L234PC', TX.Acres_CY / 10, 0)) AS Innovation_Bags,
		SUM(TX.Price_CY) AS RewardBrand_Sales,
		SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][3][0-9]%', TX.Price_CY, 0)) AS InVigor300_Sales,
		SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][2][0-9]%', TX.Price_CY, 0)) AS InVigor200_Sales,
		SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][3][0-9]%' OR PRH.Hybrid_Name = 'L234PC', TX.Price_CY, 0)) AS Innovation_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
		INNER JOIN ProductReference_Hybrids PRH ON PR.Hybrid_ID = PRH.ID
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.Level5Code, TX.RetailerCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN		
		-- Apply radius percentages
	   	UPDATE T1
		SET InVigor_Bags = InVigor_Bags * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Innovation_Bags = Innovation_Bags * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			RewardBrand_Sales = RewardBrand_Sales * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			InVigor300_Sales = InVigor300_Sales * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			InVigor200_Sales = InVigor200_Sales * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Innovation_Sales = Innovation_Sales * (ISNULL(E1.FieldValue,100)/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1)
		FROM @TEMP T1
		INNER JOIN ProductReference PR ON T1.ProductCode = PR.ProductCode
		INNER JOIN RPWeb_User_EI_FieldValues E1 ON E1.StatementCode = T1.StatementCode AND E1.MarketLetterCode = T1.MarketLetterCode AND E1.FieldCode = 'Radius_Percentage_InVigor'
		LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = PR.ChemicalGroup
		LEFT JOIN RPWeb_User_BrandInputs BI2 ON BI2.StatementCode = T1.StatementCode AND BI2.ChemicalGroup = PR.Product
		--LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = T1.RewardBrand

	END

	INSERT INTO @TOTAL_TEMP(StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand, InVigor_Bags, Innovation_Bags, RewardBrand_Sales, InVigor300_Sales, InVigor200_Sales, Innovation_Sales)
	SELECT StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand,
		SUM(InVigor_Bags), SUM(Innovation_Bags), SUM(RewardBrand_Sales),
		SUM(InVigor300_Sales), SUM(InVigor200_Sales), SUM(Innovation_Sales)
	FROM @TEMP
	GROUP BY StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand

	-- NEW: RRT-2425 - I think the qualifiying percent needs to be calculated at the statement level not retail level
	-- Get the qualifying percentage for each statement
	-- UPDATE @TOTAL_TEMP SET Qualifying_Percentage = IIF(Innovation_Bags >= 20, Innovation_Sales / RewardBrand_Sales, 0)
	INSERT INTO @QUALIFYING_PERC(StatementCode, Innovation_Bags, RewardBrand_Sales, Innovation_Sales)
	SELECT StatementCode, SUM(Innovation_Bags), SUM(RewardBrand_Sales), SUM(Innovation_Sales)
	FROM @TOTAL_TEMP
	GROUP BY StatementCode
	UPDATE @QUALIFYING_PERC SET Qualifying_Percentage = IIF(Innovation_Bags >= 20, Innovation_Sales / RewardBrand_Sales, 0)
	UPDATE t1
	SET t1.Qualifying_Percentage = q1.Qualifying_Percentage
	FROM @TOTAL_TEMP t1
	JOIN @QUALIFYING_PERC q1 ON t1.StatementCode = q1.StatementCode

	-- Set the reward percentage for InVigor 300 and InVigor 200
	UPDATE @TOTAL_TEMP
		SET InVigor300_Percentage = CASE Level5Code WHEN 'D000001' THEN @InVigor300_Percentage_Ind ELSE @InVigor300_Percentage_Coop END,
			InVigor200_Percentage = CASE Level5Code WHEN 'D000001' THEN @InVigor200_Percentage_Ind ELSE @InVigor200_Percentage_Coop END 
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage

	-- Calculate the InVigor 300 and 200 Series reward for each statement
	UPDATE @TOTAL_TEMP
		SET InVigor300_Reward = IIF(InVigor300_Sales > 0, InVigor300_Sales * InVigor300_Percentage, 0),
			InVigor200_Reward = IIF(InVigor200_Sales > 0, InVigor200_Sales * InVigor200_Percentage, 0)
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage

	-- Calculate the total reward as InVigor 300 reward + InVigor 200 reward
	UPDATE @TOTAL_TEMP
		SET Reward = InVigor300_Reward + InVigor200_Reward
	WHERE InVigor300_Reward > 0 OR InVigor200_Reward > 0

	-- Insert values from temporary table into permanent table
	DELETE FROM RP2022_DT_Rewards_W_INV_INNOV WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2022_DT_Rewards_W_INV_INNOV(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode
	FROM @TOTAL_TEMP

END