﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_CLL] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	/*
	 MP: Code review on Sep/06/2021
		 This program goes on to single market letter i.e CCP
		 There is no point maintaing market letter code and program code in temp tables, waste of additional joins.
		
		I am removing market letter code and program code columns from Temp tables. If required pull a back up before Sep/6/2021 for comparison.
		By removing market letter code and program code from temp table, procedure would run faster.
		Same needs to be done in other programs as well
	*/

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CCP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'

	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[RetailerCode] VARCHAR(20) NOT NULL,
		[FarmCode] VARCHAR(20) NOT NULL,
		[Brand] VARCHAR(100) NOT NULL,
		[Brand_Acres] DECIMAL(18,4) NOT NULL,
		[Brand_Sales] MONEY NOT NULL
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC,
			[Brand] ASC
		)
	)

	DECLARE @FINAL_RESULTS TABLE (
		[StatementCode] INT NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[RetailerCode] VARCHAR(20) NOT NULL,
		[FarmCode] VARCHAR(20) NOT NULL,
		[CLL_Acres] DECIMAL(18,4) NOT NULL,
		[CLL_Sales] MONEY NOT NULL,
		[CLL_Herb_Acres] DECIMAL(18,4) NOT NULL,
		[CLL_Herb_Sales] MONEY NOT NULL,
		[Matched_Acres] DECIMAL(18,4) NOT NULL,
		[Matched_Sales] MONEY NOT NULL DEFAULT 0,
		[Reward_Percentage] DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC
		)
	)
	
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		-- NEW: RRT-2068 - original code was not matching herb acres with CLL at the family level

		-- Get data from sales consolidated
		-- Difference here is I am replacing TX.RetailerCode with S.RetailerCode - I want all rows to be a single R-code
		INSERT INTO @TEMP(StatementCode, Level5Code, RetailerCode, FarmCode, Brand, Brand_Acres, Brand_Sales)
		SELECT ST.StatementCode, TX.Level5Code, S.RetailerCode, 'FXXXXX' [FarmCode], PR.ChemicalGroup,
				SUM(CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Acres_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Acres_CY END) [Brand_Acres],
				SUM(CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Price_CY END) [Brand_Sales]
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
			INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
			INNER JOIN RPWeb_User_EI_FieldValues EI ON ST.StatementCode = EI.StatementCode AND EI.MarketLetterCode=@CCP_ML_CODE AND EI.FieldCode = 'Radius_Percentage_CPP'
			INNER JOIN RPWeb_Statements S ON ST.StatementCode = S.StatementCode
		WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.Level5Code,  S.RetailerCode, PR.ChemicalGroup

		-- Match acres
		-- Difference here is we are producing 1 total row instead of X total rows where X = number of locations in the family
		-- To calculate matched sales, if matched herb acres is less than total herb acres, we take the % of matched vs total and multiple by total herb sales
		INSERT INTO @FINAL_RESULTS(StatementCode, Level5Code, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Matched_Sales, Reward_Percentage)
		SELECT StatementCode, Level5Code, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, 
			CASE WHEN CLL_Acres > CLL_Herb_Acres THEN CLL_Herb_Acres ELSE CLL_Acres END AS 'Matched_Acres',
			ISNULL(CASE WHEN CLL_Acres > CLL_Herb_Acres THEN CLL_Herb_Sales ELSE CLL_Herb_Sales * (CLL_Acres / NULLIF(CLL_Herb_Acres,0)) END,0) AS 'Matches_Sales',
			CASE Level5Code WHEN 'D0000117' THEN 0.0275 ELSE 0.03 END  AS 'Reward_Percentage'
		FROM (
			SELECT
				StatementCode, Level5Code, RetailerCode, FarmCode,
				SUM(CASE Brand WHEN 'CLEARFIELD LENTILS' THEN Brand_Acres ELSE 0 END) [CLL_Acres],
				SUM(CASE Brand WHEN 'CLEARFIELD LENTILS' THEN Brand_Sales ELSE 0 END) [CLL_Sales],
				SUM(CASE WHEN Brand <> 'CLEARFIELD LENTILS' THEN Brand_Acres ELSE 0 END) [CLL_Herb_Acres],
				SUM(CASE WHEN Brand <> 'CLEARFIELD LENTILS' THEN Brand_Sales ELSE 0 END) [CLL_Herb_Sales]
			FROM @TEMP
			GROUP BY StatementCode, Level5Code, RetailerCode, FarmCode
		) Data_SUM

		UPDATE @FINAL_RESULTS SET [Reward] = [Matched_Sales] * [Reward_Percentage]				

		
		-- Copy data from temporary table to permanent table
		--FORECAST PROJECTION TABLE
		DELETE FROM RP2021_DT_Rewards_W_ClearfieldLentils WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

		INSERT INTO RP2021_DT_Rewards_W_ClearfieldLentils(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Reward_Percentage, Reward)
		SELECT StatementCode, @CCP_ML_CODE as MarketLetterCode, @PROGRAM_CODE as ProgramCode, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Reward_Percentage, Reward
		FROM @FINAL_RESULTS

		RETURN

	END -- END OF FORECAST , PROJECTION

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN		
		DECLARE @FARMCODES TABLE (
			Farmcode VARCHAR(20),
			PRIMARY KEY CLUSTERED (
				Farmcode ASC
			)
		)

		
		DECLARE @CUT_OFF_DATE DATE = CAST('2021-07-09' AS DATE)
		
		-- TEMP HERBICIDE SALES
		DROP TABLE IF EXISTS #TEMP_HERB_SALES

		SELECT ST.Statementcode, S.VersionType, S.StatementLevel, S.Level5Code, S.Retailercode, tx.FarmCode, tx.ChemicalGroup ,tx.Acres AS [Acres]				
			,CASE 
				WHEN S.StatementLevel='LEVEL 5' THEN tx.Price_SWP 
				WHEN S.Level5Code='D0000117' THEN tx.Price_SRP 
				ELSE tx.Price_SDP 
			END [Sales]			
			,CAST(IIF(PR.ConversionW=0, 0,	
					CASE 
						WHEN S.StatementLevel='LEVEL 5' THEN PRP.SWP
						WHEN S.Level5Code='D0000117' THEN PRP.SRP
						ELSE PRP.SDP 
					END)/PR.ConversionW AS MONEY) AS [PPA]
			,TX.RetailerCode AS EffectiveRetailerCode
		INTO #TEMP_HERB_SALES
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements S				ON TX.StatementCode=S.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
			INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID 			
			INNER JOIN ProductReference	PR			ON PR.ProductCode=TX.ProductCode
			INNER JOIN ProductReferencePricing PRP	ON TX.ProductCode=PRP.ProductCode AND PRP.Season=@SEASON AND PRP.Region='WEST'
		WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD LENTILS'		
			AND TX.FarmCode NOT IN ('F520020402','F520040993')

		DELETE FROM #TEMP_HERB_SALES WHERE Level5Code <> 'D520062427' AND ChemicalGroup IN ('DUET','MIZUNA')

		-- OU AND HEAD OFFICE BREAK DOWN	
		DELETE T1
		FROM RP_DT_HO_BreakDownSummary T1
			INNER JOIN @STATEMENTS ST
			ON ST.Statementcode=T1.Statementcode
		WHERE T1.ProgramCode=@PROGRAM_CODE 

		INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
		SELECT tx.Statementcode, @CCP_ML_CODE, @PROGRAM_CODE, tx.EffectiveRetailerCode, tx.Farmcode,SUM(tx.Sales) AS Sales, 1		
		FROM #TEMP_HERB_SALES tx					
		WHERE tx.StatementLevel IN ('LEVEL 2','LEVEL 5')
		GROUP BY tx.Statementcode,tx.EffectiveRetailerCode ,tx.Farmcode


		DROP TABLE IF EXISTS #Herbicides

		SELECT Statementcode, VersionType, Level5Code, Retailercode, FarmCode, ChemicalGroup ,PPA		
			,SUM(Sales) AS [Sales]						
			,SUM(Acres) AS [Acres]			
			,CAST(0 AS DECIMAL(18,2)) AS MatchedAcres
			,CAST(0 AS MONEY) AS MatchedSales
			,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
			,CAST(0 AS MONEY) AS Reward
			,ROW_NUMBER() OVER(PARTITION BY [StatementCode] ORDER BY [PPA] DESC) AS [MatchingOrder]	
		INTO #Herbicides
		FROM #TEMP_HERB_SALES D
		GROUP BY Statementcode, VersionType, Level5Code, Retailercode, FarmCode, ChemicalGroup, PPA
			
		INSERT INTO @FARMCODES(Farmcode)
		SELECT DISTINCT Farmcode FROM #Herbicides
		
		-- CLEARFIELD LENTIL COMMITED ACRES	
		-- Statementcode INT,
		 DECLARE @Lentils TABLE (		
			Farmcode VARCHAR(20) NOT NULL,
			Acres Decimal(18,2) NOT NULL
		 )

		-- use case we refresh one at a time	
		IF EXISTS(SELECT * FROM #Herbicides WHERE VERSIONTYPE='USE CASE')	
			INSERT INTO @Lentils(Farmcode, Acres)
			SELECT Farmcode, SUM(Acres) Acres
			FROM (
				SELECT Farmcode, ProductCode, MAX(Acres) Acres
				FROM (
					SELECT WC.Farmcode, WCV.ProductCode, WCV.Acres ,WCV.RegistrationDate
					FROM @STATEMENTS ST			
						INNER JOIN RP_DT_Commitments WC					ON WC.StatementCode=ST.Statementcode 
						INNER JOIN RP_DT_CommitmentsVarietyInfo WCV 	ON WCV.StatementCode=ST.Statementcode AND WCV.RP_WC_ID=WC.RP_ID
						INNER JOIN @Farmcodes F							ON WC.Farmcode=F.Farmcode												
					WHERE WC.CommitmentType='Lentils'  AND WC.[Status]='Complete' AND WCV.BASFSeason=@SEASON 
				) CL
				WHERE  CAST(RegistrationDate AS DATE) <= @CUT_OFF_DATE
				GROUP BY Farmcode, ProductCode		
			) D
			GROUP BY Farmcode		
		ELSE
			INSERT INTO @Lentils(Farmcode, Acres)
			SELECT Farmcode, SUM(Acres) Acres
			FROM (
				SELECT Farmcode, ProductCode, MAX(Acres) Acres
				FROM (
					SELECT WC.[Parentcode] AS Farmcode, WCV.ProductCode, WCV.Acres 					
						,IIF(ISDATE(WCV.RegistrationDate)=1,WCV.RegistrationDate,NULL) AS RegistrationDate
					FROM @Farmcodes F
						INNER JOIN WebCommitments WC				ON WC.Parentcode=F.Farmcode
						INNER JOIN WebCommitmentsVarietyInfo WCV 	ON WCV.DocCode=WC.DocCode
					WHERE WC.CommitmentType='Lentils' AND WC.[Status]='Complete' AND WCV.Season=@SEASON AND WCV.RegistrationDate IS NOT NULL 
				) CL
				WHERE CAST(RegistrationDate AS DATE) <= @CUT_OFF_DATE
				GROUP BY Farmcode, ProductCode
			) D
			GROUP BY Farmcode
	
		UPDATE T1
		SET MatchedAcres=IIF(T2.[RunningTotal] <= CLL.Acres, T1.Acres,T1.Acres+(CLL.[Acres]-T2.[RunningTotal]))
		FROM #Herbicides T1
			INNER JOIN @Lentils CLL		
			ON CLL.FarmCode=T1.FarmCode 
			INNER JOIN (
				SELECT [StatementCode],[RetailerCode],[Farmcode],[ChemicalGroup],[MatchingOrder]
					,SUM(Acres) OVER(PARTITION BY [StatementCode],[RetailerCode],[Farmcode] ORDER BY [MatchingOrder] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
				FROM #Herbicides
			) T2
			ON T2.Statementcode=T1.StatementCode AND T2.RetailerCode=T1.RetailerCode AND T2.FarmCode=T1.FarmCode AND T2.ChemicalGroup=T1.ChemicalGroup AND T2.MatchingOrder=T1.MatchingOrder
		WHERE (T2.[RunningTotal] <= CLL.Acres  OR T1.Acres+(CLL.[Acres]-T2.[RunningTotal]) > 0)
				
		-- UPDATE MATCHING SALES, REWARD PERCENTAGE AND REWARD
		-- FCL : 2.75% , All others 3%
		UPDATE #Herbicides SET [MatchedSales]=[Sales] * ([MatchedAcres]/[Acres]) WHERE [Acres] > 0
		UPDATE #Herbicides SET [Reward_Percentage] = IIF(Level5Code='D0000117',0.0275,0.03) WHERE [MatchedSales] > 0
		UPDATE #Herbicides SET [Reward]=[MatchedSales] * [Reward_Percentage] WHERE [Reward_Percentage] > 0
		
		-- Copy data from temporary table to permanent table
		--ACTUALS TABLE
		DELETE FROM RP2021_DT_Rewards_W_ClearfieldLentils_Actual WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

		INSERT INTO RP2021_DT_Rewards_W_ClearfieldLentils_Actual(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,MatchingOrder,Acres,Sales,MatchedAcres,MatchedSales,Reward_Percentage,Reward)
		SELECT StatementCode, @CCP_ML_CODE, @PROGRAM_CODE, RetailerCode, FarmCode,ChemicalGroup
			,MIN(MatchingOrder) AS MatchingOrder
			,SUM(Acres) AS Acres
			,SUM(Sales) AS Sales
			,SUM(MatchedAcres) AS MatchedAcres
			,SUM(MatchedSales) AS MatchedSales
			,MAX(Reward_Percentage) Reward_Percentage
			,SUM(Reward) AS Reward
		FROM #Herbicides
		GROUP BY StatementCode, RetailerCode, FarmCode, ChemicalGroup
	
			UNION

		SELECT T2.Statementcode, @CCP_ML_CODE, @PROGRAM_CODE,  T2.RetailerCode, T1.FarmCode,'CLEARFIELD LENTILS' AS RewardBrand, 0 AS MatchingOrder, Acres, 0 as Sales, 0 as MatchedAcres, 0 as MatchedSales, 0 as Reward_Percentage, 0 as Reward
		FROM @Lentils T1
			INNER JOIN (
				SELECT DISTINCT StatementCode, RetailerCode, FarmCode FROM #Herbicides 
			) T2
			ON T2.FarmCode=T1.FarmCode

	END --END ACTUAL

END




