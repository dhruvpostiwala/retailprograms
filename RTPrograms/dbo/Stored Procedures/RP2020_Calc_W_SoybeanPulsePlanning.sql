﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_W_SoybeanPulsePlanning] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

		
	/*
		IMI Herbicide Brands: IMI Herbicide Brands: Odyssey® NXT herbicide, Odyssey Ultra NXT, Solo® ADV herbicide, Solo Ultra, Viper® ADV herbicide (all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT)

		Part A: Retails receive a 2% payment when they maintain 95% growth YOY & a 4% payment when they maintain 100% growth YOY on 2020 sales of eligible IMI Herbicide brands.
		Part B: Retails receive an additional 1% payment on their 2020 POGs of IMI Herbicides when they meet or exceed their Heat LQ/Heat Complete Target.
				Target must be a minimum of 100% of 2019 POG$ of Heat LQ/Heat Complete. Target is combined across both brands.
		Part C: Retails receive an additional 1% payment on their 2020 POGs of IMI Herbicides when they meet or exceed their Priaxor/Dyax Bonus Target.
				Target must be a minimum of 100% of 2019 POG$ of Priaxor/Dyax. Target is combined across both brands.
	*/

	/*
		NEW: RRT-720 - the editable inputs for Part B and C targets are no longer actual targets, they are percentages.
		We must change the Part B and C bonus calculations to see if the retail has exceeded 2019 POG Sales x <input %> to qualify
	*/


	/*
		FOR ACTUALS

		SOYBEAN/PULSE PLANNING AND SUPPORT REWARD
		Reward Brands IMI herbicides: Odyssey NXT herbicide, Odyssey Ultra NXT, Solo ADV herbicide, Solo Ultra, Viper ADV herbicide (all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT)

		Qualifying Level: Level 2. For standalone will be at level 1.
		Calculating Level: Level 2. For standalone will be at level 1.

		Rounding Rules: 0.5% included in qualification of reward tiers.

		Reward Opportunity
		Retails can earn up to 6% on their 2020 Eligible POG Sales of Reward Brands IMI herbicides.

		--********** REQUIREMENTS **********--				
			For Independents
				•	Final plan due February 11th, 2020. (buffer included)
				•	Retails must place order with Retail Connect for IMI herbicides by April 29th, 2020 based on their February 11th, 2020 final POG plan. 
					Their order must be at least 60% of their final IMI herbicide POG plan to qualify for the reward.
					
					Need to meet following three conditions 
					o	Final Plan 
					o	Order Date
					o	Order Amount/Amount Planned should be minimum of 60%
		
			For COOPS:
				•	Retails must have a plan including IMI herbicides by February 11th, 2020. (buffer included)
	
	*/



	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode in (2)
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_SOYBEAN_PULSE_REWARD'


	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	DECLARE @REFRESHING_VERSION_TYPE AS VARCHAR(20)='UNLOCKED'

	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		GroupLabel VARCHAR(100) NOT NULL,
		PartA_QualSales_CY MONEY NOT NULL,
		PartA_QualSales_LY MONEY NOT NULL,
		PartA_Reward_Sales MONEY NOT NULL,
		PartA_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,	
		PartA_Reward MONEY NOT NULL DEFAULT 0,		
		PartB_QualSales_CY MONEY NOT NULL,
		PartB_QualSales_LY MONEY NOT NULL,
		PartB_Reward_Sales MONEY NOT NULL DEFAULT 0,
		PartB_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,	
		PartB_Reward MONEY NOT NULL DEFAULT 0,		
		PartC_QualSales_CY MONEY NOT NULL,
		PartC_QualSales_LY MONEY NOT NULL,
		PartC_Reward_Sales MONEY NOT NULL DEFAULT 0,
		PartC_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,	
		PartC_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		PartA_QualSales_CY MONEY NOT NULL,
		PartA_QualSales_LY MONEY NOT NULL,
		PartA_Reward_Percentage FLOAT NOT NULL DEFAULT 0,		
		PartB_QualSales_CY MONEY NOT NULL,
		PartB_QualSales_LY MONEY NOT NULL,
		PartB_Reward_Percentage FLOAT NOT NULL DEFAULT 0,		
		PartC_QualSales_CY MONEY NOT NULL,
		PartC_QualSales_LY MONEY NOT NULL,
		PartC_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,	
		Growth MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)


	DECLARE @REQUIREMENTS TABLE(
		Statementcode INT,		
		VersionType VARCHAR(20),
		StatementLevel VARCHAR(20),
		Level5Code VARCHAR(20),
		Plan_Date DATE,
		Plan_Amount MONEY,
		Order_Amount MONEY,
		Plan_Passed BIT,
		Orders_Passed BIT		
	)

	IF EXISTS(SELECT Statementcode FROM @STATEMENTS WHERE StatementCode IN (SELECT Statementcode FROM RP_Statements WHERE StatementType='Actual' AND VersionType='Use Case'))
		SET @REFRESHING_VERSION_TYPE='USE CASE'
	   	 

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,PartA_QualSales_CY,PartA_QualSales_LY,PartA_Reward_Sales,PartB_QualSales_CY,PartB_QualSales_LY,PartB_Reward_Sales,PartC_QualSales_CY,PartC_QualSales_LY,PartC_Reward_Sales)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel,
		SUM(CASE WHEN tx.GroupType = 'REWARD_BRANDS' THEN tx.Price_CY ELSE 0 END) AS PartA_QualSales_CY,
		SUM(CASE WHEN tx.GroupType = 'REWARD_BRANDS' THEN tx.Price_LY1 ELSE 0 END) AS PartA_QualSales_LY,
		SUM(CASE WHEN tx.GroupType = 'REWARD_BRANDS' THEN tx.Price_CY ELSE 0 END) AS PartA_Reward_Sales,
		SUM(CASE WHEN tx.GroupType = 'QUALIFYING_BRANDS' AND tx.GroupLabel NOT IN ('PRIAXOR','DYAX') THEN tx.Price_CY ELSE 0 END) AS PartB_QualSales_CY,
		SUM(CASE WHEN tx.GroupType = 'QUALIFYING_BRANDS' AND tx.GroupLabel NOT IN ('PRIAXOR','DYAX') THEN tx.Price_LY1 ELSE 0 END) AS PartB_QualSales_LY,
		SUM(CASE WHEN tx.GroupType = 'REWARD_BRANDS' THEN tx.Price_CY ELSE 0 END) AS PartB_Reward_Sales,
		SUM(CASE WHEN tx.GroupType = 'QUALIFYING_BRANDS' AND tx.GroupLabel IN ('PRIAXOR','DYAX') THEN tx.Price_CY ELSE 0 END) AS PartC_QualSales_CY,
		SUM(CASE WHEN tx.GroupType = 'QUALIFYING_BRANDS' AND tx.GroupLabel IN ('PRIAXOR','DYAX') THEN tx.Price_LY1 ELSE 0 END) AS PartC_QualSales_LY,
		SUM(CASE WHEN tx.GroupType = 'REWARD_BRANDS' THEN tx.Price_CY ELSE 0 END) AS PartC_Reward_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements RS					
		ON RS.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode = @PROGRAM_CODE 
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel

	--Extra inserts of record needed to insert Duet and Mizuna for only Nutrien
	--RP 2954
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,PartA_QualSales_CY,PartA_QualSales_LY,PartA_Reward_Sales,PartB_QualSales_CY,PartB_QualSales_LY,PartB_Reward_Sales,PartC_QualSales_CY,PartC_QualSales_LY,PartC_Reward_Sales)
	SELECT ST.StatementCode, 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, 'RP2020_W_SOYBEAN_PULSE_REWARD' AS ProgramCode, tx.GroupLabel,
		SUM(tx.Price_CY) AS PartA_QualSales_CY,
		SUM(tx.Price_LY1) AS PartA_QualSales_LY,
		SUM(tx.Price_CY) AS PartA_Reward_Sales,
		0 AS PartB_QualSales_CY,
		0 AS PartB_QualSales_LY,
		SUM(tx.Price_CY) AS PartB_Reward_Sales,
		0 AS PartC_QualSales_CY,
		0 AS PartC_QualSales_LY,
		SUM(tx.Price_CY) AS PartC_Reward_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements RS					
		ON RS.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode = 'NON_REWARD_BRANDS_SUMMARY' AND RS.Level5Code IN ('D520062427') AND GroupLabel in ('DUET','MIZUNA')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel

	-- Determine growth based on the sum of brand sales
	INSERT INTO @TEMP_TOTALS(StatementCode,MarketLetterCode,ProgramCode,PartA_QualSales_CY,PartA_QualSales_LY,PartB_QualSales_CY,PartB_QualSales_LY,PartC_QualSales_CY,PartC_QualSales_LY)
	SELECT StatementCode, MarketLetterCode, ProgramCode		
		,SUM(PartA_QualSales_CY) AS PartA_QualSales_CY, SUM(PartA_QualSales_LY) AS PartA_QualSales_LY
		,SUM(PartB_QualSales_CY) AS PartB_QualSales_CY, SUM(PartB_QualSales_LY) AS PartB_QualSales_LY
		,SUM(PartC_QualSales_CY) AS PartC_QualSales_CY, SUM(PartC_QualSales_LY) AS PartC_QualSales_LY
	FROM @TEMP
	GROUP BY StatementCode, MarketLetterCode, ProgramCode

	/*
	PART A: 2% payment when they maintain 95% growth YOY & a 4% payment when they maintain 100% growth YOY on 2020 sales of eligible IMI Herbicide brands.
	Growth in 2020 relative to the 2019 IMI Sales
	>100%			4%
	95% to 99.9%	2%
	*/

	-- Determine growth on total sales even though we are storing the data at brand level sales
	UPDATE @TEMP_TOTALS
	SET [Growth] = [PartA_QualSales_CY] / [PartA_QualSales_LY]
	WHERE [PartA_QualSales_LY] > 0

	-- Update the reward %
	-- Rounding Rules: 0.5% included in qualification of reward tiers.

	UPDATE @TEMP_TOTALS SET [PartA_Reward_Percentage] = 0.04 WHERE ([Growth]=0 AND [PartA_QualSales_CY] > 0) OR [Growth]+@Roundup_Value >= 1 
	UPDATE @TEMP_TOTALS SET [PartA_Reward_Percentage] = 0.02 WHERE [PartA_Reward_Percentage] = 0 AND [Growth]+@Roundup_Value >= 0.95

	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN	
		DECLARE @PLAN_CUT_OFF_DATE AS DATE=CAST('2020-02-11' AS DATE)
		
		-- CHECK FOR ACTUAL REQUIREMENTS , IF NOT MET SET [PartA_Reward_Percentage] to 0
		INSERT INTO @REQUIREMENTS(Statementcode,VersionType,StatementLevel,Level5Code,Plan_Date, Plan_Amount, Order_Amount, Plan_Passed, Orders_Passed)
		SELECT ST1.StatementCode, ST1.VersionType , ST1.StatementLevel ,ST1.Level5Code, NULL AS Plan_Date, 0 AS Plan_Amount, 0 AS Order_Amount, 0 AS Plan_Passed, 0 AS Orders_Passed 
		FROM RP_Statements  ST1
			INNER JOIN @STATEMENTS ST2			
			ON ST2.Statementcode=ST1.StatementCode
		
		-- FINAL POG PLAN DATE		
		IF @REFRESHING_VERSION_TYPE='UNLOCKED'		-- IN CASE OF UNLOCKED STATEMENTS :  SCENARIO TRANSACTIONS SHOULD BE OBTAINED FROM RCT_SCENARIODETAILS
		BEGIN
			-- POG PLAN TRANSACTIONS TOTAL FOR INDEPENDENTS
			UPDATE T1
			SET Plan_Amount=T2.Plan_Amount
				,Plan_Passed=IIF(T2.Plan_Amount > 0, 1, 0)
			FROM @REQUIREMENTS T1
				INNER JOIN (
					SELECT tx.Statementcode, SUM(tx.Quantity * SDP) AS Plan_Amount
					FROM TVF_Get_IndependentPlanTransactions(@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS) tx
						INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
						INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
					WHERE GR.MarketLetterCode='ML2020_W_CER_PUL_SB' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
					GROUP BY tx.Statementcode
				) T2
				ON T2.StatementCode=T1.StatementCode
			WHERE T1.VersionType='Unlocked' AND T1.Level5Code='D000001'
	
			-- POG PLAN TRANSACTIONS TOTAL FOR COOPS
			UPDATE T1
			SET Plan_Amount=T2.Plan_Amount
				,Plan_Passed=IIF(T2.Plan_Amount > 0, 1, 0)
			FROM @REQUIREMENTS T1
				INNER JOIN (
					SELECT SCN.Statementcode							
						,SUM(TX.QTY * PRP.SDP) AS Plan_Amount
					FROM (
						SELECT StatementCode, ScenarioCode
						FROM TVF_Get_NonIndependent_Scenarios(@Season, @PLAN_CUT_OFF_DATE, @STATEMENTS)
						WHERE [ScenarioStatus] IN ('FINAL','Preferred')
					) SCN
						INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
						INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
						INNER JOIN RP_Config_Groups_Brands BR					ON BR.ChemicalGroup=PR.ChemicalGroup
						INNER JOIN RP_Config_Groups  GR							ON GR.GroupID=BR.GroupID								
						INNER JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'
					WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
						AND GR.MarketLetterCode='ML2020_W_CER_PUL_SB'  AND CAST(tx.UpdateDate AS DATE) <= @PLAN_CUT_OFF_DATE
					GROUP BY SCN.Statementcode 					
				) T2
				ON T2.StatementCode=T1.StatementCode
			WHERE T1.VersionType='Unlocked'  AND (T1.Level5Code='D0000117' OR T1.StatementLevel = 'LEVEL 5')
		END
		
		-- POG PLAN AMOUNT AS CUT OFF DATE FEB 11 2020
		IF @REFRESHING_VERSION_TYPE='USE CASE'		-- IN CASE OF USE CASE STATEMENTS :   SCENARIO TRANSACTIONS SHOULD BE OBTAINED FROM RP_DT_ScenarioTransactions
		BEGIN
			UPDATE T1
			SET Plan_Amount = T2.Plan_Amount
				,Plan_Passed=IIF(T2.Plan_Amount > 0, 1, 0)
			FROM @REQUIREMENTS T1
				INNER JOIN (
					SELECT tx.Statementcode ,SUM(TX.Price_SDP) AS Plan_Amount						
					FROM RP_DT_ScenarioTransactions tx
						INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
						INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
					WHERE GR.MarketLetterCode='ML2020_W_CER_PUL_SB' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	
						AND CAST(TX.UC_DateModified AS DATE) <= @PLAN_CUT_OFF_DATE
					GROUP BY tx.Statementcode
				) T2
				ON T2.StatementCode=T1.StatementCode 		
			WHERE T1.VersionType='USE CASE'
		END 		

		/*
		RP-2631: Not a requirement anymore
		-- ORDER AMOUNT	FOR INDEPENDENTS
		IF EXISTS(SELECT * FROM @REQUIREMENTS WHERE LEVEL5CODE='D000001' OR StatementLevel='LEVEL 5')
		BEGIN
			DECLARE @ORDER_CUT_OFF_DATE AS DATE=CAST('2020-04-29' AS DATE)

			UPDATE T1
			SET Order_Amount = T2.Orders_SDP 
			FROM @REQUIREMENTS T1
				INNER JOIN (				
					SELECT ST.StatementCode ,SUM(TX.Quantity * PRP.SDP)  AS Orders_SDP
					FROM @STATEMENTS ST				
						INNER JOIN [TVF_Get_OrdersData](@Season, @ORDER_CUT_OFF_DATE, @STATEMENTS)	TX
						ON TX.StatementCode=ST.StatementCode
							INNER JOIN ProductReference PR				ON PR.Productcode=TX.ProductCode
							INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=TX.ProductCode 
							INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=PR.ChemicalGroup
							INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
					WHERE PRP.Season=@SEASON AND PRP.Region='WEST' 
						AND GR.MarketLetterCode='ML2020_W_CER_PUL_SB' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
					GROUP BY ST.StatementCode
				) T2
				ON T2.Statementcode=T1.Statementcode
			WHERE T1.Level5Code='D000001'

			-- WEST LINE COMPANIES
			SET @ORDER_CUT_OFF_DATE = CAST('2020-04-14' AS DATE)

			UPDATE T1
			SET Order_Amount = T2.Orders_SDP 
			FROM @REQUIREMENTS T1
				INNER JOIN (				
					SELECT ST.StatementCode ,SUM(TX.Quantity * PRP.SDP)  AS Orders_SDP
					FROM @STATEMENTS ST				
						INNER JOIN [TVF_Get_OrdersData](@Season, @ORDER_CUT_OFF_DATE, @STATEMENTS)	TX
						ON TX.StatementCode=ST.StatementCode
							INNER JOIN ProductReference PR				ON PR.Productcode=TX.ProductCode
							INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=TX.ProductCode 
							INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=PR.ChemicalGroup
							INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
					WHERE PRP.Season=@SEASON AND PRP.Region='WEST' 
						AND GR.MarketLetterCode='ML2020_W_CER_PUL_SB' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
					GROUP BY ST.StatementCode
				) T2
				ON T2.Statementcode=T1.Statementcode
			WHERE T1.StatementLevel='LEVEL 5'
					   			 
			UPDATE @REQUIREMENTS
			SET Orders_Passed=1
			WHERE (Level5Code='D000001' OR StatementLevel='LEVEL 5')  AND  Plan_Amount > 0 AND (Order_Amount/Plan_Amount) + @Roundup_Value >= 0.60 -- 60% or more of planned amount

			UPDATE  T1
			SET PartA_Reward_Percentage=0
			FROM @TEMP_TOTALS  T1
				INNER JOIN 	@REQUIREMENTS T2	
				ON T2.Statementcode=T1.Statementcode
			WHERE (T2.Level5Code='D000001' OR T2.StatementLevel = 'LEVEL 5') AND (T2.Plan_Passed=0 OR Orders_Passed=0)
		END
		*/

		/*************  RP-2631  generic rule for all retailers *****/
		/*		
		UPDATE  T1
		SET PartA_Reward_Percentage=0
		FROM @TEMP_TOTALS  T1
			INNER JOIN 	@REQUIREMENTS T2	
			ON T2.Statementcode=T1.Statementcode
		WHERE (T2.Level5Code='D0000117' OR T2.StatementLevel='LEVEL 5') AND  (T2.Plan_Passed=0 OR T2.Plan_Amount <= 0)
		*/
		
		UPDATE  T1
		SET PartA_Reward_Percentage=0
		FROM @TEMP_TOTALS  T1
			INNER JOIN 	@REQUIREMENTS T2	
			ON T2.Statementcode=T1.Statementcode
		WHERE T2.Plan_Passed=0
		/************* END OF GENERIC RULE *****/
				
		-- THIS PROGRAM IS APPLICABLE ON ONE MARKET LETTER ONLY, AND REQUIREMENTS TABLE IS SOMETHING FOR US TO TROUBLE SHOOT
		-- HENCE WE ARE NOT MAINTAINING MARKETLETTERCODE IN REQUIREMENTS TABLE UNLIKE OTHER TABLE FOR THE SAME PROGRAM
		DELETE RP2020_Req_W_SoybeanPulsePlanning WHERE [Statementcode] IN (SELECT Statementcode FROM @STATEMENTS)

		INSERT INTO RP2020_Req_W_SoybeanPulsePlanning(Statementcode, Plan_Date, Plan_Amount, Order_Amount, Plan_Passed, Orders_Passed)
		SELECT StatementCode,Plan_Date,Plan_Amount,Order_Amount,Plan_Passed,Orders_Passed FROM @REQUIREMENTS 									

	END -- OF ACTUAL BLOCK

	-- Update the growth % and reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.[Growth]=T2.[Growth]
		,T1.[PartA_Reward_Percentage]=T2.[PartA_Reward_Percentage]
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2 
		ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode] = T2.[MarketLetterCode] AND T1.[ProgramCode] = T2.[ProgramCode]

	-- Update reward
	UPDATE @TEMP SET [PartA_Reward] = [PartA_Reward_Sales] * [PartA_Reward_Percentage]

	/*
	Part B: an additional 1% payment on their 2020 POGs of IMI Herbicides when they meet or exceed their Heat LQ/Heat Complete Target. 
	Note: If Retails qualifies for Part A they are eligible to receive Part B
	NEW: RRT-747 - If a retail has 0 POG$ in 2019 and a minimum of 1 POG$ in 2020 then they would receive the 1% bonus
	*/

	-- Determine if eligible for reward
	IF @STATEMENT_TYPE='ACTUAL'
		-- Note: Target = 2019 Eligible POG$ of Brands
		UPDATE T1
		SET T1.PartB_Reward_Percentage = 0.01
		FROM @TEMP_TOTALS T1
		WHERE T1.PartA_Reward_Percentage > 0 AND  T1.PartB_QualSales_CY > 0 
			AND	(
				T1.PartB_QualSales_LY <= 0 
					OR
				(T1.PartB_QualSales_CY/T1.PartB_QualSales_LY) + @Roundup_Value >= 1
			)
	ELSE
		UPDATE T1
		SET T1.PartB_Reward_Percentage = 0.01
		FROM @TEMP_TOTALS T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI 
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='PartB_Target'
		WHERE T1.PartA_Reward_Percentage > 0 AND T1.PartB_QualSales_CY > 0 AND T1.PartB_QualSales_CY >= (T1.PartB_QualSales_LY * (EI.FieldValue / 100))  --AND T1.PartB_QualSales >= EI.FieldValue -- OLD

	-- Update the reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.[PartB_Reward_Percentage] = T2.[PartB_Reward_Percentage]
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 
	ON T1.[StatementCode]=T2.[StatementCode] AND T1.[MarketLetterCode]=T2.[MarketLetterCode] AND T1.[ProgramCode]=T2.[ProgramCode]

	-- Update reward
	UPDATE @TEMP SET [PartB_Reward] = [PartB_Reward_Sales] * [PartB_Reward_Percentage] 

	/*
	Part C: an additional 1% payment on their 2020 POGs of IMI Herbicides when they meet or exceed their Priaxor/Dyax Bonus Target. Note: If Retails qualifies for Part A they are eligible to receive Part C
	NEW: RRT-747 - If a retail has 0 POG$ in 2019 and a minimum of 1 POG$ in 2020 then they would receive the 1% bonus
	*/

	-- Determine if eligible for reward
	IF @STATEMENT_TYPE='ACTUAL'
		-- Note: Target = 2019 Eligible POG$ of Brands
		UPDATE T1
		SET T1.PartC_Reward_Percentage = 0.01
		FROM @TEMP_TOTALS T1
		WHERE T1.PartA_Reward_Percentage > 0 AND T1.PartC_QualSales_CY > 0 
			AND (
				T1.PartC_QualSales_LY <= 0
					OR
					--T1.PartC_QualSales_CY >= T1.PartC_QualSales_LY
				(T1.PartC_QualSales_CY/T1.PartC_QualSales_LY) + @Roundup_Value >= 1
				
			)
	ELSE
		UPDATE T1
		SET T1.PartC_Reward_Percentage = 0.01
		FROM @TEMP_TOTALS T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI 
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='PartC_Target'
		WHERE T1.PartA_Reward_Percentage > 0 AND T1.PartC_QualSales_CY > 0 AND T1.PartC_QualSales_CY >= (T1.PartC_QualSales_LY * (EI.FieldValue / 100)) -- AND T1.PartC_QualSales >= EI.FieldValue -- OLD

	-- Update the reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.[PartC_Reward_Percentage] = T2.[PartC_Reward_Percentage]
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode] = T2.[MarketLetterCode] AND T1.[ProgramCode] = T2.[ProgramCode]

	-- Update reward
	UPDATE @TEMP SET [PartC_Reward] = [PartC_Reward_Sales] * [PartC_Reward_Percentage] 

	-- Update final reward
	UPDATE @TEMP 
	SET [Reward] = [PartA_Reward] + [PartB_Reward] + [PartC_Reward] 
	--WHERE [PartA_Reward] + [PartB_Reward] + [PartC_Reward] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [PartA_Reward] = [PartA_Reward] * ISNULL(EI.FieldValue/100,1),
			[PartB_Reward] = [PartB_Reward] * ISNULL(EI.FieldValue/100,1),
			[PartC_Reward] = [PartC_Reward] * ISNULL(EI.FieldValue/100,1),
			[Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
	
	DELETE FROM RP2020_DT_Rewards_W_SoybeanPulsePlanning WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_SoybeanPulsePlanning(Statementcode, MarketLetterCode, ProgramCode, GroupLabel, PartA_QualSales_CY, PartA_QualSales_LY, PartA_Reward_Sales, PartA_Reward_Percentage, PartA_Reward, PartB_QualSales_CY, PartB_QualSales_LY, PartB_Reward_Sales, PartB_Reward_Percentage, PartB_Reward, PartC_QualSales_CY, PartC_QualSales_LY, PartC_Reward_Sales, PartC_Reward_Percentage, PartC_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, GroupLabel, PartA_QualSales_CY, PartA_QualSales_LY, PartA_Reward_Sales, PartA_Reward_Percentage, PartA_Reward, PartB_QualSales_CY, PartB_QualSales_LY, PartB_Reward_Sales, PartB_Reward_Percentage, PartB_Reward, PartC_QualSales_CY, PartC_QualSales_LY, PartC_Reward_Sales, PartC_Reward_Percentage, PartC_Reward, Reward
	FROM @TEMP

END

