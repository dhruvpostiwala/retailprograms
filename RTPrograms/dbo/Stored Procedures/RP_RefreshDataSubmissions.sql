﻿
CREATE PROCEDURE [dbo].[RP_RefreshDataSubmissions] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

		 --FOR TESTING 
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 904
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'Actual'	

	--RetailerDataSubmissionDataAll
	--RetailerDataNotExpectedTrackerAll
	--[dbo].[RP_DT_RetailerDataNotExpectedTrackerAll]
	--[dbo].[RP_DT_RetailerDataSubmissionDataAll]
	
	DELETE T1
	FROM RP_DT_RetailerDataSubmissionDataAll T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'
	
	DELETE T1
	FROM RP_DT_RetailerDataNotExpectedTrackerAll T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'

	INSERT INTO RP_DT_RetailerDataSubmissionDataAll([StatementCode], [Season], [DocCode], [RetailerCode], [DSCode], [Status], [DateReceived], [MultipleRetailers], [DataType], [MonthOfSubmission],[RP_ID])
	SELECT ST.StatementCode,DataSUB.Season,DataSUB.DocCode,MAP.RetailerCode,DataSUB.DSCode,DataSUB.Status,DataSUB.DateReceived,DataSUB.MultipleRetailers,DataSUB.DataType,DataSUB.MonthOfSubmission
			,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY DataSUB.RetailerCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT Season ,DocCode, DealerCode AS RetailerCode,DSCOde,Status, DateReceived, MultipleRetailers, DataType, MonthOfSubmission
			FROM RetailerDataSubmissionDataAll Where SEASON = @SEASON
		) DataSUB 
		ON DataSUB.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.VersionType <> 'USE CASE'

	INSERT INTO RP_DT_RetailerDataNotExpectedTrackerAll([StatementCode], [RetailerCode], [Season], [DataType], [MonthOfSubmission], [Comments], [DateModified],[RP_ID])
	SELECT ST.StatementCode,MAP.RetailerCode,DataSUB.Season, DataSUB.DataType,DataSUB.MonthOfSubmission, DataSUB.Comments,DataSUB.DateModified
			,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY DataSUB.RetailerCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT Season, DSCode as RetailerCode,DataType, MonthOfSubmission, Comments, DateModified
			FROM RetailerDataNotExpectedTrackerAll Where SEASON = @SEASON
		) DataSUB 
		ON DataSUB.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.VersionType <> 'USE CASE'

	/*
		INSERT INTO RP_DT_RetailerDataSubmissionDataAll([StatementCode], [Season], [DocCode], [RetailerCode], [DSCode], [Status], [DateReceived], [MultipleRetailers], [DataType], [MonthOfSubmission],[RP_ID])
	SELECT ST.StatementCode,DataSUB.Season,DataSUB.DocCode,DataSUB.RetailerCode,DataSUB.DSCode,DataSUB.Status,DataSUB.DateReceived,DataSUB.MultipleRetailers,DataSUB.DataType,DataSUB.MonthOfSubmission
			,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY DataSUB.RetailerCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT Season ,DocCode, DealerCode AS RetailerCode,DSCOde,Status, DateReceived, MultipleRetailers, DataType, MonthOfSubmission
			FROM RetailerDataSubmissionDataAll Where SEASON = @SEASON
		) DataSUB 
		ON DataSUB.RetailerCode = MAP.ActiveRetailerCode 
	WHERE RS.VersionType <> 'USE CASE'

	INSERT INTO RP_DT_RetailerDataNotExpectedTrackerAll([StatementCode], [RetailerCode], [Season], [DataType], [MonthOfSubmission], [Comments], [DateModified],[RP_ID])
	SELECT ST.StatementCode,MAP.RetailerCode,DataSUB.Season, DataSUB.DataType,DataSUB.MonthOfSubmission, DataSUB.Comments,DataSUB.DateModified
			,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY DataSUB.RetailerCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT Season, DSCode as RetailerCode,DataType, MonthOfSubmission, Comments, DateModified
			FROM RetailerDataNotExpectedTrackerAll Where SEASON = @SEASON
		) DataSUB 
		ON DataSUB.RetailerCode = MAP.RetailerCode 
	WHERE RS.VersionType <> 'USE CASE'
	*/

END
