﻿


CREATE PROCEDURE [dbo].[RPST_Agent_East_EarlyPayment_LockStatements] @SEASON INT,  @USERNAME VARCHAR(150),  @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	-- UPDATE CODE TO ACCOMODATE LOCKING OF MULTIPLE STATEMENTS AT A TIME

	/* 	HANDLES LOCKING MULTIPL STATEMENTS AT A TIME */
	
	SET NOCOUNT ON;
	
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;
	DECLARE @STATEMENTS_TO_LOCK UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @PROCESS_OU_TASKS BIT = 0;
	DECLARE @PROCESS_HO_TASKS BIT = 0;
	DECLARE @SQL_COMMAND NVARCHAR(MAX);

	DECLARE @OU_STATEMENTS TABLE(
		StatementCode INT,
		LockedStatementCode INT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)
	
	DECLARE @HO_STATEMENTS TABLE(
		StatementCode INT,		
		LockedStatementCode INT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)

	DECLARE @VALIDATIONS TABLE(
		Statementcode INT,
		RetailerInfo VARCHAR(MAX)
	)
		
	DECLARE @RETAILER_CODES AS VARCHAR(MAX)

	BEGIN TRY
	
		-- OBTAIN A UNIQUE IDENTIFIER FOR THIS JOB
		DECLARE @JOB_ID uniqueidentifier=NEWID();

		INSERT INTO @STATEMENTS(STATEMENTCODE)
		SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')
	   	
		-- WE NEED TO GET ALL STATEMENTS CODES BELONGING TO A FAMILY
		INSERT INTO @STATEMENTS_TO_LOCK(StatementCode, StatementLevel, Retailercode, DataSetCode, DataSetCode_L5 ,Summary)
		SELECT StatementCode,StatementLevel,Retailercode,DataSetCode, DataSetCode_l5, Summary
		FROM TVF_Get_StatementCodesToRefresh(@STATEMENTS) 	

		INSERT INTO @OU_STATEMENTS(StatementCode)
		SELECT DISTINCT DataSetCode FROM @STATEMENTS_TO_LOCK WHERE DataSetCode > 0
	
		INSERT INTO @HO_STATEMENTS(StatementCode)
		SELECT DISTINCT DataSetCode_L5 FROM @STATEMENTS_TO_LOCK WHERE DataSetCode_L5 > 0
	   
	   
		/**************************** VALIDATIONS BEGIN ****************************/
	
	
	
		/********************* VALIDATION #3: MAKE SURE LEVEL 1 AND LEVEL 2 STATEMENTS THAT WILL BE PAID AT LEVEL 5 HAS AN ACTIVE LOCKED LEVEL 5 SUMAMRY STATEMENT *********************/	
		IF EXISTS(SELECT * FROM @HO_STATEMENTS)
		BEGIN
			UPDATE HO
			SET LockedStatementCode=LK.StatementCode
			FROM @HO_STATEMENTS HO
				INNER JOIN RPWeb_Statements LK	ON LK.SRC_StatementCode=HO.StatementCode
			WHERE LK.Season=@Season AND LK.[Status]='Active' AND LK.VersionType='Archive' AND LK.StatementLevel='LEVEL 5'

			IF EXISTS(SELECT * FROM @HO_STATEMENTS WHERE LockedStatementCode=0)
			BEGIN		
				SELECT @RETAILER_CODES=STRING_AGG(ISNULL(RP.RetailerName,'') + ' (' + S.RetailerCode + ')'   , ',<br>' )
				FROM @HO_STATEMENTS HO
					INNER JOIN RPWeb_Statements S	ON S.Statementcode=HO.Statementcode				
					LEFT JOIN RetailerProfile RP	ON RP.Retailercode=S.Retailercode
				WHERE HO.LockedStatementCode=0
			
				SELECT 'failed' as [Status], 'There is NO active LEVEL 5 Locked Statement for one or more of Level 1 and Level 2 Statements. Please lock following Level 5 statement(s)<br>' + @RETAILER_CODES AS [error_message]		
				RETURN
			END
		END
	   
		/**************************** VALIDATIONS END ****************************/
		
		IF EXISTS(SELECT * FROM @HO_STATEMENTS)
			SET @PROCESS_HO_TASKS=1

		IF EXISTS(SELECT * FROM @OU_STATEMENTS)
			SET @PROCESS_OU_TASKS=1
			   		 	
	
		/* LETS START LOCKING PROCESS 
			- GENERATE NEW LOCKED STATEMENT CODE (ON NEWLY INSERTED RECORD, SRC_STATEMENTCODE COLUMN VALUE SHOULD BE SET TO UNLOCKED STATEMENT CODE)
			- COPY UNLOCKED DATA FOR LOCKED STATEMENTS
		*/
		DELETE FROM @STATEMENTS		   		 
		INSERT INTO @STATEMENTS(StatementCode)
		SELECT UL.[StatementCode]
		FROM @STATEMENTS_TO_LOCK UL		
			INNER JOIN RPWeb_StatementInformation sti		ON sti.[StatementCode]=UL.[StatementCode]		
		WHERE  sti.[FieldName]='Status' AND sti.[FieldValue]='Ready for QA'


		/* INSERT NEW LOCKED STATEMENT INTO WEB STATEMENTS TABLE */	
		/* WEB STATEMENTS TABLE STATEMENTCODE COLUMN IS DEFINED AS IDENTITY COLUMN, SET TO INSERT NEGATIVE STATEMENT CODES */
		INSERT INTO RPWeb_Statements(Locked_Date,Lock_Job_ID, VersionType, StatementLevel, Status, Season, Region, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, SRC_StatementCode,ChequeRunName) 	
		SELECT GetDate(), @JOB_ID, 'Locked', StatementLevel, Status, Season, Region, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, StatementCode
			,'JULY EARLY PAYMENT'  AS ChequeRunName 
		FROM RPWeb_Statements ST
		WHERE StatementCode IN (SELECT [StatementCode] FROM @STATEMENTS)
	   

		/* 	TARGET: UNLOCKED STATEMENT 		ACTION:  UPDATE STATUS 	*/	
		UPDATE STI
		SET [FieldValue]='Ready For Payment'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA process - Lock','Field','Status',deleted.FieldValue,inserted.FieldValue
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI
			INNER JOIN @Statements TGT		ON TGT.StatementCode=STI.StatementCode		
		WHERE STI.[FieldName]='Status' AND [FieldValue]='Ready for QA'

		EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY
	   
		/*
			REGION: EAST
			ON NEWLY CREATED LOCKED STATEMENTS: WE NEED TO UPDATE DATASETCODE, AS COPIED OVER DATASETCODE IS POINTING TO UNLOCKED LEVEL 2 STATEMENTCODE.
			DATASETCODE SHOULD POINT TO LOCKED STATEMENTCODE OF LEVEL 2 STATEMENT WITHIN FAMILY
		*/
		IF @PROCESS_OU_TASKS=1
		UPDATE T1
		SET DataSetCode=T2.StatementCode
		FROM RPWeb_Statements  T1
			INNER JOIN RPWeb_Statements  T2
			ON T2.Lock_Job_ID=T1.Lock_Job_ID AND T2.SRC_StatementCode=T1.DataSetCode
		WHERE T1.Lock_Job_ID=@JOB_ID AND T1.Region='EAST' AND T1.VersionType='Locked' AND T1.[Status]='Active'


		IF @PROCESS_HO_TASKS=1
		BEGIN
			/* UPDATE DATASETCODE_L5 to match with Locked Version STatementcode on newly locked statments */
			UPDATE T1
			SET DataSetCode_L5=T2.LockedStatementCode
			FROM RPWeb_Statements  T1
				INNER JOIN @HO_STATEMENTS  T2
				ON T2.StatementCode=T1.DataSetCode_L5
			WHERE T1.Lock_Job_ID=@JOB_ID AND T1.VersionType='Locked' AND T1.StatementLevel IN ('LEVEL 1','LEVEL 2') 
		END	   		 	  

	 			
		/*  TARGET: UNLOCKED STATEMENTS
			ACTION:  UPDATE PAYMENTS 
			CURRENT PAYMENT=NEXT PAYMENT, NEXT_PAYMENT=0 
		*/
		UPDATE RPWeb_Rewards_Summary
		SET [CurrentPayment]=[NextPayment],[NextPayment]=0
		WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)		    


		INSERT INTO RPWeb_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)
		SELECT ST.StatementCode,RS.MarketLetterCode,RS.RewardCode,RS.RewardValue,RS.RewardEligibility,RS.RewardMessage,RS.TotalReward,RS.PaidToDate,RS.CurrentPayment,RS.NextPayment,RS.PaymentAmount
		FROM RPWeb_Rewards_Summary RS
			INNER JOIN RPWeb_Statements ST
			ON ST.[SRC_StatementCode]=RS.[StatementCode]
		WHERE ST.[Status]='Active' AND ST.[Lock_Job_ID]=@JOB_ID AND ST.[VersionType]='Locked' 
			AND RS.RewardCode='OTP_JULY_EARLY_E'
	   
		/*				   
			/* COPY DATA FROM UNLOCKED STATEMENTS TO ASSOCIATED LOCKED STATEMENTS */
			DECLARE @COPY_CMD NVARCHAR(MAX)
			DECLARE @COPY_PARAMS NVARCHAR(50)='@JOB_ID uniqueidentifier'
			DECLARE COPY_LOCKED_DATA CURSOR FOR 
				SELECT 'INSERT INTO '+so.[name]+'([StatementCode],'+STUFF((
					SELECT ',['+[Name]+']'
					FROM syscolumns sc
					/* LETS NOT INCLUDE THESE COLUMNS - WE ARE UPDATING STATEMENT CODE TO BE THE LOCKED STATEMENT CODE, THE REST ARE UNIQUE RECORD IDS */
					WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id] 
					ORDER BY sc.[id]
					FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+')
				SELECT st.[StatementCode],'+STUFF((
					SELECT ',custom_table.['+[Name]+']'
					FROM syscolumns sc
					WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id]
					ORDER BY sc.[id]
					FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+'
				FROM '+so.[name]+' custom_table
					INNER JOIN RPWeb_Statements ST
					ON ST.[SRC_StatementCode]=custom_table.[StatementCode]
				WHERE ST.[Status]=''Active'' AND ST.[Lock_Job_ID]=@JOB_ID AND ST.[VersionType]=''Locked''' AS [CODE]
				FROM sysobjects so
				WHERE so.[xtype]='U' AND so.[name] IN (SELECT TableName	FROM RP_Config_WebTableNames WHERE Season=@Season AND LockStatementAction='Yes')

			OPEN COPY_LOCKED_DATA
			FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
			WHILE(@@FETCH_STATUS=0)
				BEGIN	
					EXEC SP_EXECUTESQL @COPY_CMD,@COPY_PARAMS,@JOB_ID
					FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
				END
			CLOSE COPY_LOCKED_DATA
			DEALLOCATE COPY_LOCKED_DATA	  
		*/

		-- LETS UPDATE DATASETCODES		
		IF @PROCESS_OU_TASKS=1 OR @PROCESS_HO_TASKS=1
		BEGIN 		
			DECLARE UPDATE_DATASETCODES CURSOR FOR 
				SELECT '
					UPDATE T1
					SET [DataSetCode]=T2.[DataSetCode] ,[DataSetCode_L5]=T2.[DataSetCode_L5]
					FROM '+so.[name]+' T1
						INNER JOIN (
							SELECT [StatementCode], [DataSetCode], [DataSetCode_L5]
							FROM RPWEB_Statements 
							WHERE [Lock_Job_ID]=@JOB_ID AND [Status]=''Active''  AND [Versiontype]=''Locked'' AND [StatementLevel]=''LEVEL 1''  
								AND ([DataSetCode] < 0 OR [DataSetCode_L5] < 0)				
						) T2
						ON T2.Statementcode=T1.Statementcode ' AS [Code]
				FROM sysobjects so
				WHERE so.[xtype]='U' AND so.[name] IN (SELECT TableName	FROM RP_Config_WebTableNames WHERE Season=@Season AND UpdateDataSetCodes='Yes')
			   
			OPEN UPDATE_DATASETCODES
			FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
			WHILE(@@FETCH_STATUS=0)
				BEGIN	
					EXEC SP_EXECUTESQL @SQL_COMMAND, N'@SEASON INT, @JOB_ID uniqueidentifier', @SEASON, @JOB_ID
					FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
				END
			CLOSE UPDATE_DATASETCODES
			DEALLOCATE UPDATE_DATASETCODES
		END 

			/*	   
				IF @PROCESS_HO_TASKS=1
				BEGIN			   
					-- FOR LEVEL 5 ,  DELETE EXISTING REWARD SUMMARY DATA OF LOCKED STATEMENT AND REPOPULATE IT FROM LOCKED STATEMENTS OF LEVEL 1
					DELETE REW
					FROM RPWeb_Rewards_Summary REW
						INNER JOIN @HO_STATEMENTS HO	ON HO.LockedStatementcode=REW.Statementcode			
		
					INSERT INTO RPWeb_Rewards_Summary(Statementcode,MarketLetterCode,RewardCode,RewardEligibility,RewardMessage,RewardValue,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)
					SELECT HO.LockedStatementCode, REW.MarketLetterCode, REW.RewardCode, 'Eligible', ''
						,SUM(REW.RewardValue) AS [RewardValue]
						,SUM(REW.TotalReward) AS [TotalReward]
						,SUM(REW.PaidToDate) AS [PaidToDate]
						,SUM(REW.CurrentPayment) AS [CurrentPayment]
						,SUM(REW.NextPayment) AS [NextPayment]
						,SUM(ISNULL(REW.PaymentAmount,0)) AS [PaymentAmount]
					FROM @HO_STATEMENTS HO
						INNER JOIN RPWeb_Statements ST			ON ST.DataSetCode_L5=HO.LockedStatementCode
						INNER JOIN RPWeb_Rewards_Summary REW	ON REW.StatementCode=ST.StatementCode
					WHERE ST.VersionType='LOCKED' AND ST.StatementLevel='LEVEL 1' 
					GROUP BY HO.LockedStatementCode, REW.MarketLetterCode, REW.RewardCode
				END
		*/
		/****************** POST LOCK ACTIVITY ***********************/
		/*
			ONE TIME PAYMENTS
			TARGET: UNLOCKED STATEMENTS 
			ACTION: UPDATE NEXT PAYMENT OF OTP REWARDS		
		*/

		UPDATE  T1
		SET [NextPayment]=[CurrentPayment] * -1
		FROM RPWeb_Rewards_Summary T1		
			INNER JOIN @STATEMENTS ST 						ON ST.StatementCode=T1.StatementCode		
			INNER JOIN RP_Config_Rewards_Summary CFG		ON CFG.RewardCode=T1.RewardCode  
		WHERE CFG.Season=@SEASON AND CFG.RewardType='OTP' AND T1.[PaidToDate]=0
	

			/* SUMMARY STATEMENT REFRESH */
		IF @PROCESS_HO_TASKS=1
			EXEC RP_Refresh_HO_RewardsSummary @SEASON, 'Actual', @STATEMENTS

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:
	SELECT 'success' as [Status]
		,ISNULL(@JOB_ID,'') AS Lock_Job_ID  
		,'' AS Error_Message
		,'' as confirmation_message
		,'' as warning_message

END




