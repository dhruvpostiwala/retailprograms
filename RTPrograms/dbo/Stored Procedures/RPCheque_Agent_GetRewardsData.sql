﻿

CREATE PROCEDURE [dbo].[RPCheque_Agent_GetRewardsData] @SEASON INT,  @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
		-- @USERNAME VARCHAR(150), 
		-- @FIELDVALUE VARCHAR(60), 
		SET NOCOUNT ON;	

		DECLARE @STATEMENTS AS TABLE (Statementcode INT);

		INSERT INTO @STATEMENTS(STATEMENTCODE)
		SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')
		

		DROP TABLE IF EXISTS #Rewards_Summary

		SELECT T.StatementCode
			,SUM(IIF(T.FundsRequestGroup = 'INV', T.TotalReward, 0)) AS InvTotalReward
			,SUM(IIF(T.FundsRequestGroup = 'INV', T.PaidToDate, 0)) AS InvPaidToDate
			,SUM(IIF(T.FundsRequestGroup = 'INV', T.CurrentPayment, 0)) AS InvCurrentPayment
			,SUM(IIF(T.FundsRequestGroup = 'INV', T.NextPayment, 0)) AS InvNextPayment

			,SUM(IIF(T.FundsRequestGroup = 'CCP', T.TotalReward, 0)) AS CcpTotalReward
			,SUM(IIF(T.FundsRequestGroup = 'CCP', T.PaidToDate, 0)) AS CcpPaidToDate
			,SUM(IIF(T.FundsRequestGroup = 'CCP', T.CurrentPayment, 0)) AS CcpCurrentPayment
			,SUM(IIF(T.FundsRequestGroup = 'CCP', T.NextPayment, 0)) AS CcpNextPayment

			,SUM(IIF(T.FundsRequestGroup = 'OTHER', T.TotalReward,0)) AS OthTotalReward
			,SUM(IIF(T.FundsRequestGroup = 'OTHER', T.PaidToDate, 0)) AS OthPaidToDate
			,SUM(IIF(T.FundsRequestGroup = 'OTHER', T.CurrentPayment, 0)) AS OthCurrentPayment
			,SUM(IIF(T.FundsRequestGroup = 'OTHER', T.NextPayment, 0)) AS OthNextPayment

			,SUM(T.TotalReward) AS TotalReward
			,SUM(T.PaidToDate) AS PaidToDate
			,SUM(T.CurrentPayment) AS CurrentPayment
			,SUM(T.NextPayment) AS NextPayment
		INTO #Rewards_Summary
		FROM (
			SELECT t1.*, ISNULL(t2.FundsRequestGroup ,'OTHER') AS FundsRequestGroup
			FROM RPWeb_Rewards_Summary t1
				INNER JOIN @STATEMENTS ST2
				ON ST2.StatementCode=T1.Statementcode
				LEFT JOIN [dbo].[RP_Config_MarketLetters] t2
				ON t1.MarketLetterCode = t2.MarketLetterCode
		) T
		GROUP BY T.StatementCode

		SELECT LK_ST.SRC_StatementCode [statementcode],LK_ST.StatementCode [locked_statementcode],LK_ST.DataSetCode ,LK_ST.DataSetCode_L5
			,IIF(DATENAME(MONTH,LK_ST.LOCKED_DATE)='July','July Early',DATENAME(MONTH,LK_ST.LOCKED_DATE)) AS PaymentMonth
			,LK_ST.retailercode,RP.RetailerName	
			,RP.MailingAddress1	 AS Address1
			,RP.MailingAddress2	 AS Address2
			,RP.MailingCity	As City
			,RP.MailingProvince	AS Province
			,RP.MailingPostalCode AS PostalCode
			,RP.SalesRep ,RP.Territory
			,ISNULL(HORT.HortRep,'') AS HortRep, ISNULL(HORT.HortTerritory,'') AS HortTerritory
			,LK_ST.Region,RP.RetailerLanguage
			,LK_ST.level5code, ISNULL(RP5.RetailerName,'') AS Level5Name
			,LK_ST.level2code, ISNULL(RP2.RetailerName,'') AS Level2Name
			,LK_ST.StatementLevel
			,UL_STI.FieldValue as [Status]	
			,RS.InvTotalReward,RS.InvPaidToDate,RS.InvCurrentPayment,RS.InvNextPayment
			,RS.OthTotalReward,RS.OthPaidToDate,RS.OthCurrentPayment,RS.OthNextPayment
			,RS.CcpTotalReward,RS.CcpPaidToDate,RS.CcpCurrentPayment,RS.CcpNextPayment
			,RS.totalreward,RS.paidtodate,RS.currentpayment,RS.nextpayment
			,RP.PayableTo			
			,CASE 
				WHEN ISNULL(HO.Summary,'')='' THEN ''   
				WHEN HO.Summary='Yes' THEN LK_ST.Region + ' HO'
				ELSE LK_ST.Region + ' Distributor'
			END [Model]	
		FROM #Rewards_Summary RS
			INNER JOIN RPWeb_Statements LK_ST				ON LK_ST.StatementCode=RS.StatementCode			
	--		INNER JOIN RPWeb_StatementInformation LK_STI 	ON LK_STI.StatementCode=LK_ST.StatementCode
			INNER JOIN RPWeb_StatementInformation UL_STI	ON UL_STI.StatementCode=LK_ST.SRC_StatementCode AND UL_STI.FieldName='Status'						
			INNER JOIN RetailerProfile RP					ON RP.RetailerCode=LK_ST.RetailerCode
			LEFT JOIN RP_Config_HOPaymentLevel HO			ON HO.Season=@SEASON AND HO.Level5Code=LK_ST.RetailerCode
			LEFT JOIN RetailerProfile RP5 					ON RP5.RetailerCode=RP.Level5Code			
			LEFT JOIN RetailerProfile RP2 					ON RP.RetailerType IN ('Level 1','Level 2') AND RP2.RetailerCode=RP.Level2Code			
			LEFT JOIN (
				SELECT Retailercode, HortRep, HortTerritory 
				FROM (
					SELECT RetailerCode, IsPrimary, EmployeeName AS HortRep, ClassificationName as HortTerritory
						,RANK() OVER (PARTITION BY RetailerCode ORDER BY ISNULL(IsPrimary,'No') DESC) AS [Priority]
					FROM RetailerAccess
					WHERE EmployeeRole = 'Hort' 
				) H
				WHERE [Priority]=1
			) HORT
			ON HORT.RetailerCode=RP.RetailerCode
		WHERE LK_ST.Season=@SEASON 
		--AND LK_STI.FieldName='CreateChequeStatus' AND LK_STI.FieldValue=@FieldValue

END
