﻿


CREATE   PROCEDURE [dbo].[RP_RefreshTransactions] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON_STRING VARCHAR(4)=CAST(@SEASON AS VARCHAR(4))

	DECLARE @SOLD_TO_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-02' AS DATE)		
	-- DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-01' AS DATE)	
	DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)
		
	/*
		NOTE: FOR FORECAST AND PROJECTION STATEMENT TYPES:
		- FOR CURRENT YEAR TRANSACTIONS ,WE ARE GOING TO USE POG PLAN TRANSACTIONS INSTEAD OF ACTUAL TRANSACTIONS	
	*/
	DECLARE @BASF_SEASONS TABLE (SEASON INT NOT NULL);

	DROP TABLE IF EXISTS #TEMP_ProductReference
	SELECT  *		
	INTO #TEMP_ProductReference
	FROM TVF_Get_ProductsInfo(@SEASON)


	/************* POG SALES TRANSACTIONS ****************/

	-- GET LAST TWO YEARS SALES FOR ALL STATEMENTTYPES (FORECAST,PROJECTION,ACTUAL) 
	INSERT INTO @BASF_SEASONS(SEASON)
	SELECT @SEASON-1 AS Season
		UNION
	SELECT @SEASON-2 AS Season

	-- GET CURRENT YEAR SALES FOR 'ACTUAL' STATEMENTTYPE
	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN		
		INSERT INTO @BASF_SEASONS(SEASON)
		VALUES(@SEASON)

		IF @SEASON=2021
		INSERT INTO @BASF_SEASONS(SEASON)
		VALUES(@SEASON-3)
	END


	/**** LETS COLLECT RETAILER CODES **/
	DROP TABLE IF EXISTS #TARGET_RETAILERS
	   
	SELECT RS.StatementCode, RS.StatementLevel, RS.Region, RS.Level2Code, RS.Level5Code, RS.DataSetCode, RS.DataSetCode_L5, MAP.RetailerCode AS RetailerCode, CAST(1 AS BIT) AS [Include_CY_Trans]
	INTO #TARGET_RETAILERS
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS 			ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
	--WHERE LEFT(MAP.ActiveRetailerCode,3) <> 'DLR'
	
	/*
		IN CASE OF SOLD RETAILER i.e., RETAILERCODE STARTING WITH "DLR"
		WE SHOULD FETCH LAST TWO YEARS TRANSACTION OF "SOLDRETAILERCODE" ON TO STATEMENT
		SET [Include_CY_Trans] TO 0 --> DO NOT INCLUDE CURRENT YEAR TRANSACTIONS
	*/
	IF @STATEMENT_TYPE='ACTUAL'
		INSERT INTO #TARGET_RETAILERS(StatementCode, StatementLevel, Region, Level2Code, Level5Code, DataSetCode, DataSetCode_L5, RetailerCode, Include_CY_Trans)
		SELECT SRC.StatementCode, SRC.StatementLevel, SRC.Region, SRC.Level2Code, SRC.Level5Code, SRC.DataSetCode, SRC.DataSetCode_L5, RP.SoldRetailerCode, 0 AS [Include_CY_Trans]		
		FROM #TARGET_RETAILERS SRC
			INNER JOIN RetailerProfile RP			ON RP.RetailerCode=SRC.RetailerCode		  	  	 	
		
	/* LETS COLLECT TRANSACTIONS FOR TARAGET RETAILERS */
	DROP TABLE IF EXISTS #GrowerBASF_Transactions
	SELECT GTD.TransCode,GTD.RetailerCode								
		,GTD.FarmCode, GTD.ProductCode, GTD.BASFSeason, GTD.Season, GTD.InvoiceNumber, GTD.InvoiceDate, GTD.Quantity
		,CFStatus
		,DSCode
		,HoldingID		
		,ID
	INTO #GrowerBASF_Transactions
	FROM GrowerTransactionsBASFDetails GTD	
		INNER JOIN @BASF_SEASONS S			ON S.Season=GTD.BASFSeason
		INNER JOIN (
			SELECT RetailerCode 
			FROM #TARGET_RETAILERS 
			GROUP BY RetailerCode
		) RET	
		ON RET.RetailerCode=GTD.RetailerCode	
	WHERE GTD.Quantity <> 0
	

	IF  @SEASON=2021 AND @STATEMENT_TYPE='ACTUAL' 
	DELETE TX
	FROM #GrowerBASF_Transactions TX
		LEFT JOIN  (
			SELECT ProductCode 
			FROM ProductReference
			WHERE ChemicalGroup = 'NODULATOR DUO SCG' 
		) PR
		ON PR.ProductCode=TX.ProductCode
	WHERE TX.BASFSeason=@SEASON-3 AND PR.ProductCode IS NULL


	/***** LETS HANDLE SOLD TO RETATILER SALES *****/
	-- 2020 AND PRIOR: DocCode to DSC Mapping 
	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		IF @SEASON <= 2020
			UPDATE GTD
			SET RetailerCode=HL.DLRCode
			FROM #GrowerBASF_Transactions GTD
				INNER JOIN (				
					SELECT AH.DLRCode, AH.DocCode
					FROM Agdeliverholding AH
						INNER JOIN (
							SELECT @SEASON AS Season
						) S
						ON S.Season=AH.[Year]
						INNER JOIN RetailerProfile RP
						ON RP.RetailerCode=AH.DLRCode AND RP.SoldDate BETWEEN @SOLD_TO_START_DATE AND @SOLD_TO_END_DATE
					WHERE  AH.[TransactionType]='POG' AND ISNULL(AH.DLRCode,'') <> '' AND AH.DLRCode <> AH.RetailerCode						
				) HL
				ON HL.DocCode=GTD.DSCode


		ELSE -- STARTING 2021  HOLDING ID IS THE COMMON COLUMN
			UPDATE GTD
			SET RetailerCode=HL.DLRCode
			FROM #GrowerBASF_Transactions GTD
				INNER JOIN (				
					SELECT AH.DLRCode, AH.HoldingID
					FROM Agdeliverholding AH
						INNER JOIN (
							SELECT @SEASON AS Season
						) S
						ON S.Season=AH.[Year]
						INNER JOIN RetailerProfile RP
						ON RP.RetailerCode=AH.DLRCode AND RP.SoldDate BETWEEN @SOLD_TO_START_DATE AND @SOLD_TO_END_DATE
					WHERE  AH.[TransactionType]='POG' AND ISNULL(AH.DLRCode,'') <> '' AND AH.DLRCode <> AH.RetailerCode						
				) HL
				ON HL.HoldingID=GTD.HoldingID
	END 

	/* MAP TRANSACTIONS TO STATEMENTCODE */
	DROP TABLE IF EXISTS #Temp_Transactions
	   
	SELECT RS.StatementCode, RS.Level5Code, RS.DataSetCode, RS.DataSetCode_L5, tx.RetailerCode, tx.FarmCode, tx.TransCode, tx.ProductCode, tx.Quantity
		,tx.Quantity * IIF(RS.Region='EAST', PR.ConversionE,PR.ConversionW) AS Acres
		,tx.BASFSeason, tx.Season, tx.InvoiceNumber, tx.InvoiceDate, PR.ChemicalGroup
		,tx.Quantity * ISNULL(PR.SRP,0) AS Price_SRP
		,tx.Quantity * ISNULL(PR.SDP,0) AS Price_SDP		
		,tx.Quantity * ISNULL(PR.SWP,0) AS Price_SWP		
		,IIF(tx.BASFSeason <= 2018,1,COALESCE(Grower.InRadius,BR_EX.InRadius,TX_EX21.InRadius,TX_EX20.InRadius,0)) AS InRadius		
		,tx.CFStatus
		,tx.HoldingID
		,ROW_NUMBER() OVER(PARTITION BY RS.[StatementCode] ORDER BY tx.[TransCode]) AS [StatementTransNumber]
	INTO #Temp_Transactions
	FROM #TARGET_RETAILERS RS			
		INNER JOIN #GrowerBASF_Transactions TX	ON TX.RetailerCode=RS.RetailerCode 				
		INNER JOIN #TEMP_ProductReference PR	ON PR.ProductCode=TX.ProductCode  -- AND RS.Region=IIF(PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'),'West',PR.Region)  
		LEFT JOIN (			
			SELECT FL.RetailerCode, FL.FarmCode, 1 AS InRadius, REL.Season AS BASFSeason
			FROM PER_RetailFarmList FL
				INNER JOIN PER_Relationships REL
				ON REL.PerID=FL.PerID
			WHERE  REL.IsActive=1 AND REL.Season IN (@SEASON,@SEASON-1,@SEASON-2)			 
		) Grower
		ON Grower.FarmCode=TX.FarmCode AND Grower.RetailerCode=TX.RetailerCode AND Grower.BASFSeason=tx.BASFSeason	
		LEFT JOIN (
			SELECT FL.RetailerCode, FL.FarmCode, BR.ChemicalGroup, 1 AS InRadius, BR.Season AS BASFSeason
			FROM PER_RetailFarmList FL
				INNER JOIN PER_RelationshipBrand BR
				ON BR.PerID=FL.PerID
			WHERE  BR.IsActive=1 AND BR.Season IN (@SEASON,@SEASON-1,@SEASON-2)  
		) BR_EX
		ON BR_EX.FarmCode=TX.FarmCode AND BR_EX.RetailerCode=TX.Retailercode AND BR_EX.BASFSeason=TX.BASFSeason AND BR_EX.ChemicalGroup=PR.ChemicalGroup
		LEFT JOIN (
			-- SEASON 2020 and prior should be joined on Transcode
			SELECT Season, TransCode,  1 AS InRadius
			FROM [dbo].[PER_RelationshipTransaction]	
			WHERE IsActive=1 AND Season <= 2020 AND ISNULL(TransCode,'') <> ''
				AND Season IN (2020,2019,2018) 
		) TX_EX20
		ON TX_EX20.Season=TX.BASFSeason AND TX_EX20.TransCode=TX.TransCode
		LEFT JOIN (
			-- SEASON 2021 and later should be joined on TransID
			SELECT Season, TransID,  1 AS InRadius
			FROM [dbo].[PER_RelationshipTransaction]	
			WHERE IsActive=1 AND Season >= 2021 AND ISNULL(TransID,'') <> ''
				AND Season IN (@SEASON,@SEASON-1,@SEASON-2) 
		) TX_EX21
		ON TX_EX21.Season=TX.BASFSeason AND TX_EX21.TransID=TX.ID
	WHERE (TX.BASFSeason < @SEASON OR RS.[Include_CY_Trans]=1)
			AND
		(ISNULL(PR.Region,'')='' OR RS.Region=IIF(PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'),'West',PR.Region)) 
	

	/* DELETE AND RELOAD TRANSACTIONS */							
	DELETE T1
	FROM RP_DT_Transactions T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE  T3.VersionType <> 'USE CASE'

	INSERT INTO RP_DT_Transactions(StatementCode, DataSetCode, DataSetCode_L5, RetailerCode, FarmCode, TransCode, ProductCode, Quantity, Acres, BASFSeason, Season, InvoiceNumber, InvoiceDate, ChemicalGroup, Price_SRP, Price_SDP,Price_SWP, InRadius, CFStatus, StatementTransNumber)
	SELECT StatementCode, DataSetCode, DataSetCode_L5, RetailerCode, FarmCode, TransCode, ProductCode, Quantity ,Acres ,BASFSeason, Season, InvoiceNumber, InvoiceDate, ChemicalGroup ,Price_SRP ,Price_SDP ,Price_SWP ,InRadius ,CFStatus, [StatementTransNumber]
	FROM #Temp_Transactions

	/************ LETS MANAGE LIBERTY 150 ADJUSTMENTS (AGRESOURCE) RP-2647 ************/
	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN 
		DROP TABLE IF EXISTS #NEXT_TRANSNUMBER
			
		SELECT StatementCode,MAX(StatementTransNumber) AS StatementTransNumber
		INTO #NEXT_TRANSNUMBER
		FROM #Temp_Transactions
		GROUP BY StatementCode

		DELETE #Temp_Transactions WHERE Level5Code <> 'D000001' OR BASFSeason < 2020

		DROP TABLE IF EXISTS #Liberty_Adjustments

		SELECT tx.StatementCode
			,tx.BASFSeason
			,'PRD-CIRD-B24KNS' AS ProductCode
			,[Matching Acres] = IIF([INVIGOR L258HPC] < [LIBERTY 150],[INVIGOR L258HPC],[LIBERTY 150])	
			,CAST(ROW_NUMBER() OVER(PARTITION BY tx.StatementCode ORDER BY tx.BASFSeason)  AS INT) AS Row#
		INTO #Liberty_Adjustments
		FROM (
			SELECT	t.StatementCode ,t.BASFSeason
				,sum(iif(pr.product = 'INVIGOR L258HPC',Acres,0)) [INVIGOR L258HPC]
				,sum(iif(pr.product = 'LIBERTY 150',Acres,0)) [LIBERTY 150]
			FROM #Temp_Transactions t
				INNER JOIN ProductReference pr 	
				ON pr.productcode = t.productcode	
			WHERE  t.Level5Code='D000001' AND T.BASFSeason >= 2020 AND  T.BASFSeason IN (@Season,@Season-1,@Season-2)  
				AND T.inradius = 1 AND pr.product in ('INVIGOR L258HPC','LIBERTY 150') 
			GROUP BY t.StatementCode ,t.BASFSeason
			Having	sum(iif(pr.product = 'INVIGOR L258HPC',Acres,0)) > 0 and  sum(iif(pr.product = 'LIBERTY 150',Acres,0)) > 0
		) tx
									
		INSERT INTO RP_DT_Transactions(StatementCode, DataSetCode, DataSetCode_L5, RetailerCode, FarmCode, TransCode, ProductCode, Quantity, Acres, BASFSeason, Season, InvoiceNumber, InvoiceDate, ChemicalGroup, Price_SRP, Price_SDP,Price_SWP, InRadius, CFStatus, StatementTransNumber)
		SELECT ST.StatementCode	,ST.DataSetCode	,ST.DataSetCode_L5	,ST.RetailerCode 
			,'ADJUSTMENT' FarmCode
			,'ADJUSTMENT' AS TransCode
			,ADJ.ProductCode			
			,-(ADJ.[Matching Acres]/PR.ConversionW) AS Quantity
			,-ADJ.[Matching Acres] AS Acres
			,ADJ.BASFSeason AS BASFSeason
			,ADJ.BASFSeason AS Season
			,'ADJUSTMENT' AS InvoiceNumber
			,CAST(@SEASON_STRING + '-09-30' AS DATE) AS InvoiceDate
			,pr.ChemicalGroup 
			,-(([Matching Acres]/PR.ConversionW)*PR.SRP) AS Price_SRP
			,-(([Matching Acres]/PR.ConversionW)*PR.SDP) AS Price_SDP
			,-(([Matching Acres]/PR.ConversionW)*PR.SWP) AS Price_SWP
			,1 AS InRadius 
			,'-' AS CFStatus 
			,ISNULL(NT.StatementTransNumber,0) + ADJ.Row# AS StatementTransNumber
		FROM #Liberty_Adjustments Adj
			INNER JOIN RP_Statements ST				ON ST.Statementcode=ADJ.Statementcode
			INNER JOIN #TEMP_ProductReference PR	ON PR.ProductCode=ADJ.ProductCode AND PR.Region=ST.Region
			LEFT JOIN #NEXT_TRANSNUMBER NT			ON NT.StatementCode=Adj.Statementcode

		/*					
			-- LETS UPDATE STATEMENT MAPPINGS : REMOVE INACTIVE AND SOLD RETAILERCODES WITH NO TRANSACTIONS FOR CURRENT SEASON		
			-- THIS PART IS NOW TAKEN CARE IN [dbo].[RP_Refresh_StatementMappings]		
		*/
	END

END