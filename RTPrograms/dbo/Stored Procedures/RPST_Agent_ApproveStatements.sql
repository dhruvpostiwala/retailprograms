﻿


CREATE PROCEDURE [dbo].[RPST_Agent_ApproveStatements] @SEASON INT,  @USERNAME VARCHAR(150), @USERROLE VARCHAR(20), @APPROVERTYPE VARCHAR(50), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	/* 
		LOCKED STATEMENTCODES ARE PROVIDED AS INPUT
		TARGET COULD BE MULTIPLE STATEMENTS OR SINGLE STATEMENT 
		DEPENDS ON VALUE FROM "LOCK_JOB_ID" COLUMN TO TARGET ALL STATEMENTS IN A FAMILY WHERE APPLICABLE

		WEST REGION:
			- ALL STATEMENT LEVELS TO BE APPROVED

		EAST REGION:
			- ALL STATEMENT LEVEL 1 TO BE APPROVED
			- OU STATEMENT (STATEMENT LEVEL 2) SHOULD NOT BE APPROVED
	*/
	
	SET NOCOUNT ON;
	
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	-- DECLARE @LOCKED_STATEMENTS UDT_RP_STATEMENT;		
	DECLARE @LOCKED_STATEMENTS TABLE(
		StatementCode INT,
		SRC_StatementCode INT NULL,
		DataSetCode INT NULL,
		Region VARCHAR(4) NULL,
		RetailerCode VARCHAR(20) NULL,
		Level5Code VARCHAR(20) NULL,
		StatementLevel VARCHAR(20) NULL,	
		Current_Status VARCHAR(100) NULL DEFAULT '',
		Next_Status VARCHAR(100) NULL DEFAULT '',
		Has_PendingExceptions Bit DEFAULT 0,
		Has_PendingIssue Bit DEFAULT 0,
		Category VARCHAR(50) NOT NULL DEFAULT 'Regular'
	)
		
	DECLARE @EAST_LEVEL2_QA TABLE (
		StatementCode INT,
		L1_StatementsCount INT,
		L1_NextStatusCount INT 
	)

	DECLARE @CURRENT_STATUS VARCHAR(100)='';
	DECLARE @NEXT_STATUS VARCHAR(100)='';
		

	BEGIN TRY

	-- TARGET STATEMENTS (LOCKED STATEMENTCODES)
	INSERT INTO @LOCKED_STATEMENTS(StatementCode)
	SELECT DISTINCT VALUE AS StatementCode  FROM String_Split(@STATEMENTCODES, ',')
		
	UPDATE T1
	SET SRC_StatementCode=UL.StatementCode 
		,DataSetCode=LCK.DataSetCode
		,Region=UL.Region
		,RetailerCode=UL.RetailerCode
		,Level5Code=UL.Level5Code
		,StatementLevel=UL.StatementLevel
		,Current_Status=STI.FieldValue
		,Category=UL.Category
	FROM @LOCKED_STATEMENTS T1
		INNER JOIN RPWeb_Statements LCK 			ON LCK.StatementCode=T1.StatementCode
		INNER JOIN RPWeb_Statements UL				ON UL.Statementcode=LCK.Src_Statementcode
		INNER JOIN RPWeb_StatementInformation STI	ON STI.Statementcode=UL.Statementcode AND STI.FieldName='Status'
	WHERE LCK.[Status]='Active' AND LCK.[VersionType]='Locked' AND LCK.[StatementType]='Actual'  

	-- DELETE @LOCKED_STATEMENTS WHERE [Has_PendingExceptions]=1
	-- DELETE @LOCKED_STATEMENTS WHERE [Has_PendingIssues]=1

	
	--Commenting following block until we finish July Early Payment
	-- EAST: OU STATEMENTS 	 
	-- OU STATEMENTS ARE NOT APPROVED BY INTERNAL (KENNA) APPROVERS AND REPS. INTERNAL APPROVERS AND REPS APPROVE LEVEL 1 STATEMENTS ONLY 	
	-- OU STATEMENTS ARE PROGRAMATICALLY MOVED TO NEXT LEVEL 'RR APPROVAL' OR 'QA APPROVAL' STATUS RESPECTIVELY UP ON ALL LEVEL 1 STATEMENTS ARE MOVED TO 'RR APPROVAL' OR 'QA APPROVAL' STATUS	
	DELETE @LOCKED_STATEMENTS WHERE [REGION]='EAST' AND [StatementLevel]='Level 2' AND Current_Status IN ('Kenna Approval','RR APPROVAL','Admin(K) Review Pending')

	
	/*
	/******************************/
	-- Comment following block after finishing July Early Payment
	UPDATE STI
	SET [FieldValue]='Ready For Payment'
	OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA Process - Approve','Field','Status',deleted.FieldValue,inserted.FieldValue
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	FROM RPWeb_StatementInformation STI		
		INNER JOIN @LOCKED_STATEMENTS T2		
		ON T2.SRC_StatementCode=STI.StatementCode		
	WHERE STI.[FieldName]='Status' 

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	
	
	DELETE @LOCKED_STATEMENTS WHERE [REGION]='EAST'
	DELETE @EDIT_HISTORY
	/******************************/
	*/
	
	-- EAST: LOCATION LEVEL STATEMENTS FROM A FAMILY: BEYOND "RR APPROVAL" SHOULD BE UPDATED TO MATCH OU STATEMENT STATUS
	DELETE @LOCKED_STATEMENTS WHERE [REGION]='EAST' AND [StatementLevel]='Level 1' AND DataSetCode <> 0 AND Current_Status NOT IN ('Kenna Approval','RR Approval','Admin(K) Review Pending')
	

	-- DETERMINE NEXT STATUS IN APPORVAL PROCESS		
	IF @ApproverType IN ('Kenna Statement Approver','Admin(K) Approver') 		
		BEGIN
		-- WEST RETAILERS 				
		UPDATE @LOCKED_STATEMENTS SET Next_Status='QA Approval'	WHERE [Region]='WEST' AND [Current_Status] IN ('Kenna Approval','Admin(K) Review Pending')
				
		-- EAST RETAILERS		
		/*			
			As per Standard Workflow Next_Status should be 'RR Approval'.
			But requested an exception to be added for 2020 season to push it to QA apporval starting January Cheque Run and Revert back to standard in 2021.
			-- https://kennahome.jira.com/browse/RP-3376 
			UPDATE @LOCKED_STATEMENTS SET Next_Status='RR Approval'	WHERE [Region]='EAST' AND [Current_Status] IN ('Kenna Approval','Admin(K) Review Pending')
		*/
		UPDATE @LOCKED_STATEMENTS SET Next_Status='QA Approval'	WHERE [Region]='EAST' AND [Current_Status] IN ('Kenna Approval','Admin(K) Review Pending') 	AND @SEASON <= 2020
		UPDATE @LOCKED_STATEMENTS SET Next_Status='RR Approval'	WHERE [Region]='EAST' AND [Current_Status] IN ('Kenna Approval','Admin(K) Review Pending') 	AND @SEASON >= 2021	


		IF @ApproverType = 'Admin(K) Approver' 		
			BEGIN
			-- WEST INDEPENDENT STATEMENTS	
			UPDATE @LOCKED_STATEMENTS SET Next_Status='Submitted For RC Reconciliation'	WHERE [Region]='WEST' AND [Level5Code]='D000001' AND  [Current_Status]='Ready For Payment' 
		
			-- SUMMARY STATEMENTS
			UPDATE @LOCKED_STATEMENTS SET Next_Status='Ready For Payment' WHERE [Current_Status]='Kenna Summary Approval' -- AND [Region]='WEST' 
			END
		END	

	ELSE IF @APPROVERTYPE='BR APPROVER' 	-- EAST 
		UPDATE @LOCKED_STATEMENTS SET Next_Status='QA Approval'	WHERE [Region]='EAST' AND [Current_Status]='RR Approval'

	ELSE IF @APPROVERTYPE='HORT APPROVER' 	-- EAST 
		UPDATE @LOCKED_STATEMENTS SET Next_Status='QA Approval'	WHERE [Region]='EAST' AND [Current_Status]='RR Approval'

	ELSE IF @ApproverType='BASF Statement Approver'
		BEGIN
		-- CUSTOM AERIAL
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Finance Approval'	WHERE [Region]='WEST' AND Category='Custom Aerial' AND [Current_Status]='QA Approval' 

		-- WEST INDEPENDENTS
		UPDATE @LOCKED_STATEMENTS SET Next_Status='RC Rep Approval'		WHERE [Region]='WEST' AND Category='Regular' AND [Level5Code]='D000001' AND [Current_Status]='QA Approval' 
		
		-- WEST FCL/COOP
		UPDATE @LOCKED_STATEMENTS SET Next_Status='SAM Approval'	WHERE [Region]='WEST' AND Category='Regular' AND [Level5Code]='D0000117' AND [Current_Status]='QA Approval'
				

		-- WEST LINE COMPANIES)
		UPDATE @LOCKED_STATEMENTS SET Next_Status='SAM Approval'	WHERE [Region]='WEST' AND Category='Regular' AND [Level5Code] IN ('D0000107','D520062427','D0000137','D0000244')  AND [Current_Status]='QA Approval'

		-- EAST
		UPDATE @LOCKED_STATEMENTS SET Next_Status='SAM Approval'	WHERE [Region]='EAST' AND [Current_Status]='QA Approval' 
		END
			   
	ELSE IF @APPROVERTYPE='RC APPROVER'	-- WEST INDEPENDENTS
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Finance Approval'	WHERE [Region]='WEST' AND [Level5Code]='D000001' AND [Current_Status]='RC Rep Approval' 
			
	ELSE IF @UserRole='SAM'
		BEGIN

		-- WEST FCL/COOP AND LINE COMPANIES
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Ready For Payment'	WHERE [Level5Code] IN ('D0000107','D520062427','D0000137','D0000244','D0000117')  AND  [Current_Status]='SAM Approval' 

		-- NON WEST FCL/COOP AND NON LINE COMPANIES
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Finance Approval'	WHERE  [Level5Code] NOT IN ('D0000107','D520062427','D0000137','D0000244','D0000117') AND	[Current_Status]='SAM Approval' 

		END

	ELSE IF @ApproverType='Finance Approver'		
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Ready For Payment'	WHERE [Current_Status]='Finance Approval'	
			   		

	--IF @ApproverType='Admin(K) Approver' -- WEST AND EAST	
		--UPDATE @LOCKED_STATEMENTS SET Next_Status='Submitted for Payment'	WHERE [Current_Status]='Ready For Payment'	

	/* ON UNLOCKED STATEMENT: UPDATE STATUS TO NEW_STATUS */
	UPDATE STI
	SET [FieldValue]=T2.[Next_Status]
	OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA Process - Approve','Field','Status',deleted.FieldValue,inserted.FieldValue
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	FROM RPWeb_StatementInformation STI		
		INNER JOIN @LOCKED_STATEMENTS T2		
		ON T2.SRC_StatementCode=STI.StatementCode		
	WHERE STI.[FieldName]='Status' AND T2.Next_Status <> ''

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	

	
		/* LETS DEAL WITH EAST LEVEL 2 AND LEVEL 1 STATEMENTS STATUS UPDATE */

/*
	============================================================================================================
	User Role			Current Status		East Level					Next Status	
	============================================================================================================
															Approved			|		Rejected
	============================================================================================================
	Internal Approver 	Kenna Approval			L1			RR Approval			|	Admin(K) Review Pending
	BR / HORT Approver	RR Approval				L1			QA Approval			|	Kenna Approval
	BASF				QA Approval				L2			SAM Approval		|	Kenna Approval
	SAM					SAM Approval			L2			Finance Approval	|	Kenna Approval
	Finance				Finance Approval		L2			Ready For Payment	|	Kenna Approval
	Kenna Admin			Ready For Payment		L2			Submit for Payment	|	Kenna Approval
	============================================================================================================
*/

	DELETE @EDIT_HISTORY

	IF @ApproverType IN ('Kenna Statement Approver','Admin(K) Approver') 	
		BEGIN
			SET @CURRENT_STATUS='Kenna Approval'
			-- JIRA: RP-3376
			IF @SEASON=2020
				SET @NEXT_STATUS='QA Approval'
			ELSE
				SET @NEXT_STATUS='RR Approval'
		END
	ELSE IF @USERROLE IN ('BR','Hort','RC') 
		BEGIN
			SET @CURRENT_STATUS='RR Approval'
			SET @NEXT_STATUS='QA Approval'
		END

	IF @NEXT_STATUS <> ''
		BEGIN
			-- UPDATE STATUS OF EAST OU STATEMENTS (STATEMENTLEVEL = LEVEL 2) 			
			-- IF ALL LEVEL 1 STATEMENTS ARE PUSHED TO @NEXT_STATUS THEN UPDATE STATUS OF OU STATEMENT TO @NEXT_STATUS

			INSERT INTO @EAST_LEVEL2_QA(StatementCode,L1_StatementsCount,L1_NextStatusCount)
			SELECT L2.[SRC_StatementCode]
				,SUM(1) AS L1_Statements_Count
				,SUM(IIF(STI.FieldValue=@NEXT_STATUS,1,0)) AS [L1_NextStatusCount]		
			FROM RPWeb_Statements L2
				INNER JOIN (
					-- GET LEVEL 2 STATEMENTCODE (i.e., DATASETCODE ON LEVEL 1 STATEMENTS)
					SELECT DISTINCT DataSetCode AS StatementCode
					FROM @LOCKED_STATEMENTS						
					WHERE [Region]='EAST' AND [StatementLevel]='LEVEL 1' AND [DataSetCode] < 0 AND [Next_Status]=@NEXT_STATUS
				) SRC
				ON SRC.StatementCode=L2.StatementCode 
				INNER JOIN RPWeb_Statements L1				ON L1.DataSetCode=L2.StatementCode				
				INNER JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=L1.SRC_StatementCode AND STI.FieldName='Status'				
			WHERE L1.[Status]='Active' AND L1.[StatementLevel]='LEVEL 1' AND L1.[VersionType]='Locked'
			GROUP BY L2.[SRC_StatementCode]

			-- UPDATING LEVEL 2 STATEMENT STATUS
			UPDATE STI
			SET [FieldValue]=@NEXT_STATUS
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Unlock','Field','Status',deleted.FieldValue,inserted.[FieldValue]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RPWeb_StatementInformation STI		
				INNER JOIN @EAST_LEVEL2_QA L2	
				ON L2.StatementCode=STI.StatementCode		
			WHERE STI.[FieldName]='Status' AND STI.[FieldValue]=@CURRENT_STATUS AND L2.[L1_NextStatusCount]=L2.[L1_StatementsCount]

		END

	ELSE IF @ApproverType IN ('BASF Statement Approver','SAM Approver','Finance Approver')
		BEGIN
		-- REGION EAST : IN A FAMILY
		-- WHEN LEVEL 2 STATUS IS UPDATED TO 'SAM Approval','Finance Approval','Ready For Payment'		
		-- UPDATE ALL LEVEL 1 STATEMENTS STATUS TO MATCH LEVEL 2 STATEMENT STATUS	
	
			UPDATE STI
			SET [FieldValue]=L2.[Status]
			FROM RPWeb_StatementInformation STI
				INNER JOIN RPWEB_Statements L1
				ON STI.StatementCode=L1.SRC_StatementCode
				INNER JOIN (
					-- GET LEVEL 2 STATEMENT STATUS
					SELECT StatementCode, Next_Status AS [Status]
					FROM @LOCKED_STATEMENTS 					
					WHERE [Region]='EAST' AND [StatementLevel]='LEVEL 2' AND [Next_Status] IN ('SAM Approval','Finance Approval','Ready For Payment')
				) L2
				ON L2.StatementCode=L1.DataSetCode				
			WHERE L1.[Status]='Active' AND L1.[StatementLevel]='LEVEL 1'  AND L1.[VersionType]='Locked'				
				AND STI.[FieldName]='Status' AND STI.[FieldValue] <> L2.[Status] AND STI.[FieldValue] IN ('QA Approval','SAM Approval','Finance Approval') 
		END			   

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message

END






