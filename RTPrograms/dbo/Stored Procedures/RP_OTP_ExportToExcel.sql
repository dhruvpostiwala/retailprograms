﻿CREATE PROCEDURE [dbo].[RP_OTP_ExportToExcel] @HTML_Data NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TABLE_HEAD NVARCHAR(MAX);
	DECLARE @TABLE_BODY NVARCHAR(MAX);

	SET @TABLE_HEAD = '
	<thead>
		<th style="mso-ansi-font-weight: bold;">Model</th>
		<th style="mso-ansi-font-weight: bold;">Retailer</th>
		<th style="mso-ansi-font-weight: bold;">Retailer Code</th>
		<th style="mso-ansi-font-weight: bold;">Payment Desc</th>
		<th style="mso-ansi-font-weight: bold;">Payment Amount</th>
		<th style="mso-ansi-font-weight: bold;">Payment Code</th>
		<th style="mso-ansi-font-weight: bold;">Payment Status</th>
		<th style="mso-ansi-font-weight: bold;">Payment Date</th>
		<th style="mso-ansi-font-weight: bold;">Comments</th>
	</thead>
	';

	SET @TABLE_BODY = CONVERT(NVARCHAR(MAX), (SELECT
			(SELECT RP.Region AS 'td' FOR XML PATH(''), TYPE),
			(SELECT RP.RetailerName AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.RetailerCode AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.PaymentDesc AS 'td' FOR XML PATH(''), TYPE),
			(SELECT 'mso-horizontal-page-align: right;' AS [td/@style], dbo.SVF_Commify(OTP.Amount, '$') AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.RewardCode AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.[Status] AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.PaymentDate AS 'td' FOR XML PATH(''), TYPE),
			(SELECT OTP.Comments AS 'td' FOR XML PATH(''), TYPE)
		FROM RP_Seeding_OneTimePayments OTP
			INNER JOIN RetailerProfile RP
			ON OTP.RetailerCode = RP.RetailerCode
		FOR XML PATH('tr'), ELEMENTS, TYPE
	));

	SET @TABLE_BODY = '<tbody>
							' + @TABLE_BODY + '
					  </tbody>';

	SET @HTML_Data = '
		<table>
			' + @TABLE_HEAD + '
			' + @TABLE_BODY + '
		</table>';

	SET @HTML_Data = Replace(Replace(@HTML_Data,'&lt;','<'),'&gt;','>');
    
END
