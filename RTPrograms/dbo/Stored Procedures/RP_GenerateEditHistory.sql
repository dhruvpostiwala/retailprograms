﻿
CREATE PROCEDURE [dbo].[RP_GenerateEditHistory] @EH_TYPE VARCHAR(50), @USERNAME VARCHAR(100), @EDIT_HISTORY RP_EDITHISTORY READONLY
AS
BEGIN
	DECLARE @EH_CODE INT
	DECLARE @EH_GROUP AS INT
	DECLARE @EDIT_HISTORY_UPDATE RP_EDITHISTORY
	
	/* GROUP DATA INTO LOGICAL "GROUPS" OF EDIT HISTORY ENTRIES (ie. Updating status of 1000 statements should create 1 entry with 1000 mappings */
	IF(EXISTS(SELECT [StatementCode],[EHLinkCode],[TagData] FROM @EDIT_HISTORY GROUP BY [StatementCode],[EHLinkCode],[TagData] HAVING COUNT(*)>1))
		BEGIN 
			INSERT INTO @EDIT_HISTORY_UPDATE([StatementCode],[EHLinkCode],[TagData],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage],[EHGROUPING])
			SELECT [StatementCode],[EHLinkCode],[TagData],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage],RANK() OVER(ORDER BY [StatementCode],[EHLinkCode],[TagData])
			FROM @EDIT_HISTORY
		END
	ELSE
		BEGIN
			INSERT INTO @EDIT_HISTORY_UPDATE([StatementCode],[EHLinkCode],[TagData],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage],[EHGROUPING])
			SELECT [StatementCode],[EHLinkCode],[TagData],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage],RANK() OVER(ORDER BY [EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage])
			FROM @EDIT_HISTORY
		END


	IF(EXISTS(SELECT * FROM @EDIT_HISTORY_UPDATE))
		IF(EXISTS(SELECT COUNT(DISTINCT [EHGrouping]) FROM @EDIT_HISTORY_UPDATE HAVING COUNT(DISTINCT [EHGrouping])>50))
			BEGIN
				DECLARE @EDIT_HISTORY_OPTIMIZED AS RP_EDITHISTORY_OPTIMIZED
				
				MERGE RPWeb_EditHistory AS dest
				USING(
					SELECT * FROM @EDIT_HISTORY_UPDATE
				)src ON 1=0
				WHEN NOT MATCHED THEN 
					INSERT ([EHType],[EHDate],[Username])
					VALUES (@EH_TYPE,GETDATE(),UPPER(@USERNAME))
					OUTPUT inserted.[EHCode],src.[StatementCode],src.[EHLinkCode],src.[TagData],src.[EHDisplay],src.[EHEntryType],src.[EHFieldName],src.[EHOriginalValue],src.[EHNewValue],src.[EHMessage],src.[EHGrouping]
					INTO @EDIT_HISTORY_OPTIMIZED([EHCode],[StatementCode],[EHLinkCode],[TagData],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage],[EHGrouping]);
					
					INSERT INTO RPWeb_EditHistoryMappings([StatementCode],[EHLinkCode],[EHCode],[TagData])
					SELECT [StatementCode],[EHLinkCode],[EHCode],COALESCE([TagData],'')
					FROM @EDIT_HISTORY_OPTIMIZED
							
					INSERT INTO RPWeb_EditHistoryEntry([EHCode],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage])
					SELECT [EHCode],COALESCE([EHDisplay],'YES'),[EHEntryType],[EHFieldName]
						,CASE WHEN COALESCE([EHOriginalValue],'')='' THEN '-blank-' ELSE [EHOriginalValue] END
						,CASE WHEN COALESCE([EHNewValue],'')='' THEN '-blank-' ELSE [EHNewValue] END
						,[EHMessage]
					FROM @EDIT_HISTORY_OPTIMIZED
					WHERE [EHEntryType] IS NOT NULL
			END
		ELSE
			BEGIN
				DECLARE EH_LOOP CURSOR FOR
					SELECT [EHGrouping]
					FROM @EDIT_HISTORY_UPDATE
					WHERE [EHGrouping] IS NOT NULL
					GROUP BY [EHGrouping]
				OPEN EH_LOOP
				FETCH NEXT FROM EH_LOOP INTO @EH_GROUP
					WHILE(@@FETCH_STATUS=0)
						BEGIN
							INSERT INTO RPWeb_EditHistory([EHType],[EHDate],[Username])
							SELECT @EH_TYPE,GETDATE(),UPPER(@USERNAME)
							SET @EH_CODE=SCOPE_IDENTITY()
							
							INSERT INTO RPWeb_EditHistoryMappings([StatementCode],[EHLinkCode],[EHCode],[TagData])
							SELECT [StatementCode],[EHLinkCode],@EH_CODE,COALESCE([TagData],'')
							FROM @EDIT_HISTORY_UPDATE
							WHERE [EHGrouping]=@EH_GROUP
							GROUP BY [StatementCode],[EHLinkCode],[TagData]
							
							INSERT INTO RPWeb_EditHistoryEntry([EHCode],[EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage])
							SELECT @EH_CODE,COALESCE([EHDisplay],'YES'),[EHEntryType],[EHFieldName]
								,CASE WHEN COALESCE([EHOriginalValue],'')='' THEN '-blank-' ELSE [EHOriginalValue] END
								,CASE WHEN COALESCE([EHNewValue],'')='' THEN '-blank-' ELSE [EHNewValue] END
								,[EHMessage]
							FROM @EDIT_HISTORY_UPDATE
							WHERE [EHGrouping]=@EH_GROUP AND [EHEntryType] IS NOT NULL
							GROUP BY [EHDisplay],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue],[EHMessage]
							
							FETCH NEXT FROM EH_LOOP INTO @EH_GROUP
						END
					CLOSE EH_LOOP
					DEALLOCATE EH_LOOP	
			END
END

