﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_E_Ignite_LoyaltySupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	
	IF @STATEMENT_TYPE <> 'ACTUAL'
	RETURN;


	/*
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 7878 or StatementCode IN (7878)
	DECLARE @SEASON INT = 2020
	DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2021_E_CUSTOM_GROUND_APP_REWARD'
	*/


	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		ProductCode VARCHAR(255) NOT NULL,
		RewardBrand_QTY DECIMAL(19,4) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		RewardPerUnit MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC,
			[ProductCode] ASC
		)
	)

	-- IGNITE 150 100 L DRUM
	-- IGNITE 150 2 X 10 L CASE
	-- IGNITE 150 50 L DRUM

	DECLARE @REWARD_PER_UNIT_CONFIG TABLE (
		ProductCode VARCHAR(200),
		Dollars_Per_Unit MONEY
	)
	INSERT INTO @REWARD_PER_UNIT_CONFIG(ProductCode, Dollars_per_Unit)
	SELECT ProductCode,
	CASE 
		WHEN PackSize = '100 L DRUM' THEN 550
		WHEN  PackSize = '50 L DRUM' THEN 250
		ELSE 100 END AS Dollars_Per_Unit
	FROM ProductReference 
	WHERE ChemicalGroup = 'IGNITE' --AND Status = 'Active'

	
	INSERT	INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, ProductCode, RewardBrand_QTY, RewardBrand_Sales)
	SELECT	ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode
			,SUM(TX.Acres_CY/PR.ConversionE) AS RewardBrand_QTY
			,SUM(TX.Price_CY) AS RewardBrand_Sales
	FROM	@STATEMENTS ST
		INNER	JOIN RP2021_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
		INNER	JOIN RP_Statements RS ON RS.StatementCode=ST.StatementCode
		INNER	JOIN Productreference PR ON TX.ProductCode = PR.ProductCode
	WHERE RS.StatementLEvel='LEVEL 1' AND TX.ProgramCode=@PROGRAM_CODE AND tx.GroupType = 'REWARD_BRANDS'
	GROUP	BY ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode

	-- CALCULATE THE REWARD PER UNIT
	UPDATE T1
	SET	[RewardPerUnit] = CON.[Dollars_Per_Unit]
	FROM @TEMP T1
	INNER JOIN @REWARD_PER_UNIT_CONFIG CON ON T1.ProductCode = CON.ProductCode 

	-- CALCULATE THE TOTAL REWARD
	UPDATE T1
	SET	[Reward] = [RewardBrand_QTY]*[RewardPerUnit]
	FROM @TEMP T1
	WHERE [RewardBrand_QTY] > 0 

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2021_DT_Rewards_E_Ignite_LoyaltySupport WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2021_DT_Rewards_E_Ignite_LoyaltySupport(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, ProductCode, RewardBrand_QTY, RewardPerUnit, Reward, RewardBrand_Sales)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, ProductCode, RewardBrand_QTY, RewardPerUnit, Reward, RewardBrand_Sales
    FROM @TEMP
		
      UNION

    SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, ProductCode
		,SUM(RewardBrand_QTY) AS RewardBrand_QTY
        ,MAX(RewardPerUnit) AS RewardPerUnit
        ,SUM(Reward) AS Reward
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
    FROM @TEMP
    WHERE DataSetCode > 0
    GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand, ProductCode

END
