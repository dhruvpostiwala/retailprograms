﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_E_PriaxorHeadlineSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
	Sell a minimum of 160 acres of Priaxor – If 160 acres are in POG plan = reward $1.00/acre on Total acres of Priaxor
	Sell a minimum of 80 acres of Headline – If 80 acres are in POG plan = reward $1.50/acre on Total acres of Headline.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 872 or StatementCode = 872
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_E_PRIAXOR_HEADLINE_REWARD'

	DECLARE @TEMP TABLE (
		StatementCode INT,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Priaxor_Acres NUMERIC(18,2) NOT NULL,
		Priaxor_Sales MONEY NOT NULL,
		Priaxor_Reward_Amount MONEY NOT NULL DEFAULT 0,		
		Priaxor_Reward MONEY NOT NULL DEFAULT 0,
		Headline_POG_Acres NUMERIC(18,2) NOT NULL,
		Headline_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		Headline_Sales MONEY NOT NULL,
		Headline_Reward_Amount MONEY NOT NULL DEFAULT 0,		
		Headline_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,Priaxor_Acres,Priaxor_Sales,Headline_POG_Acres,Headline_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode		
		,SUM(IIF(tx.GroupLabel='PRIAXOR',tx.Acres_CY,0)) AS Priaxor_Acres
		,SUM(IIF(tx.GroupLabel='PRIAXOR',tx.Price_CY,0)) AS Priaxor_Sales
		,SUM(IIF(tx.GroupLabel='HEADLINE',tx.Acres_CY,0)) AS Headline_POG_Acres	
		,SUM(IIF(tx.GroupLabel='HEADLINE',tx.Price_CY,0)) AS Headline_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode

	--HEADLINE APP RATE FOR THIS PROGRAM
	DECLARE @HEADLINE_APP_RATE FLOAT = cast (72 AS FLOAT) / Cast( 80 AS FLOAT) 		
	UPDATE @TEMP SET Headline_Acres = Headline_POG_Acres * @HEADLINE_APP_RATE  WHERE Headline_POG_Acres > 0

	
	UPDATE @TEMP SET Priaxor_Reward_Amount = 1  
	UPDATE @TEMP SET Headline_Reward_Amount = 1.5

	-- Update reward
	UPDATE @TEMP SET Priaxor_Reward = Priaxor_Acres * Priaxor_Reward_Amount WHERE Priaxor_Reward_Amount > 0
	UPDATE @TEMP SET Headline_Reward = Headline_Acres * Headline_Reward_Amount WHERE Headline_Reward_Amount > 0
	UPDATE @TEMP SET Reward = Priaxor_Reward + Headline_Reward WHERE Priaxor_Reward + Headline_Reward > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
			
	DELETE FROM RP2020_DT_Rewards_E_PriaxorHeadlineSupport WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_E_PriaxorHeadlineSupport(Statementcode, MarketLetterCode, ProgramCode, Priaxor_Acres,Priaxor_Sales, Priaxor_Reward_Amount, Priaxor_Reward, Headline_POG_Acres , Headline_Acres,Headline_Sales, Headline_Reward_Amount, Headline_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, Priaxor_Acres,Priaxor_Sales, Priaxor_Reward_Amount, Priaxor_Reward,Headline_POG_Acres, Headline_Acres,Headline_Sales, Headline_Reward_Amount, Headline_Reward, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode
		,SUM(Priaxor_Acres) AS Priaxor_Acres
		,SUM(Priaxor_Sales) AS Priaxor_Sales
		,AVG(Priaxor_Reward_Amount) AS Priaxor_Reward_Amount
		,SUM(Priaxor_Reward) AS Priaxor_Reward
		,SUM(Headline_POG_Acres) AS Headline_POG_Acres
		,SUM(Headline_Acres) AS Headline_Acres
		,SUM(Headline_Sales) AS Headline_Sales
		,AVG(Headline_Reward_Amount) AS Headline_Reward_Amount
		,SUM(Headline_Reward) AS Headline_Reward
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode

END
