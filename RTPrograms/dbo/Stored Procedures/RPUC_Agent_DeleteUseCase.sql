﻿

CREATE PROCEDURE [dbo].[RPUC_Agent_DeleteUseCase] @STATEMENTCODE INT 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	
	BEGIN TRY
		SELECT @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode

		UPDATE RP_Statements	Set Status='Deleted' Where VersionType='Use Case' AND (StatementCode=@STATEMENTCODE OR StatementCode=@DATASETCODE) 
		UPDATE RPWeb_Statements Set Status='Deleted', Deleted_Date=GetDate() Where VersionType='Use Case' AND (StatementCode=@STATEMENTCODE OR StatementCode=@DATASETCODE) 

		IF @REGION='EAST' AND @DATASETCODE > 0
		BEGIN   
			UPDATE RP_Statements	
			Set Status='Deleted'
			Where VersionType='Use Case' AND  DataSetCode=@DATASETCODE
			
			UPDATE RPWeb_Statements 
			Set Status='Deleted', Deleted_Date=GetDate()
			Where VersionType='Use Case' AND  DataSetCode=@DATASETCODE
		END		
		
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'success' AS [Status], '' AS Error_Message
		
END

