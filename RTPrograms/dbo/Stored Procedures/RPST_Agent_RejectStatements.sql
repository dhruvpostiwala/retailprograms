﻿

CREATE PROCEDURE [dbo].[RPST_Agent_RejectStatements] @SEASON INT,  @USERNAME VARCHAR(150), @USERROLE VARCHAR(20), @APPROVERTYPE VARCHAR(50), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	/* 
		LOCKED STATEMENTCODES ARE PROVIDED AS INPUT
		TARGET COULD BE MULTIPLE STATEMENTS OR SINGLE STATEMENT 
		DEPENDS ON VALUE FROM "LOCK_JOB_ID" COLUMN TO TARGET ALL STATEMENTS IN A FAMILY WHERE APPLICABLE

		WEST REGION:
			- ALL STATEMENT LEVELS TO BE APPROVED

		EAST REGION:
			- ALL STATEMENT LEVEL 1 TO BE APPROVED
			- OU STATEMENT (STATEMENT LEVEL 2) SHOULD NOT BE APPROVED
	*/
	
	SET NOCOUNT ON;
	
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	-- DECLARE @LOCKED_STATEMENTS UDT_RP_STATEMENT;		
	DECLARE @LOCKED_STATEMENTS TABLE(
		StatementCode INT,
		SRC_StatementCode INT NULL,
		DataSetCode INT NULL,
		Region VARCHAR(4) NULL,
		RetailerCode VARCHAR(20) NULL,
		Level5Code VARCHAR(20) NULL,
		StatementLevel VARCHAR(20) NULL,	
		Current_Status VARCHAR(100) NULL DEFAULT '',
		Next_Status VARCHAR(100) NULL DEFAULT ''
	)
		
	DECLARE @EAST_LEVEL2_QA TABLE (
		StatementCode INT,
		L1_StatementsCount INT,
		L1_NextStatusCount INT 
	)

	DECLARE @CURRENT_STATUS VARCHAR(100)='';
	DECLARE @NEXT_STATUS VARCHAR(100)='';


	BEGIN TRY		
	-- TARGET STATEMENTS (LOCKED STATEMENTCODES)
	INSERT INTO @LOCKED_STATEMENTS(StatementCode)
	SELECT DISTINCT VALUE AS StatementCode  FROM String_Split(@STATEMENTCODES, ',')

	UPDATE T1
	SET SRC_StatementCode=UL.StatementCode 
		,DataSetCode=LCK.DataSetCode
		,Region=UL.Region
		,RetailerCode=UL.RetailerCode
		,Level5Code=UL.Level5Code
		,StatementLevel=UL.StatementLevel
		,Current_Status=STI.FieldValue
	FROM @LOCKED_STATEMENTS T1
		INNER JOIN RPWeb_Statements LCK 			ON LCK.StatementCode=T1.StatementCode
		INNER JOIN RPWeb_Statements UL 				ON UL.StatementCode=LCK.SRC_StatementCode
		INNER JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=UL.StatementCode AND STI.FieldName='Status'
	WHERE LCK.[Status]='Active' AND LCK.[VersionType]='Locked' AND LCK.[StatementType]='Actual'  
	
	-- EAST: OU STATEMENTS 	 
	-- OU STATEMENTS ARE NOT APPROVED BY INTERNAL (KENNA) APPROVERS AND REPS. INTERNAL APPROVERS AND REPS APPROVE LEVEL 1 STATEMENTS ONLY 	
	-- OU STATEMENTS ARE PROGRAMATICALLY MOVED TO NEXT LEVEL 'RR APPROVAL' OR 'QA APPROVAL' STATUS RESPECTIVELY UP ON ALL LEVEL 1 STATEMENTS ARE MOVED TO 'RR APPROVAL' OR 'QA APPROVAL' STATUS	
	DELETE @LOCKED_STATEMENTS WHERE [REGION]='EAST' AND [StatementLevel]='Level 2' AND Current_Status IN ('Kenna Approval','RR APPROVAL','Admin(K) Review Pending')

	-- EAST: LOCATION LEVEL STATEMENTS BEYOND "RR APPROVAL" SHOULD BE UPDATED TO MATCH OU STATEMENT STATUS	
 	DELETE @LOCKED_STATEMENTS WHERE [REGION]='EAST' AND [StatementLevel]='Level 1' AND DataSetCode <> 0 AND Current_Status NOT IN ('Kenna Approval','RR Approval','Admin(K) Review Pending')

	-- DETERMINE NEXT STATUS IN APPORVAL PROCESS		
	
	IF @ApproverType IN ('Kenna Statement Approver') 				
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Admin(K) Review Pending'	WHERE [Current_Status]='Kenna Approval' 
		
	ELSE IF  @ApproverType = 'RC Recon Admin' 	-- WEST INDEPENDENTS
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Ready for Payment'	WHERE [Region]='WEST' AND [Level5Code]='D000001' AND [Current_Status]='Submitted For RC Reconciliation'

	ELSE IF @APPROVERTYPE='RC APPROVER'	-- WEST INDEPENDENTS
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Region]='WEST' AND [Level5Code]='D000001' AND [Current_Status]='RC Rep Approval'

	ELSE IF @APPROVERTYPE='RC-TL APPROVER' -- WEST INDEPENDENTS
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Region]='WEST'  AND [Level5Code]='D000001' AND [Current_Status]='RC Team Lead Approval'
	
	ELSE IF @APPROVERTYPE IN ('BR APPROVER','HORT APPROVER') 	-- EAST 
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Region]='EAST' AND [Current_Status]='RR Approval'

	ELSE IF @ApproverType='BASF Statement Approver'
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Current_Status]='QA Approval' 
		
	ELSE IF @UserRole='SAM'
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Current_Status]='SAM Approval' 	

	ELSE IF @ApproverType='Finance Approver'		
		UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Current_Status]='Finance Approval'

	--ELSE IF @ApproverType='Kenna Approver'		
		--UPDATE @LOCKED_STATEMENTS SET Next_Status='Ready for Payment'	WHERE [Current_Status]='Finance Approval'	
		
	--IF @ApproverType='Admin(K) Approver' -- WEST AND EAST	
		--UPDATE @LOCKED_STATEMENTS SET Next_Status='Kenna Approval'	WHERE [Current_Status]='Ready for Payment'	


	/* ON UNLOCKED STATEMENT: UPDATE STATUS TO NEW_STATUS */
	UPDATE STI
	SET [FieldValue]=T2.[Next_Status]
	OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA Process - Reject','Field','Status',deleted.FieldValue,inserted.FieldValue
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	FROM RPWeb_StatementInformation STI		
		INNER JOIN @LOCKED_STATEMENTS T2		
		ON T2.SRC_StatementCode=STI.StatementCode		
	WHERE STI.[FieldName]='Status' 

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	
	DELETE @EDIT_HISTORY
	
		/* LETS DEAL WITH EAST LEVEL 2 AND LEVEL 1 STATEMENTS STATUS UPDATE */

/*
	============================================================================================================
	User Role			Current Status		East Level					Next Status	
	============================================================================================================
															Approved			|		Rejected
	============================================================================================================
	Internal Approver 	Kenna Approval			L1			RR Approval			|	Admin(K) Review Pending
	BR / HORT Approver	RR Approval				L1			QA Approval			|	Kenna Approval
	BASF				QA Approval				L2			SAM Approval		|	Kenna Approval
	SAM					SAM Approval			L2			Finance Approval	|	Kenna Approval
	Finance				Finance Approval		L2			Ready For Payment	|	Kenna Approval
	Kenna Admin			Ready for Payment		L2			Submit for Payment	|	Kenna Approval
	============================================================================================================
*/
	IF EXISTS(SELECT * FROM  @LOCKED_STATEMENTS WHERE [REGION]='EAST')
	BEGIN
		SET @NEXT_STATUS='Kenna Approval'
		IF @ApproverType IN ('Kenna Statement Approver','Admin(K) Approver') 
			BEGIN			
				-- UPDATE STATUS OF EAST OU STATEMENTS (STATEMENTLEVEL = LEVEL 2) 			
				-- IF ALL LEVEL 1 STATEMENTS ARE PUSHED TO @NEXT_STATUS THEN UPDATE STATUS OF OU STATEMENT TO @NEXT_STATUS

				INSERT INTO @EAST_LEVEL2_QA(StatementCode,L1_StatementsCount,L1_NextStatusCount)
				SELECT L2.[SRC_StatementCode]
					,SUM(1) AS L1_Statements_Count
					,SUM(IIF(STI.FieldValue=@NEXT_STATUS,1,0)) AS [L1_NextStatusCount]		
				FROM  (						
						SELECT DISTINCT DataSetCode
						FROM @LOCKED_STATEMENTS						
						WHERE [Region]='EAST' AND [StatementLevel]='LEVEL 1' AND [DataSetCode] < 0 AND [Next_Status]=@NEXT_STATUS
					) SRC
					INNER JOIN RPWeb_Statements L2				ON SRC.DataSetCode=L2.StatementCode 
					INNER JOIN RPWeb_Statements L1				ON L1.DataSetCode=L2.StatementCode				
					INNER JOIN RPWeb_StatementInformation STI	ON STI.StatementCode=L1.SRC_StatementCode AND STI.FieldName='Status'				
				WHERE L1.[Status]='Active' AND L1.[VersionType]='Locked' AND  L1.[StatementLevel]='LEVEL 1'
				GROUP BY L2.[SRC_StatementCode]

				-- UPDATING LEVEL 2 STATEMENT STATUS
				UPDATE STI
				SET [FieldValue]=@NEXT_STATUS
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Unlock','Field','Status',deleted.FieldValue,inserted.[FieldValue]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RPWeb_StatementInformation STI		
					INNER JOIN @EAST_LEVEL2_QA L2	
					ON L2.StatementCode=STI.StatementCode		
				WHERE STI.[FieldName]='Status' AND STI.[FieldValue] <> @NEXT_STATUS AND L2.[L1_NextStatusCount] >= 1

			END

		ELSE IF @ApproverType IN ('BASF Statement Approver','SAM Approver','Finance Approver')
			BEGIN
			-- REGION EAST : IN A FAMILY
			-- WHEN LEVEL 2 STATUS IS UPDATED TO 'SAM Approval','Finance Approval','Ready For Payment'		
			-- UPDATE ALL LEVEL 1 STATEMENTS STATUS TO MATCH LEVEL 2 STATEMENT STATUS	
	
				UPDATE STI
				SET [FieldValue]=L2.[Status]
				FROM RPWeb_StatementInformation STI
					INNER JOIN RPWEB_Statements L1
					ON STI.StatementCode=L1.SRC_StatementCode
					INNER JOIN (
						-- GET LEVEL 2 STATEMENT STATUS
						SELECT StatementCode, Next_Status AS [Status]
						FROM @LOCKED_STATEMENTS 					
						WHERE [Region]='EAST' AND [StatementLevel]='LEVEL 2' AND [Next_Status]=@NEXT_STATUS
					) L2
					ON L1.DataSetCode=L2.StatementCode
				WHERE L1.[Status]='Active' AND  L1.[VersionType]='Locked' AND L1.[StatementLevel]='LEVEL 1'  
					AND STI.[FieldName]='Status' AND STI.[FieldValue] IN ('QA Approval','SAM Approval','Finance Approval') 
			END			   
	END
	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	
	
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

	--STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message
	
END




