﻿



CREATE   PROCEDURE [dbo].[RPUC_Agent_UpdateOrders] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	
	BEGIN TRY

		IF OBJECT_ID('tempdb..#Orders_To_Delete') IS NOT NULL DROP TABLE #Orders_To_Delete
		IF OBJECT_ID('tempdb..#Orders_To_Insert') IS NOT NULL DROP TABLE #Orders_To_Insert
		IF OBJECT_ID('tempdb..#AllOrders_To_Insert') IS NOT NULL DROP TABLE #AllOrders_To_Insert

		SELECT @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode
		--SET @JSON='{"created":[{"RP_ID":"","retailercode":"R400011638","farmcode":"F0059754","inradius":"No","invoicedate":"2020-04-22T16:26:18.460Z","product":{"productcode":"PRD-CIRD-B24KLL","productname":"INVIGOR L255PC PROSPER 22.7 KG BAG"},"productname":"","quantity":100}],"updated":[],"destroyed":[]}'

		-- LETS GET StatementTransNumber to be deleted 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @StatementCode AS Statementcode, t1.RP_ID
		INTO #Orders_To_Delete
		FROM (
			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b	

				UNION ALL

			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b		
		) T1			


		-- ALL STATEMENTS TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode ,t1.*
		INTO #Orders_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (												
							retailercode varchar(20) N'$.retailercode',
							farmcode VARCHAR(20) N'$.farm.farmcode',							
							invoicedate datetime N'$.invoicedate',
							productcode varchar(65) N'$.product.productcode',
							quantity numeric(18,2) N'$.quantity',					
							inradius varchar(3) N'$.inradius'						
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								farmcode VARCHAR(20) N'$.farm.farmcode',								
								invoicedate datetime N'$.invoicedate',
								productcode varchar(65) N'$.product.productcode',
								quantity numeric(18,2) N'$.quantity',							
								inradius varchar(3) N'$.inradius'						
							) as b
		) T1
	

		DELETE T1		
		FROM RP_DT_Orders T1
			INNER JOIN #Orders_To_Delete T2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.RP_ID=T2.RP_ID
		
		
		/*  
			
			PRIMARY KEY:
			[StatementCode] ASC,
			[StatementTransNumber] ASC		

			LETS REGENERATE VALUES FOR '[StatementTransNumber]' 
		
			STEP #1: COLLECT DATA FROM ACTUAL TABLE INTO TEMPORARY TABLE. SINCE WE ARE GOING TO UPDATE '[StatementTransNumber]' COLUMN, NO NEED TO FETCH IT 				
			STEP #2: DELETE RECORDS FROM ACTUAL TABLE WHERE STATEMENTCODE=@STATEMENTCODE
			STEP #3: INSERT RECORDS BACK INTO ACTUAL TABLE FROM TEMPORARY TABLE. USE 'ROW_NUMBER()' FUNCTION TO INSERT VALUE INTO '[StatementTransNumber]' COLUMN

		*/

		-- STEP #1: Collect data into temporary table #ALLSales_To_Insert
		SELECT StatementCode,DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius
		,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode, ProductCode) AS RP_ID
		INTO #AllOrders_To_Insert
		FROM (
			SELECT StatementCode,DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius
			FROM RP_DT_Orders
			WHERE StatementCode=@StatementCode
		
				UNION ALL

			SELECT Statementcode
				,@DataSetCode
				,'Use Case' AS TransCode
				--,YEAR(tx.invoicedate) AS Season
				,IIF(MONTH(tx.invoicedate) IN (10,11,12),YEAR(tx.invoicedate)+1,YEAR(tx.invoicedate)) AS Season	
				,tx.retailercode AS RetailerCode
				,tx.farmcode AS FarmCode
				,tx.ProductCode AS ProductCode
				,1 AS Conversion
				,tx.invoicedate as InvoiceDate
				,'Use Case' AS InvoiceNumber
				,tx.quantity AS Quantity
				,IIF(inradius='YES',1,0) AS InRadius
			FROM #Orders_To_Insert tx
		) T1	

		
		-- STEP #2: DELETE RECORDS FROM  RP_DT_TRANSACTIONS
		DELETE RP_DT_Orders WHERE StatementCode=@StatementCode
		
		-- STEP #3: INSERT RECORDS FROM #AllSales_To_Insert INTO RP_DT_TRANSACTIONS 
		INSERT INTO RP_DT_Orders (StatementCode,DataSetCode,RP_ID,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius)
		SELECT StatementCode,DataSetCode,RP_ID,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius		
		FROM #AllOrders_To_Insert		
				
		IF @REGION='EAST' AND @DATASETCODE > 0 
		BEGIN
			DELETE RP_DT_Orders WHERE StatementCode=@DataSetCode

			INSERT INTO RP_DT_Orders (StatementCode,DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius,RP_ID)
			SELECT @DataSetCode AS StatementCode, 0 AS DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,InRadius
				,ROW_NUMBER() OVER(PARTITION BY @DataSetCode ORDER BY RetailerCode, ProductCode) AS RP_ID
			FROM RP_DT_Orders
			WHERE DataSetCode=@DataSetCode
		END
		
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

