﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_InventoryReduction] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @TEMP_SALES TABLE (
	    Statementcode INT NOT NULL,
		RewardBrand Varchar(100) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Inventory_Sales MONEY NOT NULL DEFAULT 0
	);


	DECLARE @TEMP TABLE (
		Statementcode INT NOT NULL,
		ProgramCode Varchar (50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Inventory_Sales MONEY NOT NULL DEFAULT 0,
		Matched_Sales MONEY NOT NULL DEFAULT 0,
		RewardSegment Varchar (50) NOT NULL DEFAULT '',
		RewardPer MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			ProgramCode ASC,
			RewardBrand ASC
		)
	);


	-- Get the POG sales from consolidated table
	INSERT INTO @TEMP_SALES(StatementCode, RewardBrand, RewardBrand_Sales, Inventory_Sales)
	SELECT ST.StatementCode, PR.ChemicalGroup, SUM(TX.Price_CY) [Reward_Sales], 0 Inventory_Sales
	FROM @STATEMENTS ST
	    INNER JOIN RP_Statements RPS
		ON ST.StatementCode = RPS.StatementCode
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
	GROUP BY ST.StatementCode, PR.ChemicalGroup


	--Get the Inventory sales from RP_Seeding_InventoryAcres table
	INSERT INTO @TEMP_SALES(Statementcode, RewardBrand, RewardBrand_Sales, Inventory_Sales)
	SELECT ST.StatementCode, PR.ChemicalGroup, 0 AS RewardBrand_Sales, SUM((SIA.Acres/PR.Conversion) * PRP.SRP) AS Inventory_Sales
	FROM @STATEMENTS ST
	INNER JOIN RP_StatementsMappings Map	 
		ON Map.StatementCode = ST.StatementCode 	
	LEFT JOIN RP_Seeding_InventoryAcres SIA 
		ON SIA.RetailerCode = Map.ActiveRetailerCode
	LEFT JOIN ProductReference PR 
		ON SIA.ProductCode = PR.ProductCode
	LEFT JOIN ProductReferencePricing PRP 
		ON SIA.ProductCode = PRP.ProductCode AND PRP.Region='West' AND PRP.Season=@SEASON
	WHERE SIA.RetailerCode IS NOT NULL	
	GROUP BY ST.StatementCode, PR.ChemicalGroup


	INSERT INTO @TEMP(StatementCode, ProgramCode, RewardBrand, RewardBrand_Sales, Inventory_Sales)
	SELECT StatementCode, @PROGRAM_CODE AS ProgramCode, RewardBrand, SUM(RewardBrand_Sales), SUM(Inventory_Sales)
	FROM @TEMP_SALES
	GROUP BY StatementCode, RewardBrand
	

	--Set reported opening inventory as a maximum for each brand
	UPDATE T1
		SET Matched_Sales = IIF(T2.RewardBrand_Sales > T2.Inventory_Sales,T2.Inventory_Sales,T2.RewardBrand_Sales)
	FROM @TEMP T1
		LEFT JOIN @TEMP T2
		ON T1.Statementcode = T2.Statementcode AND T1.RewardBrand = T2.RewardBrand


    --Set the reward percentage for each segment
	UPDATE T1
	    SET RewardSegment = ISNULL(CG.GroupLabel,''),
		RewardPer = (CASE CG.GroupLabel 
		             WHEN 'Segment1' THEN .02 
					 WHEN 'Segment2' THEN .025 
					 WHEN 'Segment3' THEN .0275 
					 ELSE 0 END)
	FROM @TEMP T1
	LEFT JOIN [RP_Config_Groups_Brands] CGB
	ON T1.RewardBrand = CGB.ChemicalGroup
	LEFT JOIN [RP_Config_Groups] CG
	ON CGB.GroupID = CG.GroupID
	WHERE CG.ProgramCode = @PROGRAM_CODE AND CG.GroupLabel IN ('Segment1','Segment2','Segment3')


	--Delete the non reward brands from the temp table
	DELETE FROM @TEMP
	WHERE RewardSegment = ''
	
	
	--Calculate the reward amount
	UPDATE T1
		SET Reward = T1.Matched_Sales * T1.RewardPer
	FROM @TEMP T1

	-- Copy data from temporary table to permanent table
	DELETE FROM RP2021_DT_Rewards_W_InventoryReduction WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_InventoryReduction([Statementcode], [MarketLetterCode], [ProgramCode], [RewardBrand], [RewardBrand_Sales], [Inventory_Sales], [Matched_Sales], [RewardPer], [Reward])
	SELECT [Statementcode], 'ML2021_W_CPP', @PROGRAM_CODE, [RewardBrand], [RewardBrand_Sales], [Inventory_Sales], [Matched_Sales], [RewardPer], [Reward]
	FROM @TEMP
	
END

