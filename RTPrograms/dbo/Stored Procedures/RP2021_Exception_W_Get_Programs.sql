﻿

CREATE PROCEDURE [dbo].[RP2021_Exception_W_Get_Programs] 
	@STATEMENTCODE INT, 
	@JSON_DATA NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON INT=2021
	DECLARE @REGION VARCHAR(4)='WEST'
		
	DECLARE @TEMP TABLE (
		StatementCode INT,
		reward_code VARCHAR(50),
		title  VARCHAR(100),
		exception_type  VARCHAR(50)
	)
		
	SET @JSON_DATA='[]'

	
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	
	--INSERT INTO @STATEMENTS (StatementCode)
	--VALUES(@STATEMENTCODE)

	--DECLARE @EXCEPTIONS AS [dbo].[UDT_RP_EXCEPTIONS_DATA]
	--INSERT INTO @EXCEPTIONS
	--EXEC [dbo].[RP2021_Exceptions_W_GetRewardsData] @STATEMENTS , 'Records'

	--UPDATE @EXCEPTIONS 	SET RewardCode += 'X'


	--	-- User should not be able to create another exception of the same type on the statement if they already have one already
	----in  submitted or approved status 
	--DELETE T1
	--FROM @EXCEPTIONS T1
	--	INNER JOIN RPWeb_Exceptions W 
	--	ON W.StatementCode = T1.StatementCode and W.RewardCode = T1.rewardcode
	--WHERE W.[Status] NOT IN ('Rejected','Obsolete') and W.exception_type <> 'OTHER' 

	--SELECT @JSON_DATA = ISNULL((
	--	SELECT cfg.RewardLabel as title
	--			,ex.exception_type 
	--			,ex.RewardCode as reward_code				
	--			,ex.cy_sales_a,cy_sales_b					
	--			,ex.current_reward_a,current_reward_b	
	--			,ex.current_margin_a,current_margin_b
	--			,ex.next_margin_a,next_margin_b										
	--			,JSON_QUERY(ex.reward_margins_a) AS reward_margins_a
	--			,JSON_QUERY(ex.reward_margins_b) as reward_margins_b
	--	FROM @EXCEPTIONS ex
	--		INNER JOIN RP_Config_Rewards_Summary CFG
	--		ON CFG.RewardCode=ex.RewardCode AND CFG.Season=@SEASON
	--	FOR JSON PATH
	--) ,'[]')
	
	--RETURN


	DROP TABLE IF EXISTS #RewardCodes

	SELECT DISTINCT StatementCode, P.RewardCode + 'X' AS RewardCode 
	INTO #RewardCodes
	FROM (
				--IN CASE THERE IS NOTHING IN THE TABLE ITSELF SOME PROGRAMS DONT INSERT WHENN IT IS ZERO 

			SELECT RS.StatementCode, PS.ProgramCode
			FROM RPWeb_Rewards_Summary RS 
				INNER JOIN RP_Config_Programs PS 
				ON PS.RewardCode = RS.RewardCode
			WHERE RS.StatementCode=@StatementCode AND PS.Region = @REGION 
				--AND RS.TotalReward = 0  
					
			--Tier Reward Programs
				UNION ALL

			/*
				Brand Specific Support
				RC (ind) Retailer 4.5%,	4 %, 3 %
				SELECT * FROM [dbo].[RP2021Web_Rewards_W_BrandSpecificSupport]
			*/

			SELECT StatementCode,ProgramCode
			FROM [dbo].[RP2021Web_Rewards_W_BrandSpecificSupport]
			WHERE statementcode=@statementcode 
				--AND RewardBrand_Sales > 0 
				AND Reward_Percentage < 0.045
				
				
				UNION ALL

			/* 
				Custom Seed Treating Reward (CPP)  
				RC (ind) retailer 24%, 20%, 16%
				SELECT * FROM RP2021Web_Rewards_W_CustomSeed
			*/

			SELECT StatementCode,ProgramCode
			FROM [dbo].[RP2021Web_Rewards_W_CustomSeed]
			WHERE statementcode=@statementcode 				
			--	AND Custom_Treated_POG_Sales > 0
				AND RewardBrand_Percentage < 0.24
				

				UNION ALL
			/*
				Efficiency Rebate
				RC (ind) retailer  2.5% , 1.75%, 1%
				SELECT * FROM RP2021Web_Rewards_W_EFF_BONUS
				--Support_Reward_Percentage
			*/
									
			SELECT StatementCode,ProgramCode
			FROM [dbo].[RP2021Web_Rewards_W_EFF_BONUS]
			WHERE statementcode=@statementcode 				
				AND Support_Reward_Sales > 0 
				AND Support_Reward_Percentage < 0.025
			

				UNION ALL
			/* 
				GROWTH BONUS
				RC (IND) Retailer 6.25%
				SELECT * FROM  [RP2021Web_Rewards_W_GrowthBonus]
			*/

			SELECT StatementCode,ProgramCode
			FROM [dbo].[RP2021Web_Rewards_W_GrowthBonus]
			WHERE statementcode=@statementcode 
				AND QualSales_CY > 0 
				AND Reward_Percentage < 0.0625

				UNION ALL
			/*
				InVigor Performance Reward (InVigor)
				RC (IND) retailer 2%
				SELECT * FROM [dbo].[RP2021Web_Rewards_W_INV_PERF]
			*/
			SELECT StatementCode,ProgramCode
			FROM [dbo].[RP2021Web_Rewards_W_INV_PERF]
			WHERE statementcode=@statementcode 
				AND RewardBrand_Sales > 0 
				AND Reward_Percentage < 0.02
		) PRG 
			INNER JOIN RP_Config_Programs P 
			ON PRG.ProgramCode = P.ProgramCode 
		WHERE P.RewardCode NOT IN ('INVENTORY_MGMT_W','TM_BONUS_W','POT_W','CLL_W','WH_PAYMENT_W','CEREAL_S_F_W','CST_W','NOD_DUO_SCG_W','FUNG_W','ADV_W_CTRL_W','CUSTOM_AA_W'
			,'DITP_W'
			,'SEG_SELL_W','INV_INB_W') 

	
	INSERT INTO @TEMP(StatementCode,[reward_code],[title],exception_type)
	SELECT EXCP.StatementCode,EXCP.RewardCode [reward_code], RewardLabel as [title],'NON-TIER' AS exception_type
	FROM [dbo].[RP_Config_Rewards_Summary] S
		INNER JOIN #RewardCodes EXCP 		ON S.RewardCode = EXCP.RewardCode
		INNER JOIN RPWeb_Statements ST 		ON ST.StatementCode = EXCP.StatementCode AND DatasetCode = 0
	WHERE S.Season = @Season 	AND S.RewardType = 'Exception'		
			
	-- User should not be able to create another exception of the same type on the statement if they already have one already
	--in  submitted or approved status   
	DELETE T1
	FROM @TEMP T1
		INNER JOIN (
			SELECT SRC_StatementCode, RewardCode
			FROM RPWeb_Exceptions 
			WHERE Season = @SEASON AND [Status] NOT IN ('Rejected','Obsolete','Void') and exception_type <> 'OTHER' 
			GROUP BY SRC_StatementCode, RewardCode
		) W 
		ON W.SRC_StatementCode = T1.StatementCode and W.RewardCode = T1.reward_code
	--WHERE W.[Status] NOT IN ('Rejected','Obsolete','Void') and W.exception_type <> 'OTHER' 
		
	

	UPDATE @TEMP 
	SET exception_type  = 'TIER' 
	WHERE Reward_Code IN ('BSS_WX','CST_WX','EBR_WX','GROWTH_WX','INV_PER_WX','LOYALTY_WX')

	SELECT @JSON_DATA = ISNULL((
	SELECT reward_code, title,exception_type 
	FROM @TEMP
	FOR JSON PATH) ,'[]')

END