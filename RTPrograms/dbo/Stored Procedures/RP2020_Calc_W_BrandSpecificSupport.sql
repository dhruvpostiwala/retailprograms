﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_W_BrandSpecificSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
		2020 InVigor POG Plan /  Manual Input InVigor Target @ SDP X Corresponding InVigor % in table above
	*/
	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @Temp TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,						
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,	
		Level5Code Varchar(10) NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(7,4) NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	INSERT INTO @Temp(StatementCode,MarketLetterCode,ProgramCode,RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2,Level5Code)
	SELECT ST.StatementCode, tx.MarketLetterCode,tx.ProgramCode
		,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
		,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Price_CY,0)) AS QualBrand_Sales_CY
		,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Price_LY1,0)) AS QualBrand_Sales_LY1
		,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Price_LY2,0)) AS QualBrand_Sales_LY2,
		Max(S.Level5Code) Level5Code
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements S on S.StatementCode = TX.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN  ('REWARD_BRANDS','QUALIFYING_BRANDS')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode



	UPDATE @TEMP
	SET Growth=QualBrand_Sales_CY / ((QualBrand_Sales_LY1+QualBrand_Sales_LY2)/2)
	WHERE QualBrand_Sales_LY1+QualBrand_Sales_LY2 > 0

	/*
	Growth in 2020 relative to the 2-year average	Reward on 2020 POG sales of InVigor
	110% +	4.5%
	106% to 109.9%	4%
	103% to 105.9%	3.5%
	100% to 102.9%	3%
	95% to 99.9%	2%
	90% to 94.9%	1%
	*/
	


	-- Rounding Rule: Apply a rounding rule of 0.5% to all rewards
	-- RP-1646 fixed so if QualBrand_Sales_CY = 0 then growth should be 0. If 2-yr avg sales is 0 then we usually give the max
	-- UPDATE @TEMP  SET [Reward_Percentage]=0.045 WHERE Growth=0 OR Growth >= 1.1

	DECLARE @WEST_IND VARCHAR(10) = 'D000001';
	DECLARE @WEST_COOP VARCHAR(10) = 'D0000117';
	DECLARE @CARGILL VARCHAR(10) = 'D0000107';
	DECLARE @UFA VARCHAR(10) = 'D0000244';
	DECLARE @NUTRIEN VARCHAR(10) = 'D520062427';
	DECLARE @RICHARDSON VARCHAR(10) = 'D0000137';



	-- Reward Tiers IND - COOPs 
	UPDATE @TEMP  SET [Reward_Percentage]=0.045	WHERE (Growth=0 AND QualBrand_Sales_CY>0) OR Growth+@Roundup_Value >= 1.1 AND Level5Code IN (@WEST_IND,@WEST_COOP )
	UPDATE @TEMP  SET [Reward_Percentage]=0.04 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.06 AND Level5Code IN (@WEST_IND,@WEST_COOP )
	UPDATE @TEMP  SET [Reward_Percentage]=0.035	WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.03 AND Level5Code IN (@WEST_IND,@WEST_COOP )
	UPDATE @TEMP  SET [Reward_Percentage]=0.03 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1 AND Level5Code IN (@WEST_IND,@WEST_COOP )
	UPDATE @TEMP  SET [Reward_Percentage]=0.02 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.95 AND Level5Code IN (@WEST_IND,@WEST_COOP )
	UPDATE @TEMP SET [Reward_Percentage]=0.01 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.90 AND Level5Code IN (@WEST_IND,@WEST_COOP )

	--DISTRIBUTORS
	--CARGILL AND UFA
	UPDATE @TEMP  SET [Reward_Percentage]=0.035	WHERE (Growth=0 AND QualBrand_Sales_CY>0) OR Growth+@Roundup_Value >= 1.1 AND Level5Code IN (@CARGILL,@UFA )
	UPDATE @TEMP  SET [Reward_Percentage]=0.03 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.06 AND Level5Code IN (@CARGILL,@UFA )
	UPDATE @TEMP  SET [Reward_Percentage]=0.025	WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.03 AND Level5Code IN (@CARGILL,@UFA )
	UPDATE @TEMP  SET [Reward_Percentage]=0.02 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1 AND Level5Code IN (@CARGILL,@UFA )
	UPDATE @TEMP  SET [Reward_Percentage]=0.015 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.95 AND Level5Code IN (@CARGILL,@UFA )
	UPDATE @TEMP SET [Reward_Percentage]=0.01 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.90 AND Level5Code IN (@CARGILL,@UFA )

	--RICHARDSON
	UPDATE @TEMP  SET [Reward_Percentage]=0.045	WHERE (Growth=0 AND QualBrand_Sales_CY>0) OR Growth+@Roundup_Value >= 1.1 AND Level5Code = @RICHARDSON 
	UPDATE @TEMP  SET [Reward_Percentage]=0.04 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.06 AND Level5Code = @RICHARDSON 
	UPDATE @TEMP  SET [Reward_Percentage]=0.035	WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1.03 AND Level5Code = @RICHARDSON 
	UPDATE @TEMP  SET [Reward_Percentage]=0.03 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 1 AND Level5Code = @RICHARDSON 
	UPDATE @TEMP  SET [Reward_Percentage]=0.02 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.95 AND Level5Code = @RICHARDSON 
	UPDATE @TEMP SET [Reward_Percentage]=0.01 WHERE [Reward_Percentage]=0 AND Growth+@Roundup_Value >= 0.90 AND Level5Code = @RICHARDSON 

	--NUTRIEN
	UPDATE @TEMP  SET [Reward_Percentage]=0.01	WHERE (Growth=0 AND QualBrand_Sales_CY>0) OR Growth+@Roundup_Value >= 1.1 AND Level5Code = @NUTRIEN 

		
	UPDATE @Temp  SET [Reward]=RewardBrand_Sales * Reward_Percentage WHERE [Reward_Percentage] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
			
	DELETE FROM RP2020_DT_Rewards_W_BrandSpecificSupport WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_BrandSpecificSupport(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, 'InVigor', RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward
	FROM @TEMP
	
END
