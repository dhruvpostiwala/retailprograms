﻿



CREATE PROCEDURE [dbo].[RP2020_Calc_E_RowCropFungSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
	NEW: RC-3760 - Qualification is done at the OU level. We take all the POG$ from each level 1 within the OU combine them in the Level 2 summary and then compare that number with last year

	This reward is based on your continued support of the following BASF Qualifying Fungicides:
	Caramba, Cotegra, Headline AMP, Headline, Priaxor and Twinline

	Reward Opportunity
	If a retailer’s combined 2020 POG sales of BASF Qualifying Fungicides are greater than their 2019 POG sales of these products, the retailer will
	be eligible to receive a reward on BASF Qualifying Fungicide POG sales at outlined in the table below.

	100% to 109.99%		$1.25/acre
	110%+				$1.75/acre

	Also: Retailers must exceed 2,000 acres of 2020 POG Sales of BASF Qualifying Fungicides to be eligible for this reward.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE  DataSetCode = 872 or StatementCode = 872
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2021_E_FUNGICIDES_SUPPORT'

	DECLARE @TEMP TABLE (
		StatementCode INT,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,	
		RewardBrand_Acres NUMERIC(18,2) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		QualBrand_Acres_CY NUMERIC(18,2) NOT NULL,
		QualBrand_Acres_LY NUMERIC(18,2) NOT NULL,
		QualBrand_Sales_CY NUMERIC(18,2) NOT NULL DEFAULT 0,
		QualBrand_Sales_LY NUMERIC(18,2) NOT NULL DEFAULT 0,
		Reward_Amount MONEY NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,
		Growth NUMERIC(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand_Acres NUMERIC(18,2) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		QualBrand_Acres_CY NUMERIC(18,2) NOT NULL,
		QualBrand_Acres_LY NUMERIC(18,2) NOT NULL,
		QualBrand_Sales_CY NUMERIC(18,2) NOT NULL DEFAULT 0,
		QualBrand_Sales_LY NUMERIC(18,2) NOT NULL DEFAULT 0,
		Growth NUMERIC(18,2) NOT NULL DEFAULT 0,
		Reward_Amount MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- Qualifying Brand sales calculated on Actual POG sale
	-- Reward Brand Sales calculated on Eligible POG Sale
	-- Ticket RP-2924
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Acres,RewardBrand_Sales,QualBrand_Acres_CY,QualBrand_Acres_LY,QualBrand_Sales_CY,QualBrand_Sales_LY)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel
	/*, SUM(tx.Acres_CY) AS RewardBrand_Acres
	, SUM(tx.Price_CY) AS RewardBrand_Sales
	, SUM(tx.Acres_CY) AS QualBrand_Acres_CY
	, SUM(tx.Acres_LY1) AS QualBrand_Acres_LY
	, SUM(tx.Price_CY) AS QualBrand_Sales_CY
	, SUM(tx.Price_LY1) AS QualBrand_Sales_LY*/
	, SUM(tx.Acres_CY) AS RewardBrand_Acres
	, SUM(tx.Price_CY) AS RewardBrand_Sales
	, SUM(tx.Acres_Q_CY) AS QualBrand_Acres_CY
	, SUM(tx.Acres_Q_LY1) AS QualBrand_Acres_LY
	, SUM(tx.Price_Q_SDP_CY) AS QualBrand_Sales_CY
	, SUM(tx.Price_Q_SDP_LY1) AS QualBrand_Sales_LY
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX	ON TX.StatementCode=ST.StatementCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel

	-- Determine growth based on the sum of brand acres at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Acres,RewardBrand_Sales,QualBrand_Acres_CY,QualBrand_Acres_LY,QualBrand_Sales_CY,QualBrand_Sales_LY)
	SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(RewardBrand_Acres) AS RewardBrand_Acres
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(QualBrand_Acres_CY) AS QualBrand_Acres_CY
		,SUM(QualBrand_Acres_LY) AS QualBrand_Acres_LY
		,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
		,SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	-- Determine growth on total acres even though we are storing the data at brand level sales
	UPDATE @TEMP_TOTALS
	SET Growth = QualBrand_Sales_CY / QualBrand_Sales_LY
	WHERE QualBrand_Sales_LY > 0

	-- Update the reward $
	UPDATE @TEMP_TOTALS SET [Reward_Amount] = 1.75 WHERE [QualBrand_Acres_CY] >= 2000 AND ([QualBrand_Acres_LY] = 0 OR [Growth] >= 1.1)
	UPDATE @TEMP_TOTALS SET [Reward_Amount] = 1.25 WHERE [Reward_Amount] = 0 AND [QualBrand_Acres_CY] >= 2000 AND [Growth] >= 1

	-- Update the growth % and reward $ back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Amount = T2.Reward_Amount
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward $ back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Amount = T2.Reward_Amount
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0

	-- Update reward
	UPDATE @TEMP SET [Reward] = RewardBrand_Acres * Reward_Amount

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
			
	DELETE FROM RP2020_DT_Rewards_E_RowCropFungSupport WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_E_RowCropFungSupport(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Acres,RewardBrand_Sales, QualBrand_Acres_CY, QualBrand_Acres_LY,QualBrand_Sales_CY, QualBrand_Sales_LY, Reward_Amount, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Acres,RewardBrand_Sales, QualBrand_Acres_CY, QualBrand_Acres_LY,QualBrand_Sales_CY, QualBrand_Sales_LY, Reward_Amount, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Acres) AS RewardBrand_Acres
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(QualBrand_Acres_CY) AS QualBrand_Acres_CY
		,SUM(QualBrand_Acres_LY) AS QualBrand_Acres_LY
		,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
		,SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
		,AVG(Reward_Amount) AS Reward_Amount
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
	
END



