﻿


CREATE PROCEDURE [dbo].[RP2021_Calc_E_SalesSupportLoyaltyBonus] @SEASON INT, @STATEMENT_TYPE VARCHAR(20),  @PROGRAM_CODE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	-- Declare table for inserting data into
	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		RewardBrand_POG_Sales MONEY NOT NULL DEFAULT 0,
		Qualifying_Percentage DECIMAL(9,4) NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(9,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	)


	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL,
		RewardBrand_POG_Sales MONEY NOT NULL DEFAULT 0,
		Qualifying_Percentage DECIMAL(9,4) NOT NULL DEFAULT 0,	
		Reward_Percentage DECIMAL(9,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0		
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)



	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
	-- Insert data into temp table from sales consolidated
	INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode,
		   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Price_CY) * (EI.FieldValue / 100) ELSE SUM(TX.Price_CY) END [RewardBrand_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN RP_Statements RS
		ON ST.StatementCode = RS.StatementCode
		INNER JOIN RPWeb_USER_EI_FieldValues EI
		ON ST.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_InVigor'
	WHERE RS.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE
	GROUP BY ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, EI.FieldValue

	-- Get qualifying percentage from manual inputs
	UPDATE T1
	SET T1.Qualifying_Percentage = (EI.FieldValue / 100)
	FROM @TEMP T1
		INNER JOIN RPWeb_User_EI_FieldValues EI
		ON T1.StatementCode = EI.StatementCode AND EI.ProgramCode = @PROGRAM_CODE AND EI.FieldCode = 'Expected_Forecast_Achievement'
		
	
	-- SET REWARD PERCENTAGE
	UPDATE @TEMP SET Reward_Percentage = 0.0425 WHERE Qualifying_Percentage >= 1.00
	UPDATE @TEMP SET Reward_Percentage = 0.0250 WHERE Qualifying_Percentage >= 0.95 AND Reward_Percentage = 0
	UPDATE @TEMP SET Reward_Percentage = 0.0100 WHERE Qualifying_Percentage >= 0.90 AND Reward_Percentage = 0
		
	END --END FORECAST PROJECTION


	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Sales,RewardBrand_POG_Sales)
		SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode,tx.ProgramCode
			,SUM(tx.Price_CY) AS RewardBrand_Sales
			,SUM(tx.Price_Forecast) AS RewardBrand_POG_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2021_DT_Sales_Consolidated TX	 ON TX.StatementCode=ST.StatementCode
		WHERE rs.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode

	-- Now Determine growth based on the sum of brand sales at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Sales,RewardBrand_POG_Sales)
	SELECT
		CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(RewardBrand_Sales) RewardBrand_Sales,SUM(RewardBrand_POG_Sales) RewardBrand_POG_Sales
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode
	
	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)= dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	-- DETERMINE GROWTH ON TOTAL SALES
	UPDATE @TEMP_TOTALS
	SET Qualifying_Percentage = RewardBrand_Sales / RewardBrand_POG_Sales
	WHERE RewardBrand_POG_Sales > 0

	/*
	Reward Tier on RewardBrand_Sales
	100% +	4.25%
	95% to 99.99%	2.5%
	90% to 94.99%	1%
	*/

	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.0425 WHERE (Qualifying_Percentage = 0 AND RewardBrand_Sales > 0 AND RewardBrand_POG_Sales > 0) OR Qualifying_Percentage + @ROUNDUP_VALUE >= 1
	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.025	WHERE [Reward_Percentage]=0 AND Qualifying_Percentage + @ROUNDUP_VALUE >= 0.95 
	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.01	WHERE [Reward_Percentage]=0 AND Qualifying_Percentage  + @ROUNDUP_VALUE >= 0.90


	-- UPDATE THE GROWTH % AND REWARD % BACK ON THE TEMP TABLE AT LOCATION LEVEL SALES FOR A FAMILY
	UPDATE T1
	SET T1.Qualifying_Percentage = T2.Qualifying_Percentage, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- UPDATE THE GROWTH % AND REWARD % BACK ON THE TEMP TABLE AT LOCATION LEVEL SALES FOR A SINGLE LOCATION
	UPDATE T1
	SET T1.Qualifying_Percentage = T2.Qualifying_Percentage, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0
	
	END --END ACTUAL
	

	-- CALCULATE THE REWARD AS REWARDBRAND_SALES * REWARD_PERCENTAGE
	UPDATE @TEMP
	SET Reward = RewardBrand_Sales * Reward_Percentage
--	WHERE RewardBrand_Sales > 0

	-- Insert from the temp table into the DT table
	DELETE FROM [dbo].[RP2021_DT_Rewards_E_SalesSupportLoyaltyBonus] WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO [dbo].[RP2021_DT_Rewards_E_SalesSupportLoyaltyBonus](StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales,Reward_Percentage, Reward, RewardBrand_POG_Sales)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, Reward_Percentage, Reward, RewardBrand_POG_Sales
	FROM @TEMP

		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode,
		   SUM(RewardBrand_Sales) [RewardBrand_Sales],
		   MAX(Reward_Percentage) [Reward_Percentage],
		   SUM(Reward) [Reward],
		   SUM(RewardBrand_POG_Sales) [RewardBrand_POG_Sales]
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode
END
