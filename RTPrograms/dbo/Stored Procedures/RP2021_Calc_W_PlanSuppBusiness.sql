﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_PlanSuppBusiness] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
/*
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode IN (7920)
	DECLARE @SEASON INT = 2021
	DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2021_W_PLANNING_SUPP_BUSINESS'
*/

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		EXEC [dbo].[RP2021_Calc_W_PlanSuppBusiness_Forecast] @SEASON, @STATEMENT_TYPE, @PROGRAM_CODE, @STATEMENTS;
		RETURN;
	END

	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;
	DECLARE @PLAN_CUT_OFF_DATE DATE  = CAST('2021-02-06' AS DATE)

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		LY_Sales MONEY NOT NULL DEFAULT 0,
		CY_POG_Plan MONEY NOT NULL DEFAULT 0,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	);

	DECLARE @QUALIFYING TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		LY_Sales MONEY NOT NULL DEFAULT 0,
		CY_POG_Plan MONEY NOT NULL DEFAULT 0,
		Growth FLOAT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	);

	DECLARE @SCENARIOS TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,
		[ScenarioCode] [varchar](20) NOT NULL,	
		[ScenarioStatus] [varchar](20) NOT NULL,
		[Cut_Off_Date] [Date] NOT NULL
		PRIMARY KEY CLUSTERED (
		    [StatementCode] ASC,
			[RetailerCode] ASC,
			[ScenarioCode] ASC
		)
	);


	INSERT	INTO @SCENARIOS (StatementCode, RetailerCode, ScenarioCode, ScenarioStatus, Cut_Off_Date)
	SELECT	StatementCode, RetailerCode, ScenarioCode, ScenarioStatus, DateModified
	FROM	[dbo].[TVF_Get_NonIndependent_Scenarios](@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS)

	
	
	INSERT	INTO @TEMP (Statementcode,MarketLetterCode,ProgramCode,RewardBrand,CY_POG_Plan,LY_Sales,RewardBrand_Sales)		
	SELECT	StatementCode, MarketLetterCode,ProgramCode,RewardBrand
			,SUM(POGPlan_CY) AS POGPlan_CY
			,SUM(LY_Sales) AS LY_Sales
			,SUM(IIF(RewardBrand <> 'CENTURION', RewardBrand_Sales, 0)) AS RewardBrand_Sales
	FROM	(

		-- SALES DATA
		SELECT	ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel AS RewardBrand
				,0 AS POGPlan_CY
				,SUM(Price_LY1) AS LY_Sales
				,SUM(Price_CY) AS RewardBrand_Sales
		FROM	@STATEMENTS ST
		INNER	JOIN RP2021_DT_Sales_Consolidated TX	ON ST.StatementCode = TX.StatementCode
		WHERE	ProgramCode = @PROGRAM_CODE AND GroupType = 'ALL_CPP_Brands'
		GROUP	BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel

		UNION	ALL

		-- USE CASE PLAN DATA
		SELECT	ST.StatementCode, 'ML2021_W_CPP' AS MarketLetterCode, @PROGRAM_CODE AS ProgramCode, PR.ChemicalGroup AS RewardBrand
				,SUM(Price_SRP) AS POGPlan_CY
				,0 AS LY_Sales
				,0 AS RewardBrand_Sales
		FROM	@STATEMENTS ST
		INNER	JOIN RP_DT_SCenarioTransactions TX		ON ST.StatementCode = TX.StatementCode
		INNER	JOIN ProductReference PR				ON PR.ProductCode = TX.ProductCode
		WHERE	TX.UC_DateModified IS NOT NULL AND TX.UC_DateModified <= @PLAN_CUT_OFF_DATE
		GROUP	BY ST.StatementCode, PR.ChemicalGroup

		UNION	ALL

		-- PLAN DATA
		SELECT	SCN.StatementCode, 'ML2021_W_CPP' AS MarketLetterCode, @PROGRAM_CODE AS ProgramCode, PR.ChemicalGroup AS RewardBrand
				,SUM(TX.QTY*PRP.SRP) AS POGPlan_CY
				,0 AS LY_Sales
				,0 AS RewardBrand_Sales
		FROM	@SCENARIOS SCN
		INNER	JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
		INNER	JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode						
		INNER	JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'		
		GROUP	BY SCN.StatementCode, PR.ChemicalGroup

	)A
	GROUP	BY StatementCode, MarketLetterCode,ProgramCode,RewardBrand


	-- DELETE ALL BRANDS NOT IN THE MARKET LETTER
	DELETE	FROM @TEMP
	WHERE RewardBrand NOT IN (
		-- LIST OF ALL WEST 
		SELECT DISTINCT B.ChemicalGroup
		FROM RP_Config_Groups G
		INNER	JOIN RP_Config_Groups_Brands B ON G.GroupID = B.GroupID
		WHERE ProgramCode = 'ELIGIBLE_SUMMARY' 
			AND GroupType = 'ELIGIBLE_BRANDS'
			AND MarketLetterCode = 'ML2021_W_CPP'
	)


	INSERT	INTO @QUALIFYING (StatementCode, MarketLetterCode, ProgramCode, CY_POG_Plan, LY_Sales)
	SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(CY_POG_Plan) AS CY_POG_Plan, SUM(LY_Sales) AS LY_Sales
	FROM	@TEMP
	GROUP	BY StatementCode, MarketLetterCode, ProgramCode


	-- CALCULATE GROWTH PERCENTAGE
	UPDATE	@QUALIFYING
	SET		Growth = 
				CASE
					WHEN CY_POG_Plan > 0 AND LY_Sales <= 0 THEN 9.9999
					WHEN CY_POG_Plan <= 0 AND LY_Sales <= 0 THEN 0
					ELSE CY_POG_Plan/LY_Sales
				END

	
	-- CALCULATE REWARD PERCENTAGE
	UPDATE	T1
	SET		Reward_Percentage = IIF(RewardBrand = 'LIBERTY 150', 0.0175, 0.02)
	FROM	@TEMP T1
	INNER	JOIN @QUALIFYING T2 ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode
	WHERE	T2.Growth + @ROUNDING_PERCENT >= 0.90 AND RewardBrand <> 'CENTURION'


	-- CALCULATE REWARD AMOUNT
	UPDATE	@TEMP
	SET		Reward = RewardBrand_Sales*Reward_Percentage
	WHERE	Reward_Percentage > 0 


	-- SET THE REWARD AMOUNT TO 0 ONLY IF THE TOTAL REWARD IS LESS THAN 0 (not at a line level but overall statement)
	-- CPP BRANDS
	UPDATE	T1
	SET		Reward  = 0 
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP T1
		WHERE	RewardBrand <> 'LIBERTY 150'
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0
		AND T1.RewardBrand <> 'LIBERTY 150'


	-- SET THE REWARD AMOUNT TO 0 ONLY IF THE TOTAL REWARD IS LESS THAN 0 (not at a line level but overall statement)
	-- CPP BRANDS
	UPDATE	T1
	SET		Reward  = 0 
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP T1
		WHERE	RewardBrand = 'LIBERTY 150'
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0
		AND T1.RewardBrand = 'LIBERTY 150'


	-- DELETE OLD VALUES FROM PERMANENT TABLE
	DELETE FROM RP2021_DT_Rewards_W_PlanningTheBusiness_Actual WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	-- INSERT TEMP VALUES INTO PERMANENT TABLE
	INSERT	INTO RP2021_DT_Rewards_W_PlanningTheBusiness_Actual (StatementCode,MarketLetterCode,ProgramCode,RewardBrand,LY_Sales,CY_POG_Plan,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT	StatementCode,MarketLetterCode,ProgramCode,RewardBrand,LY_Sales,CY_POG_Plan,RewardBrand_Sales,Reward_Percentage,Reward
	FROM	@TEMP 

END
