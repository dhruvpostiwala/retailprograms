﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_CLL] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON


	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CPP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'

	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[RetailerCode] VARCHAR(20) NOT NULL,
		[FarmCode] VARCHAR(20) NOT NULL,
		[Brand] VARCHAR(100) NOT NULL,
		[Brand_Acres] DECIMAL(18,4) NOT NULL,
		[Brand_Sales] MONEY NOT NULL
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC,
			[Brand] ASC
		)
	)

	DECLARE @FINAL_RESULTS TABLE (
		[StatementCode] INT NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[RetailerCode] VARCHAR(20) NOT NULL,
		[FarmCode] VARCHAR(20) NOT NULL,
		[CLL_Acres] DECIMAL(18,4) NOT NULL,
		[CLL_Sales] MONEY NOT NULL,
		[CLL_Herb_Acres] DECIMAL(18,4) NOT NULL,
		[CLL_Herb_Sales] MONEY NOT NULL,
		[Matched_Acres] DECIMAL(18,4) NOT NULL,
		[Matched_Sales] MONEY NOT NULL DEFAULT 0,
		[Reward_Percentage] DECIMAL(18,4) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC
		)
	)
	
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION','REWARD PLANNER')
	BEGIN
		-- NEW: RRT-2068 - original code was not matching herb acres with CLL at the family level

		-- Get data from sales consolidated
		-- Difference here is I am replacing TX.RetailerCode with S.RetailerCode - I want all rows to be a single R-code
		INSERT INTO @TEMP(StatementCode, Level5Code, RetailerCode, FarmCode, Brand, Brand_Acres, Brand_Sales)
		SELECT ST.StatementCode, TX.Level5Code, S.RetailerCode, 'FXXXXX' [FarmCode], PR.ChemicalGroup,
				SUM(TX.Acres_CY * (ISNULL(EI.FieldValue, 100) / 100) ) [Brand_Acres],
				SUM(TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ) [Brand_Sales]
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
			INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
			INNER JOIN RPWeb_User_EI_FieldValues EI ON ST.StatementCode = EI.StatementCode AND EI.MarketLetterCode=@CPP_ML_CODE AND EI.FieldCode = 'Radius_Percentage_CPP'
			INNER JOIN RPWeb_Statements S ON ST.StatementCode = S.StatementCode
		WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.Level5Code,  S.RetailerCode, PR.ChemicalGroup

		-- Match acres
		-- Difference here is we are producing 1 total row instead of X total rows where X = number of locations in the family
		-- To calculate matched sales, if matched herb acres is less than total herb acres, we take the % of matched vs total and multiple by total herb sales
		INSERT INTO @FINAL_RESULTS(StatementCode, Level5Code, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Matched_Sales, Reward_Percentage)
		SELECT StatementCode, Level5Code, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, 
			CASE WHEN CLL_Acres > CLL_Herb_Acres THEN CLL_Herb_Acres ELSE CLL_Acres END AS 'Matched_Acres',
			ISNULL(CASE WHEN CLL_Acres > CLL_Herb_Acres THEN CLL_Herb_Sales ELSE CLL_Herb_Sales * (CLL_Acres / NULLIF(CLL_Herb_Acres,0)) END,0) AS 'Matches_Sales',
			CASE Level5Code WHEN 'D0000117' THEN 0.0275 ELSE 0.03 END  AS 'Reward_Percentage'
		FROM (
			SELECT
				StatementCode, Level5Code, RetailerCode, FarmCode,
				SUM(CASE Brand WHEN 'CLEARFIELD LENTILS' THEN Brand_Acres ELSE 0 END) [CLL_Acres],
				SUM(CASE Brand WHEN 'CLEARFIELD LENTILS' THEN Brand_Sales ELSE 0 END) [CLL_Sales],
				SUM(CASE WHEN Brand <> 'CLEARFIELD LENTILS' THEN Brand_Acres ELSE 0 END) [CLL_Herb_Acres],
				SUM(CASE WHEN Brand <> 'CLEARFIELD LENTILS' THEN Brand_Sales ELSE 0 END) [CLL_Herb_Sales]
			FROM @TEMP
			GROUP BY StatementCode, Level5Code, RetailerCode, FarmCode
		) Data_SUM

		UPDATE @FINAL_RESULTS SET [Reward] = [Matched_Sales] * [Reward_Percentage]				

		
		-- Copy data from temporary table to permanent table
		--FORECAST PROJECTION TABLE
		DELETE FROM RP2022_DT_Rewards_W_ClearfieldLentils WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

		INSERT INTO RP2022_DT_Rewards_W_ClearfieldLentils(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Reward_Percentage, Reward)
		SELECT StatementCode, @CPP_ML_CODE as MarketLetterCode, @PROGRAM_CODE as ProgramCode, RetailerCode, FarmCode, CLL_Acres, CLL_Sales, CLL_Herb_Acres, CLL_Herb_Sales, Matched_Acres, Reward_Percentage, Reward
		FROM @FINAL_RESULTS

		RETURN

	END -- END OF FORECAST , PROJECTION

END




