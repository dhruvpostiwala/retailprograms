﻿

CREATE PROCEDURE [dbo].[RP2021_Exceptions_W_GetRewardsData] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
		SET NOCOUNT ON;		
		/*
			FOR WEST REGION:
			EXCEPTIONS TO BE CREATED FROM UNLOCKED STATEMENTS AND APPROVED FROM UNLOCKED STATEMENTS ONLY.
			STATEMENTCODE TO BE PASSED IN SHOULD BE OF UNLOCKED STATEMENTS
		*/

		DECLARE @TEMP AS [dbo].[UDT_RP_EXCEPTIONS_DATA]

		DECLARE @REWARD_MARGINS TABLE (
			ProgramCode [Varchar](50) NOT NULL,
			Level5Code  [Varchar](20) NOT NULL,
			Margin_A DECIMAL(6,4) NOT NULL DEFAULT 0,
			Margin_B DECIMAL(6,4) NULL ,
			PRIMARY KEY CLUSTERED 
			(
				ProgramCode ASC,
				Level5Code ASC,
				Margin_A ASC
			)WITH (IGNORE_DUP_KEY = OFF)
		)

		INSERT INTO @REWARD_MARGINS(ProgramCode, Level5Code, Margin_A, Margin_B)
		VALUES('RP2021_W_BRAND_SPEC_SUPPORT','D000001',0.03,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D000001',0.04,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D000001',0.045,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000117',0.03,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000117',0.04,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000117',0.045,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000137',0.02,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000137',0.03,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000137',0.04,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000137',0.045,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000107',0.01,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000107',0.02,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000107',0.03,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000107',0.035,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000244',0.01,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000244',0.02,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000244',0.03,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D0000244',0.035,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D520062427',0.01,NULL)
		,('RP2021_W_BRAND_SPEC_SUPPORT','D520062427',0.005,NULL)
		,('RP2021_W_CUSTOM_SEED_TREATING_REWARD','D000001',0.025,NULL)
		,('RP2021_W_CUSTOM_SEED_TREATING_REWARD','D000001',0.0175,NULL)
		,('RP2021_W_CUSTOM_SEED_TREATING_REWARD','D000001',0.01,NULL)
		,('RP2021_W_GROWTH_BONUS','D000001',0.03,NULL)
		,('RP2021_W_GROWTH_BONUS','D000001',0.05,NULL)
		,('RP2021_W_GROWTH_BONUS','D000001',0.0575,NULL)
		,('RP2021_W_GROWTH_BONUS','D000001',0.0625,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000117',0.01,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000117',0.02,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000117',0.0375,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000117',0.045,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000137',0.03,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000137',0.05,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000137',0.0575,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000137',0.0625,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000244',0.03,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000244',0.05,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000244',0.0575,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000244',0.0625,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000107',0.03,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000107',0.05,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000107',0.0575,NULL)
		,('RP2021_W_GROWTH_BONUS','D0000107',0.0625,NULL)
		,('RP2021_W_GROWTH_BONUS','D520062427',0.03,NULL)
		,('RP2021_W_GROWTH_BONUS','D520062427',0.05,NULL)
		,('RP2021_W_GROWTH_BONUS','D520062427',0.0575,NULL)
		,('RP2021_W_GROWTH_BONUS','D520062427',0.0625,NULL)
		,('RP2021_W_INV_PERFORMANCE','D520062427',0.085,NULL)
		,('RP2021_W_INV_PERFORMANCE','D000001',0.01,NULL)
		,('RP2021_W_INV_PERFORMANCE','D000001',0.015,NULL)
		,('RP2021_W_INV_PERFORMANCE','D000001',0.02,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000117',0.0075,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000117',0.01,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000117',0.0145,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000137',0.02,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000137',0.025,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000137',0.03,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000244',0.02,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000244',0.025,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000244',0.03,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000107',0.02,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000107',0.025,NULL)
		,('RP2021_W_INV_PERFORMANCE','D0000107',0.03,NULL)
		,('RP2021_W_LOYALTY_BONUS','All',0.03,0.03)
		,('RP2021_W_LOYALTY_BONUS','All',0.035,0.035)
		,('RP2021_W_LOYALTY_BONUS','All',0.0575,0.1)
		,('RP2021_W_LOYALTY_BONUS','All',0.0625,0.1225)

		INSERT into @TEMP(StatementCode,Level5Code,ProgramCode,RewardCode,MarketLetterCode,next_margin_A,next_margin_B,reward_margins_a,reward_margins_b)
		SELECT RS.StatementCode,RS.Level5Code,P.ProgramCode,P.RewardCode,E.MarketLetterCode,0,0,'[0]','[0]'
		FROM @STATEMENTS ST
			INNER JOIN RPWeb_Statements RS			ON RS.StatementCode=ST.StatementCode
			INNER JOIN RPWeb_ML_ELG_Programs E		ON E.StatementCode = ST.StatementCode
			INNER JOIN RP_Config_programs P 		ON P.ProgramCode = E.ProgramCode
		WHERE P.Exception_Enabled=1 AND RS.VersionType = 'Unlocked'
					
		--SupplySales only using on CCP MarketLetter
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2021_W_INV' AND RewardCode = 'SUPPLY_SALES_W'
		
		-- InVigor Liberty Loyaly bonus  is applicable on two market letters
		-- But we only need to treat as single record, hence delete it for ML2021_W_CPP
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2021_W_CPP' AND RewardCode = 'INV_LLB_W'
		
		UPDATE @TEMP SET exception_type = 'TIER' WHERE RewardCode IN ('BSS_W','CST_W','EBR_W','GROWTH_W','INV_PER_W','LOYALTY_W')
		UPDATE @TEMP SET exception_type = 'NON-TIER' WHERE exception_type IS NULL
				
		-- NON-TIER (QUALIFIER) REWARDS
		-- InVigor and Liberty Loyalty
		-- INVIGOR => SALES_A, next margin 0.02
		-- LIBERTY => SALES_B, next margin 0.05

		UPDATE T1
		SET cy_sales_A = T2.[cy_sales_A]  ,next_margin_A = 0.02 				
			,cy_sales_B = T2.[cy_sales_B] ,next_margin_B = 0.05 
		FROM @TEMP T1
			INNER JOIN (
				SELECT StatementCode, ProgramCode
					,MAX(IIF(MarketLetterCode = 'ML2021_W_INV' , RewardBrand_Sales, 0)) AS [cy_sales_A]						
					,MAX(IIF(MarketLetterCode = 'ML2021_W_CPP' , RewardBrand_Sales, 0)) AS [cy_sales_B]						
				FROM [dbo].[RP2021Web_Rewards_W_INV_LLB] 					
					WHERE Bonus_Percentage = 0
						AND RewardBrand_Sales > 0
				GROUP BY StatementCode, ProgramCode
			) T2
			ON T2.StatementCode = T1.StatementCode and T2.ProgramCode = T1.ProgramCode
								
		--Logistics Support Reward
		--@REWARDCODE='LOG_SUP_W'		
		UPDATE T1
		SET cy_sales_A = T2.cy_sales_a				
			,next_margin_A = 0.01
		FROM @TEMP T1
			INNER JOIN (
				SELECT StatementCode, ProgramCode, SUM(Segment_Reward_Sales) AS cy_sales_a
				FROM [RP2021Web_Rewards_W_LogisticsSupport]
				WHERE Segment_Reward_Percentage=0 
				GROUP BY StatementCode, ProgramCode
			) T2
			ON T2.StatementCode = T1.StatementCode and T2.ProgramCode = T1.ProgramCode
			
			
		UPDATE T1
		SET T1.cy_sales_A = NTIER.CYSales
			,next_margin_A = NTIER.next_margin_A
		FROM @TEMP T1
			INNER JOIN (										
				--Segment Selling Bonus
				SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.02 AS next_margin_A
				FROM [dbo].[RP2021Web_Rewards_W_SegmentSelling]				
				GROUP BY StatementCode, ProgramCode
				
				UNION ALL
				
				--InVigor Innovation Bonus
				SELECT StatementCode, ProgramCode, SUM(RewardBrand_Sales) as 'CYSales', 0.02 AS next_margin_A
				FROM [dbo].[RP2021Web_Rewards_W_INV_INNOV]				
				GROUP BY StatementCode, ProgramCode
			) NTIER	
			ON NTIER.StatementCode = T1.StatementCode and T1.ProgramCode = NTIER.ProgramCode
		WHERE T1.exception_type = 'NON-TIER' 
		


		/********************* TIERED REWARDS *****************************/			   	
		
		UPDATE T1
			SET T1.cy_sales_A = TIER.CYSales_A
				,T1.cy_sales_B = TIER.CYSales_B
				,T1.current_margin_A = TIER.CurrentMargin_A
				,T1.current_margin_B = TIER.CurrentMargin_B
				,T1.current_reward_A = TIER.CurrentReward_A
				,T1.current_reward_B = TIER.CurrentReward_B				
		FROM @TEMP T1		
		INNER JOIN (
			SELECT StatementCode, ProgramCode				
				,SUM(RewardBrand_Sales) AS 'CYSales_A',MAX(Reward_Percentage) AS 'CurrentMargin_A',SUM(Reward) AS 'CurrentReward_A'
				,0 AS 'CYSales_B',0 AS 'CurrentMargin_B',0 AS 'CurrentReward_B'				
			FROM [dbo].[RP2021Web_Rewards_W_BrandSpecificSupport] 
			GROUP BY StatementCode, ProgramCode

			UNION ALL

			SELECT StatementCode, ProgramCode				
				,SUM(Planned_POG_Sales) AS 'CYSales_A' ,MAX(RewardBrand_Percentage) AS 'CurrentMargin_A', SUM(Reward) AS 'CurrentReward_A' 
				,0 AS 'CYSales_B' ,0 AS 'CurrentMargin_B' ,0 AS 'CurrentReward_B'				
		    FROM [dbo].[RP2021Web_Rewards_W_CustomSeed] 
			GROUP BY StatementCode, ProgramCode

			UNION ALL 

			SELECT StatementCode, ProgramCode
				,SUM(Support_Reward_Sales) as 'CYSales_A',MAX(Support_Reward_Percentage) AS 'CurrentMargin_A',SUM(Reward) as 'CurrentReward_A'
				,0 AS 'CYSales_B',0 AS 'CurrentMargin_B',0 AS 'CurrentReward_B'								
		    FROM [dbo].[RP2021Web_Rewards_W_EFF_BONUS] 
			GROUP BY StatementCode, ProgramCode

			UNION ALL

			SELECT StatementCode, ProgramCode
				,SUM(QualSales_CY) as 'CYSales_A',MAX(Reward_Percentage) AS 'CurrentMargin_A',SUM(Reward) AS 'CurrentReward_A'
				,0 AS 'CYSales_B',0 AS 'CurrentMargin_B',0 AS 'CurrentReward_B'
		    FROM [dbo].[RP2021Web_Rewards_W_GrowthBonus] 
			GROUP BY StatementCode, ProgramCode

			UNION ALL

			SELECT StatementCode, ProgramCode
				,SUM(RewardBrand_Sales) as 'CYSales_A',MAX(Reward_Percentage) AS 'CurrentMargin_A',SUM(Reward) as 'CurrentReward_A'
				,0 AS 'CYSales_B' ,0 AS 'CurrentMargin_B',0 AS 'CurrentReward_B'												
		    FROM [dbo].[RP2021Web_Rewards_W_INV_PERF] 
			GROUP BY StatementCode, ProgramCode

			UNION ALL

			SELECT StatementCode, ProgramCode
				,SUM(SegmentA_Reward_Sales) as 'CYSales_A',MAX(SegmentA_Reward_Percentage) AS 'CurrentMargin_A',SUM(SegmentA_Reward) as 'CurrentReward_A'
				,SUM(SegmentB_Reward_Sales) AS 'CYSales_B',MAX(SegmentB_Reward_Percentage) AS 'CurrentMargin_B',SUM(SegmentB_Reward) as 'CurrentReward_B'										
		   FROM [dbo].[RP2021Web_Rewards_W_LoyaltyBonus]		   
		   GROUP BY StatementCode, ProgramCode
		) TIER	
		ON TIER.StatementCode = T1.StatementCode and TIER.ProgramCode = T1.ProgramCode
		WHERE T1.exception_type = 'TIER'
		

		/***** SOME ADJUSTMENTS *****/
		UPDATE @TEMP
		SET Level5Code='All'
		WHERE ProgramCode='RP2021_W_LOYALTY_BONUS'

		UPDATE @TEMP 		SET Current_Margin_A = 0 		WHERE cy_sales_A <= 0
		UPDATE @TEMP 		SET Current_Margin_B = 0 		WHERE cy_sales_B <= 0


		/************ SEGEMENT A NEXT REWARD MARGIN TO BE APPLIED ******************/		
		UPDATE TGT
		SET Next_Margin_A = SRC.Margin_A		
		FROM @TEMP TGT
		 INNER JOIN (
			SELECT T1.StatementCode, T1.ProgramCode, T2.Margin_A
				,RANK() OVER(PARTITION BY T1.StatementCode, T1.ProgramCode ORDER BY T2.Margin_A ) AS [Rank]				
			FROM @TEMP T1
				INNER JOIN @REWARD_MARGINS T2
				ON T2.ProgramCode=T1.ProgramCode AND T2.Level5Code = T1.Level5Code AND T2.Margin_A > T1.Current_Margin_A
			WHERE (T2.Level5Code=T1.Level5Code OR T2.Level5Code = 'All') 
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.[Rank]=1

		/************ SEGEMENT A REWARD MARGINS ******************/		
		UPDATE TGT
		SET Reward_Margins_A = '[' + ISNULL(SRC.Reward_Margins,'0') + ']'				
		FROM @TEMP TGT
		 INNER JOIN (
			SELECT T1.StatementCode, T1.ProgramCode, STRING_AGG(T2.Margin_A, ',') AS Reward_Margins
			FROM @TEMP T1
				INNER JOIN @REWARD_MARGINS T2
				ON T2.ProgramCode=T1.ProgramCode AND T2.Level5Code = T1.Level5Code AND T2.Margin_A > T1.Current_Margin_A			
			GROUP BY T1.StatementCode, T1.ProgramCode
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode = TGT.ProgramCode 
		
		/************ SEGEMENT B NEXT REWARD MARGIN TO BE APPLIED ******************/
		UPDATE TGT
		SET Next_Margin_B = SRC.Margin_B						
		FROM @TEMP TGT
		 INNER JOIN (
			SELECT T1.StatementCode, T1.ProgramCode, T2.Margin_B
				,RANK() OVER(PARTITION BY T1.StatementCode, T1.ProgramCode ORDER BY T2.Margin_B ) AS [Rank]				
			FROM @TEMP T1
				INNER JOIN @REWARD_MARGINS T2
				ON T2.ProgramCode=T1.ProgramCode AND T2.Level5Code = T1.Level5Code AND T2.Margin_B > T1.Current_Margin_B
			WHERE T2.Level5Code=T1.Level5Code AND T2.Margin_B IS NOT NULL
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.[Rank]=1

		/************ SEGEMENT B REWARD MARGINS ******************/		
		UPDATE TGT
		SET Reward_Margins_B = '[' + ISNULL(SRC.Reward_Margins,'0') + ']'
		FROM @TEMP TGT
		 INNER JOIN (
			SELECT T1.StatementCode, T1.ProgramCode, STRING_AGG(T2.Margin_B, ',') AS Reward_Margins
			FROM @TEMP T1
				INNER JOIN @REWARD_MARGINS T2
				ON T2.ProgramCode=T1.ProgramCode AND T2.Level5Code = T1.Level5Code AND T2.Margin_B > T1.Current_Margin_B
			WHERE T2.Margin_B IS NOT NULL 
			GROUP BY T1.StatementCode, T1.ProgramCode
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode = TGT.ProgramCode 
		
FINAL:		
	/*
		IF @OUTPUT_TYPE='JSON'
			SELECT ISNULL(
				(
					SELECT exception_type ,RewardCode as reward_code
						,cy_sales_a,cy_sales_b					
						,current_reward_a,current_reward_b	
						,current_margin_a,current_margin_b
						,next_margin_a,next_margin_b									
						,JSON_QUERY(reward_margins_a) AS reward_margins_a
						,JSON_QUERY(reward_margins_b) as reward_margins_b
					FROM @TEMP 						
					FOR JSON PATH
				) 
			,'[]')[json_data];
		ELSE
		*/
		SELECT * FROM @TEMP

END

