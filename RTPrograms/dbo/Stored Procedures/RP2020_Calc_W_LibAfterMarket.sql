﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_LibAfterMarket]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

		-- DO NOT CACULATE THIS REWARD ON FORECAST AND PROJECTION STATEMENTS. THIS PROGRAM WAS NOT MENTIONED UNTIL CALCULTIONS FOR ACTUAL STATEMENTS 
		-- WE DO NOT WANT TO IMPACT TOTALS ON FORECAST AND PROJECTIONS STATEMENT WHERE THIS REWARD VALUE WAS NEITHER DISPLAYED NOR INCLUDED  EARLIER.
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN
	
		DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		RewardBrand_Sales [numeric](18, 2) NOT NULL DEFAULT 0,
		[Quantity] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC
	)
		)

	/*
	LIBERTY AFTER MARKET PAYMENT
	ML2020_W_CCP Market Letter

	Reward Brands: Liberty 150 Brand

	Qualifying Level: Level 2. For standalone will be at level 1.
	Calculating Level: Level 2. For standalone will be at level 1.

	Reward Opportunity:
	Retails earn $1.50/13.5L Liberty jug equivalents

	Calculation:
	$1.50 * 2020 Eligible Liberty jug equivalents
	*/
	

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity)
	SELECT ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode
	,MAX(tx.GroupLabel) as RewardBrand
	,SUM(IIF(tx.GroupLabel='LIBERTY 150',tx.Price_CY/10,0)) AS RewardBrand_Sales     -- acres/10 1 jug
	,SUM(IIF(tx.GroupLabel='LIBERTY 150',tx.Acres_CY/10,0)) AS Quantity     -- acres/10 1 jug
		FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupLabel ='LIBERTY 150'
	GROUP BY ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode

	UPDATE @TEMP SET Reward = Quantity * 1.5   WHERE Quantity > 0

	DELETE FROM RP2020_DT_Rewards_W_Lib_AFM WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_Lib_AFM(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity, Reward)
	SELECT  StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity, Reward
	FROM @TEMP

END

