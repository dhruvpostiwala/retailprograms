﻿


CREATE PROCEDURE [dbo].[RP_User_MarginTool_UpdateData] @SEASON AS INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	--'[{"statementcode":-92,"amount":800,"percentage":0.07}]'

	DECLARE @TEMP_TABLE TABLE( 
		[StatementCode] int NOT NULL,
		[Locked_Statementcode] INT NOT NULL DEFAULT 0,		
		[Percentage] float NOT NULL DEFAULT 0,
		[Amount] money NOT NULL DEFAULT 0,
		[DateCreated] datetime NOT NULL DEFAULT GETDATE(),
		[DateModified] datetime NOT NULL DEFAULT GETDATE(),
		PRIMARY KEY CLUSTERED (
			STATEMENTCODE ASC
		) 
	)
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	BEGIN TRY

	INSERT INTO @TEMP_TABLE (StatementCode, Amount, Percentage)
	SELECT StatementCode, Amount, Percentage
	FROM OPENJSON(@JSON)
	WITH (
		StatementCode INT N'$.statementcode',
		Amount money N'$.amount',
		[Percentage] money N'$.percentage'
	) a
	
	INSERT INTO @STATEMENTS(Statementcode)
	SELECT DISTINCT Statementcode FROM @TEMP_TABLE

	UPDATE UL
	SET Locked_Statementcode=ISNULL(LCK.Statementcode,0)
	FROM @TEMP_TABLE UL
		INNER JOIN RPWeb_Statements LCK
		ON LCK.SRC_Statementcode=UL.Statementcode AND LCK.VersionType='Locked' AND LCK.[Status]='Active' AND LCK.ChequeRunName='OCT RECON'
		
	/*
	MERGE RPWeb_Est_MarginTopUp mt 
	USING (
			/*-- UNLOCKED STATEMENTCODES
			SELECT Statementcode, Amount, Percentage, DateCreated, DateModified,  NULL as SRC_Statementcode
			FROM @TEMP_TABLE
			
				UNION */
			
			-- LOCKED STATEMETNT CODES
			SELECT Locked_Statementcode AS Statementcode, Amount, Percentage, DateCreated, DateModified,  Statementcode as SRC_Statementcode
			FROM @TEMP_TABLE
			WHERE Locked_Statementcode < 0
		)	t 
		ON mt.statementcode = t.statementcode 
	WHEN MATCHED THEN UPDATE SET
		mt.Amount = 0
		,mt.[Percentage] = isnull(t.[Percentage],0)
		,mt.DateCreated = coalesce(mt.DateCreated,t.DateCreated)
		,mt.DateModified = t.DateModified		
	WHEN NOT MATCHED THEN 
		INSERT (StatementCode, Amount, Percentage, DateCreated,DateModified)
		VALUES (t.StatementCode, t.Amount, t.Percentage, t.DateCreated,t.DateModified)
	OUTPUT t.[SRC_StatementCode],t.[SRC_StatementCode],'Margin Top Up Amount','Field','Percentage',deleted.Percentage*100,inserted.Percentage*100
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue]);


	-- NO NEED TO CAPTURE EDIT HISTORY FOR LOCKED STATEMENTS
	DELETE FROM @EDIT_HISTORY WHERE [Statementcode] < 0
	*/

	MERGE RPWeb_Est_MarginTopUp mt 
	USING (
			SELECT Locked_Statementcode, StatementCode, Amount, [Percentage], DateCreated, DateModified
			FROM @TEMP_TABLE
		)	t 
		ON mt.SRC_Statementcode = t.statementcode 
	WHEN MATCHED THEN 
	UPDATE SET
		mt.[Percentage] = isnull(t.[Percentage],0)
		,mt.Amount = 0		
		,mt.DateModified = t.DateModified		
	WHEN NOT MATCHED THEN 
		INSERT (StatementCode, SRC_Statementcode, [Percentage], Amount, DateCreated, DateModified)
		VALUES (t.Locked_StatementCode, t.StatementCode, t.[Percentage], t.Amount, t.DateCreated,t.DateModified)
	OUTPUT t.[StatementCode],t.[StatementCode],'Margin Top Up Percentage','Field','Percentage',deleted.[Percentage] * 100 ,inserted.[Percentage] * 100
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue]);
	   	 
	EXEC RP_GenerateEditHistory 'Statement', 'Margin Top Up Percentage Update', @EDIT_HISTORY 
		
	EXEC [dbo].[RP_RefreshEstimatesRewardsSummary] @SEASON, @STATEMENTS 
	   
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() AS Error_Message
		RETURN;	
	END CATCH

	SELECT 'Success' AS [Status], '' AS Error_Message

END





