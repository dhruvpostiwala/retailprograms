﻿

CREATE PROCEDURE [dbo].[RP2020_RefreshMaximizeRewardMessages] @STATEMENT_TYPE VARCHAR(50),  @STATEMENTS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE(
		StatementCode INT NOT NULL,
		MarketLetterCode Varchar(50) NOT NULL,
		ProgramCode Varchar(50) NOT NULL	
	)	

	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode
	FROM @STATEMENTS ST			
		INNER JOIN RPWeb_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
			   		
	DELETE FROM RPWeb_MaximizeRewardMessages	WHERE [StatementCode] IN (SELECT StatementCode From @STATEMENTS)
		
	INSERT INTO RPWeb_MaximizeRewardMessages(StatementCode,MarketLetterCode,ProgramCode,MaximizeRewardMessage)

	-- InVigor & Liberty Loyalty Bonus 	 -- ML : ML2020_W_CCP and ML2020_W_INV
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 'Achieve a > 1 Ratio between your InVigor and Liberty to qualify for 2% Reward' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		LEFT JOIN RP2020Web_Rewards_W_INV_LLB Rew		
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.ProgramCode='RP2020_W_INV_LIB_LOY_BONUS'  AND ISNULL(Rew.Reward_Percentage,0)=0
	
		UNION
			
	-- BRAND SPECIFIC SUPPORT	-- Increase your POG Plan for qualifying brands by $XX,XXX to move to the next rebate level of X% and earn additional rewards of $XX,XXX
	SELECT StatementCode, MarketLetterCode, ProgramCode
		,'Increase your POG Projection for qualifying brands by $' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to move to the next rebate level of ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '% and earn additional rewards of $' + convert(varchar(50), CAST(Additional_Reward as money), -1) AS [MaximizeRewardMessage]
	FROM (
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,rew.Required_Sales - rew.QualBrand_Sales_CY as [Increase_Sales]
			,rew.Next_Percentage		
			,(rew.[Required_Sales]  * rew.[Next_Percentage]) - rew.[Old_Reward] AS [Additional_Reward]
		FROM @TEMP T1
			INNER JOIN (
				SELECT *
					,CASE 
						WHEN [Next_Percentage] = 0.045 THEN [TwoYear_Avg] * 1.10
						WHEN [Next_Percentage] = 0.04  THEN [TwoYear_Avg] * 1.06
						WHEN [Next_Percentage] = 0.035 THEN [TwoYear_Avg] * 1.03
						WHEN [Next_Percentage] = 0.03  THEN [TwoYear_Avg]
						WHEN [Next_Percentage] = 0.02  THEN [TwoYear_Avg] * .95
												 	   ELSE [TwoYear_Avg] * 0.90
					END  AS [Required_Sales]
				FROM (
					Select StatementCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Reward AS [Old_Reward]
						,(QualBrand_Sales_LY1+QualBrand_Sales_LY2)/2  AS [TwoYear_Avg]					
						,CASE 
							WHEN round(Reward_Percentage,2) = 0.04  THEN 0.045 
							WHEN round(Reward_Percentage,2) = 0.035 THEN 0.04
							WHEN round(Reward_Percentage,2) = 0.03  THEN 0.035 
							WHEN round(Reward_Percentage,2) = 0.02  THEN 0.03 
							WHEN round(Reward_Percentage,2) = 0.01  THEN 0.02 
														   ELSE 0.01
						END [Next_Percentage]						
					FROM RP2020Web_Rewards_W_BrandSpecificSupport 
					WHERE (Reward_Percentage=0 OR Reward_Percentage < 0.045)	
				) D
			) Rew		
			ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT'  
	) FinalData

		UNION

	-- INVIGOR PERFORMANCE	
	SELECT StatementCode, MarketLetterCode, ProgramCode
		,'If you increase you POG projected sales of InVigor by $' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' you will increase your reward margin to ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%  on InVigor POG$.'  AS [MaximizeRewardMessage]
	FROM (
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,(rew.Required_Qty - rew.RewardBrand_Qty) * 10 as [Increase_Sales]
			,rew.Next_Percentage
			,(rew.Required_Qty * 10 * rew.Next_Percentage) - rew.Old_Reward AS [Additional_Reward]			
		FROM @TEMP T1
			INNER JOIN (
				SELECT *
					,CASE 
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.04  THEN Sales_Target
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.035 THEN Sales_Target * 0.975
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.03  THEN Sales_Target * 0.95
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.02  THEN Sales_Target * 0.925
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.015 THEN Sales_Target * 0.90
						WHEN [MarketLetterCode]='ML2020_W_INV' AND Next_Percentage = 0.01  THEN Sales_Target * 0.85

						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.04  THEN Sales_Target
						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.03  THEN Sales_Target * 0.975
						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.02  THEN Sales_Target * 0.95
						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.015 THEN Sales_Target * 0.925
						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.01  THEN Sales_Target * 0.90
						WHEN [MarketLetterCode]='ML2020_W_CCP' AND Next_Percentage = 0.005 THEN Sales_Target * 0.85							
																						   ELSE 0
					END  AS [Required_Qty]
				FROM (
					Select StatementCode, MarketLetterCode, ProgramCode,  RewardBrand_Qty, Sales_Target, Reward_Percentage, Reward as [Old_Reward]										
						,CASE 
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0.035 THEN 0.04
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0.03  THEN 0.035
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0.02  THEN 0.03
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0.015 THEN 0.02
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0.01  THEN 0.015
							WHEN [MarketLetterCode]='ML2020_W_INV' AND round(Reward_Percentage,2) = 0     THEN 0.01

							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0.03  THEN 0.04
							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0.02  THEN 0.03
							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0.015 THEN 0.02
							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0.01  THEN 0.015
							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0.005 THEN 0.01
							WHEN [MarketLetterCode]='ML2020_W_CCP' AND round(Reward_Percentage,2) = 0     THEN 0.005							
																								 ELSE 0
						END [Next_Percentage]						
					FROM RP2020Web_Rewards_W_INV_PERF 
					WHERE Sales_Target > 0 AND RewardBrand_Sales > 0 AND [Reward_Percentage] BETWEEN 0 AND 0.035
				) D
				WHERE Next_Percentage > 0
			) Rew		
			ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_INV_PERFORMANCE'  
	) FinalData

		UNION

	-- Efficiency Rebate -- INDEPENDENTS
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode
		,'To qualify for the Efficiency Rebate, increase your total projected BASF sales by $' + convert(varchar(50), CAST(2000000 - ISNULL(All_QualSales_CY,0) as money), -1) + ' to achieve an additional 2.5% rebate' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		LEFT JOIN (
			Select StatementCode, MarketLetterCode, ProgramCode, All_QualSales_CY
			From RP2020Web_Rewards_W_EFF_REBATE_QUAL
			WHERE All_QualSales_CY >=  2000000-(2000000 * .10) AND All_QualSales_CY < 2000000
		) Rew		
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.ProgramCode='RP2020_W_EFF_REBATE'   


		UNION

	-- Coop RETAIL BONUS [Canola Crop Protection]
	-- RP2020Web_Rewards_W_RetailBonus(Statementcode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1, Growth_Sales_LY2, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward)	
	-- "If growth is between 95-99% then display “Increase your forecast of Cereal & Pulse or Seed treatment and Inoculant crop protection by XX dollars to maximize your Retail Bonus reward”"


	/*
	Growth in 2020 relative to the 2-year average
	 (Growth_Sales_CY / ((Growth_Sales_LY1 + Growth_Sales_LY2)/2))
	Reward on Centurion
				Above 2M	Below 2M
	0 to 99.9%	1%			1%
	100%+		3%			2%

	Reward on Liberty
				Above 2M	Below 2M
	0 to 99.9%	3%			2%
	100%+		6%			4%

	Lib_Reward_Percentage, Cent_Reward_Percentage

	*/
		
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode 
		,'Increase your forecast of Cereal & Pulse or Seed treatment and Inoculant crop protection by $' + convert(varchar(50), CAST(((Growth_Sales_LY1 + Growth_Sales_LY2)/2) - Growth_Sales_CY  as money), -1) + ' to maximize your Retail Bonus reward' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		INNER JOIN (
			SELECT *
				,(Growth_Sales_CY / NULLIF(((Growth_Sales_LY1 + Growth_Sales_LY2)/2),0)) AS [Growth]
				,((Growth_Sales_LY1 + Growth_Sales_LY2)/2) - Growth_Sales_CY AS [Increase_Sales]
				,CASE WHEN Growth_Sales_CY >= 2000000  THEN 0.03 ELSE 0.02 END AS Cent_Reward_Next_Percentage
				,CASE WHEN Growth_Sales_CY >= 2000000  THEN 0.06 ELSE 0.04 END AS Lib_Reward_Next_Percentage
			FROM (
				SELECT RB.StatementCode, RB.MarketLetterCode, RB.ProgramCode
					,IIF(@STATEMENT_TYPE IN ('FORECAST','PROJECTION') , CASE WHEN  ISNULL(EI.FieldValue,1)=1 THEN 2000000 ELSE 0 END , RB.Growth_Sales_CY) AS Growth_Sales_CY
					,Growth_Sales_LY1 
					,Growth_Sales_LY2
					,Cent_Reward_Percentage
					,Lib_Reward_Percentage					
				FROM RP2020Web_Rewards_W_RetailBonus  RB
					LEFT JOIN RPWeb_User_EI_FieldValues EI
					ON EI.StatementCode=RB.StatementCode AND EI.MarketLetterCode=RB.MarketLetterCode AND EI.ProgramCode=RB.ProgramCode AND EI.FieldCode='Bonus_Qualified'				
			) D
			WHERE (Growth_Sales_CY >= 2000000 AND  (Cent_Reward_Percentage < 0.03 OR Lib_Reward_Percentage < 0.06))
				OR (Growth_Sales_CY < 2000000 AND  (Cent_Reward_Percentage < 0.02 OR Lib_Reward_Percentage < 0.04))
		) Rew		
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.ProgramCode='RP2020_W_RETAIL_BONUS_PAYMENT'  AND (Rew.[Growth] >= 0.95 AND Rew.[Growth] < 1)


		UNION

	/*
	-- Custom Seed
	-- RP2020_W_CUSTOM_SEED_TREATING_REWARD
	-- INSURE CEREAL  , INSURE PULSE 8.16326514724293  Acres Per Litre

	-- RP2020Web_Rewards_W_CustomSeed(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Acres, RewardBrand_Sales, RewardBrand_CustApp, Reward_Percentage, Reward)
	*/
	
	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode 		
		,'To qualify for the Insure Cereal Custom Seed Treating Reward, increase your litres of Insure Cereal & Insure Cereal FX4 by ' + CONVERT(varchar(20) , CAST(ROUND(450 - ISNULL(Litres,0),2) as Money), -1) + ' to achieve a 22% rebate' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		LEFT JOIN (
			Select StatementCode, ProgramCode, MarketLetterCode, RewardBrand_Acres/ 8.16326514724293 as Litres				
			From RP2020Web_Rewards_W_CustomSeed 
			Where RewardBrand='INSURE CEREAL' AND RewardBrand_Acres < 3674
		)REW
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode 
	WHERE T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD'

		UNION


	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode 		
		,'To qualify for the Insure Pulse Custom Seed Treating Reward, increase your litres of Insure Pulse by ' + CONVERT(varchar(20) , CAST(ROUND(360 - ISNULL(Litres,0),2) as Money), -1) + ' to achieve a 22% rebate' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		LEFT JOIN (
			Select StatementCode, ProgramCode, MarketLetterCode, RewardBrand_Acres/ 8.16326514724293 as Litres			
			From RP2020Web_Rewards_W_CustomSeed 
			Where RewardBrand='INSURE PULSE' AND RewardBrand_Acres < 2939
		)REW
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode 
	WHERE T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD'

		UNION

	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode 		
		,'To qualify for the Nodulator PRO Custom Seed Treating Reward, increase your POG$ of Nodulator PRO by $' + convert(varchar(20), CAST( 20000 - ISNULL(RewardBrand_Sales,0)  as money), -1)    + ' to achieve an 8% rebate' AS [MaximizeRewardMessage]
	FROM @TEMP T1
		LEFT JOIN ( 
			SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales
			FROM RP2020Web_Rewards_W_CustomSeed  
			WHERE RewardBrand='NODULATOR PRO' AND RewardBrand_Sales < 20000
		) REW
		ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode 
	WHERE  T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD' 


	UNION

	--Demarey March 27 2020
	--Added maximise message for
	--Porfolio Achievement Reward

	SELECT StatementCode, MarketLetterCode, ProgramCode
		,CASE 
			WHEN MarketLetterCode ='ML2020_W_CCP' THEN 
			'Increase your POG projection for the following brands: Cotegra, Lance, Lance AG & Facet L by ' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%' 
			WHEN MarketLetterCode ='ML2020_W_ST_INC' THEN
			'Increase your POG projection for the following brands: Insure Cereal, Insure Pulse & Nodulator by ' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%' 
			ELSE
			'Increase your POG projection for the following: Cereal, Pulse & Soybean brands by  ' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%' 
			END AS [MaximizeRewardMessage]
	
	FROM (
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,(rew.Required_Sales - rew.RewardBrand_Sales) as [Increase_Sales]
			,rew.Next_Percentage
			,(rew.Required_Sales * rew.Next_Percentage) - rew.Old_Reward AS [Additional_Reward]			
		FROM @TEMP T1
			INNER JOIN (
				SELECT *
					,
					CASE WHEN Level5Code ='D0000117' THEN  --coop get different reward structure
						CASE 
						WHEN [Next_Percentage] = 0.06 THEN QualBrand_Sales_LY * 1.15
						WHEN [Next_Percentage] = 0.055 THEN QualBrand_Sales_LY * 1.10
						WHEN [Next_Percentage] = 0.05  THEN QualBrand_Sales_LY * 0.95
						WHEN [Next_Percentage] = 0.04 THEN QualBrand_Sales_LY * 0.90
												      ELSE QualBrand_Sales_LY * 0.85
						END
					ELSE
					CASE 
						WHEN [Next_Percentage] = 0.07 THEN QualBrand_Sales_LY * 1.15
						WHEN [Next_Percentage] = 0.065 THEN QualBrand_Sales_LY * 1.10
						WHEN [Next_Percentage] = 0.06  THEN QualBrand_Sales_LY * 0.95
						WHEN [Next_Percentage] = 0.05 THEN QualBrand_Sales_LY * 0.90
												      ELSE QualBrand_Sales_LY * 0.85
					END 
					END AS [Required_Sales]
				FROM (
					Select P.StatementCode, RP.Level5Code, MarketLetterCode, ProgramCode, RewardBrand_Sales, QualBrand_Sales_CY,QualBrand_Sales_LY, Reward_Percentage, Reward as [Old_Reward]										
						,
						CASE WHEN RP.Level5Code ='D0000117' THEN  --coop get different reward structure
								CASE 
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB')  AND round(Reward_Percentage,2) = 0.055  THEN 0.06
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.05  THEN 0.055
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.04 THEN 0.05
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.03  THEN 0.04
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.02  THEN 0.03						
										 ELSE 0
							END
						ELSE
							CASE 
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB')  AND round(Reward_Percentage,2) = 0.065  THEN 0.07
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.06  THEN 0.065
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.05 THEN 0.06
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.04  THEN 0.05
								WHEN [MarketLetterCode] IN ('ML2020_W_ST_INC','ML2020_W_CCP', 'ML2020_W_CER_PUL_SB') AND round(Reward_Percentage,2) = 0.03  THEN 0.04						
										 ELSE 0
							END

						END AS [Next_Percentage]						
					FROM RP2020Web_Rewards_W_PortfolioAchievement P
						INNER JOIN RPWeb_Statements RS on Rs.StatementCode = P.StatementCode
						INNER JOIN RetailerProfile RP  on RP.RetailerCode=RS.RetailerCode
					WHERE  RewardBrand_Sales > 0 AND [Reward_Percentage] BETWEEN 0 AND 0.065
				) D
				WHERE Next_Percentage > 0
			) Rew		
			ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_PORFOLIO_ACHIEVEMENT' 
	) FinalData


	UNION
	--Demarey March 30 2020
	--Added maximise message for
	--Soybean/Pulse Planning & Support Reward


	SELECT StatementCode, MarketLetterCode, ProgramCode
		,CASE 
			WHEN MarketLetterCode ='ML2020_W_CER_PUL_SB' THEN 
			'Increase your POG projection for the following brands: Odyssey, Solo & Viper by $' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%' 
			END AS [MaximizeRewardMessage]
	FROM (
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,(rew.Required_Sales - rew.RewardBrand_Sales) as [Increase_Sales]
			,rew.Next_Percentage
			,(rew.Required_Sales * rew.Next_Percentage) - rew.Old_Reward AS [Additional_Reward]			
		FROM @TEMP T1
			INNER JOIN (
				SELECT *,
					CASE 
						WHEN [Next_Percentage] = 0.04 THEN QualBrand_Sales_LY * 1
						ELSE  QualBrand_Sales_LY * 0.95
					END AS [Required_Sales]
				FROM (
					Select StatementCode, MarketLetterCode, ProgramCode, PartA_Reward_Sales as RewardBrand_Sales, PartA_QualSales_CY as QualBrand_Sales_CY,PartA_QualSales_LY as QualBrand_Sales_LY, PartA_Reward_Percentage as Reward_Percentage, PartA_Reward as [Old_Reward]										
						,
				
							CASE
								WHEN [MarketLetterCode] = 'ML2020_W_CER_PUL_SB' AND PartA_Reward_Percentage = 0.02  THEN 0.04						
										 ELSE 0.02
						END AS [Next_Percentage]						
					FROM RP2020Web_Rewards_W_SoybeanPulsePlanning 
					WHERE  PartA_Reward > 0 AND [PartA_Reward_Percentage] BETWEEN 0 AND 0.03
				) D
				WHERE Next_Percentage > 0
			) Rew		
			ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD' 
	) FinalData

	UNION

	--Demarey March 30 2020
	--Added maximise message for
	-- AG REWARD BONUS Support Reward
	--Displayed at all times
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 
		'Earn up to 6% when you generate & close a lead as well as sell  3 separate product segments to the same grower.'  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
				Select StatementCode, MarketLetterCode, ProgramCode from
					RP2020Web_Rewards_W_AgRrewardsBonus
			) AG		
			ON AG.StatementCode=T1.StatementCode AND AG.ProgramCode=T1.ProgramCode AND AG.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_AGREWARDS_BONUS' 


		UNION

	--Demarey March 30 2020
	--Added maximise message for
	-- Liberty/ Centurion / Facet L Tank Mix Bonus
	--Displayed once there is pog

		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 
		'Earn up to $6.00/jug on Liberty when you sell Liberty, Centurion & Facet L to the same grower.'  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
				Select StatementCode, MarketLetterCode, ProgramCode,Liberty_Acres,Centurion_Acres,Facet_L_Acres from
					RP2020Web_Rewards_W_TankMixBonus
					WHERE Liberty_Acres > 0 OR Centurion_Acres > 0 OR Facet_L_Acres > 0
			) Tank		
			ON Tank.StatementCode=T1.StatementCode AND Tank.ProgramCode=T1.ProgramCode AND Tank.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_TANK_MIX_BONUS'
		

		UNION

		
	--Demarey APRIL 1 2020
	--Added maximise message for
	--Clearfield Lentils Support Reward: Show this if they have input the specific brands into their POG plan.

			
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode,
				CASE 
				WHEN (LentilAcres - MatchAcres) > 0  THEN
				'Increase your reward by increasing one or more of your Clearfield Lentil Herbicide brands by ' + convert(varchar(50), CAST((LentilAcres - MatchAcres) as money), -1) +  ' acres combined to earn 3% reward on matched acres. Clearfield Lentil Herbicide brands: Odyssey® NXT herbicide, Odyssey Ultra NXT, Solo® ADV herbicide, Solo Ultra.'
				ELSE
				''
		end AS [MaximizeRewardMessage]	
		
		FROM @TEMP T1
			INNER JOIN (
					Select  StatementCode, MarketLetterCode, ProgramCode,
							RewardBrand
							 Herbicide_Brand_Acres,Herbicide_Match_Sales,
							 SUM(Herbicide_Lentil_Acres) OVER (PARTITION BY StatementCode, MarketLetterCode) AS LentilAcres,
							 SUM(Herbicide_Match_Acres ) OVER (PARTITION BY StatementCode, MarketLetterCode) AS MatchAcres
							FROM
							RP2020Web_Rewards_W_ClearfieldLentils
					) CLL
		
		ON CLL.StatementCode=T1.StatementCode AND CLL.ProgramCode=T1.ProgramCode AND CLL.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_W_CLL_SUPPORT' 


		----------------------------------------------------------
		/*
		EAST

		*/
		----------------------------------------------------------

		UNION
		--Demarey March 31 2020
		-- Supply of Sales Reward: Show at all times not necessarily anything to trigger this message - it should be present at all times as a constant reminder to users.

		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 
		'Submit sales to BASF on a monthly basis from January to September to be eligible for 0.75% reward margin.'  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
				Select StatementCode, MarketLetterCode, ProgramCode,RewardBrand from
				      RP2020Web_Rewards_E_SupplySales 				
			) SS		
			ON SS.StatementCode=T1.StatementCode AND SS.ProgramCode=T1.ProgramCode AND SS.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_SUPPLY_SALES' 

		UNION

		--Demarey March 31 2020
		-- Show this messaging once the retail has input Pursuit into their POG Plan

		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 
		'Earn $75.00 on all cases of Pursuit when 2020 POG sales of Pursuit are equal or greater than 2019 POG sales of Pursuit.'  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
				Select StatementCode, MarketLetterCode, ProgramCode,Pursuit_Qty_CY from
				     RP2020Web_Rewards_E_PursuitSupport
					 WHERE Pursuit_Qty_CY > 0
			) PR		
			ON PR.StatementCode=T1.StatementCode AND PR.ProgramCode=T1.ProgramCode AND PR.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_PURSUIT_REWARD'
		
		UNION

		--Demarey March 31 2020
		--Custom APP Reward
		-- Show at all times not necessarily anything to trigger this message - it should be present at all times as a constant reminder to users.

		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, 
		'Earn $1.00 and/or $0.50 acre by entering the number of acres/brand expected to Custom Apply in "Edit Inputs".'  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
				Select StatementCode , MarketLetterCode, ProgramCode, RewardBrand from
				     RP2020Web_Rewards_E_CustomGroundApplication
			) CA		
			ON CA.StatementCode=T1.StatementCode AND CA.ProgramCode=T1.ProgramCode AND CA.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_CUSTOM_GROUND_APP_REWARD' 

		UNION

		--Demarey March 31 2020
		--Priaxor/Headline Reward
		-- Show this messaging once the retail has input Priaxor &/or Headline into their POG Plan.

		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode, Priaxor_Acres_Message + Headline_Acres_Message  AS [MaximizeRewardMessage]					
		FROM @TEMP T1
			INNER JOIN (
			Select  StatementCode, MarketLetterCode, ProgramCode,
				CASE 
				WHEN Priaxor_Acres > 0 AND Priaxor_Acres < 160 THEN
					'Increase 2020 planned purchase of Priaxor by '+ convert(varchar(50), CAST( 160 - Priaxor_Acres as money), -1) +' Acres to receive $1.00/acre reward.' ELSE
					''
				END AS Priaxor_Acres_Message, 
				CASE WHEN Headline_Acres > 0 AND Headline_Acres < 80 THEN
				'Increase 2020 planned purchase of Headline by '+ convert(varchar(50), CAST( 80 - Headline_Acres as money), -1) +' Acres to receive $1.50/acre reward.' ELSE ''
				END AS Headline_Acres_Message
					FROM
				     RP2020Web_Rewards_E_PriaxorHeadlineSupport
					 WHERE Priaxor_Acres > 0 OR Headline_Acres > 0
			) PH		
			ON PH.StatementCode=T1.StatementCode AND PH.ProgramCode=T1.ProgramCode AND PH.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_PRIAXOR_HEADLINE_REWARD' 


		UNION

		--Demarey March 31 2020
		--Portfolio Support Reward
		-- Show this message at all times as well. If a retail is not qualifying for the reward 
		--originally then this would appear notifying them that if they increase their total POG$ 
		--by $X they will be rewarded the first tier of the reward. If they have already qualified 
		--for the reward then show them how increasing POG$ further will give them additional reward %


		SELECT StatementCode, MarketLetterCode, ProgramCode
		,	'To qualify for the next reward tier increase your planned POG$ by ' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive ' +  convert(varchar(6), CAST(Next_Percentage * 100 as money), -1) + '%' 
			 AS [MaximizeRewardMessage]
	
	FROM (
		SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,(rew.Required_Sales - rew.RewardBrand_Sales) as [Increase_Sales]
			,rew.Next_Percentage
			,(rew.Required_Sales * rew.Next_Percentage) - rew.Old_Reward AS [Additional_Reward]			
		FROM @TEMP T1
			INNER JOIN (
				SELECT *,
					CASE WHEN RewardBrand_Sales < 100000 THEN 
						 100000
					ELSE 
						CASE WHEN [Next_Percentage] = 0.06  THEN IIF(QualBrand_Sales_2YA = 0,100000,QualBrand_Sales_2YA)  * 1
							 WHEN [Next_Percentage] = 0.04 THEN IIF(QualBrand_Sales_2YA = 0,100000,QualBrand_Sales_2YA)  * 0.95
														ELSE IIF(QualBrand_Sales_2YA = 0,100000,QualBrand_Sales_2YA) * 0.90
						END 
					END AS [Required_Sales]
				FROM (
					Select StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1 + QualBrand_Sales_LY2 as 'QualBrand_Sales_2YA',Reward_Percentage, Reward as [Old_Reward]										
						,
						CASE 
								WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.04  THEN 0.06
								WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND round(Reward_Percentage,2) = 0.03  THEN 0.04						
										 ELSE 0.03
						END AS [Next_Percentage]						
					FROM RP2020Web_Rewards_E_PortfolioSupport 
					WHERE  RewardBrand_Sales > 0 AND [Reward_Percentage] BETWEEN 0 AND 0.05
				) Portfolio
				WHERE Next_Percentage > 0
			) Rew		
			ON Rew.STatementCode=T1.StatementCode AND Rew.ProgramCode=T1.ProgramCode AND Rew.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_PORFOLIO_SUPPORT' 
	) FinalData


	UNION

		--Demarey April 1 2020
		--Row Crop Fungicide Support Reward
		-- Show this messaging once the retail has input at least 1 unit of one of the qualifying brands: Caramba, Cotegra, Headline AMP, Headline, Priaxor & Twinline into their POG Plan

		SELECT StatementCode, MarketLetterCode, ProgramCode,
				CASE 
				WHEN Combined_QualBrand_Acres_CY < 2000  THEN
					'Increase your combined 2020 planned purchases of Caramba, Cotegra, Headline AMP, Headline, Priaxor & Twinline by '+ convert(varchar(50), CAST( 2000 - Combined_QualBrand_Acres_CY as money), -1) +' acres to qualify for the reward.'
					ELSE
					'Increase your combined 2020 planned purchases of Caramba, Cotegra, Headline AMP, Headline, Priaxor & Twinline by $' + convert(varchar(50), CAST(Increase_Sales as money), -1) + ' to receive $' +  convert(varchar(6), CAST(Next_Reward_Amount as money), -1) + ' acre on 2020 POG$ of qualifying herbicide brands.' 			
		end AS [MaximizeRewardMessage]	
		
		FROM (	SELECT T1.StatementCode, T1.MarketLetterCode, T1.ProgramCode			
			,SUM((rew.Required_Sales - rew.RewardBrand_Acres)) OVER (PARTITION BY rew.StatementCode, rew.MarketLetterCode) as [Increase_Sales]
			,rew.Next_Reward_Amount
			,(rew.Required_Sales * rew.Next_Reward_Amount) - rew.Old_Reward AS [Additional_Reward],
			Combined_QualBrand_Acres_CY, RewardBrand	

		FROM @TEMP T1
			INNER JOIN (
			SELECT *,
					CASE WHEN Combined_QualBrand_Acres_CY < 2000 THEN --minimum required
						 2000
					ELSE 
						CASE WHEN [Next_Reward_Amount] = 1.75  THEN IIF(QualBrand_Acres_LY = 0,2000,QualBrand_Acres_LY)  * 1.10
							 WHEN [Next_Reward_Amount] = 1.25 THEN IIF(QualBrand_Acres_LY = 0,2000,QualBrand_Acres_LY)   * 1
						END
					END AS [Required_Sales]
	
			FROM (

					Select  StatementCode, MarketLetterCode, ProgramCode,
							RewardBrand_Acres, Reward_Amount, RewardBrand,
							CASE 
										WHEN [MarketLetterCode] = 'ML2020_E_CCP' AND Reward_Amount = 1.25  THEN 1.75						
												 ELSE 1.25
							END AS [Next_Reward_Amount]
							, Reward as Old_Reward ,
							QualBrand_Acres_LY,
							SUM (QualBrand_Acres_CY) Over(Partition by StatementCode,ProgramCode) As Combined_QualBrand_Acres_CY
							FROM
							RP2020Web_Rewards_E_RowCropFungSupport
							WHERE QualBrand_Acres_CY > 0 and Reward_Amount < 1.75
					) RC
						WHERE Next_Reward_Amount > 0
			)REW
		
		ON REW.StatementCode=T1.StatementCode AND REW.ProgramCode=T1.ProgramCode AND REW.MarketLetterCode=T1.MarketLetterCode
		WHERE T1.ProgramCode='RP2020_E_ROW_CROP_FUNG_REWARD' 
		)FinalData

END

