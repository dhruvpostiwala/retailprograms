﻿
CREATE PROCEDURE [dbo].[RP2022_Calc_W_PlanSuppBusiness] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		QualSales_CY MONEY NOT NULL,
		QualSales_LY MONEY NOT NULL,
		Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		CPP_Reward_Sales MONEY NOT NULL,
		CPP_Reward_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		CPP_Reward MONEY NOT NULL DEFAULT 0,
		Cent_Reward_Sales MONEY NOT NULL,
		Cent_Reward_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		Cent_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	);

	-- Variables for the reward percentages for CPP and Centurion, respectively
	DECLARE @CPP_Reward_Percentage DECIMAL(6,4) = 0.03;
	DECLARE @Cent_Reward_Percentage DECIMAL(6,4) = 0.025;

	-- Get the data from sales consolidated
	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, QualSales_CY, QualSales_LY, CPP_Reward_Sales, Cent_Reward_Sales)
	SELECT ST.StatementCode, TX.MarketLetterCode, @PROGRAM_CODE AS ProgramCode,
		SUM(CASE
				WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN
					TX.Price_CY * CASE WHEN TX.GroupLabel NOT IN ('CENTURION') THEN (ISNULL(EI1.FieldValue, 100) / 100) ELSE (ISNULL(EI2.FieldValue, 100) / 100) END
				ELSE
					TX.Price_CY
			END) [QualSales_CY],
		SUM(TX.Price_LY1) [QualSales_LY],
		SUM(IIF(TX.GroupLabel NOT IN ('CENTURION'), TX.Price_CY, 0)) [CPP_Reward_Sales],
		SUM(IIF(TX.GroupLabel = 'CENTURION', TX.Price_CY, 0)) [Cent_Reward_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		LEFT JOIN RPWeb_User_EI_FieldValues EI1
		ON EI1.StatementCode=ST.StatementCode AND EI1.MarketLetterCode=TX.MarketLetterCode  AND EI1.FieldCode='Radius_Percentage_CPP'
		LEFT JOIN RPWeb_User_EI_FieldValues EI2
		ON EI2.StatementCode=ST.StatementCode AND EI2.MarketLetterCode=TX.MarketLetterCode  AND EI2.FieldCode='Radius_Percentage_Centurion'
	WHERE TX.ProgramCode = 'ELIGIBLE_SUMMARY'
	GROUP BY ST.StatementCode, TX.ProgramCode, TX.MarketLetterCode

	-- Get the growth percentage for each statement
	UPDATE @TEMP
		SET Growth_Percentage = IIF(QualSales_LY > 0, QualSales_CY / QualSales_LY, IIF(QualSales_CY > 0, 9.99, 0))

	-- Set the reward percentage and reward amount for CPP and Liberty
	UPDATE @TEMP
		SET CPP_Reward_Percentage = @CPP_Reward_Percentage,
			CPP_Reward = CPP_Reward_Sales * @CPP_Reward_Percentage,
			Cent_Reward_Percentage = @Cent_Reward_Percentage,
			Cent_Reward = Cent_Reward_Sales * @Cent_Reward_Percentage
	WHERE Growth_Percentage >= 0.90

	-- RC-4369: Set the overall reward
	UPDATE @TEMP SET Reward = CPP_Reward + Cent_Reward

	-- Copy values from the temporary table into the permanent table
	DELETE FROM RP2022_DT_Rewards_W_PlanningTheBusiness WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2022_DT_Rewards_W_PlanningTheBusiness(StatementCode, MarketLetterCode, ProgramCode, QualSales_CY, QualSales_LY, Growth_Percentage,
														CPP_Reward_Sales, CPP_Reward_Percentage, CPP_Reward, Cent_Reward_Sales, Cent_Reward_Percentage,
														Cent_Reward, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, QualSales_CY, QualSales_LY, Growth_Percentage, CPP_Reward_Sales, CPP_Reward_Percentage,
	CPP_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward
	FROM @TEMP
END
