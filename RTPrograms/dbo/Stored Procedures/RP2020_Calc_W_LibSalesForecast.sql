﻿



CREATE PROCEDURE [dbo].[RP2020_Calc_W_LibSalesForecast]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

		-- DO NOT CACULATE THIS REWARD ON FORECAST AND PROJECTION STATEMETNS. AS THIS PROGRAM WAS NOT MENTIONED UNTIL CALCULTIONS FOR ACTUAL STATEMENTS 
		-- WE DO NOT WANT TO IMPACT TOTALS ON FORECAST AND PROJECTIONS STATEMENT WHERE THIS REWARD VALUE IS NOT DISPLAYED ON REWARDS PAGE
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN

	
		DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		RewardBrand_Sales [numeric](18, 2) NOT NULL,
		Reward MONEY NOT NULL DEFAULT 0,
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC
	)
		)

	/*
	SALES FOPECAST PAYMENT
	ML2020_W_LIBERTY_200 Market Letter

	Reward Brands: Liberty 200 Brand

	Qualifying Level: Level 2. For standalone will be at level 1.
	Calculating Level: Level 2. For standalone will be at level 1.

	No Forecast was developed for Liberty 200 ML. 
	Therefore, we are going to grant 4% 
	all retails that sold Liberty 200 in 2020 season. 
	The calculations will be off of 2020 Eligible Liberty 200 POG$.
	
	4% x 2020 Eligible Liberty 200 POG$ = Sales Forecast Reward

	*/
	
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
	SELECT ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode	
		,MAX(tx.GroupLabel) as RewardBrand
		,SUM(IIF(tx.GroupLabel='LIBERTY 200',tx.Price_CY,0)) AS RewardBrand_Sales  
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupLabel ='LIBERTY 200'
	GROUP BY ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode	

	DECLARE @REWARD_PERC FLOAT = 0.04;

	--UPDATE REWARD DOLLARS
	UPDATE @TEMP 
	SET Reward = RewardBrand_Sales * @REWARD_PERC   
	WHERE RewardBrand_Sales > 0

	DELETE FROM RP2020_DT_Rewards_W_Lib_SalesForecast WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_Lib_SalesForecast(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward
	FROM @TEMP

END

