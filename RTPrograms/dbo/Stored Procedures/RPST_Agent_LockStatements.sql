﻿
CREATE PROCEDURE [dbo].[RPST_Agent_LockStatements] @SEASON INT,  @USERNAME VARCHAR(150),  @STATEMENTCODES VARCHAR(MAX), @CHEQUERUNID INT
AS
BEGIN
	-- UPDATE CODE TO ACCOMODATE LOCKING OF MULTIPLE STATEMENTS AT A TIME
	-- @CHEQUERUNNAME VARCHAR(50),

	/* 
		HANDLES LOCKING MULTIPL STATEMENTS AT A TIME
	*/
	
	SET NOCOUNT ON;
	
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;
	DECLARE @STATEMENTS_TO_LOCK UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @PROCESS_OU_TASKS BIT = 0;
	DECLARE @PROCESS_HO_TASKS BIT = 0;
	DECLARE @SQL_COMMAND NVARCHAR(MAX);

	DECLARE @OU_STATEMENTS TABLE(
		StatementCode INT,
		LockedStatementCode INT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)
	
	DECLARE @HO_STATEMENTS TABLE(
		StatementCode INT,		
		LockedStatementCode INT DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)

	DECLARE @VALIDATIONS TABLE(
		Statementcode INT,
		RetailerInfo VARCHAR(MAX)
	)
		
	DECLARE @RETAILER_CODES AS VARCHAR(MAX)

	BEGIN TRY
	
	-- OBTAIN A UNIQUE IDENTIFIER FOR THIS JOB
	DECLARE @JOB_ID uniqueidentifier=NEWID();

	INSERT INTO @STATEMENTS(STATEMENTCODE)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')
	   	
	-- WE NEED TO GET ALL STATEMENTS CODES BELONGING TO A FAMILY
	INSERT INTO @STATEMENTS_TO_LOCK(StatementCode, StatementLevel, Retailercode, DataSetCode, DataSetCode_L5 ,Summary)
	SELECT StatementCode,StatementLevel,Retailercode,DataSetCode, DataSetCode_l5, Summary
	FROM TVF_Get_StatementCodesToRefresh(@STATEMENTS) 	

	INSERT INTO @OU_STATEMENTS(StatementCode)
	SELECT DISTINCT DataSetCode FROM @STATEMENTS_TO_LOCK WHERE DataSetCode > 0
	
	INSERT INTO @HO_STATEMENTS(StatementCode)
	SELECT DataSetCode_L5 
	FROM @STATEMENTS_TO_LOCK
	WHERE DataSetCode_L5 > 0 
	GROUP BY DataSetCode_L5
	   	   
	/**************************** VALIDATIONS BEGIN ****************************/
	
	/********************* VALIDATION #1: STATEMENTS SHOULD NOT HAVE ACTIVE LOCKED STATEMENT *********************/
	DELETE FROM @VALIDATIONS

	INSERT INTO @VALIDATIONS(StatementCode,RetailerInfo)
	SELECT LCK.StatementCode, STRING_AGG(RP.RetailerCode + ' - ' + ISNULL(RP.RetailerName,''), ',<br>') 
	FROM RPWeb_Statements LCK
		INNER JOIN @STATEMENTS_TO_LOCK UL	ON LCK.SRC_StatementCode=UL.StatementCode 
		INNER JOIN RetailerProfile RP		ON RP.Retailercode=LCK.RetailerCode
	WHERE LCK.VersionType='Locked' AND LCK.[Status]='Active'
	GROUP BY LCK.Statementcode

	IF EXISTS(SELECT * FROM @VALIDATIONS)
	BEGIN
		SELECT @RETAILER_CODES=RetailerInfo FROM @VALIDATIONS
		SELECT 'failed' as [Status], 'One or more statements you are trying to lock has an active locked statement.<br>' + @RETAILER_CODES AS [error_message]		
		RETURN
	END

	/********************* VALIDATION #2: STATEMENTS SHOULD NOT HAVE ACTIVE PAYMENT (INCLUDE LEVEL 5 SUMMARY STATEMENT CODE) *********************/
	DELETE FROM @VALIDATIONS

	INSERT INTO @VALIDATIONS(StatementCode,RetailerInfo)
	SELECT ARC.StatementCode,  STRING_AGG(RP.RetailerCode + ' - ' + ISNULL(RP.RetailerName,''), ',<br>') 
	FROM RPWeb_Statements ARC
		INNER JOIN (
			SELECT [Statementcode] FROM @STATEMENTS_TO_LOCK 
				UNION
			SELECT [Statementcode] FROM @HO_STATEMENTS
		) UL		
		ON ARC.SRC_StatementCode=UL.StatementCode 
		INNER JOIN (
			SELECT Statementcode 
			FROM RPWeb_Cheques 
			WHERE [VersionType]='Current' AND [ChequeStatus] IN ('New','Ready To Print','Print','QA Completed')
		) CHQ
		ON CHQ.Statementcode=ARC.Statementcode
		INNER JOIN RetailerProfile RP		ON RP.Retailercode=ARC.RetailerCode
	WHERE ARC.VersionType='Archive'
	GROUP BY ARC.Statementcode

	
	IF EXISTS(SELECT * FROM @VALIDATIONS)
	BEGIN
		SELECT @RETAILER_CODES=RetailerInfo FROM @VALIDATIONS
		SELECT 'failed' as [Status], 'Payment (Cheque/EFT) is in progress for one or more statements.<br>' + @RETAILER_CODES AS [error_message]		
		RETURN
	END


	/********************* VALIDATION #3: MAKE SURE LEVEL 1 AND LEVEL 2 STATEMENTS THAT WILL BE PAID AT LEVEL 5 HAS AN ACTIVE LOCKED LEVEL 5 SUMAMRY STATEMENT *********************/	
	IF EXISTS(SELECT * FROM @HO_STATEMENTS)
	BEGIN
		UPDATE HO
		SET LockedStatementCode=LK.StatementCode
		FROM @HO_STATEMENTS HO
			INNER JOIN RPWeb_Statements LK	ON LK.SRC_StatementCode=HO.StatementCode
		WHERE LK.Season=@Season AND LK.[Status]='Active' AND LK.VersionType='Locked' AND LK.StatementLevel='LEVEL 5'

		IF EXISTS(SELECT * FROM @HO_STATEMENTS WHERE LockedStatementCode=0)
		BEGIN		
			SELECT @RETAILER_CODES=STRING_AGG(ISNULL(RP.RetailerName,'') + ' (' + S.RetailerCode + ')'   , ',<br>' )
			FROM @HO_STATEMENTS HO
				INNER JOIN RPWeb_Statements S	ON S.Statementcode=HO.Statementcode				
				LEFT JOIN RetailerProfile RP	ON RP.Retailercode=S.Retailercode
			WHERE HO.LockedStatementCode=0
			
			SELECT 'failed' as [Status], 'There is NO active LEVEL 5 Locked Statement for one or more of Level 1 and Level 2 Statements. Please lock following Level 5 statement(s)<br>' + @RETAILER_CODES AS [error_message]		
			RETURN
		END
	END
	   
	/**************************** VALIDATIONS END ****************************/
	
	--DATENAME(MONTH,ST.LOCKED_DATE) + ' Reward')
	-- LETS LOCK SUMMARY STATEMENT
	IF EXISTS(SELECT * FROM @STATEMENTS_TO_LOCK WHERE Summary='Yes')
	BEGIN
		INSERT INTO RPWeb_Statements(Locked_Date,Lock_Job_ID, VersionType, StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, SRC_StatementCode, ChequeRunName, ChequeRunID) 	
		SELECT GetDate(), @JOB_ID,  'Locked', StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, StatementCode
			--,@CHEQUERUNNAME AS ChequeRunName 
			,'' AS ChequeRunName 
			,@CHEQUERUNID AS ChequeRunID
		FROM RPWeb_Statements ST
		WHERE Statementcode IN (
				SELECT DISTINCT UL.Statementcode 
				FROM @STATEMENTS_TO_LOCK UL
					INNER JOIN RPWeb_StatementInformation  STI	ON STI.Statementcode=UL.StatementCode
				WHERE UL.Summary='Yes' AND  STI.FieldName='Status' AND STI.FieldValue='Open'
			) 
		
		UPDATE HO
		SET LockedStatementCode=LK.StatementCode
		FROM @HO_STATEMENTS HO
			INNER JOIN RPWeb_Statements LK	ON LK.SRC_StatementCode=HO.StatementCode
		WHERE LK.Lock_Job_ID=@JOB_ID AND LK.Season=@Season AND LK.[Status]='Active' AND LK.VersionType='Locked' AND LK.StatementLevel='LEVEL 5'

		DELETE @EDIT_HISTORY

		UPDATE STI
		SET [FieldValue]='Kenna Summary Approval'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA process - Lock','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI
			INNER JOIN @STATEMENTS_TO_LOCK TGT		ON TGT.StatementCode=STI.StatementCode			
		WHERE STI.[FieldName]='Status' AND TGT.Summary='Yes'
				
		EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY


		/* 
			TAKE SNAP SHOT OF EXISTING CHEQUES / EFT
			CHEQUES (THIS NEEDS TO BE DONE SEPERATELY SINCE WE ARE INSERTING THEM AS HISTORICAL AND BECAUSE CHEQUES ARE DIRECTLY LINKED TO THE ARCHIVED STATEMENT) 
		*/
		INSERT INTO RPWeb_Cheques([StatementCode],[VersionType],[ChequeDocCode],[RetailerCode],[PayableToName],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeStatus],[SendTo],[ChequeAmount],[AmtRequested],[AmtReceived],[AmtPaid],[PDFJobID],[PDFDocCode])
		SELECT STL.[StatementCode],'Historical',CHQ.[ChequeDocCode],CHQ.[RetailerCode],CHQ.[PayableToName],CHQ.[ChequeType],CHQ.[ChequeNumber],CHQ.[ChequeDate],CHQ.[ChequeStatus],CHQ.[SendTo],CHQ.[ChequeAmount],CHQ.[AmtRequested],CHQ.[AmtReceived],CHQ.[AmtPaid],CHQ.[PDFJobID],CHQ.[PDFDocCode]
		FROM (
			SELECT StatementCode, SRC_StatementCode 
			FROM RPWeb_Statements 
			WHERE Season=@SEASON AND [VersionType]='LOCKED' AND [Status]='Active' AND [Lock_Job_ID]=@JOB_ID 
		) STL
			INNER JOIN (
				SELECT Statementcode, SRC_Statementcode 
				FROM RPWeb_Statements 
				WHERE Season=@SEASON AND VersionType='Archive' AND [Status]='Active'
			) ARC		
			ON ARC.SRC_StatementCode=STL.SRC_StatementCode
			INNER JOIN RPWeb_Cheques CHQ
			ON CHQ.Statementcode=ARC.Statementcode AND CHQ.VersionType='Current'

		GOTO FINAL
	END	
	
	IF EXISTS(SELECT * FROM @HO_STATEMENTS)
		SET @PROCESS_HO_TASKS=1

	IF EXISTS(SELECT * FROM @OU_STATEMENTS)
		SET @PROCESS_OU_TASKS=1
			   		 	
	
	/* LETS START LOCKING PROCESS 
		- GENERATE NEW LOCKED STATEMENT CODE (ON NEWLY INSERTED RECORD, SRC_STATEMENTCODE COLUMN VALUE SHOULD BE SET TO UNLOCKED STATEMENT CODE)
		- COPY UNLOCKED DATA FOR LOCKED STATEMENTS
	*/
	DELETE FROM @STATEMENTS		   		 
	INSERT INTO @STATEMENTS(StatementCode)
	SELECT UL.[StatementCode]
	FROM @STATEMENTS_TO_LOCK UL		
		INNER JOIN RPWeb_StatementInformation sti		ON sti.[StatementCode]=UL.[StatementCode]		
	WHERE  sti.[FieldName]='Status' AND sti.[FieldValue]='Ready for QA'


	/* INSERT NEW LOCKED STATEMENT INTO WEB STATEMENTS TABLE */	
	/* WEB STATEMENTS TABLE STATEMENTCODE COLUMN IS DEFINED AS IDENTITY COLUMN, SET TO INSERT NEGATIVE STATEMENT CODES */
	INSERT INTO RPWeb_Statements(Locked_Date,Lock_Job_ID, VersionType, StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, SRC_StatementCode, ChequeRunName , ChequeRunID) 	
	SELECT GetDate(), @JOB_ID, 'Locked', StatementLevel, Status, Season, Region, Category, StatementType, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5, IsPayoutStatement, StatementCode
		--,@CHEQUERUNNAME AS ChequeRunName 
		,'' AS ChequeRunName 
		,@CHEQUERUNID AS ChequeRunID
	FROM RPWeb_Statements ST
	WHERE StatementCode IN (SELECT [StatementCode] FROM @STATEMENTS)

	/*
		TRANSFER EXCEPTIONS FROM UNLOCKED TO LOCKED STATEMENTS THAT WAS NOT APART OF THE REGULAR LOCK-UNLOCK PROCESS
		IN ESSENCE, IF THERE IS A UNLOCKED STATEMENT(SRC_STATEMENTCODE) IN RPWEB_EXCEPTIONS BUT WE HAVE NO 
		CORRESPONDING ROW THAT HAS ZERO FOR THE STATEMENT CODE THE ABOVE SOLUTION WILL NOT WORK.
		THIS WILL ENSURE THAT IT TRANSFERS TO THE NEW UNLOCKED STATEMENT.

	*/
	DELETE FROM @EDIT_HISTORY 

	-- This takes care of the cases where the exception only exist on the unlock statement and needs to be transfered to newly locked statement
	-- TAKE SNAP SHOT OF PREVIOUS EXCEPTIONS
		INSERT INTO RPWeb_Exceptions([StatementCode],[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy],[Show_In_UL],VersionType,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode])   
		OUTPUT inserted.[StatementCode],inserted.ID,'Exception carried over from unlocked statement'
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
		SELECT STL.[StatementCode],EX.[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], STL.[Src_StatementCode], [Src_ID], [Comments], GETDATE(), GETDATE(), [PostedBy], 1 AS [Show_In_UL]		
			,'Historical' AS VersionType
			,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode]
		FROM (
				SELECT StatementCode, SRC_StatementCode , Region
				FROM RPWeb_Statements 
				WHERE Season=@SEASON AND [VersionType]='LOCKED' AND [Status]='Active' AND [Lock_Job_ID]=@JOB_ID 
			) STL
			INNER JOIN (
				SELECT [StatementCode],[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy], [Show_In_UL]
					,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode]										
				FROM RPWeb_Exceptions
				WHERE Season=@SEASON AND [Status]='Approved' AND ISNULL(Show_IN_UL ,0)=1 
					AND VersionType='Current' 
					--AND StatementCode <> 0
			) EX		
			ON EX.SRC_StatementCode=STL.SRC_StatementCode		
		WHERE (
				(EX.Season = 2020 AND EX.StatementCode <> 0)
					OR				
				(STL.Region='EAST' AND EX.StatementCode <> 0) -- 2021+
					OR
				(STL.Region='WEST' AND EX.StatementCode < 0)  -- 2021+
			) 
		

	/* 
		UPDATE RPWEB_EXCEPTIONS 
		--This will take care of cases when the statement is unlocked from a locked version		
		-- LETS VERIFY IF WE STILL NEED THIS APPROACH IN 2021 
	*/
	IF @SEASON=2020
		BEGIN
			UPDATE EX
			SET EX.StatementCode = LCK.Statementcode
			OUTPUT inserted.[StatementCode],inserted.ID,'Exception carried over from unlocked statement'
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
			FROM RPWeb_Exceptions EX
				INNER JOIN @STATEMENTS_TO_LOCK UL	ON UL.StatementCode=EX.SRC_StatementCode
				INNER JOIN RPWeb_Statements LCK		ON LCK.SRC_Statementcode=UL.StatementCode AND LCK.VersionType='Locked' AND LCK.[Status]='Active'
			WHERE EX.[Status]='Approved' AND EX.VersionType='Current' AND EX.StatementCode = 0  			
		END	
	ELSE IF @SEASON >= 2021
		BEGIN
			-- CURRENTLY COPYING OVER WEST EXCEPTIONS FROM UNLOCKED STATEMENTS (VERSIONTYPE=CURRENT)
			INSERT INTO RPWeb_Exceptions([StatementCode],[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy],[Show_In_UL],VersionType,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode])   
				OUTPUT inserted.[StatementCode],inserted.ID,'Exception carried over from unlocked statement'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
			SELECT STL.[StatementCode],EX.[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], STL.[Src_StatementCode], [Src_ID], [Comments], GETDATE(), GETDATE(), [PostedBy], 1 AS [Show_In_UL]		
				,EX.VersionType
				,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode]
			FROM (
					SELECT StatementCode, SRC_StatementCode , Region
					FROM RPWeb_Statements 
					WHERE Season=@SEASON AND [VersionType]='LOCKED' AND [Status]='Active' AND [Lock_Job_ID]=@JOB_ID 			
				) STL
				INNER JOIN (
					SELECT [StatementCode],[VersionType],[RetailerCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy], [Show_In_UL]
						,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode]										
					FROM RPWeb_Exceptions
					WHERE Season=@SEASON AND [Status]='Approved' 
						AND VersionType='Current' 
						AND [StatementCode] >= 0
						--AND ISNULL(Show_IN_UL ,0)=1  -- This column has something to do with East Exceptions
				) EX		
				ON EX.StatementCode=STL.SRC_StatementCode	-- unlocked statementcodes
			
		END
			   		 				
	EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY
	   
	/* 	TARGET: UNLOCKED STATEMENT 		ACTION:  UPDATE STATUS 	*/	
	UPDATE STI
	SET [FieldValue]='Kenna Approval'
	OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA process - Lock','Field','Status',deleted.FieldValue,inserted.FieldValue
	INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
	FROM RPWeb_StatementInformation STI
		INNER JOIN @Statements TGT		ON TGT.StatementCode=STI.StatementCode		
	WHERE STI.[FieldName]='Status' AND [FieldValue]='Ready for QA'

	DELETE FROM @EDIT_HISTORY
	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY
	   
	/*
		REGION: EAST
		ON NEWLY CREATED LOCKED STATEMENTS: WE NEED TO UPDATE DATASETCODE, AS COPIED OVER DATASETCODE IS POINTING TO UNLOCKED LEVEL 2 STATEMENTCODE.
		DATASETCODE SHOULD POINT TO LOCKED STATEMENTCODE OF LEVEL 2 STATEMENT WITHIN FAMILY
	*/
	IF @PROCESS_OU_TASKS=1
	UPDATE T1
	SET DataSetCode=T2.StatementCode
	FROM RPWeb_Statements  T1
		INNER JOIN RPWeb_Statements  T2
		ON T2.Lock_Job_ID=T1.Lock_Job_ID AND T2.SRC_StatementCode=T1.DataSetCode
	WHERE T1.Lock_Job_ID=@JOB_ID AND T1.Region='EAST' AND T1.VersionType='Locked' AND T1.[Status]='Active'

	--UPDATE LEVEL 2 EXCEPTIONS WITH THEIR APPROPRIATE SRC IDS FOR EAST
	UPDATE E
	SET E.SRC_ID = L2.ID
	FROM RPWeb_Exceptions E
	INNER JOIN RPWeb_Statements S ON E.StatementCode = S.StatementCode AND S.VersionType = 'Locked' AND S.Status= 'Active'
	INNER JOIN (
		SELECT ID,StatementCode,RewardCode,Exception_Type, Season, Status, Show_IN_UL 
		FROM RPWeb_exceptions WHERE Exception_level = 'LEVEL 2' AND Status = 'Approved' AND ISNULL(Show_In_UL,0) = 1
	)L2 ON L2.StatementCode = S.DatasetCode AND E.RewardCode = L2.RewardCode AND E.Season = L2.Season AND E.Show_In_UL = L2.Show_In_UL AND E.Status = L2.Status AND E.Exception_Type = L2.Exception_Type
	WHERE E.Exception_Level = 'LEVEL 1'


	IF @PROCESS_HO_TASKS=1
	BEGIN
		/* UPDATE DATASETCODE_L5 to match with Locked Version STatementcode on newly locked statments */
		UPDATE T1
		SET DataSetCode_L5=T2.LockedStatementCode
		FROM RPWeb_Statements  T1
			INNER JOIN @HO_STATEMENTS  T2
			ON T2.StatementCode=T1.DataSetCode_L5
		WHERE T1.Lock_Job_ID=@JOB_ID AND T1.VersionType='Locked' AND T1.StatementLevel IN ('LEVEL 1','LEVEL 2') 
	END	   		 	  

	 			
	/*  TARGET: UNLOCKED STATEMENTS
		ACTION:  UPDATE PAYMENTS 
		CURRENT PAYMENT=NEXT PAYMENT, NEXT_PAYMENT=0 
	*/
	UPDATE RPWeb_Rewards_Summary
	SET [CurrentPayment]=[NextPayment],[NextPayment]=0
	WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)		    
	   
	/* 
		TAKE SNAP SHOT OF EXISTING CHEQUES / EFT
		CHEQUES (THIS NEEDS TO BE DONE SEPERATELY SINCE WE ARE INSERTING THEM AS HISTORICAL AND BECAUSE CHEQUES ARE DIRECTLY LINKED TO THE ARCHIVED STATEMENT) 
	*/
	INSERT INTO RPWeb_Cheques([StatementCode],[VersionType],[ChequeDocCode],[RetailerCode],[PayableToName],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeStatus],[SendTo],[ChequeAmount],[AmtRequested],[AmtReceived],[AmtPaid],[PDFJobID],[PDFDocCode])
	SELECT STL.[StatementCode],'Historical',CHQ.[ChequeDocCode],CHQ.[RetailerCode],CHQ.[PayableToName],CHQ.[ChequeType],CHQ.[ChequeNumber],CHQ.[ChequeDate],CHQ.[ChequeStatus],CHQ.[SendTo],CHQ.[ChequeAmount],CHQ.[AmtRequested],CHQ.[AmtReceived],CHQ.[AmtPaid],CHQ.[PDFJobID],CHQ.[PDFDocCode]
	FROM (
		SELECT StatementCode, SRC_StatementCode 
		FROM RPWeb_Statements 
		WHERE Season=@SEASON AND [VersionType]='LOCKED' AND [Status]='Active' AND [Lock_Job_ID]=@JOB_ID 
	) STL
		INNER JOIN (
			SELECT Statementcode, SRC_Statementcode 
			FROM RPWeb_Statements 
			WHERE Season=@SEASON AND VersionType='Archive' AND [Status]='Active'
		) ARC		
		ON ARC.SRC_StatementCode=STL.SRC_StatementCode
		INNER JOIN RPWeb_Cheques CHQ
		ON CHQ.Statementcode=ARC.Statementcode AND CHQ.VersionType='Current'
			   
	/* DEMAREY FOR NON SUMMARY STATEMENTS */


	/* COPY DATA FROM UNLOCKED STATEMENTS TO ASSOCIATED LOCKED STATEMENTS */
	DECLARE @COPY_CMD NVARCHAR(MAX)
	DECLARE @COPY_PARAMS NVARCHAR(50)='@JOB_ID uniqueidentifier'
	DECLARE COPY_LOCKED_DATA CURSOR FOR 
		SELECT 'INSERT INTO '+so.[name]+'([StatementCode],'+STUFF((
			SELECT ',['+[Name]+']'
			FROM syscolumns sc
			/* LETS NOT INCLUDE THESE COLUMNS - WE ARE UPDATING STATEMENT CODE TO BE THE LOCKED STATEMENT CODE, THE REST ARE UNIQUE RECORD IDS */
			WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id] 
			ORDER BY sc.[id]
			FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+')
		SELECT st.[StatementCode],'+STUFF((
			SELECT ',custom_table.['+[Name]+']'
			FROM syscolumns sc
			WHERE [name] NOT IN ('StatementCode','ExceptionID') AND sc.[id]=so.[id]
			ORDER BY sc.[id]
			FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+'
		FROM '+so.[name]+' custom_table
			INNER JOIN RPWeb_Statements ST
			ON ST.[SRC_StatementCode]=custom_table.[StatementCode]
		WHERE ST.[Status]=''Active'' AND ST.[Lock_Job_ID]=@JOB_ID AND ST.[VersionType]=''Locked''' AS [CODE]
		FROM sysobjects so
		WHERE so.[xtype]='U' AND so.[name] IN (SELECT TableName	FROM RP_Config_WebTableNames WHERE Season=@Season AND LockStatementAction='Yes')

	OPEN COPY_LOCKED_DATA
	FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
	WHILE(@@FETCH_STATUS=0)
		BEGIN	
			EXEC SP_EXECUTESQL @COPY_CMD,@COPY_PARAMS,@JOB_ID
			FETCH NEXT FROM COPY_LOCKED_DATA INTO @COPY_CMD
		END
	CLOSE COPY_LOCKED_DATA
	DEALLOCATE COPY_LOCKED_DATA	  

	-- LETS UPDATE DATASETCODES		
	IF @PROCESS_OU_TASKS=1 OR @PROCESS_HO_TASKS=1
	BEGIN 		
		DECLARE UPDATE_DATASETCODES CURSOR FOR 
			SELECT '
				UPDATE T1
				SET [DataSetCode]=T2.[DataSetCode] ,[DataSetCode_L5]=T2.[DataSetCode_L5]
				FROM '+so.[name]+' T1
					INNER JOIN (
						SELECT [StatementCode], [DataSetCode], [DataSetCode_L5]
						FROM RPWEB_Statements 
						WHERE [Lock_Job_ID]=@JOB_ID AND [Status]=''Active''  AND [Versiontype]=''Locked'' AND [StatementLevel]=''LEVEL 1''  
							AND ([DataSetCode] < 0 OR [DataSetCode_L5] < 0)				
					) T2
					ON T2.Statementcode=T1.Statementcode ' AS [Code]
			FROM sysobjects so
			WHERE so.[xtype]='U' AND so.[name] IN (SELECT TableName	FROM RP_Config_WebTableNames WHERE Season=@Season AND UpdateDataSetCodes='Yes')
			   
		OPEN UPDATE_DATASETCODES
		FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
		WHILE(@@FETCH_STATUS=0)
			BEGIN	
				EXEC SP_EXECUTESQL @SQL_COMMAND, N'@SEASON INT, @JOB_ID uniqueidentifier', @SEASON, @JOB_ID
				FETCH NEXT FROM UPDATE_DATASETCODES INTO @SQL_COMMAND
			END
		CLOSE UPDATE_DATASETCODES
		DEALLOCATE UPDATE_DATASETCODES
	END 
	   
	IF @PROCESS_HO_TASKS=1
	BEGIN			   
		-- FOR LEVEL 5 ,  DELETE EXISTING REWARD SUMMARY DATA OF LOCKED STATEMENT AND REPOPULATE IT FROM LOCKED STATEMENTS OF LEVEL 1
		DELETE REW
		FROM RPWeb_Rewards_Summary REW
			INNER JOIN @HO_STATEMENTS HO	ON HO.LockedStatementcode=REW.Statementcode			
		
		INSERT INTO RPWeb_Rewards_Summary(Statementcode,MarketLetterCode,RewardCode,RewardEligibility,RewardMessage,RewardValue,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)
		SELECT HO.LockedStatementCode, REW.MarketLetterCode, REW.RewardCode, 'Eligible', ''
			,SUM(REW.RewardValue) AS [RewardValue]
			,SUM(REW.TotalReward) AS [TotalReward]
			,SUM(REW.PaidToDate) AS [PaidToDate]
			,SUM(REW.CurrentPayment) AS [CurrentPayment]
			,SUM(REW.NextPayment) AS [NextPayment]
			,SUM(ISNULL(REW.PaymentAmount,0)) AS [PaymentAmount]
		FROM @HO_STATEMENTS HO
			INNER JOIN RPWeb_Statements ST			ON ST.DataSetCode_L5=HO.LockedStatementCode
			INNER JOIN RPWeb_Rewards_Summary REW	ON REW.StatementCode=ST.StatementCode
		WHERE ST.Status='Active' AND ST.VersionType='LOCKED' AND (ST.Region='WEST' OR ST.StatementLevel='LEVEL 1')
		GROUP BY HO.LockedStatementCode, REW.MarketLetterCode, REW.RewardCode
	END

	/****************** POST LOCK ACTIVITY ***********************/
	/*
		ONE TIME PAYMENTS
		TARGET: UNLOCKED STATEMENTS 
		ACTION: UPDATE NEXT PAYMENT OF OTP REWARDS		
	*/

	UPDATE  T1
	SET [NextPayment]=[CurrentPayment] * -1
	FROM RPWeb_Rewards_Summary T1		
		INNER JOIN @STATEMENTS ST 						ON ST.StatementCode=T1.StatementCode		
		INNER JOIN RP_Config_Rewards_Summary CFG		ON CFG.RewardCode=T1.RewardCode  
	WHERE CFG.Season=@SEASON AND CFG.RewardType='OTP' AND T1.[PaidToDate]=0
	
	/* ESTIMATES ADJUSTMENT */
	EXEC [dbo].[RP_RefreshEstimates]  @SEASON, @STATEMENTS 

	/* SUMMARY STATEMENT REFRESH */
	IF @PROCESS_HO_TASKS=1
		EXEC RP_Refresh_HO_RewardsSummary @SEASON, 'Actual', @STATEMENTS

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:
	SELECT 'success' as [Status]
		,ISNULL(@JOB_ID,'') AS Lock_Job_ID  
		,'' AS Error_Message
		,'' as confirmation_message
		,'' as warning_message

END




