﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_E_CustomGroundApplication]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	NEW: RC-3757 - Keep the edit inputs at the OU level as they currently are.
	All products that are eligible for this reward should be summed up respectively within the OU calculation.
	The line item will appear on single location retails, and for families it will appear on the level 2 summary only.

	Custom ground application of products is a key service provided by retailers. Reward per acre
	and acre/case conversion rates are outlined below.

	Reward Opportunity
	• $1.00/acre for custom ground application of the following fungicides and insecticides:
	Caramba® fungicide, Cotegra® fungicide, Headline® AMP fungicide, Lance® fungicide,
	Cantus® fungicide, Priaxor® fungicide and Twinline® fungicide, and Sefina™ insecticide.

	• $0.50/acre for custom ground application of the following herbicides: Conquest® LQ
	herbicide, Engenia® herbicide, Frontier® Max herbicide, Integrity® herbicide, Optill® herbicide,
	Prowl® H2O herbicide and Zidua® SC herbicide.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 416 or StatementCode IN (416, 904)
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_E_CUSTOM_GROUND_APP_REWARD'


	/*
	RP-3121
	NEW NOVEMBER 5 2020
	DEMAREY BAKER 
	CUSTOM APP AMOUNT IS CAPPED ON TOTAL OU POG TOTAL.
	PREVIOUSLY WAS CAPPED AT THE LEVEL 1 THAT CUSTOM APPLIED IT.
	*/
	
	DECLARE @TEMP TABLE (
		StatementCode INT,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		POG_Actual_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		RewardBrand_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		RewardBrand_Sales Numeric(18,2) NOT NULL,
		CustApp_Actual_Acres Numeric(18,2) NOT NULL DEFAULT 0,		
		RewardBrand_CustApp Numeric(18,2) NOT NULL DEFAULT 0,		
		Reward_Amount MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,
		POG_Actual_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		CA_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)
	
	
	DECLARE @TEMP_CA TABLE (
		DataSetCode INT NOT NULL,
		RewardBrand Varchar(100) NOT NULL,
		CA_Actual_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[RewardBrand] ASC
		)
	)



	

	-- NEW: RC-3757 - removed the filter on RS.StatementLevel='LEVEL 1' so we can apply custom app numbers on single location retails and ONLY the level 2 summary for families
	-- Instead of the above insert into @temp, we need to include ALL custom app brands because the reward is calculated based on only user inputs
	-- and the table RP2020_DT_Sales_Consolidated typically only includes qualified brands which have historical and/or current sales.
	-- Ex. If a user entered 500 acres of Cotegra but there was no Cotegra sales, then it would not get included in rewards


	-- NEW: RC-3757 - added addition AND DataSetCode = 0 filter which guarantees that the editable input will only calculate on single location retails and only on the level 2 summary for families
	-- Update Custom App acres
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Acres,RewardBrand_Sales)
		SELECT CFG.StatementCode,CFG.DataSetCode,CFG.MarketLetterCode, CFG.ProgramCode, CFG.GroupLabel, ISNULL(ST.RewardBrand_Acres,0) AS RewardBrand_Acres, ISNULL(ST.RewardBrand_Sales,0) AS RewardBrand_Sales
		FROM (
			SELECT ST.StatementCode,RS.DataSetCode, MLP.MarketLetterCode, MLP.ProgramCode, CG.GroupLabel
			FROM RP_Config_ML_Programs MLP
			LEFT JOIN RP_Config_Groups CG ON MLP.ProgramCode = CG.ProgramCode
			INNER JOIN @STATEMENTS ST ON 1=1
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
			WHERE (RS.StatementLevel='LEVEL 2' OR (RS.StatementLevel='LEVEL 1' AND RS.DataSetCode = 0)) AND MLP.ProgramCode=@PROGRAM_CODE AND CG.GroupType='Reward_Brands'
		) CFG
		LEFT JOIN (
	 		SELECT ST.StatementCode,RS.DataSetCode,tx.MarketLetterCode,tx.ProgramCode,tx.GroupLabel,SUM(tx.Acres_CY) AS RewardBrand_Acres,SUM(tx.Price_CY) AS RewardBrand_Sales
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
				INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
			WHERE (RS.StatementLevel='LEVEL 2' OR (RS.StatementLevel='LEVEL 1' AND RS.DataSetCode = 0)) AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands'
			GROUP BY ST.StatementCode,RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel
		) ST ON CFG.StatementCode = ST.StatementCode AND CFG.MarketLetterCode = ST.MarketLetterCode AND CFG.ProgramCode = ST.ProgramCode AND CFG.GroupLabel = ST.GroupLabel

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Caramba_Acres'
		WHERE RewardBrand = 'CARAMBA'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Cotegra_Acres'
		WHERE RewardBrand = 'COTEGRA'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='HeadlineAmp_Acres'
		WHERE RewardBrand = 'HEADLINE AMP'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Lance_Acres'
		WHERE RewardBrand = 'LANCE'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Cantus_Acres'
		WHERE RewardBrand = 'CANTUS'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Priaxor_Acres'
		WHERE RewardBrand = 'PRIAXOR'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Twinline_Acres'
		WHERE RewardBrand = 'TWINLINE'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Sefina_Acres'
		WHERE RewardBrand = 'SEFINA'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='ConquestLQ_Acres'
		WHERE RewardBrand = 'CONQUEST LQ'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Engenia_Acres'
		WHERE RewardBrand = 'ENGENIA'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='FrontierMax_Acres'
		WHERE RewardBrand = 'FRONTIER'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Integrity_Acres'
		WHERE RewardBrand = 'INTEGRITY'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Optill_Acres'
		WHERE RewardBrand = 'OPTILL'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Prowl_Acres'
		WHERE RewardBrand = 'PROWL'

		UPDATE T1
		SET RewardBrand_CustApp=ISNULL(EI.FieldValue,0)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Zidua_Acres'
		WHERE RewardBrand = 'ZIDUA SC'

	END

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		
		INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,POG_Actual_Acres,RewardBrand_Sales)
		SELECT ST.StatementCode,RS.DataSetCode,tx.MarketLetterCode,tx.ProgramCode,tx.GroupLabel,SUM(tx.Acres_CY) AS POG_Actual_Acres,SUM(tx.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'  
		GROUP BY ST.StatementCode,RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
		
		--GET CUSTOM APP DATA
		INSERT INTO @TEMP_CA(DataSetCode,RewardBrand,CA_Actual_Acres)
		SELECT IIF(CAP.DataSetCode > 0,CAP.DataSetCode,CAP.Statementcode),PR.ChemicalGroup AS RewardBrand
					,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',AcresPerJug,IIF(cap.PackageSize='Litres',PR.AcresPerLitre,PR.ConversionE))) AS [Acres]																																			
		FROM @STATEMENTS ST
			INNER JOIN	RP_DT_CustomAppProducts CAP ON CAP.StatementCode = ST.StatementCode
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode AND RS.StatementLevel='LEVEL 1'
			INNER JOIN (					
				SELECT PR1.Productcode, ChemicalGroup, ConversionE
					,ConversionE/NULLIF(Jugs,0) AS AcresPerJug
					,ConversionE/NULLIF(Volume,0) as AcresPerLitre
					FROM ProductReference PR1
					INNER JOIN ProductReferencePricing PRP
					ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='EAST'
			) PR			ON PR.ProductCode=CAP.ProductCode
		GROUP BY IIF(CAP.DataSetCode > 0,CAP.DataSetCode,CAP.StatementCode), PR.ChemicalGroup


		-- FOR OUS
		INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,POG_Actual_Acres,CA_Acres)
		SELECT 
			T1.DataSetCode,T1.MarketLetterCode,T1.ProgramCode,T1.RewardBrand,
			SUM(T1.POG_Actual_Acres) AS POG_Actual_Acres, MAX(CA.CA_Actual_Acres) AS CA_Acres
		FROM @TEMP T1
		INNER JOIN @TEMP_CA CA ON CA.DatasetCode = T1.DatasetCode AND CA.RewardBrand = T1.RewardBrand
		WHERE T1.DatasetCode > 0
		GROUP BY T1.DataSetCode, T1.MarketLetterCode, T1.ProgramCode,T1.RewardBrand
		
		--MATCHING
		--FOR STANDALONES the datasetcode will be the statementcode so we can join on that condition
		UPDATE T1 
		SET CustApp_Actual_Acres = IIF(CA_Actual_Acres > POG_Actual_Acres, POG_Actual_Acres, CA_Actual_Acres)
		FROM @TEMP T1
			INNER JOIN RP_Statements RS	ON RS.StatementCode = T1.StatementCode
			INNER JOIN @TEMP_CA CA		ON CA.DataSetCode = T1.StatementCode AND T1.RewardBrand = CA.RewardBrand
		WHERE RS.StatementLevel = 'LEVEL 1' AND CA_Actual_Acres > 0 AND T1.DatasetCode = 0
	
		--FOR OUS MATCHING
		UPDATE T1
		SET CustApp_Actual_Acres =  IIF(T2.RunningTotal <= T3.CA_Acres
			   ,IIF(T1.POG_Actual_Acres > T3.CA_Acres,T3.CA_Acres,T1.POG_Actual_Acres)
			   ,T1.POG_Actual_Acres+(T3.CA_Acres-T2.[RunningTotal]))
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T3 ON T3.DatasetCode = T1.DatasetCode AND t1.RewardBrand = t3.RewardBrand
			INNER JOIN (
						SELECT StatementCode, RewardBrand
						,SUM(POG_Actual_Acres) OVER(PARTITION BY DataSetCode, RewardBrand ORDER BY POG_Actual_Acres DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
						FROM @TEMP
						WHERE DataSetCode > 0
			) T2
		ON T2.StatementCode=T1.StatementCode  AND T2.RewardBrand=T1.RewardBrand 
		WHERE T1.POG_Actual_Acres > 0 AND (T2.[RunningTotal] <= T3.POG_Actual_Acres  OR T1.POG_Actual_Acres-(T3.POG_Actual_Acres - T2.[RunningTotal]) > 0)
		AND T3.DatasetCode > 0
		



		--RP-2347
		DECLARE @INTEGRITY_CASE_APP_RATE FLOAT = cast (4 AS FLOAT) / Cast( 6 AS FLOAT) 		
		-- CHNAGE CASE APPLICATION RATE FOR INTEGRITY POG ACRES
		UPDATE @TEMP SET RewardBrand_Acres = IIF(RewardBrand = 'INTEGRITY',POG_Actual_Acres * @INTEGRITY_CASE_APP_RATE, POG_Actual_Acres) --WHERE POG_Actual_Acres > 0

		-- CHNAGE CASE APPLICATION RATE FOR INTEGRITY CUSTOM APP
		UPDATE @TEMP SET RewardBrand_CustApp = IIF(RewardBrand = 'INTEGRITY', CustApp_Actual_Acres * @INTEGRITY_CASE_APP_RATE, CustApp_Actual_Acres) WHERE CustApp_Actual_Acres > 0 
	END

	-- update reward amounts
	UPDATE @Temp SET Reward_Amount = 1 WHERE RewardBrand IN ('CARAMBA','COTEGRA','HEADLINE AMP','LANCE','CANTUS','PRIAXOR','TWINLINE','SEFINA')
	UPDATE @Temp SET Reward_Amount = 0.5 WHERE RewardBrand IN ('CONQUEST LQ','ENGENIA','FRONTIER','INTEGRITY','OPTILL','PROWL','ZIDUA SC')

	-- update reward
	UPDATE @Temp SET [Reward] = [RewardBrand_CustApp] * [Reward_Amount]

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_CustomGroundApplication WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	--Statementcode	MarketLetterCode	ProgramCode	RewardBrand	POG_Actual_Acres	RewardBrand_Acres	RewardBrand_Sales	CustApp_Acutal_Acres	RewardBrand_CustApp	Reward_Amount	Reward
	INSERT INTO RP2020_DT_Rewards_E_CustomGroundApplication(Statementcode, MarketLetterCode, ProgramCode, RewardBrand,POG_Actual_Acres, RewardBrand_Acres, RewardBrand_Sales,CustApp_Actual_Acres, RewardBrand_CustApp, Reward_Amount, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand,POG_Actual_Acres, RewardBrand_Acres, RewardBrand_Sales,CustApp_Actual_Acres,RewardBrand_CustApp,Reward_Amount, Reward
    FROM @TEMP
		
      UNION

    SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
        ,SUM(POG_Actual_Acres) AS POG_Actual_Acres
		,SUM(RewardBrand_Acres) AS RewardBrand_Acres
        ,SUM(RewardBrand_Sales) AS RewardBrand_Sales
        ,SUM(CustApp_Actual_Acres) AS CustApp_Actual_Acres
		,SUM(RewardBrand_CustApp) AS RewardBrand_CustApp
        ,MAX(Reward_Amount) AS Reward_Amount
        ,SUM(Reward) AS Reward
    FROM @TEMP
    WHERE DataSetCode > 0
    GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
END


