﻿


CREATE PROCEDURE [dbo].[RP_Agent_RunActivities] @ACTIVITY_SEASON INT, @ACTIVITY_GROUP VARCHAR(50), @ACTIVITY_TYPE VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ACTIVITY_LOGID TABLE([ActivityLogID] INT)	
	DECLARE @DEPENDENCY_CHECK BIT
	DECLARE @ACTIVITY_ID INT
	DECLARE @ACTIVITY_NAME VARCHAR(100)
	DECLARE @ACTIVITY_ACTION VARCHAR(255)
	DECLARE ACTIVITY_LOOP CURSOR FOR
		SELECT [ActivityID],[ActivityAction],[ActivityName]
		FROM RP_ActivityDetails
		WHERE [ActivitySeason]=@ACTIVITY_SEASON AND [ActivityStatus]='Active' AND [ActivityGroup]=@ACTIVITY_GROUP AND [ActivityType]=@ACTIVITY_TYPE
		ORDER BY [ActivityID]
	OPEN ACTIVITY_LOOP
	FETCH NEXT FROM ACTIVITY_LOOP INTO @ACTIVITY_ID, @ACTIVITY_ACTION, @ACTIVITY_NAME
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			--CHECK TO MAKE SURE THE REQUIRED DEPENDENCIES RAN
			SET @DEPENDENCY_CHECK=0
			IF(
				--CHECK TO MAKE SURE WE HAVE AT LEAST ONE DEPENDENCY
				EXISTS(SELECT * FROM RP_ActivityDependencies WHERE [ActivityID]=@ACTIVITY_ID)
				--CHECK TO SEE IF ANY DEPENDENCY FAILED
				AND EXISTS(
					SELECT al.[ActivityID],al.[ActivityStatus]
					FROM RP_ActivityDependencies ad
						INNER JOIN(
							SELECT [ActivityID],[ActivityStatus]
							FROM(
								SELECT [ActivityID],[ActivityStatus],RANK() OVER (PARTITION BY [ActivityID] ORDER BY [ActivityDate] DESC) AS [ActivityRank]
								FROM RP_ActivityLog al
							)logs
							WHERE [ActivityRank]=1 AND COALESCE([ActivityStatus],'')<>'SUCCESS'
						) al
						ON al.[ActivityID]=ad.[DependencyID]
					WHERE ad.[ActivityID]=@ACTIVITY_ID))
				BEGIN 
					SET @DEPENDENCY_CHECK=0
					INSERT INTO RP_ActivityLog([ActivityID],[ActivityType],[ActivityName],[ActivityAction],[ActivityStatus],[ActivityStatusMessage],[ActivityDate],[ActivityEndDateTime])
					VALUES (@ACTIVITY_ID,@ACTIVITY_TYPE,@ACTIVITY_NAME,@ACTIVITY_ACTION,'FAILURE','Activity did not run, because a required dependency did not complete successfully.',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)					 
				END 
			ELSE 
				BEGIN
					SET @DEPENDENCY_CHECK=1
				END
				
			--RUN ACTIVITY (IF WE PASSED DEPENDENCY CHECK)
			IF(@DEPENDENCY_CHECK=1)
				BEGIN
					DELETE FROM @ACTIVITY_LOGID
					INSERT INTO RP_ActivityLog([ActivityID],[ActivityType],[ActivityName],[ActivityAction],[ActivityStatus],[ActivityStatusMessage],[ActivityDate])
					OUTPUT INSERTED.ActivityLogID INTO @ACTIVITY_LOGID
					VALUES (@ACTIVITY_ID,@ACTIVITY_TYPE,@ACTIVITY_NAME,@ACTIVITY_ACTION,'IN PROGRESS','Activity is currently running',CURRENT_TIMESTAMP)					 
											
					BEGIN TRY 
						--execute activity
						EXEC ('EXEC '+@ACTIVITY_ACTION)
						--update activity log to success
						UPDATE RP_ActivityLog
						SET [ActivityStatus]='SUCCESS',[ActivityEndDateTime]=CURRENT_TIMESTAMP,[ActivityStatusMessage]=''
						WHERE [ActivityLogID] IN (SELECT [ActivityLogID] FROM @ACTIVITY_LOGID)
					END TRY
					
					BEGIN CATCH
						--update activity log to failure
						UPDATE RP_ActivityLog
						SET [ActivityStatus]='FAILURE',[ActivityEndDateTime]=CURRENT_TIMESTAMP,[ActivityStatusMessage] = ISNULL(ERROR_PROCEDURE(),'') + ' ' + ERROR_MESSAGE()
						WHERE [ActivityLogID] IN (SELECT [ActivityLogID] FROM @ACTIVITY_LOGID)
					END CATCH					
				END
			FETCH NEXT FROM ACTIVITY_LOOP INTO @ACTIVITY_ID, @ACTIVITY_ACTION, @ACTIVITY_NAME
		END
	CLOSE ACTIVITY_LOOP
	DEALLOCATE ACTIVITY_LOOP	
	
END
