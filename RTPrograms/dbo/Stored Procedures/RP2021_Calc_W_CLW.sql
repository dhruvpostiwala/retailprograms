﻿

CREATE PROCEDURE [dbo].[RP2021_Calc_W_CLW]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	--This reward is only for Nutrien D520062427

	/*
	Reward Opportunity
	Clearfield Commitment Reward	
	Matching Acres Reward
	Distributors can earn 7.5% reward on matching acres of Altitude FX herbicides sold to Clearfield wheat growers who have a Clearfield commitment received on or before October 8, 2020.

		-- 2021
	·	Growers must have a valid signed Evergreen Clearfield Commitment to qualify.
	·	Registered Clearfield acres must be received by October 8, 2021.
	·	Multiple herbicide reward brands may be combined to match the total amount of registered CL Wheat acres.
	·	Matched herbicide acres will be prioritized by highest value first.
	·	In a case where multiple varieties are registered, this reward will sum the highest acre registration per variety, for all registered varieties.
	·	For example, if a grower registers 100 acres of Variety A, and then re-submits 200 acres of variety A and 300 acres of variety B, RP will calculate based on 500 acres of Clearfield Wheat.
	*/


	DECLARE @CommitmentReward AS TABLE (
		Statementcode INT NOT NULL,
		MarketLetterCode VARCHAR(50),
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		CommitmentDate  Date not null,
		Req_ReceivedByDate BIT NOT NULL DEFAULT 0,
		RewardType varchar(50) NOT NULL DEFAULT '',
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC,
			Retailercode ASC,
			Farmcode ASC
		)
	)


	DECLARE @FarmsWithCommitment TABLE (
		StatementCode INT NOT NULL,
		Farmcode VARCHAR(20) NOT NULL
	)

	DECLARE @MatchingPriority TABLE (
		ChemicalGroup VARCHAR(65),
		[Priority] INT,		
		PRIMARY KEY CLUSTERED (
			ChemicalGroup ASC
		)
	)
	
	INSERT INTO @MatchingPriority(ChemicalGroup,[Priority])
	VALUES('ALTITUDE FX2',1)
	,('ALTITUDE FX3',1)
	,('ALTITUDE FX',2)
	   		
	DECLARE @REWARD_PRECENTAGE FLOAT=0.075; 
	

	-- MATCHING CLEARFIELD HERBICIDES REWARD
	--on Altitude FX 
			
	INSERT INTO @FarmsWithCommitment (StatementCode, FarmCode)
	SELECT C.StatementCode, C.FarmCode
	FROM @Statements S
		INNER JOIN (
			SELECT StatementCode, FarmCode ,MIN(CAST(CommitmentDate AS DATE)) AS CommitmentDate	
			FROM RP_DT_Commitments 
			WHERE [Status]='Complete'  AND [CommitmentType]='Wheat' AND BASFSeason IN (@SEASON, @SEASON+1)
				AND CommitmentDate <=  CAST('2021-10-08' AS DATE)
			GROUP BY  StatementCode, FarmCode

		) C		
		ON C.StatementCode=S.StatementCode	
	
	-- CLEARFIELD WHEAT COMMITED ACRES
	DROP TABLE IF EXISTS #Wheat
	SELECT ST.Statementcode, MLP.MarketLettercode, s.Retailercode, tx.FarmCode	,SUM(Acres) AS [Acres]						
	INTO #Wheat
	FROM @FarmsWithCommitment ST		
		INNER JOIN RP_Statements S				ON S.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT StatementCode, Farmcode, ISNULL(Acres,0) AS Acres
			FROM RP_DT_Transactions 
			WHERE BASFSeason=@SEASON AND InRadius=1 AND ChemicalGroup='CLEARFIELD WHEAT' AND CFStatus='Complete'
				AND FarmCode NOT IN ('F520020402','F520040993')
		) TX		
		ON TX.StatementCode=ST.StatementCode AND TX.FarmCode=ST.Farmcode
	WHERE MLP.ProgramCode=@PROGRAM_CODE 
	GROUP BY ST.Statementcode,  MLP.MarketLettercode, s.Retailercode, tx.FarmCode
	   
	-- TEMP Altitude Sales
	DROP TABLE IF EXISTS #TEMP_Altitude_SALES

	SELECT ST.Statementcode,  S.StatementLevel, GR.MarketLetterCode, s.Retailercode, tx.FarmCode, tx.ChemicalGroup  
		,tx.Price_SWP AS [Sales]						
		,tx.Acres
		,M.[Priority]  AS MatchingOrder
		,TX.RetailerCode AS EffectiveRetailerCode
	INTO #TEMP_Altitude_SALES
	FROM (
			SELECT StatementCode 
			FROM @FarmsWithCommitment  
			GROUP BY Statementcode
		) ST
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements S				ON TX.StatementCode=S.StatementCode
		INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
		INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID
		INNER JOIN @MatchingPriority M			ON M.ChemicalGroup=TX.ChemicalGroup
	WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD WHEAT'
		AND TX.FarmCode NOT IN ('F520020402','F520040993')
	
	DELETE T1
	FROM #TEMP_Altitude_SALES T1
		LEFT JOIN @FarmsWithCommitment T2
		ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.Farmcode
	WHERE T2.Farmcode IS NULL

	-- OU AND HEAD OFFICE BREAK DOWN
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN (
			SELECT StatementCode 
			FROM @FarmsWithCommitment 
			GROUP BY StatementCode
		) ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT tx.Statementcode, tx.MarketLetterCode, @PROGRAM_CODE, tx.EffectiveRetailerCode, tx.Farmcode,SUM(tx.Sales) AS Sales, 1		
	FROM #TEMP_Altitude_SALES tx					
	WHERE tx.StatementLevel IN ('LEVEL 2','LEVEL 5')
	GROUP BY tx.Statementcode, tx.MarketLetterCode, tx.EffectiveRetailerCode, tx.Farmcode
	-- END OF HEAD OFFICE BREAK DOWN
	
	DROP TABLE IF EXISTS #ALTITUDE
	SELECT Statementcode, MarketLetterCode, RetailerCode, FarmCode, ChemicalGroup 
		,SUM(Sales) AS [Sales]
		,SUM(Acres) AS [Acres]
		,MAX(MatchingOrder) AS MatchingOrder
		,CAST(0 AS DECIMAL(18,2)) AS MatchedAcres
		,CAST(0 AS MONEY) AS MatchedSales
		,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
		,CAST(0 AS MONEY) AS Reward
	INTO #ALTITUDE
	FROM #TEMP_Altitude_SALES
	GROUP BY Statementcode, MarketLetterCode, RetailerCode, FarmCode, ChemicalGroup

	-- LETS DO Altitude FX MATCHING
	UPDATE T1
	SET MatchedAcres = IIF(T2.[RunningTotal] <= CLW.Acres, T1.Acres,T1.Acres+(CLW.[Acres]-T2.[RunningTotal]))
	FROM #ALTITUDE T1
		INNER JOIN #Wheat CLW
		ON CLW.Statementcode=T1.StatementCode AND CLW.MarketLetterCode=T1.MarketLetterCode AND CLW.FarmCode=T1.FarmCode -- AND CLW.RetailerCode=T1.RetailerCode
		INNER JOIN (
			SELECT [StatementCode],[RetailerCode],[Farmcode],[ChemicalGroup]
				,SUM(Acres) OVER(PARTITION BY [StatementCode],[RetailerCode],[Farmcode] ORDER BY [MatchingOrder] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
			FROM #ALTITUDE
		) T2
		ON T2.Statementcode=T1.StatementCode AND T2.RetailerCode=T1.RetailerCode AND T2.FarmCode=T1.FarmCode AND T2.ChemicalGroup=T1.ChemicalGroup
	WHERE (T2.[RunningTotal] <= CLW.Acres  OR T1.Acres+(CLW.[Acres]-T2.[RunningTotal]) > 0)


	-- UPDATE MATCHING SALES, REWARD PERCENTAGE AND REWARD
	UPDATE #ALTITUDE SET [MatchedSales]=[Sales] * ([MatchedAcres]/[Acres]) WHERE [Acres] > 0
	UPDATE #ALTITUDE SET [Reward_Percentage]=@REWARD_PRECENTAGE,  [Reward]=[MatchedSales] * @REWARD_PRECENTAGE	WHERE [MatchedSales] > 0

	/*
	-- LETS INSERT DATA INTO TABLES FROM TEMP TABLES		
	DELETE FROM RP2020_DT_Rewards_W_CLW_Comm WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CLW_Comm(Statementcode, MarketLetterCode, ProgramCode, Retailercode, Farmcode, CommitmentDate, Req_ReceivedByDate, RewardType, Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode,  CommitmentDate, Req_ReceivedByDate, RewardType, Reward
	FROM @CommitmentReward
	*/

	DELETE FROM RP2021_DT_Rewards_W_CLW_Herb WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2021_DT_Rewards_W_CLW_Herb(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,Acres,Sales,MatchedAcres,MatchedSales,Reward_Percentage,Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode, ChemicalGroup as RewardBrand, Acres, Sales, MatchedAcres, MatchedSales, Reward_Percentage, Reward
	FROM #ALTITUDE
		
		UNION

	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode,'CLEARFIELD WHEAT' as RewardBrand, Acres, 0 as Sales, 0 as MatchedAcres, 0 as MatchedSales, 0 as Reward_Percentage, 0 as Reward
	FROM #Wheat
	   
END
