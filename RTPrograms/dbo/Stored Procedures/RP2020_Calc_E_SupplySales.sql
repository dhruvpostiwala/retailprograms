﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_E_SupplySales]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		Level5Code VARCHAR(50) NOT NUll,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		DataSubmissionCount INT NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
			
		)
	)

	DECLARE @VALIDMONTHSUB AS INT

	
	DECLARE @SUBMISSIONS TABLE (
	 StatementCode INT NOT NULL,	 
	 Valid_Sep_Oct  BIT DEFAULT 0,
	 ValidMonthofSubmissionCount INT NOT NULL DEFAULT 0,
	 Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
	 PRIMARY KEY CLUSTERED  (
		[Statementcode] ASC
	 )
	)
	
	DECLARE @RewardPercentage Float=0.0075;

	/*
		Receive up to 0.75% reward on 2020 POG sales of InVigor hybrid canola when you submit transactional data monthly to BASF
	*/

	-- TO BE CONFIRMED : PROBABLY THIS PROGRAM QUALIFICATION SHOULD BE BASED OF FAMILY LEVEL (LEVEL 2) DATA SUBMISSION ?????????????????????

	-- FOR FORECAST / PROJECTION REWARD IS STRAIGHT FORWARD 0.75 %
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN			
		INSERT INTO @TEMP(StatementCode,DataSetCode,Level5Code,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,DataSubmissionCount)
		SELECT ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup AS RewardBrand
			,SUM(tx.Price_CY) AS RewardBrand_Sales
			,12 AS DataSubmissionCount
			-- ,@RewardPercentage AS Reward_Percentage 	,SUM(tx.Price_CY * @RewardPercentage) AS Reward
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS					ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
		WHERE rs.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND (tx.GroupType IN ('REWARD_BRANDS','ELIGIBLE_BRANDS') OR Pr.ChemicalGroup in ('POLYRAM', 'KUMULUS'))
		GROUP BY ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup 

		UPDATE @TEMP
		SET Reward_Percentage=@RewardPercentage, Reward=RewardBrand_Sales * @RewardPercentage
		WHERE RewardBrand_Sales > 0

		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	
	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN

		INSERT INTO @TEMP(StatementCode,DataSetCode,Level5Code,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
		SELECT ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup AS RewardBrand
			,SUM(tx.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS						ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
		WHERE rs.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND (tx.GroupType IN ('REWARD_BRANDS','ELIGIBLE_BRANDS') OR Pr.ChemicalGroup in ('POLYRAM', 'KUMULUS'))
		GROUP BY ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup 

		-- QUALIFICATION @ LEVEL 2 FOR FAIMILIES AND @ LEVEL 1 FOR INDEPENDENTS
		DROP TABLE IF EXISTS #QUALIFIER_STATEMENTCODES		
		SELECT StatementCode
		INTO #QUALIFIER_STATEMENTCODES
		FROM (
			SELECT IIF(DATASETCODE > 0,DATASETCODE,StatementCode) AS StatementCode
			FROM @TEMP
		) T
		GROUP BY StatementCode

		INSERT INTO @SUBMISSIONS(StatementCode,Valid_Sep_Oct,ValidMonthofSubmissionCount)
		SELECT ST.StatementCode
			,MAX(CASE WHEN MonthOfSubmission IN ('Sep','Oct') THEN 1  ELSE 0 END) AS Valid_Sep_Oct			
			,COUNT(DISTINCT MonthOfSubmission) AS ValidMonthofSubmissionCount
		FROM #QUALIFIER_STATEMENTCODES ST
			INNER JOIN (
				SELECT StatementCode,RetailerCode
					--,trim(SUBSTRING(MonthOfSubmission, CHARINDEX('.', MonthOfSubmission)+1, LEN(MonthOfSubmission)-CHARINDEX('.', MonthOfSubmission))) AS MonthOfSubmission 
					,RIGHT(LEFT(MonthOfSubmission,7),3) AS MonthOfSubmission 
				FROM ( 
					SELECT StatementCode, Season,RetailerCode,DataType,MonthOfSubmission 
					FROM RP_DT_RetailerDataNotExpectedTrackerAll
					
						UNION ALL
					
					SELECT StatementCode, Season,RetailerCode,DataType,MonthOfSubmission 
					FROM RP_DT_RetailerDataSubmissionDataAll
				)sub
				-- WHERE sub.DataType <>'Macro Data' 
				WHERE sub.DataType IN ('Invigor Data','Chemical Transactional Data','Transactional Data')
					AND sub.MonthOfSubmission <> '<Not A Standard Submission>'
			) DataSub 
			ON DataSub.StatementCode=ST.StatementCode 
		WHERE DataSub.MonthOfSubmission NOT IN ('Nov','Dec')
		GROUP BY ST.StatementCode 
		
		DELETE FROM @SUBMISSIONS WHERE Valid_Sep_Oct = 0

		--Reward valid statments 
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.0025	WHERE  ValidMonthofSubmissionCount = 1 
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.005	WHERE  ValidMonthofSubmissionCount BETWEEN 3 AND 5 
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.0075	WHERE  ValidMonthofSubmissionCount >= 6 

		-- LEVEL 1 INDEPENDENT STATEMENTS 
		UPDATE T1
		SET DataSubmissionCount  = Sub.ValidMonthofSubmissionCount
			,T1.Reward_Percentage = Sub.Reward_Percentage
			,T1.Reward = Sub.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
			INNER JOIN @SUBMISSIONS Sub
			ON Sub.StatementCode = T1.StatementCode
			
		-- LEVEL 1 STATEMENTS FROM FAMILY
		UPDATE T1
		SET DataSubmissionCount  = Sub.ValidMonthofSubmissionCount
			,T1.Reward_Percentage = Sub.Reward_Percentage
			,T1.Reward = Sub.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
			INNER JOIN @SUBMISSIONS Sub
			ON Sub.StatementCode = T1.DataSetCode
					   			 

		/*
		UPDATE T1
		SET DataSubmissionCount  = Sub.ValidMonthofSubmissionCount, T1.Reward_Percentage = Sub.Reward_Percentage, T1.Reward = Sub.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
		INNER JOIN @SUBMISSIONS Sub ON Sub.StatementCode  = T1.StatementCode
		*/
		/*	: The following East Distributors should always receive 0.75% (max reward) as they submit monthly. 
			1.	Thompsons
			2.	Synagri
			3.	Cargill
			4.	Sollio
			select Distinct(Level5Code),RetailerName from RetailerProfile where Retailername like '%sollio%'
			union
			select Distinct(Level5Code),RetailerName from RetailerProfile where Retailername like '%Synagri%'
			union
			select Distinct(Level5Code),RetailerName from RetailerProfile where Retailername like '%Thompsons%'
			--
			D0000111	SOLLIO AGRICULTURE S.E.C.
			D0000212	THOMPSONS LIMITED
			D520001300	SYNAGRI
		*/
		
		UPDATE @TEMP SET Reward_Percentage = 0.0075 WHERE Level5Code IN('D0000111','D0000212','D520001300')
		
		UPDATE T1
		SET T1.Reward = T1.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
		WHERE Level5Code IN('D0000111','D0000212','D520001300')

	END
	

	-- INSERT INTO DT TABEL FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_SupplySales WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_SupplySales(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,MAX(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand

END
