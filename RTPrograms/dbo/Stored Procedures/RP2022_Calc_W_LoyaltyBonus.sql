﻿
CREATE PROCEDURE [dbo].[RP2022_Calc_W_LoyaltyBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CPP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'
	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;
	DECLARE @HAS_LINE_COMPANY_STATEMENTS BIT = 0
	
	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		RewardBrand VARCHAR(255) NOT NULL,
		
		SegmentA_Sales_LY_Pulse MONEY NOT NULL,
		SegmentA_Sales_LY_Pulse_INDEX MONEY NOT NULL,
		SegmentA_Sales_LY_Cereal MONEY NOT NULL,
		SegmentA_Sales_LY_Cereal_INDEX MONEY NOT NULL,

		SegmentA_Sales_LY MONEY NOT NULL DEFAULT 0,
		SegmentA_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentA_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		SegmentA_Reward_Sales MONEY NOT NULL,
		SegmentA_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentA_Reward MONEY NOT NULL DEFAULT 0,
		
		SegmentB_Sales_LY MONEY NOT NULL,
		SegmentB_Sales_LY_INDEX MONEY NOT NULL,
		SegmentB_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		SegmentB_Reward_Sales MONEY NOT NULL,
		SegmentB_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentB_Reward MONEY NOT NULL DEFAULT 0,

		SegmentC_Sales_LY MONEY NOT NULL DEFAULT 0,
		SegmentC_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentC_Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		SegmentC_Reward_Sales MONEY NOT NULL DEFAULT 0,
		SegmentC_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentC_Reward MONEY NOT NULL DEFAULT 0,

		SegmentD_Sales_LY MONEY NOT NULL DEFAULT 0,
		SegmentD_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentD_Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		SegmentD_Reward_Sales MONEY NOT NULL DEFAULT 0,
		SegmentD_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		SegmentD_Reward MONEY NOT NULL DEFAULT 0,

		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			RewardBrand ASC
		)
	);


	DECLARE @QUALIFYING TABLE (
		StatementCode INT NOT NULL,
		
		SegmentA_Reward_Sales MONEY NOT NULL,
		SegmentA_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentA_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,

		SegmentB_Reward_Sales MONEY NOT NULL,
		SegmentB_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentB_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		
		SegmentC_Reward_Sales MONEY NOT NULL,
		SegmentC_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentC_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		
		SegmentD_Reward_Sales MONEY NOT NULL,
		SegmentD_Sales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		SegmentD_Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,

		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC
		)
	);

	DECLARE @CROP_SEGMENT TABLE (
		Crop_Segment VARCHAR(50),
		ChemicalGroup VARCHAR(100),
		PRIMARY KEY CLUSTERED (
			[ChemicalGroup] ASC
		)
	);


	INSERT	INTO @CROP_SEGMENT(Crop_Segment, ChemicalGroup)
	SELECT	IIF(B.ChemicalGroup = 'ARMEZON', 'CEREAL', 'PULSE') AS Crop_Segment,
			B.ChemicalGroup
	FROM	RP_Config_Groups G
			INNER	JOIN RP_Config_Groups_Brands B 
			ON B.GroupID = G.GroupID
	WHERE	ProgramCode = @PROGRAM_CODE AND GroupType = 'QUALIFYING_BRANDS'


	-- Get data from sales consolidated
	INSERT INTO @TEMP(StatementCode, Level5Code, RewardBrand		
		,SegmentA_Sales_LY_Pulse, SegmentA_Sales_LY_Pulse_INDEX
		,SegmentA_Sales_LY_Cereal, SegmentA_Sales_LY_Cereal_INDEX
		,SegmentA_Reward_Sales
		,SegmentB_Sales_LY, SegmentB_Sales_LY_INDEX, SegmentB_Reward_Sales
		--,SegmentC_Sales_LY, SegmentC_Sales_LY_INDEX, SegmentC_Reward_Sales
		--,SegmentD_Sales_LY, SegmentD_Sales_LY_INDEX, SegmentD_Reward_Sales
	)
	SELECT ST.StatementCode, TX.Level5Code, PR.ChemicalGroup
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'PULSE' , TX.Price_LY1 ,0), 0)) [SegmentA_Sales_LY_Pulse]
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'PULSE' ,TX.Price_LY1 * ISNULL(TX.YOY_Index, 1) ,0), 0)) [SegmentA_Sales_LY_Pulse_INDEX]

		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'CEREAL', TX.Price_LY1 ,0), 0)) [SegmentA_Sales_LY_Cereal]
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT A', IIF(CS.Crop_Segment = 'CEREAL' ,TX.Price_LY1 * ISNULL(TX.YOY_Index, 1) ,0), 0)) [SegmentA_Sales_LY_Cereal_INDEX]

		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT A', TX.Price_CY, 0)) [SegmentA_Reward_Sales]
		   
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1, 0)) [SegmentB_Sales_LY]
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_LY1 * ISNULL(TX.YOY_Index, 1), 0)) [SegmentB_Sales_LY_Index]
		   ,SUM(IIF(TX.GroupLabel = 'SEGMENT B', TX.Price_CY, 0)) [SegmentB_Reward_Sales]
		   
		   /*
		   This code ment for season 2021 Line Company calculations, may be we need it for 2022 as well.
		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT C', TX.Price_LY1, 0)) [SegmentC_Sales_LY]
		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT C', TX.Price_LY1 * ISNULL(TX.YOY_Index, 1), 0)) [SegmentC_Sales_LY_Index]
		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT C', TX.Price_CY, 0)) [SegmentC_Reward_Sales]		   

		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT D', TX.Price_LY1, 0)) [SegmentD_Sales_LY]
		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT D', TX.Price_LY1 * ISNULL(TX.YOY_Index, 1), 0)) [SegmentD_Sales_LY_Index]
		   ,SUM(IIF(@STATEMENT_TYPE='Actual' AND TX.GroupLabel = 'SEGMENT D', TX.Price_CY, 0)) [SegmentD_Reward_Sales]
		   */
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR					ON TX.ProductCode = PR.ProductCode
		LEFT JOIN @CROP_SEGMENT CS						ON CS.ChemicalGroup = PR.ChemicalGroup		
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
	GROUP BY ST.StatementCode, TX.Level5Code, PR.ChemicalGroup
		
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		BEGIN
			UPDATE T1
			SET SegmentA_Reward_Sales = SegmentA_Reward_Sales * (ISNULL(EI.FieldValue, 100) / 100)
				,SegmentB_Reward_Sales = SegmentB_Reward_Sales * (ISNULL(EI.FieldValue, 100) / 100)
			FROM @TEMP T1
				LEFT JOIN RPWeb_User_EI_FieldValues EI
				ON EI.StatementCode=T1.StatementCode AND EI.FieldCode = 'Radius_Percentage_CPP'
			WHERE EI.MarketLetterCode=@CPP_ML_CODE  
					   			 
			-- Factor in CAR indexing percentages for the different brand groupings
			-- Pulse
			UPDATE T1
			SET SegmentA_Sales_LY_Pulse = SegmentA_Sales_LY_Pulse * (EI.FieldValue / 100),
				SegmentB_Sales_LY = SegmentB_Sales_LY * (EI.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RPWeb_User_EI_FieldValues EI
				ON T1.StatementCode = EI.StatementCode AND EI.FieldCode = 'CAR_Indexing_Pulse'
			WHERE EI.MarketLetterCode=@CPP_ML_CODE

			-- Cereal
			UPDATE T1
			SET SegmentA_Sales_LY_Cereal = SegmentA_Sales_LY_Cereal * (EI.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RPWeb_User_EI_FieldValues EI
				ON T1.StatementCode = EI.StatementCode  AND EI.FieldCode = 'CAR_Indexing_Cereal'
			WHERE EI.MarketLetterCode=@CPP_ML_CODE  

			-- SET THE INDEX VALUES IDENTICAL TO THE REGULAR VALUES (FOR FORECAST AND PROJECTIONS)
			-- This is because index values are used for display and calculation purposes (aka growth)
			UPDATE	T1
			SET		T1.SegmentA_Sales_LY_Pulse_INDEX = T1.SegmentA_Sales_LY_Pulse
					,T1.SegmentA_Sales_LY_Cereal_INDEX = T1.SegmentA_Sales_LY_Cereal
					,T1.SegmentB_Sales_LY_INDEX =  T1.SegmentB_Sales_LY
			FROM	@TEMP T1


		END -- END FORECAST PROJECTION	
	ELSE IF EXISTS(SELECT TOP 1 1 FROM @TEMP WHERE LEVEL5CODE IN ('D0000107','D0000137','D0000244','D520062427') )
		SET @HAS_LINE_COMPANY_STATEMENTS = 1

	-- Add together the separate brand groupings to get total qualifying sales for last year
	UPDATE	@TEMP
	SET		SegmentA_Sales_LY = SegmentA_Sales_LY_Pulse + SegmentA_Sales_LY_Cereal
			,SegmentA_Sales_LY_INDEX = SegmentA_Sales_LY_Pulse_INDEX + SegmentA_Sales_LY_Cereal_INDEX

	
	-- INSERT INTO QUALIFYING AGGREGATE NUMBERS
	INSERT	INTO @QUALIFYING(StatementCode,SegmentA_Reward_Sales,SegmentA_Sales_LY_INDEX,SegmentB_Reward_Sales,SegmentB_Sales_LY_INDEX,SegmentC_Reward_Sales,SegmentC_Sales_LY_INDEX,SegmentD_Reward_Sales,SegmentD_Sales_LY_INDEX)
	SELECT	StatementCode
			,SUM(SegmentA_Reward_Sales) AS SegmentA_Reward_Sales			,SUM(SegmentA_Sales_LY_INDEX) AS SegmentA_Sales_LY_INDEX
			,SUM(SegmentB_Reward_Sales) AS SegmentB_Reward_Sales			,SUM(SegmentB_Sales_LY_INDEX) AS SegmentB_Sales_LY_INDEX
			,SUM(SegmentC_Reward_Sales) AS SegmentC_Reward_Sales				,SUM(SegmentC_Sales_LY_INDEX) AS SegmentC_Sales_LY_INDEX
			,SUM(SegmentD_Reward_Sales) AS SegmentD_Reward_Sales				,SUM(SegmentD_Sales_LY_INDEX) AS SegmentD_Sales_LY_INDEX
	FROM	@TEMP 
	GROUP	BY StatementCode

	
	-- CALCULATE GROWTH PERCENTAGE IN QUALIFYING TABLE	
	UPDATE @QUALIFYING
		SET SegmentA_Growth_Percentage = IIF(SegmentA_Sales_LY_INDEX > 0, SegmentA_Reward_Sales / SegmentA_Sales_LY_INDEX, IIF(SegmentA_Reward_Sales > 0, 9.9999, 0)),
			SegmentB_Growth_Percentage = IIF(SegmentB_Sales_LY_INDEX > 0, SegmentB_Reward_Sales / SegmentB_Sales_LY_INDEX, IIF(SegmentB_Reward_Sales > 0, 9.9999, 0))
	WHERE SegmentA_Reward_Sales > 0 OR SegmentB_Reward_Sales > 0
	
			
	-- INSERT THE GROWTH PERCENTAGE INTO TEMP TABLE
	UPDATE	T1
	SET		T1.SegmentA_Growth_Percentage  = T2.SegmentA_Growth_Percentage
			,T1.SegmentB_Growth_Percentage = T2.SegmentB_Growth_Percentage
			,T1.SegmentC_Growth_Percentage = T2.SegmentC_Growth_Percentage
			,T1.SegmentD_Growth_Percentage = T2.SegmentD_Growth_Percentage
	FROM	@TEMP T1
		INNER	JOIN @QUALIFYING T2 
		ON T1.StatementCode = T2.StatementCode 

	DECLARE @REWARD_MARGINS_A AS [dbo].[UDT_RP_TIERED_MARGINS]
	DECLARE @REWARD_MARGINS_B AS [dbo].[UDT_RP_TIERED_MARGINS]
	
	INSERT INTO @REWARD_MARGINS_A(Level5Code, Min_Value, Max_Value, Reward_Percentage)
	VALUES('D000001',0.80,0.90,0.015)		--	>= 80% AND < 90%	1.25%
	,('D000001',0.90,1,0.0375)			    --	>= 90% AND < 100%	3.75%
	,('D000001',1,-1,0.0425)			    --	>= 100%				4.25%
	--FCL COOP
	,('D0000117',0.80,0.90,0.02)		--	>= 80% AND < 90%		2.00%
	,('D0000117',0.90,1,0.0325)			--	>= 90% AND < 100%		3.25%
	,('D0000117',1,-1,0.04)	      		--	>= 100%					4.00%


	INSERT INTO @REWARD_MARGINS_B(Level5Code, Min_Value, Max_Value, Reward_Percentage)
	VALUES('D000001',0.80,0.90,0.02)
	,('D000001',0.90,1,0.05)
	,('D000001',1,-1,0.07)
	-- FCL COOP
	,('D0000117',0.80,0.90,0.04)
	,('D0000117',0.90,1,0.065)
	,('D0000117',1,-1,0.0725)


	
	-- Set the reward percentages based on the growth	
	UPDATE T1 
	SET SegmentA_Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
		INNER JOIN @REWARD_MARGINS_A T2
		ON T2.Level5Code = T1.Level5Code
	WHERE T1.[SegmentA_Growth_Percentage] + @ROUNDING_PERCENT >= T2.[Min_Value] AND (T2.[Max_Value] = -1 OR [SegmentA_Growth_Percentage] + @ROUNDING_PERCENT < T2.[Max_Value])

	UPDATE T1 
	SET SegmentB_Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
		INNER JOIN @REWARD_MARGINS_B T2
		ON T2.Level5Code = T1.Level5Code
	WHERE T1.[SegmentB_Growth_Percentage] + @ROUNDING_PERCENT >= T2.[Min_Value] AND (T2.[Max_Value] = -1 OR [SegmentB_Growth_Percentage] + @ROUNDING_PERCENT < T2.[Max_Value])


	-- Set the reward values for each segment
	UPDATE @TEMP
		SET SegmentA_Reward = SegmentA_Reward_Sales * SegmentA_Reward_Percentage
			,SegmentB_Reward = SegmentB_Reward_Sales * SegmentB_Reward_Percentage
			,SegmentC_Reward = SegmentC_Reward_Sales * SegmentC_Reward_Percentage
			,SegmentD_Reward = SegmentD_Reward_Sales * SegmentD_Reward_Percentage
	--WHERE SegmentA_Reward_Sales > 0 OR SegmentB_Reward_Sales > 0

	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[SegmentA_Reward] = IIF(T2.Statement_Reward_A < 0, 0, T1.SegmentA_Reward)
			,[SegmentB_Reward] = IIF(T2.Statement_Reward_B < 0, 0, T1.SegmentB_Reward)
			,[SegmentC_Reward] = IIF(T2.Statement_Reward_C < 0, 0, T1.SegmentC_Reward)
			,[SegmentD_Reward] = IIF(T2.Statement_Reward_D < 0, 0, T1.SegmentD_Reward)
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode
			--, MarketLetterCode, ProgramCode
			,SUM(SegmentA_Reward_Sales) AS Statement_Reward_A
			,SUM(SegmentB_Reward_Sales) AS Statement_Reward_B
			,SUM(SegmentC_Reward_Sales) AS Statement_Reward_C
			,SUM(SegmentD_Reward_Sales) AS Statement_Reward_D
		FROM	@TEMP
		GROUP	BY StatementCode 
	)T2
		ON T1.StatementCode = T2.StatementCode
	WHERE T2.Statement_Reward_A < 0 OR T2.Statement_Reward_B < 0 OR T2.Statement_Reward_C < 0 OR T2.Statement_Reward_D < 0


	-- Copy the records from the temporary table, into the permanent table
	DELETE FROM RP2022_DT_Rewards_W_LoyaltyBonus WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2022_DT_Rewards_W_LoyaltyBonus(StatementCode, MarketLetterCode, ProgramCode, RewardBrand
		--,SegmentA_QualSales_CY -- this column is not required, bcuz, same as SegmentA_Reward_Sales
		,SegmentA_QualSales_LY, SegmentA_QualSales_LY_INDEX
		,SegmentA_Growth_Percentage, SegmentA_Reward_Sales,SegmentA_Reward_Percentage, SegmentA_Reward
		
		--,SegmentB_QualSales_CY -- this column is not required, bcuz, same as SegmentB_Reward_Sales
		,SegmentB_QualSales_LY, SegmentB_QualSales_LY_INDEX
		,SegmentB_Growth_Percentage, SegmentB_Reward_Sales,SegmentB_Reward_Percentage, SegmentB_Reward			
		
		--,SegmentC_QualSales_LY, SegmentC_QualSales_LY_INDEX
		--,SegmentC_Growth_Percentage, SegmentC_Reward_Sales, SegmentC_Reward_Percentage, SegmentC_Reward
		--,SegmentD_QualSales_LY, SegmentD_QualSales_LY_INDEX
		--,SegmentD_Growth_Percentage, SegmentD_Reward_Sales, SegmentD_Reward_Percentage, SegmentD_Reward
		
		)

	SELECT StatementCode, @CPP_ML_CODE AS MarketLetterCode, @PROGRAM_CODE AS ProgramCode, RewardBrand
		--,SegmentA_Reward_Sales AS SegmentA_QualSales_LY
		,SegmentA_Sales_LY, SegmentA_Sales_LY_INDEX
		,SegmentA_Growth_Percentage ,SegmentA_Reward_Sales, SegmentA_Reward_Percentage, SegmentA_Reward
		
		--,SegmentB_Reward_Sales AS SegmentB_QualSales_LY
		,SegmentB_Sales_LY, SegmentB_Sales_LY_INDEX
		,SegmentB_Growth_Percentage, SegmentB_Reward_Sales, SegmentB_Reward_Percentage, SegmentB_Reward
		
		--,SegmentC_Sales_LY, SegmentC_Sales_LY_INDEX
		--,SegmentC_Growth_Percentage, SegmentC_Reward_Sales, SegmentC_Reward_Percentage, SegmentC_Reward
		--,SegmentD_Sales_LY, SegmentD_Sales_LY_INDEX
		--,SegmentD_Growth_Percentage, SegmentD_Reward_Sales, SegmentD_Reward_Percentage, SegmentD_Reward				
	FROM @TEMP


END
