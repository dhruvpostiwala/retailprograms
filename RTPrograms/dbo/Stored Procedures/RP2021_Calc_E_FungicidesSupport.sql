﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_FungicidesSupport] @SEASON INT, @STATEMENT_TYPE VARCHAR(20),  @PROGRAM_CODE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE  DataSetCode = 7874 or StatementCode = 7874
	DECLARE @SEASON INT = 2020
	DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2021_E_FUNGICIDES_SUPPORT'
	*/

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	-- Declare tables for storing data
	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY MONEY NOT NULL,
		QualBrand_Acres_CY DECIMAL(20,4) NOT NULL,
		RewardPerAcre MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY MONEY NOT NULL,
		QualBrand_Acres_CY DECIMAL(20,4) NOT NULL,
		Growth DECIMAL(20,4) NOT NULL DEFAULT 0,
		RewardPerAcre MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[DataSetCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	)

	-- Insert data from sales consolidated into table
	INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, QualBrand_Sales_CY, QualBrand_Sales_LY, QualBrand_Acres_CY)
	SELECT ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup [RewardBrand],
		   SUM(TX.Price_CY)  [QualBrand_Sales_CY],
		   SUM(TX.Price_LY1) [QualBrand_Sales_LY],
		   SUM(TX.Acres_CY)  [QualBrand_Acres_CY]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN RP_Statements RS
		ON ST.StatementCode = RS.StatementCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE RS.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup

		
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE  T1
		SET [QualBrand_Sales_CY] *= ISNULL(EI.FieldValue/100,1) 
		FROM @TEMP T1
		INNER JOIN RPWeb_USER_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
	END

	-- Determine qualification at the OU (level 2) level
	INSERT INTO @TEMP_TOTALS(DataSetCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, QualBrand_Sales_LY, QualBrand_Acres_CY)
	SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END [DataSetCode], MarketLetterCode, ProgramCode,
		   SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY],
		   SUM(QualBrand_Sales_LY) [QualBrand_Sales_LY],
		   SUM(QualBrand_Acres_CY) [QualBrand_Acres_CY]
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	UPDATE @TEMP_TOTALS SET Growth = CASE WHEN QualBrand_Sales_LY = 0 THEN CASE WHEN QualBrand_Sales_CY > 0 THEN 99.999 ELSE 0 END ELSE QualBrand_Sales_CY / QualBrand_Sales_LY END
	

	-- When retailer has < 10,000 acres of qualifying brands
	UPDATE @TEMP_TOTALS SET RewardPerAcre = 1.75 WHERE QualBrand_Acres_CY >= 2000 AND (QualBrand_Acres_CY * 1.05)  < 10000 AND Growth + @ROUNDUP_VALUE >= 1.10
	UPDATE @TEMP_TOTALS SET RewardPerAcre = 1.25 WHERE QualBrand_Acres_CY >= 2000 AND (QualBrand_Acres_CY * 1.05) < 10000 AND Growth + @ROUNDUP_VALUE >= 1.00 AND RewardPerAcre = 0

	-- When retailer has >= 10,000 acres of qualifying brands
	UPDATE @TEMP_TOTALS SET RewardPerAcre = 1.75 WHERE (QualBrand_Acres_CY * 1.05) >= 10000 AND Growth + @ROUNDUP_VALUE>= 1.00
	UPDATE @TEMP_TOTALS SET RewardPerAcre = 1.25 WHERE (QualBrand_Acres_CY * 1.05) >= 10000 AND Growth + @ROUNDUP_VALUE >= 0.90 AND RewardPerAcre = 0

	-- After qualification, set the reward per acre at the location level accordingly
	UPDATE T1
	SET T1.RewardPerAcre = T2.RewardPerAcre
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2
		ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	UPDATE T1
	SET T1.RewardPerAcre = T2.RewardPerAcre
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2
		ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0



	-- Calculate the reward using the reward per acre value and the number of acres for each location level retail
	UPDATE @TEMP
	SET Reward = RewardPerAcre * QualBrand_Acres_CY
	WHERE QualBrand_Acres_CY > 0

	-- Insert from temp table into DT table
	DELETE FROM [dbo].[RP2021_DT_Rewards_E_FungicidesSupport] WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO [dbo].[RP2021_DT_Rewards_E_FungicidesSupport](StatementCode, MarketLetterCode, ProgramCode, RewardBrand, QualBrand_Sales_CY, QualBrand_Sales_LY, QualBrand_Acres_CY, RewardPerAcre, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, QualBrand_Sales_CY, QualBrand_Sales_LY, QualBrand_Acres_CY, RewardPerAcre, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand,
		   SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY],
		   SUM(QualBrand_Sales_LY) [QualBrand_Sales_LY],
		   SUM(QualBrand_Acres_CY) [QualBrand_Acres_CY],
		   MAX(RewardPerAcre) [RewardPerAcre],
		   SUM(Reward) [Reward]
	FROM @TEMP
	WHERE DataSetCode > 0 
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode, RewardBrand
END
