﻿				 

CREATE   PROCEDURE [dbo].[RPST_Agent_ExportTotalSalesToExcel] @Scode INT

AS
BEGIN

	SET NOCOUNT ON;

	Declare @thead nvarchar(MAX) = '';
	Declare @tbody nvarchar(MAX) = '';
	Declare @exceldata nvarchar(MAX) = '';
	Declare @HTML_Data as nvarchar(max);


	DECLARE @SEASON INT

	SELECT @SEASON=Season
	FROM RPWeb_Statements
	WHERE Statementcode = @SCODE

	SELECT @thead = CONVERT(NVARCHAR(MAX),(SELECT 'Product' AS 'th',  'Product Code' AS 'th',  'In Radius' AS 'th',  'Invoice Date' AS 'th',  'Unit' AS 'th',  'Quantity' AS 'th',  'Acres' AS 'th',  'Brand' AS 'th',  'Invoice Number' AS 'th',  'SDP' AS 'th',  'SRP' AS 'th',  'SWP' AS 'th',  'BASF Season' AS 'th',  'Retailer' AS 'th',  'Retailer Code' AS 'th',  'City' AS 'th',  'Farm Code' AS 'th' FOR XML RAW('tr'), ELEMENTS, TYPE))
	/*
	SET @THEAD = '<tr>	
	<th>Product</th>
	<th>Product Code</th>
	<th>In Radius</th>
	<th>Invoice Date</th>
	<th>Unit</th>
	<th>Quantity</th>
	<th>Acres</th>
	<th>Brand</th>
	<th>Invoice Number</th>
	<th>SDP</th>
	<th>SRP</th>
	<th>SWP</th>
	<th>BASF Season</th>
	<th>Retailer</th>
	<th>Retailer Code</th>
	<th>City</th>
	</tr>'
	*/
	SELECT @tbody = CONVERT(NVARCHAR(MAX),(
		SELECT
			(SELECT  D1.product as 'td' for xml path(''), type)			
			,(SELECT '''' + D1.productcode as 'td' for xml path(''), type)
			,(SELECT  D1.InRadius as 'td' for xml path(''), type)
			,(SELECT  D1.invoicedate as 'td' for xml path(''), type)
			,(SELECT 'Quantity' as 'td' for xml path(''), type)
			,(SELECT  convert(varchar(50), D1.Quantity) as 'td' for xml path(''), type)
			,(SELECT  convert(varchar(50), D1.Acres) as 'td' for xml path(''), type)
			,(SELECT  D1.ChemicalGroup as 'td' for xml path(''), type)
			,(SELECT  D1.invoicenumber as 'td' for xml path(''), type)						
			,(SELECT  '$ ' + convert(varchar(50), CAST(D1.sdp as money), -1)  as 'td' for xml path(''), type)
			,(SELECT  '$ ' + convert(varchar(50), CAST(D1.srp as money), -1)  as 'td' for xml path(''), type)
			,(SELECT  '$ ' + convert(varchar(50), CAST(D1.swp as money), -1)  as 'td' for xml path(''), type)
			,(SELECT  D1.BASFSeason as 'td' for xml path(''), type)
			,(SELECT  D1.retailername as 'td' for xml path(''), type)
			,(SELECT  convert(varchar(50), D1.RetailerCode) as 'td' for xml path(''), type)
			,(SELECT  D1.CITY as 'td' for xml path(''), type)			
			,(SELECT  D1.Farmcode as 'td' for xml path(''), type)			
		FROM (
			SELECT
				isnull(pr.productname,tx.ProductCode) as product
				,tx.productcode
				,CONVERT(char(10), tx.invoicedate,126) as invoicedate
				,tx.invoicenumber, tx.retailercode
				,tx.quantity, tx.acres
				,tx.price_sdp as sdp, tx.price_srp as srp, tx.price_swp as swp
				,pr.ChemicalGroup, tx.BASFSeason
				,isnull(rp.retailername,'') as retailername, isnull(rp.mailingcity,'') + ',' + isnull(rp.mailingprovince,'') as CITY
				,IIF(InRadius=1,'Yes','No') as InRadius
				,tx.Farmcode
			FROM RPWeb_Transactions tx
				left join RetailerProfile RP	on RP.RetailerCode=tx.RetailerCode
				left join ProductReference PR	on PR.ProductCode=tx.Productcode
			WHERE [StatementCode]=@SCode
		) D1 
		FOR XML PATH('tr'), ELEMENTS, TYPE))

	-- do not apply string_escape on the following line
	-- '<style>br {mso-data-placement:same-cell;} mso-pattern:auto;</style>' -- For excel To Use New Line within same cell in place of <br>
	
	SET @exceldata = '<table border=1>'
	SET @exceldata += @thead + IsNull(@tbody,'<tr><td colspan=12>No Data Found</td></tr>')
	SET @exceldata += '</table>'
	
	SET @HTML_Data = @exceldata
	SET @HTML_DATA = REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') 
	IF @SEASON <= 2020
		SET @HTML_DATA = STRING_ESCAPE(@HTML_DATA, 'json')
	
	SELECT '<style>br {mso-data-placement:same-cell;} mso-pattern:auto;</style>' + @HTML_DATA AS [data]
END
