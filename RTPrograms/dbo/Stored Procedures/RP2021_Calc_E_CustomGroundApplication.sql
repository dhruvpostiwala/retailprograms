﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_E_CustomGroundApplication] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 7878 or StatementCode IN (7878)
	DECLARE @SEASON INT = 2020
	DECLARE @STATEMENT_TYPE VARCHAR(20) = 'ACTUAL'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2021_E_CUSTOM_GROUND_APP_REWARD'
	*/




	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,		
		GroupLabel VARCHAR(100) NOT NULL,
		RewardBrand_Acres NUMERIC(18,2) NOT NULL,
		RewardBrand_Acres_ContributionPercentage NUMERIC(20,4) NOT NULL DEFAULT 0,
		RewardBrand_Sales NUMERIC(18,2) NOT NULL,
		RewardBrand_CustApp NUMERIC(18,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Custom_App_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		RewardBrand_Acres NUMERIC(18,2) NOT NULL,
		RewardBrand_CustApp NUMERIC(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[DataSetCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_CA TABLE (
		DataSetCode INT NOT NULL,
		RewardBrand Varchar(100) NOT NULL,
		CA_Actual_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[RewardBrand] ASC
		)
	)



	DECLARE @Group1_RewardPerAcre MONEY = 1.00
	DECLARE @Group2_RewardPerAcre MONEY = 0.50

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN

		-- Get sales data and insert it into temp table
		INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,GroupLabel,RewardBrand_Acres,RewardBrand_Sales)
		SELECT	ST.StatementCode
				,RS.DataSetCode
				,TX.MarketLetterCode
				,TX.ProgramCode
				,PR.ChemicalGroup
				,TX.GroupLabel
				,SUM(CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN (EI.FieldValue / 100) * TX.Acres_CY ELSE TX.Acres_CY END) [RewardBrand_Acres]
				,SUM(CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN (EI.FieldValue / 100) * TX.Price_CY ELSE TX.Price_CY END) [RewardBrand_Sales]
		FROM @STATEMENTS ST				
			INNER JOIN RP_Statements RS
			ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON TX.StatementCode=ST.StatementCode
			INNER JOIN Productreference PR
			ON PR.ProductCode = tx.ProductCode
			INNER JOIN RPWeb_USER_EI_FieldValues EI
			ON ST.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
		WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType = 'REWARD_BRANDS' 
		GROUP BY ST.StatementCode,RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup, TX.GroupLabel --FOR OLD CODE



		-- Get total acres for each level 2 retailer
		INSERT INTO @TEMP_TOTALS (DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Acres)
		SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END [DataSetCode], MarketLetterCode, ProgramCode, RewardBrand,
			   SUM(RewardBrand_Acres) [RewardBrand_Acres]
		FROM @TEMP
		GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode, RewardBrand

		-- Figure out how many custom app acres there are for each brand
		UPDATE T1
		SET RewardBrand_CustApp = CASE WHEN EIV.FieldValue > T1.RewardBrand_Acres THEN T1.RewardBrand_Acres ELSE EIV.FieldValue END
		FROM @TEMP_TOTALS T1
			INNER JOIN RP_Config_EI_Fields EI
			ON (T1.RewardBrand = EI.Label OR (T1.RewardBrand = 'PROWL' AND EI.Label = 'PROWL H2O') OR (T1.RewardBrand = 'ARMEZON PRO' AND EI.Label = 'ARMEZON') OR (T1.RewardBrand = 'FRONTIER' AND EI.Label = 'FRONTIER MAX')) AND EI.ProgramCode = @PROGRAM_CODE
			INNER JOIN RPWeb_User_EI_FieldValues EIV
			ON T1.DataSetCode = EIV.StatementCode AND EI.FieldCode = EIV.FieldCode

		-- Figure out what percentage of each brand each level 1 retailer contributed to the total
		UPDATE T1
		SET RewardBrand_Acres_ContributionPercentage = CASE WHEN T2.RewardBrand_Acres = 0 THEN 0 ELSE T1.RewardBrand_Acres / T2.RewardBrand_Acres END
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T2
			ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
		WHERE T1.DataSetCode > 0

		UPDATE T1
		SET RewardBrand_Acres_ContributionPercentage = CASE WHEN T2.RewardBrand_Acres = 0 THEN 0 ELSE T1.RewardBrand_Acres / T2.RewardBrand_Acres END
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T2
			ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
		WHERE T1.DataSetCode = 0

		-- Figure out how many custom app acres each level 1 retailer gets, based on their contribution percentage to the total acres
		UPDATE T1
		SET RewardBrand_CustApp = T2.RewardBrand_CustApp * T1.RewardBrand_Acres_ContributionPercentage
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T2
			ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
		WHERE T1.DataSetCode > 0

		UPDATE T1
		SET RewardBrand_CustApp = T2.RewardBrand_CustApp * T1.RewardBrand_Acres_ContributionPercentage
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T2
			ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
		WHERE T1.DataSetCode = 0

	END --END FORECAST PROJECTION

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		
		INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,GroupLabel,RewardBrand_Acres,RewardBrand_Sales,Custom_App_Acres)
		SELECT	ST.StatementCode
				,RS.DataSetCode
				,TX.MarketLetterCode
				,TX.ProgramCode 
				,PR.ChemicalGroup as RewardBrand 
				,TX.GroupLabel 
				,SUM(ISNULL(tx.Acres_CY, 0)) AS RewardBrand_Acres
				,SUM(ISNULL(tx.Price_CY, 0)) AS RewardBrand_Sales
				,SUM(ISNULL(tx.Custom_App_Acres, 0)) AS Custom_App_Acres
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
--			INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
-- RP-3619: SHOW CUSTOM APP ACRES EVEN NO POG ACRES
			INNER JOIN (
				SELECT	StatementCode, MarketLetterCode, PRogramCode, ProductCode, GroupLabel, GroupType, IIF(ProductCode='PRD-LCAR-94NPE4',Acres_CY * 1/6,Acres_CY) as Acres_CY, Price_CY, 0 AS Custom_App_Acres
				FROM	RP2021_DT_Sales_Consolidated
				WHERE	ProgramCode = @PROGRAM_CODE AND GroupType = 'REWARD_BRANDS'

				UNION	ALL

				SELECT	CA.StatementCode, G.MarketLetterCode, G.ProgramCode, CA.ProductCode, G.GroupLabel, G.GroupType
						, 0 AS Acres_CY
						, 0 AS Price_CY
						,CA.Quantity*IIF(CA.PackageSize='Jugs',AcresPerJug,IIF(CA.PackageSize='Litres',PR1.AcresPerLitre,PR1.ConversionE)) AS Custom_App_Acres	
				FROM	RP_DT_CustomAppProducts CA
				INNER	JOIN ProductReference PR ON CA.ProductCode = PR.ProductCode
				INNER	JOIN RP_Config_Groups_Brands B ON B.ChemicalGroup = PR.chemicalGroup
				INNER	JOIN RP_Config_Groups G ON G.GroupID = B.GroupID
				INNER	JOIN (					
					SELECT PR1.Productcode, ChemicalGroup
					    --,ConversionE
						--,ConversionE/NULLIF(Jugs,0) AS AcresPerJug
						--,ConversionE/NULLIF(Volume,0) as AcresPerLitre
						,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,IIF(PR1.Product='OPTILL',PR1.ConversionE * 1/6,PR1.ConversionE)) AS ConversionE
						,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,IIF(PR1.Product='OPTILL',PR1.ConversionE * 1/6,PR1.ConversionE))/NULLIF(Jugs,0) AS AcresPerJug
						,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,IIF(PR1.Product='OPTILL',PR1.ConversionE * 1/6,PR1.ConversionE))/NULLIF(Volume,0) AS AcresPerLitre
						FROM ProductReference PR1
				) PR1			ON PR1.ProductCode = CA.ProductCode
				WHERE G.ProgramCode = @PROGRAM_CODE AND G.GroupType = 'REWARD_BRANDS'
			)TX 
				ON TX.StatementCode=ST.StatementCode
-- END OF NEW ADDITION
			INNER JOIN ProductReference PR ON PR.ProductCode = tx.ProductCode
		WHERE RS.StatementLevel='LEVEL 1'  AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType = 'REWARD_BRANDS'
		GROUP BY ST.StatementCode,RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup, TX.GroupLabel 


		--GET CUSTOM APP DATA
		--Integrity is rewarded at 40 acre/case, Not 60 acre/case
		INSERT INTO @TEMP_CA(DataSetCode,RewardBrand,CA_Actual_Acres)
		SELECT IIF(CAP.DataSetCode > 0,CAP.DataSetCode,CAP.Statementcode),PR.ChemicalGroup AS RewardBrand
					,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',AcresPerJug,IIF(cap.PackageSize='Litres',PR.AcresPerLitre,PR.ConversionE))) AS [Acres]																																			
		FROM @STATEMENTS ST
			INNER JOIN	RP_DT_CustomAppProducts CAP ON CAP.StatementCode = ST.StatementCode
			INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode AND RS.StatementLevel='LEVEL 1'
			INNER JOIN (					
				SELECT PR1.Productcode, ChemicalGroup
				    --, ConversionE
					--,ConversionE/NULLIF(Jugs,0) AS AcresPerJug
					--,ConversionE/NULLIF(Volume,0) as AcresPerLitre				
					,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,PR1.ConversionE) AS ConversionE
					,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,PR1.ConversionE)/NULLIF(Jugs,0) AS AcresPerJug
					,IIF(PR1.Product='INTEGRITY',PR1.ConversionE * 2/3,PR1.ConversionE)/NULLIF(Volume,0) as AcresPerLitre
					FROM ProductReference PR1
					INNER JOIN ProductReferencePricing PRP
					ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='EAST'
			) PR			ON PR.ProductCode=CAP.ProductCode
			GROUP BY IIF(CAP.DataSetCode > 0,CAP.DataSetCode,CAP.StatementCode), PR.ChemicalGroup

		-- FOR OUS
		INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Acres,RewardBrand_CustApp)
		SELECT 
			T1.DataSetCode,T1.MarketLetterCode,T1.ProgramCode,T1.RewardBrand,
			SUM(T1.RewardBrand_Acres) AS RewardBrand_Acres, MAX(CA.CA_Actual_Acres) AS RewardBrand_CustApp
		FROM @TEMP T1
		INNER JOIN @TEMP_CA CA ON CA.DatasetCode = T1.DatasetCode AND CA.RewardBrand = T1.RewardBrand
		WHERE T1.DatasetCode > 0
		GROUP BY T1.DataSetCode, T1.MarketLetterCode, T1.ProgramCode,T1.RewardBrand

		--MATCHING
		--FOR STANDALONES the datasetcode will be the statementcode so we can join on that condition
		UPDATE T1 
		SET RewardBrand_CustApp = IIF(CA_Actual_Acres > RewardBrand_Acres, RewardBrand_Acres, CA_Actual_Acres)
		FROM @TEMP T1
			INNER JOIN RP_Statements RS	ON RS.StatementCode = T1.StatementCode
			INNER JOIN @TEMP_CA CA		ON CA.DataSetCode = T1.StatementCode AND T1.RewardBrand = CA.RewardBrand
		WHERE RS.StatementLevel = 'LEVEL 1' AND CA_Actual_Acres > 0 AND T1.DatasetCode = 0

		--FOR OUS MATCHING
		UPDATE T1
		SET RewardBrand_CustApp =  IIF(T2.RunningTotal <= T3.RewardBrand_CustApp
			   ,IIF(T1.RewardBrand_Acres > T3.RewardBrand_CustApp,T3.RewardBrand_CustApp,T1.RewardBrand_Acres)
			   ,T1.RewardBrand_Acres+(T3.RewardBrand_CustApp-T2.[RunningTotal]))
		FROM @TEMP T1
			INNER JOIN @TEMP_TOTALS T3 ON T3.DatasetCode = T1.DatasetCode AND t1.RewardBrand = t3.RewardBrand
			INNER JOIN (
						SELECT StatementCode, RewardBrand
						,SUM(RewardBrand_Acres) OVER(PARTITION BY DataSetCode, RewardBrand ORDER BY RewardBrand_Acres DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
						FROM @TEMP
						WHERE DataSetCode > 0
			) T2
		ON T2.StatementCode=T1.StatementCode  AND T2.RewardBrand=T1.RewardBrand 
		WHERE T1.RewardBrand_Acres > 0 AND (T2.[RunningTotal] <= T3.RewardBrand_CustApp OR T1.RewardBrand_Acres+(T3.RewardBrand_CustApp - T2.[RunningTotal]) > 0) --NEW LOGIC
		AND T3.DatasetCode > 0

	END





	-- Calculate the Reward Amount / Brand
	UPDATE @TEMP SET Reward = RewardBrand_CustApp * CASE GroupLabel WHEN 'REWARD GROUP 1' THEN @Group1_RewardPerAcre ELSE @Group2_RewardPerAcre END
	WHERE RewardBrand_CustApp > 0

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2021_DT_Rewards_E_CustomGroundApplication WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)

	INSERT INTO [dbo].[RP2021_DT_Rewards_E_CustomGroundApplication](Statementcode, MarketLetterCode, ProgramCode, RewardBrand, GroupLabel, RewardBrand_Acres, RewardBrand_Sales, RewardBrand_CustApp, Reward, Custom_App_Acres)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, GroupLabel, RewardBrand_Acres, RewardBrand_Sales, RewardBrand_CustApp, Reward, Custom_App_Acres
    FROM @TEMP
		
      UNION

    SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, GroupLabel
		,SUM(RewardBrand_Acres) AS RewardBrand_Acres
        ,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(RewardBrand_CustApp) AS RewardBrand_CustApp
        ,SUM(Reward) AS Reward
		,SUM(Custom_App_Acres) AS Custom_App_Acres
    FROM @TEMP
    WHERE DataSetCode > 0
    GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand, GroupLabel
END