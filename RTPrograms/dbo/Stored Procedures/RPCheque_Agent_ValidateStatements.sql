﻿


CREATE PROCEDURE [dbo].[RPCheque_Agent_ValidateStatements] @SEASON INT,  @USERNAME VARCHAR(150),  @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @EPSILON DECIMAL(4,4)=dbo.SVF_GetEpsilon()		

	/* Locked statement codes are passed */
	
	DECLARE @STATEMENTS TABLE(
		[StatementCode] [int] NOT NULL,		
		[Result] [varchar](50) NULL,
		[Reason] [varchar](300) NULL,
		PRIMARY KEY CLUSTERED 
		(
		 [StatementCode] ASC
		)WITH (IGNORE_DUP_KEY = OFF)
	)

	BEGIN TRY

	INSERT INTO @STATEMENTS(STATEMENTCODE)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')

	UPDATE T1
	SET T1.Result='Validation Failed',Reason='SQL Validation: Recieves Payment flag for this Location is set to No' 	
	FROM @STATEMENTS T1
		LEFT JOIN RPWeb_Statements ST
		ON ST.[StatementCode]=T1.[StatementCode] -- Locked Statements
	WHERE ISNULL(ST.IsPayoutStatement,'No') <> 'Yes'
		
	/*
		Make Sure 
			Statement is in 'Ready For Payment' status 
			Locked and Unlocked Statements are in active status
	*/

	UPDATE T1
	SET T1.Result='Validation Failed',Reason='SQL Validation: NOT active Or Unlocked Or NOT Ready for Payment' 	
	FROM @STATEMENTS T1
		LEFT JOIN RPWeb_Statements LK_ST			ON LK_ST.[StatementCode]=T1.[StatementCode] -- Locked Statements
		LEFT JOIN RPWeb_Statements UL_ST			ON UL_ST.[StatementCode]=LK_ST.[SRC_StatementCode] -- Unlocked Statements 		  
		LEFT JOIN RPWeb_StatementInformation SI		ON SI.[StatementCode]=LK_ST.[SRC_StatementCode] AND SI.[FieldName]='STATUS'
	WHERE COALESCE(T1.[RESULT],'')='' 
		AND (COALESCE(LK_ST.[Status],'') <> 'Active' OR COALESCE(LK_ST.[VersionType],'') <> 'LOCKED' OR COALESCE(UL_ST.[Status],'')<>'Active' OR COALESCE(SI.[FieldValue],'')<>'Ready for Payment')
	
	/* Make sure no active cheques/etf exists for the statement (same season) */
	UPDATE T1	
	SET [Result]='Validation Failed', Reason='SQL Validation: Active cheque exists'	
	FROM @STATEMENTS T1
		INNER JOIN (
			SELECT DISTINCT ST.[SRC_StatementCode]  AS StatementCode
			FROM RPWeb_Statements ST
				INNER JOIN RPWeb_Cheques CHQ 	
				ON CHQ.[StatementCode]=ST.[StatementCode]				
			WHERE CHQ.[ChequeType] IN ('cheque','ETF') AND chq.[ChequeStatus]  IN ('New','Ready To Print','Printed') 			
		) T2
		ON T2.StatementCode=T1.StatementCode
	WHERE COALESCE(T1.[RESULT],'')='' 
			

	/* To process a cheque/ETF minimum payment of $50 is required */
	UPDATE T1
	SET Result='Validation Failed', Reason='SQL Validation: Reward Amount or Current Payment amount is less than $50.00'	
	FROM @STATEMENTS T1
		INNER JOIN (
			SELECT StatementCode , Sum(TotalReward) AS TotalReward, Sum(CurrentPayment) AS CurrentPayment
			FROM RPWeb_Rewards_Summary
			WHERE [StatementCode] < 0
			GROUP BY StatementCode			
		) T2
		ON T2.StatementCode=T1.StatementCode 
	WHERE COALESCE([RESULT],'')='' AND T2.CurrentPayment + @EPSILON < 50
		--AND ( T2.TotalReward + @EPSILON < 50 OR T2.CurrentPayment + @EPSILON < 50)

					
	/* There are outstanding issues for this retailer 
	Setup for issues in not setup , probably will be implemented differently. But leaving this code here just in case
	UPDATE T1
	SET [Result]='Validation Failed', Reason='SQL Validation: Unlocked Version has an unresolved issue.'
	FROM @STATEMENTS T1
		INNER JOIN RPWeb_Statements T2			ON T1.[StatementCode]=t2.[StatementCode]
		INNER JOIN RPWeb_IssueStatements iss	ON iss.[StatementCode]=t2.[SRC_StatementCode]
	WHERE iss.[Status]='Unresolved'
	*/

	/* Inactive Retailers */
	UPDATE T1
	SET [Result]='Validation Failed', Reason='SQL Validation: Retailer status is neither Active nor Sold. (Status:  ' + COALESCE(RP.[Status],'Not Found') + ')'
	FROM @STATEMENTS T1
		INNER JOIN RPWeb_Statements RS		ON RS.[StatementCode]=T1.[StatementCode] 
		LEFT JOIN RetailerProfile RP		ON RP.[Retailercode]=RS.[RetailerCode] 
	WHERE COALESCE([Result],'')='' AND  COALESCE(RP.[Status],'Not Found') NOT IN ('Active','Sold')


	/* UPDATE ALL REMAINING STATEMENST AS PASSED */
	UPDATE @STATEMENTS 
	SET [Result]='Validation Passed'
	WHERE COALESCE([RESULT],'')=''

	END TRY

	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS [Error_Message] ,'' AS warning_message
		RETURN;	
	END CATCH
	

	/*DELETE CHEQUEVALIDATION FLAGS */
	DELETE RPWeb_StatementInformation WHERE [FieldName]='CreateChequeStatus' AND [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)
	
	/* UPDATE ChequeCreationStatus FLAG */
	INSERT INTO RPWeb_StatementInformation([StatementCode],[FieldName],[FieldValue],[Comments]) 
	SELECT [StatementCode]
		,'CreateChequeStatus' AS [FieldName]
		,[Result] AS [FieldValue]
		,IIF([Result]='Validation Failed', Reason, NULL) AS [Comments]
	FROM @STATEMENTS
	
	SELECT 'success' AS [Status]
		,'' AS [Error_Message]
		,'' AS confirmation_message
		,'' AS warning_message
		,SUM(CASE WHEN [RESULT]='Validation Passed' THEN 1 ELSE 0 END) AS [PassCount]
		,SUM(CASE WHEN [RESULT]='Validation Failed' THEN 1 ELSE 0 END) AS [FailCount]
	FROM @STATEMENTS	


END



