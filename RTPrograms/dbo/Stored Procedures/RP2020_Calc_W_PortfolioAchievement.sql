﻿

CREATE  PROCEDURE [dbo].[RP2020_Calc_W_PortfolioAchievement]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	/*
		Portfolio Achievement Bonus: Growth is calculated by dividing 2020 POG Sales by the 2019 POG Sales.
		Qualify on CCP
		Reward on CCP
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_PORFOLIO_ACHIEVEMENT'

	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		GroupLabel VARCHAR(100) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,		
		RewardBrand_Sales Money NOT NULL,
		QualBrand_Sales_CY Money NOT NULL,
		QualBrand_Sales_LY Money NOT NULL,						
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,		
		Reward Money NOT NULL DEFAULT 0,
		Growth Decimal(7,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC,
			[Level5Code] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		RewardBrand_Sales Money NOT NULL,
		QualBrand_Sales_CY Money NOT NULL,
		QualBrand_Sales_LY Money NOT NULL,
		Growth NUMERIC(7,4) NOT NULL DEFAULT 0,
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[Level5Code] ASC
		)
	)

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Level5Code,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.Level5Code, SUM(tx.Price_CY) AS RewardBrand_Sales, SUM(tx.Price_CY) AS QualBrand_Sales_CY, SUM(tx.Price_LY1) AS QualBrand_Sales_LY
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.Level5Code

	-- Determine growth based on the sum of brand sales
	INSERT INTO @TEMP_TOTALS(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY)
	SELECT StatementCode, MarketLetterCode, ProgramCode, Level5Code, SUM(RewardBrand_Sales) AS RewardBrand_Sales, SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY, SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
	FROM @TEMP
	GROUP BY StatementCode, MarketLetterCode, ProgramCode, Level5Code

	/*
	NEW: RC-3538 - discovered that the reward percentages are different for Indep West vs Coops
	Growth in 2020 relative to the 2-year average	Reward on 2020 POG sales of InVigor
				
				Indep West	Coops
	115%+		7%			6%
	110-114.9%	6.5%		5.5%
	95- 109.9%	6%			5%
	90-94.9%	5%			4%
	85-89.9%	4%			3%
	<85%		3%			2%
	*/

	-- Determine growth on total sales even though we are storing the data at brand level sales
	UPDATE @TEMP_TOTALS
	SET Growth = QualBrand_Sales_CY / QualBrand_Sales_LY
	WHERE QualBrand_Sales_LY > 0

	-- Update the reward %
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.07 WHERE (Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth+@Roundup_Value >= 1.15
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.065 WHERE [Reward_Percentage] = 0 AND Growth+@Roundup_Value >= 1.1
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.06 WHERE [Reward_Percentage] = 0 AND Growth+@Roundup_Value >= 0.95
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.05 WHERE [Reward_Percentage] = 0 AND Growth+@Roundup_Value >= 0.9
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.04 WHERE [Reward_Percentage] = 0 AND Growth+@Roundup_Value >= 0.85  
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.03 WHERE [Reward_Percentage] = 0 AND Growth <> 0 

	--UPDATE @TEMP_TOTALS 	SET [Growth]=0.85 	WHERE [Growth] < 0.85  AND [Reward_Percentage]=0.04

	-- NEW: RC-3538 - Coops reward percentage is 1% less
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = [Reward_Percentage] - 0.01 WHERE [Level5Code] = 'D0000117' AND [Reward_Percentage] > 0
	   
	-- Update the growth % and reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode

	-- Update reward
	UPDATE @TEMP SET [Reward] = RewardBrand_Sales * Reward_Percentage

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
			
	DELETE FROM RP2020_DT_Rewards_W_PortfolioAchievement WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_PortfolioAchievement(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY, Reward_Percentage, Reward
	FROM @TEMP
	
END


