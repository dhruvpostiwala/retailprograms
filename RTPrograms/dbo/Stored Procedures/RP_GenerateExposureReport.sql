﻿
CREATE PROCEDURE [dbo].[RP_GenerateExposureReport] @SEASON INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @STATUS AS VARCHAR(20)='';
	DECLARE @ExposureID INT;
	DECLARE @DATETIME DATETIME=GETDATE();
	DECLARE @SEASON_STRING VARCHAR(4)=CAST(@SEASON AS VARCHAR(4))

	-- IF @SEASON >= 2021
	--	RETURN

	BEGIN TRY
	   BEGIN TRANSACTION
		
		INSERT INTO RPWeb_Reports_Exposure(Season, ReportDate)
		VALUES(@SEASON, @DATETIME)
		
		SELECT @EXPOSUREID=SCOPE_IDENTITY()

		-- STEP #1 ACCRUAL SUMMARY
		INSERT INTO RPWeb_Reports_Exposure_AccrualSummary(ExposureID,Region,MarketLetterCode,RewardCode,AccrualAmount)
		SELECT @EXPOSUREID,Region,MarketLetterCode,RewardCode,AccrualAmount
		FROM RP_Config_Accrual
		WHERE Season=@SEASON
			
	
		-- STEP #2 DISTRIBUTOR LEVEL SUMMARY
		INSERT INTO RPWeb_Reports_Exposure_L5_Summary(ExposureID,Level5Code,RetailerName,MarketLetterCode,RewardCode,ExposureType,RewardAmount,CurrentPayment,PaidToDate,NextPayment,Claims,AveragePayment)
		 SELECT @ExposureID
			,exposure.Level5Code
			,UPPER(RP.RetailerName) AS RetailerName
			,ISNULL(acc.[ALT_MarketLetterCode],acc.[MarketLetterCode]) AS MarketLetterCode
			,acc.[RewardCode]
			,CASE WHEN RIGHT(acc.[RewardCode],1)='X' THEN 'EXCEPTION' ELSE 'PROGRAM' END AS [ExposureType]
			,ISNULL([RewardValue],0) AS [RewardAmount]		
			,ISNULL([CurrentPayment],0) AS [RewardPayment]	
			,ISNULL([PaidToDate],0) AS [PaidToDate]
			,ISNULL([NextPayment],0) as [NextPayment]
			,ISNULL([Claims],0) AS [Claims]
			,ISNULL([AveragePayment],0) AS [AveragePayment]
		FROM RP_Config_Accrual acc	
	   		  INNER JOIN(			
				SELECT st.[Level5Code],[MarketLetterCode],[RewardCode]
					,SUM([Rewardvalue]) AS [RewardValue]
					,SUM([CurrentPayment]) AS [CurrentPayment]
					,SUM([PaidToDate]) AS [PaidToDate]
					,SUM([NextPayment]) AS [NextPayment]
					,AVG(CASE WHEN [RewardValue]<>0 THEN [RewardValue] END) AS [AveragePayment]
					,COUNT(DISTINCT CASE WHEN [RewardValue]<>0 THEN rs.[StatementCode] END) AS [Claims]
				FROM RPWeb_Statements ST								
					INNER JOIN(
						SELECT [StatementCode],[MarketLetterCode],[RewardCode],[RewardValue],[CurrentPayment],[PaidToDate],[NextPayment]
						FROM RPWeb_Rewards_Summary 
						WHERE ([RewardEligibility] = 'ELIGIBLE' OR [PaidToDate] != 0) 						
					)rs		
					ON rs.[StatementCode]=st.[StatementCode]
				WHERE ST.[Season]=@Season AND ST.[Status]='Active' AND ST.[VersionType]='Unlocked' 
					AND ST.StatementType='ACTUAL'	
					AND ST.IsPayoutStatement = 'Yes'				
				GROUP BY ST.[Level5code],[MarketLetterCode],[RewardCode]
			) exposure
			ON exposure.MarketLetterCode=acc.MarketLetterCode AND exposure.[RewardCode]=acc.[RewardCode]
			LEFT JOIN RetailerProfile RP	
			ON RP.RetailerCode=exposure.Level5Code AND RP.RetailerType='LEVEL 5'
		WHERE acc.[Season]=@SEASON  
		 --AND acc.[Status]='Active'

		UPDATE TGT
		SET [RewardAmount]=SRC.[RewardAmount]
			,[PaidToDate]=SRC.[PaidToDate]
			,[CurrentPayment]=SRC.[CurrentPayment]
			,[NextPayment]=SRC.[NextPayment]
		FROM RPWeb_Reports_Exposure_AccrualSummary TGT
			INNER JOIN (
				SELECT ExposureID, MarketLetterCode, RewardCode
					,SUM(RewardAmount) AS [RewardAmount]
					,SUM(PaidToDate) AS [PaidToDate]
					,SUM(CurrentPayment) AS [CurrentPayment]
					,SUM(NextPayment) AS [NextPayment]
				FROM RPWeb_Reports_Exposure_L5_Summary
				WHERE ExposureID=@ExposureID
				GROUP BY ExposureID, MarketLetterCode, RewardCode
			) SRC
			ON SRC.ExposureID=TGT.ExposureID AND SRC.MarketLetterCode=TGT.MarketLetterCode AND SRC.RewardCode=TGT.RewardCode

			COMMIT TRAN -- Transaction Success!
	END TRY

	BEGIN CATCH		
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN --RollBack in case of Error

			-- you can Raise ERROR with RAISEERROR() Statement including the details of the exception
			--RAISERROR(ERROR_MESSAGE(), ERROR_SEVERITY(), 1)
			SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;
	END CATCH
	   
	
	SELECT 'success' as status
	

END




