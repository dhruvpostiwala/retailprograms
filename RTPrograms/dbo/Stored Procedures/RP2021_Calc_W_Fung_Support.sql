﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_Fung_Support] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @PROGRAM_CODE VARCHAR(25) = 'RP2021_W_FUNG_SUPPORT'
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'Projection'
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS VALUES (5252)

	-- The level 5 codes for West Independents and West Co-ops
	DECLARE @WEST_IND VARCHAR(20) =  [dbo].[SVF_GetDistributorCode] ('WEST_RC');
	DECLARE @WEST_COOP VARCHAR(20) = [dbo].[SVF_GetDistributorCode] ('WEST_COOP');

	DECLARE @CARAMBA_REWARD MONEY = 10.00;
	DECLARE @COTEGRA_REWARD MONEY = 20.00;

	DECLARE @TEMP TABLE 
	(
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		ProductName VARCHAR(255) NOT NULL,
		ProductCode VARCHAR(50) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		RewardBrand_QTY NUMERIC(18,2) DEFAULT 0,
		Reward_Per_Quantity INT DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC,
			[ProductName] ASC,
			[ProductCode] ASC
		)
	)

	-- 1. Caramba - Reward $0.50/acre on all Projected purchases of Caramaba POG.
	-- 2. Cotegra - Reward $0.50/acre on all Projected purchases of Cotegra POG.
	-- Store Total Acres in the Reward_Per_Quantity column
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- GET SALES DATA
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,ProductName,ProductCode,Reward_Per_Quantity,RewardBrand_QTY, RewardBrand_Sales)
		SELECT	ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName,TX.ProductCode,
			SUM(TX.Acres_CY), -- store total Acres instead
			SUM(TX.Acres_CY/PR.ConversionW) AS RewardSales_QTY,
			SUM(TX.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
		WHERE TX.ProgramCode = @PROGRAM_CODE AND tx.GroupType = 'REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName, TX.ProductCode

		UPDATE @TEMP SET [Reward] = [Reward_Per_Quantity] * 0.5 WHERE [Reward_Per_Quantity] > 0
	END

	-- SET THE REWARD AMOUNT
	IF @STATEMENT_TYPE IN ('ACTUAL')
	BEGIN
		-- GET SALES DATA
		/*
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,ProductName,ProductCode,Reward_Per_Quantity,RewardBrand_QTY, RewardBrand_Sales)
		SELECT	ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName, TX.ProductCode,
			PR.LEUConversion*IIF(TX.GroupLabel = 'CARAMBA', @CARAMBA_REWARD, @COTEGRA_REWARD) AS Reward_Per_Quantity,
			SUM(TX.Acres_CY/PR.ConversionW) AS RewardSales_QTY,
			SUM(TX.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
		WHERE TX.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName,
		TX.ProductCode,PR.LEUConversion*IIF(TX.GroupLabel = 'CARAMBA', @CARAMBA_REWARD, @COTEGRA_REWARD)

		UPDATE @TEMP SET [Reward] = [RewardBrand_QTY]*[Reward_Per_Quantity] WHERE [RewardBrand_QTY] > 0
		*/

		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,ProductName,ProductCode,RewardBrand_QTY,RewardBrand_Sales,Reward_Per_Quantity)
		SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName, TX.ProductCode,			
			SUM(TX.Acres_CY/PR.ConversionW) AS RewardSales_QTY,
			SUM(TX.Price_CY) AS RewardBrand_Sales,
			MAX(PR.LEUConversion * IIF(PR.ChemicalGroup='CARAMBA',@CARAMBA_REWARD,@COTEGRA_REWARD)) AS Reward_Per_Quantity
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
		WHERE TX.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, PR.ProductName,TX.ProductCode


		UPDATE @TEMP 
		SET [Reward] = [RewardBrand_QTY] * Reward_Per_Quantity
		WHERE [RewardBrand_QTY] > 0
	END

	DELETE FROM RP2021_DT_Rewards_W_Fung_Support WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2021_DT_Rewards_W_Fung_Support(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, ProductName, ProductCode, RewardBrand_QTY, Reward_Per_Quantity, Reward, RewardBrand_Sales)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, ProductName, ProductCode, RewardBrand_QTY, Reward_Per_Quantity, Reward, RewardBrand_Sales
	FROM @TEMP

END
