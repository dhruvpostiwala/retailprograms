﻿

CREATE PROCEDURE [dbo].[RPST_Agent_Obsolete_Exceptions]  @ExceptionID INT, @REGION VARCHAR(4), @EXCEPTION_LEVEL VARCHAR(20), @USERNAME VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;
	--DEMAREY BAKER
	--APRIL 4 2021
	BEGIN TRY

	BEGIN TRANSACTION

	IF @EXCEPTION_LEVEL = 'LEVEL 1'
	BEGIN

		UPDATE [RPWeb_Exceptions] 
		SET Status = 'Obsolete' 
		OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		WHERE ID = @ExceptionID 


		--FIRST SCENARIO EXCEPTION CODE IS THE SAME SO EASY DELETE 
		--BOTH EAST AND WEST
		--LOCKED STATEMENTS						
		DELETE RS FROM
		RPWeb_Rewards_Summary RS
		INNER JOIN [RPWeb_Exceptions] EX ON EX.StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode
		AND EX.RewardCode NOT IN ('OTHER_E_X','OTHER_W_X','OTHER_EX','OTHER_WX') AND EX.Status = 'Obsolete'
		WHERE EX.ID = @ExceptionID

		--UNLOCKED STATEMENTS
		DELETE RS 
		FROM RPWeb_Rewards_Summary RS
		INNER JOIN [RPWeb_Exceptions] EX ON EX.SRC_StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode
		AND EX.RewardCode NOT IN ('OTHER_E_X','OTHER_W_X','OTHER_EX','OTHER_WX')  AND EX.Status = 'Obsolete'
		WHERE EX.ID = @ExceptionID

		
		----------------------SECOND SCENARIO SINCE STATEMENTS CAN HAVE MULTIPLE OTHER EXCEPTIONS----------------------------
		--LOCKED RewardSSummary
		UPDATE RS 
		SET
				TotalReward -= EX.Exception_Value,
				RewardValue -=  EX.Exception_Value,
				CurrentPayment -= EX.Exception_Value
		FROM RPWeb_Rewards_Summary RS
		INNER JOIN (
			Select StatementCode,RewardCode, SUM(Exception_Value) Exception_Value
			FROM [RPWeb_Exceptions]
			WHERE RewardCode IN ('OTHER_E_X','OTHER_W_X','OTHER_EX','OTHER_WX') AND Status = 'Obsolete'
			AND ID = @ExceptionID
			AND Exception_Level = 'LEVEL 1'
			GROUP BY StatementCode,RewardCode
		) EX ON EX.StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode
	
		----UNLOCKED RewardSummary
		UPDATE RS 
		SET		TotalReward -= EX.Exception_Value,
				RewardValue -=  EX.Exception_Value,
				CurrentPayment -= EX.Exception_Value
		FROM RPWeb_Rewards_Summary RS
		INNER JOIN (
			Select Src_StatementCode StatementCode,RewardCode, SUM(Exception_Value) Exception_Value
			FROM [RPWeb_Exceptions]
			WHERE RewardCode IN ('OTHER_E_X','OTHER_W_X','OTHER_EX','OTHER_WX') AND Status = 'Obsolete'
			AND Exception_Level = 'LEVEL 1'
			AND ID = @ExceptionID
			GROUP BY Src_StatementCode,RewardCode
		)EX ON EX.StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode

		IF UPPER(@REGION) = 'EAST'
		BEGIN

		
		IF OBJECT_ID('tempdb..#OU_OTHER_EX') IS NOT NULL DROP TABLE #OU_OTHER_EX

		SELECT ID,StatementCode,RewardCode,Show_IN_UL,VersionType,Src_StatementCode, Exception_Value, [Status]
		INTO #OU_OTHER_EX
		FROM RpWeb_exceptions
		WHERE RewardCode IN ('OTHER_E_X') AND Status = 'Approved' AND Exception_Level = 'LEVEL 2' AND SRC_ID = 0 
		AND StatementCode = (
				SELECT datasetcode AS StatementCOde 
				FROM RPWeb_exceptions E 
				INNER JOIN RPweb_Statements ST ON  ST.StatementCode = E.StatementCode 
				WHERE Id  = @ExceptionID
		)

		--UPDATE LOCKED STATEMENT SUMMARY 
		UPDATE Rew
		SET		TotalReward = RS_OU.TotalReward,
				RewardValue =  RS_OU.RewardValue,
				CurrentPayment = RS_OU.CurrentPayment
		FROM 
		RPWeb_Rewards_Summary Rew
		INNER JOIN
		(
			SELECT
			DatasetCode as StatementCode
			,RewardCode AS RewardCode
			,SUM(TotalReward) TotalReward
			,SUM(RewardValue) RewardValue
			,SUM(CurrentPayment)CurrentPayment
			FROM RPWeb_Rewards_summary RS
			INNER JOIN
			(
				SELECT ST.StatementCode,ST.DataSetCode from RPWeb_Statements ST  
				INNER JOIN #OU_OTHER_EX OU ON OU.StatementCode = ST.DataSetCode
			) L1 ON L1.StatementCode = RS.StatementCode
			AND RS.RewardCode = 'OTHER_E_X'
			GROUP BY DataSetCode,RewardCode
		) RS_OU ON RS_OU.StatementCode = Rew.StatementCode  AND RS_OU.RewardCode = Rew.RewardCode

		--UPDATE UNLOCKED STATEMENT SUMMARY 
		UPDATE RS_U
		SET		TotalReward = RS_L.TotalReward,
				RewardValue =  RS_L.RewardValue,
				CurrentPayment = RS_L.CurrentPayment
		FROM 
		RPweb_Rewards_Summary RS_U
		INNER JOIN RPWeb_Statements S ON S.StatementCode = RS_U.StatementCode AND VersionType = 'Unlocked' AND StatementType = 'Actual' AND Status = 'Active'
		INNER JOIN
		(
			SELECT TotalReward,RewardValue,CurrentPayment, SRC_StatementCode 
			FROM RPweb_Rewards_Summary L 
			INNER JOIN #OU_OTHER_EX OU ON OU.StatementCode = L.StatementCode AND OU.RewardCode = L.RewardCode
		)RS_L ON RS_L.Src_StatementCode = S.StatementCode 
		WHERE  RS_U.RewardCode = 'OTHER_E_X'
		
		
	
		UPDATE E
			SET Status = 'Obsolete',
			DateModified = GETDATE()
		OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPweb_Exceptions E
		INNER JOIN #OU_OTHER_EX OU ON OU.StatementCode = E.StatementCode AND OU.RewardCode = E.RewardCode AND E.[Status] = OU.[Status]
		WHERE E.RewardCode = 'OTHER_E_X' AND Exception_level = 'LEVEL 2' AND Src_Id = 0


		--SET PREVIOUSLY ENTERED OU EXCEPTION TO Obsolete AND THEN CREATE NEW ONE AT LEVEL 2
		INSERT INTO [RPWeb_Exceptions] ([RetailerCode], [StatementCode], [Season], [RewardCode], [Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value], [Exception_Reward_Margin], [Status], [Src_StatementCode], [Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy],Show_IN_UL,VersionType)  		
		OUTPUT inserted.[StatementCode],inserted.ID,'Create new other OU exception removing Obsolete values'
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
		SELECT S.RetailerCode,R.StatementCode,S.Season, R.RewardCode,'LEVEL 2','Other Exceptions', 'OTHER',R.TotalReward,0,'Approved',OB.Src_StatementCode,0,'OU OTHER EXCEPTION ROLLED UP',GETDATE(),GETDATE(),@USERNAME,OB.Show_in_Ul,OB.VersionType
		FROM RPWeb_Rewards_Summary R
		INNER JOIN RPWeb_Statements S ON S.StatementCode = R.StatementCode AND S.REGION =  'EAST'
		INNER JOIN #OU_OTHER_EX OB ON OB.StatementCode = R.StatementCode AND OB.RewardCode = R.RewardCode AND R.TotalReward <> OB.Exception_Value
		WHERE	R.RewardCode = 'OTHER_E_X'
				AND S.VersionType = 'Locked' AND S.Status = 'Active'
				AND StatementLevel = 'LEVEL 2'
				AND R.TotalReward > 0
			
		END --END REGION




		EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY
		
	END --END EXCEPTION LEVEL

	ELSE IF UPPER(@REGION) = 'EAST' AND UPPER(@EXCEPTION_LEVEL) = 'LEVEL 2'
	BEGIN
		
		UPDATE [RPWeb_Exceptions] 
		SET Status = 'Obsolete' 
		OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID
		
		
		--LOCKED STATEMENTS						
		DELETE RS FROM
		RPWeb_Rewards_Summary RS
		INNER JOIN [RPWeb_Exceptions] EX ON EX.StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode
		AND EX.RewardCode <> 'OTHER_E_X' AND EX.Status = 'Obsolete'
		WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID
		
		--UNLOCKED STATEMENTS
		DELETE RS 
		FROM RPWeb_Rewards_Summary RS
		INNER JOIN [RPWeb_Exceptions] EX ON EX.SRC_StatementCode = RS.StatementCode AND Ex.RewardCode = RS.RewardCode
		AND EX.RewardCode <> 'OTHER_E_X' AND EX.Status = 'Obsolete'
		WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID

	END  --END  REGION AND LEVEl
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
			ROLLBACK TRANSACTION
			RETURN;	
	END CATCH	
END

