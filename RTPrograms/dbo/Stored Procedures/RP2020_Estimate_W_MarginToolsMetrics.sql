﻿





CREATE PROCEDURE [dbo].[RP2020_Estimate_W_MarginToolsMetrics]  @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON

/*DECLARE @SEASON INT = 2020
	--DECLARE @REGION varchar (4) = 'West'
	DECLARE  @STATEMENTS AS UDT_RP_STATEMENT */


--MarketLetterCode
	DECLARE @Region VARCHAR(4)='WEST'
	DECLARE @Region_Key VARCHAR(1)=LEFT(@REGION,1);

	DECLARE @INV_ML AS VARCHAR(50)=				'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_INV'				--InVigor Hybrids
	DECLARE @CCP_ML AS VARCHAR(50)=				'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_CCP'				--Canola Crop Protection
	DECLARE @CER_PUL_SB_ML AS VARCHAR(50)=		'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_CER_PUL_SB'		--Cereal, Pulse and Soybean Crop Protection
	DECLARE @ST_INC_ML AS VARCHAR(50)=			'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_ST_INC'			--Seed Treatment and Inoculants
	DECLARE @LIB200_ML AS VARCHAR(50)=			'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_LIBERTY_200'		--Liberty 200
	DECLARE @OTHERREWARDS_ML AS VARCHAR(50)=	'ML' + CONVERT(VARCHAR,@SEASON) + '_' + @Region_Key +'_ADD_PROGRAMS'	--OTHER REWARDS  


	--DECLARE @TARGET_STATEMENTS AS UDT_RP_STATEMENT 
	DECLARE @TARGET_STATEMENTS AS TABLE(
		Statementcode INT NOT NULL,
		Locked_Statementcode INT NOT NULL
	)


	DECLARE @Item_Code TABLE (
		Item_Code VARCHAR(50) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL
	);

	DECLARE @TEMP_TABLE TABLE(
		Statementcode INT NOT NULL,		
		Locked_Statementcode VARCHAR(50) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		Item_Code VARCHAR(50) NOT NULL,
		Item_Label VARCHAR(200) NOT NULL DEFAULT '',		
		Item_Value DECIMAL(18,4) NULL DEFAULT 0		
	)

	DECLARE @ML_POG TABLE(
		Statementcode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		POG_Amount DECIMAL(18,4) NOT NULL DEFAULT 0,
		PY_POG_Amount DECIMAL(18,4) NOT NULL  DEFAULT 0,
		Reward_Amount Money NOT NULL  DEFAULT 0,
		Ratio DECIMAL(18,4) NOT NULL DEFAULT 0
	  )
	  
	DECLARE @STATEMENT_MAPPINGS TABLE (
		Statementcode INT,
		RetailerCode VARCHAR(20)
	)
		
	-- GET UNLOCKED STATEMENTCODES HAVING ACTIVE LOCKED STATEMENTS
	INSERT INTO @TARGET_STATEMENTS(Statementcode, Locked_Statementcode)
	SELECT ST.Statementcode, LCK.Statementcode
	FROM @STATEMENTS ST
		INNER JOIN RPWeb_Statements LCK ON LCK.SRC_Statementcode=ST.statementcode		
	WHERE LCK.[Status]='Active'  AND LCK.StatementType='Actual' and LCK.Season=@Season and LCK.region = @Region AND LCK.VersionType='Locked' 
		AND LCK.ChequeRunName='OCT RECON'
		AND LCK.Level5Code='D000001'
		
	IF NOT EXISTS(SELECT * FROM @TARGET_STATEMENTS)
		RETURN
	
	INSERT INTO @STATEMENT_MAPPINGS(StatementCode, RetailerCode)
	SELECT MAP.StatementCode, MAP.RetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN @TARGET_STATEMENTS ST	
		ON ST.Statementcode=MAP.Statementcode


	-- LETS INSERT ALL REQUIRED METRICS 
	-- STEP #1 - INSERT ALL REQUIRED METRICS 
	INSERT INTO  @Item_Code (Item_Code, MarketLettercode)
	VALUES ('CY_ML_Margin','')	-- Overall Margin TotalReward/Total Eligible Sales at Statement Level
	,('PY_ML_Margin','')		-- Overall Margin PY TotalReward/PY Total Eligible Sales at Statement Level. Should these values be obtained from RP2020 or RP2019 statement ?????
	,('CY_INC_Max','') 
	,('CY_INC_Est','') 
	,('CY_TOTAL_REWARD','')
	,('CY_POG','')
	,('CY_POG_PLAN','')
	,('PY_POG','') 
	,('CY_INV_Margin',@INV_ML)
	,('CY_INV_POG',@INV_ML)
	,('CY_CCP_Margin',@CCP_ML)
	,('CY_CCP_POG', @CCP_ML)
	,('CY_ST_INC_Margin',@ST_INC_ML)
	,('CY_ST_INC_POG',@ST_INC_ML)
	,('CY_CER_PUL_SB_Margin',@CER_PUL_SB_ML)
	,('CY_CER_PUL_SB_POG',@CER_PUL_SB_ML)
	,('CY_LIBERTY_200_Margin',@LIB200_ML)
	,('CY_LIBERTY_200_POG',@LIB200_ML)
	,('ADD_PROGRAMS_TOTALREWARD',@OTHERREWARDS_ML)
	,('OPENING_INV','')
	,('SAP_SALES','')  
	,('EST_RETURNS','') 
	,('EST_REBILL','') 
	,('EST_SALES','') 
	,('CALC_POG','')
	,('HEAT_PreHar_Distinct_Est','')
	,('Pre_Exceptions_Est','') 

	-- STEP #2 - INSERT ALL REQUIRED METRICS 
	INSERT INTO @TEMP_TABLE (Statementcode, Locked_Statementcode, MarketLetterCode, Item_Code)
	SELECT ST.StatementCode, ST.Locked_Statementcode, t.MarketLetterCode, t.Item_Code	
	FROM @TARGET_STATEMENTS ST, @Item_Code T
			   
	-- OTHER METRICS
	Drop table if exists #2019EndingInventory
	Select		oi.Retailercode, sum(oi.Ending_Inventory*pr.SDP) [2019EndingInventory]
	Into		#2019EndingInventory
	From		[dbo].[Opening_Inventory] oi
		left join	dbo.retailerprofile rp on rp.retailercode = oi.retailercode
		Left join	dbo.productreferencepricing pr on pr.productcode =  oi.product_link_code and rp.Region = pr.region and pr.Season = @Season
	where		oi.season = @SEASON - 1 
	Group by	oi.retailercode
	
	Drop table if exists #Q4Orders 
	Select		ge.Sold_To_RetailerCode Retailercode, sum(ge.Q4_Orders*pr.SDP) Q4Orders
	Into		#Q4Orders
	FROM		[dbo].[Get_Q4_Orders](NULL)  ge
		left join	dbo.retailerprofile rp on rp.retailercode = ge.Sold_To_RetailerCode
		Left join	dbo.productreferencepricing pr on pr.productcode =  ge.productcode and rp.Region = pr.region and pr.Season = @Season
	where		ge.season = @SEASON - 1 or ge.season is null
	group by	ge.Sold_To_RetailerCode

	Drop table if exists #SAP_IC
	Select 	sap.Retailercode, sum(SAPIC_QTY*pr.SDP) SAPSALES
	Into		#SAP_IC
	From		(
		Select		iif(Ship_To_RetailerCode = 'R520166422',Sold_To_RetailerCode,Ship_To_RetailerCode) Retailercode, coalesce(p.productcode,y.productcode) Productcode, isnull(cast(Invoice_quantity as float),0) SAPIC_Qty, Invoice_Date
		From		dbo.YSeed140 y
			Left join	(
				Select p.*, Convert(Date,Case Timestart When '10/01' Then '10-01-2019'
						   When '07/01' Then '07-01-2020'
							   End) Time_Start,
								Convert(Date,Case TimeEnd When '06/30' Then '06-30-2020'
						   When '09/30' Then '09-30-2020'
							   End) Time_End						    
				From dbo.parentproductmapping p) p on isnull(p.parentproductcode,'') = y.productcode and Convert(date,y.filter_date) between p.Time_start and p.Time_End
				where		convert(date,Invoice_Date) between '10-01-2019' and '09-30-2020' and Ship_To_RetailerCode is not null and Sold_To_Retailercode is not null
					and IS_order = 1 and Is_Return = 0 and  Is_Invoice = 0
			) sap
			left join	dbo.retailerprofile rp on rp.retailercode = sap.Retailercode
			Left join	dbo.productreferencepricing pr on pr.productcode =  sap.productcode and rp.Region = pr.region and pr.Season = @Season
		group by	sap.Retailercode

	drop table if exists #order_IC_Entries
	CREATE TABLE #order_IC_Entries(
			Retailercode Varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
			Productcode Varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
			ReturnAmount Float,
			RebillAmount Float,
			rnk int
			)
	insert into #order_IC_Entries 
	Select		Retailercode, ProductCode, ReturnAmount, RebillAmount, row_number () over (partition by Retailercode, productcode order by OrderID desc) rnk
	From		dbo.IC_Order_Entries 

	drop table if exists #rebill_return
	Select		a.Retailercode, sum(ReturnAmount*pr.SDP) ReturnAmount, sum(RebillAmount*pr.SDP) RebillAmount
	Into		#rebill_return
	From		#order_IC_Entries a
	Left join	dbo.retailerprofile rp on rp.retailercode = a.retailercode
	Left join	dbo.productreferencepricing pr on pr.productcode =  a.productcode and rp.Region = pr.region and pr.Season = @Season
	where		rnk = 1
	group by	a.Retailercode


	drop table if exists #wholesale_purchases
	Select		retailercode, sum(InyearSDP) wholesale_purchases
	Into		#wholesale_purchases
	From		[dbo].[RetailerTransactions_Wholesaler]
	where		TranstypeDesc = '2020 NON-BASF WHOLESALE POG'
	group by	retailercode


	drop table if exists #Incentive_Estimate
	SELECT MAP.StatementCode, SUM(ISNULL(Amount,0)) AS Amount, 'CY_INC_Est' Item_Code
	into	#Incentive_Estimate
	FROM	@STATEMENT_MAPPINGS MAP
			INNER JOIN ( 
					SELECT RetailerCode, ISNULL([SeasonEstimate],0) AS Amount
					FROM Incentive
					WHERE Season=@SEASON AND PaymentMethod='Paid by Retail Statement' AND [Status] NOT IN ('DRAFT','DELETED')				
				) i 
				ON i.retailercode = map.retailercode 
	GROUP BY MAP.StatementCode

	drop table if exists #Pre_Harvest_Distinct_Est
	Select	StatementCode, sum(Amount) Amount, 'HEAT_PreHar_Distinct_Est' Item_Code
	Into	#Pre_Harvest_Distinct_Est
	From	RPWeb_Est_HeatDistinctTopUp
	Group By StatementCode

	
	drop table if exists #Est_Exceptions
	Select	StatementCode, sum(Amount) Amount, 'Pre_Exceptions_Est' Item_Code
	Into	#Est_Exceptions
	From	RPWeb_Est_Exceptions
	Group By StatementCode
		

		
	-- LETS GET ALL APPLICABLE MARKET LETTER CODES AND POG VALUES AND REWARD VALUES 
	INSERT INTO @ML_POG (Statementcode, MarketLetterCode, POG_Amount, Reward_Amount )
	SELECT ST.StatementCode, ML.MarketLetterCode
			,ISNULL(TX.POG_Amount,0) POG_Amount
			,ISNULL(TotalReward,0) Reward_Amount						
	FROM @TARGET_STATEMENTS ST
		INNER JOIN (			
			SELECT DISTINCT Statementcode, MarketLetterCode
			FROM RPWEB_Rewards_Summary
			where MarketLetterCode in (	@INV_ML,@CCP_ML,@ST_INC_ML,@CER_PUL_SB_ML,@LIB200_ML,@OTHERREWARDS_ML)			
		) ML
		ON ML.Statementcode=ST.Locked_Statementcode
		LEFT JOIN (
			SELECT Statementcode, MarketLetterCode, Sum(Price_CY) POG_Amount  
			FROM dbo.RP2020Web_Sales_Consolidated 
			WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS'
			GROUP BY Statementcode, MarketLetterCode
		) TX		
		ON TX.StatementCode=ST.Locked_Statementcode AND TX.MarketLetterCode=ML.MarketLetterCode
		LEFT JOIN (
			SELECT Rs.StatementCode, RS.MarketLetterCode, SUM(RS.TotalReward) AS TotalReward
			FROM RPWEB_Rewards_Summary RS								
			GROUP BY StatementCode, MarketLetterCode
		) REW
		ON REW.StatementCode=ST.Locked_Statementcode AND REW.MarketLetterCode=ML.MarketLetterCode
		
	

	-- MARKET LETTER RAIOS
	UPDATE @ML_POG	SET Ratio=Reward_Amount/POG_Amount	WHERE POG_Amount <> 0 AND MarketLetterCode <> ''
		


	-- UPDATE POG AMOUNT AT MARKETLETTER LEVEL
	UPDATE T1
	SET Item_Value =T2.POG_Amount 
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT StatementCode, MarketLetterCode, SUM(POG_Amount) AS POG_Amount
			FROM  @ML_POG			
			GROUP BY STATEMENTCODE, MarketLetterCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.Item_Code IN ('CY_INV_POG','CY_CCP_POG','CY_ST_INC_POG','CY_CER_PUL_SB_POG','CY_LIBERTY_200_POG')

	-- UPDATE MARGIN VALUE AT MARKETLETTER LEVEL
	UPDATE T1
	SET Item_Value =T2.Ratio 
	FROM @TEMP_TABLE T1
		INNER JOIN  @ML_POG T2
		ON T2.StatementCode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.Item_Code IN ('CY_INV_Margin','CY_CCP_Margin','CY_ST_INC_Margin','CY_CER_PUL_SB_Margin','CY_LIBERTY_200_Margin')
				

	-- LETS UPDATE STATEMENT LEVEL POGS
	UPDATE T1
	SET Item_Value=CASE 
			WHEN Item_Code = 'CY_POG' THEN  T2.POG_Amount
			WHEN Item_Code = 'PY_POG' THEN  CASE WHEN T2.PY_POG_Amount < 0 THEN 0 ELSE T2.PY_POG_Amount END
			WHEN Item_Code = 'CY_POG_PLAN' THEN  T2.For_Amount
				ELSE T1.Item_Value
			END
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT Statementcode, Sum(Price_CY) POG_Amount, Sum(Price_Forecast) For_Amount, sum(Price_LY1) PY_POG_Amount
			FROM RP2020Web_Sales_Consolidated 
			WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS' 
			GROUP BY Statementcode
		) T2
		ON T2.Statementcode=T1.Statementcode
	WHERE T1.Item_Code IN ('CY_POG','CY_POG_PLAN','PY_POG')
	
	
	-- UPDATE CY TOTALREWARD AT STATEMENT LEVEL
	UPDATE T1
	SET Item_Value =T2.Reward_Amount 
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT StatementCode,  SUM(Reward_Amount) AS Reward_Amount
			FROM  @ML_POG			
			GROUP BY STATEMENTCODE
		) T2
		ON T2.StatementCode=T1.Statementcode
	WHERE T1.Item_Code IN ('CY_TOTAL_REWARD')


	-- UPDATE STATEMENT LEVEL MARGIN RATIO
	
	UPDATE T1
	SET Item_Value =isnull(T2.Ratio,0)
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT Statementcode, 'CY_ML_MARGIN' Item_Code, IIF(POG_Amount = 0, 0, Reward/POG_Amount) AS Ratio
			FROM
			(
				SELECT T.Statementcode, sum((isnull(Reward_Amount,0) + isnull(inc.amount,0) + isnull(Pre.Amount,0) + isnull(est.Amount,0))) Reward, POG_Amount
				FROM @TARGET_STATEMENTS T
				LEFT JOIN (
					SELECT Statementcode, SUM(Reward_Amount) AS Reward_Amount, SUM(POG_Amount) AS POG_Amount
					FROM @ML_POG	
					GROUP BY Statementcode	
				) D 
				ON D.Statementcode = T.Statementcode
				LEFT JOIN (
					SELECT Statementcode, Amount
					FROM #Incentive_Estimate
				) INC
				ON INC.Statementcode = T.Statementcode
				LEFT JOIN (
					SELECT Statementcode, Amount
					FROM  #Pre_Harvest_Distinct_Est
				) PRE
				ON PRE.StatementCode = T.Locked_Statementcode
				LEFT JOIN (
					SELECT Statementcode, Amount
					FROM  #Est_Exceptions
				) est
				ON est.StatementCode = T.Locked_Statementcode
				Group BY T.Statementcode, POG_Amount
			) AL
		) T2
		ON T2.StatementCode=T1.Statementcode and t2.Item_Code=t1.item_Code
	WHERE T1.Item_Code IN ('CY_ML_MARGIN')
	

	/**
		-- UPDATE CY TOTALREWARD AT STATEMENT LEVEL
	UPDATE T1
	SET Item_Value =T2.Reward_Amount 
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT StatementCode, SUM(ISNULL(Reward_Amount,0)) AS Reward_Amount
			FROM @ML_POG
			GROUP BY STATEMENTCODE
		) T2
		ON T2.StatementCode=T1.Statementcode
	WHERE T1.Item_Code IN ('CY_TOTAL_REWARD')

	   

	-- UPDATE STATEMENT LEVEL MARGIN RATIO
	UPDATE T1
	SET Item_Value = IIF(ISNULL(D.POG_Amount,0)=0, 0, ISNULL(Reward_Amount,0) / D.POG_Amount)		
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT Statementcode, SUM(ISNULL(Reward_Amount,0)) AS Reward_Amount, SUM(ISNULL(POG_Amount,0)) AS POG_Amount
			FROM (
				SELECT Statementcode, Reward_Amount, POG_Amount FROM @ML_POG	
					UNION ALL
				SELECT Statementcode, Amount, 0 FROM #Pre_Harvest_Distinct_Est					
					UNION ALL
				SELECT Statementcode, Amount, 0 FROM #Est_Exceptions						
					UNION ALL				 
				SELECT Statementcode, Amount, 0 FROM #Incentive_Estimate
			) D1
			GROUP BY StatementCode
		) D 
		ON D.Statementcode=T1.Statementcode
	WHERE T1.Item_Code IN ('CY_ML_MARGIN') **/
	   	  		
	-- UPDATE OTHER REWARDS
	UPDATE T1
	SET Item_Value = T2.Other_Margin
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT m.StatementCode,m.MarketLetterCode, isnull(sum(m.Reward_Amount/nullif(po.POG_AMOunt,0)),0) OTHER_Margin
			FROM  @ML_POG m
				LEFT JOIN (
					Select StatementCode, sum(POG_Amount) POG_Amount
					From @ML_POG
					Group by StatementCode 
				) po on po.statementcode = m.statementcode
			GROUP BY m.StatementCode, m.MarketLetterCode	
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode
	WHERE T1.Item_Code IN ('ADD_PROGRAMS_TOTALREWARD')

		
	-- LETS COLLECT RP2019 DATA
	IF OBJECT_ID('tempdb..#RP2019_DATA') IS NOT NULL DROP TABLE #RP2019_DATA
	Select s.RetailerCode, sum(CYRewardMargin) CYRewardMargin, sum(CYRewardValue) CYRewardValue, sum(CYSales) CYSales, sum(CYEligibleSales) CYEligibleSales
	INTO #RP2019_DATA
	from [dbo].[RP_View_Statement_Advanced] s
	Where Year=@SEASON-1 AND StatementType='Actual' AND Version='Unlocked'  AND IsQA='False' and summary='false'    
			and model in ('west') --'west distributor','west invigor','west ca' 
	group by s.RetailerCode 
			
	-- PY POG_AMOUNT
	/*UPDATE T1
	SET Item_Value = t2.CYSales
	FROM @TEMP_TABLE T1
		INNER JOIN (
			SELECT MAP.StatementCode, isnull(sum(CYEligibleSales),0)  CYSales, 'PY_POG' Item_Code
			FROM @STATEMENT_MAPPINGS MAP
				LEFT JOIN #RP2019_DATA POG
				on pog.RetailerCode = map.RetailerCode
			GROUP BY MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('PY_POG') */
	
	-- PY_ML_Margin
	UPDATE T1
	SET Item_Value = t2.CYRewardMargin
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			SELECT MAP.StatementCode, isnull(sum(CYRewardMargin),0) CYRewardMargin, 'PY_ML_Margin' Item_Code
			FROM @STATEMENT_MAPPINGS MAP
				LEFT JOIN #RP2019_DATA POG
				on pog.RetailerCode = map.RetailerCode
			GROUP BY MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('PY_ML_Margin')

	-- CY_INCENTIVES_MAX_EXPOSURE
	/* 
		INCENTIVES
		for Max Exposure, pull from Resource, 
		In-Season Estimate = SeasonEstimate, 
		Total Earned = TotalEarned
	*/

	UPDATE T1
	SET Item_Value = t2.Amount
	FROM @TEMP_TABLE T1
		INNER JOIN (		
			SELECT MAP.StatementCode, SUM(Amount) AS Amount, 'CY_INC_Max' Item_Code
			FROM @STATEMENT_MAPPINGS MAP
				INNER JOIN ( 
					SELECT RetailerCode, [Resource] AS Amount
					FROM Incentive
					WHERE Season=@SEASON AND PaymentMethod='Paid by Retail Statement' AND [Status] NOT IN ('DRAFT','DELETED')				
				) i 
				ON i.retailercode = map.retailercode 
			GROUP BY MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('CY_INC_Max') 

	-- CY_INCENTIVES_ESTIMATED
	UPDATE T1
	SET Item_Value = t2.Amount
	FROM @TEMP_TABLE T1
		INNER JOIN ( SELECT *
					FROM #Incentive_Estimate
					) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('CY_INC_Est') 
	
	-- OPENING INVENTORY
	UPDATE T1
	SET Item_Value = t2.opening_inventory
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum((isnull([2019EndingInventory],0) + isnull(Q4Orders,0))),0) opening_inventory, 'OPENING_INV' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #2019EndingInventory POG
				on pog.RetailerCode = map.RetailerCode
				LEFT JOIN #Q4Orders ord
				on ord.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('OPENING_INV')

	-- SAP SALES
	UPDATE T1
	SET Item_Value = t2.SAPSALES
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum(SAPSALES),0) SAPSALES, 'SAP_SALES' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #SAP_IC SAP
				on sap.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('SAP_SALES')
	
	-- EST_RETURNS
	UPDATE T1
	SET Item_Value = t2.ReturnAmount
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum(ReturnAmount),0) ReturnAmount, 'EST_RETURNS' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #rebill_return re
				on re.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('EST_RETURNS')

	-- EST_REBILL
	UPDATE T1
	SET Item_Value = t2.RebillAmount
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum(RebillAmount),0) RebillAmount, 'EST_REBILL' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #rebill_return re
				on re.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('EST_REBILL')

	-- ESTIMATED FINANCIAL SALES
	UPDATE T1
	SET Item_Value = t2.EST_FINANCIAL_SALES
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum((isnull(SAPSALES,0) - isnull(ret.ReturnAmount,0) - isnull(reb.RebillAmount,0))),0) EST_FINANCIAL_SALES, 'EST_SALES' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #SAP_IC SAP
				on sap.RetailerCode = map.RetailerCode
				LEFT JOIN #rebill_return reb
				on reb.RetailerCode = map.RetailerCode
				LEFT JOIN #rebill_return ret
				on ret.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('EST_SALES')
	
	-- CALCULATED POG
	UPDATE T1
	SET Item_Value = t2.CALC_POG
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select MAP.StatementCode, isnull(sum((isnull([2019EndingInventory],0) + isnull(Q4Orders,0) + isnull(wholesale_purchases,0) + isnull(SAPSALES,0) - isnull(ret.ReturnAmount,0) - isnull(reb.RebillAmount,0))),0) CALC_POG, 'CALC_POG' Item_Code
			From @STATEMENT_MAPPINGS MAP
				LEFT JOIN #SAP_IC SAP
				on sap.RetailerCode = map.RetailerCode
				LEFT JOIN #rebill_return reb
				on reb.RetailerCode = map.RetailerCode
				LEFT JOIN #rebill_return ret
				on ret.RetailerCode = map.RetailerCode
				LEFT JOIN #wholesale_purchases wh
				on wh.retailercode = map.retailercode
				LEFT JOIN #2019EndingInventory POG
				on pog.RetailerCode = map.RetailerCode
				LEFT JOIN #Q4Orders ord
				on ord.RetailerCode = map.RetailerCode
			Group by MAP.StatementCode
		) T2
		ON T2.StatementCode=T1.Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('CALC_POG')


	-- HEAT_Pre_Harvest_Distinct_Est
	UPDATE T1
	SET Item_Value = t2.Amount
	FROM @TEMP_TABLE T1
		INNER JOIN  (Select *
			From #Pre_Harvest_Distinct_Est
		) T2
		ON T2.StatementCode=T1.Locked_Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('HEAT_PreHar_Distinct_Est')

	-- Pre-Exception Estimate or Est_excemptions 
	UPDATE T1
	SET Item_Value = t2.Amount
	FROM @TEMP_TABLE T1
		INNER JOIN  (
			Select * From #Est_Exceptions
		) T2
		ON T2.StatementCode=T1.Locked_Statementcode AND T2.Item_Code=T1.Item_Code
	WHERE T1.Item_Code IN ('Pre_Exceptions_Est')
	  

	DELETE T1
	FROM [dbo].[RPWeb_MarginToolData_Summary] T1
	WHERE T1.StatementCode IN (Select Statementcode From @TARGET_STATEMENTS)

	INSERT INTO [dbo].[RPWeb_MarginToolData_Summary] (Statementcode, Item_Code, Item_Value)
	Select Statementcode, Item_Code, Item_Value
	FROM @TEMP_TABLE

END



















