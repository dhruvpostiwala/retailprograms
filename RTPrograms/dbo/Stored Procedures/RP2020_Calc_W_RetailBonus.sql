﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_RetailBonus]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
		Planned Growth: (%) 2020 POG Plan ($) relative to 2019/2018 2-Year Avg POG Sales ($)
		Qualify on a whole bunch of brands
		Reward on Centurion - above 2M qualifies <=100% = 1%, >100% = 3%, below 2M qualifies <=100% = 1%, >100% = 2%
		Reward on Liberty   - above 2M qualifies <=100% = 1%, >100% = 3%, below 2M qualifies <=100% = 1%, >100% = 2%

		NEW: RC-3544 - We must include Invigor/Liberty/Centurion (SRP) to qualify the above vs below 2 million but when comparing
					   year over year sales to determine growth %, we do NOT include Invigor/Liberty/Centurion
		NEW: RC-3545 - Based on the most recent BRD, I believe Aimee wanted a manual input, similar to how the efficiency rebate works in RRT,
					   where a user could indicate (Yes/No) if they will be over $2m in POGs (even if their POG plan does not currently reflect this. 
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 2
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_RETAIL_BONUS_PAYMENT'

	DECLARE @EPSILON DECIMAL(4,4)=dbo.SVF_GetEpsilon();
	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	
	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		QualBrand_Sales_EI BIT NOT NULL DEFAULT 0,
		QualBrand_Sales_CY Money NOT NULL,
		Growth_Sales_CY Money NOT NULL,
		Growth_Sales_LY1 Money NOT NULL,
		Growth_Sales_LY2 Money NOT NULL,
		Lib_Reward_Sales Money NOT NULL,
		Lib_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Lib_Reward Money NOT NULL DEFAULT 0,
		Cent_Reward_Sales Money NOT NULL,
		Cent_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Cent_Reward Money NOT NULL DEFAULT 0,
		totalEligible_Reward_Sales Money NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		Growth Money NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- Determine qualifiers. Note that qualifier brands include the reward brands (centurion + liberty)
	--new Demarey
	--remove centurion and liberty as qualifying brands  RP-2257
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,QualBrand_Sales_CY,Growth_Sales_CY,Growth_Sales_LY1,Growth_Sales_LY2,Lib_Reward_Sales,Cent_Reward_Sales)
	SELECT ST.StatementCode, tx.MarketLetterCode,tx.ProgramCode
		,SUM(IIF(tx.GroupLabel='QUALIFIERS',tx.Price_CY,0)) AS QualBrand_Sales_CY									-- all brands + Invigor + liberty + Centurion
		,SUM(IIF(tx.GroupLabel='QUALIFIERS',tx.Price_CY,0)) AS Growth_Sales_CY		-- all brands
		,SUM(IIF(tx.GroupLabel='QUALIFIERS',tx.Price_LY1,0)) AS Growth_Sales_LY1	-- all brands
		,SUM(IIF(tx.GroupLabel='QUALIFIERS',tx.Price_LY2,0)) AS Growth_Sales_LY2	-- all brands
		,SUM(IIF(tx.GroupLabel='LIBERTY',tx.Price_CY,0)) AS Lib_Reward_Sales
		,SUM(IIF(tx.GroupLabel='CENTURION',tx.Price_CY,0)) AS Cent_Reward_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode

	UPDATE T1
		SET totalEligible_Reward_Sales = sales.Price_CY
		FROM @TEMP T1
		INNER JOIN
			(
				SELECT st.STATEMENTCODE, SUM(Price_CY) as Price_CY
				FROM RP2020_DT_Sales_Consolidated RS
				INNER JOIN @STATEMENTS ST
				ON ST.STATEMENTCODE = RS.STATEMENTCODE
				WHERE programCode = 'ELIGIBLE_SUMMARY' 
				AND groupType = 'ELIGIBLE_BRANDS'
				GROUP BY st.STATEMENTCODE 
			) sales
		ON sales.STATEMENTCODE = T1.STATEMENTCODE

	UPDATE @TEMP
	SET Growth = Growth_Sales_CY / ((Growth_Sales_LY1 + Growth_Sales_LY2)/2)
	WHERE Growth_Sales_LY1 + Growth_Sales_LY2 > 0 

	/*
	Growth in 2020 relative to the 2-year average

	Reward on Centurion
				Above 2M	Below 2M
	0 to 99.9%	1%			1%
	100%+		3%			2%

	Reward on Liberty
				Above 2M	Below 2M
	0 to 99.9%	3%			2%
	100%+		6%			4%
	*/

	-- NEW: RC-3545
	-- Determine the reward percentage based on the new editable input, not the actual sales calculation
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
		BEGIN
			UPDATE T1
			SET [QualBrand_Sales_EI] = ISNULL(EI.FieldValue,1)
			FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=@Program_Code AND EI.FieldCode='Bonus_Qualified'

			-- Determine Centurion percentage
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.03 WHERE Cent_Reward_Percentage = 0 AND QualBrand_Sales_EI = 1 AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth >= 1)
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.02 WHERE Cent_Reward_Percentage = 0 AND QualBrand_Sales_EI = 0 AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth >= 1)
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.01 WHERE Cent_Reward_Percentage = 0

			-- Determine Liberty percentage
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.06 WHERE Lib_Reward_Percentage = 0 AND QualBrand_Sales_EI = 1 AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth >= 1)
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.03 WHERE Lib_Reward_Percentage = 0 AND QualBrand_Sales_EI = 1
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.04 WHERE Lib_Reward_Percentage = 0 AND QualBrand_Sales_EI = 0 AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth >= 1)
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.02 WHERE Lib_Reward_Percentage = 0 AND QualBrand_Sales_EI = 0

		END
	ELSE
		BEGIN
			DECLARE @SalesThreshold INT = 2000000

			-- Determine Centurion percentage
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.03 WHERE Cent_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON >= @SalesThreshold AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth+@Roundup_Value >= 1)
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.02 WHERE Cent_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON < @SalesThreshold AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth+@Roundup_Value >= 1)
			UPDATE @TEMP SET [Cent_Reward_Percentage] = 0.01 WHERE Cent_Reward_Percentage = 0

			-- Determine Liberty percentage
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.06 WHERE Lib_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON >= @SalesThreshold AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth+@Roundup_Value >= 1)
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.04 WHERE Lib_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON < @SalesThreshold AND ((Growth=0 AND Growth_Sales_CY>0) OR Growth+@Roundup_Value >= 1)
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.03 WHERE Lib_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON >= @SalesThreshold
			UPDATE @TEMP SET [Lib_Reward_Percentage] = 0.02 WHERE Lib_Reward_Percentage = 0 AND totalEligible_Reward_Sales+@EPSILON < @SalesThreshold

		END

	-- Determine reward(s)
	UPDATE @TEMP SET [Cent_Reward] = [Cent_Reward_Sales] * [Cent_Reward_Percentage]
	UPDATE @TEMP SET [Lib_Reward] = [Lib_Reward_Sales] * [Lib_Reward_Percentage]
	UPDATE @TEMP SET [Reward] = [Cent_Reward] + [Lib_Reward] WHERE [Cent_Reward] + [Lib_Reward] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Cent_Reward] = [Cent_Reward] * ISNULL(EI.FieldValue/100,1),
			[Lib_Reward] = [Lib_Reward] * ISNULL(EI.FieldValue/100,1),
			[Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
	
	DELETE FROM RP2020_DT_Rewards_W_RetailBonus WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_RetailBonus(Statementcode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1, Growth_Sales_LY2, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1, Growth_Sales_LY2, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward
	FROM @TEMP
	
END


