﻿




CREATE    PROCEDURE [dbo].[RPUC_Agent_UpdateLeads] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	
	BEGIN TRY

		IF OBJECT_ID('tempdb..#Leads_To_Delete') IS NOT NULL DROP TABLE #Leads_To_Delete
		IF OBJECT_ID('tempdb..#Leads_To_Insert') IS NOT NULL DROP TABLE #Leads_To_Insert
		IF OBJECT_ID('tempdb..#AllLeads_To_Insert') IS NOT NULL DROP TABLE #AllLeads_To_Insert

		SELECT @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode
		--SET @JSON='{"created":[{"rp_id":0,"retailercode":"R400011638","farm":{"farmcode":"F0000125","label":"F0000125 (P)"},"modifieddate":"2020-04-23T15:11:27.003Z","product":{"productcode":"PRD-CIRD-B24KLL","productname":"INVIGOR L255PC PROSPER 22.7 KG BAG"},"productname":"","variety":{"varietycode":"CROP-BBRG-9XBQQJ","variety":"Clearfield canola"},"varietyname":"","acre":20}],"updated":[],"destroyed":[]}'

		-- LETS GET StatementTransNumber to be deleted 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @StatementCode AS Statementcode, t1.rp_id
		INTO #Leads_To_Delete
		FROM (
			SELECT b.rp_id
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						rp_id INT N'$.rp_id'					
					) as b	

				UNION ALL

			SELECT b.rp_id
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						rp_id INT N'$.rp_id'					
					) as b		
		) T1			


		-- ALL STATEMENTS TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode ,t1.*
		INTO #Leads_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (												
							retailercode varchar(20) N'$.retailercode',
							farmcode VARCHAR(20) N'$.farm.farmcode',							
							inradius varchar(3) N'$.inradius',
							productcode varchar(65) N'$.product.productcode',
							varietycode varchar(65) N'$.variety.varietycode',
							acres numeric(18,2) N'$.acres',
							modifieddate datetime N'$.CreatedDate'
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								farmcode VARCHAR(20) N'$.farm.farmcode',							
								inradius varchar(3) N'$.inradius',
								productcode varchar(65) N'$.product.productcode',
								varietycode varchar(65) N'$.variety.varietycode',
								acres numeric(18,2) N'$.acres',
								modifieddate datetime N'$.CreatedDate'					
							) as b
		) T1

		DELETE T1		
		FROM RP_DT_Leads T1
			INNER JOIN #Leads_To_Delete T2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.rp_id=T2.rp_id
		
		
		/*  
			
			PRIMARY KEY:
			[StatementCode] ASC,
			[StatementTransNumber] ASC		

			LETS REGENERATE VALUES FOR '[StatementTransNumber]' 
		
			STEP #1: COLLECT DATA FROM ACTUAL TABLE INTO TEMPORARY TABLE. SINCE WE ARE GOING TO UPDATE '[StatementTransNumber]' COLUMN, NO NEED TO FETCH IT 				
			STEP #2: DELETE RECORDS FROM ACTUAL TABLE WHERE STATEMENTCODE=@STATEMENTCODE
			STEP #3: INSERT RECORDS BACK INTO ACTUAL TABLE FROM TEMPORARY TABLE. USE 'ROW_NUMBER()' FUNCTION TO INSERT VALUE INTO '[StatementTransNumber]' COLUMN

		*/

		-- STEP #1: Collect data into temporary table #ALLSales_To_Insert
		SELECT StatementCode,LeadID,Season,RetailerCode,FarmCode,inradius,ProductCode,VarietyCode,Acres,AppRate,BookType,GeneratingSource,CreatedDate
		,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode, ProductCode, VarietyCode) AS rp_id
		INTO #AllLeads_To_Insert
		FROM (
			SELECT StatementCode,LeadID,Season,RetailerCode,FarmCode,inradius,ProductCode,VarietyCode,Acres,AppRate,BookType,GeneratingSource,CreatedDate
			FROM RP_DT_Leads
			WHERE StatementCode=@StatementCode

				UNION ALL

			SELECT Statementcode
				,0 AS LeadID
				,IIF(MONTH(tx.modifieddate) IN (10,11,12),YEAR(tx.modifieddate)+1,YEAR(tx.modifieddate)) AS Season				
				,tx.retailercode AS RetailerCode
				,tx.farmcode AS FarmCode
				,IIF(upper(inradius)='YES',1,0) AS inradius
				,tx.ProductCode AS ProductCode
				,tx.VarietyCode AS VarietyCode
				,tx.acres AS Acres
				,0 AS AppRate
				,'Chemical' AS BookType
				,'Grower Connect' AS GeneratingSource
				,tx.modifieddate AS CreatedDate
			FROM #Leads_To_Insert tx				
		) T1	
		

		-- STEP #2: DELETE RECORDS FROM  RP_DT_TRANSACTIONS
		DELETE RP_DT_Leads WHERE StatementCode=@StatementCode
		
		-- STEP #3: INSERT RECORDS FROM #AllSales_To_Insert INTO RP_DT_TRANSACTIONS 
		INSERT INTO RP_DT_Leads (StatementCode,rp_id,LeadID,Season,RetailerCode,FarmCode,inradius,ProductCode,VarietyCode,Acres,AppRate,BookType,GeneratingSource,CreatedDate)
		SELECT StatementCode,rp_id,LeadID,Season,RetailerCode,FarmCode,inradius,ProductCode,VarietyCode,Acres,AppRate,BookType,GeneratingSource,CreatedDate		
		FROM #AllLeads_To_Insert		
				
		IF @REGION='EAST' AND @DATASETCODE > 0 
		BEGIN
			DELETE RP_DT_Orders WHERE StatementCode=@DataSetCode

			INSERT INTO RP_DT_Orders (StatementCode,DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,inradius,rp_id)
			SELECT @DataSetCode AS StatementCode, 0 AS DataSetCode,TransCode,Season,RetailerCode,FarmCode,ProductCode,Conversion,InvoiceDate,InvoiceNumber,Quantity,inradius
				,ROW_NUMBER() OVER(PARTITION BY @DataSetCode ORDER BY RetailerCode, ProductCode) AS rp_id
			FROM RP_DT_Orders
			WHERE DataSetCode=@DataSetCode
		END
		
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

