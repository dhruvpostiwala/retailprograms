﻿

CREATE PROCEDURE [dbo].[RP_GenerateStatements_CustomAerial] @SEASON INT
AS
BEGIN
	SET NOCOUNT ON;

	/*
		Custom Aerial App
		Custom Applicator - East
		Fungicide Custom App
		Third Party Custom Aerial App			
	*/

	DECLARE @STATEMENT_TYPE VARCHAR(20)='Actual'
	DECLARE @STATEMENT_CATEGORY VARCHAR(50)='Custom Aerial'
	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @SEASON_START_DATE DATE = CAST(@SEASON_STRING + '-10-02' AS DATE)
	DECLARE @SEASON_END_DATE DATE = CAST(@SEASON_STRING + '-09-30' AS DATE)

	DECLARE @ELIGIBLE_RETAILERS TABLE ( 
		RetailerCode VARCHAR(20) NOT NULL
	)

	INSERT INTO @ELIGIBLE_RETAILERS(RetailerCode)
	VALUES('R520133727')
		,('R770026162')
		,('R520133400')
		,('R520018583')
		,('R520134728')
		,('R520130565')
		,('R0001101')
		,('R520127874')
		,('R520145043')
		,('R520168956')
		,('R520165299')
		,('R520162988')
		,('R520165197')
		,('R770026183')
		,('R520164967')
		,('R520127516')
		,('R520133572')
		,('R520127707')
		,('R200003020')
		,('R0001274')
		,('R520162577')
		,('R0000955')

	INSERT INTO RP_Statements(StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,ActiveRetailerCode,DataSetCode,DataSetCode_L5,IsPayoutStatement,Category)
	SELECT @STATEMENT_TYPE as StatementType, 'Unlocked' AS VersionType, RP.RetailerType AS StatementLevel,'Active' AS [Status], @SEASON AS [Season], RP.Region, RP.RetailerCode, RP.RetailerType
		,RP.Level2Code	,RP.Level5Code, '' AS ActiveRetailerCode, 0 AS DataSetCode, 0 AS DataSetCode_L5 ,'Yes' AS IsPayoutStatement
		,@STATEMENT_CATEGORY AS Category 
	FROM @ELIGIBLE_RETAILERS ER
		INNER JOIN RetailerProfile rp
		ON RP.Retailercode=ER.RetailerCode
		INNER JOIN (
			SELECT RetailerCode
			FROM RetailerProfileAccountType		
			WHERE AccountType IN ('Custom Aerial App','Third Party Custom Aerial App') 
			GROUP BY RetailerCode
		) ACT 
		ON ACT.Retailercode=RP.Retailercode
		LEFT JOIN (
			SELECT Retailercode 
			FROM RetailerAccess 
			WHERE ClassificationType='RC Territory'
			GROUP BY Retailercode 
		) RA
		ON RA.RetailerCode=RP.RetailerCode
		LEFT JOIN (
			SELECT RetailerCode
			FROM RP_Statements
			WHERE Season=@SEASON AND Category IN ('Regular',@STATEMENT_CATEGORY) AND StatementType=@STATEMENT_TYPE AND VersionType='Unlocked' AND Level5Code='D000001' AND StatementLevel IN ('Level 1','Level 2') 
			GROUP BY RetailerCode
		) RS
		ON RS.RetailerCode=RP.Retailercode		
	WHERE rp.Level5Code='D000001' AND rp.RetailerType IN ('Level 1','Level 2') 	
		AND (
			rp.Status='Active' 
				OR 
			(rp.Status='Sold' AND CAST(RP.SoldDate AS Date) BETWEEN @SEASON_START_DATE AND @SEASON_END_DATE)
		) 
		AND RA.RetailerCode IS NULL -- Should not be under RC Territory
		AND RS.RetailerCode IS NULL -- Should not have a RP Statement either under regular or customer aerial categories
	ORDER  BY RP.Level2Code, RP.RetailerName
	
	DECLARE @SQL_CMD NVARCHAR(MAX)
	
	SET @SQL_CMD = '
		SET IDENTITY_INSERT dbo.RPWeb_Statements ON
		
		INSERT INTO RPWeb_Statements(Statementcode, StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,ActiveRetailerCode,DataSetCode,DataSetCode_L5,IsPayoutStatement,Category)
		SELECT T1.Statementcode,StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,ActiveRetailerCode,DataSetCode,DataSetCode_L5,IsPayoutStatement,Category
		FROM RP_Statements T1
			LEFT JOIN (
				SELECT StatementCode
				FROM RPWeb_Statements 
				WHERE Season=@SEASON AND Category=@STATEMENT_CATEGORY
			) T2
			ON T2.StatementCode=T1.StatementCode
		WHERE Category=@STATEMENT_CATEGORY AND T2.StatementCode IS NULL
		
		SET IDENTITY_INSERT dbo.RPWeb_Statements OFF 			'

	EXEC SP_EXECUTESQL @SQL_CMD, N'@SEASON INT, @STATEMENT_CATEGORY VARCHAR(50)', @SEASON, @STATEMENT_CATEGORY


	DROP TABLE IF EXISTS #Custom_Aerial_StatementCodes
	SELECT StatementCode
	INTO #Custom_Aerial_StatementCodes
	FROM RPWeb_Statements 
	WHERE Season=@SEASON AND Category=@STATEMENT_CATEGORY

	INSERT INTO RP_StatementsMappings(StatementCode, RetailerCode, ActiveRetailerCode)
	SELECT t1.StatementCode, RetailerCode, RetailerCode  AS ActiveRetailercode
	FROM RPWeb_Statements  T1
		LEFT JOIN #Custom_Aerial_StatementCodes T2
		ON T2.StatementCode=T1.StatementCode
	WHERE Season=@SEASON AND  Category=@STATEMENT_CATEGORY AND T2.StatementCode IS NULL
	
	INSERT INTO RPWeb_StatementInformation(StatementCode, FieldName, FieldValue)
	SELECT T1.StatementCode, 'Status' AS FieldName, 'Open' AS FieldValue
	FROM RPWeb_Statements  T1
		LEFT JOIN #Custom_Aerial_StatementCodes T2
		ON T2.StatementCode=T1.StatementCode
	WHERE T1.Season=@SEASON AND T1.Category=@STATEMENT_CATEGORY AND T2.StatementCode IS NULL
	

		   	
END
