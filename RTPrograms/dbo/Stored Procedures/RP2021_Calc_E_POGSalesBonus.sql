﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_POGSalesBonus] @SEASON INT, @STATEMENT_TYPE VARCHAR(20),  @PROGRAM_CODE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @REWARD_PERCENTAGE DECIMAL(6,4) = 0.02

	-- Insert sales data in to the temp table
	INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup,
		   SUM(TX.Price_CY) AS [RewardBrand_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
		INNER JOIN RP_Statements RS ON ST.StatementCode = RS.StatementCode
		INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
	WHERE RS.StatementLevel='LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE
	GROUP BY ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup
	
	/* OLD CODE
		INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup,
		   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Price_CY) * (EI.FieldValue / 100) ELSE SUM(TX.Price_CY) END [RewardBrand_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN RP_Statements RS
		ON ST.StatementCode = RS.StatementCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
		INNER JOIN RPWeb_USER_EI_FieldValues EI
		ON ST.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_InVigor'
	WHERE RS.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE
	GROUP BY ST.StatementCode, RS.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup, EI.FieldValue

	*/

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE  T1
		SET [RewardBrand_Sales] *= ISNULL(EI.FieldValue/100,1) 
		FROM @TEMP T1
		INNER JOIN RPWeb_USER_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_InVigor'
	END

	-- Update the reward percentage
	UPDATE @TEMP
	SET Reward_Percentage = @REWARD_PERCENTAGE
	--WHERE RewardBrand_Sales > 0

	-- Update the reward
	UPDATE @TEMP
	SET Reward = RewardBrand_Sales * Reward_Percentage
	--WHERE RewardBrand_Sales > 0

	-- Insert into DT table from temp table
	DELETE FROM RP2021_DT_Rewards_E_POGSalesBonus WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_E_POGSalesBonus(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand,
		   SUM(RewardBrand_Sales) [RewardBrand_Sales],
		   MAX(Reward_Percentage) [Reward_Percentage],
		   SUM(Reward) [Reward]
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode, RewardBrand
END
