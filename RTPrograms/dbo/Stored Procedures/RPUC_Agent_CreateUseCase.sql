﻿

CREATE PROCEDURE [dbo].[RPUC_Agent_CreateUseCase] @USERNAME VARCHAR(100),@USECASE_NAME AS VARCHAR(200), @STATEMENTCODE INT, @COPYDATA BIT 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	DECLARE @NEW_DATASETCODE INT=0;
	DECLARE @VERSIONTYPE VARCHAR(20)='';
	DECLARE @STATEMENTLEVEL VARCHAR(20)='';	
	DECLARE @LEVEL2_CODE VARCHAR(20)='';
	DECLARE @NEW_STATEMENTCODE INT;
	
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;

	BEGIN TRY

	-- OBTAIN A UNIQUE IDENTIFIER FOR THIS JOB
	DECLARE @JOB_ID UNIQUEIDENTIFIER=NEWID();	
	
	SELECT @SEASON=SEASON, @REGION=REGION, @VERSIONTYPE=VersionType, @STATEMENTLEVEL=StatementLevel, @DATASETCODE=DatasetCode, @LEVEL2_CODE=Level2Code
	FROM RPWEB_STATEMENTS 
	WHERE [Status]='Active' AND StatementType='Actual' AND VersionType IN ('Unlocked','Use Case') AND STATEMENTCODE=@STATEMENTCODE 
	
	SET @REGION=ISNULL(@REGION,'')
	SET @DATASETCODE=ISNULL(@DATASETCODE,0)
	
	-- IN CASE OF EAST FAMILY, CREATE USECASE FOR ALL IN THE FAMILY
	IF @REGION='WEST' OR (@REGION='EAST' AND @STATEMENTLEVEL='LEVEL 1' AND @DATASETCODE=0)
		INSERT INTO @STATEMENTS(StatementCode)
		SELECT StatementCode
		FROM RPWeb_Statements
		WHERE [Status]='Active' AND StatementType='Actual' AND VersionType=@VERSIONTYPE AND  StatementCode=@STATEMENTCODE
	ELSE IF @REGION='EAST' AND @STATEMENTLEVEL='LEVEL 1' AND @DATASETCODE > 0
		INSERT INTO @STATEMENTS(StatementCode)
		SELECT StatementCode
		FROM RPWeb_Statements
		WHERE [Status]='Active' AND StatementType='Actual' AND VersionType=@VERSIONTYPE AND (StatementCode=@DATASETCODE OR DataSetCode=@DATASETCODE) 
	ELSE IF @REGION='EAST' AND @STATEMENTLEVEL='LEVEL 2'
		BEGIN 
			SET @DATASETCODE=@STATEMENTCODE
			INSERT INTO @STATEMENTS(StatementCode)
			SELECT StatementCode
			FROM RPWeb_Statements
			WHERE [Status]='Active' AND StatementType='Actual' AND VersionType=@VERSIONTYPE AND (StatementCode=@STATEMENTCODE OR DataSetCode=@DATASETCODE) 
		END 
		
	INSERT INTO RP_Statements(UC_Job_ID,UC_SRC_Statementcode,StatementType,VersionType,StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,ActiveRetailerCode,DataSetCode)
	SELECT @JOB_ID,Statementcode, StatementType, 'Use Case' AS VersionType, StatementLevel,Status,Season,Region,RetailerCode,RetailerType,Level2Code,Level5Code,ActiveRetailerCode,DataSetCode
	FROM RPWeb_Statements 
	WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	-- LETS MAINTAIN STATEMENT MAPPINGS FOR USE CASE TO PROVIDE "RPWeb_StatementLocations" INFORMATION 
	INSERT INTO RP_StatementsMappings(StatementCode, RetailerCode,ActiveRetailerCode)
	SELECT DISTINCT ST.StatementCode, MAP.ActiveRetailerCode, MAP.ActiveRetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN (
			SELECT StatementCode, UC_SRC_Statementcode 
			FROM RP_Statements 
			WHERE UC_Job_ID=@JOB_ID
		) ST
		ON ST.UC_SRC_Statementcode=MAP.StatementCode


	/* FOR EAST STATEMENTS UPDATE DATASETCODE TO MATCH TO NEWLY INSERTED STATEMENTCODE */
	--PRINT 'DATA SET CODE: ' + CAST(@DATASETCODE AS VARCHAR)
	IF @REGION='EAST' AND @DATASETCODE > 0
	BEGIN
		SELECT @NEW_DATASETCODE=StatementCode, @LEVEL2_CODE=Level2Code
		FROM RP_Statements 
		WHERE UC_Job_ID=@JOB_ID AND UC_SRC_Statementcode=@DATASETCODE AND Region='EAST' AND StatementLevel='LEVEL 2' 
	
		--PRINT 'NEW DATA SET CODE: ' + CAST(@NEW_DATASETCODE AS VARCHAR)
		UPDATE RP_Statements
		SET DATASETCODE=@NEW_DATASETCODE
		WHERE VersionType='USE CASE' AND UC_Job_ID=@JOB_ID AND DataSetcode=@DATASETCODE AND Region='EAST' AND StatementLevel='LEVEL 1' 
	END
				
	INSERT INTO RPWeb_StatementInformation(StatementCode, FieldName, FieldValue)
	SELECT StatementCode, 'UseCaseName', ISNULL(@USECASE_NAME,'NO NAME PROVIDED')
	FROM RP_Statements
	WHERE UC_Job_ID=@JOB_ID 
		
		UNION

	SELECT StatementCode, 'UseCaseDate', CONVERT(varchar,GetDate(),120) AS [Date]
	FROM RP_Statements
	WHERE UC_Job_ID=@JOB_ID 


	-- GET NEWLY INSERTED STATEMENTCODE
	SELECT @NEW_STATEMENTCODE=STATEMENTCODE
	FROM RP_Statements 
	WHERE UC_Job_ID=@JOB_ID AND UC_SRC_Statementcode=@STATEMENTCODE
	
	DECLARE @TEMP NVARCHAR(MAX);
	
	IF @COPYDATA <> 1 
		GOTO FINAL
	   	 
	/* LETS START COPYING DATA (Transactions,Scenarions,Leads, etc.,)  OVER FOR NEWLY CREATED USE CASE STATEMENTS	*/

	/* COPY OVER DATA TO LOCKED STATEMENTS FROM SOURCE STATEMENTS */
	DECLARE @SQL_CMD NVARCHAR(MAX)
	DECLARE @PARAMS NVARCHAR(50)='@JOB_ID UNIQUEIDENTIFIER'
	
	DECLARE SQL_COMMANDS CURSOR FOR 
		SELECT '
		INSERT INTO '+so.[name]+'([StatementCode],'+STUFF((
			SELECT ',['+[Name]+']'
			FROM syscolumns sc
			/* LETS NOT INCLUDE THESE COLUMNS  */
			WHERE [name] NOT IN ('StatementCode') AND sc.[id]=so.[id] 
			ORDER BY sc.[id]
			FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+')
		SELECT ST.[StatementCode],'+STUFF((
			SELECT ',WEB.[' + [Name] +']'
			FROM syscolumns sc
			WHERE [name] NOT IN ('StatementCode') AND sc.[id]=so.[id]
			ORDER BY sc.[id]
			FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+'
		FROM ' + CFG.[WebTableName] +' WEB
			INNER JOIN RP_Statements ST			ON ST.[UC_SRC_Statementcode]=WEB.[StatementCode]
		WHERE ST.[Status]=''Active'' AND ST.[UC_Job_ID]=@JOB_ID AND ST.[VersionType]=''USE CASE''
			
		' AS [CODE]
		FROM sysobjects so
			INNER JOIN (
				SELECT DT_TableName, TableName AS WebTableName
				FROM RP_Config_WebTableNames
				WHERE Season=@SEASON AND UseCaseCreateAction='Yes' 
			) CFG
			ON CFG.[DT_TableName]=SO.[Name] 
		WHERE so.[xtype]='U' 
	
	OPEN SQL_COMMANDS
	FETCH NEXT FROM SQL_COMMANDS INTO @SQL_CMD
	WHILE(@@FETCH_STATUS=0)
		BEGIN	
		--	PRINT @SQL_CMD		
			EXEC SP_EXECUTESQL @SQL_CMD,@PARAMS,@JOB_ID		
			FETCH NEXT FROM SQL_COMMANDS INTO @SQL_CMD
		END
	CLOSE SQL_COMMANDS
	DEALLOCATE SQL_COMMANDS

FINAL:	
	SET @PARAMS='STATEMENTCODE=' + CAST(@NEW_STATEMENTCODE AS  VARCHAR) 
	EXEC RP_Agent_Refresh @SEASON, 'ACTUAL', 'USE CASE', @PARAMS

	END TRY
					
	BEGIN CATCH
		IF CURSOR_STATUS('global','SQL_COMMANDS')>=-1		
			DEALLOCATE SQL_COMMANDS
		
		SELECT 'error' AS [status],  ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS error_message
		RETURN;	
	END CATCH
	
	SELECT 'success' AS [status],  @NEW_STATEMENTCODE  AS statementcode
	
END




