﻿


CREATE PROCEDURE [dbo].[RP_RefreshExceptions_West] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
		SET NOCOUNT ON;

		/*
			PASSED IN @STATEMENTS  = UNLOCKED STATEMENTS 
			Target exceptions linked to unlocked statements only. Exceptions linked to locked/archive statements should not be updated.
			i.e., SELECT * FROM RPWeb_Exceptions WHERE StaementCode > 0 
		*/

		DECLARE @SEASON_STRING VARCHAR(4)= CAST(@SEASON AS VARCHAR(4))
		DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;
		DECLARE @USERNAME VARCHAR(50) = 'Exceptions Verifier'			
		
		DECLARE @EXCEPTION_IDS AS TABLE (ID INT)

		DECLARE @INVIGOR_MARKETLETTER_CODE VARCHAR(50) 
		DECLARE @CPP_MARKET_LETTER_CODE VARCHAR(50) 

		DECLARE @STORED_PROC_NAME VARCHAR(255) =  'dbo.RP' + @SEASON_STRING + '_Exceptions_W_GetRewardsData'

		DECLARE @EXCEPTIONS AS [dbo].[UDT_RP_EXCEPTIONS_DATA]

		INSERT INTO @EXCEPTIONS
		EXEC @STORED_PROC_NAME @STATEMENTS

		--Note: Above exection returns standard rewardcode, we need to add 'X' to end of it to make it exception reward code
		UPDATE @EXCEPTIONS 	SET RewardCode = RewardCode + 'X'
			   
		-- LETS MAKE EXCPETIONS OBSOLETE IF ORGANIC REWARD PERCENTAGE IS MET OR SURPASSED EXCEPTION REWARD PERCENTAGE		
		UPDATE TGT
		SET [Status] = 'Obsolete', DateModified = GETDATE()
			OUTPUT inserted.[StatementCode], inserted.ID, 'Retailer no longer qualifies for exception','Field','Status',deleted.[Status],inserted.[Status]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_Exceptions TGT
			INNER JOIN @EXCEPTIONS SRC				
			ON SRC.StatementCode = TGT.StatementCode AND SRC.RewardCode=TGT.RewardCode		
		WHERE TGT.[Status] NOT IN ('Obsolete','Rejected','Void') 
				AND
			SRC.Current_Margin_A >= TGT.Exception_Reward_Margin_A
				AND
			 SRC.Current_Margin_B >= TGT.Exception_Reward_Margin_B

		-- LETS UPDATE REWARD VALUE TO ZERO FOR EXCEPTIONS THAT BECAME OBSOLETE
		UPDATE T1
		SET RewardValue = 0
		FROM RPWeb_Rewards_Summary T1
			INNER JOIN @EDIT_HISTORY EH			ON T1.StatementCode=EH.StatementCode
			INNER JOIN RPWeb_Exceptions EX		ON EX.ID = EH.[EHLinkCode] AND EX.RewardCode=T1.RewardCode
		WHERE EX.[Status]= 'Obsolete'
			  			   
		--  LETS REDUCE STATEMENTCODES TO REFRESH BY REMOVING EXCEPTIONS WHICH ARE ALREADY IN OBSOLETE STATUS
		-- LETS CHECK AND ADJUST EXCEPTION REWARD AMOUNTS

		DELETE TGT
		FROM @EXCEPTIONS TGT
			INNER JOIN RPWeb_Exceptions EX
			ON EX.StatementCode = TGT.StatementCode AND TGT.RewardCode = EX.RewardCode
		WHERE EX.[Status] IN ('Obsolete','Rejected','Void')

		DROP TABLE IF EXISTS #EXCEPTION_AMOUNTS 
		SELECT EX.ID			
			,IIF(EX.Exception_Reward_Margin_A <= 0, 0,ROUND(CAST(SRC.CY_Sales_A * (EX.Exception_Reward_Margin_A - SRC.Current_Margin_A) AS MONEY),2)) AS Exception_Value_A
			,IIF(EX.Exception_Reward_Margin_B <= 0, 0,ROUND(CAST(SRC.CY_Sales_B * (EX.Exception_Reward_Margin_B - SRC.Current_Margin_B) AS MONEY),2))  AS Exception_Value_B
			,CAST(0 AS Money) AS Exception_Value
		INTO #EXCEPTION_AMOUNTS
		FROM @EXCEPTIONS SRC			
			INNER JOIN RPWeb_Exceptions EX		
			ON EX.StatementCode = SRC.StatementCode AND EX.RewardCode=SRC.RewardCode		
				
		UPDATE #EXCEPTION_AMOUNTS 
		SET Exception_Value = Exception_Value_A + Exception_Value_B

		-- SEGMENT A EXCEPTION VALUE
		UPDATE TGT
		SET [Exception_Value_A] =  SRC.Exception_Value_A, DateModified = GETDATE()
			OUTPUT inserted.[StatementCode], inserted.ID, 'Retailer Exception Value (A) Adjusted','Field','Exception Value (A)',deleted.[Exception_Value_A],inserted.[Exception_Value_A]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_Exceptions TGT
			INNER JOIN #EXCEPTION_AMOUNTS SRC
			ON SRC.ID=TGT.ID
		WHERE TGT.Exception_Value_A <> SRC.Exception_Value_A

		DELETE EH
		FROM @EDIT_HISTORY EH
			INNER JOIN RPWeb_Exceptions EX
			ON EX.ID = EH.[EHLinkCode] AND EX.Exception_Reward_Margin > 0
		WHERE EH.[EHFieldName] = 'Exception Value (A)'  
		
		-- SEGMENT B EXCEPTION EXCEPTION VALUE
		UPDATE TGT
		SET [Exception_Value_B] =  SRC.Exception_Value_B, DateModified = GETDATE()
			OUTPUT inserted.[StatementCode], inserted.ID, 'Retailer Exception Value (B) Adjusted','Field','Exception Value (B)',deleted.[Exception_Value_B],inserted.[Exception_Value_B]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_Exceptions TGT
			INNER JOIN #EXCEPTION_AMOUNTS SRC
			ON SRC.ID=TGT.ID
		WHERE TGT.Exception_Value_B <> SRC.Exception_Value_B

		-- EXCEPTION VALUE
		UPDATE TGT
		SET [Exception_Value] =  SRC.Exception_Value, DateModified = GETDATE()
			OUTPUT inserted.[StatementCode], inserted.ID, 'Retailer Exception Value Adjusted','Field','Exception Value',deleted.[Exception_Value],inserted.[Exception_Value]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_Exceptions TGT
			INNER JOIN #EXCEPTION_AMOUNTS SRC
			ON SRC.ID=TGT.ID
		WHERE TGT.Exception_Value <> SRC.Exception_Value
						
		INSERT INTO @EXCEPTION_IDS(ID)
		SELECT [EHLinkCode] 
		FROM @EDIT_HISTORY
		WHERE [EHFieldName] Like 'Exception Value%'
		GROUP BY [EHLinkCode]
		
		EXEC RP_GenerateEditHistory 'Exception', @USERNAME ,@EDIT_HISTORY		
					   
		-- LETS UPDATE RPWEB REWARDS SUMMARY
		DROP TABLE IF EXISTS #APPROVED_EXCEPTIONS

		SELECT StatementCode, RewardCode, Exception_Value,Exception_Value_A,Exception_Value_B
		INTO #APPROVED_EXCEPTIONS
		FROM RPWeb_Exceptions  T1
			INNER JOIN @EXCEPTION_IDS T2	
			ON T2.ID = T1.ID
		WHERE T1.[Status] = 'Approved'  


		IF @SEASON >= 2021
		BEGIN
			SET @INVIGOR_MARKETLETTER_CODE  = 'ML' + @SEASON_STRING + '_W_INV'
			SET @CPP_MARKET_LETTER_CODE  = 'ML' + @SEASON_STRING + '_W_CPP'

			UPDATE TGT
			SET RewardValue = 
				CASE 
					WHEN TGT.RewardCode = 'INV_LLB_WX' AND TGT.MarketLetterCode = @INVIGOR_MARKETLETTER_CODE THEN SRC.Exception_Value_A
					WHEN TGT.RewardCode = 'INV_LLB_WX' AND TGT.MarketLetterCode = @CPP_MARKET_LETTER_CODE THEN  SRC.Exception_Value_B
					ELSE SRC.Exception_Value 
				END		
			FROM RPWeb_Rewards_Summary  TGT
				INNER JOIN #APPROVED_EXCEPTIONS SRC
				ON SRC.StatementCode=TGT.StatementCode AND SRC.RewardCode=TGT.RewardCode 				
		END

END

