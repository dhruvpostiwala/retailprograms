﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_CentAfterMarket]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
			
		-- DO NOT CACULATE THIS REWARD ON FORECAST AND PROJECTION STATEMETNS. AS THIS PROGRAM WAS NOT MENTIONED UNTIL CALCULTIONS FOR ACTUAL STATEMENTS 
		-- WE DO NOT WANT TO IMPACT TOTALS ON FORECAST AND PROJECTIONS STATEMENT WHERE THIS REWARD VALUE IS NOT DISPLAYED ON REWARDS PAGE
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN

	
		DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		RewardBrand_Sales [numeric](18, 2) NOT NULL DEFAULT 0,
		[Quantity] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC
	)
		)

	/*
	Reward Calculation Rules:
	
		Reward Brands: Centurion Brands

		Qualifying Level: Level 2. For standalone will be at level 1.
		Calculating Level: Level 2. For standalone will be at level 1.

		Reward Opportunity:	Retails earn $6.00/Centurion case

		Calculation:	$6.00 * 2020 Eligible Centurion cases
	*/
	--ML2020_W_CCP

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity)
	SELECT ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode
		,MAX(tx.GroupLabel) as RewardBrand
		,SUM(IIF(tx.GroupLabel='CENTURION',tx.Price_CY,0)) AS RewardBrand_Sales 
		,SUM(IIF(tx.GroupLabel='CENTURION',tx.Acres_CY/PR.ConversionW,0)) AS Quantity   
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupLabel ='CENTURION'
	GROUP BY ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode 

	UPDATE @TEMP 
	SET Reward = Quantity * 6.0   
	WHERE Quantity > 0

	DELETE FROM RP2020_DT_Rewards_W_Cent_AFM WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_Cent_AFM(StatementCode, MarketLetterCode, ProgramCode, RewardBrand,RewardBrand_Sales, Quantity, Reward)
	SELECT  StatementCode, MarketLetterCode, ProgramCode, RewardBrand,RewardBrand_Sales, Quantity, Reward
	FROM @TEMP
	
END


