﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_PortfolioSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ROUNDING_DOLLAR MONEY=5000;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		StatementType VARCHAR(50) NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)

	INSERT INTO @TEMP(StatementCode,StatementType,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY1,QualBrand_Sales_LY2)
	SELECT ST.StatementCode, RS.StatementType, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
		,SUM(tx.Price_CY) AS RewardBrand_Sales
		,SUM(TX.Price_CY) AS QualBrand_Sales_CY	
		,SUM(TX.Price_LY1) AS QualBrand_Sales_LY1
		,SUM(TX.Price_Q_SDP_LY2) AS QualBrand_Sales_LY2
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS
		ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
		INNER JOIN Productreference PR
		ON PR.ProductCode = tx.ProductCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ALL_EAST_BRANDS' 
	GROUP BY ST.StatementCode, RS.StatementType, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup


	DELETE FROM @TEMP
	WHERE	StatementType = 'ACTUAL'
	AND		RewardBrand IN 
			('ABSOLUTE', 'ACCELERON', 'ADRENALIN', 'BASF 28% UAN', 'CABRIO PLUS', 'CHARTER', 'CLEARFIELD COMMITMENT FEE', 'DUET', 'DYAX', 'EQUINOX'
			,'FACET L','FLAXMAX DLX', 'FLO RITE 1197', 'FLO RITE 1706', 'FLO RITE 3330', 'FRUIT & VEGETABLE APHICIDE', 'GEMINI', 'HEADLINE DUO'
			,'HEAT', 'HEAT COMPLETE', 'HEAT LQ', 'HONOR', 'ILEVO', 'IMPACT', 'INSURE Cereal', 'INSURE CEREAL FX4', 'INSURE Pulse', 'KUMULUS', 'LADDOK', 'LANCE AG'
			,'LIBERTY 150', 'LIBERTY LINK', 'MAKO', 'MIZUNA', 'NEMATAC C', 'NEW PRE-HARVEST HERBICIDE', 'NEXICOR'
			,'ODYSSEY', 'ODYSSEY DLX', 'ODYSSEY NXT', 'ODYSSEY ULTRA', 'ODYSSEY ULTRA NXT', 'POLYRAM', 'PRIAXOR DS', 'PROWL 400', 'PYRAMIN' ,'PURSUIT ULTRA'
			,'SALUTE', 'Seed Colorant', 'SEED ENHANCEMENT', 'SOLO', 'SOLO ADV', 'SOLO ULTRA', 'STAMINA', 'VIPER', 'VIPER ADV')
	

	/* OLD CODE
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY1,QualBrand_Sales_LY2)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
		,SUM(tx.Price_CY) AS RewardBrand_Sales
		,SUM(CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN (EI.FieldValue / 100) * TX.Price_CY ELSE Price_CY END) AS QualBrand_Sales_CY	
		,SUM(TX.Price_LY1) AS QualBrand_Sales_LY1
		,SUM(TX.Price_LY2) AS QualBrand_Sales_LY2
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS
		ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
		INNER JOIN Productreference PR
		ON PR.ProductCode = tx.ProductCode
		INNER JOIN RPWeb_USER_EI_FieldValues EI
		ON ST.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ALL_EAST_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
	*/
	-- Determine growth based on the sum of brand sales at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY1,QualBrand_Sales_LY2)
	SELECT IIF(DataSetCode > 0,DataSetCode,StatementCode) AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(RewardBrand_Sales) AS RewardBrand_Sales,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY,SUM(QualBrand_Sales_LY1) AS QualBrand_Sales_LY1,SUM(QualBrand_Sales_LY2) AS QualBrand_Sales_LY2
	FROM @TEMP
	GROUP BY IIF(DataSetCode > 0,DataSetCode,StatementCode), MarketLetterCode, ProgramCode



	UPDATE @TEMP_TOTALS
	SET Growth = 
			CASE 
				WHEN QualBrand_Sales_LY1 <= 0 AND QualBrand_Sales_LY2 <= 0 THEN 
					CASE 
						WHEN QualBrand_Sales_CY <= 0 THEN 0 
						ELSE 9.9999 
					END 
				WHEN QualBrand_Sales_LY1 > 0 AND QualBrand_Sales_LY2 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY1
				WHEN QualBrand_Sales_LY2 > 0 AND QualBrand_Sales_LY1 <= 0 THEN QualBrand_Sales_CY/QualBrand_Sales_LY2
				ELSE QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2)
			END

	-- Update the reward %
	DECLARE @MIN_SALES INT = 100000
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.06 WHERE [QualBrand_Sales_CY] + @ROUNDING_DOLLAR >= @MIN_SALES AND ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth >= 1)
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.04 WHERE [QualBrand_Sales_CY] + @ROUNDING_DOLLAR >= @MIN_SALES AND [Reward_Percentage] = 0 AND Growth >= 0.95
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.03 WHERE [QualBrand_Sales_CY] + @ROUNDING_DOLLAR >= @MIN_SALES AND [Reward_Percentage] = 0 AND Growth >= 0.90
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.01 WHERE [QualBrand_Sales_CY] + @ROUNDING_DOLLAR >= @MIN_SALES AND [Reward_Percentage] = 0 AND Growth >= 0.80

	-- Update the growth % and reward % back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward % back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0

	-- UPDATE REWARD 
	UPDATE T1
	SET T1.Reward = RewardBrand_Sales * Reward_Percentage
	FROM @TEMP T1


	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		Reward = 0
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode
	WHERE	T2.Statement_Reward < 0


	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE  T1
		SET RewardBrand_Sales *= ISNULL(EI.FieldValue/100,1) 
		FROM @TEMP T1
		INNER JOIN RPWeb_USER_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
	END

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2021_DT_Rewards_E_PortfolioSupport WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2021_DT_Rewards_E_PortfolioSupport(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
		,SUM(QualBrand_Sales_LY1) AS QualBrand_Sales_LY1
		,SUM(QualBrand_Sales_LY2) AS QualBrand_Sales_LY2
		,AVG(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
END


--select * from RP2021_DT_Rewards_E_PortfolioSupport where StatementCode = 7874