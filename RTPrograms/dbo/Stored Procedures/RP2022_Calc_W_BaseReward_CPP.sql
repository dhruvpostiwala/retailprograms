﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_BaseReward_CPP] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;	
	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'FORECAST'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_BASE_REWARD_CPP'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (7984)
	*/

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code  VARCHAR (10) NOT NULL DEFAULT '',
		RewardSegment  VARCHAR (100) NOT NULL DEFAULT '',
		RewardBrand  VARCHAR (100) NOT NULL DEFAULT '',
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[Level5Code] ASC,
			[RewardSegment] ASC,
			[RewardBrand] ASC
		)
	)


	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Get data for all the statements
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardSegment, RewardBrand, RewardBrand_Sales)
		SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, TX.GroupLabel, PR.ChemicalGroup, SUM(TX.Price_CY)
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
		WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType IN ('REWARD_BRANDS')
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code,TX.GroupLabel, PR.ChemicalGroup
	END


	--IF @STATEMENT_TYPE IN ('ACTUAL')
	--BEGIN
		--TBD
	--END
	

	--UPDATE Reward_Percentage in the TEMP table
	UPDATE Temp 
	 	SET Reward_Percentage = 
		CASE WHEN Level5Code = 'D0000117' THEN
			CASE RewardSegment 
				WHEN 'IMI Herbicides' THEN .05
				WHEN 'Liberty 150' THEN .063
				WHEN 'Centurion' THEN 0
				WHEN 'Legacy CPP' THEN .035
				WHEN 'Inoculants' THEN 0
				ELSE 0 
			END
		ELSE 
			CASE RewardSegment 
				WHEN 'IMI Herbicides' THEN .1
				WHEN 'Liberty 150' THEN .07
				WHEN 'Centurion' THEN .0325
				WHEN 'Legacy CPP' THEN .0725
				WHEN 'Inoculants' THEN .0625
				ELSE 0 
			END
		END
	FROM @TEMP Temp       
				 
	-- Set the reward for all the statements based on the reward percentage set previously
	UPDATE @TEMP SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE [RewardBrand_Sales] > 0	
	
	DELETE FROM RP2022_DT_Rewards_W_BaseReward_CPP WHERE StatementCode IN (SELECT StatementCode FROM @Statements)	
	INSERT INTO RP2022_DT_Rewards_W_BaseReward_CPP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardSegment, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardSegment, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

END
