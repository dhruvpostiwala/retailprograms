﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_Rewards_Summary_West] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 109

	DELETE REW
	FROM RP_DT_Rewards_Summary REW
	INNER JOIN @STATEMENTS ST
	ON ST.StatementCode=REW.StatementCode
		
	/*
	RP2020_W_INV_LIB_LOY_BOUNS
	RP2020_W_INV_PERFORMANCE
	*/

	-- INVIGOR LIBERTY LOYALTY BONUS
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_INV_LLB RS		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_INV_LIB_LOY_BONUS'

	-- INVIGOR PERFORMANCE		
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_INV_PERF RS		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_INV_PERFORMANCE'
	
	-- EFFICIENCY REBATE
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_EFF_REBATE RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_EFF_REBATE'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- BRAND SPECIFIC SUPPORT
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_BrandSpecificSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT'

	-- PAYMENT ON TIME
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_PaymentOnTime RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_PAYMENT_ON_TIME'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- WAREHOUSE PAYMENT
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_WarehousePayments RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_WAREHOUSE_PAYMENT'

	-- Liberty/ Centurion / Facet L Tank Mix Bonus 
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_TankMixBonus RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_TANK_MIX_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Planning the Business Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_PlanningTheBusiness RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_PLANNING_SUPP_BUSINESS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	
	-- AgRewards Bonus
		-- ACTUALS
		INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
		SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
		FROM @STATEMENTS ST		
			INNER JOIN RP_Statements S							ON S.Statementcode=ST.Statementcode 
			INNER JOIN RP_DT_ML_ELG_Programs ELG				ON ELG.StatementCode=ST.StatementCode 
			INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
			LEFT JOIN RP2020_DT_Rewards_W_AgRewBonus_Actual RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
		WHERE S.StatementType='Actual' AND ELG.ProgramCode='RP2020_W_AGREWARDS_BONUS'  
		GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		--FORECAST AND PROJECTION STATEMENTS
		INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
		SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements S						ON S.Statementcode=ST.Statementcode 
			INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
			INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
			LEFT JOIN RP2020_DT_Rewards_W_AgRrewardsBonus RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
		WHERE S.StatementType IN ('Forecast','Projection') AND ELG.ProgramCode='RP2020_W_AGREWARDS_BONUS'
		GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	
	-- Portfolio Achievement Bonus
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_PortfolioAchievement RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_PORFOLIO_ACHIEVEMENT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Inventory Management
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_InventoryManagement RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_INVENTORY_MGMT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Truckload Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_TruckloadReward RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_TRUCKLOAD_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Retail Bonus Payment
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_RetailBonus RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_RETAIL_BONUS_PAYMENT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Custom Seed Treating Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_CustomSeed RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Soybean/Pulse Planning & Support Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_W_SoybeanPulsePlanning RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_W_SOYBEAN_PULSE_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- Clearfield Lentils Support Reward
		-- FORECAST AND PROJECTION
		INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
		SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements S						ON S.Statementcode=ST.Statementcode 
			INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
			INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
			LEFT JOIN RP2020_DT_Rewards_W_ClearfieldLentils RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
		WHERE S.StatementType IN ('FORECAST','PROJECTION') AND  ELG.ProgramCode='RP2020_W_CLL_SUPPORT'
		GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		-- ACTUAL STATEMENTS
		INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
		
		SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements S						ON S.Statementcode=ST.Statementcode 
			INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
			INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
			LEFT JOIN (
				SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
				FROM RP2020_DT_Rewards_W_CLL_Comm_Actual 
					
					UNION ALL

				SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
				FROM RP2020_DT_Rewards_W_CLL_Herb_Actual 					
			) RS	
			ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
		WHERE S.StatementType='Actual' AND ELG.ProgramCode='RP2020_W_CLL_SUPPORT'
		GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	
	---- Clearfield Wheat Support Reward --Nutrien
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
		
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements S						ON S.Statementcode=ST.Statementcode 
		INNER JOIN RP_DT_ML_ELG_Programs ELG			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN (
			SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
			FROM RP2020_DT_Rewards_W_CLW_Comm
					
				UNION ALL

			SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
			FROM RP2020_DT_Rewards_W_CLW_Herb
		) RS	
		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE S.StatementType='Actual' AND ELG.ProgramCode='RP2020_W_CLW_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	

	-- Centurion After Market Payment	
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN  RP2020_DT_Rewards_W_Cent_AFM RS		ON RS.StatementCode=ST.StatementCode 
	WHERE ELG.ProgramCode='RP2020_W_CENT_AFTER_MARKET'

	-- Liberty After Market Payment	
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN  RP2020_DT_Rewards_W_Lib_AFM RS		ON RS.StatementCode=ST.StatementCode 
	WHERE ELG.ProgramCode='RP2020_W_LIB_AFTER_MARKET'

	-- Sales Forecast Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		INNER JOIN  RP2020_DT_Rewards_W_Lib_SalesForecast RS		ON RS.StatementCode=ST.StatementCode 
	WHERE ELG.ProgramCode='RP2020_W_LIB_SALES_FORECAST'

	-- Sales Forecast Execution Reward
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		INNER JOIN  RP2020_DT_Rewards_W_Lib_SalesForecastExec RS		ON RS.StatementCode = ST.StatementCode 
	WHERE ELG.ProgramCode='RP2020_W_LIB_SALES_FORECAST_EXEC'

		--Titan COmpetitive Program
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, RS.MarketLetterCode AS MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP2020_DT_Rewards_W_TitanCompProg RS		ON RS.StatementCode = ST.StatementCode 
		LEFT JOIN RP_Config_Programs PRG						ON PRG.ProgramCode = RS.ProgramCode

	--NODULATOR DUO SCG REWARD PROGRAM
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, RS.MarketLetterCode AS MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN (
				SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
				FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog
					
					UNION ALL

				SELECT Statementcode, MarketLetterCode, ProgramCode, Reward
				FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog_NewGrowers
				) RS								ON RS.StatementCode=ST.StatementCode 
		LEFT JOIN RP_Config_Programs PRG			ON PRG.ProgramCode = RS.ProgramCode
	GROUP BY ST.StatementCode, RS.MarketLetterCode, PRG.RewardCode

	--Custom Aerial Application
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, RS.MarketLetterCode AS MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP2020_DT_Rewards_W_CustomAerial RS		ON RS.StatementCode = ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode = RS.ProgramCode 
	GROUP BY ST.StatementCode, RS.MarketLetterCode, PRG.RewardCode 

	--Incentive
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 
	'ML2020_W_ADD_PROGRAMS'AS MarketLetterCode, 
	'Incentives_W'AS RewardCode
	,ISNULL(SUM(RI.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP_DT_Incentives RI							ON RI.StatementCode = ST.StatementCode 
	GROUP BY ST.StatementCode

	--WholeSale Transition Program
		--RP 2955
		--The reward Wholesaler Transition Program is not eligible for West line companies
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, RS.MarketLetterCode AS MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP2020_DT_Rewards_W_AG_WS_TransitProg RS		ON RS.StatementCode = ST.StatementCode 
		INNER JOIN RPWeb_Statements	RWS								ON RWS.StatementCode = ST.StatementCode
		INNER JOIN RP_Config_Programs PRG							ON PRG.ProgramCode = RS.ProgramCode
	WHERE RWS.RetailerCode NOT IN ('D0000107', 'D0000137', 'D0000244', 'D520062427')
	
	--APA Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2020_W_ADD_PROGRAMS' AS MarketLetterCode, APA.RewardCode AS RewardCode
		,ISNULL(SUM(APA.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
	INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
	INNER JOIN RP_Seeding_APAPayments APA ON APA.RetailerCode = MAP.RetailerCode AND RS.Season = APA.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'WEST'
	GROUP BY ST.StatementCode, APA.RewardCode
	
	
	--Over/UnderPayments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2020_W_ADD_PROGRAMS'AS MarketLetterCode, OUP.RewardCode AS RewardCode
	,ISNULL(SUM(OUP.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
	INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
	INNER JOIN RP_Seeding_Over_Under_Payments OUP ON OUP.RetailerCode = MAP.RetailerCode AND RS.Season = OUP.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'WEST'
	GROUP BY ST.StatementCode, OUP.RewardCode
	

	--Over/UnderPayments LEVEL 5 STATEMENTS
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2020_W_ADD_PROGRAMS'AS MarketLetterCode, OUP.RewardCode AS RewardCode
	,ISNULL(SUM(OUP.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode AND StatementLevel = 'LEVEL 5'
	INNER JOIN RP_Seeding_Over_Under_Payments OUP ON OUP.RetailerCode = RS.RetailerCode AND RS.Season = OUP.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'WEST'
	GROUP BY ST.StatementCode, OUP.RewardCode


	/*
	-- SUMMARIZE REWARDS AT HEAD OFFICE LEVEL
	-- FCL -- ROLL UP REWARDS TO LEVEL 5 STATEMENT	
	DECLARE @HO_STATEMENTS TABLE(
		StatementCode INT NOT NULL,
		StatementCode_L5 INT NOT NULL
	)

	INSERT INTO @HO_STATEMENTS(StatementCode,StatementCode_L5)
	SELECT DISTINCT ST2.StatementCode, ST2.DataSetCode_L5 AS StatementCode_L5	
	FROM @STATEMENTS ST1
		INNER JOIN RP_Statements ST2				ON ST2.StatementCode=ST1.StatementCode		
		INNER JOIN RP_Config_HOPaymentLevel HO 		ON HO.Level5Code=ST2.Level5Code AND HO.Season=ST2.Season
	WHERE HO.Summary='Yes'

	DELETE REW
	FROM RP_DT_Rewards_Summary REW		
		INNER JOIN (
			SELECT DISTINCT StatementCode_L5 AS Statementcode FROM @HO_STATEMENTS
		) T2
		ON T2.Statementcode=REW.Statementcode
		
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode,  SUM(REW.RewardValue), 'Eligible' , ''
	FROM @HO_STATEMENTS HO
		INNER JOIN RP_DT_Rewards_Summary REW	
		ON REW.StatementCode=HO.StatementCode	
	WHERE RewardEligibility='Eligible'
	GROUP BY HO.StatementCode_L5, REW.MarketLetterCode, REW.RewardCode
	*/

END
