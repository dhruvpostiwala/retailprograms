﻿
CREATE PROCEDURE [dbo].[RP_Refresh_HO_RewardsSummary] @SEASON INT, @STATEMENT_TYPE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @HO_SUMMARY_STATEMENTS AS UDT_RP_STATEMENT
		
	DECLARE @LOCATION_STATEMENTS TABLE (
		StatementCode INT NOT NULL,
		DataSetCode_L5 INT NOT NULL
	)
	
	INSERT INTO @HO_SUMMARY_STATEMENTS(StatementCode)
	SELECT ST.DataSetCode_L5
	FROM @STATEMENTS T1		
		INNER JOIN RP_Statements ST					ON ST.Statementcode=T1.Statementcode
		INNER JOIN RP_Config_HOPaymentLevel HO 		ON HO.Level5Code=ST.Level5Code AND HO.Season=ST.Season 		
	WHERE ST.[Status]='Active' AND ST.StatementType=@STATEMENT_TYPE AND ST.DataSetCode_L5 > 0 AND HO.Summary='Yes' 
	GROUP BY ST.DataSetCode_L5
	 	 	  
	IF NOT EXISTS(SELECT * FROM @HO_SUMMARY_STATEMENTS)
	RETURN
		
	INSERT INTO @LOCATION_STATEMENTS(Statementcode ,DataSetCode_L5)
	SELECT ST.StatementCode, ST.DataSetCode_L5	
	FROM RP_Statements ST
		INNER JOIN @HO_SUMMARY_STATEMENTS HO
		ON ST.DataSetCode_L5 = HO.StatementCode
	WHERE ST.[Status]='Active' AND ST.[VersionType]='Unlocked' AND (ST.Region='WEST' OR ST.[StatementLevel]='LEVEL 1') 	 
						   		 	  	  	 
	-- SUMMARIZE REWARDS AT HEAD OFFICE LEVEL
	DELETE REW
	FROM RPWeb_Rewards_Summary REW
		INNER JOIN @HO_SUMMARY_STATEMENTS HO
		ON REW.Statementcode = HO.StatementCode
								
	INSERT INTO RPWeb_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardEligibility,RewardMessage,RewardValue,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)
	SELECT ST.DataSetCode_L5, REW.MarketLetterCode, REW.RewardCode
		,'Eligible' AS RewardEligibility
		, '' AS RewardMessage
		,SUM(REW.RewardValue) AS [RewardValue]
		,SUM(TotalReward) AS TotalReward
		,SUM(PaidToDate) AS PaidToDate
		,SUM(CurrentPayment) AS CurrentPayment
		,SUM(NextPayment) AS NextPayment
		,SUM(PaymentAmount) AS PaymentAmount
	FROM @LOCATION_STATEMENTS ST
		INNER JOIN RPWeb_Rewards_Summary REW		
		ON REW.StatementCode=ST.StatementCode	
	GROUP BY ST.DataSetCode_L5, REW.MarketLetterCode, REW.RewardCode
	
END
