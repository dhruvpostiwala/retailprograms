﻿


CREATE PROCEDURE [dbo].[RP_Calc_Rewards_Summary] @SEASON INT, @REGION VARCHAR(4), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [RP_DT_Rewards_Summary]
	WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)
	
	IF(@SEASON=2020 AND @REGION='West') EXEC [RP2020_Calc_Rewards_Summary_West] @STATEMENTS 
	IF(@SEASON=2020 AND @REGION='East') EXEC [RP2020_Calc_Rewards_Summary_East] @STATEMENTS

	IF(@SEASON=2021 AND @REGION='West')
	BEGIN
		EXEC [RP2021_Calc_Rewards_Summary_West] @STATEMENTS
		EXEC [dbo].[RP2021_Calc_RewardsByGroup] @STATEMENTS
	END
	ELSE IF(@SEASON=2021 AND @REGION='East')
	BEGIN
		EXEC [RP2021_Calc_Rewards_Summary_East] @STATEMENTS
		EXEC [dbo].[RP2021_Calc_RewardsByGroup] @STATEMENTS
	END
	
	IF(@SEASON=2022 AND @REGION='West')
	BEGIN
		EXEC [RP2022_Calc_Rewards_Summary_West] @STATEMENTS
		--EXEC [dbo].[RP2022_Calc_RewardsByGroup] @STATEMENTS
	END
	ELSE IF(@SEASON=2022 AND @REGION='East')
	BEGIN
		EXEC [RP2022_Calc_Rewards_Summary_East] @STATEMENTS
		--EXEC [dbo].[RP2022_Calc_RewardsByGroup] @STATEMENTS
	END
	
/*		
	/* EXCEPTIONS */	
	INSERT INTO GP_RewardSummary([StatementCode],[RewardCode],[RewardEligibility],[RewardValue],[RewardMessage])
	SELECT ex.[StatementCode],'EX_'+COALESCE(pc.[RewardSummaryCode],'OTHER') AS [RewardCode],'ELIGIBLE',SUM(ex.[Amount]) AS [Amount]
		,STUFF(( 
			SELECT ', '+[OtherDesc]
			FROM GPWeb_ExceptionStatements exst
			WHERE [Status]='Approved' AND [ProgramCode] IS NULL AND exst.[StatementCode]=ex.[StatementCode]
			FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,2,'')
	FROM GPWeb_ExceptionStatements ex
		LEFT JOIN GP_ProgramConfig pc
		ON pc.[ProgramCode]=ex.[ProgramCode]
		INNER JOIN @STATEMENTS st
		ON st.[StatementCode]=ex.[StatementCode]
	WHERE ex.[Status]='Approved'	
	GROUP BY ex.[StatementCode],pc.[RewardSummaryCode]
	
	/* OVERPAYMENTS */
	INSERT INTO GP_RewardSummary([StatementCode],[RewardCode],[RewardEligibility],[RewardValue])
	SELECT op.[StatementCode],'OVERPAYMENT','ELIGIBLE',SUM(op.[Amount])*-1
	FROM GPWeb_OverpaymentStatements op
		INNER JOIN @STATEMENTS st
		ON st.[StatementCode]=op.[StatementCode]
	GROUP BY op.[StatementCode]
*/
END







