﻿
CREATE   PROCEDURE dbo.ADHoc_pullReport @SEASON INT, @REGION VARCHAR(4)
AS
BEGIN
SET NOCOUNT ON;

/*
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	--This store procedure gives output back in HTML format
	--Needs to be converted into a .csv file
	--To convert html to CSV. Can use website:
	-- https://www.convertcsv.com/html-table-to-csv.htm
	DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @RETAILER_NAME NVARCHAR(MAX);
	declare @STATEMENTCODE INT;

	DECLARE @THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @TBODY_ROWS NVARCHAR(MAX)=''
	DECLARE @TOTAL_ROW NVARCHAR(MAX)=''

	DECLARE @PULL_REPORT_SECTION NVARCHAR(MAX)=''

	DROP TABLE IF EXISTS #TEMPP

	SELECT St.StatementCode, St.StatementLevel, RP.RetailerCode, RP.RetailerName as Retailer_Name, St.DataSetCode
	INTO #TEMPP
	FROM RPWEB_STATEMENTS ST
		LEFT JOIN RetailerProfile RP	
		ON RP.Retailercode=st.RetailerCode 
	WHERE ST.STATUS = 'Active' 
	AND Season = 2020 
	AND st.Region = 'East'
	AND Month(Locked_Date) IN (10,11)
	AND VersionType IN ('Locked','Archive')

	SET @THEAD_ROW='<tr>
		<thead>
			<th>Operating Unit</th>
			<th>Retailer</th>
			<th>Retailer Code</th>
		</thead>
		</tr>'

	SELECT * FROM #TEMPP

	DECLARE RETAILER_LOOP CURSOR FOR 
		SELECT DISTINCT  StatementCode, RetailerCode, Retailer_Name FROM #TEMPP WHERE StatementLevel = 'Level 2' ORDER BY Retailer_Name
	OPEN RETAILER_LOOP
	FETCH NEXT FROM RETAILER_LOOP INTO @STATEMENTCODE, @RETAILERCODE, @RETAILER_NAME
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			
			SET @TBODY_ROWS += '<tr class="sub_total_row"><td colspan=1>' + @RETAILER_NAME + ' - ' + @RETAILERCODE + '</td><td colspan = 2></td></tr>'

			SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
					(select '' AS 'td' for xml path(''), type)
					,(select Retailer_Name AS 'td' for xml path(''), type)
					,(select RetailerCode AS 'td' for xml path(''), type)					
				FROM #TEMPP
				WHERE DataSetCode=@STATEMENTCODE AND StatementLevel = 'Level 1'
				ORDER BY Retailer_Name
				FOR XML PATH('tr')	, ELEMENTS, TYPE))

		FETCH NEXT FROM RETAILER_LOOP INTO @STATEMENTCODE, @RETAILERCODE, @RETAILER_NAME
		END
	CLOSE RETAILER_LOOP
	DEALLOCATE RETAILER_LOOP

	SET @TBODY_ROWS += '<tr class="sub_total_row"><td colspan=1>STANDALONES</td><td colspan = 2></td></tr>'

	SET @TBODY_ROWS += CONVERT(NVARCHAR(MAX),(SELECT 	
			(select '' AS 'td' for xml path(''), type)
			,(select Retailer_Name AS 'td' for xml path(''), type)
			,(select RetailerCode AS 'td' for xml path(''), type)					
		FROM #TEMPP
		WHERE DataSetCode=0 AND StatementLevel = 'Level 1'
		ORDER BY Retailer_Name
		FOR XML PATH('tr')	, ELEMENTS, TYPE))

	SET @PULL_REPORT_SECTION='<div class="st_section">
			<div class = "st_header">
				<span>Pull Report - ' + 'East' + '</span>
				<div class="st_toggle collapse" style="display: block;" onclick="st_toggle(this);"></div>
				<span class="st_right"</span>
			</div>
			<div class="st_content open">					
				<table class="rp_report_table">' + @THEAD_ROW + @TBODY_ROWS + '</table>				
			</div>
		</div>'

	SELECT @PULL_REPORT_SECTION
*/

DECLARE @RETAILERCODE VARCHAR(20);
	DECLARE @RETAILER_NAME NVARCHAR(MAX);
	declare @STATEMENTCODE INT;

	DECLARE @THEAD_ROW NVARCHAR(MAX)='';
	DECLARE @TBODY_ROWS NVARCHAR(MAX)=''
	DECLARE @TOTAL_ROW NVARCHAR(MAX)=''

	DECLARE @PULL_REPORT_SECTION NVARCHAR(MAX)=''

	DECLARE @TEMP TABLE (
		OU VARCHAR(50) NOT NULL,
		Retailer VARCHAR(50) NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL
	)

	DROP TABLE IF EXISTS #TEMPP

	SELECT St.StatementCode, St.StatementLevel, RP.RetailerCode, RP.RetailerName as Retailer_Name, St.DataSetCode
	INTO #TEMPP
	FROM RPWEB_STATEMENTS ST
		LEFT JOIN RetailerProfile RP	
		ON RP.Retailercode=st.RetailerCode 
	WHERE ST.STATUS = 'Active' 
	AND Season = @SEASON 
	AND st.Region = @REGION
	AND Month(Locked_Date) IN (10,11)
	AND VersionType IN ('Locked','Archive')

	DECLARE RETAILER_LOOP CURSOR FOR 
		SELECT DISTINCT  StatementCode, RetailerCode, Retailer_Name FROM #TEMPP WHERE StatementLevel = 'Level 2' ORDER BY Retailer_Name
	OPEN RETAILER_LOOP
	FETCH NEXT FROM RETAILER_LOOP INTO @STATEMENTCODE, @RETAILERCODE, @RETAILER_NAME
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			
			SET @TBODY_ROWS += '<tr class="sub_total_row"><td colspan=1>' + @RETAILER_NAME + ' - ' + @RETAILERCODE + '</td><td colspan = 2></td></tr>'

			INSERT INTO @TEMP(OU, Retailer, RetailerCode)
			SELECT @RETAILER_NAME + ' - ' + @RETAILERCODE, '', ''

			INSERT INTO @TEMP(OU, Retailer, RetailerCode)
			SELECT '', Retailer_Name, RetailerCode
			FROM #TEMPP
			WHERE DataSetCode=@STATEMENTCODE AND StatementLevel = 'Level 1'
			ORDER BY Retailer_Name

		FETCH NEXT FROM RETAILER_LOOP INTO @STATEMENTCODE, @RETAILERCODE, @RETAILER_NAME
		END
	CLOSE RETAILER_LOOP
	DEALLOCATE RETAILER_LOOP

	INSERT INTO @TEMP(OU, Retailer, RetailerCode)
	SELECT 'STANDALONE', '', ''

	INSERT INTO @TEMP(OU, Retailer, RetailerCode)
	SELECT '', Retailer_Name, RetailerCode
	FROM #TEMPP
	WHERE DataSetCode=0 AND StatementLevel = 'Level 1'
	ORDER BY Retailer_Name

	SELECT * FROM @TEMP

	END
