﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_W_InvLibertyLoyaltyBonus]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,		
		Liberty_Qual_Acres NUMERIC(18,2) NOT NULL,
		Invigor_Acres NUMERIC(18,2) NOT NULL,
		Reseeded_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		RewardBrand Varchar(100) NOT NULL,				
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @REWARD_PERCENTAGE FLOAT=0.02;

	/*
	Calculation Example
		Retail sells 400 bags of InVigor plus 144 jugs of Liberty, 15 x 108L drums and 7 x 432 totes.
		Total jug equivalents sold 144 + 120 + 224 = 488 jug eq
		488/400 = 1.22 Liberty to InVigor Ratio; retail qualifies 2.0% off all InVigor and Liberty purchased.	
	*/

	/*
		LIBERTY 1 JUG, CONV: 10,  10 ACRES
		INVIGOR 1 BAG, CONV: 10,  10 ACRES		
	*/
	/*
		ML2020_W_CCP	RP2020_W_INV_LIB_LOY_BONUS
		ML2020_W_INV	RP2020_W_INV_LIB_LOY_BONUS

		REWARD_BRANDS
		QUALIFYING_BRANDS
	*/
	
	DROP TABLE IF EXISTS #SUMMARY
	SELECT ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode
		,SUM(IIF(tx.MarketLetterCode='ML2020_W_CCP' AND PR.ChemicalGroup = 'LIBERTY 150',tx.Acres_CY,0)) AS Liberty_Qual_Acres
		,SUM(IIF(tx.MarketLetterCode='ML2020_W_INV' AND PR.ChemicalGroup = 'INVIGOR',tx.Acres_CY,0)) AS Invigor_Acres
		,SUM(IIF(tx.MarketLetterCode='ML2020_W_CCP' AND PR.ChemicalGroup = 'LIBERTY 150',tx.Price_CY,0)) AS Liberty_150_Sales
		,SUM(IIF(tx.MarketLetterCode='ML2020_W_CCP' AND PR.ChemicalGroup = 'LIBERTY 200',tx.Price_CY,0)) AS Liberty_200_Sales
		,SUM(IIF(tx.MarketLetterCode='ML2020_W_INV' AND PR.ChemicalGroup = 'INVIGOR',tx.Price_CY,0)) AS Invigor_Sales
		,MAX(tx.GroupLabel) RewardBrand
		,MAX(tx.Level5Code) Level5Code
		,MAX(RP.StatementLevel) StatementLevel
	INTO #SUMMARY
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_STATEMENTS RP						ON TX.StatementCode= RP.StatementCode
		INNER JOIN ProductReference PR on PR.ProductCode = tx.ProductCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode,TX.MarketLetterCode,TX.ProgramCode
	   		 
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Liberty_Qual_Acres, Invigor_Acres,RewardBrand,RewardBrand_Sales)	
	SELECT ST.StatementCode, Elg.MarketLetterCode, Elg.ProgramCode as ProgramCode
		,MAX(S.Liberty_Qual_Acres) AS Liberty_Qual_Acres
		,MAX(S.Invigor_Acres) AS Invigor_Acres		
		,MAX(IIF(ELG.MarketLetterCode = 'ML2020_W_CCP','LIBERTY','INVIGOR')) AS RewardBrand		
		, CASE WHEN Level5Code = 'D0000117' OR StatementLevel = 'LEVEL 5'  THEN ---COOP AND WEST LINE COMPANIES
			MAX(IIF(ELG.MarketLetterCode = 'ML2020_W_INV',s.Invigor_Sales,(s.Liberty_200_Sales + s.Liberty_150_Sales)))   --- ALL LIBERTY
			ELSE  ---IND WEST LINE ETC
			MAX(IIF(ELG.MarketLetterCode = 'ML2020_W_INV',s.Invigor_Sales,s.Liberty_150_Sales))
		END
		AS RewardBrand_Sales			 	
	FROM @STATEMENTS ST 
		INNER JOIN RP_DT_ML_Elg_Programs ELG	ON ELG.Statementcode=ST.Statementcode
		INNER JOIN #SUMMARY S	ON S.Statementcode=ST.Statementcode
	WHERE ELG.ProgramCode=@PROGRAM_CODE
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, Elg.ProgramCode, Level5Code,StatementLevel
	
	-- LETS ADJUST INVIGOR ACRES BY DEDUCTING RESEEEDED ACRES
	UPDATE T1
	SET T1.Reseeded_Acres = T2.Reseeded_Acres
		,T1.Invigor_Acres = T1.Invigor_Acres-T2.Reseeded_Acres
	FROM @TEMP T1
		INNER JOIN (
			SELECT ST.StatementCode, SUM(APA.ProblemAcres) AS Reseeded_Acres
			FROM @STATEMENTS ST
				INNER JOIN [dbo].[RP_StatementsMappings] MAP
				ON MAP.StatementCode=ST.StatementCode
				INNER JOIN (
					SELECT DealerCode AS RetailerCode, ProblemAcres
					FROM APA
					WHERE SoftDeleted='No' AND [Year]=@SEASON AND NatureOfInquiry='INVIGOR RESEED' AND [Status] <> 'DRAFT' AND [DealerCode] <> '' AND ProblemAcres > 0		
				) APA
				ON APA.RetailerCode=MAP.RetailerCode
			GROUP BY ST.Statementcode				
		) T2
		ON T2.Statementcode=T1.Statementcode
									
	UPDATE @TEMP 
	SET [Reward_Percentage]=@REWARD_PERCENTAGE 
		,[Reward]=[RewardBrand_Sales] * @REWARD_PERCENTAGE
	WHERE Liberty_Qual_Acres > 0 AND Invigor_Acres > 0 AND Liberty_Qual_Acres >= Invigor_Acres    --qualification

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	DELETE FROM RP2020_DT_Rewards_W_INV_LLB WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_INV_LLB(StatementCode,MarketLetterCode,ProgramCode,Liberty_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward,Reseeded_Acres)
	SELECT StatementCode,MarketLetterCode,ProgramCode,Liberty_Qual_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward, Reseeded_Acres
	FROM @TEMP

END
