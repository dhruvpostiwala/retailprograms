﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_LogisticsSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		BrandSegment VARCHAR(50) NOT NULL,
		Segment_Reward_Sales MONEY NOT NULL,
		LY_Sales MONEY NOT NULL DEFAULT 0,
		POG_Plan MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,4) NOT NULL DEFAULT 0,
		Segment_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[BrandSegment] ASC
		)
	);

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,		
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL,
		SWP MONEY NOT NULL,
		Price MONEY NOT NULL	
	)

	DECLARE @UNLOCKED_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @USE_CASE_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @PLAN_CUT_OFF_DATE AS DATE

	DECLARE @BASELINE_PERCENTAGE DECIMAL(6,4) = 0.01;

	DECLARE @GROWTH_TABLE TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,			
		Statement_LY_Sales MONEY NOT NULL,
		Statement_POG_Plan MONEY NOT NULL
	)	

	
	SET @PLAN_CUT_OFF_DATE=CAST('2021-02-06' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED 

	DROP TABLE IF EXISTS #RC_Statements
	SELECT ST.StatementCode, RPS.RetailerCode
	INTO #RC_Statements
	FROM @STATEMENTS ST
	    INNER JOIN RP_Statements RPS
		ON ST.StatementCode = RPS.StatementCode
	WHERE RPS.Level5Code='D000001'


	DROP TABLE IF EXISTS #LC_Statements
	SELECT ST.StatementCode, RPS.RetailerCode
	INTO #LC_Statements
	FROM @STATEMENTS ST
	    INNER JOIN RP_Statements RPS
		ON ST.StatementCode = RPS.StatementCode
	WHERE RPS.Level5Code IN ('D0000107','D0000137','D0000244','D520062427')

	--------------------------------------------------------------------------
	
	   	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN

	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel,
			SUM(TX.Price_CY) [Segment_Reward_Sales]
			,SUM(TX.Price_LY1) LY_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
	GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel


	-- 1% default reward if >= 90% growth YOY on CPP brands
	UPDATE T1
	SET T1.Segment_Reward_Percentage = CASE WHEN GR.Growth_Percentage > 0.90 THEN @BASELINE_PERCENTAGE ELSE 0 END
	FROM @TEMP T1
		INNER JOIN (
			SELECT ST.StatementCode,
				   CASE WHEN SUM(ISNULL(TX.Price_LY1, 0)) > 0 THEN SUM(ISNULL(TX.Price_CY, 0)) / SUM(TX.Price_LY1) ELSE CASE WHEN SUM(ISNULL(TX.Price_CY, 0)) > 0 THEN 9.9999 ELSE 0 END END [Growth_Percentage]
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX
				ON ST.StatementCode = TX.StatementCode
			WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'ALL_CPP_BRANDS'
			GROUP BY ST.StatementCode
		) GR
		ON T1.StatementCode = GR.StatementCode

	-- Reward based on manual inputs
	UPDATE T1
	SET Segment_Reward_Percentage += CASE WHEN GR.Growth_Percentage > 0.90 THEN EI.FieldValue ELSE 0 END
	FROM @TEMP T1
		INNER JOIN (
			SELECT ST.StatementCode, EI.GroupTitle, SUM(EIV.FieldValue / 100) [FieldValue]
			FROM @STATEMENTS ST
				INNER JOIN RPWeb_User_EI_FieldValues EIV
				ON ST.StatementCode = EIV.StatementCode
				INNER JOIN RP_Config_EI_Fields EI
				ON EI.ProgramCode = EIV.ProgramCode AND EI.FieldCode = EIV.FieldCode
			WHERE EI.ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT'
			GROUP BY ST.StatementCode, EI.GroupTitle
		) EI
		ON T1.BrandSegment = EI.GroupTitle AND T1.StatementCode = EI.StatementCode
		INNER JOIN (
			SELECT ST.StatementCode,
				   CASE WHEN SUM(ISNULL(TX.Price_LY1, 0)) > 0 THEN SUM(ISNULL(TX.Price_CY, 0)) / SUM(TX.Price_LY1) ELSE CASE WHEN SUM(ISNULL(TX.Price_CY, 0)) > 0 THEN 9.9999 ELSE 0 END END [Growth_Percentage]
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX
				ON ST.StatementCode = TX.StatementCode
			WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'ALL_CPP_BRANDS'
			GROUP BY ST.StatementCode
		) GR
		ON T1.StatementCode = GR.StatementCode
	END ---END FORECAST PROJECTION



	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales)
		SELECT ST.StatementCode, TX.MarketLetterCode, @PROGRAM_CODE AS ProgramCode, TX.GroupLabel,
				SUM(TX.Price_CY) [Segment_Reward_Sales]
			   ,SUM(TX.Price_LY1) LY_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON ST.StatementCode = TX.StatementCode
		WHERE TX.MarketLetterCode='ML2021_W_CPP' AND  TX.ProgramCode = 'ELIGIBLE_SUMMARY' AND TX.GroupType = 'Eligible_Brands'
		GROUP BY ST.StatementCode, TX.MarketLetterCode,  TX.GroupLabel


		INSERT INTO @UNLOCKED_STATEMENTS(Statementcode, Level5Code, StatementLevel)
		SELECT RS.Statementcode, RS.Level5Code, RS.StatementLevel
		FROM  @STATEMENTS ST
			INNER JOIN  RP_Statements RS			
			ON RS.Statementcode=ST.Statementcode
		WHERE RS.Region='WEST' AND RS.VersionType='UNLOCKED' AND RS.StatementType='Actual'

		
		INSERT @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT tx.Statementcode, GR.MarketLetterCode, GR.GroupLabel, SUM(tx.Quantity * SDP) AS Plan_Amount, 0 AS SRP, 0 AS SWP,  SUM(tx.Quantity * SDP) AS Price
		FROM TVF_Get_IndependentPlanTransactions(@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS) tx
			INNER JOIN @UNLOCKED_STATEMENTS UL			ON UL.StatementCode=tx.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
			INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
		WHERE UL.Level5Code='D000001' AND GR.MarketLetterCode='ML2021_W_CPP' AND GR.ProgramCode='ELIGIBLE_SUMMARY' AND GR.GroupType='Eligible_Brands'	
		GROUP BY tx.Statementcode, GR.MarketLetterCode, GR.GroupLabel	
				

		-- SET CURRENT YEAR POG PLAN DOLLAR VALUES
		MERGE @TEMP AS TGT
		USING (
			SELECT t1.StatementCode, t1.MarketLetterCode, t1.GroupLabel, T1.Price, t2.Level5Code
			FROM @SCENARIO_TRANSACTIONS t1
				INNER JOIN  @UNLOCKED_STATEMENTS t2
				ON t2.Statementcode=t1.StatementCode
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.GroupLabel=TGT.BrandSegment
		WHEN MATCHED THEN
			UPDATE SET POG_Plan = SRC.Price
		WHEN NOT MATCHED THEN
			INSERT (StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales)
			VALUES(SRC.StatementCode, SRC.MarketLetterCode, @PROGRAM_CODE, SRC.GroupLabel, SRC.Price)	;


		-- DETERMINE GROWTH SALES AT STATEMENT LEVEL
		INSERT	INTO @GROWTH_TABLE
		SELECT	StatementCode, MarketLetterCode
				,SUM(LY_Sales)
				,SUM(POG_Plan)
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode


		-- PART A:
		-- FIND THE GROWTH PERCENTAGE USING THE SUM OF CPP SALES BASED ON OU LEVEL SALES (AKA. ALL THE GROWTH PERCENT SHOULD BE THE SAME WITHIN A STATEMENT)
		UPDATE	T1
		SET		Growth = 
					CASE 
						WHEN  T2.[Statement_POG_Plan] > 0 AND T2.[Statement_LY_Sales] <= 0 THEN 9.9999
						WHEN  T2.[Statement_POG_Plan] <= 0 AND T2.[Statement_LY_Sales] <= 0 THEN 9.9999
						ELSE T2.[Statement_POG_Plan]/T2.[Statement_LY_Sales]
					END
		FROM	@TEMP T1
		INNER	JOIN @GROWTH_TABLE T2 ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode


		-- PART A:
		-- Update the reward % 
		UPDATE	@TEMP 
		SET		Segment_Reward_Percentage = @BASELINE_PERCENTAGE 
		WHERE	(Growth + @ROUNDUP_VALUE) >= 0.89

		-- Update the reward %  for line companies, it is always 1%
		UPDATE	T1 
			SET	Segment_Reward_Percentage = @BASELINE_PERCENTAGE 
		FROM @TEMP T1
		INNER JOIN #LC_Statements ST	  
		ON T1.StatementCode = ST.StatementCode

	END --END ACTUAL

	

	-- Calculate reward from reward margin
	UPDATE @TEMP
	SET Reward = Segment_Reward_Sales * Segment_Reward_Percentage

	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[Reward] = 0
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0



	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode = T1.StatementCode AND EI.MarketLetterCode = T1.MarketLetterCode 
			AND EI.FieldCode = 
			CASE BrandSegment
				WHEN 'Liberty' THEN 'Radius_Percentage_Liberty'
				WHEN 'Centurion' THEN 'Radius_Percentage_Centurion'
				ELSE 'Radius_Percentage_CPP'
			END
		WHERE [Reward] > 0
	END

	-- Copy data from temporary table to permanent table
	DELETE FROM RP2021_DT_Rewards_W_LogisticsSupport WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_LogisticsSupport(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales,Pog_Plan, Segment_Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE AS ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales,Pog_Plan, Segment_Reward_Percentage, Reward
	FROM @TEMP


	
	--------------------------------------------------
	---------------PART B CALCULATIONS----------------
	--------------------------------------------------
	   

	DECLARE @TEMP_PART_B TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RetailerCode VARCHAR(50) NOT NULL,
		BrandSegment VARCHAR(50) NOT NULL,
		March_Take DECIMAL(6,4) NOT NULL DEFAULT 0,
		March_Reward DECIMAL(6,4) NOT NULL DEFAULT 0,
		April_Take DECIMAL(6,4) NOT NULL DEFAULT 0,
		April_Reward DECIMAL(6,4) NOT NULL DEFAULT 0,
		May_Take DECIMAL(6,4) NOT NULL DEFAULT 0,
		May_Reward DECIMAL(6,4) NOT NULL DEFAULT 0,
		June_Take DECIMAL(6,4) NOT NULL DEFAULT 0,
		June_Reward DECIMAL(6,4) NOT NULL DEFAULT 0,
		July_Take DECIMAL(6,4) NOT NULL DEFAULT 0,
		July_Reward DECIMAL(6,4) NOT NULL DEFAULT 0,
		Segment_Reward_Sales MONEY NOT NULL,
		Segment_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[BrandSegment] ASC
		)
	);	
	

	-- RC RETAILERS
	INSERT INTO @TEMP_PART_B(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, BrandSegment, Segment_Reward_Sales)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, ST.RetailerCode, TX.GroupLabel, SUM(TX.Price_CY) [Segment_Reward_Sales]
	FROM #RC_Statements ST
	INNER JOIN (
			SELECT StatementCode, MarketLetterCode, ProgramCode, GroupType, Price_CY
				,CASE WHEN GroupLabel IN ('FUNGICIDES','INSECTICIDES') THEN 'FUNGICIDES AND INSECTICIDES' ELSE GroupLabel END AS GroupLabel 
			FROM RP2021_DT_Sales_Consolidated
		) TX
	ON ST.StatementCode = TX.StatementCode
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
	GROUP BY ST.StatementCode, ST.RetailerCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel
	

	-- LINE COMPANIES RETAILERS
	INSERT INTO @TEMP_PART_B(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, BrandSegment, Segment_Reward_Sales)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, ST.RetailerCode,'ALL',SUM(TX.Price_CY) [Segment_Reward_Sales]
	FROM #LC_Statements ST	    
	INNER JOIN RP2021_DT_Sales_Consolidated TX
	ON ST.StatementCode = TX.StatementCode
	WHERE TX.MarketLetterCode='ML2021_W_CPP' AND  TX.ProgramCode = 'ELIGIBLE_SUMMARY' AND TX.GroupType = 'Eligible_Brands'
	GROUP BY ST.StatementCode, ST.RetailerCode,  TX.MarketLetterCode, TX.ProgramCode
	
		  

	UPDATE T1
		SET March_Take = ISNULL(SLS.March_Take,0),
		    March_Reward = ISNULL(SLS.March_Reward,0),
			April_Take = ISNULL(SLS.April_Take,0),
			April_Reward = ISNULL(SLS.April_Reward,0),
			May_Take = ISNULL(SLS.May_Take,0),
			May_Reward = ISNULL(SLS.May_Reward,0),
			June_Take = ISNULL(SLS.June_Take,0),
			June_Reward = ISNULL(SLS.June_Reward,0),
			July_Take = ISNULL(SLS.July_Take,0),
			July_Reward = ISNULL(SLS.July_Reward,0)
	FROM @TEMP_PART_B T1
		LEFT JOIN RP_Seeding_W_LogisticsSupport SLS
		ON SLS.Season = @Season AND T1.RetailerCode = SLS.RetailerCode AND T1.BrandSegment = SLS.BrandSegment

	UPDATE T1
		SET Segment_Reward_Percentage = March_Reward + April_Reward + May_Reward + June_Reward + July_Reward,
		    Reward = Segment_Reward_Sales * (March_Reward + April_Reward + May_Reward + June_Reward + July_Reward)
	FROM @TEMP_PART_B T1

		
	-- Copy data from temporary table to permanent table
	DELETE FROM RP2021_DT_Rewards_W_LogisticsSupport_PartB WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_LogisticsSupport_PartB([Statementcode], [MarketLetterCode], [ProgramCode], [RetailerCode], [BrandSegment], [March_Take], [March_Reward], [April_Take], [April_Reward], [May_Take], [May_Reward], [June_Take], [June_Reward], [July_Take], [July_Reward], [Segment_Reward_Sales], [Segment_Reward_Percentage], [Reward])
	SELECT [Statementcode], [MarketLetterCode], @PROGRAM_CODE AS [ProgramCode], [RetailerCode], [BrandSegment], [March_Take], [March_Reward], [April_Take], [April_Reward], [May_Take], [May_Reward], [June_Take], [June_Reward], [July_Take], [July_Reward], [Segment_Reward_Sales], [Segment_Reward_Percentage], [Reward]
	FROM @TEMP_PART_B
END


