﻿CREATE PROCEDURE [dbo].[RP2021_Calc_W_CustomSeed] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[Level5Code] VARCHAR(20) NOT NULL,
		[MarketLetterCode] VARCHAR(50) NOT NULL,
		[ProgramCode] VARCHAR(50) NOT NULL,
		[RewardBrand] VARCHAR(100) NOT NULL,
		[Planned_POG_Sales] MONEY NOT NULL,
		[Custom_Treated_POG_Sales] MONEY NOT NULL DEFAULT 0,
		[RewardBrand_Percentage] DECIMAL(18,2) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[Level5Code] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	
	DECLARE @ROUND_SALES INT = 5000;

	-- GET DATA FROM SALES CONSOLIDATED
	INSERT INTO @TEMP(StatementCode, Level5Code, MarketLetterCode, ProgramCode, RewardBrand, Planned_POG_Sales)
	SELECT ST.StatementCode, TX.Level5Code, TX.MarketLetterCode, TX.ProgramCode,PR.ChemicalGroup,
		   SUM(CASE WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ELSE TX.Price_CY END) [Planned_POG_Sales]
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
		LEFT JOIN RPWeb_User_EI_FieldValues EI
		ON ST.StatementCode = EI.StatementCode AND TX.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'Radius_Percentage_CPP'
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
	GROUP BY ST.StatementCode, TX.Level5Code, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup

	

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		BEGIN
			-- DETERMINE CUSTOM TREATED POG SALES
			UPDATE T1
			SET T1.Custom_Treated_POG_Sales = T1.Planned_POG_Sales * (EIV.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RP_Config_EI_Fields EI
				ON EI.ProgramCode = @PROGRAM_CODE AND T1.RewardBrand = EI.Label
				INNER JOIN RPWeb_User_EI_FieldValues EIV
				ON T1.StatementCode = EIV.StatementCode AND EI.ProgramCode = EIV.ProgramCode AND EI.FieldCode = EIV.FieldCode
		END
	ELSE
		BEGIN
			--FOR ACTUALS
			-- FOR ACTUAL STATEMENTS USE CUSTOM SEED APP DATA 
			UPDATE T1
			SET Custom_Treated_POG_Sales = T2.Sales
			FROM @TEMP T1
				INNER JOIN (
					SELECT ST.Statementcode ,PR.ChemicalGroup AS RewardBrand
						,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',AcresPerJug,IIF(cap.PackageSize='Litres',PR.AcresPerLitre,PR.ConversionW))) AS [Acres]		
						,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',PR.LitresPerJug,IIF(cap.PackageSize='Litres',1,PR.Volume))) [Litres]																	
						,SUM(
							cap.Quantity *
							(
								CASE WHEN cap.PackageSize='Jugs' THEN
									IIF(RS.Level5Code = 'D0000117',  PR.PricePerJug_SRP, IIF(RS.StatementLevel ='LEVEL 5',PR.PricePerJug_SWP,PR.PricePerJug_SDP))
								WHEN cap.PackageSize='Litres' THEN
									IIF(RS.Level5Code = 'D0000117',  PR.PricePerLitre_SRP, IIF(RS.StatementLevel ='LEVEL 5',PR.PricePerLitre_SWP,PR.PricePerLitre_SDP))
								ELSE
									IIF(RS.Level5Code = 'D0000117', PR.SRP, IIF(RS.StatementLevel ='LEVEL 5',PR.SWP,PR.SDP))
								END
							)
							) AS [Sales]																	
					FROM @STATEMENTS ST
						INNER JOIN RP_STATEMENTS RS ON RS.Statementcode=ST.Statementcode
						INNER JOIN (
							SELECT StatementCode,ProductCode,PackageSize,SUM(Quantity) Quantity FROM RP_DT_CustomAppProducts
							WHERE ProductCode <> ''
							GROUP BY StatementCode,ProductCode,PackageSize
						) CAP 	ON CAP.Statementcode=ST.Statementcode
						INNER JOIN (					
							SELECT PR1.Productcode, ChemicalGroup, ConversionW, Jugs,  Volume
								,ConversionW/Jugs AS AcresPerJug
								,ConversionW/Volume as AcresPerLitre
								,Volume/Jugs AS LitresPerJug						
								,PRP.SDP
								,PRP.SDP/Jugs AS PricePerJug_SDP
								,PRP.SDP/Volume AS PricePerLitre_SDP
								,PRP.SRP
								,PRP.SRP/Jugs AS PricePerJug_SRP
								,PRP.SRP/Volume AS PricePerLitre_SRP
								,PRP.SWP
								,PRP.SWP/Jugs AS PricePerJug_SWP
								,PRP.SWP/Volume AS PricePerLitre_SWP
							FROM ProductReference PR1
								INNER JOIN ProductReferencePricing PRP
								ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'
							
							WHERE Chemicalgroup in ('INSURE PULSE','INSURE CEREAL','INSURE CEREAL FX4','TERAXXA F4')  OR Product='NODULATOR PRO 100'
						) PR			
						ON PR.ProductCode=CAP.ProductCode				
					GROUP BY ST.Statementcode, PR.ChemicalGroup			
				) T2
				ON T2.Statementcode=T1.Statementcode AND T2.RewardBrand = T1.RewardBrand		
			WHERE T2.[Acres] > 0
		END
	
	-- Determine reward percentage
	DROP TABLE IF EXISTS #Qualifying_Table

	SELECT StatementCode, SUM(Custom_Treated_POG_Sales) [Combined_Treated_Sales]
	INTO #Qualifying_Table
	FROM @TEMP
	GROUP BY StatementCode
	
	DECLARE @REWARD_PERCENTAGES TABLE (
		[Minimum_Sales] MONEY NOT NULL,
		[Maximum_Sales] MONEY NOT NULL,
		[Reward_Percentage] DECIMAL(6,4) NOT NULL
	)

	-- Part A Independents and Line Companies
	UPDATE T1
	SET T1.RewardBrand_Percentage =
	CASE
		WHEN T2.Combined_Treated_Sales + @ROUND_SALES >= 185000 THEN 0.24
		WHEN T2.Combined_Treated_Sales + @ROUND_SALES >= 80000 AND T2.Combined_Treated_Sales + @ROUND_SALES < 185000 THEN 0.20
		ELSE 0.16
	END
	FROM @TEMP T1
		INNER JOIN #Qualifying_Table T2 
		ON T2.StatementCode = T1.StatementCode 
	WHERE T1.Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427')  


	-- Part A Co-ops
	UPDATE T1
	SET T1.RewardBrand_Percentage =
	CASE
		WHEN T2.Combined_Treated_Sales + @ROUND_SALES >= 225000 THEN 0.22
		WHEN T2.Combined_Treated_Sales + @ROUND_SALES >= 100000 AND T2.Combined_Treated_Sales + @ROUND_SALES < 225000 THEN 0.18
		ELSE 0.14
	END
	FROM @TEMP T1
		INNER JOIN #Qualifying_Table T2 ON T2.StatementCode = T1.StatementCode
	WHERE T1.Level5Code = 'D0000117'

	-- Part B (8% for all retailers)
	UPDATE @TEMP SET RewardBrand_Percentage = 0.08 WHERE RewardBrand = 'NODULATOR PRO'

	-- Calculate reward
	UPDATE @TEMP
	SET Reward = IIF([Planned_POG_Sales] < Custom_Treated_POG_Sales, [Planned_POG_Sales], Custom_Treated_POG_Sales) * RewardBrand_Percentage 
	WHERE Custom_Treated_POG_Sales > 0 and [Planned_POG_Sales] > 0


	-- COPY FROM TEMPORARY TABLE INTO PERMANENT TABLE
	DELETE FROM RP2021_DT_Rewards_W_CustomSeed WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_CustomSeed(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, Planned_POG_Sales, Custom_Treated_POG_Sales, RewardBrand_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, Planned_POG_Sales, Custom_Treated_POG_Sales, RewardBrand_Percentage, Reward
	FROM @TEMP


END

