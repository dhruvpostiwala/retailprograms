﻿

CREATE   PROCEDURE [dbo].[RP2020_Refresh_Sales_Consolidated]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 904
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'	

	/*
	2020 Pricing Metric
		FCL / COOP
		Market Letter					Pricing
		Seed Treatment & Inoculant 		SDP
		Canola Crop Protection			SDP (Liberty, Centurion in SRP)
		Cereal, Pulse & Soybean	SDP		(Titan in SRP)
		InVigor 						SRP


		IND and Line Companies
		2020 SDP (invoice price) pricing will be applied to all sales.
	*/
	   
	DECLARE @SEASON_LY1 INT = @SEASON-1;
	DECLARE @SEASON_LY2 INT = @SEASON-2;
	
	DROP TABLE IF EXISTS #TEMP_Statements
	SELECT ST.StatementCode, ST.Season, ST.Region, ST.RetailerCode ,ST.Level5Code ,ISNULL(ST.DataSetCode,0) AS DataSetCode, ISNULL(ST.DataSetCode_L5,0) AS DataSetCode_L5 
	INTO #TEMP_Statements
	FROM RP_STATEMENTS  ST
		INNER JOIN @STATEMENTS T1
		ON T1.Statementcode=ST.StatementCode
	WHERE SEASON=@SEASON
	
	--CREATE NONCLUSTERED INDEX ST_IX1  ON #TEMP_Statements (Statementcode) ;


	/* Collect all sales information into a temporary table */
	DROP TABLE IF EXISTS #TEMP_Sales
	SELECT RS.StatementCode, RS.Level5Code, tx.RetailerCode, tx.TransType, tx.BASFSeason, tx.ProductCode, tx.ChemicalGroup, tx.Quantity, tx.Acres, tx.Price_SDP, tx.Price_SRP, tx.Price_SWP, tx.InRadius, Adjustment
	INTO #TEMP_Sales
	FROM #TEMP_Statements RS		
		INNER JOIN (
			/* HOUSES CURRENT YEAR POG PLAN TRANSACTIONS ONLY */
			SELECT StatementCode, RetailerCode, 'POG_Plan' AS TransType, @SEASON AS BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, 1 AS InRadius 
				,0 AS Adjustment
			FROM RP_DT_ScenarioTransactions
			
				UNION ALL

			/* IN CASE OF FORECAST/PROJECTION STATEMENTS, HOLDS ONLY LAST TWO YEARS TRANSACTIONS */
			SELECT StatementCode, RetailerCode, 'RET_TRANSACTION' AS TransType, BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, InRadius 
				,IIF(InVoiceNumber='Adjustment',1,0) AS Adjustment
			FROM RP_DT_Transactions 			
			
		) TX
		ON TX.StatementCode=RS.StatementCode

	--	CREATE NONCLUSTERED INDEX TX_IX1  ON #TEMP_Sales (Statementcode) INCLUDE(ProductCode);


	/* Lets consolidate sales to group by market letter, programcode and chemicalgroup, reward/qualifying brand */
	DROP TABLE IF EXISTS #Consolidated_Sales
			   
	/*
		Some programs have requirement to take sales of all basf portfolio products into consideration to determine reward percentage. 		
		Example : Program "Efficiency Rebate (RP2020_W_EFF_REBATE)" from "Canola Crop Protection" and "InVigor Hybrids" Market leters 
		Since different prices are applied depending on market letter, program and products combination. We should maintain save this information for applicable marketletter and programcode
		
	*/
	
	/*******************************************************************************************************************************************/	
	-- ALL BASF PORTFOLIO BRANDS	
	SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0, tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			

	INTO #Consolidated_Sales	
	FROM #TEMP_Sales TX		
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ElG_Programs
		) MLP
		ON tx.StatementCode=MLP.StatementCode
		INNER JOIN (
			SELECT * FROM RP_Config_Groups  WHERE GroupType='ALL_BASF_BRANDS'
		) GR
		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode	
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
	
	/*******************************************************************************************************************************************/
	-- FOR EAST CCP, THIS MARKET LETTER IS BASED ON:
	-- Products: All row crop products, and all horticulture products.
	-- *Excludes All Inoculant products, Kumulus Fungicide, Polyram Fungicide, & Cabrio Plus Fungicide.

	/* maintain selection column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
	INSERT INTO #Consolidated_Sales
	SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode ,tx.ChemicalGroup AS GroupLabel ,tx.ProductCode, MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0, tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2
	
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			

	FROM #TEMP_Sales TX		
		INNER JOIN (
			/* Note: ProgramCode "ELIGIBLE_SUMMARY" is not listed in RP_Config_Programs. Only added to table "[RP_Config_Groups]" to keep track of eligible brands sales (i.e., products on which reward will paid out) */
			SELECT DISTINCT StatementCode, MarketLetterCode, 'ELIGIBLE_SUMMARY' AS ProgramCode 
			FROM RP_DT_ML_ElG_Programs 
			WHERE MarketLetterCode='ML2020_E_CCP'
				UNION
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ElG_Programs WHERE MarketLetterCode='ML2020_E_CCP'
		) MLP
		ON tx.StatementCode=MLP.StatementCode
		INNER JOIN (
			SELECT * FROM RP_Config_Groups  WHERE GroupType='ALL_ROW_CROP_EX_INOCULANT_BRANDS'
		) GR
		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode	
		INNER JOIN (
			SELECT ProductCode 
			FROM ProductReference 
			WHERE DocType in (10,80) AND Sector IN ('ROW CROP','HORTICULTURE')  AND Category NOT IN ('INOCULANT')	
				AND ChemicalGroup NOT IN ('KUMULUS','POLYRAM')					---RP-2366				 			
		) PR
		ON PR.ProductCode = TX.ProductCode
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode ,tx.ChemicalGroup , tx.ProductCode, MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType

	DELETE #Consolidated_Sales
	WHERE MarketLetterCode = 'ML2020_E_CCP' AND GroupLabel='InVigor'

	/*******************************************************************************************************************************************/
	/* LETS INSERT TRANSACATION BASED ON CONFIG SETUP */	
	/* maintain selection column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
	INSERT INTO #Consolidated_Sales
	SELECT ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode , MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,'') AS ProgramCode, GR.GroupType						
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0, tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			
	
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	FROM @STATEMENTS ST
		INNER JOIN (
			/* Note: ProgramCode "ELIGIBLE_SUMMARY" is not listed in RP_Config_Programs. Only added to table "[RP_Config_Groups]" to keep track eligible brands sales (i.e., reward will paid out on) */
			SELECT DISTINCT StatementCode, MarketLetterCode, 'ELIGIBLE_SUMMARY' AS ProgramCode FROM RP_DT_ML_ElG_Programs 
				UNION
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ELG_Programs
			WHERE ProgramCode <> 'RP2020_E_CUSTOM_GROUND_APP_REWARD'
		) MLP
		ON MLP.StatementCode=ST.StatementCode						
		
		INNER JOIN RP_Config_Groups GR		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode	

		-- SKU LEVEL JOIN, NOT REQUIRED IN 2020
		-- INNER JOIN RP_Config_Groups_ProductSKUs SKU		ON SKU.GroupID=GR.GroupID				

		-- BRAND LEVEL JOIN
		INNER JOIN RP_Config_Groups_Brands BR		ON BR.GroupID=GR.GroupID
		INNER JOIN #TEMP_Sales TX	ON TX.StatementCode=ST.StatementCode AND TX.ChemicalGroup=BR.ChemicalGroup
		
		-- IN CASE OF SKU LEVEL		ON TX.StatementCode=ST.StatementCode AND TX.ProductCode=SKU.ProductCode	
	GROUP BY ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,''), GR.GroupType

	
		/*******************************************************************************************************************************************/
	/* SPECIAL CASE AS WE WANT TRANSACTIONS FOR CUSTOM APP REWARD EVEN IF THEY ARE ZERO FOR A RETAILER */
	/* LETS INSERT TRANSACATION FOR CUSTOM APP BASED ON CONFIG SETUP */	
	/* maintain selection column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
	-- https://kennahome.jira.com/browse/RP-2971

	INSERT INTO #Consolidated_Sales
	SELECT ST.StatementCode, ISNULL(tx.Level5Code,''), ISNULL(tx.RetailerCode,''), GR.GroupLabel, ISNULL(tx.ProductCode,(SELECT TOP 1 PRODUCTCODE FROM PRODUCTREFERENCE WHERE CHEMICALGROUP = GR.GROUPLABEL)) , MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,'') AS ProgramCode, GR.GroupType						
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0, tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			
	
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	FROM @STATEMENTS ST
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ELG_Programs
			WHERE ProgramCode = 'RP2020_E_CUSTOM_GROUND_APP_REWARD'
		) MLP
		ON MLP.StatementCode=ST.StatementCode						
		INNER JOIN RP_Config_Groups GR		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode	
		INNER JOIN RP_Config_Groups_Brands BR		ON BR.GroupID=GR.GroupID
		LEFT JOIN #TEMP_Sales TX	ON TX.StatementCode=ST.StatementCode AND TX.ChemicalGroup=BR.ChemicalGroup
	GROUP BY ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,''), GR.GroupType

	/*******************************  DATA HYGIENE   *******************************/

	UPDATE #Consolidated_Sales
	SET GroupType='ELIGIBLE_BRANDS'
	WHERE GroupType='ALL_ROW_CROP_EX_INOCULANT_BRANDS'

	-- RP-2891
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE T1.Level5Code='D000001' AND PR.ChemicalGroup='LIBERTY 200'   
		AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS')	
		AND T1.MarketLetterCode <> 'ML2020_W_LIBERTY_200'

	/*
		DATA HYGIENE
		SOME INVIGOR PRODUCTS ARE CONSIDERED 'NON-REWARD' BRANDS AND SHOULD NOT BE INCLUDED AS REWARD BRANDS
		SINCE OUR GROUP CONFIGURATION IS MAPPED TO CHEMICAL GROUP LEVEL.
		WE NEED TO DELETE SUCH PRODUCTS FROM OUR REWARD BRANDS.

		VALID INVIGOR PRODUCTS (REWARD BRAND PRODUCTS):		
		WEST: 		INVIGOR L230, INVIGOR L233P, INVIGOR L234PC, INVIGOR L241C, INVIGOR L252, INVIGOR L255PC, INVIGOR L345PC, INVIGOR L352C, InVigor LR344PC --RP-2407
		EAST:		INVIGOR L345PC, INVIGOR L352C, INVIGOR L234PC,	INVIGOR L255PC,	INVIGOR L233P,	INVIGOR L252

	*/
	
	 -- Non-Reward InVigor Products
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN #TEMP_Statements ST		ON ST.Statementcode=T1.StatementCode
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE ST.Region='WEST' AND PR.ChemicalGroup='INVIGOR'
		AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS')		
		AND PR.PRODUCT NOT IN ('INVIGOR L230','INVIGOR L233P','INVIGOR L234PC','INVIGOR L241C','INVIGOR L252','INVIGOR L255PC','INVIGOR L345PC','INVIGOR L352C','INVIGOR LR344PC','INVIGOR CHOICE LR344PC')
	
	--RP-2708 - InVigor Performance: ALL Invigor should count as Qualifying for Invigor Performance Reward
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN #TEMP_Statements ST		ON ST.Statementcode=T1.StatementCode
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE ST.Region='WEST' AND PR.ChemicalGroup='INVIGOR'
		AND T1.GROUPTYPE = 'QUALIFYING_BRANDS'
		AND T1.ProgramCode <> 'RP' + CAST(@SEASON AS VARCHAR(4))+ '_W_INV_PERFORMANCE'
		AND PR.PRODUCT NOT IN ('INVIGOR L230','INVIGOR L233P','INVIGOR L234PC','INVIGOR L241C','INVIGOR L252','INVIGOR L255PC','INVIGOR L345PC','INVIGOR L352C','INVIGOR LR344PC','INVIGOR CHOICE LR344PC')
	

	-- RP-2831 InVigor Performance: Remove InVigor Health Brands
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN #TEMP_Statements ST		ON ST.Statementcode=T1.StatementCode
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE ST.Region='WEST' AND PR.ChemicalGroup='INVIGOR'
		AND T1.GROUPTYPE = 'QUALIFYING_BRANDS'
		AND T1.ProgramCode = 'RP' + CAST(@SEASON AS VARCHAR(4))+ '_W_INV_PERFORMANCE'
		AND PR.PRODUCT IN ('INVIGOR L156H','INVIGOR L157H','INVIGOR L258HPC')

	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN #TEMP_Statements ST		ON ST.Statementcode=T1.StatementCode
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE ST.Region='EAST' AND PR.ChemicalGroup='INVIGOR' AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS','QUALIFYING_BRANDS')
		AND PR.PRODUCT NOT IN ('INVIGOR L345PC','INVIGOR L352C','INVIGOR L234PC','INVIGOR L255PC','INVIGOR L233P','INVIGOR L252')

/*
	JIRA: RP-2778
	DATA HYGIENE: PURSUIT
	ChemicalGroup	Product	ProductCode	ProductName
	PURSUIT	PURSUIT HERBICIDE (W)	000000000059010168	PURSUIT HERBICIDE (W) 2 X 3.3 L CASE
	PURSUIT	PURSUIT HERBICIDE (E)	000000000059010167	PURSUIT HERBICIDE (E) 2 X 3.3 L CASE
	
*/

	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN #TEMP_Statements ST			
		ON ST.Statementcode=T1.StatementCode
	WHERE (ST.Region='WEST' AND T1.ProductCode = '000000000059010167') OR (ST.Region='EAST' AND T1.ProductCode = '000000000059010168')


	/*
	--RP-2366
	EAST -- GET RID OF KUMULUS AND POLYRAM if not SUPPLY SALES  'KUMULUS','POLYRAM'
	*/
	DELETE #Consolidated_Sales 
	WHERE GroupLabel IN ('KUMULUS','POLYRAM') 
		AND ProgramCode  NOT IN ('RP2020_E_SUPPLY_SALES','ELIGIBLE_SUMMARY')

	/*
	 DATA HYGIENE
	 INLUCDE PROWL H20 AND PROWL 400(RP-2945) (PRODUCT) ONLY UNDER PROWL CHEMICAL GROUP 
	*/
	
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
		INNER JOIN #TEMP_Statements ST			
		ON ST.Statementcode=T1.StatementCode
	WHERE PR.ChemicalGroup='PROWL' 
	AND PR.Product NOT IN ('PROWL H2O', 'PROWL 400')
	AND ST.Region='East' 

	--AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS') 
	
	--  FOR WEST PROWL H2O ONLY 
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
		INNER JOIN #TEMP_Statements ST			
		ON ST.Statementcode=T1.StatementCode
	WHERE ST.Region='West' 
	AND PR.ChemicalGroup='PROWL' 
	AND PR.Product <> 'PROWL H2O'	

	/*
	 DATA HYGIENE
	 INLUCDE FRONTIER MAX(PRODUCT) ONLY UNDER FRONTIER CHEMICAL GROUP 
	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup='FRONTIER' AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS')
		AND PR.PRODUCT  <> 'FRONTIER MAX'

	/*
		DATA HYGIENE
		PROGRAM: WEST - CUSTOM SEED TREATING REWARD 
		INCLUDE PRODUCT 'NODULATOR PRO 100' ONLY UNDER 'NODULATOR PRO' CHEMICAL GROUP
	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup='NODULATOR PRO' AND T1.GROUPTYPE='REWARD_BRANDS'
		AND PR.Product <> 'NODULATOR PRO 100'
		AND T1.ProgramCode='RP2020_W_CUSTOM_SEED_TREATING_REWARD'
		
	/*
	DATA HYGIENE
	DELETE LIBERTY 150 AND CENTURION TRANSACTIONS FOR INDEPENDENT AND COOPS
	THIS REQUIREMENT IS ONLY FOR WEST LINE COMPANIES 
	INVENTORY MANAGEMENT REWARD
	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup IN ('LIBERTY 150','LIBERTY 200','CENTURION') AND T1.GROUPTYPE = 'REWARD_BRANDS'
		AND T1.ProgramCode  = 'RP2020_W_INVENTORY_MGMT' AND T1.Level5Code IN ('D000001','D0000117')
	
	/*
	DATA HYGIENE
	DELETE LIBERTY 200 TRANSACTIONS FOR WEST LINE AND IND FOR
	PLANNING AND SUPPORTING THE BUSINESS AND INVIGOR PERFORMANCE FOR Canola Crop Protection Program ML
	**NEW Demarey ..enable liberty 200 for WEST LINE   - RP-2451
	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup = 'LIBERTY 200' AND T1.GROUPTYPE = 'REWARD_BRANDS'
		AND (T1.ProgramCode  = 'RP2020_W_PLANNING_SUPP_BUSINESS' OR T1.ProgramCode  = 'RP2020_W_INV_PERFORMANCE')  AND T1.MarketLetterCode='ML2020_W_CCP' AND T1.Level5Code = 'D000001'
		--AND (T1.ProgramCode  = 'RP2020_W_PLANNING_SUPP_BUSINESS' OR T1.ProgramCode  = 'RP2020_W_INV_PERFORMANCE')  AND T1.MarketLetterCode='ML2020_W_CCP' AND T1.Level5Code IN ('D000001','D0000107','D520062427','D0000137','D0000244')
		

	/*	 DATA HYGIENE - RP-2861	 	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup='LIBERTY 200' AND T1.Level5Code='D000001' AND T1.ProgramCode='RP2020_W_RETAIL_BONUS_PAYMENT'
	
	
	/*
	Mahesh Pantangi added comments on SEP/03/2020 12:15 PM	
		THIS DOES NOT SEEMS TO BE THE CASE ANYMORE AND ALSO WHERE CONDITIONS CODE IS WRONG, GROUP LABEL IS NOT UTILIZED CORRECTLY

	DATA HYGIENE - 	BRAND SPECIFIC SUPPORT REWARD
	DELETE CENTURION TRANSACTIONS FOR IND AND COOP 
	** PROWL H20 IS ONLY FOR WEST LINE COMPANIES
	** ZAMPRO AND CABRIO IS ONLY FOR COOP AND IND 
	** MIZUNA AND DUET IS ONLY FOR NUTRIEN

	-- CONDITIONS ARE WRONG, GROUP LABEL IS NOT UTILIZED CORRECTLY
	DELETE
	FROM #Consolidated_Sales
	WHERE ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT' AND GroupLabel='QUALIFIERS_SDP' AND GROUPLABEL = 'PROWL' AND  Level5Code NOT IN  ('D0000107','D520062427','D0000137','D0000244')
	

	DELETE
	FROM #Consolidated_Sales
	WHERE ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT' AND GroupLabel='QUALIFIERS_SDP' AND GROUPLABEL IN ('CABRIO','ZAMPRO') AND  Level5Code NOT IN  ('D0000117','D0000107','D520062427','D0000137','D0000244')

	DELETE
	FROM #Consolidated_Sales
	WHERE ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT' AND GroupLabel='QUALIFIERS_SDP' AND GROUPLABEL IN ('MIZUNA','DUET') AND  Level5Code <> 'D520062427'
	*/

	--DATA HYGIENE - 	BRAND SPECIFIC SUPPORT REWARD
	--  MIZUNA AND DUET IS ONLY FOR NUTRIEN
	DELETE T1
	FROM #Consolidated_Sales T1		
		INNER JOIN ProductReference PR 		ON PR.ProductCode=T1.ProductCode
	WHERE ProgramCode='RP2020_W_BRAND_SPEC_SUPPORT' AND PR.ChemicalGroup IN ('MIZUNA','DUET') AND  Level5Code <> 'D520062427'
		  -- AND GroupLabel='QUALIFIERS_SDP' IS ONLY FOR NUTRIEN


	/*
	DATA HYGIENE
	DELETE CENTURION TRANSACTIONS FOR IND AND COOP FOR
	PLANNING AND SUPPORTING THE BUSINESS
	

	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup = 'CENTURION' AND T1.GROUPTYPE = 'REWARD_BRANDS'
		AND T1.ProgramCode  = 'RP2020_W_PLANNING_SUPP_BUSINESS' AND T1.MarketLetterCode='ML2020_W_CCP' AND T1.Level5Code IN ('D000001','D0000117')
	*/

	/*
	DATA HYGIENE
	DELETE ALTITUDE FX, DUET, MIZUNA EVERYWHERE EXCEPT NUTRIEN
	--New Demarey November  18 2020
	--it seems the someone added a space in the chemical group for alti
	*/
	DELETE T1
	FROM #Consolidated_Sales T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE PR.ChemicalGroup IN ('DUET','MIZUNA','ALTITUDE FX','ALTITUDE FX 2','ALTITUDE FX 3','ALTITUDE FX2','ALTITUDE FX3') --AND T1.GROUPTYPE IN ('ELIGIBLE_BRANDS','REWARD_BRANDS')
	  AND T1.Level5Code <> 'D520062427'
 
	  
  	/******** NON REWARD BRANDS **********/
		INSERT INTO #Consolidated_Sales	
		SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode 
			,'NON_REWARD_BRANDS' AS MarketLetterCode , 'NON_REWARD_BRANDS_SUMMARY' ProgramCode , 'NON_REWARD_BRANDS' AS GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0, tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.Adjustment=0,tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
			
	FROM #TEMP_Sales TX		
		LEFT JOIN (
			SELECT DISTINCT StatementCode, Productcode
			FROM #Consolidated_Sales 
			WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS'
		) TX2
		ON TX2.StatementCode=TX.StatementCode AND TX2.ProductCode=TX.Productcode 
	WHERE TX2.ProductCode IS NULL
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode 

   	 
   	/* LETS START SETTING DATA VALUES AS NEEDED DEFINED BY BRD */

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')	
	BEGIN
		UPDATE #Consolidated_Sales
		SET Acres_E_CY=Acres_Forecast
			,Acres_E_LY1=Acres_Q_LY1
			,Acres_E_LY2=Acres_Q_LY2

			,Price_E_SDP_CY=SDP_Forecast
			,Price_E_SDP_LY1=Price_Q_SDP_LY1
			,Price_E_SDP_LY2=Price_Q_SDP_LY2

			,Price_E_SRP_CY=SRP_Forecast
			,Price_E_SRP_LY1=Price_Q_SRP_LY1
			,Price_E_SRP_LY2=Price_Q_SRP_LY2

			,Price_E_SWP_CY=SWP_Forecast
			,Price_E_SWP_LY1=Price_Q_SWP_LY1
			,Price_E_SWP_LY2=Price_Q_SWP_LY2
	END	   

	ALTER TABLE #Consolidated_Sales ADD [Pricing_Type] [VARCHAR](3) NULL
	UPDATE #Consolidated_Sales 	SET [Pricing_Type]='SDP' -- DEFAULT PRICING TYPE
		

	-- ################# DETERMINE PRICE TYPE TO  BE USED ################# 

	-- D000001 - Independent West (RC REP) - SDP
	-- D0000117 - COOP (FCL) (BR) - SRP
	

	/*
	EAST INVIGOR MARKET LETTER 
	JIRA: RC-3506
	Pricing: Valued & rewarded on in SDP (Suggested Retail Pricing)

	East Canola Crop Protection Market Letter
	JIRA: RC-3714
	Pricing: Valued & rewarded on in SRP (Suggested Retail Pricing)


	UPDate July 9 2020
	Use SDP Pricing RP-2329
	
	UPDATE #Consolidated_Sales	Set Pricing_Type='SDP'		WHERE MarketLetterCode IN ('ML2020_E_INV','ML2020_E_CCP')
	*/

	

	/*
	WEST COOP/FCL
		Market Letter				Pricing
		Seed Treatment & Inoculant 	SDP
		Canola Crop Protection		SDP (Liberty, Centurion in SRP)
		Cereal, Pulse & Soybean		SDP (Titan in SRP)
		InVigor 					SRP
	*/
	
	UPDATE T1
	Set Pricing_Type='SRP' 	
	FROM #Consolidated_Sales 	 T1
		INNER JOIN ProductReference PR
		ON PR.ProductCode=T1.ProductCode
	WHERE T1.Level5Code='D0000117' AND  PR.ChemicalGroup IN ('LIBERTY 150','LIBERTY 200','CENTURION','TITAN','INVIGOR') 

	---West Line Companies Pricing --CARGILL, NUTRIEN, RICHARDSON, UFA
	UPDATE T1
	Set Pricing_Type='SWP'
	FROM #Consolidated_Sales T1
	INNER JOIN RP_Statements RS on RS.STatementCode = T1.StatementCode
	WHERE T1.Level5Code IN ('D0000107', 'D520062427','D0000137','D0000244') AND RS.Region = 'WEST'

	
	-- EXCEPTION: COOP AUGUST CHEQUE RUN 
	--DELETE #Consolidated_Sales			WHERE MarketLetterCode='NON_REWARD_BRANDS'


	--  Make sure to insert correct price values based on pricing type  in columns Price_CY, Price_LY1, Price_LY2
	
	DELETE FROM RP2020_DT_Sales_Consolidated WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)
	
	INSERT INTO RP2020_DT_Sales_Consolidated(StatementCode,DataSetCode,DataSetCode_L5,Level5Code,RetailerCode,GroupLabel,ProductCode,MarketLetterCode,ProgramCode,GroupType,Pricing_Type
		,Acres_Forecast
		,Price_Forecast		
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_CY, Acres_LY1 ,Acres_LY2
		,Price_CY, Price_LY1, Price_LY2	)	
	SELECT TX.StatementCode, ST.DataSetCode, ST.DataSetCode_L5, ST.Level5Code ,TX.RetailerCode
		,GroupLabel,ProductCode, MarketLetterCode, ProgramCode, GroupType,Pricing_Type
		,Acres_Forecast
		, CASE WHEN Pricing_Type='SRP' THEN SRP_Forecast
			   WHEN Pricing_Type='SWP' THEN SWP_Forecast
			   ELSE SDP_Forecast 
		  END AS [Price_Forecast]
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_E_CY AS [Acres_CY], Acres_E_LY1 AS [Acres_LY1], Acres_E_LY2  AS [Acres_LY2]
		,CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_CY
			  WHEN Pricing_Type='SWP' THEN Price_E_SWP_CY
			  ELSE Price_E_SDP_CY 
		  END AS Price_CY
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY1
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY1
			 ELSE  Price_E_SDP_LY1
		  END AS Price_LY1
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY2
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY2
			 ELSE  Price_E_SDP_LY2
		  END AS Price_LY2
	FROM #TEMP_Statements ST
		INNER JOIN #Consolidated_Sales TX		
		ON TX.StatementCode=ST.StatementCode


END
