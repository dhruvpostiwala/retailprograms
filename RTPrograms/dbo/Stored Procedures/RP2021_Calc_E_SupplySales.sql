﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_SupplySales] @SEASON INT, @STATEMENT_TYPE VARCHAR(20),  @PROGRAM_CODE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		Level5Code VARCHAR(50) NOT NUll,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(100) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		DataSubmissionCount INT NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	
	DECLARE @VALIDMONTHSUB AS INT

	
	DECLARE @SUBMISSIONS TABLE (
	 StatementCode INT NOT NULL,	 
	 Valid_Sep_Oct  BIT DEFAULT 0,
	 ValidMonthofSubmissionCount INT NOT NULL DEFAULT 0,
	 Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
	 PRIMARY KEY CLUSTERED  (
		[Statementcode] ASC
	 )
	)


	DECLARE @REWARD_PERCENTAGE DECIMAL(6,4) = 0.0075

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		-- Insert sales data in to the temp table
		INSERT INTO @TEMP(StatementCode, DataSetCode,Level5Code, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales)
		SELECT ST.StatementCode, RS.DataSetCode,RS.Level5Code, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup,
			   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Price_CY) * (CASE WHEN TX.MarketLetterCode = 'ML2021_E_CPP' THEN EI_CPP.FieldValue ELSE EI_INV.FieldValue END / 100) ELSE SUM(TX.Price_CY) END [RewardBrand_Sales]
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON ST.StatementCode = TX.StatementCode
			INNER JOIN RP_Statements RS
			ON ST.StatementCode = RS.StatementCode
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
			INNER JOIN RPWeb_USER_EI_FieldValues EI_CPP
			ON ST.StatementCode = EI_CPP.StatementCode AND EI_CPP.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI_CPP.FieldCode = 'Radius_Percentage_CPP'
			INNER JOIN RPWeb_USER_EI_FieldValues EI_INV
			ON ST.StatementCode = EI_INV.StatementCode AND EI_INV.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI_INV.FieldCode = 'Radius_Percentage_InVigor'
		WHERE RS.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE
		GROUP BY ST.StatementCode, RS.DataSetCode, RS.Level5Code, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup, EI_CPP.FieldValue, EI_INV.FieldValue
		
			-- Update the reward percentage
		UPDATE @TEMP
		SET Reward_Percentage = @REWARD_PERCENTAGE
		WHERE RewardBrand_Sales > 0

		-- Update the reward
		UPDATE @TEMP
		SET Reward = RewardBrand_Sales * Reward_Percentage
		WHERE RewardBrand_Sales > 0
	END --END FORECAST PROJECTION
	
	
	IF @STATEMENT_TYPE = 'ACTUAL' 
	BEGIN
		INSERT INTO @TEMP(StatementCode,DataSetCode,Level5Code,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
		SELECT ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup AS RewardBrand
			,SUM(tx.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS						ON RS.StatementCode=ST.StatementCode
			INNER JOIN RP2021_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
		WHERE rs.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE
		GROUP BY ST.StatementCode, RS.DataSetCode,RS.Level5Code, tx.MarketLetterCode, tx.ProgramCode, Pr.ChemicalGroup

		-- QUALIFICATION @ LEVEL 2 FOR FAIMILIES AND @ LEVEL 1 FOR INDEPENDENTS
		DROP TABLE IF EXISTS #QUALIFIER_STATEMENTCODES		
		SELECT StatementCode
		INTO #QUALIFIER_STATEMENTCODES
		FROM (
			SELECT IIF(DATASETCODE > 0,DATASETCODE,StatementCode) AS StatementCode
			FROM @TEMP
		) T
		GROUP BY StatementCode


		-- Month Of Submission represented as month number of BASF Season. (Nov = 1, Dec = 2, Jan = 3 ...)
		INSERT INTO @SUBMISSIONS(StatementCode,Valid_Sep_Oct,ValidMonthofSubmissionCount)
		SELECT ST.StatementCode
			,MAX(CASE WHEN MonthOfSubmission IN ('9','10') THEN 1  ELSE 0 END) AS Valid_Sep_Oct			
			,COUNT(DISTINCT MonthOfSubmission) AS ValidMonthofSubmissionCount
		FROM #QUALIFIER_STATEMENTCODES ST
			INNER JOIN (
				SELECT sub.StatementCode,sub.RetailerCode
					,CASE
						WHEN ISNUMERIC(Replace(LEFT(MonthOfSubmission,2),'.','')) = 1 THEN CAST(CAST(Replace(LEFT(MonthOfSubmission,2),'.','') AS INT) as VARCHAR(2))
						ELSE '00' 
					END AS MonthOfSubmission 
				FROM ( 
					SELECT StatementCode, Season,RetailerCode,DataType,MonthOfSubmission 
					FROM RP_DT_RetailerDataNotExpectedTrackerAll
					
						UNION ALL
					
					SELECT StatementCode, Season,RetailerCode,DataType,MonthOfSubmission 
					FROM RP_DT_RetailerDataSubmissionDataAll
				)sub
				INNER JOIN RP_Statements S ON S.StatementCode = sub.StatementCode
				WHERE sub.DataType IN ('Invigor Data','Chemical Transactional Data','Transactional Data')
					AND sub.MonthOfSubmission <> '<Not A Standard Submission>'
			) DataSub ON DataSub.StatementCode=ST.StatementCode 
			
		WHERE DataSub.MonthOfSubmission NOT IN ('11','12') 
		GROUP BY ST.StatementCode 
		
		DELETE FROM @SUBMISSIONS WHERE Valid_Sep_Oct = 0

		--Reward valid statments 
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.0025	WHERE  ValidMonthofSubmissionCount = 1 OR ValidMonthofSubmissionCount = 2
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.005	WHERE  ValidMonthofSubmissionCount BETWEEN 3 AND 5 
		UPDATE @SUBMISSIONS SET Reward_Percentage = 0.0075	WHERE  ValidMonthofSubmissionCount >= 6 

		-- LEVEL 1 INDEPENDENT STATEMENTS 
		UPDATE T1
		SET DataSubmissionCount  = Sub.ValidMonthofSubmissionCount
			,T1.Reward_Percentage = Sub.Reward_Percentage
			,T1.Reward = Sub.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
			INNER JOIN @SUBMISSIONS Sub
			ON Sub.StatementCode = T1.StatementCode
			
		-- LEVEL 1 STATEMENTS FROM FAMILY
		UPDATE T1
		SET DataSubmissionCount  = Sub.ValidMonthofSubmissionCount
			,T1.Reward_Percentage = Sub.Reward_Percentage
			,T1.Reward = Sub.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
			INNER JOIN @SUBMISSIONS Sub
			ON Sub.StatementCode = T1.DataSetCode
					   			 
		/*	The following East Distributors should always receive 0.75% (max reward) as they submit monthly. 
			D0000111	SOLLIO AGRICULTURE S.E.C.
			D0000212	SLYVITE
			D520001300	SYNAGRI
		*/
		
		UPDATE @TEMP SET Reward_Percentage = 0.0075 WHERE Level5Code IN('D0000111','D0000212','D520001300')
		
		UPDATE T1
		SET T1.Reward = T1.Reward_Percentage * T1.RewardBrand_Sales
		FROM @TEMP T1
		WHERE Level5Code IN('D0000111','D0000212','D520001300')


	END
		

	-- Insert into DT table from temp table
	DELETE FROM RP2021_DT_Rewards_E_SupplySales WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_E_SupplySales(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand,
		   SUM(RewardBrand_Sales) [RewardBrand_Sales],
		   MAX(Reward_Percentage) [Reward_Percentage],
		   SUM(Reward) [Reward]
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode, RewardBrand
END
