﻿CREATE PROCEDURE [dbo].[RP_Get_Forecast_RewardsSummary]  @STATEMENTCODE INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT;
	DECLARE @ScenarioCodes NVARCHAR(MAX);
	DECLARE @SalesSummary NVARCHAR(MAX);
	DECLARE @RewardsDetails NVARCHAR(MAX);
	DECLARE @RewardsDetails2 NVARCHAR(MAX);
	DECLARE @MaximizeRewardMessages NVARCHAR(MAX);
	DECLARE @RadiusDetails NVARCHAR(MAX);


	DECLARE @StatementType  Varchar(20);
	DECLARE @STATEMENTCODE_F INT;
	DECLARE @STATEMENTCODE_P INT = 0;
	DECLARE @Level5Code VARCHAR(20);

	DECLARE @RCT_PLANTYPE VARCHAR(255)='';
	

	SELECT @SEASON=SEASON, @StatementType = StatementType, @Level5Code = Level5Code FROM RPWeb_Statements WHERE StatementCode=@StatementCode;


	IF @StatementType='FORECAST'
		BEGIN
			SET @StatementCode_F = @STATEMENTCODE
		END
    ELSE IF @StatementType='PROJECTION'
        BEGIN 
            SET @StatementCode_P = @STATEMENTCODE
			--get Forecast StatementCode
			SET @STATEMENTCODE_F =  (SELECT T1.StatementCode AS [ForecastSTatementCode]
            From RPWeb_Statements T1
                INNER JOIN (
                    SELECT Season, RetailerCode, StatementLevel
                    FROM RPWeb_Statements
                    Where StatementCode= @STATEMENTCODE   --PROJECTED STATEMENTCODE
                ) T2
                ON T2.Season=T1.Season AND T2.RetailerCode=T1.RetailerCode AND T2.StatementLevel=T1.StatementLevel 
            WHERE T1.[Status]='Active' AND T1.StatementType='Forecast' AND T1.VersionType='Final-Preferred'
			)
        END 
	
	IF @STATEMENTTYPE='FORECAST'
		SET @RCT_PLANTYPE='PLAN'
	ELSE IF @STATEMENTTYPE='PROJECTION'
		SET @RCT_PLANTYPE='PROJECTION'

	IF OBJECT_ID('tempdb..#TEMP_Sales') IS NOT NULL DROP TABLE #TEMP_Sales
	SELECT RetailerCode,GroupLabel
		,MAX(MarketLetterCode) MarketLetterCode
		,MAX(Pricing_Type) Pricing_Type,
		SUM(ISNULL( Sales_CY_F,0)) Sales_CY_F, 
		SUM(ISNULL( Sales_CY_P,0)) Sales_CY_P, 
		SUM(ISNULL( Sales_LY1,0)) Sales_LY1,
		SUM(ISNULL( Sales_LY2,0)) Sales_LY2
	INTO #TEMP_Sales
	FROM (
		SELECT StatementCode,RetailerCode,GroupLabel
			,MAX(MarketLetterCode) MarketLetterCode
			,MAX(Pricing_Type) Pricing_Type
			,SUM(IIF(StatementCode=@STATEMENTCODE_F,Sales_CY,0)) AS Sales_CY_F
			,SUM(IIF(StatementCode=@STATEMENTCODE_P,Sales_CY,0)) AS Sales_CY_P
			,SUM(IIF(StatementCode=@STATEMENTCODE,Sales_LY1,0)) AS  Sales_LY1
			,SUM(IIF(StatementCode=@STATEMENTCODE,Sales_LY2,0)) AS  Sales_LY2		
		FROM RPWeb_ML_Elg_Sales
		WHERE [StatementCode] IN (@STATEMENTCODE_F,@StatementCode_P)
		GROUP BY StatementCode,RetailerCode,GroupLabel
	) DATA
	GROUP BY RetailerCode,GroupLabel
		
	SELECT @ScenarioCodes = ISNULL((
		SELECT ScenarioID as [id], label, active_id			
		FROM (
			SELECT SC.ScenarioID, SC.[Name] AS [label], IIF(ISNULL(SC2.ScenarioCode,'')='','','Yes') AS [Active_ID]
			FROM RPWeb_Statements RS
				INNER JOIN Retail_Plan_Scenarios SC	ON RS.RetailerCode=SC.RetailerID AND SC.Season=RS.Season AND SC.ScenarioType=RS.StatementType
				LEFT JOIN RPWeb_Scenarios SC2		ON SC2.StatementCode=RS.StatementCode AND SC2.ScenarioCode=SC.ScenarioID
			WHERE RS.StatementCode=@STATEMENTCODE AND RS.[Status]='Active' AND  SC.[Status] IN ('FINAL','PREFERRED','DRAFT','PROJECTED') AND SC.[Name] NOT IN ('SYSTEM-CREATED')

				UNION 

			SELECT CAST(SC.ID  AS VARCHAR(20)) AS ScenarioID, SC.[Name] AS [label], IIF(ISNULL(SC2.ScenarioCode,'')='','','Yes') AS [Active_ID]
			FROM RPWeb_Statements RS
				INNER JOIN RCT_Scenarios SC			ON SC.RetailerCode=RS.RetailerCode AND SC.[Primary]=1
				INNER JOIN RCT_PlanningModes PM		ON PM.ID=SC.PlanningModeID AND PM.Season=RS.Season	AND PM.[Type]=@RCT_PLANTYPE
				LEFT JOIN RPWeb_Scenarios SC2		ON SC2.StatementCode=RS.StatementCode AND SC2.ScenarioCode=CAST(SC.ID AS VARCHAR(20))
			WHERE RS.StatementCode=@STATEMENTCODE AND RS.[Status]='Active' 
		) D
		FOR JSON PATH
	),'[]')
	
	
	SET @RadiusDetails=ISNULL((				
		SELECT ml.marketlettercode, radius.forecast_value, radius.projection_value
		FROM (
			SELECT DISTINCT MarketLetterCode	
			FROM RPWeb_ML_ELG_Programs 
			WHERE StatementCode IN (@STATEMENTCODE_F,@STATEMENTCODE_P)
			) ml
			 inner join (
				SELECT marketlettercode
					,MAX(IIF(StatementCode=@STATEMENTCODE_F,fieldvalue,0)) AS forecast_value
					,MAX(IIF(StatementCode=@STATEMENTCODE_P,fieldvalue,0)) AS projection_value
				FROM RPWeb_User_EI_FieldValues 
				WHERE FieldCode IN ('Radius_Percentage', 'Radius_Percentage_InVigor', 'Radius_Percentage_CPP', 'Radius_Percentage_Liberty', 'Radius_Percentage_Centurion') AND StatementCode IN (@STATEMENTCODE_F,@STATEMENTCODE_P)
				GROUP BY MarketLetterCode
			) radius
			on radius.marketlettercode=ml.marketlettercode
		FOR JSON PATH
	),'[]')

	-- Get total sales for the current year
	IF OBJECT_ID('tempdb..#TEMP_Total_Sales') IS NOT NULL DROP TABLE #TEMP_Total_Sales
	CREATE TABLE #TEMP_Total_Sales (
		MarketLetterCode VARCHAR(50) NOT NULL,
		Sales_CY MONEY NOT NULL,
		Sales_LY1 MONEY NOT NULL,
		Sales_LY2 MONEY NOT NULL
	);

	DECLARE @TOTAL_SALES_SQL_COMMAND NVARCHAR(MAX) = '
	SELECT ML.MarketLetterCode, ISNULL(TOTAL.Price_CY, 0), ISNULL(TOTAL.Price_LY1, 0), ISNULL(TOTAL.Price_LY2, 0)
	FROM (
		SELECT SUM(IIF(BASFSeason = ' + CAST(@SEASON AS VARCHAR(4)) + ', IIF(''' + @Level5Code + ''' = ''D0000117'', Price_SRP, Price_SDP), 0)) [Price_CY],
			   SUM(IIF(BASFSeason = ' + CAST(@SEASON AS VARCHAR(4)) + ' - 1, IIF(''' + @Level5Code + ''' = ''D0000117'', Price_SRP, Price_SDP), 0)) [Price_LY1],
			   SUM(IIF(BASFSeason = ' + CAST(@SEASON AS VARCHAR(4)) + ' - 2, IIF(''' + @Level5Code + ''' = ''D0000117'', Price_SRP, Price_SDP), 0)) [Price_LY2]
		FROM (
			SELECT StatementCode, RetailerCode, ''POG_Plan'' AS TransType, ' + CAST(@SEASON AS VARCHAR(4)) + ' AS BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, 1 AS InRadius 
			FROM RP_DT_ScenarioTransactions WHERE StatementCode = ' + CAST(@STATEMENTCODE_F AS VARCHAR(4)) + '
			
				UNION ALL

			SELECT StatementCode, RetailerCode, ''RET_TRANSACTION'' AS TransType, BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, InRadius 
			FROM RP_DT_Transactions WHERE StatementCode = ' + CAST(@STATEMENTCODE_F AS VARCHAR(4)) + '
		) TOTAL
	) TOTAL
		LEFT JOIN RP_Config_MarketLetters ML
		ON ML.MarketLetterCode LIKE ''ML' + CAST(@SEASON AS VARCHAR(4)) + '_' + CASE (SELECT Region FROM RPWeb_Statements WHERE StatementCode = @STATEMENTCODE_F) WHEN 'West' THEN 'W' ELSE 'E' END + '%''
	';

	INSERT INTO #TEMP_Total_Sales
	EXEC (@TOTAL_SALES_SQL_COMMAND)

	-- Remember to return All column labels in lowercase
	SELECT @SalesSummary=ISNULL((
		SELECT ML.marketlettercode as ml_code, ML.title as ml_title
			,IsNull(summary_eligible.[Sales_CY_F],0) AS total_eligible_cy_f
			,IsNull(summary_eligible.[Sales_CY_P],0) AS total_cy_p
			,IsNull(summary_eligible.[Sales_LY1],0) AS total_eligible_ly1
			,IsNull(summary_eligible.[Sales_LY2],0) AS total_eligible_ly2
			,IsNull(summary_total.[Sales_CY],0) AS total_cy_f
			,IsNull(summary_total.[Sales_LY1],0) AS total_ly1
			,IsNull(summary_total.[Sales_LY2],0) AS total_ly2
			,retailers.retailercode, retailers.retailername, retailers.city, retailers.province
			,retailers.sales_cy_f,retailers.sales_cy_p, retailers.sales_ly1, retailers.sales_ly2
			,transactions.GroupLabel as [brand], transactions.pricing_type, transactions.sales_cy_f,transactions.sales_cy_p, transactions.sales_ly1, transactions.sales_ly2
		FROM RP_Config_MarketLetters ML
			INNER JOIN 	( 
				SELECT DISTINCT MarketLetterCode	
				FROM RPWeb_ML_ELG_Programs 
				WHERE StatementCode=@StatementCode
			)	MLP		
			ON MLP.MarketLetterCode=ML.MarketLetterCode			

			LEFT JOIN (
				SELECT MarketLetterCode, SUM(Sales_CY_F) AS [Sales_CY_F],SUM(Sales_CY_P) AS [Sales_CY_P], SUM(Sales_LY1) AS [Sales_LY1], SUM(Sales_LY2) AS [Sales_LY2]						
				FROM #TEMP_Sales 
				GROUP BY MarketLetterCode
			) summary_eligible
			ON Summary_Eligible.MarketLetterCode=MLP.MarketLetterCode

			LEFT JOIN #TEMP_Total_Sales summary_total
			ON Summary_Total.MarketLetterCode=MLP.MarketLetterCode

			LEFT JOIN (
				SELECT tx.MarketLetterCode, tx.Retailercode, rp.retailername, RP.mailingcity as  city, RP.mailingprovince as province	
					,SUM(tx.Sales_CY_F) AS [Sales_CY_F],SUM(tx.Sales_CY_P) AS [Sales_CY_P], SUM(tx.Sales_LY1) AS [Sales_LY1], SUM(tx.Sales_LY2) AS [Sales_LY2]						
				FROM #TEMP_Sales tx
					INNER JOIN RetailerProfile RP	ON RP.RetailerCode=TX.RetailerCode				
				GROUP BY tx.MarketLetterCode, tx.Retailercode, rp.retailername, RP.mailingcity, RP.mailingprovince
			) retailers
			ON retailers.MarketLetterCode=MLP.MarketLetterCode 

			LEFT JOIN (
				SELECT 	tx.retailercode, tx.MarketLetterCode,tx.GroupLabel,tx.pricing_type ,tx.sales_cy_f,tx.sales_cy_p,tx.sales_ly1 ,tx.sales_ly2 
				FROM #TEMP_Sales TX														
			) transactions
			ON transactions.MarketLetterCode=MLP.MarketLetterCode AND transactions.retailercode=retailers.retailercode 
		WHERE ML.[Status]='Active'
		ORDER BY ML.[Sequence], retailers.retailername, retailers.city--,  transactions.GroupLabel
		FOR JSON AUTO
	),'[]')



	-- Maximize Reward Messages
	SELECT @MaximizeRewardMessages=ISNULL((
		SELECT ML.Title as ml_title, PRG.Title AS program_title, T1.maximizerewardmessage
		FROM RPWeb_MaximizeRewardMessages T1
			INNER JOIN RP_Config_MarketLetters ML 		ON ML.MarketLetterCode=T1.MarketLetterCode
			INNER JOIN RP_Config_Programs PRG			ON PRG.ProgramCode=T1.ProgramCode
		WHERE StatementCode=@Statementcode
		Order By ml.[Sequence], program_title
		FOR JSON PATH
	),'[]')

	/*
	-- Commenting out code, because we are calling TVF dynamically 
	IF @SEASON=2020
	BEGIN
		SELECT @RewardsDetails=ISNULL((
			SELECT forecast.RowNumber as row_number, 
			forecast.rowtype as row_type, 
			forecast.marketlettercode, 
			forecast.marketletterpdf,
			forecast.rewardcode,
			forecast.rowlabel,
			forecast.displayinput as 'forecast_displayinput', 
			forecast.inputprefix as 'forecast_inputprefix',
			forecast.inputtext as 'forecast_inputtext',
			forecast.inputsuffix as 'forecast_inputsuffix', 
			forecast.displayqualifier as 'forecast_displayqualifier', 
			forecast.qualifierprefix as 'forecast_qualifierprefix',
			forecast.qualifier as 'forecast_qualifier' , 
			forecast.qualifiersuffix as 'forecast_qualifiersuffix', 
			forecast.reward as 'forecast_reward' ,
			forecast.ProgramDescription AS program_description, 
			projection.displayinput as  'projection_displayinput', 
			projection.inputprefix as 'projection_inputprefix', 
			projection.inputtext as 'projection_inputtext' , 
			projection.inputsuffix as 'projection_inputsuffix',
			projection.displayqualifier as 'projection_displayqualifier',
			projection.qualifierprefix as 'projection_qualifierprefix', 
			projection.qualifier as 'projection_qualifier', 
			projection.qualifiersuffix as 'projection_qualifiersuffix', 
			projection.reward as 'projection_reward'
				,IIF(forecast.rowtype='Program Total','Sample Text','') as maximize_reward_description
			FROM TVF2020_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_F) Forecast  --
			LEFT JOIN  TVF2020_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_P) Projection on Projection.RowNumber = Forecast.RowNumber
			WHERE Forecast.[RenderRow]='YES'
			ORDER BY Forecast.RowNumber
			FOR JSON PATH
		),'[]')
	END
	*/
	-- Following code is to call procedure name based on season rather than hard coding to check for season and call a specific stored procedure
	DECLARE @SQL_Command nvarchar (max);
	SET @SQL_Command='
	SELECT @RewardsDetails=ISNULL((
		SELECT forecast.RowNumber as row_number, 
			forecast.rowtype as row_type, 
			forecast.marketlettercode, 
			forecast.marketletterpdf,
			forecast.rewardcode,
			forecast.rowlabel,
			forecast.displayinput as ''forecast_displayinput'', 
			forecast.inputprefix as ''forecast_inputprefix'',
			forecast.inputtext as ''forecast_inputtext'',
			forecast.inputsuffix as ''forecast_inputsuffix'', 
			forecast.displayqualifier as ''forecast_displayqualifier'', 
			forecast.qualifierprefix as ''forecast_qualifierprefix'',
			forecast.qualifier as ''forecast_qualifier'' , 
			forecast.qualifiersuffix as ''forecast_qualifiersuffix'', 
			forecast.reward as ''forecast_reward'' ,
			forecast.ProgramDescription AS program_description, 
			projection.displayinput as  ''projection_displayinput'', 
			projection.inputprefix as ''projection_inputprefix'', 
			projection.inputtext as ''projection_inputtext'' , 
			projection.inputsuffix as ''projection_inputsuffix'',
			projection.displayqualifier as ''projection_displayqualifier'',
			projection.qualifierprefix as ''projection_qualifierprefix'', 
			projection.qualifier as ''projection_qualifier'', 
			projection.qualifiersuffix as ''projection_qualifiersuffix'', 
			projection.reward as ''projection_reward''
				,IIF(forecast.rowtype=''Program Total'',''Sample Text'','''') as maximize_reward_description
			FROM TVF' + CAST(@SEASON AS VARCHAR(4))  + '_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_F) Forecast  --
				LEFT JOIN  TVF' + CAST(@SEASON AS VARCHAR(4))  + '_Refresh_ForecastRewardsDisplay(@STATEMENTCODE_P) Projection on Projection.RowNumber = Forecast.RowNumber
			WHERE Forecast.[RenderRow]=''YES''
			ORDER BY Forecast.RowNumber
			FOR JSON PATH
		),''[]'') '

	EXEC sp_executesql @SQL_Command, N'@STATEMENTCODE_F INT, @STATEMENTCODE_P INT, @RewardsDetails NVARCHAR(MAX) OUTPUT', 
		@STATEMENTCODE_F=@STATEMENTCODE_F, @STATEMENTCODE_P=@STATEMENTCODE_P, @RewardsDetails=@RewardsDetails OUTPUT

	SELECT @StatementType as StatementType
		,@ScenarioCodes AS ScenarioCodes
		,@SalesSummary AS SalesSummary
		,@RewardsDetails AS RewardsDetails		
		,@RadiusDetails AS RadiusDetails
		,@MaximizeRewardMessages as MaxRewardMessages
		
	OPTION (RECOMPILE);

END
