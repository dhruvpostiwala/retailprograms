﻿
CREATE PROCEDURE [dbo].[RP2021_Exception_E_Get_Programs] 		
	@STATEMENTCODE INT, 
	@JSON_DATA NVARCHAR(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON INT=2021
	DECLARE @REGION VARCHAR(4)='EAST'
		
	DECLARE @TEMP TABLE (
		StatementCode INT,
		reward_code VARCHAR(50),
		title  VARCHAR(100),
		exception_type  VARCHAR(50)
	)

	SET @JSON_DATA='[]'
	
	INSERT INTO @TEMP(StatementCode,[reward_code],[title],exception_type)
	SELECT EEX.StatementCode,EEX.RewardCode [reward_code], RewardLabel as [title],'NON-TIER' AS exception_type		
	FROM [dbo].[RP_Config_Rewards_Summary] S
		INNER JOIN (
			SELECT DISTINCT StatementCode, P.RewardCode + 'X' AS RewardCode 
			FROM (
				--IN CASE THERE IS NOTHING IN THE TABLE ITSELF SOME PROGRAMS DONT INSERT WHENN IT IS ZERO 
				SELECT RS.StatementCode, PS.ProgramCode
				FROM RPWeb_Rewards_Summary RS 
					INNER JOIN RP_Config_programs PS 
					ON PS.RewardCode = RS.RewardCode AND PS.Region=@REGION 
				WHERE RS.StatementCode=@StatementCode AND  RS.TotalReward = 0 
					
				--Tier Reward Programs
				UNION ALL

				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_SupplySales]
				WHERE statementcode=@statementcode AND Reward_Percentage < 0.75
				
				UNION ALL

				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
				WHERE statementcode=@statementcode AND Reward_Percentage < 0.75
			
				UNION ALL

				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_PortfolioSupport] 
				where statementcode=@statementcode AND Reward_Percentage < 0.06


				UNION ALL
				
				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_FungicidesSupport]
				WHERE StatementCode=@Statementcode AND Reward=0
			
				UNION ALL
								
				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_Sollio_Growth_Bonus]
				WHERE StatementCode=@Statementcode AND Reward = 0 

				UNION ALL
										
				SELECT StatementCode,ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_Ignite_LoyaltySupport]
				WHERE StatementCode=@Statementcode AND Reward = 0 

			) PRG 
				INNER JOIN RP_Config_Programs P 
				ON PRG.ProgramCode = P.ProgramCode 
			WHERE P.RewardCode NOT IN ('CGAS_E','PRX_HEAD_E','SG_E') 
		) EEX 
		ON S.RewardCode = EEX.RewardCode
		INNER JOIN RPWeb_Statements ST 
		ON ST.StatementCode = EEX.StatementCode AND DatasetCode = 0
	WHERE EEX.StatementCode = @StatementCode 
		AND S.Season = @Season 
		AND S.RewardType='Exception'
	--	AND S.RewardLabel LIKE '%Exception%' 
		
		
	-- User should not be able to create another exception of the same type on the statement if they already have one already
	--in  submitted or approved status   
	DELETE T1
	FROM @TEMP T1
		INNER JOIN RPWeb_Exceptions W 
		ON W.StatementCode = T1.StatementCode and W.RewardCode = T1.reward_code
	WHERE W.[Status] NOT IN ('Rejected','Obsolete') and W.exception_type <> 'OTHER' -- and W.Season = @Season
	-- Spelling mistake Obselete

	UPDATE @TEMP 
	SET exception_type='TIER' 
	WHERE Reward_Code IN ('PORT_SUP_EX','SSLB_EX','SUPPLY_SALES_EX','ROW_CROP_FUNG_EX')

	SELECT @JSON_DATA = ISNULL((
	SELECT reward_code, title,exception_type 
	FROM @TEMP
	FOR JSON PATH) ,'[]')

END

