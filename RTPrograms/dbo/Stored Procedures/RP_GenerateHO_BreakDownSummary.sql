﻿

CREATE PROCEDURE [dbo].[RP_GenerateHO_BreakDownSummary] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SEASON_STRING VARCHAR(4)=CAST(@SEASON AS VARCHAR(4))
	DECLARE @OU_HO_STATEMENTS AS UDT_RP_STATEMENT
	DECLARE @PROGRAMCODES_TO_SKIP TABLE (
		ProgramCode VARCHAR(50) NOT NULL
	)
	
	INSERT INTO @OU_HO_STATEMENTS(Statementcode)
	SELECT S.Statementcode
	FROM @STATEMENTS S
		INNER JOIN RP_Statements R 
		ON R.Statementcode=S.Statementcode
	WHERE R.VersionType='Unlocked' AND R.StatementType='ACTUAL' AND R.Region='WEST' AND R.StatementLevel IN ('LEVEL 2','LEVEL 5') 
		
	IF @SEASON=2021
		BEGIN 
			INSERT INTO @PROGRAMCODES_TO_SKIP(ProgramCode)
			VALUES('ELIGIBLE_SUMMARY')
			,('NON_REWARD_BRANDS_SUMMARY')
			,('RP2021_W_ADVANCED_WEED_CTRL')
			,('RP2021_W_CEREAL_START_FINISH')
			,('RP2021_W_DICAMBA_TOLERANT')
			,('RP2021_W_HEAT_UP_YOUR_HARVEST')
			,('RP2021_W_CLL_SUPPORT')
			,('RP2021_W_CLW_SUPPORT')
			,('RP2021_W_NOD_DUO_SCG')
			,('RP2021_W_TANK_MIX_BONUS')
			,('RP2021_W_CUSTOM_AER_APP')
		END   
	ELSE IF @SEASON=2020
		BEGIN 
			INSERT INTO @PROGRAMCODES_TO_SKIP(ProgramCode)
			VALUES('ELIGIBLE_SUMMARY')
			,('NON_REWARD_BRANDS_SUMMARY')
			,('RP2020_W_AGREWARDS_BONUS')
			,('RP2020_W_CLL_SUPPORT')
			,('RP2020_W_CLW_SUPPORT')
			,('RP2020_W_TANK_MIX_BONUS')
			,('RP2020_W_NOD_DUO_SCG_REW')
			,('RP2020_W_AG_REW_WS_TR')
			,('RP2020_W_CUSTOM_AERIAL_APP_REW')
		END   	  


	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS T2				ON T2.StatementCode=T1.StatementCode
		LEFT JOIN @PROGRAMCODES_TO_SKIP PRG		ON PRG.ProgramCode=T1.ProgramCode
	WHERE PRG.ProgramCode IS NULL
	
	IF @SEASON=2021
		INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales)
		SELECT S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode,'' AS FarmCode ,SUM(Price_CY) AS Sales
		FROM @OU_HO_STATEMENTS S
			INNER JOIN RP2021Web_Sales_Consolidated TX		ON TX.StatementCode=S.Statementcode
			LEFT JOIN @PROGRAMCODES_TO_SKIP PRG				ON PRG.ProgramCode=TX.ProgramCode
		WHERE TX.GroupType IN ('REWARD_BRANDS','ELIGIBLE_BRANDS','ALL_CPP_BRANDS') AND PRG.ProgramCode IS  NULL
		GROUP BY S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode	
	ELSE IF @SEASON = 2020
		INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales)
		SELECT S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode,'' AS FarmCode ,SUM(Price_CY) AS Sales
		FROM @OU_HO_STATEMENTS S
			INNER JOIN RP2020Web_Sales_Consolidated TX		ON TX.StatementCode=S.Statementcode
			LEFT JOIN @PROGRAMCODES_TO_SKIP PRG				ON PRG.ProgramCode=TX.ProgramCode
		WHERE TX.GroupType IN ('REWARD_BRANDS','ELIGIBLE_BRANDS') AND PRG.ProgramCode IS  NULL
		GROUP BY S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode	
	

	/*

	DECLARE @SQL_CMD NVARCHAR(MAX)
	SET @SQL_CMD = '
	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales)
	SELECT S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode,'' AS FarmCode ,SUM(Price_CY) AS Sales
	FROM @OU_HO_STATEMENTS S
		INNER JOIN RP' + @SEASON_STRING + 'Web_Sales_Consolidated TX		ON TX.StatementCode=S.Statementcode
		LEFT JOIN @PROGRAMCODES_TO_SKIP PRG				ON PRG.ProgramCode=TX.ProgramCode
	WHERE TX.GroupType IN (''REWARD_BRANDS'',''ELIGIBLE_BRANDS'') AND PRG.ProgramCode IS  NULL
	GROUP BY S.Statementcode, TX.MarketLetterCode, TX.ProgramCode, TX.RetailerCode '
	
	EXEC @SQL_CMD 
	*/
			
	UPDATE T1
	SET Sales=ROUND(T1.Sales,2)
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @OU_HO_STATEMENTS S
		ON S.StatementCode=T1.Statementcode
	

	-- OU / HO LEVEL SALES
	UPDATE T1
	SET OU_Sales = ROUND(T2.OU_Sales,2)
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN (
			SELECT c.StatementCode, c.MarketLetterCode, C.ProgramCode, SUM(c.Sales) AS OU_Sales
			FROM RP_DT_HO_BreakDownSummary c
				INNER JOIN @OU_HO_STATEMENTS s
				ON s.Statementcode=c.Statementcode
			GROUP BY c.StatementCode, c.MarketLetterCode, C.ProgramCode
		) T2
		ON T2.Statementcode=T1.Statementcode
	WHERE T2.MarketLetterCode=T1.MarketLetterCode AND T2.ProgramCode=T1.ProgramCode

	-- OU / HO LEVEL REWARDS
	UPDATE T1
	SET OU_Reward = ROUND(T2.TotalReward,2)
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN (
			SELECT T1.Statementcode, RS.MarketLetterCode, P.Programcode, RS.RewardValue AS TotalReward			
			FROM @OU_HO_STATEMENTS T1
				INNER JOIN RP_DT_Rewards_Summary RS		ON RS.StatementCode=T1.Statementcode
				INNER JOIN RP_Config_Programs P			ON P.RewardCode=RS.RewardCode 				
			WHERE P.Season=@SEASON			
		) T2
		ON T2.Statementcode=T1.Statementcode  AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.ProgramCode=T1.Programcode
	
	UPDATE RP_DT_HO_BreakDownSummary
	SET Retailer_Reward = ROUND(OU_Reward *  (Sales/OU_Sales),2)
	WHERE OU_Sales <> 0 

END