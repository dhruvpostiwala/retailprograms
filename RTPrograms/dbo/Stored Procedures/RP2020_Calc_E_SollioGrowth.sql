﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_E_SollioGrowth]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	IF @STATEMENT_TYPE <> 'ACTUAL' 
	RETURN

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(100) NOT NULL,	
		RewardBrand VARCHAR(100) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		CYSales MONEY NOT NULL,
		LYSales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL,
		CYSales MONEY NOT NULL,
		LYSales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)

	-- THIS IS A STRAIGHT FORWARD 1% REWARD.
	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,CYSales,LYSales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup AS RewardBrand
	,SUM(tx.Price_E_SDP_CY) AS RewardBrand_Sales
	,SUM(tx.Price_Q_SDP_CY) AS CYSales
	,SUM(tx.Price_Q_SDP_LY1) AS LYSales
	FROM @STATEMENTS ST				
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR On PR.ProductCode = tx.ProductCode 
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ELIGIBLE_BRANDS' AND tx.Level5Code = 'D0000111'
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup

	-- Determine growth based on the sum of brand sales at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Sales,CYSales,LYSales)
	SELECT
		CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(RewardBrand_Sales) AS RewardBrand_Sales,SUM(CYSales) AS CYSales,SUM(LYSales) AS LYSales
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	-- Determine growth on total sales
	UPDATE @TEMP_TOTALS
	SET Growth = CYSales/LYSales
	WHERE LYSales > 0

	-- Update the reward %
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.05 WHERE (Growth = 0 AND CYSales > 0) OR Growth + @ROUNDUP_VALUE >= 1.20
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.04 WHERE [Reward_Percentage] = 0 AND Growth + @ROUNDUP_VALUE >= 1.15
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.03 WHERE [Reward_Percentage] = 0 AND Growth + @ROUNDUP_VALUE >= 1.10

	-- Update the growth % and reward % back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward % back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0

	-- Update reward
	UPDATE @TEMP SET [Reward] = RewardBrand_Sales * Reward_Percentage

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_SollioGrowth WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_SollioGrowth(StatementCode, MarketLetterCode, ProgramCode,RewardBrand,RewardBrand_Sales, CYSales, LYSales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode,RewardBrand, RewardBrand_Sales, CYSales, LYSales, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode
		,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(CYSales) AS CYSales
		,SUM(LYSales) AS LYSales
		,AVG(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand

END
