﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_CLL_Forecast]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		EXEC RP2020_Calc_W_CLL_Actual @SEASON, @STATEMENT_TYPE, @PROGRAM_CODE, @STATEMENTS
		RETURN
	END 



	/*
	Retails receive $200/grower for newly signed commitments prior to February 28, 2020 &/or receive $100/grower for newly signed commitments submitted between March 1 2020 & October 8 2020.
	Retails receive a 3% reward on matched acres of Clearfield lentil herbicides including, Odyssey® NXT herbicide, Odyssey Ultra NXT, Solo® ADV herbicide, Solo Ultra herbicides
	(and all previous formulations including Solo WG, Odyssey WG, Odyssey Ultra, Odyssey DLX, Odyssey NXT).
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode in (188,189)
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_CLL_SUPPORT'
	
	DECLARE @TEMP TABLE (
		StatementCode int NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		RewardBrand varchar(100) NOT NULL,
		Commitment_Early int NOT NULL DEFAULT 0,
		Commitment_Early_Amount MONEY NOT NULL DEFAULT 0,
		Commitment_Early_Reward MONEY NOT NULL DEFAULT 0,
		Commitment_Late int NOT NULL DEFAULT 0,
		Commitment_Late_Amount MONEY NOT NULL DEFAULT 0,
		Commitment_Late_Reward MONEY NOT NULL DEFAULT 0,
		Herbicide_Lentil_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		Herbicide_Brand_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		Herbicide_Brand_Sales MONEY NOT NULL DEFAULT 0,
		Herbicide_Match_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		Herbicide_Match_Sales MONEY NOT NULL DEFAULT 0,
		Herbicide_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Herbicide_Reward  MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_MATCH TABLE (
		StatementCode int NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		Lentils Numeric(18,2) NOT NULL DEFAULT 0,
		SoloUltra Numeric(18,2) NOT NULL DEFAULT 0,
		OdysseyUltraNXT Numeric(18,2) NOT NULL DEFAULT 0,
		OdysseyUltra Numeric(18,2) NOT NULL DEFAULT 0,
		SoloADV Numeric(18,2) NOT NULL DEFAULT 0,
		OdysseyNXT Numeric(18,2) NOT NULL DEFAULT 0,
		Solo Numeric(18,2) NOT NULL DEFAULT 0,
		Odyssey Numeric(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC
		)
	)

	DECLARE @Early_Commitment INT = 200;
	DECLARE @Late_Commitment INT = 100;
	DECLARE @Herbicide_RewardPerc FLOAT = 0.03;

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN

		-- First calculate the commitment reward
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,Herbicide_Lentil_Acres)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode, 'FXXXXX' AS FarmCode, 'COMMITMENT' AS RewardBrand,
			SUM(CASE WHEN tx.GroupLabel = 'CLEARFIELD LENTILS' THEN tx.Acres_CY ELSE 0 END) AS Herbicide_Lentil_Acres
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX	ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements S ON S.StatementCode = ST.StatementCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode

		UPDATE T1
		SET Commitment_Early = ISNULL(EI.FieldValue,0), Commitment_Early_Amount = @Early_Commitment, Commitment_Early_Reward = ISNULL(EI.FieldValue,0) * @Early_Commitment
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Early_Commitment'
		WHERE RewardBrand = 'COMMITMENT'

		UPDATE T1
		SET Commitment_Late = ISNULL(EI.FieldValue,0), Commitment_Late_Amount = @Late_Commitment, Commitment_Late_Reward = ISNULL(EI.FieldValue,0) * @Late_Commitment
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Late_Commitment'
		WHERE RewardBrand = 'COMMITMENT'

		-- Now calculate the matching reward

		-- Determine herbicide brand acres and sales
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,Herbicide_Brand_Acres,Herbicide_Brand_Sales)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode, 'FXXXXX' AS FarmCode, tx.GroupLabel AS Herbicide_Brand,
			SUM(CASE WHEN tx.GroupLabel <> 'CLEARFIELD LENTILS' THEN tx.Acres_CY ELSE 0 END) AS Herbicide_Brand_Acres,
			SUM(CASE WHEN tx.GroupLabel <> 'CLEARFIELD LENTILS' THEN tx.Price_CY ELSE 0 END) AS Herbicide_Brand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX	ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements S ON S.StatementCode = ST.StatementCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode, tx.GroupLabel

		-- Now calculate matching acres

		/*
		-- these are taken from 2019 prices, i'll just assume for now it's the same order for 2020
		SOLO ULTRA			$20.63825
		ODYSSEY ULTRA NXT	$20.07975
		ODYSSEY ULTRA		$18.92875
		SOLO ADV			$17.483
		ODYSSEY NXT			$16.581625
		SOLO				$15.4575
		ODYSSEY				$14.935	
		*/

		INSERT INTO @TEMP_MATCH(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Lentils,SoloUltra,OdysseyUltraNXT,OdysseyUltra,SoloADV,OdysseyNXT,Solo,Odyssey)
		SELECT StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode,
			SUM(CASE WHEN RewardBrand = 'COMMITMENT' THEN Herbicide_Lentil_Acres ELSE 0 END) AS Lentils,
			SUM(CASE WHEN RewardBrand = 'SOLO ULTRA' THEN Herbicide_Brand_Acres ELSE 0 END) AS SoloUltra,
			SUM(CASE WHEN RewardBrand = 'ODYSSEY ULTRA NXT' THEN Herbicide_Brand_Acres ELSE 0 END) AS OdysseyUltraNXT,
			SUM(CASE WHEN RewardBrand = 'ODYSSEY ULTRA' THEN Herbicide_Brand_Acres ELSE 0 END) AS OdysseyUltra,
			SUM(CASE WHEN RewardBrand = 'SOLO ADV' THEN Herbicide_Brand_Acres ELSE 0 END) AS SoloADV,
			SUM(CASE WHEN RewardBrand = 'ODYSSEY NXT' THEN Herbicide_Brand_Acres ELSE 0 END) AS OdysseyNXT,
			SUM(CASE WHEN RewardBrand = 'SOLO' THEN Herbicide_Brand_Acres ELSE 0 END) AS Solo,
			SUM(CASE WHEN RewardBrand = 'ODYSSEY' THEN Herbicide_Brand_Acres ELSE 0 END) AS Odyssey
		FROM @TEMP
		GROUP BY StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode

		UPDATE @TEMP_MATCH SET SoloUltra = CASE WHEN SoloUltra >= Lentils THEN Lentils ELSE SoloUltra END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - SoloUltra
		UPDATE @TEMP_MATCH SET OdysseyUltraNXT = CASE WHEN OdysseyUltraNXT >= Lentils THEN Lentils ELSE OdysseyUltraNXT END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - OdysseyUltraNXT
		UPDATE @TEMP_MATCH SET OdysseyUltra = CASE WHEN OdysseyUltra >= Lentils THEN Lentils ELSE OdysseyUltra END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - OdysseyUltra
		UPDATE @TEMP_MATCH SET SoloADV = CASE WHEN SoloADV >= Lentils THEN Lentils ELSE SoloADV END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - SoloADV
		UPDATE @TEMP_MATCH SET OdysseyNXT = CASE WHEN OdysseyNXT >= Lentils THEN Lentils ELSE OdysseyNXT END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - OdysseyNXT
		UPDATE @TEMP_MATCH SET Solo = CASE WHEN Solo >= Lentils THEN Lentils ELSE Solo END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - Solo
		UPDATE @TEMP_MATCH SET Odyssey = CASE WHEN Odyssey >= Lentils THEN Lentils ELSE Odyssey END
		UPDATE @TEMP_MATCH SET Lentils = Lentils - Odyssey
		
		-- Now update the matched acres back in the main table
		UPDATE T1
		SET T1.[Herbicide_Match_Acres] =
			CASE T1.RewardBrand
				WHEN 'SOLO ULTRA' THEN M1.SoloUltra
				WHEN 'ODYSSEY ULTRA NX' THEN M1.OdysseyUltraNXT
				WHEN 'ODYSSEY ULTRA' THEN M1.OdysseyUltra
				WHEN 'SOLO ADV' THEN M1.SoloADV
				WHEN 'ODYSSEY NXT' THEN M1.OdysseyNXT
				WHEN 'SOLO' THEN M1.Solo
				WHEN 'ODYSSEY' THEN M1.Odyssey
			ELSE 0 END
		FROM @TEMP T1
		LEFT JOIN @TEMP_MATCH M1
		ON T1.StatementCode=M1.StatementCode AND T1.MarketLetterCode=M1.MarketLetterCode AND T1.ProgramCode=M1.ProgramCode AND T1.RetailerCode=M1.RetailerCode AND T1.FarmCode=M1.FarmCode

		-- Calculate matched sales as a percentage of brand sales
		UPDATE @TEMP SET Herbicide_Match_Sales = Herbicide_Brand_Sales * (Herbicide_Match_Acres / Herbicide_Brand_Acres) WHERE Herbicide_Brand_Acres > 0

		-- Calculate the matched reward
		UPDATE @TEMP SET Herbicide_Reward_Percentage = @Herbicide_RewardPerc WHERE Herbicide_Match_Sales > 0
		UPDATE @TEMP SET Herbicide_Reward = Herbicide_Match_Sales * @Herbicide_RewardPerc WHERE Herbicide_Reward_Percentage > 0

		-- Update the final reward
		UPDATE @TEMP SET Reward = Commitment_Early_Reward + Commitment_Late_Reward + Herbicide_Reward WHERE Commitment_Early_Reward + Commitment_Late_Reward + Herbicide_Reward > 0

	END
	
	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'	
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	DELETE FROM RP2020_DT_Rewards_W_ClearfieldLentils WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_ClearfieldLentils(StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, RewardBrand, Commitment_Early, Commitment_Early_Amount, Commitment_Early_Reward, Commitment_Late, Commitment_Late_Amount, Commitment_Late_Reward, Herbicide_Lentil_Acres, Herbicide_Brand_Acres, Herbicide_Brand_Sales, Herbicide_Match_Acres, Herbicide_Match_Sales, Herbicide_Reward_Percentage, Herbicide_Reward, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RetailerCode, FarmCode, RewardBrand, Commitment_Early, Commitment_Early_Amount, Commitment_Early_Reward, Commitment_Late, Commitment_Late_Amount, Commitment_Late_Reward, Herbicide_Lentil_Acres, Herbicide_Brand_Acres, Herbicide_Brand_Sales, Herbicide_Match_Acres, Herbicide_Match_Sales, Herbicide_Reward_Percentage, Herbicide_Reward, Reward
	FROM @TEMP

END
