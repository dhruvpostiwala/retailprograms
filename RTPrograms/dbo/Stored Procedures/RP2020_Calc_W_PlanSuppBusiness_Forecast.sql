﻿

CREATE  PROCEDURE [dbo].[RP2020_Calc_W_PlanSuppBusiness_Forecast]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;


	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		EXEC RP2020_Calc_W_PlanSuppBusiness_Actual @SEASON, @STATEMENT_TYPE, @PROGRAM_CODE, @STATEMENTS
		RETURN
	END 
		
	/*
		Planned Growth: (%) 2020 POG Plan ($) relative to 2019 POG Sales ($)
		Qualify on CCP + Liberty + Centurion
		Reward on CCP

		UPDATE: RRT-503 - As discussed, we need to update the Planning and Supporting the Business rewards to update the calculations to include the additional "gimmie" reward percentages
		based on the assumption that retails will meet all order/delivery deadlines. 
	*/


	/*
		STATEMENT TYPE: ACTUAL

		
		Note: BASF to provide Early Order & Take Order Data to Kenna and validation of qualification will be based on “Invoice Date” of orders to determine reward qualification. Growth component of reward will be calculated on brands growth not by Sku.

		Retails must have a preliminary & final plan by the listed buffer dates/market letter.
		
		
		Note: Planned Growth will not be awarded if there is no plan (initial or final) by the final plan dates
		Planned Growth = 2020 POG Plan / 2019 POG Sales (Specific to brands in each Market Letter)
		Planned Growth Tiers
		1.	90 – 99.9% = 1% Reward
		2.	100 – 109.9% = 2% Reward
		3.	110+% = 3% Reward

		Additional Reward Percentages :
			Early Order of Customer Plan = YES = 1% Reward
		*	Take 25% delivery of Order = YES = 0.5% Reward (Specific to Canola ML Only)
			Take 75% delivery of Order = YES = 0.5% Reward
			Take 95% delivery of Order = YES = 0.5% Reward 

	*/



	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 2
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_PLANNING_SUPP_BUSINESS'

	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		GroupLabel VARCHAR(100) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		Growth_QualSales_CY Money NOT NULL,
		Growth_QualSales_LY Money NOT NULL,
		Growth_Reward_Sales Money NOT NULL,
		Growth_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Growth_Reward Money NOT NULL DEFAULT 0,
		Lib_Reward_Sales Money NOT NULL,
		Lib_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Lib_Reward Money NOT NULL DEFAULT 0,
		Cent_Reward_Sales Money NOT NULL,
		Cent_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Cent_Reward Money NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		Growth Decimal(7,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC,
			[Level5Code] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		Growth_Reward_Sales Money NOT NULL,
		Growth_QualSales_CY Money NOT NULL,
		Growth_QualSales_LY Money NOT NULL,
		Growth_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Growth Decimal(7,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[Level5Code] ASC
		)
	)

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Level5Code,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales,Lib_Reward_Sales,Cent_Reward_Sales)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.Level5Code,		
		SUM(CASE WHEN tx.GroupLabel IN ('CENTURION','LIBERTY') THEN 0 ELSE tx.Price_CY END) AS Growth_QualSales_CY,
		SUM(CASE WHEN tx.GroupLabel IN ('CENTURION','LIBERTY') THEN 0 ELSE tx.Price_LY1 END) AS Growth_QualSales_LY,
		SUM(CASE WHEN tx.GroupLabel IN ('CENTURION','LIBERTY') THEN 0 ELSE tx.Price_CY END) AS Growth_Reward_Sales,		
		SUM(CASE WHEN tx.GroupLabel = 'LIBERTY' THEN tx.Price_CY ELSE 0 END) AS Lib_Reward_Sales,
		SUM(CASE WHEN tx.GroupLabel = 'CENTURION' THEN tx.Price_CY ELSE 0 END) AS Cent_Reward_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, tx.Level5Code

	-- Determine growth based on the sum of brand sales
	INSERT INTO @TEMP_TOTALS(StatementCode,MarketLetterCode,ProgramCode,Level5Code,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales)
	SELECT StatementCode, MarketLetterCode, ProgramCode, Level5Code, SUM(Growth_QualSales_CY) AS Growth_QualSales_CY, SUM(Growth_QualSales_LY) AS Growth_QualSales_LY, SUM(Growth_Reward_Sales) AS Growth_Reward_Sales
	FROM @TEMP
	GROUP BY StatementCode, MarketLetterCode, ProgramCode, Level5Code
	
	/*
	Growth in 2020 relative to the 2019 Sales
	>110%			3%
	100% to 109.9%	2%
	90% to 99.9%	1%
	*/

	-- Determine Growth on total sales even though we are storing the data at brand level sales
	UPDATE @TEMP_TOTALS
	SET [Growth] = [Growth_QualSales_CY] / [Growth_QualSales_LY]
	WHERE [Growth_QualSales_LY] > 0
	
	-- Update the Planned Growth Reward %
	UPDATE @TEMP_TOTALS SET [Growth_Reward_Percentage] = 0.03 WHERE ([Growth] = 0 AND [Growth_QualSales_CY] > 0) OR [Growth] >= 1.1 
	UPDATE @TEMP_TOTALS SET [Growth_Reward_Percentage] = 0.02 WHERE [Growth_Reward_Percentage] = 0 AND [Growth] >= 1
	-- Rounding Rule: Apply a rounding rule of 0.5% to minimum reward tier % requirement.
	UPDATE @TEMP_TOTALS SET [Growth_Reward_Percentage] = 0.01 WHERE [Growth_Reward_Percentage] = 0 AND [Growth]+0.005 >= 0.9 

	-- Rounding Rule: Apply a rounding rule of 0.5% to minimum reward tier % requirement.
	UPDATE @TEMP_TOTALS
	SET [Growth]=0.9
	WHERE [Growth] < 0.9 AND [Growth_Reward_Percentage]=0.01 

	-- NEW: RRT-503 - Add the "gimmes" to the growth reward (an extra 2% for Indep West and 1% for Coops)

	UPDATE @TEMP_TOTALS 
	SET [Growth_Reward_Percentage] += IIF([Level5Code]='D000001',0.02,0.01)
	WHERE [Level5Code] IN ('D000001','D0000117')


	-- Update the Growth % and  Planned Growth Reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.[Growth]=T2.[Growth]
		,T1.[Growth_Reward_Percentage] = T2.[Growth_Reward_Percentage]
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2 
		ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode] = T2.[MarketLetterCode] AND T1.[ProgramCode] = T2.[ProgramCode] AND T1.[Level5Code] = T2.[Level5Code]
	
	-- Update reward
	UPDATE @TEMP SET [Growth_Reward] = [Growth_Reward_Sales] * [Growth_Reward_Percentage]
	
	-- NEW: RRT-503 - Add the "gimme" rewards for Liberty and Centurion for CCP only
	--UPDATE @TEMP SET [Lib_Reward_Percentage] = CASE WHEN [Level5Code] = 'D000001' THEN 0.02 WHEN [Level5Code] = 'D0000117' THEN 0.0175 ELSE 0 END WHERE [MarketLetterCode] = 'ML2020_W_CCP'
	UPDATE @TEMP SET [Lib_Reward_Percentage] = IIF([Level5Code] = 'D000001',0.02,0.0175)  WHERE [MarketLetterCode] = 'ML2020_W_CCP'
	UPDATE @TEMP SET [Lib_Reward] = [Lib_Reward_Sales] * [Lib_Reward_Percentage] WHERE [MarketLetterCode] = 'ML2020_W_CCP' AND [Lib_Reward_Sales] > 0

	-- UPDATE @TEMP SET [Cent_Reward_Percentage] = CASE WHEN [Level5Code] = 'D000001' THEN 0.01 ELSE 0 END WHERE [MarketLetterCode] = 'ML2020_W_CCP'
	UPDATE @TEMP SET [Cent_Reward_Percentage] = IIF([Level5Code]='D000001',0.01,0) WHERE [MarketLetterCode] = 'ML2020_W_CCP'
	UPDATE @TEMP SET [Cent_Reward] = [Cent_Reward_Sales] * [Cent_Reward_Percentage] WHERE [MarketLetterCode] = 'ML2020_W_CCP' AND [Cent_Reward_Sales] > 0
		 
	-- Update final reward
	UPDATE @TEMP SET [Reward] = [Growth_Reward] + [Lib_Reward] + [Cent_Reward] WHERE [Growth_Reward] + [Lib_Reward] + [Cent_Reward] > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD		
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'
	BEGIN
		UPDATE T1
		SET [Growth_Reward] = [Growth_Reward] * ISNULL(EI.FieldValue/100,1),
			[Lib_Reward] = [Lib_Reward] * ISNULL(EI.FieldValue/100,1),
			[Cent_Reward] = [Cent_Reward] * ISNULL(EI.FieldValue/100,1),
			[Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0
	END
	
	DELETE FROM RP2020_DT_Rewards_W_PlanningTheBusiness WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_PlanningTheBusiness(Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Growth_QualSales_CY, Growth_QualSales_LY, Growth_Reward_Sales, Growth_Reward_Percentage, Growth_Reward, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Growth_QualSales_CY, Growth_QualSales_LY, Growth_Reward_Sales, Growth_Reward_Percentage, Growth_Reward, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward
	FROM @TEMP
	
END

