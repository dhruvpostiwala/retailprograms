﻿
CREATE   PROCEDURE [dbo].[RP_RefreshOrders] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	DELETE T1
	FROM [dbo].[RP_DT_Orders] T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'

	INSERT INTO [RP_DT_Orders](StatementCode,DataSetCode, TransCode, Season, RetailerCode, FarmCode, ProductCode, Conversion, InvoiceDate, InvoiceNumber, Quantity , InRadius, RP_ID)
	SELECT ST.StatementCode,RS.DataSetCode, Orders.TransCode,Orders.Year, Orders.RetailerCode,Orders.FarmCode, Orders.ProductCode,Orders.Conversion, Orders.InvoiceDate, Orders.InvoiceNumber,Orders.Quantity
		,ISNULL(Grower.InRadius,0) AS InRadius
		--,DENSE_RANK() OVER(PARTITION BY ST.STATEMENTCODE ORDER BY Orders.RetailerCode) AS RP_ID
		,ROW_NUMBER() OVER(PARTITION BY St.StatementCode ORDER BY Orders.RetailerCode, Orders.ProductCode) AS RP_ID
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN(
			SELECT TransCode, Year, RetailerCode, FarmCode, ProductCode, Conversion, InvoiceDate, InvoiceNumber, Quantity 		
			FROM Orders
			WHERE Year=@SEASON
		) Orders 
		ON Orders.RetailerCode = MAP.ActiveRetailerCode 
		LEFT JOIN (
			SELECT FL.RetailerCode, FL.FarmCode, 1 AS InRadius
			FROM PER_RetailFarmList FL
				INNER JOIN PER_Relationships REL
				ON REL.PerID=FL.PerID
			WHERE REL.IsActive=1 AND REL.Season=@SEASON-1
		) Grower
		ON Grower.FarmCode=Orders.FarmCode AND Grower.RetailerCode=Orders.RetailerCode
	WHERE RS.Region='WEST' AND RS.VersionType <> 'USE CASE'
END
