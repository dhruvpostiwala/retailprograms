﻿
CREATE PROCEDURE [dbo].[RP_Agent_Transfer_Statements] @STATEMENTS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	--RETURN;
			
/* Statements - Status does not get updated because it will be updated during nightly verifications */
-- DECLARE @db varchar(255) 
-- EXEC [dbo].[GetDbName] @dbName=@db output

DECLARE @TRANSFER_STATEMENTS UDT_RP_STATEMENT;
DECLARE @TRANSFER_STATEMENTS_INFO AS UDT_RP_STATEMENT_INFO;
DECLARE @SEASON INT;
DECLARE @REGION VARCHAR(4);

DECLARE @SQL nvarchar (MAX);
DECLARE @Parmdef nvarchar (500);

-- SET IDENTITY_INSERT '+ @db+'.dbo.RPWeb_Statements ON


INSERT INTO @TRANSFER_STATEMENTS(StatementCode)
SELECT StatementCode FROM  @STATEMENTS
		
	UNION

SELECT DISTINCT DataSetCode_L5 
FROM RP_Statements
WHERE ISNULL(DataSetCode_L5,0) > 0 AND StatementCode IN (SELECT StatementCode FROM @STATEMENTS)
	


SET @Parmdef = ' @TRANSFER_STATEMENTS UDT_RP_STATEMENT READONLY '
SET @SQL = '

SET IDENTITY_INSERT dbo.RPWeb_Statements ON

MERGE RPWeb_Statements dest
USING(
	SELECT [StatementCode],[StatementType],[VersionType],[StatementLevel],[Status],[Season],[Region],[RetailerCode],[RetailerType],[Level2Code],[Level5Code],[ActiveRetailerCode],[DataSetCode],[DataSetCode_L5],[UC_SRC_Statementcode],[UC_Job_ID],[IsPayoutStatement]
	FROM RP_Statements		
	WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TRANSFER_STATEMENTS)
) src ON src.[StatementCode]=dest.[StatementCode]
WHEN MATCHED THEN
	UPDATE SET 
			[StatementType]=SRC.[StatementType]
			,[VersionType]=SRC.[VersionType]
			,[StatementLevel]=SRC.[StatementLevel]
			,[Status]=SRC.[Status]
			,[Season]=SRC.[Season]
			,[Region]=SRC.[Region]
			,[RetailerCode]=SRC.[RetailerCode]
			,[RetailerType]=SRC.[RetailerType]
			,[Level2Code]=SRC.[Level2Code]
			,[Level5Code]=SRC.[Level5Code]
			,[ActiveRetailerCode]=SRC.[ActiveRetailerCode]
			,[DataSetCode]=SRC.[DataSetCode]
			,[DataSetCode_L5]=SRC.[DataSetCode_L5]
			,[IsPayoutStatement]=SRC.[IsPayoutStatement]
WHEN NOT MATCHED THEN
	INSERT([StatementCode],[StatementType],[VersionType],[StatementLevel],[Status],[Season],[Region],[RetailerCode],[RetailerType],[Level2Code],[Level5Code],[ActiveRetailerCode],[DataSetCode],[DataSetCode_L5],[UC_SRC_Statementcode],[UC_Job_ID],[IsPayoutStatement])
	VALUES([StatementCode],[StatementType],[VersionType],[StatementLevel],[Status],[Season],[Region],[RetailerCode],[RetailerType],[Level2Code],[Level5Code],[ActiveRetailerCode],[DataSetCode],[DataSetCode_L5],[UC_SRC_Statementcode],[UC_Job_ID],[IsPayoutStatement]);

SET IDENTITY_INSERT dbo.RPWeb_Statements OFF '

	EXEC SP_EXECUTESQL @SQL, @Parmdef,  @TRANSFER_STATEMENTS
	
	-- INSERT STATUS FIELD IF NONE EXIST
	INSERT INTO RPWeb_StatementInformation(StatementCode, FieldName, FieldValue)
	SELECT T1.StatementCode,'Status','Open'
	FROM @TRANSFER_STATEMENTS T1
		LEFT JOIN RPWeb_StatementInformation T2
		ON T2.StatementCode=T1.StatementCode AND T2.FieldName='Status'
	WHERE ISNULL(T2.StatementCode,0)=0

	/* Statement Locations Details at Operation Unit Level (LEVEL 2) and HO Level (Level 5) */
	DELETE FROM RPWeb_StatementLocations WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TRANSFER_STATEMENTS)

	INSERT INTO RPWeb_StatementLocations(StatementCode, StatementLevel, RetailerCode)
	SELECT MAP.StatementCode, IIF(MAP.RetailerCode=ST.RetailerCode,ST.StatementLevel,'N/A') AS [StatementLevel], MAP.RetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN @TRANSFER_STATEMENTS TS		ON TS.StatementCode=MAP.StatementCode
		INNER JOIN RP_Statements ST				ON ST.StatementCode=MAP.StatementCode 
	WHERE ST.Region='West' AND ST.StatementLevel IN ('LEVEL 2','LEVEL 5')

		UNION
	
	SELECT MAP.StatementCode, 'LEVEL 1' AS [StatementLevel], MAP.RetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN @TRANSFER_STATEMENTS TS		ON TS.StatementCode=MAP.StatementCode
		INNER JOIN RP_Statements ST				ON ST.StatementCode=MAP.StatementCode 
	WHERE ST.Region='East' AND ST.StatementLevel='LEVEL 2'	

		UNION
	
	SELECT TS.StatementCode, ST.[StatementLevel], ST.RetailerCode
	FROM @TRANSFER_STATEMENTS TS		
		INNER JOIN RP_Statements ST				ON ST.StatementCode=TS.StatementCode 
	WHERE ST.Region='East' AND ST.StatementLevel='LEVEL 2'	
	   
	/* Reward Summary - BEGIN */
	UPDATE rs1
	SET [RewardValue]=rs2.[RewardValue], [RewardEligibility]=rs2.[RewardEligibility],[RewardMessage]=rs2.[RewardMessage],[TotalReward]=0
	FROM RPWeb_Rewards_Summary rs1
		INNER JOIN @TRANSFER_STATEMENTS ts		ON rs1.[StatementCode]=ts.[StatementCode]
		INNER JOIN RP_DT_Rewards_Summary rs2	ON rs1.[StatementCode]=rs2.[StatementCode] AND rs1.[RewardCode]=rs2.[RewardCode] AND rs1.[MarketLetterCode]=rs2.[MarketLetterCode]
		

	INSERT INTO RPWeb_Rewards_Summary([StatementCode],[MarketLetterCode],[RewardCode],[RewardValue],[RewardEligibility],[RewardMessage],[TotalReward],[PaidToDate],[CurrentPayment],[NextPayment])
	SELECT rs1.[StatementCode],rs1.[MarketLetterCode],rs1.[RewardCode],rs1.[RewardValue],rs1.[RewardEligibility],rs1.[RewardMessage],0,0,0,0
	FROM RP_DT_Rewards_Summary rs1
		INNER JOIN @TRANSFER_STATEMENTS ts		ON ts.[StatementCode]=rs1.[StatementCode]
		LEFT JOIN RPWeb_Rewards_Summary rs2		ON rs1.[StatementCode]=rs2.[StatementCode] AND rs1.[RewardCode]=rs2.[RewardCode] AND rs1.[MarketLetterCode]=rs2.[MarketLetterCode]
	WHERE rs2.[StatementCode] IS NULL

	UPDATE rs1
	SET [RewardValue]=0,[RewardEligibility]='INELIGIBLE',[RewardMessage]=NULL,[TotalReward]=0
	FROM RPWeb_Rewards_Summary rs1
		INNER JOIN @TRANSFER_STATEMENTS ts			ON ts.[StatementCode]=rs1.[StatementCode]
		INNER JOIN RP_Statements ST					ON ST.StatementCode=RS1.Statementcode
		INNER JOIN RP_Config_Rewards_Summary CFG	ON CFG.RewardCode=rs1.RewardCode AND CFG.Season=ST.Season		
		LEFT JOIN RP_DT_Rewards_Summary rs2			ON rs1.[StatementCode]=rs2.[StatementCode] AND rs1.[RewardCode]=rs2.[RewardCode] AND rs1.[MarketLetterCode]=rs2.[MarketLetterCode]
	WHERE rs2.[StatementCode] IS NULL AND CFG.RewardType = 'REGULAR' 

	
	DELETE RS1
	FROM RPWeb_Rewards_Summary rs1
		INNER JOIN @TRANSFER_STATEMENTS ts			ON ts.[StatementCode]=rs1.[StatementCode]
	WHERE RS1.[RewardEligibility]='INELIGIBLE' AND RS1.[PaidToDate]=0 AND RS1.[CurrentPayment]=0
	
		
	/* WE NEED TO CREATE A TABLE THAT HAS THE REGION AND SEASON INFORMATION OF THE STATEMENTS AS WELL */
	INSERT INTO @TRANSFER_STATEMENTS_INFO(StatementCode, Season, Region, StatementType)
	SELECT RS.[StatementCode],RS.[Season],RS.[Region],RS.[StatementType]
	FROM RP_Statements RS
		INNER JOIN @STATEMENTS ST		ON ST.[StatementCode]=RS.[StatementCode]
	WHERE COALESCE(RS.[Season],0) > 0

	DECLARE SEASON_REGION_LOOP CURSOR FOR 
		SELECT DISTINCT [Season], [Region] FROM @TRANSFER_STATEMENTS_INFO
	OPEN SEASON_REGION_LOOP
	FETCH NEXT FROM SEASON_REGION_LOOP INTO @SEASON, @REGION
	WHILE(@@FETCH_STATUS=0)
	BEGIN	
		/* CREATE A SUB STATEMENT LIST THAT CONTAINS ONLY STATEMENTS FOR THE CURRENT YEAR AND REGION */
		DELETE FROM @TRANSFER_STATEMENTS
		INSERT INTO @TRANSFER_STATEMENTS
		SELECT [StatementCode] FROM @TRANSFER_STATEMENTS_INFO WHERE [Season]=@SEASON AND [Region]=@REGION GROUP BY [StatementCode]

		SET @SQL=''	
		DECLARE CURSOR_TRANSFER_DATA CURSOR FOR 
			SELECT '
				DELETE FROM '+so.[name]+' 
				WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TRANSFER_STATEMENTS) 
				INSERT INTO '+so.[name]+'('+STUFF((
					SELECT ',['+[Name]+']'
					FROM syscolumns sc
					WHERE sc.[id]=so.[id] 
					ORDER BY sc.[id]
					FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+')
				SELECT '+STUFF((
					SELECT ',DT.['+[Name]+']'
					FROM syscolumns sc
					WHERE sc.[id]=so.[id]
					ORDER BY sc.[id]
					FOR XML PATH(''),TYPE).value('.','varchar(max)'),1,1,'')+'
				FROM ' + CFG.[DT_TableName] + ' DT 
				WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TRANSFER_STATEMENTS) 
				/*
				DELETE T1  
				FROM ' + CFG.[DT_TableName]  + ' T1
					INNER JOIN @TRANSFER_STATEMENTS  T2			ON T2.[StatementCode]=T1.[StatementCode]		
					INNER JOIN RP_Statements RS					ON RS.StatementCode=T2.StatementCode
				WHERE RS.VersionType <> ''USE CASE''
				*/
				' AS [CODE]
			FROM sysobjects so
				INNER JOIN (
					SELECT TableName, DT_TableName
					FROM RP_Config_WebTableNames
					WHERE Season=@SEASON AND Region IN ('Both',@Region) AND TransferStatementAction='Yes' AND IsNULL(DT_TableName,'') <> '' 
				) CFG
				ON CFG.[TableName]=SO.[Name] 
			WHERE so.[xtype]='U' 
	
		OPEN CURSOR_TRANSFER_DATA
		FETCH NEXT FROM CURSOR_TRANSFER_DATA INTO @SQL
		WHILE(@@FETCH_STATUS=0)
		BEGIN	
			EXEC SP_EXECUTESQL @SQL, @Parmdef, @TRANSFER_STATEMENTS					
			--PRINT @SQL
			FETCH NEXT FROM CURSOR_TRANSFER_DATA INTO @SQL
		END
		CLOSE CURSOR_TRANSFER_DATA
		DEALLOCATE CURSOR_TRANSFER_DATA
		
	FETCH NEXT FROM SEASON_REGION_LOOP INTO @SEASON, @REGION
	END
	CLOSE SEASON_REGION_LOOP
	DEALLOCATE SEASON_REGION_LOOP
END

