﻿


CREATE PROCEDURE [dbo].[RP_RefreshEstimateExceptions] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	RETURN

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4));
	
	DECLARE @ESTIMATES TABLE(
		Statementcode int NOT NULL,
		Locked_Statementcode INT NOT NULL DEFAULT 0,			
		Amount MONEY NOT NULL DEFAULT 0
	)

	DECLARE @STATEMENT_MAPPINGS TABLE (
		Statementcode INT,
		RetailerCode VARCHAR(20) NOT NULL		
	)

	INSERT INTO @STATEMENT_MAPPINGS(StatementCode, Retailercode)
	SELECT MAP.StatementCode, MAP.ActiveRetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN @STATEMENTS ST	
		ON ST.Statementcode=MAP.Statementcode
					
	INSERT INTO @ESTIMATES(Statementcode, Locked_Statementcode)
	SELECT UL.StatementCode, ISNULL(LCK.Statementcode,0) AS Locked_Statementcode
	FROM @STATEMENTS UL
		INNER JOIN RPWeb_Statements LCK
		ON LCK.SRC_Statementcode=UL.Statementcode AND LCK.VersionType='Locked' AND LCK.[Status]='Active' AND LCK.ChequeRunName='OCT RECON'
		
	UPDATE T1
	SET Amount = t2.Amount		
	FROM @ESTIMATES T1
		INNER JOIN (
			SELECT MAP.StatementCode, SUM(ISNULL(i.Amount,0)) AS Amount
			FROM @STATEMENT_MAPPINGS MAP
				INNER JOIN ( 
					SELECT Retailercode, SUM(Amount) Amount
					FROM RP_Seeding_Est_Exceptions
					WHERE Season=@SEASON AND [Status]='Open'
					GROUP BY Retailercode
				) i 
				ON i.Retailercode = map.Retailercode 
			GROUP BY MAP.StatementCode
		) T2
		ON T2.Statementcode=T1.Statementcode


	UPDATE T1
	SET [Status]='Posted', [Statementcode]=MAP.Statementcode, DateModified=GETDATE()
	FROM RP_Seeding_Est_Exceptions T1
		INNER JOIN @STATEMENT_MAPPINGS MAP
		ON MAP.RetailerCode=T1.RetailerCode
--	WHERE T1.[Status]='Open'

	DELETE FROM RPWeb_Est_Exceptions WHERE [Statementcode] IN (SELECT Locked_Statementcode FROM @ESTIMATES)

	INSERT INTO RPWeb_Est_Exceptions(Statementcode,Amount)
	SELECT Locked_Statementcode,ISNULL(Amount,0) Amount
	FROM @ESTIMATES
	WHERE Locked_Statementcode < 0

END

