﻿
CREATE PROCEDURE [dbo].[RP2021_Refresh_EI_FieldValues] @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SEASON INT=2021;
	DECLARE @FORECAST_STATEMENTS AS UDT_RP_STATEMENT

	-- IN CASE OF PROJECTION STATEMENT , JUST COPY THE INFORMATION ENTERED FOR RETAILERS FORECAST STATEMENT
	IF @STATEMENT_TYPE='PROJECTION'
	BEGIN
		INSERT INTO RPWeb_User_EI_FieldValues(StatementCode,FieldCode,ProgramCode,MarketLetterCode,FieldValue,CalcFieldValue)
		SELECT RS.StatementCode_P, EI_F.FieldCode, EI_F.ProgramCode, EI_F.MarketLetterCode, EI_F.FieldValue, EI_F.CalcFieldValue
		FROM (
			SELECT F.StatementCode AS [StatementCode_F], P.StatementCode AS [StatementCode_P]
			FROM RP_Statements F
				INNER JOIN (
					SELECT StatementCode, Season, RetailerCode, StatementLevel
					FROM RP_Statements
					WHERE [StatementCode] IN (SELECT StatementCode From @Statements)						
				) P
				ON P.Season=F.Season AND P.RetailerCode=F.RetailerCode AND P.StatementLevel=F.StatementLevel 
			WHERE F.[Status]='Active' AND F.StatementType='Forecast' AND F.VersionType='Final-Preferred'			
		) RS
			INNER JOIN RPWeb_User_EI_FieldValues EI_F		
			ON EI_F.StatementCode=RS.StatementCode_F
			LEFT JOIN RPWeb_User_EI_FieldValues  EI_P		
			ON EI_P.StatementCode=RS.StatementCode_P AND  EI_P.ProgramCode=EI_F.ProgramCode AND EI_P.MarketLetterCode=EI_F.MarketLetterCode AND  EI_P.FieldCode=EI_F.FieldCode
		WHERE ISNULL(EI_P.StatementCode,0)=0
				
		RETURN
	END 

	-- LETS TAKE CARE OF FORECAST STATEMENTS

	MERGE RPWeb_User_EI_FieldValues AS TGT
	USING (
		SELECT ST.StatementCode, EI.FieldCode, EI.ProgramCode, EI.MarketLetterCode, EI.DefaultValue, EI.CalcFieldValue
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS
			ON RS.StatementCode=ST.StatementCode 
			INNER JOIN (
				SELECT EIF.FieldCode, EIF.ProgramCode, CASE WHEN EIF.ProgramCode = 'RP2021_W_GROWTH_LOYALTY_RB' THEN 'ML2021_W_CPP' ELSE EIML.MarketLetterCode END AS 'MarketLetterCode', EIF.DefaultValue, EIF.CalcFieldValue
				FROM RP_Config_Programs PRG
					INNER JOIN RP_Config_EI_Fields EIF	ON EIF.ProgramCode=PRG.ProgramCode
					LEFT JOIN RP_Config_EI_ML_Fields EIML	ON EIML.ProgramCode = EIF.ProgramCode AND EIML.FieldCode = EIF.FieldCode
				WHERE PRG.[Season]=@SEASON AND PRG.[Status]='Active' AND ((EIF.ProgramCode IN ('RP2021_W_ALL_PROGRAMS','RP2021_E_ALL_PROGRAMS') AND EIF.[DisplayOnce]=0) OR (EIF.ProgramCode = 'RP2021_W_GROWTH_LOYALTY_RB' AND EIF.[DisplayOnce] = 1))
			) EI
			ON 1=1		
		WHERE (RS.Region='WEST' AND EI.ProgramCode IN ('RP2021_W_ALL_PROGRAMS', 'RP2021_W_GROWTH_LOYALTY_RB')) OR (RS.Region='EAST' AND EI.ProgramCode='RP2021_E_ALL_PROGRAMS')
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.MarketLetterCode=TGT.MarketLetterCode
	WHEN NOT MATCHED THEN
		INSERT (StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)	
		VALUES (SRC.StatementCode, SRC.FieldCode, SRC.ProgramCode, SRC.MarketLetterCode, SRC.DefaultValue, SRC.CalcFieldValue);

	-- FOLLOWING INPUTS SHOULD BE DISPLAYED ONLY ONCE FOR USER
	MERGE RPWeb_User_EI_FieldValues AS TGT
	USING (
		SELECT ST.StatementCode, EIF.FieldCode, MLP.ProgramCode, MLP.MarketLetterCode,  EIF.DefaultValue, EIF.CalcFieldValue
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode		
			INNER JOIN RP_Config_EI_Fields EIF		ON EIF.ProgramCode=MLP.ProgramCode	
		WHERE EIF.DisplayOnce=1			
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.MarketLetterCode=TGT.MarketLetterCode AND SRC.FieldCode=TGT.FieldCode
	WHEN NOT MATCHED THEN
		INSERT (StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)	
		VALUES (SRC.StatementCode, SRC.FieldCode, SRC.ProgramCode, SRC.MarketLetterCode, SRC.DefaultValue, SRC.CalcFieldValue);

END