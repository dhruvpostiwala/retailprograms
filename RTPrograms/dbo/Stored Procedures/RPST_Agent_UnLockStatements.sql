﻿


CREATE PROCEDURE [dbo].[RPST_Agent_UnLockStatements] @SEASON INT,  @USERNAME VARCHAR(150), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	/* 
		@STATEMENTCODES PARAM CONTAINS LOCKED STATEMENTCODES 
		THIS ACTION WILL BE EXECUTED ON MULTIPLE STATEMENT CODES
		DEPENDS ON VALUE FROM "LOCK_JOB_ID" COLUMN TO TARGET ALL STATEMENTS IN A FAMILY WHERE APPLICABLE
		WE CANNOT USE "LOCK_JOB_ID"  WHEN WORKING WITH LEVEL 5 SUMMARY STATEMENTS
	*/
	
	SET NOCOUNT ON;

	DECLARE @EDIT_HISTORY RP_EDITHISTORY;
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT;
	DECLARE @UNLOCKED_STATEMENTS AS UDT_RP_STATEMENT;

	DECLARE @LOCKED_STATEMENTS TABLE(
		StatementCode INT NOT NULL,			
		SRC_StatementCode INT NOT NULL,
		Summary VARCHAR(3) NOT NULL
	)

	DECLARE @SRC_IDS TABLE (ID INT)

	BEGIN TRY

		
		INSERT INTO @STATEMENTS(STATEMENTCODE)
		SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')

		/*
		DEMAREY BAKER APRIL 20, 2021
		THERE ARE SOME INSTANCES WHERE CERTAIN GRIDS NAMELY THE ONES THAT DO WITH APPROVAL 
		PASS IN THE UNLOCKED STATEMENTCODE WHICH WILL MAKE THIS NOT WORK
		CONVERTING THEM BACK HERE
		*/
		UPDATE S
		SET StatementCode = LK.StatementCode
		FROM @STATEMENTS S 
		INNER JOIN RPWeb_Statements LK ON S.StatementCode = LK.SRC_StatementCode AND Status = 'Active'
		WHERE S.StatementCode > 0


		DELETE @STATEMENTS 
		WHERE [StatementCode] 
		NOT IN (SELECT StatementCode FROM RPWeb_Statements WHERE [Season]=@Season AND [Status]='Active' AND [VersionType]='Locked')

		INSERT INTO @LOCKED_STATEMENTS(StatementCode, SRC_StatementCode, Summary)
		SELECT StatementCode, SRC_StatementCode, Summary
		FROM TVF_Get_LockedStatementCodesToRefresh(@STATEMENTS) 
		WHERE SUMMARY='No'

		INSERT INTO @UNLOCKED_STATEMENTS(StatementCode)
		SELECT SRC_StatementCode FROM @LOCKED_STATEMENTS
			   
		/* 
			TARGET: LOCKED STATEMENTS
			ACTION: UPDATE STATUS TO DELETED 
		*/			
		UPDATE T1
		SET [Status]='Deleted' ,[Deleted_Date]=GETDATE()
		FROM RPWeb_Statements T1
			INNER JOIN @LOCKED_STATEMENTS T2
			ON T2.StatementCode=T1.StatementCode

		/* 
			TARGET: UNLOCKED STATEMENT
			ACTION: UPDATE STATUS TO READY FOR QA 
		*/
		UPDATE STI
		SET [FieldValue]='Ready for QA'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA process - Unlock','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI		
			INNER JOIN @LOCKED_STATEMENTS LS
			ON LS.SRC_StatementCode=STI.StatementCode		
		WHERE STI.[FieldName]='Status' 
	
		-- REMOVE ASSIGNMENTS
		DELETE STI
		FROM RPWeb_StatementInformation STI		
			INNER JOIN @LOCKED_STATEMENTS LS
			ON STI.StatementCode=LS.SRC_StatementCode
		WHERE STI.[FieldCategory]='Assignment'


		EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY

		
		-- DUPLICATE EXCEPTION DATA AND FLAG OLD EXCEPTION BINDED TO DELETED LOCKED STATEMENT
		--HANDLE LEVEL ONE EXCEPTIONS 		
		INSERT INTO RPWeb_Exceptions(RetailerCode, StatementCode, Season, RewardCode, Exception_Level, Exception_Desc, Exception_Type, Exception_Value, Exception_Reward_Margin, Status, Src_StatementCode,Src_ID,Comments,DateCreated,DateModified,PostedBy, Show_In_UL,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode])
		SELECT EX.RetailerCode, 0 AS StatementCode, EX.Season, EX.RewardCode, EX.Exception_Level, EX.Exception_Desc, EX.Exception_Type, EX.Exception_Value, EX.Exception_Reward_Margin,	EX.Status, EX.Src_StatementCode, 0 AS SRC_ID, EX.Comments, GETDATE(), GETDATE(), ex.PostedBy, 1 as Show_In_UL
			,[Exception_Value_A],[Exception_Value_B],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[MarketLetterCode]
		FROM RPWeb_Exceptions EX
			INNER JOIN @LOCKED_STATEMENTS LS ON LS.StatementCode=EX.StatementCode
			INNER JOIN RPWeb_Statements ST	ON ST.StatementCode = EX.StatementCode
		WHERE EX.Status='Approved'  AND EX.Season=@SEASON  AND EX.VersionType='Current' 
			AND (ST.Season = 2020 OR ST.Region='East')

		--THIS IS FINE
		UPDATE EX
		SET SHOW_IN_UL = 0
			,VersionType = 'Void'
		FROM RPWEB_EXCEPTIONS EX
			INNER JOIN @LOCKED_STATEMENTS LS 
			ON LS.STATEMENTCODE=EX.STATEMENTCODE
		WHERE EX.STATUS='Approved' AND SEASON =  @SEASON AND EX.VersionType='Current'
		
		EXEC RP_REFRESHPAYMENTINFORMATION @SEASON, 'ACTUAL', @UNLOCKED_STATEMENTS
	END TRY
					
	BEGIN CATCH
		SELECT 'FAILED' AS [STATUS], ERROR_MESSAGE() + ' AT LINE ' + CAST(ERROR_LINE() AS VARCHAR)  AS ERROR_MESSAGE
		RETURN;	
	END CATCH
	
	SELECT 'SUCCESS' AS [STATUS], '' AS ERROR_MESSAGE, '' AS CONFIRMATION_MESSAGE, '' AS WARNING_MESSAGE

END




