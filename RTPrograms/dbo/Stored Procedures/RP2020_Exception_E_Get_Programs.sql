﻿

CREATE PROCEDURE [dbo].[RP2020_Exception_E_Get_Programs] 
	@STATEMENTCODE INT, 
	@JSON_DATA NVARCHAR(MAX) OUTPUT
AS
BEGIN
		SET NOCOUNT ON;
		DECLARE @SEASON INT=2020
		DECLARE @REGION VARCHAR(4)='EAST'
		
		DECLARE @TEMP TABLE (
			StatementCode INT,
			reward_code VARCHAR(50),
			title  VARCHAR(100),
			exception_type  VARCHAR(50)
		)

		SET @JSON_DATA='[]'

		INSERT INTO @TEMP(StatementCode,[reward_code],[title],exception_type)
		SELECT EEX.StatementCode,EEX.RewardCode [reward_code], RewardLabel as [title],'NON-TIER' AS exception_type		
		FROM [dbo].[RP_Config_Rewards_Summary] S
		INNER JOIN (
				SELECT DISTINCT StatementCode, P.RewardCode + 'X' AS RewardCode 
				FROM (
					--IN CASE THERE IS NOTHING IN THE TABLE ITSELF SOME PROGRAMS DONT INSERT WHENN IT IS ZERO 
					SELECT StatementCode, PS.ProgramCode
					FROM RPWeb_Rewards_Summary RS 
						INNER JOIN RP_COnfig_programs PS 
						ON PS.RewardCode = RS.RewardCode
					WHERE PS.REGION = @REGION AND RS.TotalReward = 0
					
					UNION ALL
					
					SELECT StatementCode, ProgramCode 
					FROM (
						SELECT StatementCode,ProgramCode, MAX(Reward_percentage) over (Partition by statementCode) AS Reward_Percentage
						FROM [dbo].[RP2020Web_Rewards_E_PlanningTheBusiness] 
					) S WHERE S.Reward_Percentage = 0
					GROUP BY S.StatementCode, ProgramCode
					 
				
					UNION ALL

					SELECT StatementCode,ProgramCode
					FROM [dbo].[RP2020Web_Rewards_E_POGSalesBonus]
					WHERE Reward_Percentage = 0
			
					UNION ALL


					SELECT StatementCode,ProgramCode
					FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport]
					WHERE Pursuit_Reward_Amount = 0 
			
					UNION ALL

					SELECT StatementCode, ProgramCode 
					FROM (
						SELECT StatementCode,ProgramCode, MAX(Reward_percentage) over (Partition by statementCode) AS Reward_Percentage
						FROM [dbo].[RP2020Web_Rewards_E_PortfolioSupport] 
					) S 
					WHERE S.Reward_Percentage < 0.06
					GROUP BY S.StatementCode, ProgramCode


					UNION ALL
				
					SELECT StatementCode, ProgramCode 
					FROM (
						SELECT StatementCode,ProgramCode, MAX(Reward_Amount) over (Partition by statementCode) AS Reward_Amount
						FROM [dbo].[RP2020Web_Rewards_E_RowCropFungSupport] 
					) S WHERE S.Reward_Amount < 1.75
					GROUP BY S.StatementCode, ProgramCode


					UNION ALL
					
					SELECT StatementCode, ProgramCode 
					FROM (
						SELECT StatementCode,ProgramCode, MAX(Reward_Percentage) over (Partition by statementCode) AS Reward_Percentage
						FROM [dbo].[RP2020Web_Rewards_E_SSLB] 
					) S WHERE S.Reward_Percentage < 4.25
					GROUP BY S.StatementCode, ProgramCode

					UNION ALL

					SELECT StatementCode, ProgramCode 
					FROM (
						SELECT StatementCode,ProgramCode, MAX(Reward_Percentage) over (Partition by statementCode) AS Reward_Percentage
						FROM [dbo].[RP2020Web_Rewards_E_SupplySales] 
					) S WHERE S.Reward_Percentage < 0.0075
					GROUP BY S.StatementCode, ProgramCode
					
					UNION ALL

					SELECT StatementCode,ProgramCode
					FROM [dbo].[RP2020Web_Rewards_E_HortiCultureRetailSupport]
					WHERE Reward_Percentage = 0

				) PRG 
				INNER JOIN RP_Config_Programs P 
				ON PRG.ProgramCode = P.ProgramCode 
			WHERE P.RewardCode NOT IN ('CGAS_E','PRX_HEAD_E','SG_E') 
		) EEX 
		ON S.RewardCode = EEX.RewardCode
		INNER JOIN RPWeb_Statements ST 
		ON ST.StatementCode = EEX.StatementCode AND DatasetCode = 0
	WHERE EEX.StatementCode = @StatementCode 
		AND S.Season = @SEASON	
		AND S.RewardType='Exception'
		--AND S.RewardLabel LIKE '%Exception%' 
			
			 
	-- User should not be able to create another exception of the same type on the statement if they already have one already
	--in  submitted or approved status   
	DELETE T1
	FROM @TEMP T1
	INNER JOIN RPWeb_Exceptions E 
	ON E.StatementCode = T1.StatementCode and E.RewardCode = T1.reward_code
	WHERE E.status NOT IN ('Rejected','Obsolete') and E.exception_type <> 'OTHER' and E.Season = @Season
	-- spelling mistake Obselete

	UPDATE @TEMP 
	SET exception_type  = 'TIER' 
	WHERE Reward_Code IN ('PORT_SUP_EX','SSLB_EX','SUPPLY_SALES_EX','ROW_CROP_FUNG_EX')

	SELECT @JSON_DATA = ISNULL((
	SELECT reward_code, title,exception_type 
	FROM @TEMP
	FOR JSON PATH) ,'[]')

END

