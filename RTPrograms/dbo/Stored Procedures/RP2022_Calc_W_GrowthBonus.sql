﻿
CREATE PROCEDURE [dbo].[RP2022_Calc_W_GrowthBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		RewardBrand VARCHAR(255) NOT NULL,
		QualSales_CY MONEY NOT NULL,
		QualSales_LY_Canola MONEY NOT NULL,
		QualSales_LY_Canola_INDEX MONEY NOT NULL,
		QualSales_LY_Pulse MONEY NOT NULL,
		QualSales_LY_Pulse_INDEX MONEY NOT NULL,
		QualSales_LY_Cereal MONEY NOT NULL,
		QualSales_LY_Cereal_INDEX MONEY NOT NULL,
		QualSales_LY MONEY NOT NULL DEFAULT 0,
		QualSales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC, 
			[RewardBrand] ASC
		)
	);

	DECLARE @QUALIFYING TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		QualSales_CY MONEY NOT NULL,
		QualSales_LY_INDEX MONEY NOT NULL DEFAULT 0,
		Growth_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC
		)
	);

	DECLARE @CROP_SEGMENT TABLE (
		Crop_Segment VARCHAR(50),
		ChemicalGroup VARCHAR(100),
		PRIMARY KEY CLUSTERED (
			[ChemicalGroup] ASC
		)
	);

	-- IDENTIFY WHICH BRAND GOES INTO EACH GROUP
	INSERT	INTO @CROP_SEGMENT
	SELECT	*
	FROM	(
		SELECT	DISTINCT 'CANOLA' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CERTITUDE', 'COTEGRA', 'FACET L', 'LANCE', 'LANCE AG', 'LIBERTY 200')

		UNION	ALL

		SELECT	DISTINCT 'PULSE' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CEVYA', 'DYAX', 'ENGENIA', 'FORUM', 'HEADLINE', 'NODULATOR', 'NODULATOR DUO SCG', 'NODULATOR LQ', 'NODULATOR N/T',
								  'NODULATOR PRO', 'NODULATOR SCG', 'NODULATOR XL', 'PRIAXOR', 'SEFINA', 'SERCADIS', 'TITAN', 'ZIDUA SC')
		UNION	ALL

		SELECT	DISTINCT 'CEREAL' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CARAMBA', 'INSURE Cereal', 'INSURE CEREAL FX4', 'INSURE PULSE', 'NEXICOR', 'TWINLINE', 'TERAXXA F4', 'FRONTIER')
	)F


	-- Get data from sales consolidated
	INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardBrand, QualSales_CY, QualSales_LY_Canola, QualSales_LY_Pulse, QualSales_LY_Cereal,QualSales_LY_Canola_INDEX,QualSales_LY_Pulse_INDEX,QualSales_LY_Cereal_INDEX)
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup,
		   SUM(IIF(@STATEMENT_TYPE IN ('FORECAST', 'PROJECTION'), TX.Price_CY * (ISNULL(EI.FieldValue, 100) / 100) ,TX.Price_CY)) [QualSales_CY],
		   SUM(IIF(CS.Crop_Segment = 'CANOLA',TX.Price_LY1 ,0)) [QualSales_LY_Canola],
		   SUM(IIF(CS.Crop_Segment = 'PULSE' ,TX.Price_LY1 ,0)) [QualSales_LY_Pulse],
		   SUM(IIF(CS.Crop_Segment = 'CEREAL',TX.Price_LY1 ,0)) [QualSales_LY_Cereal],
		   SUM(IIF(CS.Crop_Segment = 'CANOLA',TX.Price_LY1*ISNULL(TX.YOY_Index,1) ,0)) [QualSales_LY_Canola_INDEX],
		   SUM(IIF(CS.Crop_Segment = 'PULSE' ,TX.Price_LY1*ISNULL(TX.YOY_Index,1) ,0)) [QualSales_LY_Pulse_INDEX],
		   SUM(IIF(CS.Crop_Segment = 'CEREAL',TX.Price_LY1*ISNULL(TX.YOY_Index,1) ,0)) [QualSales_LY_Cereal_INDEX]
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode
		INNER JOIN ProductReference PR
		ON PR.ProductCode = TX.ProductCode
		LEFT JOIN @CROP_SEGMENT CS 
		ON CS.ChemicalGroup = PR.ChemicalGroup
		LEFT JOIN RPWeb_User_EI_FieldValues EI
		ON EI.StatementCode=ST.StatementCode AND EI.MarketLetterCode=TX.MarketLetterCode  AND EI.FieldCode = 'Radius_Percentage_CPP'
	WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code, PR.ChemicalGroup
	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- Factor in CAR indexing percentages for different product groups
		-- Canola
		UPDATE T1
		SET T1.QualSales_LY_Canola = T1.QualSales_LY_Canola * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Canola'

		-- Pulse
		UPDATE T1
		SET T1.QualSales_LY_Pulse = T1.QualSales_LY_Pulse * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Pulse'

		-- Cereal
		UPDATE T1
		SET T1.QualSales_LY_Cereal = T1.QualSales_LY_Cereal * (EI.FieldValue / 100)
		FROM @TEMP T1
			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Cereal'


		-- SET THE INDEX VALUES IDENTICAL TO THE REGULAR VALUES (FOR FORECAST AND PROJECTIONS)
		-- This is because index values are used for display and calculation purposes (aka growth)
		UPDATE	T1
		SET		T1.QualSales_LY_Canola_INDEX = T1.QualSales_LY_Canola
				,T1.QualSales_LY_Pulse_INDEX = T1.QualSales_LY_Pulse
				,T1.QualSales_LY_Cereal_INDEX = T1.QualSales_LY_Cereal
		FROM	@TEMP T1
	END -- END FORECAST PROJECTION

	-- Add the qualifying sales for last year for all three product groups together to get the total qualifying sales for last year
	UPDATE	@TEMP
	SET		QualSales_LY = QualSales_LY_Canola + QualSales_LY_Pulse + QualSales_LY_Cereal
			,QualSales_LY_INDEX = QualSales_LY_Canola_INDEX + QualSales_LY_Pulse_INDEX + QualSales_LY_Cereal_INDEX

	-- INSERT INTO QUALIFYING
	INSERT	INTO @QUALIFYING (StatementCode,MarketLetterCode,ProgramCode,QualSales_CY,QualSales_LY_INDEX)
	SELECT	StatementCode, MarketLetterCode, ProgramCode
			,SUM(QualSales_CY) AS QualSales_CY
			,SUM(QualSales_LY_INDEX) AS QualSales_LY_INDEX
	FROM	@TEMP
	GROUP	BY StatementCode, MarketLetterCode, ProgramCode

	-- CALCULATE GROWTH PERCENTAGE IN QUALIFYING TABLE
	UPDATE	@QUALIFYING
	SET		Growth_Percentage = IIF(QualSales_LY_INDEX > 0, QualSales_CY / QualSales_LY_INDEX, IIF(QualSales_CY > 0, 9.9999, 0))

	-- INSERT THE GROWTH PERCENTAGE INTO TEMP TABLE
	UPDATE	T1
	SET		T1.Growth_Percentage = T2.Growth_Percentage
	FROM	@TEMP T1
	INNER	JOIN @QUALIFYING T2 
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode

	-- Set reward percentages for each statement based on growth
	-- FCL 0.045,0.0375,0.020,0.01
	-- RC/ID and Line comapanies 0.0625,0.575,0.05,0.03
	UPDATE @TEMP SET Reward_Percentage = IIF(Level5Code = 'D0000117', 0.0400, 0.0425) WHERE [Growth_Percentage] + @ROUNDUP_VALUE >= 1.05
	UPDATE @TEMP SET Reward_Percentage = IIF(Level5Code = 'D0000117', 0.0325, 0.0375) WHERE [Reward_Percentage] = 0 AND [Growth_Percentage] + @ROUNDUP_VALUE >= 1.00
	UPDATE @TEMP SET Reward_Percentage = IIF(Level5Code = 'D0000117', 0.0200, 0.0300) WHERE [Reward_Percentage] = 0 AND [Growth_Percentage] + @ROUNDUP_VALUE >= 0.90
	
	   
	-- Calculate the reward for each statement
	UPDATE	@TEMP
	SET		Reward = QualSales_CY * Reward_Percentage
	   
	-- SET THE REWARD AMOUNT TO 0 ONLY IF THE TOTAL REWARD IS LESS THAN 0 (not at a line level but overall statement)
	UPDATE	T1
	SET		Reward  = 0 
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP T1
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0


	-- Copy records from temporary table into permanent table
	DELETE FROM RP2022_DT_Rewards_W_GrowthBonus WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2022_DT_Rewards_W_GrowthBonus(StatementCode, MarketLetterCode,ProgramCode,RewardBrand,QualSales_CY, QualSales_LY_Canola,QualSales_LY_Pulse,QualSales_LY_Cereal,QualSales_LY, Growth_Percentage, Reward_Percentage, Reward,QualSales_LY_Canola_INDEX,QualSales_LY_Pulse_INDEX,QualSales_LY_Cereal_INDEX,QualSales_LY_INDEX)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,QualSales_CY,QualSales_LY_Canola,QualSales_LY_Pulse,QualSales_LY_Cereal,QualSales_LY, Growth_Percentage, Reward_Percentage, Reward,QualSales_LY_Canola_INDEX,QualSales_LY_Pulse_INDEX,QualSales_LY_Cereal_INDEX,QualSales_LY_INDEX
	FROM @TEMP


END
