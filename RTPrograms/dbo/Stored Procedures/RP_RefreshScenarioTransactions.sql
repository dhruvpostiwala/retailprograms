﻿

CREATE PROCEDURE [dbo].[RP_RefreshScenarioTransactions] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
		
	/*
		NOTE: FOR FORECAST AND PROJECTION STATEMENT TYPES:
		- WE ARE GOING TO USE POG PLAN TRANSACTIONS INSTEAD OF ACTUAL TRANSACTIONS AS CURRENT YEAR TRANSACTIONS
	*/

	DROP TABLE IF EXISTS #TEMP_ProductReference
	SELECT  *		
	INTO #TEMP_ProductReference
	FROM TVF_Get_ProductsInfo(@SEASON)

	-- POG PLAN Transactions		
	-- DELETE FROM RP_DT_ScenarioTransactions WHERE  StatementCode IN (SELECT STATEMENTCODE FROM @STATEMENTS)
	DELETE T1
	FROM RP_DT_ScenarioTransactions T1
		INNER JOIN @STATEMENTS T2	ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3	ON T3.StatementCode=T1.StatementCode
	WHERE  T3.VersionType <> 'USE CASE'

	/*
	DROP TABLE IF EXISTS #TARGET_STATEMENTS
	SELECT RS.StatementCode, RS.Region, RS.RetailerCode, RS.Level5Code, RS.DataSetCode, RS.DataSetCode_L5
	INTO #TARGET_STATEMENTS
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS			
		ON RS.StatementCode=ST.StatementCode
	*/

	DROP TABLE IF EXISTS #TempRetailers
	SELECT RS.Region, RS.Level5Code
	INTO #TempRetailers
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS			
		ON RS.StatementCode=ST.StatementCode
	GROUP BY RS.Region, RS.Level5Code
		   
	-- EAST OR FCL/COOP
	IF EXISTS (SELECT * FROM #TempRetailers WHERE Region='EAST' OR Level5Code='D0000117')
	BEGIN
		INSERT INTO RP_DT_ScenarioTransactions(StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP,RP_ID)
		SELECT StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP		
			,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY ScenarioCode, ProductCode) AS RP_ID
		FROM (
			-- POG PLAN CHEMICAL TRANSACTIONS
			SELECT ST.StatementCode, RS.DataSetCode, RS.DataSetCode_L5, POG.ScenarioCode, POG.RetailerCode, TX.PRoductCode, '' AS HybridCode, TX.QTY AS Quantity,tx.Qty * IIF(RS.Region='EAST',PR.ConversionE,PR.ConversionW) AS [Acres]
				,PR.ChemicalGroup ,tx.QTY * PR.SDP AS Price_SDP, tx. QTY * PR.SRP AS Price_SRP,tx. QTY * PR.SWP AS Price_SWP		
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements RS			ON RS.StatementCode=ST.StatementCode
				INNER JOIN RP_DT_Scenarios POG		ON POG.StatementCode=ST.StatementCode
				INNER JOIN Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=POG.ScenarioCode
				INNER JOIN #TEMP_ProductReference PR	ON PR.ProductCode=TX.ProductCode AND PR.Region=RS.Region			

				UNION ALL

			-- RC - POG PLAN SEED TRANSACTIONS
			-- RRT-748 - update to allow CLL varieties to be imported because they all have Forecasting_W = No
			-- RC-3485 - only import Hybrid forecasts WHERE  Forecasting_X = Yes
			SELECT ST.StatementCode, RS.DataSetCode, RS.DataSetCode_L5, POG.ScenarioCode, POG.RetailerCode
				,ISNULL(INV_PR.Productcode,PR.Productcode) AS Productcode
				,IIF(PR.ChemicalGroup='InVigor',PR.ProductCode,'') AS HybridCode
				,TX.QTY
				,tx.Qty * IIF(RS.Region='EAST',PR.ConversionE,PR.ConversionW)  AS [Acres]
				,PR.ChemicalGroup AS [ChemicalGroup]
				,tx.QTY * PR.SDP AS Price_SDP
				,tx.QTY * PR.SRP AS Price_SRP
				,tx.QTY * PR.SWP AS Price_SWP
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements RS		ON RS.StatementCode=ST.StatementCode
				INNER JOIN RP_DT_Scenarios POG	ON POG.StatementCode=ST.StatementCode
				INNER JOIN (
					SELECT CAST(ScenarioID AS Varchar(20)) AS ScenarioID, Hybrid_ID, Qty FROM Retail_Plan_Scenario_SeedTransactions 
				) TX
				ON TX.ScenarioID=POG.ScenarioCode
				INNER JOIN #TEMP_ProductReference PR 
				ON PR.Hybrid_ID=TX.Hybrid_ID AND RS.Region=IIF(PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'),'West',PR.Region)
				LEFT JOIN (
					SELECT Hybrid_ID, MAX(ProductCode) AS ProductCode
					FROM ProductReference
					WHERE  ChemicalGroup='Invigor' AND  Hybrid_ID > 0 AND DocType=10
					GROUP BY Hybrid_ID
				) INV_PR
				ON INV_PR.Hybrid_Id=tX.Hybrid_ID
			WHERE  (IIF(RS.Region='EAST',PR.Forecasting_E,PR.Forecasting_W)='Yes' OR PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'))

		) DATA	
	END -- EAST OR FCL/COOP

	--WEST INDEPENDENTS
	IF EXISTS (SELECT * FROM #TempRetailers WHERE Region='WEST' AND Level5Code='D000001') 
	BEGIN 
			-- RC REP TOOL POG TRANSACTIONS (ALL WEST RETAILERS ONLY)
			-- RRT-748 - update to allow CLL varieties to be imported because they all have Forecasting_W = No
			-- RC-3485 - only import Hybrid forecasts WHERE  Forecasting_X = Yes
		INSERT INTO RP_DT_ScenarioTransactions(StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP,RP_ID)
		SELECT StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP		
			,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY ScenarioCode, ProductCode) AS RP_ID
		FROM (
			SELECT ST.STatementCode, RS.DataSetCode, RS.DataSetCode_L5, POG.ScenarioCode, POG.RetailerCode
				,ISNULL(INV_PR.ChemicalGroup,PR.ChemicalGroup) AS [ChemicalGroup]
				,ISNULL(INV_PR.ProductCode,TX.ProductCode) AS Productcode
				,IIF(PR.ChemicalGroup='InVigor',PR.ProductCode,'') AS HybridCode
				,TX.Qty  AS Quantity
				,tx.Qty * IIF(RS.Region='EAST',PR.ConversionE,PR.ConversionW) AS [Acres]				
				,tx.Qty * PR.SDP AS Price_SDP
				,tx.Qty * PR.SRP AS Price_SRP			
				,tx.Qty * PR.SWP AS Price_SWP			
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements RS		ON RS.StatementCode=ST.StatementCode
				INNER JOIN RP_DT_Scenarios POG	ON POG.StatementCode=ST.StatementCode
				INNER JOIN (
					SELECT CAST(ScenarioID AS Varchar(20)) AS ScenarioID, ProductCode, POGAmount AS Qty	FROM RCT_ScenarioDetails 
				)	TX
				ON TX.ScenarioID=POG.ScenarioCode				
				INNER JOIN #TEMP_ProductReference PR 
				ON PR.ProductCode=TX.ProductCode AND RS.Region=IIF(PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'),'West',PR.Region) AND (PR.Forecasting_W='Yes' OR PR.ChemicalGroup IN ('CLEARFIELD LENTILS','CLEARFIELD WHEAT'))			
				LEFT JOIN ProductReference_Hybrids INV_H
				ON INV_H.Hybrid_Code=TX.Productcode
				LEFT JOIN (
					SELECT Hybrid_ID, ChemicalGroup, MAX(ProductCode) AS ProductCode
					FROM ProductReference
					WHERE  ChemicalGroup='Invigor' AND  Hybrid_ID > 0 AND DocType=10
					GROUP BY Hybrid_ID, ChemicalGroup
				) INV_PR
				ON INV_PR.Hybrid_Id=INV_H.ID
		) DATA
	
	END


	-- WEST LINE COMPANIES
	-- DISTRIBUTOR POG PLAN CHEMICAL,CLL,CLW
	IF EXISTS (SELECT * FROM #TempRetailers WHERE Region='WEST' AND Level5Code IN ('D0000107','D0000137','D0000244','D520062427') ) 
	BEGIN		
		INSERT INTO RP_DT_ScenarioTransactions(StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP,RP_ID)
		SELECT StatementCode,DataSetCode,DataSetCode_L5,ScenarioCode,RetailerCode,ProductCode,HybridCode,Quantity,ISNULL(Acres,0) AS Acres,ChemicalGroup,Price_SDP,Price_SRP,Price_SWP		
			,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY ScenarioCode, ProductCode) AS RP_ID
		FROM (
			SELECT ST.STatementCode, RS.DataSetCode, RS.DataSetCode_L5, RS.RetailerCode AS ScenarioCode, RS.RetailerCode
				,ISNULL(INV_PR.ChemicalGroup,PR.ChemicalGroup) AS [ChemicalGroup]
				,ISNULL(INV_PR.ProductCode,TX.ProductCode) AS Productcode				
				,IIF(PR.ChemicalGroup='InVigor',PR.ProductCode,'') AS HybridCode
				,tx.Qty AS Quantity
				,tx.Qty * PR.ConversionW AS [Acres]				
				,tx.Qty * PR.SDP AS Price_SDP
				,tx.Qty * PR.SRP AS Price_SRP			
				,tx.Qty * PR.SWP AS Price_SWP			
			FROM @STATEMENTS ST
				INNER JOIN RP_Statements RS		ON RS.StatementCode=ST.StatementCode 				
				INNER JOIN (
					SELECT RetailerCode, ProductCode, SUM(POGPlan) AS Qty
					FROM DistributorPlanning 
					WHERE [Year]=@SEASON
					GROUP BY [RetailerCode], [Year], [ProductCode]
					HAVING SUM(POGPlan) <> 0
				) TX
				ON TX.RetailerCode=RS.RetailerCode
				INNER JOIN #TEMP_ProductReference PR 
				ON PR.ProductCode=TX.ProductCode AND RS.Region='WEST' 		
				LEFT JOIN ProductReference_Hybrids INV_H
				ON INV_H.Hybrid_Code=TX.Productcode
				LEFT JOIN (
					SELECT Hybrid_ID, ChemicalGroup, MAX(ProductCode) AS ProductCode
					FROM ProductReference
					WHERE ChemicalGroup='INVIGOR' AND  Hybrid_ID > 0 AND DocType=10
					GROUP BY Hybrid_ID, ChemicalGroup
				) INV_PR
				ON INV_PR.Hybrid_Id=INV_H.ID
		) D			
	END


END



