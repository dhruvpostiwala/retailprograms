﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_E_SalesSupportLoyaltyBonus]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
		
	/*
		2020 InVigor POG Plan /  Manual Input InVigor Target @ SDP X Corresponding InVigor % in table above
	*/

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		Actual_CY_Sales MONEY NOT NULL,
		RewardBrand_CY_Sales MONEY NOT NULL,
		RewardBrand_POG_Sales MONEY NOT NULL DEFAULT 0,			
		Growth NUMERIC(18,2) NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,5) NOT NULL DEFAULT 0,		
		Reward MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		Actual_CY_Sales MONEY NOT NULL,
		RewardBrand_CY_Sales MONEY NOT NULL,
		RewardBrand_POG_Sales MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		Reward_Percentage DECIMAL(6,5) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0		
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)
		
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,Actual_CY_Sales,RewardBrand_CY_Sales,RewardBrand_POG_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode,tx.ProgramCode
		,SUM(IIF(tx.Pricing_Type ='SDP',Price_Q_SDP_CY,Price_Q_SRP_CY)) AS Actual_CY_Sales
		,SUM(tx.Price_CY) AS RewardBrand_CY_Sales
		,SUM(tx.Price_Forecast) AS RewardBrand_POG_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX	 ON TX.StatementCode=ST.StatementCode
	WHERE rs.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode

	-- Now Determine growth based on the sum of brand sales at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,Actual_CY_Sales,RewardBrand_CY_Sales,RewardBrand_POG_Sales)
	SELECT
		CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(Actual_CY_Sales) Actual_CY_Sales,SUM(RewardBrand_CY_Sales) RewardBrand_CY_Sales,SUM(RewardBrand_POG_Sales) RewardBrand_POG_Sales
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	
	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	
	-- Determine growth on total sales
	UPDATE @TEMP_TOTALS
	SET Growth = Actual_CY_Sales / RewardBrand_POG_Sales
	WHERE RewardBrand_POG_Sales > 0

	/*
	Reward Tier on RewardBrand_CY_Sales
	100% +	4.25%
	95% to 99.99%	2.5%
	90% to 94.99%	1%
	*/

	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.0425 WHERE (Growth = 0 AND Actual_CY_Sales > 0 AND RewardBrand_POG_Sales > 0) OR Growth + @ROUNDUP_VALUE >= 1
	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.025	WHERE [Reward_Percentage]=0 AND Growth + @ROUNDUP_VALUE >= 0.95 
	UPDATE @TEMP_TOTALS  SET [Reward_Percentage]=0.01	WHERE [Reward_Percentage]=0 AND Growth  + @ROUNDUP_VALUE >= 0.90


	-- Update the growth % and reward % back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward % back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0


	--UPDATE REWARD
	UPDATE @TEMP  SET [Reward] = RewardBrand_CY_Sales * Reward_Percentage	WHERE Reward_Percentage > 0

	
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		
		UPDATE @TEMP
		SET Reward_Percentage = 0.0425, Reward=RewardBrand_CY_Sales * 0.0425
		WHERE RewardBrand_CY_Sales > 0

		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)		
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE T1.Reward > 0		
	END 


	-- INSERT INTO DT TABEL FROM TEMP TABLE		
	DELETE FROM RP2020_DT_Rewards_E_SSLB WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_E_SSLB(StatementCode, MarketLetterCode, ProgramCode, RewardBrand,Actual_CY_Sales, RewardBrand_CY_Sales, RewardBrand_POG_Sales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, 'INVIGOR' RewardBrand,Actual_CY_Sales,RewardBrand_CY_Sales, RewardBrand_POG_Sales, Reward_Percentage, Reward
	FROM @TEMP
	
		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode, 'INVIGOR' RewardBrand
		,SUM(Actual_CY_Sales) AS Actual_CY_Sales
		,SUM(RewardBrand_CY_Sales) AS RewardBrand_CY_Sales
		,SUM(RewardBrand_POG_Sales) AS RewardBrand_POG_Sales
		,AVG(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode

END
