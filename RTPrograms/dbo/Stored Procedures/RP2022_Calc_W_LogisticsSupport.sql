﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_LogisticsSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		BrandSegment VARCHAR(50) NOT NULL,
		Segment_Reward_Sales MONEY NOT NULL,
		LY_Sales MONEY NOT NULL DEFAULT 0,
		POG_Plan MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,4) NOT NULL DEFAULT 0,
		Segment_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[BrandSegment] ASC
		)
	);

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,		
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL,
		SWP MONEY NOT NULL,
		Price MONEY NOT NULL	
	)

	DECLARE @UNLOCKED_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @USE_CASE_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @GROWTH_TABLE TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,			
		Statement_LY_Sales MONEY NOT NULL,
		Statement_POG_Plan MONEY NOT NULL
	)	


	
	-- Get the data from sales consolidated
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales)
		SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel,
				SUM(TX.Price_CY) [Segment_Reward_Sales]
			   ,SUM(TX.Price_LY1) LY_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX
			ON ST.StatementCode = TX.StatementCode
		WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'REWARD_BRANDS'
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel
	ELSE IF  @STATEMENT_TYPE  = 'ACTUAL'
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales)
		SELECT ST.StatementCode, TX.MarketLetterCode, @PROGRAM_CODE AS ProgramCode, TX.GroupLabel,
				SUM(TX.Price_CY) [Segment_Reward_Sales]
			   ,SUM(TX.Price_LY1) LY_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX
			ON ST.StatementCode = TX.StatementCode
		WHERE TX.MarketLetterCode='ML2022_W_CPP' AND  TX.ProgramCode = 'ELIGIBLE_SUMMARY' AND TX.GroupType = 'Eligible_Brands'
		GROUP BY ST.StatementCode, TX.MarketLetterCode,  TX.GroupLabel
	
	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN

	-- Reward based on manual inputs
	UPDATE T1
	SET Segment_Reward_Percentage += EI.FieldValue
	FROM @TEMP T1
		INNER JOIN (
			SELECT ST.StatementCode, EI.GroupTitle, SUM(EIV.FieldValue / 100) [FieldValue]
			FROM @STATEMENTS ST
				INNER JOIN RPWeb_User_EI_FieldValues EIV
				ON ST.StatementCode = EIV.StatementCode
				INNER JOIN RP_Config_EI_Fields EI
				ON EI.ProgramCode = EIV.ProgramCode AND EI.FieldCode = EIV.FieldCode
			WHERE EI.ProgramCode = @PROGRAM_CODE
			GROUP BY ST.StatementCode, EI.GroupTitle
		) EI
		ON T1.BrandSegment = EI.GroupTitle AND T1.StatementCode = EI.StatementCode
	END ---END FORECAST PROJECTION

	
	/*Actual - TBD */


	-- Calculate reward from reward margin
	UPDATE @TEMP
	SET Reward = Segment_Reward_Sales * Segment_Reward_Percentage

	-- ONLY IF THE STATEMENT REWARD IS LESS THAN 0, UPDATE IT TO 0 
	UPDATE	T1
	SET		[Reward] = 0
	FROM	@TEMP T1
	INNER	JOIN (
		SELECT	StatementCode, MarketLetterCode, ProgramCode, SUM(Reward) AS Statement_Reward
		FROM	@TEMP
		GROUP	BY StatementCode, MarketLetterCode, ProgramCode
	)T2
		ON T1.StatementCode = T2.StatementCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode 
	WHERE	T2.Statement_Reward < 0



	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode = T1.StatementCode AND EI.MarketLetterCode = T1.MarketLetterCode 
			AND EI.FieldCode = 
			CASE BrandSegment
				WHEN 'Liberty' THEN 'Radius_Percentage_Liberty'
				WHEN 'Centurion' THEN 'Radius_Percentage_Centurion'
				ELSE 'Radius_Percentage_CPP'
			END
		WHERE [Reward] > 0
	END

	-- Copy data from temporary table to permanent table
	DELETE FROM RP2022_DT_Rewards_W_LogisticsSupport WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2022_DT_Rewards_W_LogisticsSupport(StatementCode, MarketLetterCode, ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales,Pog_Plan, Segment_Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE AS ProgramCode, BrandSegment, Segment_Reward_Sales,LY_Sales,Pog_Plan, Segment_Reward_Percentage, Reward
	FROM @TEMP
END


