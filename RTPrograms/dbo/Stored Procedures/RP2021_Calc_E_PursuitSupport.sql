﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_PursuitSupport] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON
	/* PLEASE NOTE THIS REWARD CALUCALTION IS ACTUALLY LOYALTY SUPPORT BONUS 
	   PROGRAMCODE : RP2021_E_PURSUIT_REWARD
	   REWARDCODE : PURS_SUP_E
	*/
	/*
	DECLARE @SEASON INT = 2021
	DECLARE @STATEMENT_TYPE = 'ACTUAL'
	DECLARE @PROGRAM_CODE = 'RP2021_E_PURSUIT_REWARD'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	*/

	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @TEMP TABLE (
		[StatementCode] INT NOT NULL,
		[DataSetCode] INT NOT NULL,
		[MarketLetterCode] VARCHAR(50) NOT NULL,
		[ProgramCode] VARCHAR(50) NOT NULL,
		[RewardBrand] VARCHAR(100) NOT NULL,
		[RewardBrand_Qty] NUMERIC(20,4) NOT NULL,
		[QualBrand_Sales_LY] MONEY NOT NULL,
		[QualBrand_Sales_CY] MONEY NOT NULL,
		[Growth] NUMERIC(20,4) NOT NULL DEFAULT 0,
		[RewardPerCase] MONEY NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		[DataSetCode] INT NOT NULL,
		[MarketLetterCode] VARCHAR(50) NOT NULL,
		[ProgramCode] VARCHAR(50) NOT NULL,
		[RewardBrand] VARCHAR(100) NOT NULL,
		[QualBrand_Sales_LY] MONEY NOT NULL,
		[QualBrand_Sales_CY] MONEY NOT NULL,
		[Growth] NUMERIC(19,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[DataSetCode] ASC,
			[MarketLetterCode] ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		-- Insert data from sales consolidated into table
		INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Qty, QualBrand_Sales_LY, QualBrand_Sales_CY)
		SELECT ST.StatementCode, RP.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup,
			   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Acres_CY / PR.ConversionE) * (EI.FieldValue / 100) ELSE SUM(TX.Acres_CY / PR.ConversionE) END [RewardBrand_Qty],
			   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Price_LY1) * (EI.FieldValue / 100) ELSE SUM(TX.Price_LY1) END [QualBrand_Sales_LY],
			   CASE WHEN @STATEMENT_TYPE = 'FORECAST' THEN SUM(TX.Price_CY) * (EI.FieldValue / 100) ELSE SUM(TX.Price_CY) END [QualBrand_Sales_CY]
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON ST.StatementCode = TX.StatementCode
			INNER JOIN RP_Statements RP
			ON ST.StatementCode = RP.StatementCode
			INNER JOIN ProductReference PR
			ON TX.ProductCode = PR.ProductCode
			INNER JOIN RPWeb_USER_EI_FieldValues EI
			ON ST.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
		WHERE RP.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
		GROUP BY ST.StatementCode, RP.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup, EI.FieldValue
	END --END FORECAST PROJECTION

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN

		INSERT INTO @TEMP(StatementCode, DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Qty, QualBrand_Sales_LY, QualBrand_Sales_CY)
		SELECT ST.StatementCode, RP.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup,
			   SUM(TX.Acres_CY / PR.ConversionE)  [RewardBrand_Qty],
			   SUM(TX.Price_LY1)  [QualBrand_Sales_LY],
			   SUM(TX.Price_CY)  [QualBrand_Sales_CY]
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX ON ST.StatementCode = TX.StatementCode
			INNER JOIN RP_Statements RP ON ST.StatementCode = RP.StatementCode
			INNER JOIN ProductReference PR ON TX.ProductCode = PR.ProductCode
		WHERE RP.StatementLevel = 'LEVEL 1' AND TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
		GROUP BY ST.StatementCode, RP.DataSetCode, TX.MarketLetterCode, TX.ProgramCode, PR.ChemicalGroup



	END

	-- Determine qualification based on sum of sales at the OU (level 2) level
	INSERT INTO @TEMP_TOTALS(DataSetCode, MarketLetterCode, ProgramCode, RewardBrand, QualBrand_Sales_LY, QualBrand_Sales_CY)
	SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END [DataSetCode], MarketLetterCode, ProgramCode, RewardBrand,
		   SUM(QualBrand_Sales_LY) [QualBrand_Sales_LY],
		   SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY]
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode, RewardBrand


	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)= dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	
	-- Calculate qualification at the OU level
	UPDATE @TEMP_TOTALS
	SET Growth = CASE WHEN QualBrand_Sales_LY = 0 THEN CASE WHEN QualBrand_Sales_CY > 0 THEN 99.999 ELSE 0 END ELSE (QualBrand_Sales_CY / QualBrand_Sales_LY) + @ROUNDUP_VALUE END

	-- Copy the growth (qualification) from the OU level to the Location (level 1) level
	UPDATE T1
	SET Growth = T2.Growth
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2
		ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
	WHERE T1.DataSetCode > 0

	UPDATE T1
	SET Growth = T2.Growth
	FROM @TEMP T1
		INNER JOIN @TEMP_TOTALS T2
		ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode AND T1.ProgramCode = T2.ProgramCode AND T1.RewardBrand = T2.RewardBrand
	WHERE T1.DataSetCode = 0

	-- Determine the reward per case for each brand
	UPDATE T1
	SET RewardPerCase =
	CASE
		WHEN T1.RewardBrand = 'PURSUIT' THEN 75.00
		WHEN T1.RewardBrand = 'FRONTIER' THEN 40.00
		WHEN T1.RewardBrand = 'HEADLINE' THEN 108.00
		WHEN T1.RewardBrand = 'PRIAXOR' THEN 160.00
		WHEN T1.RewardBrand = 'CONQUEST LQ' THEN 40.00
		WHEN T1.RewardBrand = 'FORUM' THEN 105.00
		ELSE 0
	END
	FROM @TEMP T1
		INNER JOIN RP_Statements RS
		ON T1.StatementCode = RS.StatementCode
		INNER JOIN RetailerProfile RP
		ON RS.RetailerCode = RP.RetailerCode

	-- Determine the total reward per brand (rounded to nearest dollar to avoid confusion over rounding errors)
	UPDATE @TEMP
	SET Reward = ROUND(RewardBrand_Qty * RewardPerCase, 0) WHERE Growth + @ROUND_PERCENT >= 1.00 AND RewardBrand_Qty > 0 

	-- Insert from temp table into DT table
	DELETE FROM RP2021_DT_Rewards_E_PursuitSupport WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_E_PursuitSupport(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Qty, QualBrand_Sales_LY, QualBrand_Sales_CY, RewardPerCase, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Qty, QualBrand_Sales_LY, QualBrand_Sales_CY, RewardPerCase, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode, MarketLetterCode, ProgramCode, RewardBrand,
		   SUM(RewardBrand_Qty) [RewardBrand_Qty],
		   SUM(QualBrand_Sales_LY) [QualBrand_Sales_LY],
		   SUM(QualBrand_Sales_CY) [QualBrand_Sales_CY],
		   MAX(RewardPerCase) [RewardPerCase],
		   SUM(Reward) [Reward]
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode, MarketLetterCode, ProgramCode, RewardBrand
END
