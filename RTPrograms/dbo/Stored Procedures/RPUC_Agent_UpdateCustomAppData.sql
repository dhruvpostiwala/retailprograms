﻿

CREATE PROCEDURE [dbo].[RPUC_Agent_UpdateCustomAppData] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	
	BEGIN TRY

		IF OBJECT_ID('tempdb..#Records_To_Delete') IS NOT NULL DROP TABLE #Records_To_Delete
		IF OBJECT_ID('tempdb..#Records_To_Delete') IS NOT NULL DROP TABLE #Records_To_Delete
		IF OBJECT_ID('tempdb..#AllRecords_To_Insert') IS NOT NULL DROP TABLE #AllRecords_To_Insert

		SELECT @SEASON=Season, @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode
		--SET @JSON='{"created":[{"RP_ID":"","retailercode":"R400011638","farmcode":"F0059754","inradius":"No","invoicedate":"2020-04-22T16:26:18.460Z","product":{"productcode":"PRD-CIRD-B24KLL","productname":"INVIGOR L255PC PROSPER 22.7 KG BAG"},"productname":"","quantity":100}],"updated":[],"destroyed":[]}'

		-- LETS GET StatementTransNumber to be deleted 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @StatementCode AS Statementcode, t1.RP_ID
		INTO #Records_To_Delete
		FROM (
			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b	

				UNION ALL

			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b		
		) T1			


		-- ALL RECORDS TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode ,t1.*
		INTO #Records_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (												
							retailercode varchar(20) N'$.retailercode',
							productcode varchar(65) N'$.product.productcode',
							quantity numeric(18,2) N'$.quantity'
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								productcode varchar(65) N'$.product.productcode',
								quantity numeric(18,2) N'$.quantity'
													
							) as b
		) T1
	

		DELETE T1		
		FROM RP_DT_CustomAppProducts T1
			INNER JOIN #Records_To_Delete T2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.RP_ID=T2.RP_ID
		
		
		/*  
			
			PRIMARY KEY:
			[Statementcode] ASC,
			[RP_ID] ASC,
			[DocCode] ASC,
			[Type] ASC,
			[Product] ASC,
			[Productcode] ASC

			LETS REGENERATE VALUES FOR '[RP_ID]' 
		
			STEP #1: COLLECT DATA FROM ACTUAL TABLE INTO TEMPORARY TABLE. SINCE WE ARE GOING TO UPDATE '[RP_ID]' COLUMN, NO NEED TO FETCH IT 				
			STEP #2: DELETE RECORDS FROM ACTUAL TABLE WHERE STATEMENTCODE=@STATEMENTCODE
			STEP #3: INSERT RECORDS BACK INTO ACTUAL TABLE FROM TEMPORARY TABLE. USE 'ROW_NUMBER()' FUNCTION TO INSERT VALUE INTO '[RP_ID]' COLUMN

		*/

		-- STEP #1: Collect data into temporary table #ALLSales_To_Insert
		SELECT StatementCode,DataSetCode,Season,RetailerCode,DocCode,Type,T1.ProductCode,Quantity,PR.ProductName,ISNULL(PackageSize,PR.ProductUOM) AS PackageSize
			,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode, T1.ProductCode) AS RP_ID
		INTO #AllRecords_To_Insert
		FROM (
			SELECT StatementCode,DataSetCode,Season,RetailerCode,DocCode,[Type],ProductCode,Quantity,PackageSize
			FROM RP_DT_CustomAppProducts
			WHERE StatementCode=@StatementCode
		
				UNION ALL

			SELECT Statementcode,@DataSetCode,@Season,Retailercode,'Use Case' AS Doccode,'Use Case' AS [Type],ProductCode,Quantity,NULL AS PackageSize
			FROM #Records_To_Insert
		) T1	
			INNER JOIN ProductReference PR
			ON PR.ProductCode=T1.ProductCode



		-- STEP #2: DELETE RECORDS FROM  RP_DT_TRANSACTIONS
		DELETE RP_DT_CustomAppProducts WHERE StatementCode=@StatementCode
		
		-- STEP #3: INSERT RECORDS FROM #AllSales_To_Insert INTO RP_DT_TRANSACTIONS 
		INSERT INTO RP_DT_CustomAppProducts(StatementCode,DataSetCode,RP_ID,Season,RetailerCode,DocCode,Type,ProductCode,Product,Quantity,PackageSize)
		SELECT StatementCode,DataSetCode,RP_ID,Season,RetailerCode,DocCode,Type,ProductCode,ProductName,Quantity,PackageSize
		FROM #AllRecords_To_Insert		
				
/*
		CUSTOM APP IS WEST ONLY PROGRAM
		IF @REGION='EAST' AND @DATASETCODE > 0 
		BEGIN
			DELETE RP_DT_CustomAppProducts WHERE StatementCode=@DataSetCode

			INSERT INTO RP_DT_CustomAppProducts (StatementCode,DataSetCode,Season,RetailerCode,DocCode,Type,ProductCode,Product,Quantity,RP_ID)
			SELECT @DataSetCode AS StatementCode, 0 AS DataSetCode,Season,RetailerCode,DocCode,Type,ProductCode,ProductName,Quantity
				,ROW_NUMBER() OVER(PARTITION BY @DataSetCode ORDER BY RetailerCode, ProductCode) AS RP_ID
			FROM RP_DT_CustomAppProducts
			WHERE DataSetCode=@DataSetCode
		END
*/		
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

