﻿
CREATE PROCEDURE [dbo].[RPUC_Agent_UpdatePlanTransactions] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;

	BEGIN TRY

		SELECT @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode

		IF OBJECT_ID('tempdb..#Transactions_To_Delete') IS NOT NULL DROP TABLE #Transactions_To_Delete
		IF OBJECT_ID('tempdb..#Transactions_To_Insert') IS NOT NULL DROP TABLE #Transactions_To_Insert
		IF OBJECT_ID('tempdb..#AllTransactions_To_Insert') IS NOT NULL DROP TABLE #AllTransactions_To_Insert

		--SET @JSON='{"created":[{"scenariocode":"","retailercode":"R0001154","modifieddate":"2020-04-20T21:07:23.707Z","product":{"productcode":"PRD-CIRD-B24KLL","productname":"INVIGOR L255PC PROSPER 22.7 KG BAG"},"productname":"","quantity":90,"acres":900,"srp":67635,"sdp":61560}],"updated":[{"scenariocode":"2","retailercode":"R0000839","modifieddate":"2020-04-20T04:00:00.000Z","product":{"productcode":"PRD-MEVY-7CD2Y2","productname":"8N270 CLDM COMMITMENT FEE 8.25 ACRE BAG","conversion_w":8.25,"conversion_e":8.25,"srp_w":58,"srp_e":58,"sdp_w":58,"sdp_e":58},"productname":"8N270 CLDM COMMITMENT FEE 8.25 ACRE BAG","quantity":500,"acres":4125,"srp":29000,"sdp":29000,"_product":{"productcode":"PRD-MMAN-7YZR4Q","productname":"CLL CDC DAZIL (IBC-289) - SMALL RED ACRES"}}],"destroyed":[{"scenariocode":"2","retailercode":"R0000839","modifieddate":"2020-04-20T04:00:00.000Z","product":{"productcode":"000000000059011338","productname":"ABSOLUTE 40 ACRE CASE"},"productname":"ABSOLUTE 40 ACRE CASE","quantity":200,"acres":8000,"srp":200740,"sdp":200740}]}'

		-- LETS GET ProductCode to be deleted 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @STATEMENTCODE AS Statementcode, t1.RP_ID
		INTO #Transactions_To_Delete
		FROM (
			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						RP_ID INT N'$.rp_id'
					) as b
				UNION ALL

			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b		
		) T1			

		-- ALL Transactions TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode, t1.*
		INTO #Transactions_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (		
							retailercode varchar(20) N'$.retailercode',
							productcode varchar(65) N'$.product.productcode',
							quantity numeric(18,2) N'$.quantity',
							acres numeric(18,2) N'$.acres',
							srp money N'$.srp',
							sdp money N'$.sdp',
							swp money N'$.swp',
							modifiedDate Date N'$.modifieddate'
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								productcode varchar(65) N'$.product.productcode',
								quantity numeric(18,2) N'$.quantity',
								acres numeric(18,2) N'$.acres',
								srp money N'$.srp',
								sdp money N'$.sdp',
								swp money N'$.swp',
								modifiedDate Date N'$.modifieddate'							
							) as b
		) T1


		/*DELETE T1		
		FROM RP_DT_ScenarioTransactions T1
			INNER JOIN #Transactions_To_Delete T2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.RP_ID=T2.RP_ID*/

		DELETE RP_DT_ScenarioTransactions
		FROM RP_DT_ScenarioTransactions t1
		INNER JOIN #Transactions_To_Delete t2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.RP_ID=T2.RP_ID

		--select * from RP_DT_ScenarioTransactions where statementCode = @StatementCode

		/*  
			PRIMARY KEY:
			[StatementCode] ASC,
			[productcode] ASC		
			*/

		SELECT StatementCode,ScenarioCode,ProductCode,HybridCode,DataSetCode,RetailerCode,ChemicalGroup,Quantity,Acres,Price_SRP,Price_SDP,Price_SWP,UC_DateModified,
		ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode, ProductCode) AS RP_ID
		INTO #AllTransactions_To_Insert
		FROM (
			SELECT StatementCode,ScenarioCode,ProductCode,HybridCode,DataSetCode,RetailerCode,ChemicalGroup,Quantity,Acres,Price_SRP,Price_SDP,Price_SWP,UC_DateModified
			FROM RP_DT_ScenarioTransactions
			WHERE StatementCode=@StatementCode
		
			UNION ALL

			SELECT Statementcode 
				,'Use Case' AS ScenarioCode
				,tx.productcode AS ProductCode
				,'' AS HybridCode
				,@DataSetCode AS DataSetCode
				,tx.retailerCode AS RetailerCode
				,PR.ChemicalGroup AS ChemicalGroup
				,tx.quantity as Quantity
				,tx.acres as Acres
				,tx.srp AS Price_SRP
				,tx.sdp AS Price_SDP
				,tx.swp AS Price_SWP
				,tx.modifiedDate AS UC_DateModified
			FROM #Transactions_To_Insert tx
				INNER JOIN ProductReference PR
				ON PR.ProductCode=tx.ProductCode
		) T1	

		DELETE RP_DT_ScenarioTransactions where statementCode = @statementCode
		
		INSERT INTO RP_DT_ScenarioTransactions (StatementCode, DataSetCode, RP_ID, ScenarioCode, ProductCode, HybridCode, RetailerCode, ChemicalGroup, Quantity, Acres, Price_SRP, Price_SDP,Price_SWP, UC_DateModified)
		SELECT StatementCode, DataSetCode, RP_ID
			,ScenarioCode,ProductCode,HybridCode,RetailerCode,ChemicalGroup,Quantity,Acres,Price_SRP,Price_SDP,Price_SWP,UC_DateModified	
		FROM #AllTransactions_To_Insert 
	
		

		IF @REGION='EAST' AND @DATASETCODE > 0 
		BEGIN
			DELETE RP_DT_ScenarioTransactions WHERE StatementCode=@DataSetCode

			INSERT INTO RP_DT_ScenarioTransactions (StatementCode,DataSetCode,ScenarioCode, ProductCode, HybridCode, RetailerCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, UC_DateModified,RP_ID)
			SELECT @DataSetCode AS StatementCode, 0 AS DataSetCode
					,ScenarioCode,ProductCode,HybridCode,RetailerCode,ChemicalGroup,Quantity,Acres,Price_SRP,Price_SDP,Price_SWP,UC_DateModified
					,ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode, ProductCode) AS RP_ID				
			FROM RP_DT_ScenarioTransactions
			WHERE DataSetCode=@DataSetCode
		END

	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

/****** Object:  StoredProcedure [Reports].[RP2020Web_Get_W_EffRebate]    Script Date: 2020-05-19 12:57:37 PM ******/
SET ANSI_NULLS ON
