﻿
CREATE PROCEDURE [dbo].[RP_RefreshEstimate_W_HeatDistinctAmounts] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
		This code is applicable for following seasons
		2021
		2020	
	*/


	/*
		A : Heat Pre-Harvest POG Prjection - Heat PreHarvest Eligible POG = Estimated Heat Pre-Harvest Top Up POG
		B : Distinct POG Prjection - Distinct PreHarvest Eligible POG = Estimated Distinct Top Up POG
		C: (A + B) * Market Letter Margin %
			(Estimated Heat Pre-Harvest Top Up POG + Estimated Distinct Top Up POG) X (CCP Market Letter Margin %) 
		The adjustment can not be negative. 
	*/

	/*
		WE WILL BE PASSING IN UNLOCKED STATEMENT CODES
		SINCE WE DO NOT MAINTAIN STATEMENT MAPPINGS ON LOCKED STATEMENTS
		USE UNLOCKED STATEMENT CODE TO GET INCENTIVE AMOUNTS 	
	*/

	
	DECLARE @SQL_COMMAND NVARCHAR(MAX)
	DECLARE @MARKETLETTERCODE VARCHAR(50)
	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4));

	IF @SEASON=2021		
		SET @MARKETLETTERCODE ='ML' + @SEASON_STRING + '_W_CPP'
	ELSE IF @SEASON=2020
		SET @MARKETLETTERCODE ='ML' + @SEASON_STRING + '_W_CER_PUL_SB'

	SET @SQL_COMMAND = '
	-- Declare temporary tables and variables
		
	DECLARE @ESTIMATES TABLE(
		Statementcode int NOT NULL,
		Prj_StatementCode int NOT NULL DEFAULT 0,
		Locked_Statementcode INT NOT NULL DEFAULT 0,
		Heat_Prj_POG MONEY NOT NULL DEFAULT 0,
		Heat_Act_POG MONEY NOT NULL DEFAULT 0,
		Distinct_Prj_POG MONEY NOT NULL DEFAULT 0,
		Distinct_Act_POG MONEY NOT NULL DEFAULT 0,
		ML_Eligible_POG MONEY NOT NULL DEFAULT 0,
		ML_Total_Reward MONEY NOT NULL DEFAULT 0,
		Margin DECIMAL(18,4) NOT NULL DEFAULT 0,	
		Heat_Estimate MONEY NOT NULL DEFAULT 0,
		Distinct_Estimate MONEY NOT NULL DEFAULT 0,
		Amount MONEY NOT NULL DEFAULT 0
	)

	DECLARE @Products TABLE (
		ProductCode VARCHAR(65) NOT NULL,
		ChemicalGroup VARCHAR(100) NOT NULL
	)
		  	 
	INSERT INTO @ESTIMATES(Statementcode, Locked_Statementcode)
	SELECT UL.StatementCode, ISNULL(LCK.Statementcode,0) AS Locked_Statementcode
	FROM @STATEMENTS UL
		INNER JOIN RPWeb_Statements LCK
		ON LCK.SRC_Statementcode=UL.Statementcode AND LCK.VersionType=''Locked'' AND LCK.[Status]=''Active'' 
		INNER JOIN RP_Config_ChequeRuns CR
		ON CR.ID = LCK.ChequeRunID
	WHERE CR.ChequeRunName=''OCT RECON''

	IF CAST(GETDATE() AS DATE) >  CAST(''' + @SEASON_STRING + '-09-03'' AS DATE) 
	BEGIN

		UPDATE T1
		SET Statementcode=T2.Locked_Statementcode		
		FROM RPWeb_Est_HeatDistinctTopUp T1
			INNER JOIN @ESTIMATES T2	
			ON T2.Statementcode=T1.SRC_StatementCode
		WHERE T1.Statementcode <> T2.Locked_Statementcode
		
		RETURN
	END
	
	-- DELETE STATEMENTS WHICH ARE ALREADY Submitted For RC Reconciliation
	-- WE SHOULD NOT BE REFRESHING THESE STATEMENTS
	DELETE T1
	FROM @ESTIMATES T1
		INNER JOIN RPWeb_StatementInformation T2
		ON T2.StatementCode=T1.StatementCode AND T2.FieldName=''Status''
	WHERE T2.FieldValue = ''Submitted For RC Reconciliation''


	-- DELETE INACTIVE LOCKED STATEMENT ENTRIES
	DELETE T1
	FROM RPWeb_Est_HeatDistinctTopUp T1		
		INNER JOIN RPWeb_Statements LCK		ON LCK.StatementCode=T1.StatementCode
		INNER JOIN @STATEMENTS UL			ON UL.StatementCode=LCK.SRC_Statementcode		
	WHERE  LCK.[Status] IN (''Delete'',''Deleted'')
		
	INSERT INTO @Products(ProductCode, ChemicalGroup)
	SELECT Productcode,ChemicalGroup  FROM  ProductReference WHERE DocType=10 AND  ChemicalGroup IN (''HEAT'', ''HEAT LQ'') AND Product LIKE ''%PRE-HARVEST%''
		UNION
	SELECT Productcode, ChemicalGroup  FROM  ProductReference WHERE DocType=10 AND  ChemicalGroup=''Distinct''

	
	-- LETS GET PROJECTION STATEMENT CODE	
	UPDATE T1
	SET Prj_StatementCode=T2.Prj_StatementCode
	FROM @ESTIMATES T1
		INNER JOIN (
			SELECT ACT.StatementCode, PRJ.StatementCode [Prj_StatementCode]
			FROM @Statements ST
				INNER JOIN RPWeb_Statements ACT
				ON ACT.Statementcode=ST.Statementcode
				INNER JOIN (
						SELECT Statementcode, StatementLevel, RetailerCode, Season, VersionType
						FROM RPWeb_Statements 
						WHERE Season=@SEASON AND StatementType=''Projection'' AND VersionType=''Unlocked'' AND [Status]=''Active''
				) PRJ
				ON ACT.StatementLevel=PRJ.StatementLevel AND ACT.RetailerCode=PRJ.RetailerCode AND ACT.Season=PRJ.Season AND ACT.VersionType=PRJ.VersionType
			WHERE ACT.Season=@SEASON AND ACT.StatementType=''Actual'' AND ACT.VersionType=''Unlocked'' AND ACT.[Status]=''Active''						
		) T2
		ON T2.Statementcode=T1.Statementcode
	
	
	-- LETS  COLLECT POG SALES VALUES
	
	DROP TABLE IF EXISTS #POG_Sales

	SELECT st.Statementcode
		,SUM(IIF(PR.ChemicalGroup LIKE ''HEAT%'',TX.Price_CY,0)) AS Heat_POG
		,SUM(IIF(PR.ChemicalGroup=''DISTINCT'',TX.Price_CY,0)) AS Distinct_POG
	INTO #POG_Sales
	FROM (
			SELECT Statementcode From @ESTIMATES
				UNION
			SELECT Prj_StatementCode From @ESTIMATES
		) ST
		INNER JOIN RP' + @SEASON_STRING + 'Web_Sales_Consolidated tx	ON TX.Statementcode=ST.Statementcode
		INNER JOIN @Products PR						ON PR.Productcode=tx.ProductCode
	WHERE tx.ProgramCode=''ELIGIBLE_SUMMARY'' AND tx.GroupType=''ELIGIBLE_BRANDS''
	GROUP BY st.StatementCode 
	
	-- LETS UPDATE POG SALES VALUES
	UPDATE T1
	SET Heat_Act_POG=ISNULL(ACT.Heat_POG,0)
		,Distinct_Act_POG=ISNULL(ACT.Distinct_POG,0)
		,Heat_Prj_POG=ISNULL(PRJ.Heat_POG,0)
		,Distinct_Prj_POG=ISNULL(PRJ.Distinct_POG,0)
	FROM @ESTIMATES T1
		LEFT JOIN #POG_Sales ACT	ON ACT.Statementcode=T1.Statementcode
		LEFT JOIN #POG_Sales PRJ	ON PRJ.Statementcode=T1.PRJ_StatementCode
	
	-- Market Letter Level Sales and Rewards
	UPDATE T1
	SET ML_Eligible_POG=ISNULL(POG.Amount,0)
		,ML_Total_Reward=ISNULL(REW.Amount,0)
	FROM @ESTIMATES T1 
		LEFT JOIN (
			SELECT st.StatementCode, SUM(tx.Price_CY) [Amount]
			FROM @ESTIMATES ST 
				INNER JOIN RP' + @SEASON_STRING + 'Web_Sales_Consolidated  tx	ON tx.Statementcode=st.Statementcode
			WHERE tx.MarketLetterCode = @MARKETLETTERCODE AND tx.ProgramCode=''ELIGIBLE_SUMMARY'' AND tx.GroupType=''ELIGIBLE_BRANDS''
			GROUP BY st.StatementCode
		) POG
		ON POG.StatementCode=T1.Statementcode
		LEFT JOIN (
			SELECT ST.StatementCode, SUM(T1.RewardValue) [Amount]
			FROM @ESTIMATES st
				INNER JOIN RPWeb_Rewards_Summary T1			ON T1.Statementcode=ST.Statementcode
				INNER JOIN RP_Config_Rewards_Summary CFG	ON CFG.Rewardcode=T1.RewardCode AND CFG.Season=@SEASON
			WHERE CFG.RewardType=''Regular'' AND MarketLetterCode = @MARKETLETTERCODE  
			GROUP BY ST.StatementCode			
		) REW
		ON REW.Statementcode=T1.Statementcode 

	UPDATE @ESTIMATES  SET [Margin]=ML_Total_Reward/ML_Eligible_POG WHERE ML_Eligible_POG <> 0

	UPDATE @ESTIMATES 	
	SET [Heat_Estimate] = Heat_Prj_POG - Heat_Act_POG
	WHERE Heat_Prj_POG > Heat_Act_POG

	UPDATE @ESTIMATES 	
	SET [Distinct_Estimate] = Distinct_Prj_POG - Distinct_Act_POG
	WHERE Distinct_Prj_POG > Distinct_Act_POG

	UPDATE @ESTIMATES 	SET [Amount] = ([Heat_Estimate]+[Distinct_Estimate]) * [Margin]
	UPDATE @ESTIMATES 	SET [Amount] = 0 WHERE [Amount] < 0


	DELETE T1
	FROM RPWeb_Est_HeatDistinctTopUp T1		
		INNER JOIN @ESTIMATES T2
		ON T2.StatementCode = T1.SRC_Statementcode

	-- LOCKED STATEMENT CODES
	INSERT INTO RPWeb_Est_HeatDistinctTopUp (SRC_Statementcode,Statementcode,Prj_StatementCode,Heat_Prj_POG ,Heat_Act_POG,Distinct_Prj_POG,Distinct_Act_POG,ML_Eligible_POG,ML_Total_Reward,Margin,Heat_Estimate,Distinct_Estimate,Amount)
	SELECT Statementcode, Locked_Statementcode,Prj_StatementCode, Heat_Prj_POG ,Heat_Act_POG,Distinct_Prj_POG,Distinct_Act_POG,ML_Eligible_POG,ML_Total_Reward,Margin,Heat_Estimate,Distinct_Estimate,Amount
	FROM @ESTIMATES
	'



	--PRINT @SQL_COMMAND
	--RETURN
	EXEC SP_EXECUTESQL @SQL_COMMAND, N'@SEASON INT, @STATEMENTS UDT_RP_STATEMENT READONLY, @MARKETLETTERCODE VARCHAR(50)',  @SEASON, @STATEMENTS, @MARKETLETTERCODE
	

END
