﻿


CREATE PROCEDURE [dbo].[RPCheque_Agent_UpdateStatus] @SEASON INT,  @USERNAME VARCHAR(150), @NewStatus VARCHAR(50), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @STATEMENTS AS TABLE (Statementcode INT);

	INSERT INTO @STATEMENTS(STATEMENTCODE)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')

	UPDATE T1
	SET FieldValue=@NewStatus
	FROM RPWeb_StatementInformation T1
		INNER JOIN @STATEMENTS T2		ON T2.StatementCode=T1.StatementCode
	WHERE T1.FieldName='CreateChequeStatus' AND T1.FieldValue='Validation Passed'
	
END




