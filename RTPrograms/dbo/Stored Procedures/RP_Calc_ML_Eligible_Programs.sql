﻿CREATE PROCEDURE [dbo].[RP_Calc_ML_Eligible_Programs] @SEASON INT,  @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DROP TABLE IF EXISTS #TEMP_MarketLetters
	SELECT Season, Region, MarketLetterCode, IncludeInForecast , IncludeInActual, IncludeInProjection 
	INTO #TEMP_MarketLetters
	FROM RP_Config_MarketLetters 
	WHERE Season=@SEASON 

	IF @STATEMENT_TYPE='FORECAST'
		DELETE FROM #TEMP_MarketLetters WHERE IncludeInForecast <> 'Yes'
	ELSE IF @STATEMENT_TYPE='ACTUAL'		
		DELETE FROM #TEMP_MarketLetters WHERE IncludeInActual <> 'Yes'						
	ELSE IF @STATEMENT_TYPE='PROJECTION'
		DELETE FROM #TEMP_MarketLetters WHERE IncludeInProjection <> 'Yes'	
	ELSE IF @STATEMENT_TYPE='REWARD PLANNER'
		DELETE FROM #TEMP_MarketLetters WHERE IncludeInForecast <> 'Yes'
	ELSE 
		DELETE FROM #TEMP_MarketLetters 

	/****************************************************************************************************/
	DELETE FROM RP_DT_ML_ElG_Programs WHERE StatementCode IN (Select StatementCode From @STATEMENTS)

	INSERT INTO RP_DT_ML_ElG_Programs(StatementCode, DataSetCode, DataSetCode_L5, MarketLetterCode, ProgramCode)
	SELECT DISTINCT ST.StatementCode, RS.DataSetCode, RS.DataSetCode_L5, ML.MarketLetterCode, MLP.ProgramCode	
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS					ON RS.StatementCode=ST.StatementCode AND RS.Season=@SEASON AND RS.[Status]='Active'
		INNER JOIN RP_StatementsMappings MAP		ON MAP.StatementCode=RS.StatementCode
		INNER JOIN #TEMP_MarketLetters ML			ON ML.Season=RS.Season AND ML.Region=RS.Region 
		INNER JOIN RP_Config_ML_AccountTypes MLAT	ON MLAT.MarketLetterCode=ML.MarketLetterCode
		INNER JOIN RetailerProfileAccountType RPAT	ON RPAT.AccountType=MLAT.AccountType AND RPAT.RetailerCode=MAP.RetailerCode 
		INNER JOIN RP_Config_ML_Programs MLP		ON MLP.MarketLetterCode=ML.MarketLetterCode 
		INNER JOIN RP_Config_Programs PRG			ON PRG.ProgramCode=MLP.ProgramCode
		--INNER JOIN RetailerProfile RP				ON RP.RetailerCode=RS.RetailerCode 	-- AND RP.Level5Code=MLP.Level5Code
	WHERE PRG.Status='Active' AND (RS.Region='EAST' OR RS.Level5Code=MLP.Level5Code)  

	
	-- https://kennahome.jira.com/browse/RC-3654
	-- REGION : EAST, LEVEL 1 SHOULD INHERIT FROM LEVEL 2 WHICH ARE NON EXISTING
	INSERT INTO RP_DT_ML_ElG_Programs(StatementCode, MarketLetterCode, ProgramCode)
	SELECT DISTINCT ST.StatementCode, MLP2.MarketLetterCode, MLP2.ProgramCode	
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_ML_ElG_Programs MLP2	ON MLP2.StatementCode=RS.DataSetCode
		LEFT JOIN  RP_DT_ML_ElG_Programs MLP1	ON MLP1.StatementCode=RS.StatementCode AND MLP1.MarketLetterCode=MLP2.MarketLetterCode AND MLP1.ProgramCode=MLP2.ProgramCode
	WHERE RS.Region='EAST' AND RS.StatementLevel='LEVEL 1' AND RS.DataSetCode > 0 AND RS.[Status]='Active'  
		AND ISNULL(MLP1.MarketLetterCode,'')=''

END
