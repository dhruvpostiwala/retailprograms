﻿
CREATE PROCEDURE [dbo].[RPST_Agent_UpdateStatementApprover] @SEASON INT,  @USERNAME VARCHAR(150), @ACTION VARCHAR(10), @APPROVER VARCHAR(100), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @STATEMENTS UDT_RP_STATEMENT;
	DECLARE @TARGET_STATEMENTS UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @FIELDNAME VARCHAR(50);
	
	BEGIN TRY

	-- PASSED IN STATEMENT CODES COULD BE ULOCKED OR LOCKED
	INSERT INTO @STATEMENTS(StatementCode)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')

	-- ALL STATEMENTS CODES TO BE INCLUDED FROM A FAMILY
	INSERT INTO @TARGET_STATEMENTS(StatementCode, StatementLevel, DataSetCode)
	SELECT StatementCode,StatementLevel,DataSetCode 
	FROM TVF_Get_StatementCodesToRefresh(@STATEMENTS) 

	SET @FIELDNAME='STMT_QA_ASSIGNEE'

	IF @ACTION='ASSIGN'
		MERGE RPWeb_StatementInformation TGT
		USING (
			SELECT COALESCE(LCK.SRC_StatementCode, TS.StatementCode) AS StatementCode, @FIELDNAME AS FieldName, @APPROVER AS FieldValue	
			FROM @TARGET_STATEMENTS	TS				
				LEFT JOIN RPWeb_Statements LCK 		
				ON LCK.StatementCode=TS.StatementCode AND LCK.StatementType='Actual' AND LCK.VersionType='Locked' AND LCK.Status='Active'
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND TGT.FieldName=SRC.FieldName 
		WHEN MATCHED AND TGT.FieldValue <> SRC.FieldValue THEN
			UPDATE SET TGT.FieldValue=SRC.FieldValue
		WHEN NOT MATCHED THEN
			INSERT (Statementcode, FieldName, FieldValue, FieldCategory)
			VALUES (SRC.StatementCode, SRC.FieldName, SRC.FieldValue, 'Assignment')
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA Assignment','Field',@FIELDNAME,deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue]);

	IF @ACTION='UNASSIGN'
		DELETE STI
		OUTPUT deleted.[StatementCode],deleted.[StatementCode],'QA Assignment','Field',@FIELDNAME,deleted.[FieldValue],''
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI
			INNER JOIN (
				SELECT COALESCE(LCK.SRC_StatementCode, TS.StatementCode) AS StatementCode
				FROM @TARGET_STATEMENTS	TS				
					LEFT JOIN RPWeb_Statements LCK 		
					ON LCK.StatementCode=TS.StatementCode AND LCK.StatementType='Actual' AND LCK.VersionType='Locked' AND LCK.Status='Active'
			) TGT
			ON TGT.StatementCode=STI.StatementCode
		WHERE STI.FieldCategory='Assignment' AND STI.FieldName=@FIELDNAME

	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message

END




