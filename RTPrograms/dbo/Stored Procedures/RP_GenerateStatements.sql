﻿
CREATE PROCEDURE [dbo].[RP_GenerateStatements] @SEASON INT, @STATEMENT_TYPE AS [VARCHAR](20)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SEASON_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-01' AS DATE)	-- OCT-01	
	DECLARE @SEASON_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)		-- SEP-30

	DECLARE @SOLD_TO_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-02' AS DATE)		
	DECLARE @SOLD_TO_END_DATE DATE =  CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)


	DECLARE @REGIONS TABLE(Region VARCHAR(4)) 
	DECLARE @EligibleRetailers AS UDT_RP_RETAILERS_LIST;
	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;

	DECLARE @REGION VARCHAR(4);
	DECLARE @USERNAME AS VARCHAR(100);
	DECLARE @PROCEDURE_NAME VARCHAR(200);

	-- DO NOT GENERATE STATEMENTS AFTER SEASON ENDS
	-- WE NEED TO MAINTAING RETAILER HIEREARCHY AS OF SEASON
	
	IF CAST(GETDATE() AS DATE) >  @SEASON_END_DATE  
		RETURN
		

	INSERT INTO @REGIONS(REGION) 
	VALUES('West')	,('East')

	DECLARE REGIONS_LOOP CURSOR FOR
		SELECT [Region]	FROM @REGIONS
	OPEN REGIONS_LOOP
	FETCH NEXT FROM REGIONS_LOOP INTO @REGION
	WHILE(@@FETCH_STATUS=0)
		BEGIN
			SET @USERNAME='Nightly Statement Generation Process - ' + @REGION
			SET @PROCEDURE_NAME='RP_Get_Elg_RetailersForStatement_' + @REGION
						
			/* ==================================== FIRST STEP =========================================================== */	
			-- EXEC ('EXEC '+ @PROCEDURE_NAME)

			INSERT @EligibleRetailers
			EXEC @PROCEDURE_NAME @SEASON, @STATEMENT_TYPE

			-- JIRA: RP-2356 Distributor SOLLIO AGRICULTURE S.E.C. (D0000111) purchased AGRONOMY GROUP (D0000193)
			-- AS PART OF HIERARCHY CHANGE AND ACCOMMODATE RP TO TREAT AGRONMY SIMILAR 2019, LEVE4CODE ON AGRONOMY RETATILERS IS BEING SET TO 'D520167841'

			/*				
				SELECT * FROM RETAILERPROFILE WHERE Level5Code='D520167841'
				SELECT * FROM RETAILERPROFILE WHERE Level4Code='D520167841'			
				SELECT * FROM RETAILERPROFILE WHERE RetailerCode='D520167841' 
			*/

			IF @STATEMENT_TYPE='ACTUAL' AND @SEASON=2020 AND @REGION='EAST'
				UPDATE T1 
				SET LEVEL5Code=RP.Level4Code				
				FROM @EligibleRetailers T1
					INNER JOIN RetailerProfile RP
					ON RP.RetailerCode=T1.RetailerCode
				WHERE RP.Level5Code='D0000111' AND RP.Level4Code='D520167841'

			-- RETAILER TYPE IS SAME. BUT LOCATION MOVED TO A DIFFERENT FAMILY					   
			-- CAPTURE LEVEL2CODE CHANGES
			UPDATE T1
			SET  [OldLevel2Code]=IIF(T1.Level2Code <> T2.Level2Code,T2.Level2Code,'')
				--,[OldLevel5Code]=IIF(T1.Level5Code <> T2.Level5Code,T2.Level5Code,'')
				,[HierarchyChanged]=1
			FROM @EligibleRetailers T1
				INNER JOIN (
					SELECT RetailerCode, RetailerType, Level2Code, Level5Code
					FROM RP_RetailerReference
					WHERE Season=@Season AND Region=@Region 
				) T2
				ON T2.RetailerCode=T1.RetailerCode AND T1.RetailerType=T2.RetailerType
			WHERE (T1.Level2Code <> T2.Level2Code OR T1.Level5Code <> T2.Level5Code)

			/*
				LETS HANDLE CHANGE IN LEVEL 2 CODE FOR EXISTING STATEMENTS
				PLEASE NOTE:  FOLLOWING LOGIC IS BEING APPLIED UNDER THE ASSUMPTION THAT ONLY ONE LEVEL2CODE IS MAINTAINED IN A FAMILY.
			*/
			DELETE FROM @EDIT_HISTORY
			
			UPDATE T1
			SET RetailerCode=T2.RetailerCode
				,Level2Code=T2.Level2Code
			OUTPUT inserted.[StatementCode], inserted.[StatementCode], 'Change in Retailercode and Level2Code.','Field','RetailerCode/Level2Code',deleted.[RetailerCode] + '/' + deleted.[Level2Code],inserted.[RetailerCode] + '/' + inserted.[Level2Code]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RP_Statements T1
				INNER JOIN @EligibleRetailers T2	
				ON T2.OldLevel2Code=T1.Level2Code AND T2.StatementLevel=T1.StatementLevel
			WHERE T1.Season=@Season AND T1.Region=@REGION AND T1.[StatementType]=@Statement_Type  AND T1.[VersionType] IN ('Unlocked','Final-Preferred') 
				AND T1.StatementLevel='LEVEL 2' AND T1.[Status]='Active' 
				AND T1.Level2Code <> T2.Level2Code
							
			IF @REGION='EAST'
				UPDATE T1
				SET Level2Code=T2.Level2Code
				OUTPUT inserted.[StatementCode], inserted.[StatementCode], 'Change in Level2Code' ,'Field','Level2Code',deleted.[Level2Code],inserted.[Level2Code]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RP_Statements T1
					INNER JOIN @EligibleRetailers T2
					ON T2.OldLevel2Code=T1.Level2Code AND T2.StatementLevel=T1.StatementLevel AND T2.RetailerCode=T1.RetailerCode
				WHERE T1.Season=@Season AND T1.Region=@REGION AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred') 
					AND T1.StatementLevel='LEVEL 1' AND T1.[Status]='Active'
					AND T1.Level2Code <> T2.Level2Code

			EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY	

			IF CAST(GETDATE() AS DATE) <=  @SEASON_END_DATE  
			BEGIN
				-- TAKE A SNAP SHOT OF RETAILER PROFILE DATA INTO RP_RETAILERREFERENCE TABLE 			
				DELETE FROM RP_RetailerReference WHERE Season=@Season AND Region=@Region
			
				INSERT INTO RP_RetailerReference(Season, Region, RetailerCode, RetailerType, Level2Code, Level5Code)
				SELECT @Season, @Region, RetailerCode, RetailerType, Level2Code, Level5Code
				FROM RetailerProfile 
				WHERE Region=@Region AND [Status] IN ('ACTIVE','INACTIVE')  

				INSERT INTO RP_RetailerReference(Season, Region, RetailerCode, RetailerType, Level2Code, Level5Code)
				SELECT @Season, @Region, RetailerCode, RetailerType, Level2Code, Level5Code
				FROM RetailerProfile 
				WHERE Region=@Region AND [Status]='SOLD' 
					AND SoldDate IS NOT NULL 
					AND CAST(SoldDate AS Date) >= @SOLD_TO_START_DATE  
					AND CAST(SoldDate AS Date) <= @SOLD_TO_END_DATE
			END

				
			/*
			-- JIRA: RP-2487 -- Aligning RP statements with RC for Recon
			IF @SEASON=2020 AND @REGION='WEST'
			BEGIN
				/*
					UPDATE RP_RetailerReference
					SET RetailerType='LEVEL 2'
					WHERE @SEASON=SEASON AND Region=@REGION AND RetailerCode='R520157196'
				*/

				-- Original Level2Code = 'R0001120'
				UPDATE RP_RetailerReference
				SET Level2Code='R520157196'  
				WHERE @SEASON=SEASON AND Region=@REGION AND RetailerCode = 'R520157196'
					--AND RetailerCode  IN ('R520157441','R520157200','R520157196')
			END 
			*/

			-- JIRA: RP-2356 Distributor SOLLIO AGRICULTURE S.E.C. (D0000111) purchased AGRONOMY GROUP (D0000193)
			-- AS PART OF HIERARCHY CHANGE AND TO ACCOMMODATE RP TO TREAT AGRONMY SIMILAR TO 2019, LEVEL4CODE ON AGRONOMY RETATILERS IS BEING SET TO 'D520167841'
			IF @STATEMENT_TYPE='ACTUAL' AND @SEASON=2020 AND @REGION='EAST'
				UPDATE T1 
				SET LEVEL5Code=RP.Level4Code			
				FROM RP_RetailerReference T1
					INNER JOIN RetailerProfile RP
					ON RP.RetailerCode=T1.RetailerCode
				WHERE T1.Season=@SEASON AND T1.Region=@Region AND RP.Level5Code='D0000111' AND RP.Level4Code='D520167841'			  			 		
			
			/* ==================================== FOR ACTUAL STATEMENTS : DEAL WITH SOLD RETAILERS  ==================================== */		

			IF @STATEMENT_TYPE = 'ACTUAL' AND EXISTS(SELECT * FROM @EligibleRetailers WHERE [Status]='SOLD')
			BEGIN						
				/* LETS WORK WITH SOLD TO RETAILERS */
				DROP TABLE IF EXISTS #ExistingRetailers
				SELECT StatementCode, RetailerCode, StatementLevel, VersionType, [Status]
				INTO #ExistingRetailers
				FROM RP_Statements
				WHERE [Season]=@Season AND [Region]=@Region AND [StatementType]=@Statement_Type AND [VersionType]='UNLOCKED' 
					AND [ActiveRetailerCode] != ''
							
				/* REPLACE RETAILERCODE ON EXISTING STATEMENTS WHERE STATEMENT.RETAILERCODE = ELIGIBLE.ACTIVERETAILERCODE AND STATEMENT LEVEL MATCHES*/		
				DELETE FROM @EDIT_HISTORY

				UPDATE T1
				SET [RetailerCode]=ELG.[RetailerCode] --DLR CODE
					,[ActiveRetailerCode]=ELG.[ActiveRetailerCode]	--RCODE / DLRCODE
				OUTPUT inserted.[StatementCode], inserted.[StatementCode], 'Retailercode was changed based on sold to information.','Field','Status',deleted.[RetailerCode],inserted.[RetailerCode]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RP_Statements T1
					INNER JOIN @EligibleRetailers ELG	ON ELG.ActiveRetailerCode=T1.RetailerCode AND T1.[Season]=@SEASON AND T1.StatementType=@Statement_Type
					LEFT JOIN #ExistingRetailers ER		ON ELG.RetailerCode=ER.RetailerCode AND T1.[Season]=@SEASON 
				WHERE ELG.[Status]='SOLD' AND T1.StatementType=@Statement_Type AND T1.[Season]=@Season AND T1.[Status]='Active' AND T1.[VersionType]='Unlocked' AND ER.RetailerCode IS NULL

				EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY	
				
				-- CREATE A NEW STATEMENT FOR SOLD RETAILERS IF NO STATEMENTCODE EXISTS NEITHER FOR RETAILERCODE NOR FOR ACTIVERRETAILERCODE
				DELETE FROM @EDIT_HISTORY

				INSERT INTO RP_Statements(Season, Region, StatementType, [Status], VersionType, StatementLevel, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5 )
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Statement Created for Sold Retailer'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
				SELECT @Season AS [Season], @Region AS [Region], @STATEMENT_TYPE AS [StatementType], 'Active' as [Status],'Unlocked' AS [VersionType]
					,T1.StatementLevel	,T1.RetailerCode, T1.RetailerType ,T1.Level2Code ,T1.Level5Code
					,T1.ActiveRetailerCode	,0 AS DataSetCode, 0 AS DataSetCode_L5
				FROM @EligibleRetailers T1
					LEFT JOIN #ExistingRetailers T2		ON T2.RetailerCode=T1.RetailerCode 
					LEFT JOIN #ExistingRetailers T3		ON T3.RetailerCode=T1.ActiveRetailerCode 
				WHERE T1.[Status]='Sold' AND  ISNULL(T2.RetailerCode,'')='' AND ISNULL(T3.RetailerCode,'')=''		

				EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY		   		 
	
				DROP TABLE IF EXISTS #ExistingRetailers		
			END
	
			--DELETE FROM @EligibleRetailers WHERE [Status]='SOLD'
	
			/* ==================================== NEXT STEP =========================================================== */	
					   			 		  
			/* CREATE NEW STATEMENT CODE FOR  ACTIVE AND INACTIVE (HAVE SALES) RETAILERS , NOT EXISTING IN TABLE "RP_STATEMENT"	*/
			-- VERSION TYPE = Unlocked , RP Module
			DELETE FROM @EDIT_HISTORY
			
			INSERT INTO RP_Statements(Season, Region, StatementType, [Status], VersionType, StatementLevel, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5)
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Statement Created'
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
			SELECT  @Season, @Region, @STATEMENT_TYPE, 'Active' AS [Status], 'Unlocked' AS [VersionType]
				,T1.StatementLevel, T1.RetailerCode, T1.RetailerType, T1.Level2Code, T1.Level5Code
				,'' AS [ActiveRetailerCode] ,0 AS DataSetCode, 0 AS DataSetCode_L5
			FROM @EligibleRetailers T1
				LEFT JOIN (
					SELECT RetailerCode
					FROM RP_Statements
					WHERE [Season]=@Season AND [Region]=@Region AND StatementType=@Statement_Type AND [VersionType]='UNLOCKED'
				) T2
				ON T2.RetailerCode=T1.RetailerCode 
			WHERE T1.Status IN ('Active','Inactive') AND  ISNULL(T2.RetailerCode,'')=''	


			-- VERSION TYPE = Final-Preferred , to accommodate SRW module
			IF @STATEMENT_TYPE='FORECAST'
			BEGIN 
				INSERT INTO RP_Statements(Season, Region, StatementType, [Status], VersionType, StatementLevel, RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, DataSetCode, DataSetCode_L5)
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Statement Created'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])				
				SELECT  @Season, @Region, @STATEMENT_TYPE, 'Active' AS [Status], 'Final-Preferred' AS [VersionType]
					,T1.StatementLevel, T1.RetailerCode, T1.RetailerType, T1.Level2Code, T1.Level5Code
					,'' AS [ActiveRetailerCode] ,0 AS DataSetCode, 0 AS DataSetCode_L5
				FROM @EligibleRetailers T1
					LEFT JOIN (
						SELECT RetailerCode
						FROM RP_Statements
						WHERE [Season]=@Season AND [Region]=@Region AND StatementType=@Statement_Type AND [VersionType]='Final-Preferred'
					) T2
					ON T2.RetailerCode=T1.RetailerCode 
				WHERE T1.Status='Active' AND  ISNULL(T2.RetailerCode,'')=''	
			END 

			EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY


			/* ==================================== NEXT STEP =========================================================== */	
			/* REACTIVATE STATEMENTS IF STATEMENT CODE EXISTS FOR A RETAILER AND STATUS = 'DELETED' */  
			/* 
				STATUS='INVALID' ( DELETED BY USER AND NOT BY PROCESS) 
				WE DO NOT WANT TO REACTIVATE THESE STATEMENTS
			*/
			DELETE FROM @EDIT_HISTORY

			UPDATE T1
			SET Status='Active'
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Statement is Reactivated.','Field','Status',deleted.[Status],inserted.[Status]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RP_Statements T1
				INNER JOIN @EligibleRetailers T2
				ON T2.RetailerCode=T1.RetailerCode 
			WHERE T1.[Status] IN ('DELETE','DELETED') AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND [VersionType] IN ('Unlocked','Final-Preferred') 
				AND T1.[StatementLevel]=T2.[StatementLevel] AND T1.[RetailerType]=T2.[RetailerType] AND T1.[Level2Code]=T2.[Level2Code] AND T1.[Level5Code]=T2.[Level5Code]
				
			EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY


			IF @REGION='EAST'
			BEGIN
				/* ==================================== NEXT STEP =========================================================== */	
				/* LETS UPDATE DATASETCODE ON LEVEL 1 STATEMENTS */
				UPDATE T1
				SET [DataSetCode]=ISNULL(L2.[StatementCode],0)
				FROM RP_Statements T1
					INNER JOIN	@EligibleRetailers ELG
					ON ELG.[RetailerCode]=T1.[RetailerCode] AND ELG.HierarchyChanged=0 
					LEFT JOIN  (
						SELECT RetailerCode AS [Level2Code], StatementCode, VersionType, StatementType, Region
						FROM RP_Statements
						WHERE [Season]=@SEASON AND [StatementLevel]='LEVEL 2' AND [Status]='Active' 
							AND [Region]=@REGION AND [StatementType]=@STATEMENT_TYPE 
							AND [VersionType] IN ('UNLOCKED','Final-Preferred') 
					) L2
					ON L2.[Level2Code]=ELG.[Level2Code] AND L2.[VersionType]=T1.[VersionType]  AND L2.[StatementType]=T1.[StatementType]  AND L2.[Region]=T1.[Region] 
				WHERE T1.[StatementType]=@STATEMENT_TYPE AND T1.[StatementLevel]='LEVEL 1' AND T1.[Status]='Active' AND T1.[Season]=@Season AND T1.[Region]=@Region 					
			END
			
			IF @STATEMENT_TYPE='ACTUAL'
			BEGIN
				UPDATE T1
				SET [DataSetCode_L5]=ISNULL(L5.[StatementCode],0)
				FROM RP_Statements T1
					INNER JOIN	@EligibleRetailers ELG
					ON ELG.[RetailerCode]=T1.[RetailerCode] AND ELG.HierarchyChanged=0 
					LEFT JOIN  (
						SELECT RetailerCode AS [Level5Code], StatementCode, VersionType, StatementType, Region
						FROM RP_Statements
						WHERE [Season]=@SEASON AND [StatementLevel]='LEVEL 5' AND [Status]='Active' 
							AND [Region]=@REGION AND [StatementType]=@STATEMENT_TYPE 
							AND [VersionType]='UNLOCKED'
					) L5
					ON L5.[Level5Code]=ELG.[Level5Code] AND L5.[VersionType]=T1.[VersionType]  AND L5.[StatementType]=T1.[StatementType] AND L5.[Region]=T1.[Region]
				WHERE T1.[StatementType]=@STATEMENT_TYPE AND  T1.[StatementLevel] IN ('LEVEL 1','LEVEL 2') AND T1.[Status]='Active' AND T1.[Season]=@Season AND T1.[Region]=@Region 
			   				 														
				/*
					LOGIC AS TO REWARD CALCULATIONS AND WHICH STATEMENT CAN RECEIVE PAYMENT

					-- REAILERS UNDER FOLLOWING LELVEL 5 CODE
						-- REWARDS ARE CALCULATED AT LEVEL 1 AND LEVEL 2 COMBINATION
						-- PAYMENT IS MADE AT LEVEL 5 

					EAST	D0000111	SOLLIO AGRICULTURE S.E.C.
					EAST	D0000212	THOMPSONS LIMITED
					EAST	D520001300	SYNAGRI - HEAD OFFICE

					-- REAILERS UNDER FOLLOWING LELVEL 5 CODE
						-- REWARDS ARE CALCULATED AT LEVEL 5 
						-- PAYMENT IS MADE AT LEVEL 5 TOO

					WEST	D520062427	NUTRIEN AG SOLUTIONS CANADA    -------> RP-2975
					WEST	D0000107	CARGILL LIMITED - RETAIL WEST
					WEST	D0000244	UNITED FARMERS OF ALBERTA
					WEST	D0000137	RICHARDSON PIONEER LTD

					-- REAILERS UNDER FOLLOWING LELVEL 5 CODE
						-- REWARDS CALCULATD AT LEVEL1 OR LEVEL2 
						-- PAYMENT IS MADE AT LEVEL 5 

					WEST	D0000117	FEDERATED CO-OPERATIVES LIMITED
				*/

				-- BY DEFAULT LETS SET THIS VALUE TO 'YES'
				UPDATE RP_Statements 
				SET IsPayoutStatement='Yes'
				WHERE [Status]='Active' AND Season=@SEASON AND REGION=@REGION AND StatementType=@STATEMENT_TYPE AND VersionType='Unlocked' 

				UPDATE T1	
				SET IsPayoutStatement='No'
				FROM RP_Statements T1
					INNER JOIN RP_Config_HOPaymentLevel HO
					ON HO.Level5Code=T1.Level5Code AND HO.Season=T1.Season 
				WHERE T1.[Status]='Active' AND  T1.Season=@SEASON AND T1.REGION=@REGION AND T1.StatementType=@STATEMENT_TYPE AND T1.VersionType='Unlocked' 
					 AND T1.StatementLevel <> 'LEVEL 5' AND HO.Summary='Yes'
					 AND (HO.Level5Code <> 'D520062427' OR T1.Region='WEST')
					 								
				IF @REGION='EAST'
					UPDATE RP_Statements 
					SET IsPayoutStatement='No'
					WHERE [IsPayoutStatement]='Yes' AND [Status]='Active' AND Season=@SEASON AND REGION=@REGION AND StatementType=@STATEMENT_TYPE AND VersionType='Unlocked' 
						AND [StatementLevel]='LEVEL 1' AND ([DataSetCode] > 0 OR [DataSetCode_L5] > 0)

			END	
					   			
			/* ==================================== NEXT STEP =========================================================== */	
			/* UPDATE STATUS TO "ERROR" IF EXISTING RETAILERCODE IS NOT AVAILABLE IN ELIGIBLE RETAILERS LIST */
			DELETE FROM @EDIT_HISTORY

			IF @STATEMENT_TYPE = 'ACTUAL'
			BEGIN
				UPDATE T1
				SET [Status]='ERROR'		
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Status=Error: Retailer is not listed in eligible retailers list. But has an active locked statement.','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RP_Statements T1
					INNER JOIN RPWeb_Statements LCK   ON LCK.SRC_StatementCode=T1.StatementCode AND LCK.VersionType='Locked' AND LCk.[Status]='Active'
					LEFT JOIN @EligibleRetailers T2	  ON T2.RetailerCode=T1.RetailerCode   AND T2.RetailerCode <> 'R520157196'				
				WHERE T1.[Category]='Regular' AND T1.[Status]='ACTIVE' AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred')  
					AND ISNULL(T2.RetailerCode,'')='' 
					AND T1.RetailerCode <> 'R520157196'
			END 
			
			-- LETS COMPARE UNLOCKED vs ELIGIBLE Retailers to make sure none of the following values changed 
			-- Retailercode, StatementLevel, RetailerType, Level2Code and Level5Code

			UPDATE T1
			SET [Status]='ERROR'		
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Status=Error: There is a change in Retailer Hierarchy.','Field','Status',deleted.[Status],inserted.[Status]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RP_Statements T1
				LEFT JOIN @EligibleRetailers T2
				ON T2.RetailerCode=T1.RetailerCode  AND T2.StatementLevel=T1.StatementLevel AND T2.[RetailerType]=T1.[RetailerType] AND T2.[Level2Code]=T1.[Level2Code] AND T2.[Level5Code]=T1.[Level5Code]  AND T2.RetailerCode <> 'R520157196'
			WHERE T1.[Category]='Regular' AND T1.[Status]='Active' AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred')  
				AND ISNULL(T2.RetailerCode,'')='' 
				AND T1.RetailerCode <> 'R520157196'				
				
			-- LETS COMPARE UNLOCKED vs LOCKED STATEMENT Retailercode, StatementLevel, RetailerType, Level2Code and Level5Code
			IF @STATEMENT_TYPE = 'ACTUAL'
			BEGIN
				UPDATE T1
				SET [Status]='ERROR'		
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Status=Error: There is a change in Retailer Hierarchy (Unlocked vs Locked).','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RP_Statements T1
					INNER JOIN RPWeb_Statements T2
					ON T2.SRC_StatementCode=T1.StatementCode AND T2.VersionType='Locked' AND T2.[Status]='Active'			
				WHERE T1.[Category]='Regular' AND T1.[Status]='Active' AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred')  
					AND (T2.RetailerCode <> T1.RetailerCode  OR T2.StatementLevel <> T1.StatementLevel AND T2.[RetailerType] <> T1.[RetailerType] OR T2.[Level2Code] <> T1.[Level2Code] OR T2.[Level5Code] <> T1.[Level5Code])
			END	

			-- DATASET CODE HAVING ERRORED STATEMENT
			IF @REGION='EAST'
				UPDATE T1
				SET [Status]='ERROR'		
				OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Status=Error: At DataSetCode level, one or more statements are in Error status.','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RP_Statements T1
					INNER JOIN (
						SELECT [StatementCode] AS [DataSetCode]
						FROM RP_Statements 
						WHERE [Status]='Error' AND [StatementLevel]='LEVEL 2' 
							AND [Season]=@Season AND [Region]=@Region AND [StatementType]=@Statement_Type AND [VersionType] IN ('Unlocked','Final-Preferred') 	
							
							UNION

						SELECT DataSetCode
						FROM RP_Statements 
						WHERE [Status]='Error' AND [StatementLevel]='LEVEL 1' AND [DataSetCode] > 0 
							AND [Season]=@Season AND [Region]=@Region AND [StatementType]=@Statement_Type AND [VersionType] IN ('Unlocked','Final-Preferred') 						
						GROUP BY DataSetCode					
					) T2
					ON T2.DataSetCode=T1.DataSetCode OR T2.DataSetCode=T1.StatementCode 
				WHERE T1.[Category]='Regular' AND T1.[Status]='Active' AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred')  
									   																			   									 								  
			EXEC RP_GenerateEditHistory 'Statement', @USERNAME,@EDIT_HISTORY

			UPDATE T1
			SET [Status]='DELETED'		
			OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Status=Deleted: Retailer is not listed in eligible retailers list.','Field','Status',deleted.[Status],inserted.[Status]
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM RP_Statements T1
				LEFT JOIN @EligibleRetailers T2	ON T2.RetailerCode=T1.RetailerCode AND T2.RetailerCode <> 'R520157196'			
			WHERE T1.[Category]='Regular' AND T1.[Status]='ACTIVE' AND T1.[Season]=@Season AND T1.[Region]=@Region AND T1.[StatementType]=@Statement_Type AND T1.[VersionType] IN ('Unlocked','Final-Preferred')  
				AND ISNULL(T2.RetailerCode,'')='' 
				AND T1.RetailerCode <> 'R520157196'					   

			/* ==================================== NEXT STEP =========================================================== */	
			/* UPDATE STATEMENT MAPPINGS */
			EXEC [RP_UpdateStatementMappings] @SEASON, @REGION, @STATEMENT_TYPE

			DELETE FROM @EDIT_HISTORY
			DELETE FROM @EligibleRetailers

			FETCH NEXT FROM REGIONS_LOOP INTO @REGION
		END
	CLOSE REGIONS_LOOP
	DEALLOCATE REGIONS_LOOP	

	/* 
		LETS UPDATE STATUS OF STATEMENTS IN RPWEB STATEMENTS IMMEDIATELY FOR THE FOLLOWING STATUSES
		PROCESSES: TRANSFER STATEMENTS PROCESS AND ALL OTHER REFRESH (RP_AGENT_REFRESH) TARGETS STATEMENTS IN ACTIVE STATUS ONLY
		HENCE IT IS IMPORTANT WE UPDATE STATUS OF RPWEB STATEMENTS HERE ITSELF
	*/
	DROP TABLE IF EXISTS #CY_INACTIVE_STATEMENTS
	SELECT StatementCode, Status
	INTO #CY_INACTIVE_STATEMENTS
	FROM RP_Statements
	WHERE Season=@SEASON AND Status IN ('Delete','Deleted','Invalid','Error')

	UPDATE t1
	SET [Status]=t2.[Status]
		,[Deleted_Date]=GETDATE()
	FROM RPWeb_Statements t1
		INNER JOIN #CY_INACTIVE_STATEMENTS t2		
		ON t1.[StatementCode]=t2.[StatementCode]		
	WHERE t2.[Status] IN ('Delete','Deleted','Invalid') AND t1.[Status] <> t2.[Status] 
	
	UPDATE t1
	SET [Status]=t2.[Status]	
	FROM RPWeb_Statements t1
		INNER JOIN #CY_INACTIVE_STATEMENTS t2		
		ON t1.[StatementCode]=t2.[StatementCode]		
	WHERE t2.[Status]='Error' AND t1.[Status] <> t2.[Status] 

	   	
END
