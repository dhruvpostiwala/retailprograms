﻿






CREATE PROCEDURE [dbo].[RP2020_Calc_W_TitanCompProg]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
		DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		RewardBrand_Sales [numeric](18, 2) NOT NULL DEFAULT 0,
		[Quantity] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0,
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC
	)
		)

	/*
	Eligible Retails:R0001154	ARTY'S AIR SERVICER0001277	JONAIR (1988) LTD.R0001744	TABER HOME & FARM CENTRE LTD

	Calculation Rules:

	Qualifying Level: Level 2. For standalone will be at level 1.Calculating Level: Level 2. For standalone will be at level 1.

	Calculation:

	$158 x Eligible Cases of Titan Sold in 2020 = Reward $ 
	*/
	

		-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN


	DECLARE @MARKETLETTERCODE VARCHAR(50) = 'ML2020_W_ADD_PROGRAMS';

	--https://kennahome.jira.com/browse/RP-3450
	--JONAIR SOLD DLR Code 


	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales, Quantity)
	SELECT ST.StatementCode,@MARKETLETTERCODE, @PROGRAM_CODE
		,MAX(tx.ChemicalGroup) as RewardBrand
		,SUM(IIF(tx.ChemicalGroup='TITAN' AND BASFSeason=2020 AND tx.InRadius = 1 ,tx.Price_SDP,0)) AS RewardBrand_Sales  
		,SUM(IIF(tx.ChemicalGroup='TITAN' AND BASFSeason=2020 AND tx.InRadius = 1 ,tx.Acres/44,0)) AS Quantity  --Conversion to cases
	FROM @STATEMENTS ST		
		INNER JOIN RP_Statements RS			ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_DT_Transactions TX  	ON TX.StatementCode = ST.StatementCode
	WHERE RS.StatementType='Actual' AND tx.ChemicalGroup ='TITAN' AND RS.RetailerCode IN ('R0001154','R0001277','R0001744','DLR-TORO-BY6946') --Eligible Retailers
	GROUP BY ST.StatementCode

	DECLARE @REWARD_PERC FLOAT = 158;   --Rewward amount per case

	--UPDATE REWARD DOLLARS
	UPDATE @TEMP 
	SET Reward = Quantity * @REWARD_PERC   
	WHERE Quantity > 0

	DELETE FROM RP2020_DT_Rewards_W_TitanCompProg WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_TitanCompProg (StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Quantity,Reward
	FROM @TEMP

END

