﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_E_PursuitSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	NEW: RC-3759 - Qualification is done at the OU level

	If a retailer’s 2020 POG sales of Pursuit are equal to or greater than their 2019 POG sales, the
	retailer will be eligible for a rebate of $75.00 per case on all 2020 Pursuit POG sales.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_E_PURSUIT_REWARD'

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		Pursuit_Qty_CY Numeric(18,2) NOT NULL,
		Pursuit_Qty_LY Numeric(18,2) NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY MONEY NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Pursuit_Reward_Amount MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Pursuit_Qty_CY Numeric(18,2) NOT NULL,
		Pursuit_Qty_LY Numeric(18,2) NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY MONEY NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Pursuit_Reward_Amount MONEY NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)


	DECLARE @RewardAmount INT = 75;
	DECLARE @Conversion FLOAT = (SELECT ConversionE from ProductReference where ProductCode='000000000059010167'); -- Fetch conversion from Prouduct Reference Table based on the Eligible ProductCode 

	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,Pursuit_Qty_CY,Pursuit_Qty_LY,QualBrand_Sales_CY,QualBrand_Sales_LY,RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,SUM(tx.Acres_CY/@Conversion) AS Pursuit_Qty_CY
	,SUM(tx.Acres_LY1/@Conversion) AS Pursuit_Qty_LY  -- Quantity calculated by Acres / Conversion fetched from Product Reference Table
	,SUM(TX.Price_Q_SDP_CY) AS QualBrand_Sales_CY
	,SUM(tx.Price_Q_SDP_LY1) AS QualBrand_Sales_LY
	,SUM(Price_CY) RewardBrand_Sales
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode

	-- Determine growth based on the sum of pursuit acres at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,Pursuit_Qty_CY,Pursuit_Qty_LY, QualBrand_Sales_CY,QualBrand_Sales_LY,RewardBrand_Sales)
	SELECT CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(Pursuit_Qty_CY) AS Pursuit_Qty_CY,SUM(Pursuit_Qty_LY) AS Pursuit_Qty_LY, 
		SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY, SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
		,SUM(IIF(RewardBrand_Sales > 0, RewardBrand_Sales, 0)) AS RewardBrand_Sales
	FROM @TEMP
	GROUP BY CASE WHEN DataSetCode > 0 THEN DataSetCode ELSE StatementCode END, MarketLetterCode, ProgramCode

	-- Update reward $ at the OU level
	UPDATE @TEMP_TOTALS
	SET Pursuit_Reward_Amount = @RewardAmount
	WHERE QualBrand_Sales_CY >= QualBrand_Sales_LY

	-- Update the reward $ back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Pursuit_Reward_Amount = T2.Pursuit_Reward_Amount
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the reward $ back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Pursuit_Reward_Amount = T2.Pursuit_Reward_Amount
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0

	-- Update reward
	UPDATE @TEMP SET Reward = Pursuit_Qty_CY * Pursuit_Reward_Amount --WHERE Pursuit_Qty_CY > 0
			
	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END
	

	-- INSERT INTO DT TABEL FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_PursuitSupport WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_PursuitSupport(Statementcode, MarketLetterCode, ProgramCode, Pursuit_Qty_CY, Pursuit_Qty_LY,RewardBrand_Sales, Pursuit_Reward_Amount, Reward, QualBrand_Sales_CY, QualBrand_Sales_LY)
	SELECT Statementcode, MarketLetterCode, ProgramCode, Pursuit_Qty_CY, Pursuit_Qty_LY,RewardBrand_Sales, Pursuit_Reward_Amount, Reward, QualBrand_Sales_CY, QualBrand_Sales_LY
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode
		,SUM(Pursuit_Qty_CY) AS Pursuit_Qty_CY
		,SUM(Pursuit_Qty_LY) AS Pursuit_Qty_LY
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,MAX(Pursuit_Reward_Amount) AS Pursuit_Reward_Amount
		,SUM(Reward) AS Reward
		,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
		,SUM(QualBrand_Sales_LY) AS QualBrand_Sales_LY
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode

END

