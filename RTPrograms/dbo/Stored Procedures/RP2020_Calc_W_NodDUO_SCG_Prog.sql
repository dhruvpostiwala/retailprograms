﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_NodDUO_SCG_Prog]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

		-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
		IF @STATEMENT_TYPE <> 'ACTUAL' 
		RETURN
		
		
		DECLARE @TEMP TABLE (
		[StatementCode] [int] NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		RewardBrand VARCHAR(50) NOT NULL,
		[Quantity_LY] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Quantity_CY] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Sales_LY] [numeric](18, 2) NOT NULL DEFAULT 0,
		[RewardBrand_Sales] [numeric](18, 2) NOT NULL DEFAULT 0,
		[Growth] DECIMAL(5,3) NOT NULL DEFAULT 0,
		[Reward_Perc] [numeric](18, 2) DEFAULT 0,
		[Reward] MONEY NOT NULL DEFAULT 0
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC
	)
	)
	
	DECLARE @NewGrowers TABLE (
		[StatementCode] [int] NOT NULL,
		[FarmCode] VARCHAR(50) NOT NULL,
		[Acres] [numeric](18, 2) NOT NULL,
		[Reward] MONEY NOT NULL DEFAULT 0
	PRIMARY KEY CLUSTERED 
	(
		[StatementCode] ASC,
		[FarmCode] ASC
	)
	)
	/*
	Reward opportunity

	Retails will receive a 2% rebate on all 2020 Eligible POG Sales of Nodulator Duo SCG when the retail achieves at least 100% growth YOY to 2019 Eligible POG$ Sales of the same brand. 

	Retails will be eligible for additional rewards of $7.50/bag &/or $120/Q pack when they sell Nodulator Duo SCG to growers who did not purchase this brand in the 2019 season. The retailer must sell a minimum of 16 22.68KG Bags or 1 364KG Q Pack to qualify.Net New Grower = a grower who did not buy Nodulator in 2019 but could have purchased in 2018 or prior.

	Qualifying Level: Level 2. For standalone will be at level 1.Calculating Level: Level 2. For standalone will be at level 1.

	Calculation

	2020 Eligible POG$ Nodulator Duo SCG/ 2019 Eligible POG$ Nodulator Duo SCG = X%If X% => 100% Retail receives 2% x 2020 Eligible POG$ Nodulator Duo SCG.

	of Eligible Nodulator Duo SCG bags => 16 = $7.50 x # of Eligible Nodulator Duo SCG Bags to net new growers.

	of Eligible Nodulator Duo SCG Q Packs => 1= $120.00 x # of Eligible Nodulator Duo SCG Q packs to net new growers.
	*/

	DECLARE @SEASON_LY INT = @SEASON - 1;
	DECLARE @minimum_acres  int = 240;  -- 16 22.68KG Bags or 1 364KG Q Pack 
	DECLARE @conversion  int = 15;
	DECLARE @reward_Per_Bag  float = 7.5;
	DECLARE @REWARD_PERC FLOAT = 0.02;
	DECLARE @ROUND_UP_VALUE DECIMAL(3,3) = [dbo].[SVF_GetPercentageRoundUpValue](@SEASON);
	DECLARE @MARKETLETTERCODE VARCHAR(50) = 'ML2020_W_ADD_PROGRAMS';
	
	--GET NEW GROWERS FOR PART B OF REWARD
	


	--**new Demarey. Growers that bought from other retailers in previous years should not qualify
	-- RP-2545 
	IF EXISTS(SELECT * FROM RP_Statements WHERE VersionType='UNLOCKED' AND Statementcode IN (SELECT Statementcode FROM @STATEMENTS))
	BEGIN
		INSERT INTO @NewGrowers(StatementCode,FarmCode,Acres)
		SELECT ST.StatementCode ,CY.FarmCode ,SUM(CY.Acres) Acres
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_Transactions CY		
			ON CY.StatementCode = ST.StatementCode 
		WHERE CY.ChemicalGroup='NODULATOR DUO SCG' AND CY.BASFSeason=@SEASON AND CY.InRadius=1 	
		GROUP BY ST.StatementCode ,CY.FarmCode

		DELETE T1
		FROM @NewGrowers T1		
			INNER JOIN (			
				SELECT GTD.FarmCode
				FROM GrowerTransactionsBASFDetails  GTD
					INNER JOIN (
						SELECT DISTINCT Farmcode FROM @NewGrowers
					) F	
					ON F.Farmcode=GTD.Farmcode
					INNER JOIN (
						SELECT ProductCode 
						FROM ProductReference 
						WHERE DocType=10 AND ChemicalGroup='NODULATOR DUO SCG'
					) PR		
					ON PR.Productcode=GTD.Productcode					
				WHERE GTD.BASFSeason=@SEASON_LY
				GROUP BY GTD.FarmCode
				HAVING SUM(GTD.Quantity) > 0		
			) LY
			ON LY.Farmcode=T1.Farmcode
	END
	

	-- IN CASE OF "USE CASE" Statements, WE HAVE TO CHECK FOR LY SALES WITH IN SAME STATEMENTCODE
	IF EXISTS(SELECT * FROM RP_Statements WHERE VersionType='USE CASE' AND Statementcode IN (SELECT Statementcode FROM @STATEMENTS))
		INSERT INTO @NewGrowers(StatementCode,FarmCode,Acres)
		SELECT ST.StatementCode ,CY.FarmCode	,SUM(CY.Acres) Acres
		FROM @STATEMENTS ST
			INNER JOIN RP_DT_Transactions CY		ON CY.StatementCode = ST.StatementCode AND CY.ChemicalGroup='NODULATOR DUO SCG' AND CY.BASFSeason=@SEASON AND CY.InRadius=1 				
			LEFT JOIN  RP_DT_Transactions LY		ON LY.StatementCode = ST.StatementCode AND LY.ChemicalGroup='NODULATOR DUO SCG' AND LY.BASFSeason=@SEASON_LY 		
		GROUP BY ST.StatementCode ,CY.FarmCode
		HAVING SUM(ISNULL(LY.Acres,0)) <= 0
		
	---PART A 
	/*
	INSERT INTO @TEMP(StatementCode, MarketLetterCode,ProgramCode,RewardBrand,Quantity_LY,Quantity_CY,Sales_LY,RewardBrand_Sales)
	SELECT ST.StatementCode,@MARKETLETTERCODE, @PROGRAM_CODE
		,MAX(tx.ChemicalGroup) as RewardBrand		
		,SUM(IIF(tx.ChemicalGroup='NODULATOR DUO SCG' AND BASFSeason=@SEASON_LY AND tx.InRadius = 1 ,tx.Acres/@conversion,0)) AS Quantity_LY  
		,SUM(IIF(tx.ChemicalGroup='NODULATOR DUO SCG' AND BASFSeason=@SEASON AND tx.InRadius = 1 ,tx.Acres/@conversion,0)) AS Quantity_CY 
		,SUM(IIF(tx.ChemicalGroup='NODULATOR DUO SCG' AND BASFSeason=@SEASON_LY AND tx.InRadius = 1 ,IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,tx.Price_SDP),0)) AS Sales_LY  
		,SUM(IIF(tx.ChemicalGroup='NODULATOR DUO SCG' AND BASFSeason=@SEASON AND tx.InRadius = 1 ,IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,tx.Price_SDP),0)) AS RewardBrand_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_Transactions TX ON TX.StatementCode = ST.StatementCode
		INNER JOIN RP_Statements S ON S.StatementCode = TX.StatementCode
	WHERE tx.ChemicalGroup ='NODULATOR DUO SCG'
	GROUP BY ST.StatementCode		
	*/

	INSERT INTO @TEMP(StatementCode, MarketLetterCode,ProgramCode,RewardBrand,Quantity_LY,Quantity_CY,Sales_LY,RewardBrand_Sales)
	SELECT ST.StatementCode,@MARKETLETTERCODE, @PROGRAM_CODE ,tx.ChemicalGroup 
		,SUM(IIF(BASFSeason=@SEASON_LY AND tx.InRadius = 1 ,tx.Acres/@conversion,0)) AS Quantity_LY  
		,SUM(IIF(BASFSeason=@SEASON AND tx.InRadius = 1 ,tx.Acres/@conversion,0)) AS Quantity_CY 
		,SUM(IIF(BASFSeason=@SEASON_LY AND tx.InRadius = 1 ,IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,tx.Price_SDP),0)) AS Sales_LY  
		,SUM(IIF(BASFSeason=@SEASON AND tx.InRadius = 1 ,IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,tx.Price_SDP),0)) AS RewardBrand_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode = ST.StatementCode
		INNER JOIN RP_Statements S			ON S.StatementCode = TX.StatementCode
	WHERE tx.ChemicalGroup ='NODULATOR DUO SCG'
	GROUP BY ST.StatementCode, tx.ChemicalGroup		


	-- HEAD OFFICE BREAKDOWN
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT ST.StatementCode, 'ML2020_W_ADD_PROGRAMS', @PROGRAM_CODE, TX.RetailerCode, TX.FarmCode
		,SUM(IIF(S.Level5Code IN ('D0000107','D520062427','D0000137','D0000244'), tx.Price_SWP,tx.Price_SDP)) AS Sales
		,1 AS FarmLevelSplit
	FROM @STATEMENTS ST			
		INNER JOIN RP_Statements S			ON S.StatementCode = ST.StatementCode
		INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode=ST.StatementCode
	WHERE tx.BASFSeason=@SEASON
		AND tx.InRadius=1				
		AND tx.ChemicalGroup ='NODULATOR DUO SCG'			
	GROUP BY ST.StatementCode,  TX.RetailerCode, TX.FarmCode
	   	  
	-------------PART A----------------------
	
	UPDATE @TEMP
	SET Growth =  RewardBrand_Sales / Sales_LY
	WHERE Sales_LY > 0
	
	---When there are no last year sales and there are current year sales
	UPDATE @TEMP
	SET Growth =  1 
	WHERE Sales_LY = 0 AND RewardBrand_Sales > 0 

	--UPDATE PART A REWARD
	UPDATE @TEMP 
	SET Reward =  RewardBrand_Sales * @REWARD_PERC, 
		Reward_Perc = @REWARD_PERC
	WHERE Growth + @ROUND_UP_VALUE >= 1;

	----------------PART B-----------------

	UPDATE G 
	SET Reward = (Acres/@conversion) * @reward_Per_Bag --each bag times 7.5
	FROM @NewGrowers G
	INNER JOIN @TEMP T1 ON T1.StatementCode = G.StatementCode
	WHERE Acres >= @minimum_acres


	--------------------Part A-------------------- 
	DELETE FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_NodDUO_SCG_Prog (StatementCode, MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Sales_LY,Growth,Reward_Perc,Reward)
	SELECT StatementCode, MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Sales_LY,Growth,Reward_Perc,Reward
	FROM @TEMP

	-------------------Part B ------------------------
	DELETE FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog_NewGrowers WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_NodDUO_SCG_Prog_NewGrowers([StatementCode],  [MarketLetterCode], [ProgramCode], [FarmCode], [RewardBrand_Acres], [Reward_Perc], [Reward])
	SELECT StatementCode,@MARKETLETTERCODE [MarketLetterCode] , @PROGRAM_CODE [ProgramCode], [FarmCode], Acres AS [RewardBrand_Acres], @reward_Per_Bag AS [Reward_Perc], [Reward]
	FROM @NewGrowers


/*
	/* EXCEPTION CODE TO ACCOMMODATE WEST AUGUST CHEQUE RUN */

	DELETE T1
	FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog T1
		INNER JOIN @STATEMENTS ST 		ON ST.Statementcode=T1.Statementcode
	WHERE T1.[StatementCode] IN (
		SELECT Statementcode 
		FROM RP_Statements 						
		WHERE [Season]=2020 AND [Status]='Active' AND [StatementType]='ACTUAL' AND [VersionType]='Unlocked' AND [Level5Code] <> 'D000001' 		
	)
	   
	DELETE T1
	FROM RP2020_DT_Rewards_W_NodDUO_SCG_Prog_NewGrowers T1
		INNER JOIN @STATEMENTS ST 		ON ST.Statementcode=T1.Statementcode
	WHERE T1.[StatementCode] IN (
		SELECT Statementcode 
		FROM RP_Statements 						
		WHERE [Season]=2020 AND [Status]='Active' AND [StatementType]='ACTUAL' AND [VersionType]='Unlocked' AND [Level5Code] <> 'D000001' 		
	)
*/


END



