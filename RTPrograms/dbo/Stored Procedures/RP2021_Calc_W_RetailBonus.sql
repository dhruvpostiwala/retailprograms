﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_RetailBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		Growth_Sales_CY MONEY NOT NULL,
		Growth_Sales_LY1 MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_INDEX MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Canola MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Pulse MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Cereal MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Canola_INDEX MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Pulse_INDEX MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY1_Cereal_INDEX MONEY NOT NULL DEFAULt 0,
		Growth_Sales_LY2 MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY2_Canola MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY2_Pulse MONEY NOT NULL DEFAULT 0,
		Growth_Sales_LY2_Cereal MONEY NOT NULL DEFAULT 0,
		Lib_Reward_Sales MONEY NOT NULL,
		Lib_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Lib_Reward MONEY NOT NULL DEFAULT 0,
		Cent_Reward_Sales MONEY NOT NULL,
		Cent_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Cent_Reward MONEY NOT NULL DEFAULT 0,
		Growth_Percentage DECIMAL(18,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			StatementCode ASC,
			MarketLetterCode ASC,
			ProgramCode ASC
		)
	);

	DECLARE @CROP_SEGMENT TABLE (
		Crop_Segment VARCHAR(50),
		ChemicalGroup VARCHAR(100),
		PRIMARY KEY CLUSTERED (
			[ChemicalGroup] ASC
		)
	);


	-- IDENTIFY WHICH BRAND GOES INTO EACH GROUP
	INSERT	INTO @CROP_SEGMENT
	SELECT	*
	FROM	(
		SELECT	DISTINCT 'CANOLA' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CERTITUDE', 'COTEGRA', 'FACET L', 'LANCE', 'LANCE AG', 'CENTURION')

		UNION	ALL

		SELECT	DISTINCT 'PULSE' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CEVYA', 'DYAX', 'ENGENIA', 'FORUM', 'HEADLINE', 'NODULATOR', 'NODULATOR DUO SCG', 'NODULATOR LQ', 'NODULATOR N/T',
								  'NODULATOR PRO', 'NODULATOR SCG', 'NODULATOR XL', 'PRIAXOR', 'SEFINA', 'SERCADIS', 'TITAN', 'ZIDUA SC')
		UNION	ALL

		SELECT	DISTINCT 'CEREAL' AS Crop_Segment, ChemicalGroup
		FROM	ProductReference
		WHERE	ChemicalGroup IN ('CARAMBA', 'INSURE Cereal', 'INSURE CEREAL FX4', 'INSURE PULSE', 'NEXICOR', 'TWINLINE', 'TERAXXA F4')
	)F


	-- Get the data from sales consolidated




	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		BEGIN
			INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1_Canola, Growth_Sales_LY1_Pulse, Growth_Sales_LY1_Cereal, Growth_Sales_LY2_Canola, Growth_Sales_LY2_Pulse, Growth_Sales_LY2_Cereal, Lib_Reward_Sales, Cent_Reward_Sales,Growth_Sales_LY1_Canola_INDEX,Growth_Sales_LY1_Pulse_INDEX,Growth_Sales_LY1_Cereal_INDEX)
			SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode,
				   SUM(IIF(TX.GroupType IN ('ALL_CPP_BRANDS', 'QUALIFYING_BRANDS'), TX.Price_CY, 0)) [QualBrand_Sales_CY],
				   SUM(IIF(TX.GroupType = 'GROWTH_QUALIFYING_BRANDS', TX.Price_CY, 0)) [Growth_Sales_CY],
				   SUM(CASE WHEN CS.Crop_Segment = 'CANOLA' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1 ELSE 0 END) [Growth_Sales_LY1_Canola],
				   SUM(CASE WHEN CS.Crop_Segment = 'PULSE' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1 ELSE 0 END) [Growth_Sales_LY1_Pulse],
				   SUM(CASE WHEN CS.Crop_Segment = 'CEREAL' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1 ELSE 0 END) [Growth_Sales_LY1_Cereal],
				   SUM(CASE WHEN CS.Crop_Segment = 'CANOLA' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY2 ELSE 0 END) [Growth_Sales_LY2_Canola],
				   SUM(CASE WHEN CS.Crop_Segment = 'PULSE' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY2 ELSE 0 END) [Growth_Sales_LY2_Pulse],
				   SUM(CASE WHEN CS.Crop_Segment = 'CEREAL' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY2 ELSE 0 END) [Growth_Sales_LY2_Cereal],
				   SUM(IIF(TX.GroupType = 'REWARD_BRANDS' AND PR.ChemicalGroup IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200'), TX.Price_CY, 0)) [Lib_Reward_Sales],
				   SUM(IIF(TX.GroupType = 'REWARD_BRANDS' AND PR.ChemicalGroup = 'CENTURION', TX.Price_CY, 0)) [Cent_Reward_Sales],
				   SUM(CASE WHEN CS.Crop_Segment = 'CANOLA' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1*ISNULL(TX.YOY_Index, 1) ELSE 0 END) [Growth_Sales_LY1_Canola_INDEX],
				   SUM(CASE WHEN CS.Crop_Segment = 'PULSE' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1*ISNULL(TX.YOY_Index, 1) ELSE 0 END) [Growth_Sales_LY1_Pulse_INDEX],
				   SUM(CASE WHEN CS.Crop_Segment = 'CEREAL' AND TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' THEN TX.Price_LY1*ISNULL(TX.YOY_Index, 1) ELSE 0 END) [Growth_Sales_LY1_Cereal_INDEX]
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX			ON ST.StatementCode = TX.StatementCode
				INNER JOIN ProductReference PR						ON TX.ProductCode = PR.ProductCode
				LEFT JOIN @CROP_SEGMENT CS							ON CS.ChemicalGroup = PR.ChemicalGroup
			WHERE TX.ProgramCode = @PROGRAM_CODE
			GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode
			   		 	  
			-- Factor in CAR indexing percentages for different product groups
			-- Canola
			UPDATE T1
			SET T1.Growth_Sales_LY1_Canola = T1.Growth_Sales_LY1_Canola * (EI.FieldValue / 100),
				T1.Growth_Sales_LY2_Canola = T1.Growth_Sales_LY1_Canola * (EI.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RPWeb_User_EI_FieldValues EI
				ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Canola'

			-- Pulse
			UPDATE T1
			SET T1.Growth_Sales_LY1_Pulse = T1.Growth_Sales_LY1_Pulse * (EI.FieldValue / 100),
				T1.Growth_Sales_LY2_Pulse = T1.Growth_Sales_LY2_Pulse * (EI.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RPWeb_User_EI_FieldValues EI
				ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Pulse'

			-- Cereal
			UPDATE T1
			SET T1.Growth_Sales_LY1_Cereal = T1.Growth_Sales_LY1_Cereal * (EI.FieldValue / 100),
				T1.Growth_Sales_LY2_Cereal = T1.Growth_Sales_LY2_Cereal * (EI.FieldValue / 100)
			FROM @TEMP T1
				INNER JOIN RPWeb_User_EI_FieldValues EI
				ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND EI.FieldCode = 'CAR_Indexing_Cereal'


			-- SET THE INDEX VALUES IDENTICAL TO THE REGULAR VALUES (FOR FORECAST AND PROJECTIONS)
			-- This is because index values are used for display and calculation purposes (aka growth)
			UPDATE	@TEMP
			SET		Growth_Sales_LY1_Canola_INDEX = Growth_Sales_LY1_Canola
					,Growth_Sales_LY1_Pulse_INDEX = Growth_Sales_LY1_Pulse
					,Growth_Sales_LY1_Cereal_INDEX = Growth_Sales_LY1_Cereal

		END -- END OF FORECAST AND PROJECTION

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1_Canola, Growth_Sales_LY1_Pulse, Growth_Sales_LY1_Cereal, Growth_Sales_LY2_Canola, Growth_Sales_LY2_Pulse, Growth_Sales_LY2_Cereal, Lib_Reward_Sales, Cent_Reward_Sales,Growth_Sales_LY1_Canola_INDEX,Growth_Sales_LY1_Pulse_INDEX,Growth_Sales_LY1_Cereal_INDEX)
		SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode,
			   SUM(IIF(TX.GroupType IN ('ALL_CPP_BRANDS', 'QUALIFYING_BRANDS'), TX.Price_CY, 0)) [QualBrand_Sales_CY],
			   SUM(IIF(TX.GroupType = 'GROWTH_QUALIFYING_BRANDS', TX.Price_CY, 0)) [Growth_Sales_CY],
			   SUM(IIF(TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' ,TX.Price_LY1,0)) [Growth_Sales_LY1_Canola],
			   0 [Growth_Sales_LY1_Pulse],
			   0 [Growth_Sales_LY1_Cereal],
			   SUM(IIF(TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' ,TX.Price_LY2,0)) [Growth_Sales_LY2_Canola],
			   0 [Growth_Sales_LY2_Pulse],
			   0 [Growth_Sales_LY2_Cereal],
			   SUM(IIF(TX.GroupType = 'REWARD_BRANDS' AND PR.ChemicalGroup IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200'), TX.Price_CY, 0)) [Lib_Reward_Sales],
			   SUM(IIF(TX.GroupType = 'REWARD_BRANDS' AND PR.ChemicalGroup = 'CENTURION', TX.Price_CY, 0)) [Cent_Reward_Sales],
			   SUM(IIF(TX.GroupType = 'GROWTH_QUALIFYING_BRANDS' ,TX.Price_LY1*ISNULL(TX.YOY_Index, 1),0)) [Growth_Sales_LY1_Canola_INDEX],
			   0 [Growth_Sales_LY1_Pulse_INDEX],
			   0 [Growth_Sales_LY1_Cereal_INDEX]
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX			ON ST.StatementCode = TX.StatementCode
			INNER JOIN ProductReference PR						ON TX.ProductCode = PR.ProductCode			
		WHERE TX.ProgramCode = @PROGRAM_CODE
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode
	END
	   
	-- Add together the updated sales for the different brand groupings
	UPDATE @TEMP
	SET Growth_Sales_LY1 = Growth_Sales_LY1_Canola + Growth_Sales_LY1_Pulse + Growth_Sales_LY1_Cereal,
		Growth_Sales_LY1_INDEX = Growth_Sales_LY1_Canola_INDEX + Growth_Sales_LY1_Pulse_INDEX + Growth_Sales_LY1_Cereal_INDEX,
		Growth_Sales_LY2 = Growth_Sales_LY2_Canola + Growth_Sales_LY2_Pulse + Growth_Sales_LY2_Cereal

	-- Set growth percentage
	UPDATE @TEMP
		SET Growth_Percentage = IIF(Growth_Sales_LY1_INDEX > 0, Growth_Sales_CY/Growth_Sales_LY1_INDEX, IIF(Growth_Sales_CY > 0, 9.9999, 0))
	WHERE Growth_Sales_CY > 0

	-- Set the Liberty reward percentage
	UPDATE @TEMP SET Lib_Reward_Percentage = IIF(QualBrand_Sales_CY >= 2000000, 0.06, 0.04) WHERE Growth_Percentage + @ROUNDING_PERCENT >= 1.00
	UPDATE @TEMP SET Lib_Reward_Percentage = IIF(QualBrand_Sales_CY >= 2000000, 0.03, 0.02) WHERE Lib_Reward_Percentage = 0

	-- Set the Centurion reward percentage
	UPDATE @TEMP SET Cent_Reward_Percentage = IIF(QualBrand_Sales_CY >= 2000000, 0.03, 0.02) WHERE Growth_Percentage + @ROUNDING_PERCENT >= 1.00
	UPDATE @TEMP SET Cent_Reward_Percentage = 0.01 WHERE Cent_Reward_Percentage = 0

	-- Set the Liberty and Centurion reward
	UPDATE @TEMP
		SET Lib_Reward = IIF(Lib_Reward_Sales > 0, Lib_Reward_Sales * Lib_Reward_Percentage, 0),
			Cent_Reward = IIF(Cent_Reward_Sales > 0, Cent_Reward_Sales * Cent_Reward_Percentage, 0)
	WHERE Lib_Reward_Sales > 0 OR Cent_Reward_Sales > 0


	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		UPDATE T1
		SET [Lib_Reward] = [Lib_Reward] * ISNULL(EI1.FieldValue/100,1),
			[Cent_Reward] = [Cent_Reward] * ISNULL(EI2.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI1
			ON EI1.StatementCode=T1.StatementCode AND EI1.MarketLetterCode=T1.MarketLetterCode  AND EI1.FieldCode='Radius_Percentage_Liberty'

			LEFT JOIN RPWeb_User_EI_FieldValues EI2
			ON EI2.StatementCode=T1.StatementCode AND EI2.MarketLetterCode=T1.MarketLetterCode  AND EI2.FieldCode='Radius_Percentage_Centurion'
		WHERE [Lib_Reward] > 0 OR [Cent_Reward] > 0
	END

	-- Set the total reward
	UPDATE @TEMP SET Reward = Lib_Reward + Cent_Reward WHERE Lib_Reward > 0 OR Cent_Reward > 0

	-- Copy the records from the temporary table into the permanent table
	DELETE FROM RP2021_DT_Rewards_W_RetailBonus WHERE StatementCode IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_RetailBonus(StatementCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1,Growth_Sales_LY1_INDEX,Growth_Sales_LY2,
												Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, QualBrand_Sales_CY, Growth_Sales_CY, Growth_Sales_LY1,Growth_Sales_LY1_INDEX,Growth_Sales_LY2, Lib_Reward_Sales,
		   Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward
	FROM @TEMP
END
