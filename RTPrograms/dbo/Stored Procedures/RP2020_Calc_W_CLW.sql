﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_W_CLW]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	--This reward is only for Nutrien D520062427

	/*
	Reward Opportunity
	Clearfield Commitment Reward
	Distributors can earn $200/ grower, for newly signed evergreen Clearfield Commitments for Wheat, signed and submitted on or before February 29, 2020.
	Distributors can earn $100/ grower, for new Commitments for Wheat signed and submitted between March 1, 2020 and October 8, 2020
	 Matching Acres Reward
	Distributors can earn 7.5% reward on matching acres of Altitude FX herbicides sold to Clearfield wheat growers who have a Clearfield commitment received on or before October 8, 2020.

	*/
	
	DECLARE @CommitmentReward AS TABLE (
		Statementcode INT NOT NULL,
		MarketLetterCode VARCHAR(50),
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		CommitmentDate  Date not null,
		Req_ReceivedByDate BIT NOT NULL DEFAULT 0,
		RewardType varchar(50) NOT NULL DEFAULT '',
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC,
			Retailercode ASC,
			Farmcode ASC
		)
	)


	DECLARE @FarmsWithCommitment TABLE (
		StatementCode INT NOT NULL,
		Farmcode VARCHAR(20) NOT NULL
	)

	DECLARE @MatchingPriority TABLE (
		ChemicalGroup VARCHAR(65),
		[Priority] INT,		
		PRIMARY KEY CLUSTERED (
			ChemicalGroup ASC
		)
	)
	
	INSERT INTO @MatchingPriority(ChemicalGroup,[Priority])
	VALUES('ALTITUDE FX2',1)
	,('ALTITUDE FX3',1)
	,('ALTITUDE FX',2)
	   		
	DECLARE @REWARD_PRECENTAGE FLOAT=0.075; 
	DECLARE @CUT_OFF_DATE DATE = CAST('2020-02-29' AS DATE)
	   
	-- COMMITMENT REWARD
	INSERT INTO @CommitmentReward(Statementcode,MarketLetterCode,RetailerCode,Farmcode,CommitmentDate)
	SELECT ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode
		,MIN(CAST(C.CommitmentDate AS DATE)) AS CommitmentDate			
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_Commitments C 			ON ST.StatementCode=C.StatementCode 		
	WHERE MLP.ProgramCode=@PROGRAM_CODE AND c.[Status]='Complete' AND c.BASFSeason = @SEASON AND c.CommitmentType='Wheat'  
	GROUP BY ST.Statementcode, MLP.MarketLetterCode, C.RetailerCode, C.FarmCode
	
	UPDATE T1
	SET Req_ReceivedByDate=1
	FROM @CommitmentReward	 T1
		INNER JOIN (			
			SELECT ST.Statementcode,  MLP.MarketLettercode,  tx.Retailercode, tx.FarmCode
			FROM @STATEMENTS ST
				INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
				INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
			WHERE MLP.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND tx.InRadius=1 AND tx.ChemicalGroup='CLEARFIELD WHEAT' AND ISNULL(tx.CFStatus,'-')  <> '-'
				AND CAST(tx.InvoiceDate AS DATE) <= CAST('2020-07-09' AS DATE)
			GROUP BY ST.Statementcode,  MLP.MarketLettercode,  tx.Retailercode, tx.FarmCode
			HAVING SUM(Acres) > 0
		) T2
		ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.RetailerCode=T1.Retailercode AND T2.FarmCode=T1.FarmCode

	UPDATE @CommitmentReward	
	SET RewardType=IIF(CommitmentDate <= @CUT_OFF_DATE,'Early Commitment','Late Commitment')

	UPDATE @CommitmentReward	
	SET Reward=IIF(CommitmentDate <= @CUT_OFF_DATE,200,100)
	--WHERE Req_ReceivedByDate=1
		

	-- MATCHING CLEARFIELD HERBICIDES REWARD
	--on Altitude FX 
				
	-- CLEARFIELD WHEAT COMMITED ACRES

	INSERT INTO @FarmsWithCommitment (StatementCode, FarmCode)
	SELECT C.StatementCode, C.FarmCode
	FROM RP_DT_Commitments  C
		INNER JOIN @Statements S
		ON S.StatementCode=C.StatementCode
	WHERE C.[Status]='Complete'  AND C.[CommitmentType]='Wheat'
	GROUP BY C.StatementCode, C.FarmCode
	
	DROP TABLE IF EXISTS #Wheat
	SELECT ST.Statementcode, MLP.MarketLettercode, s.Retailercode, tx.FarmCode	,SUM(Acres) AS [Acres]						
	INTO #Wheat
	FROM @FarmsWithCommitment ST		
		INNER JOIN RP_Statements S				ON S.StatementCode=ST.StatementCode
		INNER JOIN RP_DT_ML_ELG_Programs MLP	ON MLP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT StatementCode, Farmcode, ISNULL(Acres,0) AS Acres
			FROM RP_DT_Transactions 
			WHERE BASFSeason=@SEASON AND InRadius=1 AND ChemicalGroup='CLEARFIELD WHEAT' AND CFStatus='Complete'
		) TX		
		ON TX.StatementCode=ST.StatementCode AND TX.FarmCode=ST.Farmcode
	WHERE MLP.ProgramCode=@PROGRAM_CODE 
	GROUP BY ST.Statementcode,  MLP.MarketLettercode, s.Retailercode, tx.FarmCode
	   
	-- TEMP Altitude Sales
	DROP TABLE IF EXISTS #TEMP_Altitude_SALES

	SELECT ST.Statementcode,  S.StatementLevel, GR.MarketLetterCode, s.Retailercode, tx.FarmCode, tx.ChemicalGroup   -- s.Retailercode used to be tx.retailercode
		,tx.Price_SWP AS [Sales]						
		,tx.Acres
		,M.[Priority]  AS MatchingOrder
		,TX.RetailerCode AS EffectiveRetailerCode
	INTO #TEMP_Altitude_SALES
	FROM (
			SELECT DISTINCT StatementCode FROM @FarmsWithCommitment 
		) ST
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Statements S				ON TX.StatementCode=S.StatementCode
		INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
		INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID
		INNER JOIN @MatchingPriority M			ON M.ChemicalGroup=TX.ChemicalGroup
	WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD WHEAT'
	
	DELETE T1
	FROM #TEMP_Altitude_SALES T1
		LEFT JOIN @FarmsWithCommitment T2
		ON T2.StatementCode=T1.StatementCode AND T2.FarmCode=T1.Farmcode
	WHERE T2.Farmcode IS NULL

	-- OU AND HEAD OFFICE BREAK DOWN
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN (
			SELECT DISTINCT StatementCode 
			FROM 	@FarmsWithCommitment 
		) ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT tx.Statementcode, tx.MarketLetterCode, @PROGRAM_CODE, tx.EffectiveRetailerCode, tx.Farmcode,SUM(tx.Sales) AS Sales, 1		
	FROM #TEMP_Altitude_SALES tx					
	WHERE tx.StatementLevel IN ('LEVEL 2','LEVEL 5')
	GROUP BY tx.Statementcode, tx.MarketLetterCode, tx.EffectiveRetailerCode, tx.Farmcode


	DROP TABLE IF EXISTS #ALTITUDE
	SELECT Statementcode, MarketLetterCode, RetailerCode, FarmCode, ChemicalGroup 
		,SUM(Sales) AS [Sales]
		,SUM(Acres) AS [Acres]
		,MAX(MatchingOrder) AS MatchingOrder
		,CAST(0 AS DECIMAL(18,2)) AS MatchedAcres
		,CAST(0 AS MONEY) AS MatchedSales
		,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
		,CAST(0 AS MONEY) AS Reward
	INTO #ALTITUDE
	FROM #TEMP_Altitude_SALES
	GROUP BY Statementcode, MarketLetterCode, RetailerCode, FarmCode, ChemicalGroup


	/*
	-- Altitude SALES  SWP Pricing
	DROP TABLE IF EXISTS #ALTITUDE
	SELECT ST.Statementcode, GR.MarketLetterCode, tx.Retailercode, tx.FarmCode, tx.ChemicalGroup 
		,SUM(tx.Price_SWP) AS [Sales]						
		,SUM(tx.Acres) AS [Acres]
		,MAX(M.[Priority])  AS MatchingOrder
		,CAST(0 AS DECIMAL(18,2)) AS MatchedAcres
		,CAST(0 AS MONEY) AS MatchedSales
		,CAST(0 AS DECIMAL(6,4)) AS Reward_Percentage
		,CAST(0 AS MONEY) AS Reward
	INTO #ALTITUDE
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_Transactions TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_Config_Groups_Brands BR	ON BR.ChemicalGroup=tx.ChemicalGroup
		INNER JOIN RP_Config_Groups GR			ON GR.GroupID=BR.GroupID
		INNER JOIN @MatchingPriority M			ON M.ChemicalGroup=TX.ChemicalGroup
	WHERE GR.ProgramCode=@PROGRAM_CODE AND tx.BASFSeason=@SEASON AND  tx.InRadius=1 AND GR.GroupType='REWARD_BRANDS'  AND tx.ChemicalGroup <> 'CLEARFIELD WHEAT'
	GROUP BY ST.Statementcode, GR.MarketLetterCode, tx.Retailercode, tx.FarmCode, tx.ChemicalGroup
	*/



	/*
	--ORDER ALTITUDE FX BY MOST ACRES  -- RP-2500
	UPDATE T1 SET MatchingOrder = T2.MatchPriority
	FROM #ALTITUDE T1
	INNER JOIN (
		SELECT StatementCode
			   ,MatchPriority = RANK () OVER( ORDER BY [Acres]) FROM #ALTITUDE 
	) T2 ON T1.StatementCode = T2.StatementCode
	*/

	-- LETS DO Altitude FX MATCHING
	UPDATE T1
	SET MatchedAcres = IIF(T2.[RunningTotal] <= CLW.Acres, T1.Acres,T1.Acres+(CLW.[Acres]-T2.[RunningTotal]))
	FROM #ALTITUDE T1
		INNER JOIN #Wheat CLW
		ON CLW.Statementcode=T1.StatementCode AND CLW.MarketLetterCode=T1.MarketLetterCode AND CLW.FarmCode=T1.FarmCode -- AND CLW.RetailerCode=T1.RetailerCode
		INNER JOIN (
			SELECT [StatementCode],[RetailerCode],[Farmcode],[ChemicalGroup]
				,SUM(Acres) OVER(PARTITION BY [StatementCode],[RetailerCode],[Farmcode] ORDER BY [MatchingOrder] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  AS [RunningTotal] 
			FROM #ALTITUDE
		) T2
		ON T2.Statementcode=T1.StatementCode AND T2.RetailerCode=T1.RetailerCode AND T2.FarmCode=T1.FarmCode AND T2.ChemicalGroup=T1.ChemicalGroup
	WHERE (T2.[RunningTotal] <= CLW.Acres  OR T1.Acres+(CLW.[Acres]-T2.[RunningTotal]) > 0)


	-- UPDATE MATCHING SALES, REWARD PERCENTAGE AND REWARD
	UPDATE #ALTITUDE SET [MatchedSales]=[Sales] * ([MatchedAcres]/[Acres]) WHERE [Acres] > 0
	UPDATE #ALTITUDE SET [Reward_Percentage]=@REWARD_PRECENTAGE,  [Reward]=[MatchedSales] * @REWARD_PRECENTAGE	WHERE [MatchedSales] > 0

	-- LETS INSERT DATA INTO TABLES FROM TEMP TABLES		
	DELETE FROM RP2020_DT_Rewards_W_CLW_Comm WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CLW_Comm(Statementcode, MarketLetterCode, ProgramCode, Retailercode, Farmcode, CommitmentDate, Req_ReceivedByDate, RewardType, Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode,  CommitmentDate, Req_ReceivedByDate, RewardType, Reward
	FROM @CommitmentReward

	DELETE FROM RP2020_DT_Rewards_W_CLW_Herb WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CLW_Herb(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand,Acres,Sales,MatchedAcres,MatchedSales,Reward_Percentage,Reward)
	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode, ChemicalGroup as RewardBrand, Acres, Sales, MatchedAcres, MatchedSales, Reward_Percentage, Reward
	FROM #ALTITUDE
		
		UNION

	SELECT StatementCode, MarketLetterCode, @PROGRAM_CODE, RetailerCode, FarmCode,'CLEARFIELD WHEAT' as RewardBrand, Acres, 0 as Sales, 0 as MatchedAcres, 0 as MatchedSales, 0 as Reward_Percentage, 0 as Reward
	FROM #Wheat
	   
END
