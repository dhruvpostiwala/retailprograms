﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_InvLibertyLoyaltyBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'REWARD PLANNER'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_INV_LIB_LOY_BONUS'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (8866)
	*/

	DECLARE @TEMP TABLE (
		StatementCode INT,
		Level5Code VARCHAR(50) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,		
		Liberty_Acres NUMERIC(18,2) NOT NULL,
		Invigor_Acres NUMERIC(18,2) NOT NULL,
		Total_Invigor_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		Total_Reseeded_Acres NUMERIC(18,2) NOT NULL DEFAULT 0,
		RewardBrand VARCHAR(100) NOT NULL,				
		RewardBrand_Sales MONEY NOT NULL,
		Bonus_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Bonus_Reward MONEY NOT NULL DEFAULT 0,
		Addendum_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Addendum_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	DECLARE @HEATADDENDUM_QUALIFY TABLE (
		StatementCode INT NOT NULL,
		Part1 BIT NOT NULL DEFAULT 1,
		Part2 BIT NOT NULL DEFAULT 1,
		AddendumOn BIT NOT NULL DEFAULT 0
	)

	IF NOT OBJECT_ID('tempdb..#SUMMARY') IS NULL DROP TABLE #SUMMARY
	SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode,
		SUM(IIF(TX.GroupLabel = 'LIBERTY',tx.Acres_CY,0)) AS Liberty_Acres,
		SUM(IIF(TX.GroupLabel = 'LIBERTY',tx.Price_CY,0)) AS Liberty_Sales,
		SUM(IIF(TX.GroupLabel = 'INVIGOR',tx.Acres_CY,0)) AS Invigor_Acres,
		SUM(IIF(TX.GroupLabel = 'INVIGOR',tx.Price_CY,0)) AS Invigor_Sales,
		SUM(IIF(TX.GroupLabel = 'HEAT LQ',tx.Price_CY,0)) AS Heat_CY_Sales,
		SUM(IIF(TX.GroupLabel = 'HEAT LQ',tx.Price_LY1,0)) AS Heat_LY_Sales,
		MAX(tx.GroupLabel) RewardBrand,
		MAX(tx.Level5Code) Level5Code,
		MAX(RP.StatementLevel) StatementLevel,
		100 AS Radius_Percentage
	INTO #SUMMARY
	FROM @STATEMENTS ST
		INNER JOIN RP2022_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN RP_STATEMENTS RP	ON TX.StatementCode = RP.StatementCode
	WHERE tx.ProgramCode = @PROGRAM_CODE AND tx.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel, TX.ProductCode

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Get editable input radius percentages
		UPDATE T1
		SET Radius_Percentage = CASE GroupLabel WHEN  'INVIGOR' THEN ISNULL(E1.FieldValue, 100) WHEN 'LIBERTY' THEN ISNULL(E2.FieldValue, 100) WHEN 'HEAT LQ' THEN ISNULL(E3.FieldValue, 100) ELSE ISNULL(E3.FieldValue, 100) END 
		FROM #SUMMARY T1
		INNER JOIN RPWeb_User_EI_FieldValues E1	ON  E1.StatementCode = T1.StatementCode AND E1.MarketLetterCode = T1.MarketLetterCode AND E1.FieldCode = 'Radius_Percentage_InVigor'
		INNER JOIN RPWeb_User_EI_FieldValues E2	ON  E2.StatementCode = T1.StatementCode AND E2.MarketLetterCode = T1.MarketLetterCode AND E2.FieldCode = 'Radius_Percentage_Liberty'
		INNER JOIN RPWeb_User_EI_FieldValues E3	ON  E3.StatementCode = T1.StatementCode AND E3.MarketLetterCode = T1.MarketLetterCode AND E3.FieldCode = 'Radius_Percentage_CPP'

		-- NEW: Apply Reward Planner %s
		-- Apply radius percentages
	   	UPDATE T1
		SET Liberty_Acres = Liberty_Acres * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Liberty_Sales = Liberty_Sales * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Invigor_Acres = Invigor_Acres * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Invigor_Sales = Invigor_Sales * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1),
			Heat_CY_Sales = Heat_CY_Sales * (Radius_Percentage/100) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1), 
			Heat_LY_Sales = Heat_LY_Sales * (Radius_Percentage/100)
		FROM #SUMMARY T1
		INNER JOIN ProductReference PR ON T1.ProductCode = PR.ProductCode
		LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = PR.ChemicalGroup
		LEFT JOIN RPWeb_User_BrandInputs BI2 ON BI2.StatementCode = T1.StatementCode AND BI2.ChemicalGroup = PR.Product
		--LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = T1.StatementCode AND BI.ChemicalGroup = T1.GroupLabel

	END

	-- Sum at the group level for the reward calculation
	IF NOT OBJECT_ID('tempdb..#SUMMARY_TOTALS') IS NULL DROP TABLE #SUMMARY_TOTALS
	SELECT StatementCode, MarketLetterCode, ProgramCode, GroupLabel,
		SUM(Liberty_Acres) AS Liberty_Acres, SUM(Liberty_Sales) AS Liberty_Sales, SUM(Invigor_Acres) AS Invigor_Acres, SUM(Invigor_Sales) AS Invigor_Sales, SUM(Heat_CY_Sales) AS Heat_CY_Sales, SUM(Heat_LY_Sales) AS Heat_LY_Sales,
		MAX(RewardBrand) AS RewardBrand, MAX(Level5Code) AS Level5Code, MAX(StatementLevel) AS StatementLevel, MAX(Radius_Percentage) AS Radius_Percentage
	INTO #SUMMARY_TOTALS
	FROM #SUMMARY
	GROUP BY StatementCode, MarketLetterCode, ProgramCode, GroupLabel

	INSERT INTO @TEMP(StatementCode, Level5Code, MarketLetterCode, ProgramCode, Liberty_Acres, Invigor_Acres, RewardBrand, RewardBrand_Sales)	
	SELECT ST.StatementCode, S.Level5Code, ELG.MarketLetterCode, ELG.ProgramCode,
		MAX(S.Liberty_Acres) AS Liberty_Acres,
		MAX(S.Invigor_Acres) AS Invigor_Acres,
		IIF(ELG.MarketLetterCode = 'ML2022_W_INV', 'INVIGOR', 'LIBERTY 150') AS RewardBrand,
		MAX(IIF(ELG.MarketLetterCode = 'ML2022_W_INV', S.Invigor_Sales, S.Liberty_Sales)) AS RewardBrand_Sales
	FROM @STATEMENTS ST 
		INNER JOIN RP_DT_ML_Elg_Programs ELG ON ELG.Statementcode = ST.Statementcode
		INNER JOIN #SUMMARY_TOTALS S ON S.Statementcode = ST.Statementcode
	WHERE ELG.ProgramCode = @PROGRAM_CODE
	GROUP BY ST.StatementCode, S.Level5Code, ELG.MarketLetterCode, ELG.ProgramCode --, Level5Code, StatementLevel

	-- LETS ADJUST INVIGOR ACRES BY DEDUCTING RESEEEDED ACRES
	--IF @STATEMENT_TYPE = 'ACTUAL'
	--BEGIN
		--TBD
	--END

	-- Update reward percentage if qualified
	UPDATE @TEMP 
	SET Bonus_Percentage = 0.02,
		Bonus_Reward = RewardBrand_Sales * 0.02
	WHERE Liberty_Acres > 0 AND Invigor_Acres > 0 AND Liberty_Acres >= Invigor_Acres
	 
	-- NEW: RC-4725 - Heat Addendum has been removed
	-- Heat Addendum calculation
	/*
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Check whether each statement qualifies for the heat addendum (update "AddendumOn" to check retailer against filter once filter is available)
		INSERT INTO @HEATADDENDUM_QUALIFY(StatementCode, Part1, Part2)
		SELECT StatementCode,
			IIF(SUM(Heat_LY_Sales) > 0, IIF(SUM(Heat_CY_Sales) / (SUM(Heat_LY_Sales)) >= 1.075, 0, 1), IIF(SUM(Heat_CY_Sales) > 0, 0, 1)) [Part1],
			IIF(SUM(Liberty_Acres) >= (0.75 * SUM(InVigor_Acres)), 0, 1) [Part2]
		FROM #SUMMARY_TOTALS
		GROUP BY StatementCode

		UPDATE HAQ
		SET AddendumOn = IIF(EI.FieldValue = 100, 1, 0)
		FROM @HEATADDENDUM_QUALIFY HAQ
		INNER JOIN RPWeb_User_EI_FieldValues EI ON HAQ.StatementCode = EI.StatementCode AND EI.FieldCode = 'Heat_Addendum'

		-- If the heat addendum is enabled for a retailer, and the retailer does not currently qualify for a reward, qualify the retailer for the reward
		UPDATE T1
		SET Addendum_Percentage = IIF(MarketLetterCode = 'ML2022_W_INV', 0.02, IIF(Level5Code = 'D000001', 0.0500, 0.0545)),
			Addendum_Reward = RewardBrand_Sales * IIF(MarketLetterCode = 'ML2022_W_INV', 0.02, IIF(Level5Code = 'D000001', 0.0500, 0.0545))
		FROM @TEMP T1
			INNER JOIN @HEATADDENDUM_QUALIFY T2 ON T1.StatementCode = T2.StatementCode
		WHERE T2.Part1 = 0 AND T2.Part2 = 0 AND T2.AddendumOn = 1 AND T1.Bonus_Reward = 0

	END
	*/

	-- Set the final reward to the sum of the bonus and addendum rewards
	UPDATE @TEMP
	SET [Reward] = [Bonus_Reward] + [Addendum_Reward]

	DELETE FROM RP2022_DT_Rewards_W_INV_LLB WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2022_DT_Rewards_W_INV_LLB(StatementCode,MarketLetterCode,ProgramCode,Liberty_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Bonus_Percentage,Bonus_Reward,Addendum_Percentage,Addendum_Reward,Reward,Total_Invigor_Acres,Total_Reseeded_Acres)
	SELECT StatementCode,MarketLetterCode,ProgramCode,Liberty_Acres,Invigor_Acres,RewardBrand,RewardBrand_Sales,Bonus_Percentage,Bonus_Reward,Addendum_Percentage,Addendum_Reward,Reward,Total_Invigor_Acres,Total_Reseeded_Acres
	FROM @TEMP

END
