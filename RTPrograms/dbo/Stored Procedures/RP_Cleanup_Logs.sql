﻿
CREATE PROCEDURE [dbo].[RP_Cleanup_Logs]  
AS
BEGIN
	SET NOCOUNT ON;

	DELETE [dbo].[RP_ActivityLog] WHERE ActivityDate <  DATEADD(day, -14, GETDATE()) 
	DELETE [dbo].[RP_ExecutionTimeLog] WHERE StartTime < DATEADD(day, -14, GETDATE()) 
	
END
