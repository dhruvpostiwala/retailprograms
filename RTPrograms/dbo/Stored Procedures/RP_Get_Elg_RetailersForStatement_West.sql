﻿


CREATE PROCEDURE [dbo].[RP_Get_Elg_RetailersForStatement_West] @SEASON INT, @STATEMENT_TYPE AS [VARCHAR](20)
AS
BEGIN
	SET NOCOUNT ON;

	/*		
		D000001  AND ISRCAllocating=1 - Independent West (RC REP) 
		D0000117 - FCL (BR)

		Line Companies - No Forecast Statements (BR)
		RetailerCode	RetailerName
			D0000107	CARGILL LIMITED - RETAIL WEST
			D0000137	RICHARDSON PIONEER LTD
			D0000244	UNITED FARMERS OF ALBERTA
			D520062427	NUTRIEN AG SOLUTIONS CANADA

	*/

		
	DECLARE @REGION VARCHAR(4)='WEST';	
	DECLARE @EligibleRetailers AS UDT_RP_RETAILERS_LIST;
	DECLARE @SEASON_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-01' AS DATE)		
	DECLARE @SEASON_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)		

	DECLARE @SOLD_TO_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-02' AS DATE)		
	--DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-01' AS DATE)		
	DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)

	DECLARE @ValidAccountTypes Table ( 
		RetailerCode [Varchar](20) NOT NULL,	
	PRIMARY KEY CLUSTERED (
			[RetailerCode] ASC		
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)

	DECLARE @IND_MAPPING TABLE(
		Level2Code Varchar(20) NOT NULL,
		MappingLevel Varchar(10) NOT NULL
	)

	
	/* ==================================== FIRST STEP =========================================================== */
	-- GET RETAILERCODES WITH VALID ACCOUNTTYPES (DEFINED IN CONFIG TABLE)
	INSERT INTO @ValidAccountTypes(RetailerCode)
	SELECT DISTINCT T1.RetailerCode
	FROM RetailerProfileAccountType T1
		INNER JOIN RP_Config_RetailerAccountTypes CFG
		ON CFG.AccountType=T1.AccountType					
	WHERE CFG.Season=@Season AND CFG.Region=@Region 
		
	
	/* ==================================== NEXT STEP =========================================================== */
	-- GET RETAILERCODES ELIGIBLE FOR STATEMENTS 
	-- FCL / COOP (BR) D0000117
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
		,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
		,RP.[Status] 
		,IIF(RP.RetailerType='LEVEL 1' AND RP.Level2Code='','LEVEL 1','LEVEL 2') AS [StatementLevel]		
		,IIF(RP.RetailerType='LEVEL 1' AND RP.Level2Code='','LOCATION','FAMILY') AS [MappingLevel]		
	FROM RetailerProfile RP
		INNER JOIN @ValidAccountTypes ACT
		ON ACT.RetailerCode=RP.RetailerCode
	WHERE RP.Region=@Region AND RP.[Status] IN ('ACTIVE','INACTIVE') 
		AND RP.RetailerName NOT LIKE '%BASF CANADA%' AND RP.RetailerName NOT LIKE '%UNKNOWN RETAIL%' 
		AND RP.Level5Code='D0000117' 
		AND (RetailerType='LEVEL 2' OR (RP.RetailerType='LEVEL 1' AND RP.Level2Code=''))

	IF @STATEMENT_TYPE='ACTUAL'
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
		,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
		,RP.[Status] 
		,IIF(RP.RetailerType='LEVEL 1' AND RP.Level2Code='','LEVEL 1','LEVEL 2') AS [StatementLevel]		
		,IIF(RP.RetailerType='LEVEL 1' AND RP.Level2Code='','LOCATION','FAMILY') AS [MappingLevel]		
	FROM RetailerProfile RP
		INNER JOIN @ValidAccountTypes ACT
		ON ACT.RetailerCode=RP.RetailerCode
	WHERE RP.Region=@Region AND RP.[Status]='SOLD'
		AND RP.RetailerName NOT LIKE '%BASF CANADA%' AND RP.RetailerName NOT LIKE '%UNKNOWN RETAIL%' 
		AND RP.Level5Code='D0000117' 
		AND (RetailerType='LEVEL 2' OR (RP.RetailerType='LEVEL 1' AND RP.Level2Code=''))		
		AND SoldDate IS NOT NULL
		AND CAST(RP.SoldDate AS Date) >= @SOLD_TO_START_DATE  AND CAST(RP.SoldDate AS Date) <= @SOLD_TO_END_DATE


	DROP TABLE IF EXISTS #IAS_CONFIG_STATEMENTS
	SELECT DISTINCT RetailerCode 
	INTO #IAS_CONFIG_STATEMENTS
	FROM IAS_Config_Statements 				
	WHERE [Season]=@SEASON AND [Version]=IIF(@SEASON = 2020 , 'July' , 'retail_programs') 

		
	-- RP-2487
	-- >> If Sold To location is the only location within a given retail family identified as a Sold To, the statement is for the entire family.
	-- >> If there is multiple Sold Tos per family, the statement is solely for the Sold To location (individual statements)
	INSERT INTO @IND_MAPPING(Level2Code,MappingLevel)
	SELECT Level2Code, IIF([SoldToCount]=1,'FAMILY','LOCATION') AS MappingLevel
	FROM (
		SELECT RP.Level2Code, COUNT(*) AS [SoldToCount]
		FROM RetailerProfile RP			
			INNER JOIN #IAS_CONFIG_STATEMENTS CFG
			ON CFG.RetailerCode = RP.RetailerCode
		WHERE RP.Region=@Region AND RP.[Status] IN ('ACTIVE','INACTIVE','SOLD') AND RP.RetailerName NOT LIKE '%BASF CANADA%' AND RP.RetailerName NOT LIKE '%UNKNOWN RETAIL%' 
			AND RP.Level5Code='D000001' AND RP.Level2Code <> ''
			AND RP.RetailerCode <> 'R520157196'
			/*
			AND RP.RetailerCode IN (
				SELECT DISTINCT RetailerCode 
				FROM IAS_Config_Statements 				
				WHERE [Season]=@SEASON AND 
					[Version]=IIF(@SEASON = 2020 , 'July' , 'retail_programs') 
			)
			*/
			
		GROUP BY Level2Code
	) D

	-- FOR INDEPENDENTS 
	-- WHICH RETAILER WILL GET A STATEMETNS IS DEPENDENT ON LIST FROM IAS_Config_Statements	
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
		,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
		,RP.[Status]
		,IIF(ISNULL(MAP.MappingLevel,'')='FAMILY','LEVEL 2','LEVEL 1') AS [StatementLevel]
		,IIF(ISNULL(MAP.MappingLevel,'')='FAMILY','FAMILY','LOCATION') AS [MappingLevel]
	FROM RetailerProfile RP
		INNER JOIN #IAS_CONFIG_STATEMENTS CFG
		ON CFG.RetailerCode = RP.RetailerCode
		/*
		INNER JOIN (
			-- FIRST YEAR (2020) WE DO NOT HAVE A VERSION = 'FORECAST' 
			-- VERSION "Forecast" was introduced in IAS_Config_Statements starting 2021. 
			-- VERSION "Retail_Programs" was introduced in IAS_Config_Statements starting 2022. 
			-- Nevertheless, it is not limited to forecast staements when it comes to Retailer Programs, we use the same flag to generate statements in RP for 
			-- Forecast, Projection, Actuals
			SELECT DISTINCT RetailerCode 
			FROM IAS_Config_Statements 
			WHERE [Season]=@SEASON 
				AND [Version]=IIF(@SEASON=2020, 'July', 'retail_programs') -- IIF(@SEASON=2021,'forecast','retail_programs')
			--AND [Version]=IIF(@SEASON=2020, 'July','forecast')  
		) CFG
		ON CFG.RetailerCode=RP.RetailerCode
		*/
		INNER JOIN (
			SELECT DISTINCT Retailercode 
			FROM RetailerAccess 
			WHERE ClassificationType='RC Territory'
		) RA			
		ON RA.RetailerCode=RP.RetailerCode
		INNER JOIN @ValidAccountTypes ACT		ON ACT.RetailerCode=RP.RetailerCode
		LEFT JOIN @IND_MAPPING MAP				ON MAP.Level2Code=RP.Level2Code
	WHERE RP.Region=@Region AND RP.[Status] IN ('ACTIVE','INACTIVE','SOLD') AND RP.RetailerName NOT LIKE '%BASF CANADA%' AND RP.RetailerName NOT LIKE '%UNKNOWN RETAIL%' 
		AND RP.Level5Code='D000001' 

	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
		SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
			,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
			,RP.[Status]
			,'LEVEL 1'  AS [StatementLevel]
			,'LOCATION' AS [MappingLevel]
		FROM RetailerProfile RP
			INNER JOIN @EligibleRetailers E
			ON E.RetailerCode=RP.SoldRetailerCode
		WHERE RP.Level5Code='D000001' AND RP.Level2Code='' 			
			AND CAST(RP.SoldDate AS Date) >= @SOLD_TO_START_DATE  AND CAST(RP.SoldDate AS Date) <= @SOLD_TO_END_DATE

			UNION

		SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
			,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
			,RP.[Status]
			,'LEVEL 2'  AS [StatementLevel]
			,'FAMILY' AS [MappingLevel]
		FROM RetailerProfile RP
			INNER JOIN @EligibleRetailers E
			ON E.RetailerCode=RP.SoldRetailerCode
		WHERE RP.Level5Code='D000001' AND RP.Level2Code <> ''
			AND CAST(RP.SoldDate AS Date) >= @SOLD_TO_START_DATE  AND CAST(RP.SoldDate AS Date) <= @SOLD_TO_END_DATE
	END  

	-- EXCEPTION  -- JIRA: RP-2487 -- Aligning RP statements with RC for Recon
	IF @SEASON IN (2020,2021)
		UPDATE @EligibleRetailers
		SET [StatementLevel]='LEVEL 2',[MappingLevel]='FAMILY' 		
		WHERE RetailerCode IN ('R520157196','R0001120')
			

	/* ==================================== NEXT STEP =========================================================== */
	-- IN CASE OF FORECAST AND PROJECTION STATEMENT TYPES, NO NEED TO CREATE STATEMENTS FOR INACTIVE AND SOLD RETAILERS 
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
		DELETE FROM @EligibleRetailers WHERE [Status] IN ('INACTIVE','SOLD')
	ELSE IF @STATEMENT_TYPE = 'ACTUAL' -- NO NEED TO CREATE STATEMENTS FOR INACTIVE AND SOLD RETAILERS HAVING NO TRANSACTIONS IN CASE OF ACTUAL STATEMENTS TYPE
		DELETE ELG
		FROM @EligibleRetailers ELG
			LEFT JOIN (
				SELECT RetailerCode 
				FROM GrowerTransactionsBASFDetails 
				WHERE BASFSeason=@SEASON
				GROUP BY RetailerCode
			) TX
			ON TX.RetailerCode=ELG.RetailerCode
		WHERE ELG.[Status]='INACTIVE' AND TX.RetailerCode IS NULL
			

	/* ==================================== LEVEL 5 HEAD OFFICE PAYMENT STATEMENTS =========================================================== */
	IF @STATEMENT_TYPE='ACTUAL'
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
		,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
		,RP.[Status]		
		,'LEVEL 5' AS [StatementLevel]
		,IIF(HO.Summary='No','NONE','FAMILY') AS [MappingLevel]		
	FROM RetailerProfile RP
		INNER JOIN RP_Config_HOPaymentLevel HO
		ON HO.Level5Code=RP.RETAILERCODE
	WHERE RP.REGION=@REGION AND HO.SEASON=@SEASON AND RP.RetailerType='LEVEL 5'

	
	DELETE T1
	FROM @EligibleRetailers T1
		INNER JOIN RetailerProfile RP
		ON RP.SoldRetailerCode=T1.Retailercode
	WHERE CAST(RP.SoldDate AS Date) = @SOLD_TO_END_DATE

	SELECT * FROM @EligibleRetailers	
END
