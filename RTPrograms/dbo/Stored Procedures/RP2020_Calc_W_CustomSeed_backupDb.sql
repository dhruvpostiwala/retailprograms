﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_W_CustomSeed_backupDb]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @EPSILON DECIMAL(4,4)=dbo.SVF_GetEpsilon()
	/*
	Retailers will receive 22% on POG Sales of Insure Cereal and Insure Cereal FX4 and/ or Insure Pulse
	that are used to custom treat seed in the 2020 Growing Season.

	Retailers will receive 8% on POG Sales of Nodulator PRO used to custom pre-inoculate soybean seed in
	the 2020 Growing Season.

	Minimum purchase:
	- 450 Litres of Insure Cereal and Insure Cereal FX4 seed treatments
	- 360 Litres of Insure Pulse seed treatment
	- $20,000 of Nodulator PRO
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 2
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_CUSTOM_SEED_TREATING_REWARD'

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,				
		RewardBrand_Sales MONEY NOT NULL,	
		RewardBrand_Acres Numeric(18,2) NOT NULL,		
		RewardBrand_Litres Numeric(18,2) NOT NULL, 				
		CustomApp_Sales MONEY NOT NULL DEFAULT 0,	
		CustomApp_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		CustomApp_Litres Numeric(18,2) NOT NULL DEFAULT 0,				
		Rewardable_Sales MONEY NOT NULL DEFAULT 0, 		
		Rewardable_Acres Numeric(18,2) NOT NULL DEFAULT 0,
		Rewardable_Litres Numeric(18,2) NOT NULL DEFAULT 0,		
		Reward_Percentage Numeric(3,2) NOT NULL DEFAULT 0,
		Reward MONEY  NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

/*
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,RewardBrand_Acres,RewardBrand_Litres)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
		,SUM(tx.Price_CY) AS RewardBrand_Sales
		,SUM(tx.Acres_CY) AS RewardBrand_Acres		
		,SUM(IIF(tx.GroupLabel='Nodulator PRO',0,(tx.Acres_CY/pr.ConversionW) * PR.Volume)) AS [RewardBrand_Litres]
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR					ON PR.Productcode=TX.ProductCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' 
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
*/	   

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN	   			   		
	
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,RewardBrand_Acres,RewardBrand_Litres)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, IIF(tx.GroupLabel IN ('INSURE CEREAL','INSURE CEREAL FX4'),'INSURE CEREAL',tx.GroupLabel) AS RewardBrand
			,SUM(tx.Price_CY) AS RewardBrand_Sales
			,SUM(tx.Acres_CY) AS RewardBrand_Acres		
			,SUM(IIF(tx.GroupLabel='Nodulator PRO',0,(tx.Acres_CY/pr.ConversionW) * PR.Volume)) AS [RewardBrand_Litres]
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR					ON PR.Productcode=TX.ProductCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' 
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,IIF(tx.GroupLabel IN ('INSURE CEREAL','INSURE CEREAL FX4'),'INSURE CEREAL',tx.GroupLabel)

		UPDATE T1
		SET Rewardable_Sales=RewardBrand_Sales * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Insure_Cereal'
		WHERE RewardBrand = 'INSURE CEREAL'

		UPDATE T1
		SET Rewardable_Sales=RewardBrand_Sales * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Insure_Pulse'
		WHERE RewardBrand = 'INSURE PULSE'

		UPDATE T1
		SET Rewardable_Sales=RewardBrand_Sales * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.ProgramCode=T1.ProgramCode AND EI.FieldCode='Nodulator_Pro'
		WHERE RewardBrand = 'NODULATOR PRO'

		-- have to confirm if the minimums are required for forecast
		/*
		UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE CEREAL' AND [RewardBrand_Acres] >= 3674	-- minimum 450L sold of 2 X 9.8 L CASE
		UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE PULSE'  AND [RewardBrand_Acres] >= 2939	-- minimum 360L sold of 2 X 9.8 L CASE		
		*/
		
		UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE CEREAL' AND [RewardBrand_Litres]+@EPSILON >= 450	-- minimum 450L sold of 2 X 9.8 L CASE
		UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE PULSE'  AND [RewardBrand_Litres]+@EPSILON >= 360	-- minimum 360L sold of 2 X 9.8 L CASE
		UPDATE @TEMP SET [Reward_Percentage] = 0.08 WHERE [RewardBrand] = 'NODULATOR PRO' AND [RewardBrand_Sales]+@EPSILON >= 20000	-- minimum 20K of Nodulator PRO

		-- update reward
		UPDATE @TEMP SET [Reward] = [Rewardable_Sales] * [Reward_Percentage]  WHERE [Reward_Percentage] > 0

		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END


	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,RewardBrand_Acres,RewardBrand_Litres)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode ,pr.ChemicalGroup
			,SUM(tx.Price_CY) AS RewardBrand_Sales
			,SUM(tx.Acres_CY) AS RewardBrand_Acres		
			,SUM(IIF(tx.GroupLabel='Nodulator PRO',0,(tx.Acres_CY/pr.ConversionW) * PR.Volume)) AS [RewardBrand_Litres]
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX		ON TX.StatementCode=ST.StatementCode
			INNER JOIN ProductReference PR					ON PR.Productcode=TX.ProductCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' 
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode ,pr.ChemicalGroup

		-- FOR ACTUAL STATEMENTS USE CUSTOM SEED APP DATA 
		UPDATE T1
		SET CustomApp_Acres=T2.Acres
			,CustomApp_Litres=IIF(T1.RewardBrand='Nodulator PRO',0,T2.Litres)
			,CustomApp_Sales=T2.Sales
		FROM @Temp T1
			INNER JOIN (
				SELECT ST.Statementcode ,PR.ChemicalGroup AS RewardBrand
					--,IIF(PR.ChemicalGroup='INSURE CEREAL FX4','INSURE CEREAL',PR.ChemicalGroup)  AS RewardBrand
					,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',AcresPerJug,IIF(cap.PackageSize='Litres',PR.AcresPerLitre,PR.ConversionW))) AS [Acres]		
					,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',PR.LitresPerJug,IIF(cap.PackageSize='Litres',1,PR.Volume))) [Litres]																	
					,SUM(cap.Quantity * IIF(cap.PackageSize='Jugs',PR.PricePerJug,IIF(cap.PackageSize='Litres',PR.PricePerLitre,PR.SDP))) [Sales]																	
				FROM @STATEMENTS ST
					INNER JOIN RP_DT_CustomAppProducts CAP 	ON CAP.Statementcode=ST.Statementcode
					INNER JOIN (					
						SELECT PR1.Productcode, ChemicalGroup, ConversionW, Jugs,  Volume
							,ConversionW/Jugs AS AcresPerJug
							,ConversionW/Volume as AcresPerLitre
							,Volume/Jugs AS LitresPerJug						
							,PRP.SDP
							,PRP.SDP/Jugs AS PricePerJug
							,PRP.SDP/Volume AS PricePerLitre
						FROM ProductReference PR1
							INNER JOIN ProductReferencePricing PRP
							ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'
							
						WHERE Chemicalgroup in ('INSURE PULSE','INSURE CEREAL','INSURE CEREAL FX4')  OR Product='Nodulator PRO 100'
					) PR			
					ON PR.ProductCode=CAP.ProductCode				
				GROUP BY ST.Statementcode, PR.ChemicalGroup
					--, IIF(PR.ChemicalGroup='INSURE CEREAL FX4','INSURE CEREAL',PR.ChemicalGroup)			
			) T2
			ON T2.Statementcode=T1.Statementcode AND T2.RewardBrand=T1.RewardBrand		
		WHERE T2.[Acres] > 0

		-- UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE CEREAL' AND [CustomApp_Acres] > 0 AND [RewardBrand_Litres]+@EPSILON >= 450		-- minimum 450L sold of 2 X 9.8 L CASE
		-- minimum 450L sold of 2 X 9.8 L CASE
		UPDATE T1
		SET [Reward_Percentage] = 0.22 
		FROM @TEMP T1
			INNER JOIN (
				SELECT  StatementCode
					,SUM([RewardBrand_Litres]) AS [RewardBrand_Litres]
					--,SUM([CustomApp_Acres]) AS [CustomApp_Acres]
				FROM @TEMP
				WHERE RewardBrand IN ('INSURE CEREAL','INSURE CEREAL FX4')
				GROUP BY Statementcode
			) T2
			ON T2.Statementcode=T1.Statementcode
		WHERE T1.[RewardBrand] IN ('INSURE CEREAL','INSURE CEREAL FX4') AND T1.[CustomApp_Acres] > 0 AND T2.[RewardBrand_Litres]+@EPSILON >= 450	
		
		UPDATE @TEMP SET [Reward_Percentage] = 0.22 WHERE [RewardBrand] = 'INSURE PULSE'  AND [CustomApp_Acres] > 0 AND [RewardBrand_Litres]+@EPSILON >= 360		-- minimum 360L sold of 2 X 9.8 L CASE			   		 
		UPDATE @TEMP SET [Reward_Percentage] = 0.08 WHERE [RewardBrand] = 'NODULATOR PRO' AND [CustomApp_Acres] > 0 AND [RewardBrand_Sales]+@EPSILON >= 20000		-- minimum 20K of Nodulator PRO
		
		-- update reward 		
		/*
			Note: Custom Seed amounts can be equal to Total but not greater than total.
			IF 1000 ACRES OF A INSURE CEREAL IS PURCHASED AND 400 ACRES IS CUSTOM TREATED. PAY 22% ON 400 ACRES WORTH OF SDP  i.e.,  SDP * (400/100) * 22%
			FOR SOME REASON CUSTOM TREAD ACRES IS 1200, WHICH IS HIGHER THAN PURCHASED ACRES. PAY 22% ON 1000 ACRES WORTH OF PURCHASED SDP i.e., SDP * 22%
		*/

		-- INSURE CEREAL AND INSURE CEREAL FX4
		UPDATE @TEMP SET [Rewardable_Litres] = IIF(CustomApp_Litres <= RewardBrand_Litres,CustomApp_Litres,RewardBrand_Litres)  WHERE [Reward_Percentage] > 0 AND [RewardBrand] IN ('INSURE CEREAL','INSURE CEREAL FX4','INSURE PULSE')
		UPDATE @TEMP SET [Rewardable_Sales] = [Rewardable_Litres] * (RewardBrand_Sales/RewardBrand_Litres) WHERE [Rewardable_Litres] > 0 AND [RewardBrand] IN ('INSURE CEREAL','INSURE CEREAL FX4','INSURE PULSE')			

		-- NODULATOR PRO
		UPDATE @TEMP SET [Rewardable_Acres] = IIF(CustomApp_Acres <= RewardBrand_Acres,CustomApp_Acres,RewardBrand_Acres)  WHERE [Reward_Percentage] > 0 AND [RewardBrand] = 'Nodulator PRO'
		UPDATE @TEMP SET [Rewardable_Sales] = [Rewardable_Acres] * (RewardBrand_Sales/RewardBrand_Acres) WHERE [Rewardable_Acres] > 0  AND [RewardBrand] = 'Nodulator PRO'
		
		-- REWARD 
		UPDATE @TEMP SET [Reward] = [Rewardable_Sales] * [Reward_Percentage]
	END
	
	
FINAL:
	DELETE FROM RP2020_DT_Rewards_W_CustomSeed WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CustomSeed(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, RewardBrand_Acres, RewardBrand_Litres,  CustomApp_Sales, CustomApp_Acres, CustomApp_Litres, Rewardable_Sales, Rewardable_Acres, Rewardable_Litres, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, RewardBrand_Acres, RewardBrand_Litres,  CustomApp_Sales, CustomApp_Acres, CustomApp_Litres, Rewardable_Sales, Rewardable_Acres, Rewardable_Litres, Reward_Percentage, Reward
	FROM @TEMP

END



