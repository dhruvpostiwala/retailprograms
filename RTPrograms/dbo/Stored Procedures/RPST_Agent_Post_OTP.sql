﻿
CREATE PROCEDURE [dbo].[RPST_Agent_Post_OTP]  @SEASON INT,  @USERNAME VARCHAR(150), @OTP_IDS VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE @OTP_Payments TABLE (
		ID INT,		
		StatementCode INT NOT NULL DEFAULT 0,
		DataSetCode INT NOT NULL DEFAULT 0,		
		DataSetCode_L5 INT NOT NULL DEFAULT 0,		
		RewardCode VARCHAR(50) NULL,
		Amount Money NULL DEFAULT 0	,		
		MarketLetterCode VARCHAR(50) NULL,
		ValidationStatus VARCHAR(20) NULL DEFAULT 'PASSED'
	)
	   	

	BEGIN TRY

	-- TARGET STATEMENTS (UNLOCKED STATEMENTCODES)
	INSERT INTO @OTP_Payments(ID)
	SELECT DISTINCT VALUE AS ID  FROM String_Split(@OTP_IDS, ',')

	UPDATE T1
	SET StatementCode=ST.StatementCode
		,DataSetCode=ST.DataSetCode
		,DataSetCode_L5=ISNULL(ST.DataSetCode_L5,0)
		,RewardCode=ISNULL(OTP.RewardCode,'')
		,Amount=ISNULL(OTP.Amount,0)				
		,MarketLetterCode='ML' + CAST(@SEASON AS VARCHAR(4)) + '_' + LEFT(ISNULL(ST.Region,'WEST'),1) + '_OTP'
	FROM @OTP_Payments T1
		INNER JOIN RP_Seeding_OneTimePayments OTP	
		ON OTP.ID=T1.ID
		INNER JOIN RPWeb_Statements ST				
		ON ST.RetailerCode=OTP.RetailerCode AND ST.Season=OTP.Season				
	WHERE OTP.[Status]='Open' AND OTP.Season=@SEASON 
		AND ST.[Status]='Active' AND ST.StatementType='Actual' AND ST.VersionType='Unlocked'  
		AND (ST.Region='WEST' OR ST.StatementLevel='Level 1')

	UPDATE @OTP_Payments 
	SET ValidationStatus='FAILED'
	WHERE Amount=0 OR RewardCode='' OR StatementCode=0   
	
	MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
    USING (
		SELECT StatementCode, MarketLetterCode, RewardCode, Amount 
		FROM @OTP_Payments WHERE ValidationStatus='PASSED'
		
			UNION
		
		SELECT DataSetCode AS StatementCode, MarketLetterCode, RewardCode, SUM(Amount) AS Amount
		FROM @OTP_Payments 		
		WHERE ValidationStatus='PASSED' AND DataSetCode > 0
		GROUP BY DataSetCode,MarketLetterCode, RewardCode
		/*	
			UNION

		SELECT DataSetCode_L5 AS StatementCode, MarketLetterCode, RewardCode, SUM(Amount) AS Amount
		FROM @OTP_Payments 		
		WHERE ValidationStatus='PASSED' AND DataSetCode_L5 > 0
		GROUP BY DataSetCode_L5,MarketLetterCode, RewardCode
		*/
	) AS SRC
    ON TGT.StatementCode=SRC.StatementCode AND SRC.MarketLetterCode=TGT.MarketLetterCode AND SRC.RewardCode=TGT.RewardCode    
   WHEN NOT MATCHED THEN  
        INSERT (StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)  
        VALUES (SRC.StatementCode,MarketLetterCode,SRC.RewardCode,0,'Eligible','',0,0,0,SRC.Amount,SRC.Amount)  
    ;
	--WHEN MATCHED THEN
    --UPDATE SET PaymentAmount = Amount,NextPayment += SRC.Amount,RewardEligibility='Eligible'

	UPDATE T1
	SET [Status] ='Posted'
		,PostedDate=GetDate()
		,PostedBy=@USERNAME
		,StatementCode=T2.StatementCode		
	FROM RP_Seeding_OneTimePayments T1
		INNER JOIN @OTP_Payments T2
		ON T2.ID=T1.ID
		

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	--STRING_ESCAPE(REPLACE(REPLACE(@HTML_DATA,CHAR(13),''),CHAR(10),'') , 'json')
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message

END




