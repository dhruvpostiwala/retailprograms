﻿

CREATE PROCEDURE [dbo].[RPST_Agent_Exceptions] @STATEMENTCODE INT,@USERNAME VARCHAR(150) ,@JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;	
	

	DECLARE @ACTION VARCHAR(20) = UPPER(JSON_VALUE(@JSON, '$.action'));

	IF @ACTION = 'APPROVE' RETURN;
	

	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);

	DECLARE @UNLOCKED_STATEMENTCODE INT;
	DECLARE @SRC_STATEMENTCODE INT;

	DECLARE @ExceptionID INT;
	DECLARE @SRC_ExceptionID INT;
	DECLARE @EXCEPTIONLEVEL VARCHAR(10);
	DECLARE @RETAILERCODE VARCHAR(50);
	DECLARE @EXCEPTIONDESC VARCHAR(200);
	DECLARE @EXCEPTIONTYPE VARCHAR(20);
	DECLARE @EXCEPTIONVALUE_A FLOAT;
	DECLARE @EXCEPTIONVALUE_B FLOAT;
	DECLARE @EXCEPTIONVALUE FLOAT;
	DECLARE @EXCEPTIONMARGIN_A FLOAT;
	DECLARE @EXCEPTIONMARGIN_B FLOAT;
	DECLARE @EXCEPTIONMARGIN FLOAT;
	DECLARE @REWARDCODE VARCHAR(30);
	DECLARE @COMMENTS VARCHAR(2000);


	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;

	SELECT @REGION = UPPER(Region),@SEASON = Season,@SRC_STATEMENTCODE = Src_StatementCode
		,@UNLOCKED_STATEMENTCODE = CASE WHEN VersionType = 'Locked' Then Src_StatementCode ELSE StatementCode END 
	From RPWeb_Statements where StatementCode = @STATEMENTCODE;
	
	
	SET @ExceptionID = JSON_VALUE(@JSON, '$.id')
	SET @EXCEPTIONLEVEL=JSON_VALUE(@JSON, '$.exception_level')
	SET @RETAILERCODE = JSON_VALUE(@JSON, '$.retailercode')
	SET @EXCEPTIONDESC = JSON_VALUE(@JSON, '$.exception_desc')
	SET @EXCEPTIONTYPE= UPPER(JSON_VALUE(@JSON, '$.exception_type'))
	SET @EXCEPTIONVALUE_A = CONVERT(FLOAT, REPLACE(JSON_VALUE(@JSON, '$.exception_value_A'),',',''))
	SET @EXCEPTIONVALUE_B = CONVERT(FLOAT, REPLACE(JSON_VALUE(@JSON, '$.exception_value_B'),',',''))
	SET @EXCEPTIONVALUE = CONVERT(FLOAT, REPLACE(JSON_VALUE(@JSON, '$.exception_value'),',',''))
	SET @EXCEPTIONMARGIN_A=ISNULL(JSON_VALUE(@JSON, '$.exception_margin_A'),0)
	SET @EXCEPTIONMARGIN_B=ISNULL(JSON_VALUE(@JSON, '$.exception_margin_B'),0)
	SET @EXCEPTIONMARGIN=ISNULL(JSON_VALUE(@JSON, '$.exception_margin'),0)
	SET @REWARDCODE= JSON_VALUE(@JSON, '$.reward_code')
	SET @COMMENTS= JSON_VALUE(@JSON, '$.comments')



	IF @REGION = 'EAST'
	BEGIN
		BEGIN TRY

		BEGIN TRANSACTION
				
			DELETE FROM @EDIT_HISTORY


			IF(@EXCEPTIONLEVEL = 'LEVEL 1')
			BEGIN
				--ACTIONS SAVE, DELETE, UPDATE, SUBMIT
				IF @ACTION = 'DELETE'					
				BEGIN
					DELETE FROM RPWeb_Exceptions WHERE ID = @ExceptionID										
				END 
				ELSE 
					IF @ACTION = 'SAVE'
						BEGIN 										
							INSERT INTO [RPWeb_Exceptions](RetailerCode,[StatementCode],[Season], [RewardCode],Exception_Level,Exception_Desc, [Exception_Type], [Exception_Value_A],[Exception_Value_B],[Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode], [Comments], [DateCreated], [DateModified], [PostedBy])
							OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							VALUES(@RetailerCode,@StatementCode, @Season,@RewardCode,@ExceptionLevel,@ExceptionDesc, @ExceptionType, @ExceptionValue_A,@ExceptionValue_B,@ExceptionValue,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',@SRC_STATEMENTCODE, @Comments, GETDATE(),GETDATE(),NULL);

						END --END SAVE
					IF @ACTION = 'UPDATE' 
						BEGIN							
							IF @EXCEPTIONTYPE = 'OTHER'
								UPDATE [RPWeb_Exceptions]
								SET Exception_Desc = @EXCEPTIONDESC
								,Exception_Value_A = @EXCEPTIONVALUE_A
								,Exception_Value_B = @EXCEPTIONVALUE_B
								,Exception_Value = @EXCEPTIONVALUE
								,Comments = @COMMENTS
								,DateModified = GETDATE()
								OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
								INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
								WHERE ID = @ExceptionID
							ELSE
								UPDATE [RPWeb_Exceptions]
								SET  Comments = @COMMENTS
									,DateModified = GETDATE()
								OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
								INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
								WHERE ID = @ExceptionID
						END --END UPDATE
					IF @ACTION = 'SUBMIT'
						BEGIN					
							UPDATE [RPWeb_Exceptions] 
							SET Status = 'Submitted'
							,Show_In_UL = 1
							OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
							WHERE ID = @ExceptionID
						END --END SUBMIT

					IF @ACTION = 'REJECT'
						BEGIN
							UPDATE [RPWeb_Exceptions] 
							SET  Status = 'Rejected'
							OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
							WHERE ID = @ExceptionID
						END --END REJECT

					IF @ACTION = 'OBSOLETE'
						BEGIN
						--GET RID OF EXCEPTIONS FROM RpWEB_Rewards_SUMMARY
							EXEC [dbo].[RPST_Agent_Obsolete_Exceptions] @ExceptionID, @REGION, @EXCEPTIONLEVEL,@USERNAME 
						END --END OBSOLETE

			END -- END Exception LEVEL
			ELSE
			BEGIN
				--ACTIONS SAVE, DELETE, UPDATE, SUBMIT
				--Level 2  OU 
				DECLARE @ID TABLE (ID int)

				IF @ACTION = 'SAVE'
				BEGIN
			

					DROP TABLE IF EXISTS #TEMP
					SELECT OG.StatementCode AS StatementCode_L, OG.RetailerCode,Src_StatementCode AS StatementCode_U, COUNT(OG.RetailerCode) OVER() AS Retailer_Count
					INTO #TEMP
					from RPWeb_Statements OG
					WHERE OG.datasetcode =  @STATEMENTCODE
				
					
					INSERT INTO [RPWeb_Exceptions](RetailerCode,[StatementCode],[Season], [RewardCode],Exception_Level,Exception_Desc, [Exception_Type], [Exception_Value_A], [Exception_Value_B], [Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode], [Comments], [DateCreated], [DateModified], [PostedBy])
					OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
					VALUES(@RetailerCode,@StatementCode, @Season,@RewardCode,@ExceptionLevel,@ExceptionDesc, @ExceptionType, @ExceptionValue_A,@ExceptionValue_B,@ExceptionValue,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',@SRC_STATEMENTCODE, @Comments, GETDATE(),GETDATE(),NULL);		
					
					--ID of OU code we just inserted will only be one row
					SELECT @SRC_ExceptionID = [EHLinkCode] FROM @EDIT_HISTORY;

					IF ( @EXCEPTIONTYPE = 'OTHER') 
						BEGIN
							--SIMPLE SPLIT JUST DIVIDE BY AMOUNT OF RETAILERS
							INSERT INTO [dbo].[RPWeb_Exceptions]([RetailerCode], [StatementCode], [Season], [RewardCode],[Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value_A], [Exception_Value_B],[Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode],[Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy])
							OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							SELECT RetailerCode, StatementCode_L, @Season,@RewardCode,'LEVEL 1','OU '+@ExceptionDesc, @ExceptionType, @ExceptionValue_A/Retailer_Count,@ExceptionValue_B/Retailer_Count,@ExceptionValue/Retailer_Count,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',StatementCode_U, @SRC_ExceptionID ,@Comments, GETDATE(),GETDATE(),NULL
							FROM #TEMP
						END
					ELSE 
					BEGIN
						--LEVEL ONES GET REWARD BASED ON THEIR POG AMOUNT OF THE PERCENTAGE
						DECLARE @STATEMENTS AS UDT_RP_STATEMENT
						INSERT INTO @STATEMENTS
						SELECT StatementCode_L FROM #TEMP

						INSERT INTO [dbo].[RPWeb_Exceptions]([RetailerCode], [StatementCode], [Season], [RewardCode],[Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value_A], [Exception_Value_B],[Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode],[Src_ID], [Comments], [DateCreated], [DateModified], [PostedBy])
						OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
						INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
						SELECT RetailerCode, StatementCode_L, @Season,@RewardCode,'LEVEL 1','OU '+ @ExceptionDesc, @ExceptionType, 0,0,ISNULL((@EXCEPTIONMARGIN * EX.CYSales) - EX.CurrentReward,0),@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B, @EXCEPTIONMARGIN,'Draft',StatementCode_U, @SRC_ExceptionID ,@Comments, GETDATE(),GETDATE(),NULL
						FROM #TEMP T1
						LEFT JOIN [TVF_Get_Exception_ProgramData](@Season,@STATEMENTS,@RewardCode) EX ON T1.StatementCode_L = EX.StatementCode
						--WHERE CYSales > 0 to know when OU statements are matching 
					END												
				END --END SAVE
				IF @ACTION = 'UPDATE'
				BEGIN
				--TO DO EXCEPTION VALUE SHOULD BE SPLIT
					 IF @EXCEPTIONTYPE = 'OTHER' 
					 BEGIN

					

					 UPDATE [RPWeb_Exceptions]
							SET Exception_Desc = @EXCEPTIONDESC
							,Exception_Value_A = @EXCEPTIONVALUE_A
							,Exception_Value_B = @EXCEPTIONVALUE_B
							,Exception_Value = @EXCEPTIONVALUE
							,Comments = @COMMENTS
							,DateModified = GETDATE()
							OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							WHERE ID = @ExceptionID 

					UPDATE [RPWeb_Exceptions]
							SET Exception_Desc = 'OU '+@EXCEPTIONDESC,
							Exception_Value_A = @EXCEPTIONVALUE_A/(SELECT COUNT(RetailerCode) FROM RPWeb_Exceptions Where SRC_ID = @ExceptionID)
							,Exception_Value_B = @EXCEPTIONVALUE_B/(SELECT COUNT(RetailerCode) FROM RPWeb_Exceptions Where SRC_ID = @ExceptionID)
							,Exception_Value = @EXCEPTIONVALUE/(SELECT COUNT(RetailerCode) FROM RPWeb_Exceptions Where SRC_ID = @ExceptionID)
							,Comments = @COMMENTS
							,DateModified = GETDATE()
							OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							WHERE SRC_ID = @ExceptionID
					END
					ELSE
					BEGIN
							UPDATE [RPWeb_Exceptions]
							SET	 Comments = @COMMENTS
								,DateModified = GETDATE()
								 OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
								 INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
								 WHERE ID = @ExceptionID 

							UPDATE [RPWeb_Exceptions]
							SET	Comments = @COMMENTS
							,DateModified = GETDATE()
							OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							WHERE SRC_ID = @ExceptionID
					END

			

				END

				IF @ACTION = 'DELETE'
				BEGIN
					DELETE FROM RPWeb_Exceptions WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID
				END

				
				IF @ACTION = 'SUBMIT'
				BEGIN
					DELETE FROM @EDIT_HISTORY
					
					UPDATE [RPWeb_Exceptions] 
					SET Status = 'Submitted',
					Show_In_UL = 1
					OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
					WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID

				

				END

				IF @ACTION = 'REJECT'
				BEGIN
					DELETE FROM @EDIT_HISTORY

					UPDATE [RPWeb_Exceptions] 
					SET Status = 'Rejected'
					OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
					WHERE ID = @ExceptionID OR SRC_ID = @ExceptionID
				END

				IF @ACTION = 'OBSOLETE'
				BEGIN
					--GET RID OF EXCEPTIONS FROM RpWEB_Rewards_SUMMARY
					EXEC [dbo].[RPST_Agent_Obsolete_Exceptions] @ExceptionID, @REGION, @EXCEPTIONLEVEL, @USERNAME 
						
				END
			END -- END Exception LEVEL ELSE

			EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY

		COMMIT TRANSACTION

		END TRY
					
		BEGIN CATCH
			SELECT 'Failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
			ROLLBACK TRANSACTION
			RETURN;	
		END CATCH		
	END -- END IF Region
	ELSE IF @REGION= 'WEST'
	BEGIN
		BEGIN TRY

		BEGIN TRANSACTION

		DELETE FROM @EDIT_HISTORY
		

			IF(@EXCEPTIONLEVEL = 'LEVEL 1')
			BEGIN
				--ACTIONS SAVE, DELETE, UPDATE, SUBMIT
				IF @ACTION = 'DELETE'
					BEGIN	
							DELETE FROM RPWeb_Exceptions WHERE ID = @ExceptionID
					END --END Delete Action
					ELSE 
					IF @ACTION = 'SAVE'
					BEGIN 
										
							INSERT INTO [RPWeb_Exceptions](RetailerCode,[StatementCode],[Season], [RewardCode],Exception_Level,Exception_Desc, [Exception_Type],[Exception_Value_A],[Exception_Value_B], [Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode], [Comments], [DateCreated], [DateModified], [PostedBy])
							OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
							VALUES(@RetailerCode,@StatementCode, @Season,@RewardCode,@ExceptionLevel,@ExceptionDesc, @ExceptionType, @ExceptionValue_A, @ExceptionValue_B, @ExceptionValue,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',@SRC_STATEMENTCODE, @Comments, GETDATE(),GETDATE(),NULL);

					END --END SAVE
					IF @ACTION = 'UPDATE' 
					BEGIN
							

							IF @EXCEPTIONTYPE = 'OTHER'
								UPDATE [RPWeb_Exceptions]
								SET Exception_Desc = @EXCEPTIONDESC
								,Exception_Value_A = @EXCEPTIONVALUE_A
								,Exception_Value_B = @EXCEPTIONVALUE_B
								,Exception_Value = @EXCEPTIONVALUE
								,Comments = @COMMENTS
								,DateModified = GETDATE()
								OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
								INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
								WHERE ID = @ExceptionID
							ELSE
								UPDATE [RPWeb_Exceptions]
								SET  Comments = @COMMENTS
									,DateModified = GETDATE()
								OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
								INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
								WHERE ID = @ExceptionID


					END --END UPDATE

					IF @ACTION = 'SUBMIT'
					BEGIN
					
							UPDATE [RPWeb_Exceptions] 
							SET Status = 'Submitted'
							,Show_In_UL = 1
							OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
							WHERE ID = @ExceptionID

					END --END SUBMIT

					IF @ACTION = 'REJECT'
					BEGIN
							UPDATE [RPWeb_Exceptions] 
							SET  Status = 'Rejected'
							OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
							INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
							WHERE ID = @ExceptionID
					END --END REJECT

					IF @ACTION = 'OBSOLETE'
					BEGIN
						--GET RID OF EXCEPTIONS FROM RpWEB_Rewards_SUMMARY
						EXEC [dbo].[RPST_Agent_Obsolete_Exceptions] @ExceptionID, @REGION, @EXCEPTIONLEVEL, @USERNAME 

					END --END OBSOLETE

			END -- END Exception LEVEL

			
COMMIT_AND_EXIT:						
			EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY
			COMMIT TRANSACTION
			SELECT 'SUCCESS' AS Status, '' AS ErrorMessage
		END TRY
					
		BEGIN CATCH
			SELECT 'Failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
			ROLLBACK TRANSACTION
			RETURN;	
		END CATCH		

	END

	
END

