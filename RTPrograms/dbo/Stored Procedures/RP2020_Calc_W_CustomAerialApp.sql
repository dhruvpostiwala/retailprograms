﻿



CREATE PROCEDURE [dbo].[RP2020_Calc_W_CustomAerialApp]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	
	-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
	IF @STATEMENT_TYPE <> 'ACTUAL' 
	RETURN
	   	

	/*
	Qualifying Level: Level 2. For standalone will be at level 1.
	Calculating Level: Level 2. For standalone will be at level 1.

	Account Type: Custom Applicator Only

	Retails will receive 3% reward on all POG$ of the following brands: 
	•	Cabrio Plus
	•	Caramba
	•	Cotegra
	•	Dyax
	•	Forum
	•	Headline
	•	Lance
	•	Lance AG
	•	Nexicor

	That are custom applied in the 2020 season. If a retail has custom application acres greater than 0 
	for a brand i.e. Caramba then the retail will receive 3% reward on the value of Caramba that was custom
	applied. Example: A retail purchases 1000 acres of Caramba, they then custom apply 500 acres of Caramba. 
	The retail will receive 3% x $ value of 500 acres of Caramba.
	SDP Pricing

	West line Companies
	Qualifying Level: Level 5
	Calculating Level: Level 5
	Account Type: Custom Applicator Only
	Distributors will receive 3% reward on all POG$ of the following brands:
	• Cabrio Plus
	• Caramba
	• Cotegra
	• Dyax
	• Forum
	• Headline
	• Lance
	• Lance AG
	• Nexicor
	That are custom applied in the 2020 season. If a distributor has custom application acres greater than 0 for a brand
	i.e. Caramba then the distributor will receive 3% reward on the value of Caramba that was custom applied.
	Example: A distributor purchases 1,000 acres of Caramba, they then custom apply 500 acres of Caramba. 
	The distributor will receive 3% x $ value of 500 acres of Caramba.

	SWP Pricing
	**New September 15 2020 RP-2758
	Added 
	Priaxor
	Sercadis
	Twinline
	*/

	
	DECLARE @MARKETLETTERCODE VARCHAR(50) = 'ML2020_W_ADD_PROGRAMS';
	DECLARE @REWARD_PERC FLOAT = 0.03;

	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		ProductName Varchar(100) NOT NULL,
		ProductCode Varchar(50) NOT NULL,
		RewardBrand Varchar(50) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,	 
		Product_Quantity Numeric(18,2) NOT NULL DEFAULT 0,
		Price_SDP MONEY NOT NULL DEFAULT 0,
		Price_SWP MONEY NOT NULL DEFAULT 0,
		Product_Sales MONEY NOT NULL DEFAULT 0,
		Product_CustApp_Quantity Numeric(18,2) NOT NULL DEFAULT 0,
		Product_CustApp_Dollars MONEY NOT NULL DEFAULT 0,
		Elig_Dollars MONEY NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,5) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[ProductCode]
		)
	)

	DECLARE @CUST_AERIAL_STATEMENTS TABLE (
		Statementcode INT,		
		StatementLevel VARCHAR(50) NOT NULL,
		Category VARCHAR(50) NOT NULL
	)

	DROP TABLE IF EXISTS #Eligible_Products
	
	SELECT ProductCode, ProductName, ChemicalGroup
	INTO #Eligible_Products
	FROM ProductReference 
	WHERE ChemicalGroup IN ('CABRIO PLUS','CARAMBA','COTEGRA','DYAX','FORUM','HEADLINE','LANCE','LANCE AG','NEXICOR','PRIAXOR','SERCADIS','TWINLINE') --AND Status = 'Active'


	--Cargill does not receieve this reward
	INSERT INTO @CUST_AERIAL_STATEMENTS(Statementcode, StatementLevel, Category)
	SELECT DISTINCT rs.StatementCode,  rs.StatementLevel, RS.Category
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP_StatementsMappings Map	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT DISTINCT Retailercode
			FROM RetailerProfileAccountType
			WHERE AccountType IN ('Custom Aerial App','Third Party Custom Aerial App')
		) Act
		ON Act.Retailercode=Map.ActiveRetailercode		
	WHERE RS.RetailerCode <> 'D0000107'  
	
	--  #TEMP_SALES	-- GET SALES INFO FOR REGULAR STATEMENTS ONLY
	DROP TABLE IF EXISTS #TEMP_SALES
	SELECT ST.Statementcode, tx.RetailerCode, EB.ProductCode, EB.ProductName,  tx.ChemicalGroup AS RewardBrand
		,SUM(ISNULL(tx.Quantity,0)) AS Product_Quantity
		,SUM(ISNULL(tx.Price_SDP,0)) AS Price_SDP 
		,SUM(ISNULL(tx.Price_SWP,0)) AS Price_SWP 
	INTO #TEMP_SALES
	FROM @CUST_AERIAL_STATEMENTS ST	
		INNER JOIN RP_DT_Transactions TX ON TX.StatementCode = ST.StatementCode
		INNER JOIN #Eligible_Products EB ON EB.productcode = TX.productcode 	
	WHERE TX.BasfSeason = @SEASON and TX.InRadius = 1 AND ST.Category='REGULAR'
	GROUP BY ST.Statementcode, tx.RetailerCode, EB.Productcode, EB.ProductName, tx.ChemicalGroup
	   	  
	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales, FarmLevelSplit)
	SELECT TX.StatementCode, @MARKETLETTERCODE, @PROGRAM_CODE, tx.RetailerCode, '' AS FarmCode, SUM(IIF(R.StatementLevel='LEVEL 5',Price_SWP,Price_SDP)) AS Sales, 0 AS FarmLevelSplit
	FROM #TEMP_SALES TX
		INNER JOIN RP_Statements R
		ON R.StatementCode=TX.StatementCode
	WHERE R.Region='West' AND R.StatementLevel IN ('LEVEL 2','LEVEL 5') 
	GROUP BY TX.StatementCode, tx.RetailerCode
	   	 

	--GET CURRENT YEAR ELIGIBLE TRANSACTIONS (NOTE: #TEMP_SALES CONTAINS DATA FOR REGULAR STATEMENTS ONLY)
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,ProductName,ProductCode,RewardBrand,Product_Quantity,Price_SDP,Price_SWP)
	SELECT StatementCode, @MARKETLETTERCODE, @PROGRAM_CODE , ProductName, ProductCode
		,MAX(RewardBrand) AS RewardBrand
		,SUM(Product_Quantity) AS Product_Quantity
		,SUM(Price_SDP) AS Price_SDP 
		,SUM(Price_SWP) AS Price_SWP 
	FROM #TEMP_SALES
	GROUP BY Statementcode,ProductName, ProductCode
	
	--CHANGE PRICNG FOR WEST LINE COMPANIES
	UPDATE T1
	SET T1.Product_Sales = IIF(RS.StatementLevel = 'Level 5', T1.Price_SWP,T1.Price_SDP)
	FROM @Temp T1
		INNER JOIN @CUST_AERIAL_STATEMENTS RS 
		ON RS.StatementCode = T1.StatementCode
		
	-- CUSTOM APP DATA FOR ALL STATEMENT CATEGORIES (Regular, Custom Aerial)
	DROP TABLE IF EXISTS #Custom_App_Data
	SELECT ST.Statementcode ,PR.ProductCode											
		,SUM(
			CASE 
				WHEN cap.PackageSize='Jugs' AND PR.LEUConversion <> 0 THEN cap.Quantity/PR.LEUConversion
				WHEN cap.PackageSize IN ('Litres','Kgs') THEN cap.Quantity/PR.Volume				
				ELSE cap.Quantity
			END
		) AS Quantity
		,SUM(
			cap.Quantity *	(
				CASE 
					WHEN cap.PackageSize='Jugs'		THEN	IIF(RS.StatementLevel ='LEVEL 5',PR.PricePerJug_SWP,PR.PricePerJug_SDP)					
					WHEN cap.PackageSize IN ('Litres','Kgs')	THEN	IIF(RS.StatementLevel ='LEVEL 5',PR.PricePerLitre_SWP,PR.PricePerLitre_SDP)
					ELSE IIF(RS.StatementLevel ='LEVEL 5',PR.SWP,PR.SDP)
				END
			)
		) AS [Sales]			
	INTO #Custom_App_Data		
	FROM @CUST_AERIAL_STATEMENTS ST
		INNER JOIN RP_STATEMENTS RS 
		ON RS.Statementcode=ST.Statementcode
		INNER JOIN (
			SELECT StatementCode,ProductCode,PackageSize,SUM(Quantity) Quantity 
			FROM RP_DT_CustomAppProducts
			WHERE ProductCode <> ''
			GROUP BY StatementCode,ProductCode,PackageSize
		) CAP 	
		ON CAP.Statementcode=ST.Statementcode
		INNER JOIN (					
			SELECT PR1.Productcode, PR1.ChemicalGroup, PR1.ConversionW, PR1.Jugs,  PR1.Volume, PR1.LEUConversion
				,PR1.ConversionW/PR1.Jugs AS AcresPerJug
				,PR1.ConversionW/PR1.Volume as AcresPerLitre
				,PR1.Volume/PR1.Jugs AS LitresPerJug						
				,PRP.SDP
				,PRP.SDP/PR1.Jugs AS PricePerJug_SDP
				,PRP.SDP/PR1.Volume AS PricePerLitre_SDP				
				,PRP.SWP
				,PRP.SWP/PR1.Jugs AS PricePerJug_SWP
				,PRP.SWP/PR1.Volume AS PricePerLitre_SWP				
			FROM #Eligible_Products ELG
				INNER JOIN ProductReference PR1
				ON PR1.ProductCode=ELG.ProductCode
				INNER JOIN ProductReferencePricing PRP
				ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'													
		) PR			
		ON PR.ProductCode=CAP.ProductCode				
	GROUP BY ST.Statementcode, PR.ProductCode

	   	  
	-- CUSTOM APP DATA FOR REGULAR STATEMENTS
	-- NOTE: AT THIS POINT @TEMP ONLY HAS RECORDS FOR REGULAR STATEMENTS ONLY
	UPDATE T1
	SET T1.Product_CustApp_Quantity = CA.Quantity
		,T1.Product_CustApp_Dollars = CA.Sales
	FROM @Temp T1
		INNER JOIN #Custom_App_Data CA
		ON CA.StatementCode = T1.StatementCode AND CA.ProductCode = T1.ProductCode

	--since values are stored at product level save sales at brandlevel too
	UPDATE T 
	SET RewardBrand_Sales = RB.Product_Sales
	FROM @TEMP T
		INNER JOIN @CUST_AERIAL_STATEMENTS RS	
		ON RS.StatementCode = T.StatementCode
		LEFT JOIN (
			SELECT StatementCode,RewardBrand,SUM(Product_Sales) OVER (PARTITION BY StatementCode, RewardBrand) AS Product_Sales
			FROM @TEMP
		) RB 
		ON RB.StatementCode = T.StatementCode AND RB.RewardBrand = T.RewardBrand
	WHERE RS.Category='Regular'
	
	-- CUSTOM APP DATA FOR CUSTOM AERIAL STATEMENTS 	
	-- FOR CUSTOM AERIAL STATEMENTS , NO NEED TO GET SALES DATA
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,ProductName,ProductCode,RewardBrand,Product_CustApp_Quantity,Product_CustApp_Dollars, RewardBrand_Sales)
	SELECT CA.StatementCode, @MARKETLETTERCODE, @PROGRAM_CODE , ELG.ProductName, CA.ProductCode
		,MAX(ELG.ChemicalGroup) AS RewardBrand
		,SUM(CA.Quantity) AS Product_CustApp_Quantity
		,SUM(CA.Sales) AS Product_CustApp_Dollars
		,SUM(CA.Sales) AS RewardBrand_Sales
	FROM #Custom_App_Data CA		
		INNER JOIN #Eligible_Products ELG		ON ELG.ProductCode=CA.Productcode
		INNER JOIN @CUST_AERIAL_STATEMENTS ST	ON ST.StatementCode=CA.StatementCode
	WHERE ST.Category='Custom Aerial'
	GROUP BY CA.Statementcode, ELG.ProductName, CA.ProductCode
	   	 
	--UPDATE REWARD ENSURE IT DOES NOT OVERREWARD IF CUSTOM APP VALUE IS MORE FOR ANY REASON
	UPDATE T1
	SET Reward_Percentage = @REWARD_PERC
		,Elig_Dollars = IIF(Product_CustApp_Quantity > Product_Quantity,Product_Sales,Product_CustApp_Dollars)
		,Product_CustApp_Quantity = IIF(Product_CustApp_Quantity > Product_Quantity,Product_Quantity,Product_CustApp_Quantity)
		,Product_CustApp_Dollars = IIF(Product_CustApp_Quantity > Product_Quantity,Product_Sales,Product_CustApp_Dollars)
		,Reward = @REWARD_PERC * IIF(Product_CustApp_Quantity > Product_Quantity,Product_Sales,Product_CustApp_Dollars) 
	FROM @TEMP T1
		INNER JOIN @CUST_AERIAL_STATEMENTS RS	
		ON RS.StatementCode = T1.StatementCode
	WHERE RS.Category='Regular'
	--AND T1.Product_CustApp_Quantity > 0 RP-3381
	
	UPDATE T1
	SET Reward_Percentage = @REWARD_PERC
		,Elig_Dollars = Product_CustApp_Dollars
		,Reward = @REWARD_PERC * Product_CustApp_Dollars 
	FROM @TEMP T1
		INNER JOIN @CUST_AERIAL_STATEMENTS RS	
		ON RS.StatementCode = T1.StatementCode
	WHERE  RS.Category='Custom Aerial'
	--AND T1.Product_CustApp_Quantity > 0 RP-3381
	   	
	   	
	DELETE FROM RP2020_DT_Rewards_W_CustomAerial WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_CustomAerial (StatementCode, MarketLetterCode,ProgramCode,ProductName, ProductCode, RewardBrand,RewardBrand_Sales, Product_Quantity, Product_Sales, Product_CustApp_Quantity, Product_CustApp_Dollars, Elig_Dollars, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode,ProgramCode,ProductName, ProductCode, RewardBrand,RewardBrand_Sales, Product_Quantity, Product_Sales, Product_CustApp_Quantity, Product_CustApp_Dollars, Elig_Dollars, Reward_Percentage, Reward
	FROM @TEMP

END
