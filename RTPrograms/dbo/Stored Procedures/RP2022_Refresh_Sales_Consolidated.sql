﻿

CREATE PROCEDURE [dbo].[RP2022_Refresh_Sales_Consolidated]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SEASON_LY1 INT = @SEASON-1;
	DECLARE @SEASON_LY2 INT = @SEASON-2;

	DECLARE @EAST_STATEMENTS_EXIST BIT=0
	DECLARE @WEST_STATEMENTS_EXIST BIT=0
		
	DROP TABLE IF EXISTS #TEMP_Statements	
	SELECT ST.Season, ST.Region, ST.StatementCode, ST.RetailerCode,  ISNULL(ST.DataSetCode,0) AS DataSetCode, ISNULL(ST.DataSetCode_L5,0) AS DataSetCode_L5, ST.Level5Code
	INTO #TEMP_Statements
	FROM RP_STATEMENTS  ST
		INNER JOIN @STATEMENTS T1
		ON T1.Statementcode=ST.StatementCode
	WHERE SEASON=@SEASON

	IF EXISTS(SELECT TOP 1 1 FROM #TEMP_Statements WHERE Region='WEST')
	SET @WEST_STATEMENTS_EXIST=1

	IF EXISTS(SELECT TOP 1 1 FROM #TEMP_Statements WHERE Region='EAST')
	SET @EAST_STATEMENTS_EXIST=1


	/* Collect all sales information into a temporary table */
	DROP TABLE IF EXISTS #TEMP_Sales
	SELECT RS.StatementCode, RS.Level5Code, tx.RetailerCode, tx.TransType, tx.BASFSeason, tx.ProductCode, tx.ChemicalGroup, tx.Quantity, tx.Acres, tx.Price_SDP, tx.Price_SRP, tx.Price_SWP, tx.InRadius
	INTO #TEMP_Sales
	FROM #TEMP_Statements RS		
		INNER JOIN (
			/* HOUSES CURRENT YEAR POG PLAN TRANSACTIONS ONLY */
			SELECT StatementCode, RetailerCode, 'POG_Plan' AS TransType, @SEASON AS BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, 1 AS InRadius 
			FROM RP_DT_ScenarioTransactions
			
				UNION ALL

			/* IN CASE OF FORECAST/PROJECTION STATEMENTS, HOLDS ONLY LAST TWO YEARS TRANSACTIONS */
			SELECT StatementCode, RetailerCode, 'RET_TRANSACTION' AS TransType, BASFSeason, ProductCode, ChemicalGroup, Quantity, Acres, Price_SDP, Price_SRP,Price_SWP, InRadius 
			FROM RP_DT_Transactions 			
			
		) TX
		ON TX.StatementCode=RS.StatementCode


	/* Lets consolidate sales to group by market letter, programcode and chemicalgroup, reward/qualifying brand */
	IF OBJECT_ID('tempdb..#Consolidated_Sales') IS NOT NULL DROP TABLE #Consolidated_Sales

	-- ALL BASF PORTFOLIO BRANDS	
	SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			

	INTO #Consolidated_Sales	
	FROM #TEMP_Sales TX		
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ElG_Programs
		) MLP
		ON tx.StatementCode=MLP.StatementCode
		INNER JOIN (
			SELECT * FROM RP_Config_Groups  WHERE GroupType='ALL_BASF_BRANDS'
		) GR
		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode	
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType

	-- ALL CANOLA PROTECTION PRODUCTS BRANDS
	INSERT INTO #Consolidated_Sales
	SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	
	FROM #TEMP_Sales TX		
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ElG_Programs
		) MLP
		ON tx.StatementCode=MLP.StatementCode
		INNER JOIN (
			SELECT * FROM RP_Config_Groups  WHERE MarketLetterCode='ML2022_W_CPP' AND GroupType='ALL_CPP_BRANDS'
		) GR
		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE tx.ChemicalGroup IN ('INSURE Cereal', 'INSURE CEREAL FX4', 'INSURE Pulse', 'NODULATOR DUO SCG', 'NODULATOR LQ', 'NODULATOR SCG',
							   'TERAXXA F4', 'CENTURION', 'CERTITUDE', 'CIMEGRA', 'COTEGRA', 'FACET L', 'LANCE', 'LANCE AG', 'LIBERTY 150', 'ACROBAT',
							   'ARMEZON', 'BASAGRAN', 'BASAGRAN FORTE', 'CABRIO', 'CABRIO PLUS', 'CANTUS', 'CARAMBA', 'CEVYA', 'DISTINCT', 'DYAX',
							   'ENGENIA', 'FORUM', 'HEADLINE', 'HEAT COMPLETE', 'HEAT LQ', 'LIBERTY 200', 'NEALTA', 'NEXICOR', 'ODYSSEY',
							   'ODYSSEY DLX', 'ODYSSEY NXT', 'ODYSSEY ULTRA', 'ODYSSEY ULTRA NXT', 'OUTLOOK', 'PRIAXOR', 'PURSUIT', 'SEFINA', 'SERCADIS',
							   'SOLO', 'SOLO ADV', 'SOLO ULTRA', 'TITAN', 'TWINLINE', 'VIPER', 'VIPER ADV', 'ZIDUA SC', 'BASF 28% UAN')

		  -- Chemical groups where only certain products (not the entire group) should be included
		  OR (tx.ChemicalGroup = 'NODULATOR' AND PR.Product IN ('NODULATOR CP SCG', 'NODULATOR FB PEAT'))
		  OR (tx.ChemicalGroup = 'NODULATOR XL' AND PR.Product IN ('NODULATOR XL LQ', 'NODULATOR XL PEAT'))
		  OR (tx.ChemicalGroup = 'NODULATOR N/T' AND PR.Product = 'NODULATOR N/T SA PEAT')
		  OR (tx.ChemicalGroup = 'NODULATOR PRO' AND PR.Product = 'NODULATOR PRO 100')
		  OR (tx.ChemicalGroup = 'FRONTIER' AND PR.Product = 'FRONTIER MAX')
		  OR (tx.ChemicalGroup = 'PROWL' AND PR.Product = 'PROWL H2O')
		  OR (tx.ChemicalGroup = 'POAST' AND PR.Product = 'POAST ULTRA')
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType

	   	 
	-- ALL EAST BRANDS
	INSERT INTO #Consolidated_Sales
	SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	
	FROM #TEMP_Sales TX		
		INNER JOIN (
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ElG_Programs
		) MLP
		ON tx.StatementCode=MLP.StatementCode
		INNER JOIN (
			SELECT * FROM RP_Config_Groups  WHERE GroupType='ALL_EAST_BRANDS'
		) GR
		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE tx.ChemicalGroup IN ('ABSOLUTE', 'ACCELERON', 'ACROBAT', 'ADRENALIN', 'AMIGO', 'APOGEE', 'ARMEZON', 'ARMEZON PRO',
							   'ASSIGNMENT', 'ASSIST', 'BANVEL', 'BASAGRAN', 'BASAGRAN FORTE', 'BASF 28% UAN', 'CABRIO',
							   'CABRIO PLUS', 'CANTUS', 'CARAMBA', 'CENTURION', 'CEVYA', 'CHARTER', 'CIMEGRA', 'CLEANSWEEP', 'CLEARFIELD COMMITMENT FEE',
							   'CLEARFIELD LENTILS', 'CLEARFIELD SUNFLOWERS', 'CLEARFIELD WHEAT', 'CONQUEST', 'CONQUEST LQ', 'COTEGRA',
							   'DISTINCT', 'DUET', 'DYAX', 'DYVEL', 'DYVEL DS', 'ENGENIA', 'EQUINOX', 'ERAGON', 'ERAGON LQ', 'FACET L',
							   'FLAXMAX DLX', 'FLO RITE 1197', 'FLO RITE 1706', 'FLO RITE 3330', 'FORUM', 'FRONTIER',
							   'FRUIT & VEGETABLE APHICIDE', 'GEMINI', 'HEADLINE', 'HEADLINE AMP', 'HEADLINE DUO', 'HEAT', 'HEAT COMPLETE',
							   'HEAT LQ', 'HONOR', 'IGNITE', 'IMPACT', 'INSURE Cereal', 'INSURE CEREAL FX4', 'INSURE Pulse',
							   'INTEGRITY', 'KUMULUS', 'LADDOK', 'LANCE', 'LANCE AG', 'LIBERTY 150', 'LIBERTY 200', 'LIBERTY LINK',
							   'MAKO', 'MARKSMAN', 'MERGE', 'MERIVON', 'MIZUNA', 'NEALTA', 'NEMATAC C', 'NEW PRE-HARVEST HERBICIDE', 'NEXICOR',
							   'ODYSSEY', 'ODYSSEY DLX', 'ODYSSEY NXT', 'ODYSSEY ULTRA', 'ODYSSEY ULTRA NXT', 'OPTILL', 'OUTLOOK',
							   'POAST', 'PRIAXOR', 'PRIAXOR DS', 'PRISTINE', 'PROWL', 'PURSUIT', 'PURSUIT ULTRA', 'PYRAMIN',
							   'SALUTE', 'Seed Colorant', 'SEED ENHANCEMENT', 'SEFINA', 'SELECT', 'SERCADIS', 'SERIFEL', 'SOLO',
							   'SOLO ADV', 'SOLO ULTRA', 'SOVRAN', 'STAMINA', 'TITAN', 'TWINLINE', 'VERSYS', 'VIPER', 'VIPER ADV',
							   'VIVANDO', 'ZAMPRO', 'ZIDUA SC')

		  -- Chemical groups where only certain products (not the entire group) should be included
		  OR (tx.ChemicalGroup = 'INVIGOR' AND PR.Product IN ('INVIGOR L345PC',  'INVIGOR L234PC', 'INVIGOR L255PC','INVIGOR L252','INVIGOR L352C', 'INVIGOR L233P',  'INVIGOR L357P')) 
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode ,MLP.MarketLetterCode ,MLP.ProgramCode ,GR.GroupType

	/*******************************************************************************************************************************************/
	/* LETS INSERT TRANSACATION BASED ON CONFIG SETUP */	
	/* maintain selection column order while insreting -->  StatementCode, Level5Code, RetailerCode, GroupLabel, MarketLetterCode, ProgramCode, GroupType */
	INSERT INTO #Consolidated_Sales
	SELECT ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode , MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,'') AS ProgramCode, GR.GroupType						
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			
	
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
	FROM @STATEMENTS ST
		INNER JOIN (
			/* Note: ProgramCode "ELIGIBLE_SUMMARY" is not listed in RP_Config_Programs. Only added to table "[RP_Config_Groups]" to keep track eligible brands sales (i.e., reward will paid out on) */
			SELECT DISTINCT StatementCode, MarketLetterCode, 'ELIGIBLE_SUMMARY' AS ProgramCode FROM RP_DT_ML_ElG_Programs 
				UNION
			SELECT DISTINCT StatementCode, MarketLetterCode, ProgramCode FROM RP_DT_ML_ELG_Programs
		) MLP
		ON MLP.StatementCode=ST.StatementCode						
		
		INNER JOIN RP_Config_Groups GR		ON GR.MarketLetterCode=MLP.MarketLetterCode	AND GR.ProgramCode=MLP.ProgramCode				

		-- BRAND LEVEL JOIN
		INNER JOIN RP_Config_Groups_Brands BR		ON BR.GroupID=GR.GroupID
		INNER JOIN #TEMP_Sales TX	ON TX.StatementCode=ST.StatementCode AND TX.ChemicalGroup=BR.ChemicalGroup
		
	GROUP BY ST.StatementCode, tx.Level5Code, tx.RetailerCode, GR.GroupLabel, tx.ProductCode ,MLP.MarketLetterCode, ISNULL(MLP.ProgramCode,''), GR.GroupType

	-- Remove InVigor hybrids that have a "DistIncluded" property in the "OtherInfo" column of ProductReference_Hybrids from reward brands, if the retailer's Level5Code isn't in the list for the property.
	DELETE CS
	FROM #Consolidated_Sales CS
		INNER JOIN ProductReference PR
		ON CS.ProductCode = PR.ProductCode
		INNER JOIN ProductReference_Hybrids PRH
		ON PR.Hybrid_ID = PRH.ID
	WHERE JSON_QUERY(PRH.OtherInfo, '$.DistIncluded') IS NOT NULL
	AND CS.Level5Code NOT IN (
		SELECT value [Level5Code]
		FROM OPENJSON((SELECT JSON_QUERY(OtherInfo, '$.DistIncluded') [IncludedDist] FROM ProductReference_Hybrids WHERE ID = PRH.ID))
	)

	-- KM: REMOVE NON REWARD INVIGOR PRODUCTS (WEST MARKET LETTERS)
	DELETE CS
	FROM #Consolidated_Sales CS
		INNER JOIN ProductReference PR ON CS.ProductCode = PR.ProductCode
	WHERE CS.MarketLetterCode IN ('ML2022_W_INV', 'ML2022_W_CPP')
		AND CS.GroupLabel = 'INVIGOR'
		-- LIST ALL INVIGOR REWARD PRODUCTS AND DELETE THE ONES THAT ARE NOT ONE
		--New in 2022: InVigor L356PC, InVigor L343PC
		AND PR.Product NOT IN  ('InVigor L340PC','INVIGOR CHOICE LR344PC','InVigor L345PC','InVigor L352C','InVigor L357P',
								'InVigor L230','InVigor L233P','InVigor L234PC','InVigor L241C','InVigor L252','InVigor L255PC','InVigor L356PC','InVigor L343PC')


	
	-- KM: REMOVE NON REWARD INVIGOR PRODUCTS (EAST MARKET LETTERS)
	DELETE CS
	FROM #Consolidated_Sales CS
		INNER JOIN ProductReference PR ON CS.ProductCode = PR.ProductCode
	WHERE CS.MarketLetterCode IN ('ML2022_E_CPP', 'ML2022_E_INV')
		AND CS.GroupLabel = 'INVIGOR'
		-- LIST ALL INVIGOR REWARD PRODUCTS AND DELETE THE ONES THAT ARE NOT ONE
		AND PR.Product NOT IN  ('InVigor L345PC', 'InVigor L357P', 'InVigor L233P', 'InVigor L234PC', 'InVigor L255PC')
		

	-- KM: Remove Specific Products (REMOVE SPECIFIC PRODUCTS WITHIN ELIGIBLE BRANDS)
	DELETE CS
	FROM #Consolidated_Sales CS
		INNER JOIN ProductReference PR
		ON CS.ProductCode = PR.ProductCode
	WHERE CS.MarketLetterCode IN ('ML2022_W_CPP', 'ML2022_W_INV')
		AND (
			 (CS.GroupLabel = 'PURSUIT' AND PR.Product IN ('PURSUIT HERBICIDE (E)', 'MULTISTAR', 'GLADIATOR')) OR -- REMOVE MULTISTAR AND PURSUIT EAST AND GGLADIATOR
			 (CS.GroupLabel = 'DISTINCT' AND PR.Product IN ('DISTINCT (E)')) OR
			 (CS.GroupLabel = 'NODULATOR' AND PR.Product IN ('NODULATOR CP PEAT'))
			)

	-- KAM: Remove Specific Products EAST (REMOVE SPECIFIC PRODUCTS WITHIN ELIGIBLE BRANDS)
	DELETE CS
	FROM #Consolidated_Sales CS
		INNER JOIN ProductReference PR
		ON CS.ProductCode = PR.ProductCode
	WHERE CS.MarketLetterCode IN ('ML2022_E_CPP', 'ML2022_E_INV')
		AND (
			 (CS.GroupLabel = 'PROWL' AND PR.Product IN ('PROWL 400'))
		)

	/******** NON REWARD BRANDS **********/
		INSERT INTO #Consolidated_Sales	
		SELECT tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup AS GroupLabel, tx.ProductCode 
			,'NON_REWARD_BRANDS' AS MarketLetterCode , 'NON_REWARD_BRANDS_SUMMARY' ProgramCode , 'NON_REWARD_BRANDS' AS GroupType
		,SUM(IIF(tx.TransType='POG_Plan',tx.Acres,0)) AS Acres_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SDP,0)) AS SDP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SRP,0)) AS SRP_Forecast
		,SUM(IIF(tx.TransType='POG_Plan',tx.Price_SWP,0)) AS SWP_Forecast

		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION',tx.Acres,0)) AS Acres_Q_CY
		,SUM(IIF(tx.BASFSeason=@SEASON  AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Acres,0)) AS Acres_Q_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Acres,0)) AS Acres_Q_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Acres,0)) AS Acres_E_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION', tx.Price_SDP,0)) AS Price_Q_SDP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_CY
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SDP,0)) AS Price_Q_SDP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY1

		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SDP,0)) AS Price_Q_SDP_LY2
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SDP,0)) AS Price_E_SDP_LY2

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SRP,0)) AS Price_Q_SRP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SRP,0)) AS Price_Q_SRP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SRP,0)) AS Price_Q_SRP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SRP,0)) AS Price_E_SRP_LY2			

		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION',tx.Price_SWP,0)) AS Price_Q_SWP_CY
		,SUM(IIF(tx.BASFSeason=@SEASON AND tx.TransType='RET_TRANSACTION' AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_CY

		,SUM(IIF(tx.BASFSeason=@SEASON_LY1,tx.Price_SWP,0)) AS Price_Q_SWP_LY1
		,SUM(IIF(tx.BASFSeason=@SEASON_LY1 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY1
		
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2,tx.Price_SWP,0)) AS Price_Q_SWP_LY2				
		,SUM(IIF(tx.BASFSeason=@SEASON_LY2 AND tx.InRadius=1,tx.Price_SWP,0)) AS Price_E_SWP_LY2			
			
	FROM #TEMP_Sales TX		
		LEFT JOIN (
			SELECT DISTINCT StatementCode, Productcode
			FROM #Consolidated_Sales 
			WHERE ProgramCode='ELIGIBLE_SUMMARY' AND GroupType='ELIGIBLE_BRANDS'
		) TX2
		ON TX2.StatementCode=TX.StatementCode AND TX2.ProductCode=TX.Productcode 
	WHERE TX2.ProductCode IS NULL
	GROUP BY tx.StatementCode ,tx.Level5Code ,tx.RetailerCode, tx.ChemicalGroup, tx.ProductCode


	
	-- RP2022_W_LOGISTICS_SUPPORT Reward
	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_LOGISTICS_SUPPORT' AND ((PR.ChemicalGroup = 'NODULATOR' AND PR.Product NOT IN ('NODULATOR CP SCG', 'NODULATOR FB PEAT'))														
														  OR (PR.ChemicalGroup = 'NODULATOR PRO' AND PR.Product <> 'NODULATOR PRO 100')
														  OR (PR.ChemicalGroup = 'FRONTIER' AND PR.Product <> 'FRONTIER MAX')
														  OR (PR.ChemicalGroup = 'POAST' AND PR.Product <> 'POAST ULTRA')
														  OR (PR.ChemicalGroup = 'PROWL' AND PR.Product <> 'PROWL H2O'))

	-- RP2022_W_LOGISTICS_SUPPORT: Include Heat LQ (Pre Seed) in "PRE-SEED PRODUCTS" and Heat LQ (Pre-Harvest) ‘PRE-SEED PRODUCTS'
	UPDATE TX
	   SET GroupLabel = 'PRE-SEED PRODUCTS'
	FROM #Consolidated_Sales TX
	LEFT JOIN ProductReference PR
	ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_LOGISTICS_SUPPORT' AND PR.Product IN ('HEAT LQ (PRE-SEED)', 'HEAT LQ BULK (PRE-SEED)') AND TX.GroupLabel ='FALL HERBICIDES'
	


	/*
	-- RC-4173: Remove Outlook from all west reward qualifications and calculations
	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE PR.ChemicalGroup = 'OUTLOOK' AND TX.MarketLetterCode LIKE 'ML2022_W%'

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE PR.Product in ('HEAT (PRE-SEED)', 'HEAT (PRE-HARVEST)') and TX.MarketLetterCode LIKE 'ML2022_W%'

	-- DELETE Nodulator XL SCG
	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE PR.ChemicalGroup = 'NODULATOR XL' and PR.Product NOT IN ('NODULATOR XL LQ', 'NODULATOR XL PEAT') and TX.MarketLetterCode LIKE 'ML2022_W%'


	-- SPECIAL CASES FOR SPECIFIC PROGRAMS
	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_BRAND_SPEC_SUPPORT' AND ((PR.ChemicalGroup = 'FRONTIER' AND PR.Product <> 'FRONTIER MAX')
														   OR (PR.ChemicalGroup = 'POAST' AND PR.Product <> 'POAST ULTRA'))

	DELETE FROM #Consolidated_Sales WHERE ProgramCode = 'RP2022_W_INVENTORY_MGMT' AND GroupLabel LIKE 'NODULATOR%'

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_GROWTH_BONUS' AND (PR.ChemicalGroup = 'FRONTIER' AND PR.Product <> 'FRONTIER MAX')

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_LOYALTY_BONUS' AND (PR.ChemicalGroup = 'POAST' AND PR.Product <> 'POAST ULTRA')

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_RETAIL_BONUS_PAYMENT' AND ((PR.ChemicalGroup = 'FRONTIER' AND PR.Product <> 'FRONTIER MAX')
														     OR (PR.ChemicalGroup = 'POAST' AND PR.Product <> 'POAST ULTRA')
															 OR (PR.ChemicalGroup = 'PROWL' AND PR.Product <> 'PROWL H2O'))

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_W_CUSTOM_SEED_TREATING_REWARD' AND ((PR.ChemicalGroup = 'NODULATOR PRO' AND PR.Product <> 'NODULATOR PRO 100'))

	DELETE FROM #Consolidated_Sales WHERE ProgramCode = 'RP2022_W_SEGMENT_SELLING' AND GroupLabel IN ('INVIGOR', 'LIBERTY 150', 'CENTURION')

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_E_PLANNING_SUPP_BUSINESS' AND PR.ChemicalGroup IN ('KUMULUS', 'INVIGOR')

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_E_PORFOLIO_SUPPORT' AND 
		(PR.ChemicalGroup = 'INVIGOR' OR (PR.ChemicalGroup = 'PROWL' AND PR.Product = 'PROWL 400'))

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.MarketLetterCode = 'ML2022_E_CPP' AND TX.ProgramCode = 'RP2022_E_SUPPLY_SALES' AND PR.ChemicalGroup = 'INVIGOR'

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.ProgramCode = 'RP2022_E_PURSUIT_REWARD' AND PR.ChemicalGroup IN ('FRONTIER') AND PR.Product NOT IN ('FRONTIER MAX')

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.MarketLetterCode = 'ML2022_E_CPP' AND TX.ProgramCode = 'RP2022_E_CUSTOM_GROUND_APP_REWARD' AND PR.ChemicalGroup = 'PROWL' AND PR.Product <> 'PROWL H2O'

	DELETE TX
	FROM #Consolidated_Sales TX
		INNER JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode
	WHERE TX.MarketLetterCode = 'ML2022_W_CPP' AND TX.ProgramCode = 'RP2022_W_ADVANCED_WEED_CTRL' AND PR.ChemicalGroup = 'HEAT LQ' AND PR.Product LIKE '%PRE-HARVEST%'
	*/

	/* LETS START SETTING DATA VALUES AS NEEDED DEFINED BY BRD */

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION','REWARD PLANNER')	
	BEGIN
		UPDATE #Consolidated_Sales
		SET Acres_E_CY=Acres_Forecast
			,Acres_E_LY1=Acres_Q_LY1
			,Acres_E_LY2=Acres_Q_LY2

			,Price_E_SDP_CY=SDP_Forecast
			,Price_E_SDP_LY1=Price_Q_SDP_LY1
			,Price_E_SDP_LY2=Price_Q_SDP_LY2

			,Price_E_SRP_CY=SRP_Forecast
			,Price_E_SRP_LY1=Price_Q_SRP_LY1
			,Price_E_SRP_LY2=Price_Q_SRP_LY2

			,Price_E_SWP_CY=SWP_Forecast
			,Price_E_SWP_LY1=Price_Q_SWP_LY1
			,Price_E_SWP_LY2=Price_Q_SWP_LY2
	END

	ALTER TABLE #Consolidated_Sales ADD [Pricing_Type] [VARCHAR](3) NULL
	
	/*
	UPDATE CS
	SET [Pricing_Type] = CASE 
		WHEN CS.Level5Code = 'D0000117' AND CS.MarketLetterCode LIKE 'ML2022_W%' THEN 'SRP'
		WHEN CS.Level5Code = 'D000001' THEN 'SDP'
		WHEN RS.Region = 'EAST' THEN CASE
			WHEN PR.ChemicalGroup = 'INVIGOR' THEN 'SRP'
			ELSE 'SDP'
		END
		ELSE 'SRP'
	END
	FROM #Consolidated_Sales CS
		INNER JOIN RP_STATEMENTS RS on CS.StatementCode = RS.StatementCode
		INNER JOIN ProductReference PR on PR.ProductCode = CS.ProductCode
	*/

	IF @WEST_STATEMENTS_EXIST = 1
	BEGIN
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SWP' WHERE Level5Code IN ('D0000107','D0000137','D0000244','D520062427') -- LINE COMPANIES
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SRP' WHERE Level5Code = 'D0000117' -- FCL/COOP
		UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SDP' WHERE Level5Code = 'D000001' -- INDEPENDENTS / RC
	END

	IF @EAST_STATEMENTS_EXIST = 1
	BEGIN
		UPDATE CS
		SET [Pricing_Type] =  IIF(PR.ChemicalGroup='INVIGOR','SRP','SDP')
		FROM #Consolidated_Sales CS
			INNER JOIN RP_STATEMENTS RS on CS.StatementCode = RS.StatementCode
			INNER JOIN ProductReference PR on PR.ProductCode = CS.ProductCode
		WHERE RS.Region='EAST'
	END
		
	UPDATE #Consolidated_Sales 	SET [Pricing_Type] = 'SRP' WHERE Pricing_Type IS NULL


	--  Make sure to insert correct price values based on pricing type  in columns Price_CY, Price_LY1, Price_LY2	
	DELETE FROM RP2022_DT_Sales_Consolidated WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)	
	INSERT INTO RP2022_DT_Sales_Consolidated(StatementCode,DataSetCode,DataSetCode_L5,Level5Code,RetailerCode,GroupLabel,ProductCode,MarketLetterCode,ProgramCode,GroupType,Pricing_Type
		,Acres_Forecast
		,Price_Forecast		
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_CY, Acres_LY1 ,Acres_LY2
		,Price_CY, Price_LY1, Price_LY2	)	
	SELECT TX.StatementCode, ST.DataSetCode, ST.DataSetCode_L5, ST.Level5Code ,TX.RetailerCode
		,GroupLabel,ProductCode, MarketLetterCode, ProgramCode, GroupType,Pricing_Type
		,Acres_Forecast
		, CASE WHEN Pricing_Type='SRP' THEN SRP_Forecast
			   WHEN Pricing_Type='SWP' THEN SWP_Forecast
			   ELSE SDP_Forecast 
		  END AS [Price_Forecast]
		,Acres_Q_CY, Acres_E_CY
		,Acres_Q_LY1, Acres_E_LY1
		,Acres_Q_LY2, Acres_E_LY2
		,Price_Q_SDP_CY, Price_E_SDP_CY
		,Price_Q_SDP_LY1, Price_E_SDP_LY1
		,Price_Q_SDP_LY2, Price_E_SDP_LY2
		,Price_Q_SRP_CY, Price_E_SRP_CY
		,Price_Q_SRP_LY1, Price_E_SRP_LY1
		,Price_Q_SRP_LY2, Price_E_SRP_LY2
		,Price_Q_SWP_CY, Price_E_SWP_CY
		,Price_Q_SWP_LY1, Price_E_SWP_LY1
		,Price_Q_SWP_LY2, Price_E_SWP_LY2
		,Acres_E_CY AS [Acres_CY], Acres_E_LY1 AS [Acres_LY1], Acres_E_LY2  AS [Acres_LY2]
		,CASE 
			WHEN Pricing_Type='SRP' THEN Price_E_SRP_CY
			WHEN Pricing_Type='SWP' THEN Price_E_SWP_CY
			ELSE Price_E_SDP_CY  
		END AS Price_CY
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY1
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY1
			 ELSE  Price_E_SDP_LY1
		  END AS Price_LY1
		,
		CASE WHEN Pricing_Type='SRP' THEN Price_E_SRP_LY2
			 WHEN Pricing_Type='SWP' THEN Price_E_SWP_LY2
			 ELSE  Price_E_SDP_LY2
		  END AS Price_LY2
	FROM #TEMP_Statements ST
		INNER JOIN #Consolidated_Sales TX		
		ON TX.StatementCode=ST.StatementCode
END

