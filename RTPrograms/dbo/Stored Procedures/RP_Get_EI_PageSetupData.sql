﻿
CREATE PROCEDURE [dbo].[RP_Get_EI_PageSetupData]  @STATEMENTCODE INT
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @STATEMENTCODE INT = 7984

	DROP TABLE IF EXISTS #TBL_EI_Fields
	SELECT EI.*, CFG.DisplayOnce, CFG.DisplaySeq, CFG.GroupTitle, CFG.Label ,CFG.Suffix, CFG.HelpInfo, CFG.DefaultValue
		,PRG.Title AS Program_Title, PRG.EI_PageDisplayOrder AS Program_Seq
		,ML.Title AS ML_Title, ML.Sequence As ML_Seq
		,CASE WHEN EI.ProgramCode = 'RP2021_W_GROWTH_LOYALTY_RB' AND ISNULL(CAR.CARID,-1) <> -1 THEN CAST(CAR.CARID AS VARCHAR(10)) ELSE NULL END AS 'CARRegion'
		,S.Level5Code
	INTO #TBL_EI_Fields
	FROM RPWeb_User_EI_FieldValues EI
		INNER JOIN RP_Config_EI_Fields CFG ON CFG.FieldCode=EI.FieldCode AND CFG.ProgramCode=EI.ProgramCode
		INNER JOIN RP_Config_Programs PRG ON PRG.ProgramCode=EI.ProgramCode
		INNER JOIN RP_Config_MarketLetters ML ON ML.MarketLetterCode=EI.MarketLetterCode
		INNER JOIN RPWeb_Statements S ON S.StatementCode=EI.StatementCode
		LEFT JOIN rp.CARMappingView CAR ON S.RetailerCode = CAR.RetailerCode
	WHERE EI.StatementCode=@StatementCode AND ML.Status='Active' AND PRG.Status = 'Active' AND CFG.Status = 'Active'
	
	-- NEW: RC-4724 - the Base Reward Edit Input is specific to Independents in RCSC and not applicable or needed for FCL Coops in RC.
	DELETE #TBL_EI_Fields WHERE Level5Code = 'D0000117' AND ProgramCode = 'RP2022_W_BASE_REWARD' 
	
	SELECT [Data]=(
		SELECT programcode AS program_code, program_title
			,fieldcode AS field_code, grouptitle, label, suffix, helpinfo, fieldvalue AS field_value, defaultvalue AS default_value, calcfieldvalue AS calc_value
			,ml_codes, ml_titles_display, carregion
		FROM (
			SELECT 	ROW_NUMBER() OVER(PARTITION BY EI.FieldCode, EI.ProgramCode ORDER BY EI.DisplaySeq) AS RowNumber
				,EI.DisplayOnce
			 ,EI.FieldCode, EI.ProgramCode, EI.DisplaySeq As Field_Seq, EI.Program_Title,  EI.GroupTitle, EI.Label, EI.FieldValue, EI.DefaultValue, EI.CalcFieldValue, EI.Suffix,EI.HelpInfo
			 ,EI.Program_Seq, EI.ML_Seq, EI.CarRegion
				,ISNULL(T2.ML_Codes,EI.MarketLetterCode) AS ml_codes 
				,ISNULL(T2.ML_Titles,EI.ML_Title) AS ml_titles_display	
			FROM #TBL_EI_Fields EI
				LEFT JOIN (
					SELECT FieldCode, ProgramCode
						,STRING_AGG(MarketLetterCode,',') AS ML_Codes
						,STRING_AGG(ML_Title,'<br>') AS ML_Titles 
					FROM #TBL_EI_Fields T1
					WHERE DisplayOnce=1
					GROUP BY FieldCode, ProgramCode
				) T2
				ON T2.FieldCode=EI.FieldCode AND T2.ProgramCode=EI.ProgramCode
		) D
		WHERE DisplayOnce=0 OR RowNumber=1
		ORDER BY Program_Seq, ML_Seq, Field_Seq
		FOR JSON PATH
	)
END
