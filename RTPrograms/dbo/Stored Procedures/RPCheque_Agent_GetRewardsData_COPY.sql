﻿

CREATE PROCEDURE [dbo].[RPCheque_Agent_GetRewardsData_COPY] @SEASON INT,  @USERNAME VARCHAR(150), @FIELDVALUE VARCHAR(60), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN

		SET NOCOUNT ON;	
		DECLARE @STATEMENTS AS TABLE (Statementcode INT);

		INSERT INTO @STATEMENTS(STATEMENTCODE)
		SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')
		
		SELECT LK_ST.SRC_StatementCode [statementcode],LK_ST.StatementCode [locked_statementcode],LK_ST.DataSetCode ,LK_ST.DataSetCode_L5
			,IIF(DATENAME(MONTH,LK_ST.LOCKED_DATE)='July','July Early',DATENAME(MONTH,LK_ST.LOCKED_DATE)) AS PaymentMonth
			,LK_ST.retailercode,RP.RetailerName	
			,RP.MailingAddress1	 AS Address1
			,RP.MailingAddress2	 AS Address2
			,RP.MailingCity	As City
			,RP.MailingProvince	AS Province
			,RP.MailingPostalCode AS PostalCode
			,RP.SalesRep ,RP.Territory
			,ISNULL(HORT.HortRep,'') AS HortRep, ISNULL(HORT.HortTerritory,'') AS HortTerritory
			,LK_ST.Region,RP.RetailerLanguage
			,LK_ST.level5code, ISNULL(RP5.RetailerName,'') AS Level5Name
			,LK_ST.level2code, ISNULL(RP2.RetailerName,'') AS Level2Name
			,LK_ST.StatementLevel				
			,UL_STI.FieldValue as [Status]																	
			,RS.totalreward,RS.paidtodate,RS.currentpayment,RS.nextpayment
			,RP.PayableTo			
		FROM @STATEMENTS ST
			INNER JOIN RPWeb_Statements LK_ST				ON LK_ST.StatementCode=ST.StatementCode			
			INNER JOIN RPWeb_StatementInformation LK_STI 	ON LK_STI.StatementCode=ST.StatementCode
			INNER JOIN RPWeb_StatementInformation UL_STI	ON UL_STI.StatementCode=LK_ST.SRC_StatementCode AND UL_STI.FieldName='Status'			
			INNER JOIN (			
				SELECT StatementCode
					,SUM(TotalReward) AS TotalReward
					,SUM(PaidToDate) AS PaidToDate
					,SUM(CurrentPayment) AS CurrentPayment
					,SUM(NextPayment) AS NextPayment
				FROM RPWeb_Rewards_Summary
				GROUP BY StatementCode 
			) RS						
			ON RS.StatementCode=ST.StatementCode				
			INNER JOIN RetailerProfile RP		ON RP.RetailerCode=LK_ST.RetailerCode
			LEFT JOIN RetailerProfile RP5 		ON RP5.RetailerCode=RP.Level5Code			
			LEFT JOIN RetailerProfile RP2 		ON RP.RetailerType IN ('Level 1','Level 2') AND RP2.RetailerCode=RP.Level2Code			
			LEFT JOIN (
				SELECT Retailercode, HortRep, HortTerritory 
				FROM (
					SELECT RetailerCode, IsPrimary, EmployeeName AS HortRep, ClassificationName as HortTerritory, RANK() OVER (PARTITION BY RetailerCode ORDER BY ISNULL(IsPrimary,'No') DESC) AS [Priority]
					FROM RetailerAccess
					WHERE EmployeeRole = 'Hort' 
				) H
				WHERE [Priority]=1
			) HORT
			ON HORT.RetailerCode=RP.RetailerCode
		WHERE LK_ST.Season=@SEASON 	AND LK_STI.FieldName='CreateChequeStatus' AND LK_STI.FieldValue=@FieldValue			
	
END




