﻿


CREATE PROCEDURE [dbo].[RP_RefreshScenarios] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SEASON_VARCHAR VARCHAR(4)=CAST(@SEASON AS VARCHAR(4));

	DECLARE @TEMP_SCENARIOS TABLE(
		Season INT NOT NULL,
		RetailerCode Varchar(20) NOT NULL,
		ScenarioID  Varchar(20) NOT NULL,
		[Status] Varchar(20) NOT NULL,
		Created DATETIME,
		Modified DATETIME,
		[Seq1] INT,
		[Seq2] INT
	)
	   
	DELETE T1
	FROM RP_DT_Scenarios  T1
		INNER JOIN @STATEMENTS T2		ON T2.StatementCode=T1.StatementCode
		INNER JOIN RP_Statements T3		ON T3.StatementCode=T1.StatementCode
	WHERE T3.VersionType <> 'USE CASE'

	--D0000117 FCL COOPS - BR - RETAIL CONNECT	
	IF @STATEMENT_TYPE IN ('FORECAST','ACTUAL')
		INSERT INTO @TEMP_SCENARIOS(Season, RetailerCode, ScenarioID, Status, Created, Modified, Seq1, Seq2)
		Select Season, RetailerID AS RetailerCode, ScenarioID, [Status], Created, Modified
			,CASE WHEN [Status]='FINAL' THEN 1 
				  WHEN [Status]='PREFERRED' THEN 2 ELSE 3 END AS [Seq1]		
			,1 AS [Seq2]
		FROM Retail_Plan_Scenarios				
		WHERE Season=@SEASON_VARCHAR AND ScenarioType='FORECAST'  AND [Status] IN ('FINAL','PREFERRED','DRAFT') 	

	ELSE IF @STATEMENT_TYPE='PROJECTION'
		INSERT INTO @TEMP_SCENARIOS(Season, RetailerCode, ScenarioID, Status, Created, Modified, Seq1, Seq2)
		Select Season, RetailerID AS RetailerCode, ScenarioID, [Status], Created, Modified
			,IIF([Status]='PROJECTED', 1, 2) AS [Seq1]		
			,ROW_NUMBER() OVER(ORDER BY [Modified] DESC)  AS [Seq2]	
		FROM Retail_Plan_Scenarios				
		WHERE Season=@SEASON_VARCHAR AND ScenarioType='PROJECTION'  AND [Status] IN ('PROJECTED','DRAFT') 	

	
	--D0000117 FCL COOPS - BR - RETAIL CONNECT
	INSERT INTO RP_DT_Scenarios(StatementCode, DataSetCode, RetailerCode, Season, ScenarioCode, Status, DateModified) --, PreliminaryDate, EarliestFinalDate
	SELECT ST.StatementCode, RS.DataSetCode, POG.RetailerCode, POG.Season, POG.ScenarioID, POG.Status, POG.Modified
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode 
		INNER JOIN RetailerProfile RP			ON RP.RetailerCode=RS.RetailerCode
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT CAST(Season AS INT) AS [Season], RetailerCode, ScenarioID, [Status], Created, Modified
			FROM (
				SELECT *,ROW_NUMBER() OVER(PARTITION BY RetailerCode,Season ORDER BY [Seq1],[Seq2])  AS [Priority]
				FROM @TEMP_SCENARIOS
			) D
			WHERE [Priority]=1			
		) POG
		ON POG.RetailerCode=MAP.ActiveRetailerCode  -- POG.RetailerCode=MAP.RetailerCode 
	WHERE RS.StatementType=@STATEMENT_TYPE AND RS.VersionType <> 'USE CASE' 		
		AND (RS.Region='EAST' OR RP.Level5Code = 'D0000117')

			
	-- D000001 INDEPENDENT - RC REPS - RC REP TOOL
	-- RC REPS DEAL WITH WEST RETAILERS ONLY
	INSERT INTO RP_DT_Scenarios(StatementCode, DataSetCode, RetailerCode, Season, ScenarioCode, Status, DateModified) --, PreliminaryDate, EarliestFinalDate	
	SELECT ST.StatementCode, RS.DataSetCode, POG.RetailerCode, @Season, CAST(POG.ID AS VARCHAR(20)) AS ScenarioID, 'FINAL' AS [Status]
		,ISNULL(tx.DateModified,POG.SYS_Start) AS DateModified
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON ST.StatementCode=RS.StatementCode
		INNER JOIN RetailerProfile RP			ON RP.RetailerCode=RS.RetailerCode		
		INNER JOIN RP_StatementsMappings MAP	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN RCT_Scenarios POG			ON POG.RetailerCode=MAP.ActiveRetailerCode AND POG.[Primary]=1   -- POG.RetailerCode=MAP.RetailerCode
		INNER JOIN (
			SELECT ID 
			FROM RCT_PlanningModes 
			WHERE [Season]=@Season AND [Type]=IIF(@STATEMENT_TYPE='PROJECTION','PROJECTION','PLAN')
				--AND [Status]='Active'
		) PM
		ON PM.ID=POG.PlanningModeID 
		LEFT JOIN (
			SELECT ScenarioID as ID, MIN(UserModified_Date) AS DateModified
			FROM RCT_ScenarioDetails 
			GROUP BY ScenarioID
		)	tx
		ON tx.ID=POG.ID			
	WHERE RP.Level5Code='D000001' AND RS.VersionType <> 'USE CASE'
	
END
