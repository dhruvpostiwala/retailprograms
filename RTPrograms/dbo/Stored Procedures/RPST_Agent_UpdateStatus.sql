﻿

CREATE PROCEDURE [dbo].[RPST_Agent_UpdateStatus] @SEASON INT,  @USERNAME VARCHAR(150), @Status VARCHAR(100), @STATEMENTCODES VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @STATEMENTS UDT_RP_STATEMENT;
	DECLARE @TARGET_STATEMENTS UDT_RP_STATEMENT_TO_REFRESH;
	DECLARE @EDIT_HISTORY RP_EDITHISTORY;

	DECLARE @OLD_STATUS VARCHAR(100);
	
	BEGIN TRY

	-- PASSED IN STATEMENTS (UNLOCKED STATEMENTCODES)
	INSERT INTO @STATEMENTS(StatementCode)
	SELECT DISTINCT VALUE AS StatementCode FROM String_Split(@STATEMENTCODES, ',')


	-- ALL STATEMENTS CODES TO BE INCLUDED FROM FAMILY
	INSERT INTO @TARGET_STATEMENTS(StatementCode, StatementLevel, DataSetCode)
	SELECT StatementCode,StatementLevel,DataSetCode 
	FROM TVF_Get_StatementCodesToRefresh(@STATEMENTS) 

	/* ON UNLOCKED STATEMENT: UPDATE STATUS TO @STATUS */
	IF @STATUS='READY FOR QA'
		SET @OLD_STATUS='OPEN'
	ELSE IF @STATUS='OPEN'
		SET @OLD_STATUS='READY FOR QA'


	IF @STATUS IN ('READY FOR QA','OPEN')
	BEGIN
		UPDATE STI
		SET [FieldValue]=@Status
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'QA process - Update','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI		
			INNER JOIN @TARGET_STATEMENTS TS	ON TS.StatementCode=STI.StatementCode				
		WHERE STI.[FieldName]='Status'  AND STI.[FieldValue]=@OLD_STATUS
	END
	
	EXEC RP_GenerateEditHistory 'Statement', @USERNAME, @EDIT_HISTORY	

	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH
	
	SELECT 'success' AS [Status], '' AS Error_Message, '' as confirmation_message, '' as warning_message

END




