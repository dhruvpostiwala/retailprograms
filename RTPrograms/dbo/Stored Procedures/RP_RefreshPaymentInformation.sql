﻿CREATE PROCEDURE [dbo].[RP_RefreshPaymentInformation] @SEASON INT, @STATEMENT_TYPE VARCHAR(50),  @STATEMENTS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION','ACTUAL - USE CASE','REWARD PLANNER')
	BEGIN
		UPDATE RPWeb_Rewards_Summary
		SET [TotalReward]=[RewardValue]
		WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)
		RETURN
	END
	  
	EXEC RP_Refresh_HO_RewardsSummary  @SEASON, @STATEMENT_TYPE, @STATEMENTS
		
	DECLARE @TARGET_STATEMENTS TABLE( 
		Statementcode INT,				
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)
	
	INSERT INTO @TARGET_STATEMENTS(Statementcode)
	SELECT StatementCode FROM @STATEMENTS 
		
		UNION

	SELECT DataSetCode_L5
	FROM RPWeb_Statements
	WHERE StatementCode IN (SELECT Statementcode FROM @STATEMENTS)
	GROUP BY DataSetCode_L5

	-- CHEQUES PAID
	DECLARE @PAYMENTS_PAID TABLE (
		Statementcode INT NOT NULL,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)

	INSERT INTO @PAYMENTS_PAID(StatementCode)
	SELECT ARC.Statementcode
	FROM @TARGET_STATEMENTS	 UL
		INNER JOIN RPWeb_Statements ARC					ON ARC.SRC_Statementcode=UL.StatementCode
		INNER JOIN RPWeb_StatementInformation STI		ON STI.Statementcode=ARC.Statementcode
	WHERE ARC.VersionType='Archive' AND Arc.[Status] IN ('Active','Error') AND STI.FieldName='PAYMENT_STATUS' AND STI.FieldValue IN ('Sent','Cashed','Submitted To Bank','Cleared','Paid')
	GROUP BY ARC.[Statementcode]
	

	INSERT INTO @PAYMENTS_PAID(StatementCode)
	SELECT ARC.Statementcode
	FROM RPWeb_Statements Arc
		LEFT JOIN @PAYMENTS_PAID DS5	ON ARC.DataSetCode_L5=DS5.StatementCode
		LEFT JOIN @PAYMENTS_PAID DS2	ON ARC.DataSetCode=DS2.StatementCode		
		LEFT JOIN @PAYMENTS_PAID T2		ON T2.StatementCode=Arc.Statementcode
	WHERE Arc.VersionType='Archive' AND Arc.[Status] IN ('Active','Error') 		
		AND Arc.StatementLevel IN ('LEVEL 1','LEVEL 2')		
		AND COALESCE(DS5.Statementcode,DS2.StatementCode,0) < 0
		AND T2.StatementCode IS NULL
	
		
	/****************** PAYMENTS IN PROGRESS : CURRENT PAYMENT ************************/		
	DECLARE @PAYMENTS_IN_PROGRESS TABLE (
		Statementcode INT NOT NULL,
		PRIMARY KEY CLUSTERED (
			Statementcode ASC
		)
	)
	
	INSERT INTO @PAYMENTS_IN_PROGRESS(Statementcode)
	SELECT LCK.[StatementCode] 
	FROM @TARGET_STATEMENTS UL
		INNER JOIN RPWeb_Statements LCK		ON LCK.[SRC_StatementCode]=UL.[StatementCode]
	WHERE LCK.[VersionType]='Locked' AND LCK.[Status]='Active'		   		 	  	  	 
	GROUP BY LCK.[StatementCode]
		
		UNION 

	SELECT C.[StatementCode]
	FROM @TARGET_STATEMENTS UL
		INNER JOIN RPWeb_Statements ARC		ON ARC.SRC_StatementCode=UL.StatementCode 
		INNER JOIN (
			SELECT StatementCode
			FROM RPWeb_Cheques  
			WHERE VersionType='Current' AND [ChequeStatus] IN ('New','Ready to Print','Printed','QA Completed') 
			GROUP BY StatementCode
		) C 
		ON C.StatementCode=ARC.StatementCode
	WHERE ARC.VersionType='Archive' AND ARC.[Status] IN ('Active','Error') 		
		

	INSERT INTO @PAYMENTS_IN_PROGRESS(Statementcode)
	SELECT ARC.Statementcode
	FROM RPWeb_Statements Arc
		LEFT JOIN @PAYMENTS_IN_PROGRESS DS5	ON ARC.DataSetCode_L5=DS5.StatementCode
		LEFT JOIN @PAYMENTS_IN_PROGRESS DS2	ON ARC.DataSetCode=DS2.StatementCode		
		LEFT JOIN @PAYMENTS_IN_PROGRESS T2  ON T2.Statementcode=Arc.Statementcode
	WHERE Arc.VersionType='Archive' AND Arc.[Status] IN ('Active','Error')
		AND Arc.StatementLevel IN ('LEVEL 1','LEVEL 2')	
		AND COALESCE(DS5.Statementcode,DS2.StatementCode,0) < 0
		AND T2.StatementCode IS NULL
	GROUP BY ARC.Statementcode

	   	   	   		 
	/* PAID TO DATE */
	/* sum up the values on all of the cheques that are "sent" or "cashed" */
	DECLARE @PAID_TO_DATE AS UDT_RP_PAYMENTINFO	
	INSERT INTO @PAID_TO_DATE([StatementCode],[MarketLetterCode],[RewardCode],[RewardAmount])
	SELECT UL.Statementcode, Rew.MarketLetterCode, Rew.RewardCode,SUM(ISNULL(Rew.[CurrentPayment],0)) AS [RewardAmount] 
	FROM @TARGET_STATEMENTS UL
		INNER JOIN RPWeb_Statements ARC			ON ARC.SRC_Statementcode=UL.Statementcode 
		INNER JOIN @PAYMENTS_PAID CHQ			ON CHQ.Statementcode=ARC.Statementcode
		INNER JOIN RPWeb_Rewards_Summary Rew	ON Rew.Statementcode=CHQ.Statementcode
	--WHERE Arc.VersionType='Archive' AND ARC.[Status] IN ('Active','Error')
	GROUP BY UL.[StatementCode],Rew.MarketLetterCode, Rew.RewardCode		   	 			
	

	/* reset all the paid to date values to zero */
	UPDATE RPWeb_Rewards_Summary
	SET [PaidToDate]=0
	WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TARGET_STATEMENTS)
	
	/* update paid to date values of records that already exist in the reward summary table */
	UPDATE T1
	SET t1.[PaidToDate]=t2.[RewardAmount]
	FROM RPWeb_Rewards_Summary t1
		INNER JOIN @PAID_TO_DATE t2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.MarketLetterCode=T2.MarketLetterCode AND t1.[RewardCode]=t2.[RewardCode]
	
	/* insert any records that do not exist in the reward summary table that now have a paid to date value */	
	INSERT INTO RPWeb_Rewards_Summary([StatementCode],[MarketLetterCode],[RewardCode],[RewardValue],[RewardEligibility],[TotalReward],[PaidToDate],[CurrentPayment],[NextPayment])
	SELECT t1.[StatementCode],t1.MarketLetterCode,t1.[RewardCode],0,'ELIGIBLE',0,[RewardAmount],0,0
	FROM @PAID_TO_DATE t1
		LEFT JOIN RPWeb_Rewards_Summary t2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.MarketLettercode=t2.MarketLetterCode AND t1.[RewardCode]=t2.[RewardCode]
	WHERE t2.[StatementCode] IS NULL AND t1.RewardAmount <> 0
	
		
	/* CURRENT PAYMENT */
	DECLARE @CURRENT_PAYMENT AS UDT_RP_PAYMENTINFO
	INSERT INTO @CURRENT_PAYMENT ([StatementCode],[MarketLetterCode],[RewardCode],[RewardAmount])
	SELECT UL.Statementcode, Rew.MarketLetterCode, Rew.RewardCode,SUM(ISNULL(Rew.[CurrentPayment],0)) AS [RewardAmount] 
	FROM @TARGET_STATEMENTS UL
		INNER JOIN RPWeb_Statements ARC			ON ARC.SRC_Statementcode=UL.Statementcode 
		INNER JOIN @PAYMENTS_IN_PROGRESS CHQ	ON CHQ.Statementcode=ARC.Statementcode
		INNER JOIN RPWeb_Rewards_Summary Rew	ON Rew.Statementcode=CHQ.Statementcode
	--WHERE arc.[Status] IN ('Active','Error')
	GROUP BY UL.[StatementCode],Rew.MarketLetterCode, Rew.RewardCode
		
	/* reset all the current payment values to zero */
	UPDATE RPWeb_Rewards_Summary
	SET [CurrentPayment]=0
	WHERE [StatementCode] IN (SELECT [StatementCode] FROM @TARGET_STATEMENTS)
	   	
	/* update paid to date values of records that already exist in the reward summary table */
	UPDATE T1
	SET t1.[CurrentPayment]=t2.[RewardAmount]
	FROM RPWeb_Rewards_Summary t1
		INNER JOIN @CURRENT_PAYMENT t2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.MarketLettercode=t2.MarketLetterCode AND t1.[RewardCode]=t2.[RewardCode]
	
	/* insert any records that do not exist in the reward summary table that now have a paid to date value */	
	INSERT INTO RPWeb_Rewards_Summary([StatementCode],[MarketLettercode],[RewardCode],[RewardValue],[RewardEligibility],[TotalReward],[PaidToDate],[CurrentPayment],[NextPayment])
	SELECT t1.[StatementCode],t1.MarketLetterCode,t1.[RewardCode],0,'ELIGIBLE',0,0,[RewardAmount],0
	FROM @CURRENT_PAYMENT t1
		LEFT JOIN RPWeb_Rewards_Summary t2
		ON t1.[StatementCode]=t2.[StatementCode] AND t1.MarketLettercode=t2.MarketLetterCode AND t1.[RewardCode]=t2.[RewardCode]
	WHERE t2.[StatementCode] IS NULL AND t1.RewardAmount <> 0

	/* SET TOTAL REWARD + NEXT PAYMENT */
	UPDATE rs1
	SET [TotalReward]=IIF([RewardEligibility]='ELIGIBLE',[RewardValue],0)
		,[NextPayment]=IIF([RewardEligibility]='ELIGIBLE',[RewardValue],0)-[PaidToDate]-[CurrentPayment]
	FROM RPWeb_Rewards_Summary rs1
		INNER JOIN RPWeb_Statements gs				ON gs.[StatementCode]=rs1.[StatementCode]
		INNER JOIN @TARGET_STATEMENTS st			ON st.[StatementCode]=rs1.[StatementCode]
		INNER JOIN RP_Config_Rewards_Summary CFG	ON CFG.RewardCode=RS1.RewardCode AND CFG.Season=GS.Season	
	WHERE gs.[VersionType] IN ('Unlocked','Use Case') 
			
END

