﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_E_POGSalesBonus]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,		
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)

	DECLARE @SCENARIOS TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL, 
		[ScenarioCode] [varchar](20) NOT NULL,
		[ScenarioStatus] [varchar](20) NOT NULL,
		[Cut_Off_Date] [Date] NOT NULL
	)

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL
	)

	DECLARE @RewardPercentage Float=0.02;
	
	DECLARE @PLAN_CUT_OFF_DATE AS DATE

	SET @PLAN_CUT_OFF_DATE=CAST('2020-02-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED


	-- THIS IS A STRAIGHT FORWARD 2% REWARD.
	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel,SUM(tx.Price_CY) AS RewardBrand_Sales		
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS		ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX 	ON TX.StatementCode=ST.StatementCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='Reward_Brands' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,tx.GroupLabel
	
	INSERT INTO @SCENARIOS(StatementCode,RetailerCode,MarketLetterCode,ScenarioCode,ScenarioStatus,Cut_Off_Date)
	SELECT Statementcode, RetailerCode, 'ML2020_E_INV' as MarketLetterCode, ScenarioCode, ScenarioStatus, @PLAN_CUT_OFF_DATE AS Cut_Off_Date		
	FROM [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @STATEMENTS)
	
	/*
	-- SCENARIO TRANSACTIONS AS OF CUT OFF DATE
	INSERT INTO @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP)
	SELECT SCN.Statementcode, SCN.MarketLetterCode, GR.GroupLabel
		,SUM(TX.QTY * PRP.SDP) AS SDP
		,SUM(TX.QTY * PRP.SRP) AS SRP
	FROM @SCENARIOS SCN
		INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
		INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
		INNER JOIN RP_Config_Groups_Brands BR					ON BR.ChemicalGroup=PR.ChemicalGroup
		INNER JOIN RP_Config_Groups  GR							ON GR.GroupID=BR.GroupID								
		INNER JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='EAST'
	WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
		AND SCN.MarketLetterCode=GR.MarketLetterCode  AND CAST(tx.UpdateDate AS DATE) <= SCN.Cut_Off_Date
	GROUP BY SCN.Statementcode, SCN.MarketLetterCode, GR.GroupLabel
			  		
	UPDATE T1
	SET T1.Reward_Percentage=@RewardPercentage, T1.Reward = RewardBrand_Sales * @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN @SCENARIO_TRANSACTIONS tx
		ON tx.Statementcode=T1.Statementcode 			
	WHERE ST.VersionType='Unlocked' AND ST.StatementType='Actual' AND tx.SDP > 0
	

	-- USE CASE STATEMENTS
	UPDATE T1
	SET T1.Reward_Percentage=@RewardPercentage, T1.Reward = RewardBrand_Sales * @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN (
			SELECT tx.Statementcode, SUM(PRICE_SDP) AS Plan_Amount
			FROM RP_DT_ScenarioTransactions tx
					INNER JOIN @STATEMENTS ST					ON ST.StatementCode=Tx.Statementcode
					INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
					INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
			WHERE GR.MarketLetterCode = 'ML2020_E_INV' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'											
			AND tx.UC_DateModified <= @PLAN_CUT_OFF_DATE
			GROUP BY tx.Statementcode, GR.GroupLabel
		) T2
		ON T2.StatementCode=T1.StatementCode
	WHERE ST.VersionType='Use Case' AND ST.StatementType='Actual' AND  T2.Plan_Amount > 0
	*/

	-- RP-2887

	DROP TABLE IF EXISTS #SCENARIO_TRANSACTIONS

	SELECT DISTINCT StatementCode
	INTO #SCENARIO_TRANSACTIONS
	FROM (
		SELECT SCN.Statementcode, TX.productCode
		FROM @SCENARIOS SCN
			INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
			INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
		WHERE pr.docType = 10 AND CAST(tx.UpdateDate AS DATE) <= SCN.Cut_Off_Date
		GROUP BY SCN.Statementcode, TX.productCode
		HAVING SUM(TX.QTY) > 0
	) T

	UPDATE T1
	SET T1.Reward_Percentage=@RewardPercentage, T1.Reward = RewardBrand_Sales * @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN #SCENARIO_TRANSACTIONS tx
		ON tx.Statementcode= IIF(ST.DataSetCode > 0, ST.DataSetCode, ST.StatementCode) 
	WHERE ST.VersionType='Unlocked' AND ST.StatementType='Actual'

	-- USE CASE STATEMENTS
	UPDATE T1
	SET T1.Reward_Percentage=@RewardPercentage, T1.Reward = RewardBrand_Sales * @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN (
			SELECT tx.Statementcode, SUM(PRICE_SDP) AS Plan_Amount
			FROM RP_DT_ScenarioTransactions tx						
			WHERE tx.UC_DateModified <= @PLAN_CUT_OFF_DATE
			GROUP BY tx.Statementcode
		) T2
		ON T2.StatementCode=T1.StatementCode
	WHERE ST.VersionType='Use Case' AND ST.StatementType='Actual' AND  T2.Plan_Amount > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END
	

	-- INSERT INTO DT TABEL FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_POGSalesBonus WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_POGSalesBonus(StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward)
	SELECT StatementCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,Reward_Percentage,Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,AVG(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand

END
