﻿CREATE PROCEDURE [dbo].[RP2022_Calc_W_BaseReward] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20), @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;
	
	/*
	DECLARE @SEASON INT = 2022;
	DECLARE @STATEMENT_TYPE VARCHAR(50) = 'REWARD PLANNER'
	DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2022_W_BASE_REWARD'
	DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	INSERT INTO @STATEMENTS VALUES (7984)
	*/

	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;
	DECLARE @Percentage_Coop DECIMAL(6,4) = 0.045;
	DECLARE @Percentage_Ind1 DECIMAL(6,4) = 0.0775; -- Retail < $1M
	DECLARE @Percentage_Ind2 DECIMAL(6,4) = 0.0925; -- Retail > $1M

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code  VARCHAR (10) NOT NULL DEFAULT '',
		Total_Sales MONEY NOT NULL DEFAULT 0,
		RewardBrand_Sales MONEY NOT NULL DEFAULT 0,
		Order_Target_Achieved DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)
	
	DECLARE @TOTAL_SALES TABLE (
		StatementCode INT,		
		Total_Sales MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC
		)
	)

	-- NEW: RRT-2447 - Currently, reward 9.25% if greater than $1 Million in POG or 7.25% if less than $1 Million in POG. Qualification based on edit inputs and all must be set to "Yes" to receive a reward.
	--		Update: for every input turned off the reward amount will decrease by 1% margin.
	--		I am implementing this by going the other way, giving an additional 1% for each Yes starting at 6.25% (>1M) and 4.25% (<1M)

	-- NEW: RC-4724 - The Base Reward Edit Input is specific to Independents in RCSC, so just give the reward to Coops in RC
	-- NEW: RC-4689 - If 1 or more of the 3 toggles is switched to No, then the user does not qualify for the base reward.  All 3 must be YES to qualify
	-- The editable inputs have been changed from a input box to a Yes = 100 or No = 0 toggle.
	-- NEW: Apply Reward Planner %s
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION', 'REWARD PLANNER')
	BEGIN
		-- Get data for all the statements
		INSERT INTO @TEMP(StatementCode, MarketLetterCode, ProgramCode, Level5Code, RewardBrand_Sales, Order_Target_Achieved)
		SELECT ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code
			,SUM(IIF(TX.GroupType = 'REWARD_BRANDS', TX.Price_CY * ISNULL(EI4.FieldValue,100)/100, 1) * IIF(@STATEMENT_TYPE = 'REWARD PLANNER', COALESCE(BI2.PercentageValue, BI.PercentageValue, 1), 1)) AS RewardBrand_Sales
			--,MAX(IIF(ISNULL(EI1.FieldValue, 0) + ISNULL(EI2.FieldValue, 0) + ISNULL(EI3.FieldValue, 0) = 300, 1, 0)) AS Order_Target_Achieved
			,MAX(IIF(ISNULL(EI1.FieldValue, 0) = 100, 0.01, 0) + IIF(ISNULL(EI2.FieldValue, 0) = 100, 0.01, 0) + IIF(ISNULL(EI3.FieldValue, 0) = 100, 0.01, 0)) AS Order_Target_Achieved
		FROM @STATEMENTS ST
			INNER JOIN RP2022_DT_Sales_Consolidated TX ON TX.StatementCode = ST.StatementCode
			INNER JOIN ProductReference PR ON PR.ProductCode = TX.ProductCode
			LEFT JOIN RPWeb_User_EI_FieldValues EI1 ON EI1.StatementCode = ST.StatementCode AND EI1.MarketLetterCode = TX.MarketLetterCode AND EI1.FieldCode = 'Order_Target1'
			LEFT JOIN RPWeb_User_EI_FieldValues EI2 ON EI2.StatementCode = ST.StatementCode AND EI2.MarketLetterCode = TX.MarketLetterCode AND EI2.FieldCode = 'Order_Target2'
			LEFT JOIN RPWeb_User_EI_FieldValues EI3 ON EI3.StatementCode = ST.StatementCode AND EI3.MarketLetterCode = TX.MarketLetterCode AND EI3.FieldCode = 'Order_Target3'
			LEFT JOIN RPWeb_User_EI_FieldValues EI4 ON EI4.StatementCode = ST.StatementCode AND EI4.MarketLetterCode = TX.MarketLetterCode AND EI4.FieldCode = 'Radius_Percentage_InVigor'
			LEFT JOIN RPWeb_User_BrandInputs BI ON BI.StatementCode = TX.StatementCode AND BI.ChemicalGroup = PR.ChemicalGroup
			LEFT JOIN RPWeb_User_BrandInputs BI2 ON BI2.StatementCode = TX.StatementCode AND BI2.ChemicalGroup = PR.Product
		WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType IN ('REWARD_BRANDS')
		GROUP BY ST.StatementCode, TX.MarketLetterCode, TX.ProgramCode, TX.Level5Code

		-- NEW: RC-4724 - The Base Reward Edit Input is specific to Independents in RCSC, so just give the reward to Coops in RC
		UPDATE @TEMP SET Order_Target_Achieved = 0.03 WHERE Level5Code = 'D0000117'

	END

	--IF @STATEMENT_TYPE IN ('ACTUAL')
	--BEGIN
		--TBD
	--END

	-- Get Total sales	for all statements, this is for Independent retailers
	-- Retail under $1 Million gets 7.75% reward
    -- Retail over $1 Million	gets 9.25% reward
	INSERT INTO @TOTAL_SALES(StatementCode, Total_Sales)
	SELECT TX.StatementCode, SUM(TX.Price_CY) 
	FROM @TEMP Temp
	INNER JOIN RP2022_DT_Sales_Consolidated TX ON Temp.StatementCode = TX.StatementCode 
	WHERE TX.ProgramCode IN ('ELIGIBLE_SUMMARY', 'NON_REWARD_BRANDS_SUMMARY')
	GROUP BY TX.StatementCode

	-- UPDATE Total sales in the TEMP table
	UPDATE Temp  
	SET Total_Sales = TS.Total_Sales
	FROM @TEMP Temp
	LEFT JOIN @TOTAL_SALES TS ON Temp.StatementCode = TS.StatementCode 

	-- Update the reward percentage for all the statements
	UPDATE @TEMP SET [Reward_Percentage] = @Percentage_Coop + (Order_Target_Achieved - 0.03) WHERE Level5Code = 'D0000117'
	UPDATE @TEMP SET [Reward_Percentage] = IIF(Total_Sales < 1000000, @Percentage_Ind1 + (Order_Target_Achieved - 0.03), @Percentage_Ind2 + (Order_Target_Achieved - 0.03)) WHERE Level5Code = 'D000001'
	                                    
	-- Set the reward for all the statements based on the reward percentage set previously
	UPDATE @TEMP SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE [RewardBrand_Sales] > 0

	DELETE FROM RP2022_DT_Rewards_W_INV_BaseReward WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2022_DT_Rewards_W_INV_BaseReward(StatementCode, MarketLetterCode, ProgramCode, Order_Target_Achieved, Total_Sales, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, Order_Target_Achieved, Total_Sales, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

END
