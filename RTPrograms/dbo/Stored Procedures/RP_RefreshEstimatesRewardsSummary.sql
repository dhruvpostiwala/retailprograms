﻿
CREATE PROCEDURE [dbo].[RP_RefreshEstimatesRewardsSummary] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DROP TABLE IF EXISTS #RPWeb_Rewards_Summary	
	
	DECLARE @MARKET_LETTER_CODE VARCHAR(50)='ESTIMATE_OCT_W'
	   
	IF @SEASON >= 2021
	BEGIN		
		SELECT ST.StatementCode AS LK_StatementCode, ST.SRC_Statementcode AS UL_StatementCode, ST_LK.RewardCode, Amount
		INTO #RPWeb_Rewards_Summary
		FROM @STATEMENTS T1
			INNER JOIN RPWeb_Statements ST		ON ST.SRC_Statementcode=T1.Statementcode
			INNER JOIN RP_Config_ChequeRuns CR	ON CR.ID=ST.ChequeRunID
			INNER JOIN 	(
				SELECT Statementcode, 'EST_OCT_ADJ_INC_W' AS RewardCode, Amount 
				FROM RPWeb_Est_Incentives 				

					UNION 				 

				SELECT Statementcode, 'EST_OCT_ADJ_HERB_W' AS RewardCode, Amount 
				FROM RPWeb_Est_HeatDistinctTopUp			
				
			) ST_LK
			ON ST_LK.StatementCode=ST.Statementcode			
		WHERE ST.[Status] = 'Active' AND ST.VersionType = 'Locked' AND CR.ChequeRunName='OCT RECON'


		DELETE T1
		FROM RPWeb_Rewards_Summary 	T1
			INNER JOIN (
				SELECT LK_StatementCode AS StatementCode, RewardCode FROM #RPWeb_Rewards_Summary
					UNION
				SELECT UL_StatementCode AS StatementCode, RewardCode FROM #RPWeb_Rewards_Summary
			) T2
			ON T2.StatementCode = T1.StatementCode AND T2.RewardCode=T1.RewardCode
		WHERE T1.MarketLetterCode = @MARKET_LETTER_CODE
						
		INSERT INTO RPWeb_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)		
		SELECT LK_StatementCode,@MARKET_LETTER_CODE,RewardCode, 0, 'Eligible','',0 ,0 ,Amount ,0 ,Amount	
		FROM #RPWeb_Rewards_Summary 
		WHERE Amount <> 0
	
			UNION 

		SELECT UL_StatementCode,@MARKET_LETTER_CODE,RewardCode, 0, 'Eligible','',0 ,0 ,0, Amount * -1 ,Amount	
		FROM #RPWeb_Rewards_Summary 
		WHERE Amount <> 0
		
		RETURN

	END

	/*
	FOLLOWING IS THE CODE USED IN 2020
	/*
		PRIOR TO UPDATING REWARDS SUMMARY
		WE SHOULD UPDATE ESTIMATED TOP AMOUNTS VALUE 
	*/

	IF @SEASON=2020
		EXEC [dbo].[RP2020_Estimate_W_UpdateTopUpAmount] @SEASON, @STATEMENTS
	

	SELECT ST.StatementCode AS LK_StatementCode, ST.SRC_Statementcode AS UL_StatementCode, SUM(Amount) AS Amount
	INTO #RPWeb_Rewards_Summary
	FROM @STATEMENTS T1
		INNER JOIN RPWeb_Statements ST
		ON ST.SRC_Statementcode=T1.Statementcode
		INNER JOIN 	(
			SELECT Statementcode ,Sum(Amount) AS Amount
			FROM (
				SELECT Statementcode, Amount FROM RPWeb_Est_Incentives 
					UNION ALL				 
				SELECT Statementcode, Amount FROM RPWeb_Est_HeatDistinctTopUp
					UNION ALL				 
				SELECT Statementcode, Amount FROM RPWeb_Est_Exceptions
					UNION ALL				 
				SELECT Statementcode, Amount FROM RPWeb_Est_MarginTopUp				
				
			) D1
			GROUP BY StatementCode
		) ST_LK
		ON ST_LK.StatementCode=ST.Statementcode
	WHERE ST.Status='Active' AND ST.VersionType='Locked' AND ST.ChequeRunName='OCT RECON'
	GROUP BY ST.StatementCode, ST.SRC_Statementcode
	

	DELETE RPWeb_Rewards_Summary 
	WHERE RewardCode='EST_OCT_ADJ' AND StatementCode IN (
		SELECT LK_StatementCode	FROM #RPWeb_Rewards_Summary
			UNION
		SELECT UL_StatementCode FROM #RPWeb_Rewards_Summary
	)

	INSERT INTO RPWeb_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage,TotalReward,PaidToDate,CurrentPayment,NextPayment,PaymentAmount)		
	SELECT LK_StatementCode,'ESTIMATE','EST_OCT_ADJ', 0, 'Eligible','',0 ,0 ,Amount ,0 ,Amount	
	FROM #RPWeb_Rewards_Summary 
	
		UNION 

	SELECT UL_StatementCode,'ESTIMATE','EST_OCT_ADJ', 0, 'Eligible','',0 ,0 ,0, Amount * -1 ,Amount	
	FROM #RPWeb_Rewards_Summary 
	*/
END







