﻿


CREATE PROCEDURE [dbo].[RP2020_Calc_Rewards_Summary_East] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 392 OR StatementCode = 392

	DELETE T1
	FROM RP_DT_Rewards_Summary T1
		INNER JOIN @STATEMENTS ST
		ON ST.StatementCode=T1.StatementCode
		
	/*
	ProgramCode					Title							RewardCode
	RP2020_E_POG_SALES_BONUS	POG Sales Bonus Reward			POG_SALES_BONUS_E
	RP2020_E_INV_LOY_BONUS		Sales Support - Loyalty Bonus	SSLB_E
	RP2020_E_SUPPLY_SALES		Supply of Sales					SUPPLY_SALES_E
	*/

	-- RP2020_E_POG_SALES_BONUS	POG Sales Bonus Reward			POG_SALES_BONUS_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_POGSalesBonus RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_POG_SALES_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	
	-- RP2020_E_INV_LOY_BONUS		Sales Support - Loyalty Bonus	SSLB_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 		ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG			ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_SSLB RS		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_INV_LOY_BONUS'

	-- RP2020_E_SUPPLY_SALES		Supply of Sales					SUPPLY_SALES_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_SupplySales RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_SUPPLY_SALES'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode
	

	-- RP2020_E_PLANNING_SUPP_BUSINESS		Planning the Business Reward		PSB_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_PlanningTheBusiness RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_PLANNING_SUPP_BUSINESS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- RP2020_E_PORFOLIO_SUPPORT		Portfolio Support Reward	PORT_SUP_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_PortfolioSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_PORFOLIO_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- RP2020_E_CUSTOM_GROUND_APP_REWARD		Custom Ground Application Services Reward	CGAS_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 						ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG							ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_CustomGroundApplication RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_CUSTOM_GROUND_APP_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- RP2020_E_PURSUIT_REWARD		Pursuit Support Reward	PURS_SUP_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 			ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG				ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_PursuitSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_PURSUIT_REWARD'

	-- RP2020_E_ROW_CROP_FUNG_REWARD	Row Crop Fungicide Support Reward	ROW_CROP_FUNG_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_RowCropFungSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_ROW_CROP_FUNG_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- RP2020_E_HORTICULTURE_RETAIL_SUPPORT	Horticulture Retail Support Reward	HORT_RET_SUP_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 						ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG							ON PRG.ProgramCode=ELG.ProgramCode
		INNER JOIN RP2020_DT_Rewards_E_HortiCultureRetailSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_HORTICULTURE_RETAIL_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	-- RP2020_E_PRIAXOR_HEADLINE_REWARD		Priaxor Headline Support Reward		PRX_HEAD_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(RS.Reward,0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST			
		INNER JOIN RP_DT_ML_ELG_Programs ELG 				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_PriaxorHeadlineSupport RS	ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_PRIAXOR_HEADLINE_REWARD'

	-- RP2020_E_SOLLIO_GROWTH_REWARD	Sollio Growth Reward	SG_E
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, ISNULL(SUM(RS.Reward),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG 				ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG					ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2020_DT_Rewards_E_SollioGrowth RS		ON RS.STatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2020_E_SOLLIO_GROWTH_REWARD' 
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	--Incentive
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 
	'ML2020_E_ADD_PROGRAMS'AS MarketLetterCode, 
	'Incentives_E'AS RewardCode
	,ISNULL(SUM(RI.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP_DT_Incentives RI							ON RI.StatementCode = ST.StatementCode 
	GROUP BY ST.StatementCode

	--APA Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2020_E_ADD_PROGRAMS'AS MarketLetterCode, APA.RewardCode AS RewardCode
		,ISNULL(SUM(APA.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
	INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
	INNER JOIN RP_Seeding_APAPayments APA ON APA.RetailerCode = MAP.RetailerCode AND RS.Season = APA.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'EAST' --AND RS.StatementLevel = 'LEVEL 1'
	GROUP BY ST.StatementCode, APA.RewardCode
	
	--Over/Under Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2020_E_ADD_PROGRAMS'AS MarketLetterCode, OUP.RewardCode AS RewardCode
	,ISNULL(SUM(OUP.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
	INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
	INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
	INNER JOIN RP_Seeding_Over_Under_Payments OUP ON OUP.RetailerCode = MAP.RetailerCode AND RS.Season = OUP.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'EAST'
	GROUP BY ST.StatementCode, OUP.RewardCode
	

END
