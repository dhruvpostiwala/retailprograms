﻿

CREATE PROCEDURE [dbo].[RPST_Agent_Exceptions_Actions] @STATEMENTCODE INT,@USERNAME VARCHAR(150) ,@JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;	
	-- STATRED USING THIS CODE STARTING 2021
	-- PRIOR TO 2021 "RPST_Agent_Exceptions" was used

	DECLARE @ACTION VARCHAR(20) = UPPER(JSON_VALUE(@JSON, '$.action'));

	IF @ACTION = 'APPROVE' RETURN;
	
	DECLARE @SEASON INT;
	DECLARE @REGION VARCHAR(4);

	DECLARE @SRC_STATEMENTCODE INT;
	DECLARE @VERSION_TYPE VARCHAR(20);

	DECLARE @ExceptionID INT;
	DECLARE @SRC_ExceptionID INT;
	DECLARE @EXCEPTIONLEVEL VARCHAR(10);
	DECLARE @RETAILERCODE VARCHAR(50);
	DECLARE @EXCEPTIONDESC VARCHAR(200);
	DECLARE @EXCEPTIONTYPE VARCHAR(20);
	
	DECLARE @EXCEPTIONVALUE_A DECIMAL(18,2) --FLOAT;
	DECLARE @EXCEPTIONVALUE_B DECIMAL(18,2) --FLOAT;
	DECLARE @EXCEPTIONVALUE DECIMAL(18,2) -- FLOAT;
	DECLARE @EXCEPTIONMARGIN_A DECIMAL(8,5) --FLOAT;
	DECLARE @EXCEPTIONMARGIN_B DECIMAL(8,5) --FLOAT;
	DECLARE @EXCEPTIONMARGIN   DECIMAL(8,5) --FLOAT;
	

	DECLARE @REWARDCODE VARCHAR(30);
	DECLARE @COMMENTS VARCHAR(2000);
	DECLARE @MARKETLETTERCODE VARCHAR(50)

	DECLARE @ID TABLE (ID int)	
	
	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;

	BEGIN TRY
	BEGIN TRANSACTION
	   	SET @ExceptionID = JSON_VALUE(@JSON, '$.id')

		IF @ACTION='DELETE'
		BEGIN
			DELETE RPWeb_Exceptions 
			WHERE ID=@ExceptionID OR SRC_ID=@ExceptionID
		
			GOTO COMMIT_AND_EXIT
		END

		IF @ACTION='SUBMIT' 
		BEGIN					
			UPDATE [RPWeb_Exceptions] 
			SET Status = 'Submitted',Show_In_UL = 1
				OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			WHERE (ID=@ExceptionID OR SRC_ID=@ExceptionID)
		
			GOTO COMMIT_AND_EXIT
		END --END SUBMIT

		IF @ACTION='REJECT' 
		BEGIN
			UPDATE EX			
			SET Status = IIF(ST.Region='WEST','Draft','Rejected')
				OUTPUT inserted.[StatementCode], inserted.ID, 'Change in Status','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
			FROM [RPWeb_Exceptions] EX
				INNER JOIN RPWeb_Statements ST	ON ST.StatementCode=EX.StatementCode
			WHERE (ID=@ExceptionID OR SRC_ID=@ExceptionID)

			GOTO COMMIT_AND_EXIT
		END
	   	
		IF @ACTION = 'OBSOLETE' 
		BEGIN
			--GET RID OF EXCEPTIONS FROM RPWEB_Rewards_SUMMARY
			EXEC [dbo].[RPST_Agent_Obsolete_Exceptions] @ExceptionID, @REGION, @EXCEPTIONLEVEL, @USERNAME 
			GOTO COMMIT_AND_EXIT				
		END --END OBSOLETE


	
		SELECT @REGION=UPPER(Region)
			,@SEASON=Season
			,@SRC_STATEMENTCODE=IIF(VersionType='Locked',SRC_StatementCode,StatementCode) 		
			,@VERSION_TYPE=VersionType
		FROM RPWeb_Statements
		WHERE StatementCode = @STATEMENTCODE;
			
		SET @EXCEPTIONLEVEL=JSON_VALUE(@JSON, '$.exception_level')
		SET @RETAILERCODE = JSON_VALUE(@JSON, '$.retailercode')
		SET @EXCEPTIONDESC = JSON_VALUE(@JSON, '$.exception_desc')
		SET @EXCEPTIONTYPE= UPPER(JSON_VALUE(@JSON, '$.exception_type'))
		SET @EXCEPTIONVALUE_A = ISNULL(CONVERT(decimal(18,2), REPLACE(JSON_VALUE(@JSON, '$.exception_value_a'),',','')),0)
		SET @EXCEPTIONVALUE_B = ISNULL(CONVERT(decimal(18,2), REPLACE(JSON_VALUE(@JSON, '$.exception_value_b'),',','')),0)
		SET @EXCEPTIONVALUE = ISNULL(CONVERT(decimal(18,2), REPLACE(JSON_VALUE(@JSON, '$.exception_value'),',','')),0)
		SET @EXCEPTIONMARGIN_A=ISNULL(JSON_VALUE(@JSON, '$.exception_margin_a'),0)
		SET @EXCEPTIONMARGIN_B=ISNULL(JSON_VALUE(@JSON, '$.exception_margin_b'),0)
		SET @EXCEPTIONMARGIN=ISNULL(JSON_VALUE(@JSON, '$.exception_margin'),0)
		SET @REWARDCODE= JSON_VALUE(@JSON, '$.reward_code')
		SET @COMMENTS= JSON_VALUE(@JSON, '$.comments')
		SET @MARKETLETTERCODE=JSON_VALUE(@JSON, '$.market_letter_code')

		IF @EXCEPTIONTYPE = 'OTHER'	
			SET @REWARDCODE += '_' + LEFT(@REGION,1) + 'X'

		IF @ACTION='SAVE' 
		BEGIN			
			INSERT INTO [RPWeb_Exceptions](RetailerCode,[StatementCode],[Season], [RewardCode],Exception_Level,Exception_Desc, [Exception_Type],[Exception_Value_A],[Exception_Value_B], [Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode], [Comments], [MarketLetterCode], [DateCreated], [DateModified], [PostedBy])
			OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
			INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])			
			VALUES(@RetailerCode,@StatementCode, @SEASON,@REWARDCODE,@ExceptionLevel,@ExceptionDesc, @ExceptionType, @ExceptionValue_A, @ExceptionValue_B, @ExceptionValue,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',@SRC_STATEMENTCODE, @Comments, @MarketLetterCode, GETDATE(),GETDATE(),@USERNAME);
		
			IF @EXCEPTIONLEVEL='LEVEL 1' OR @REGION='WEST'
				GOTO COMMIT_AND_EXIT		
			
			-- CODE BELOW IS FOR LEVEL 2 / OU
			--ID of OU code we just inserted will only be one row
			SELECT @SRC_ExceptionID = [EHLinkCode] FROM @EDIT_HISTORY
			
			DROP TABLE IF EXISTS #TEMP
			DELETE @EDIT_HISTORY

			SELECT OG.StatementCode AS StatementCode_L, OG.RetailerCode,Src_StatementCode AS StatementCode_U, COUNT(OG.RetailerCode) OVER() AS Retailer_Count
			INTO #TEMP
			from RPWeb_Statements OG
			WHERE OG.datasetcode=@STATEMENTCODE


			IF @EXCEPTIONTYPE = 'OTHER'						
				--SIMPLY SPLIT JUST DIVIDE BY AMOUNT OF RETAILERS
				INSERT INTO [dbo].[RPWeb_Exceptions]([RetailerCode], [StatementCode], [Season], [RewardCode],[Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value_A], [Exception_Value_B],[Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode],[Src_ID], [Comments], [MarketLetterCode], [DateCreated], [DateModified], [PostedBy])
				OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
				SELECT RetailerCode, StatementCode_L, @SEASON,@REWARDCODE,'LEVEL 1','OU '+@ExceptionDesc, @ExceptionType, @ExceptionValue_A/Retailer_Count,@ExceptionValue_B/Retailer_Count,@ExceptionValue/Retailer_Count,@EXCEPTIONMARGIN_A,@EXCEPTIONMARGIN_B,@EXCEPTIONMARGIN,'Draft',StatementCode_U, @SRC_ExceptionID ,@Comments, @MARKETLETTERCODE, GETDATE(),GETDATE(),NULL
				FROM #TEMP
						
			ELSE 
				BEGIN
					--LEVEL ONES GET REWARD BASED ON THEIR POG AMOUNT OF THE PERCENTAGE
					DECLARE @STATEMENTS AS UDT_RP_STATEMENT
					INSERT INTO @STATEMENTS
					SELECT StatementCode_L FROM #TEMP

					INSERT INTO [dbo].[RPWeb_Exceptions]([RetailerCode], [StatementCode], [Season], [RewardCode],[Exception_Level], [Exception_Desc], [Exception_Type], [Exception_Value_A], [Exception_Value_B],[Exception_Value],[Exception_Reward_Margin_A],[Exception_Reward_Margin_B],[Exception_Reward_Margin], [Status], [Src_StatementCode],[Src_ID], [Comments], [MarketLetterCode], [DateCreated], [DateModified], [PostedBy])
					OUTPUT inserted.[StatementCode],inserted.ID,'New exception request created'
					INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
					SELECT RetailerCode, StatementCode_L, @SEASON,@REWARDCODE,'LEVEL 1','OU '+ @ExceptionDesc, @ExceptionType
						,0 AS Exception_Value_A
						,0 AS Exception_Value_B
						,ISNULL((@EXCEPTIONMARGIN * EX.CYSales) - EX.CurrentReward,0) AS Exception_Value
						,@EXCEPTIONMARGIN_A AS Exception_Reward_Margin_A
						,@EXCEPTIONMARGIN_B AS Exception_Reward_Margin_B
						,@EXCEPTIONMARGIN AS Exception_Reward_Margin
						,'Draft' AS Status
						,StatementCode_U AS SRC_StatementCode
						,@SRC_ExceptionID  AS SRC_ID
						,@Comments AS Comments
						,@MarketLetterCode AS MarketLetterCode
						,GETDATE() AS DateCreated
						,GETDATE() AS DateModified
						,NULL AS PostedBy
					FROM #TEMP T1
						LEFT JOIN [TVF_Get_Exception_ProgramData](@SEASON,@STATEMENTS,@REWARDCODE) EX 
						ON T1.StatementCode_L = EX.StatementCode
					--WHERE CYSales > 0 to know when OU statements are matching 
				END		
				
			GOTO COMMIT_AND_EXIT		
		END --END SAVE
			

		IF @ACTION = 'UPDATE' 
		BEGIN							
			IF @EXCEPTIONTYPE = 'OTHER'
				UPDATE [RPWeb_Exceptions]
				SET Exception_Desc = @EXCEPTIONDESC
					,Exception_Value_A = @EXCEPTIONVALUE_A
					,Exception_Value_B = @EXCEPTIONVALUE_B
					,Exception_Value = @EXCEPTIONVALUE
					,Comments = @COMMENTS
					,DateModified = GETDATE()
				OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
				WHERE ID = @ExceptionID
			ELSE
				UPDATE [RPWeb_Exceptions]
				SET  Comments = @COMMENTS
					,DateModified = GETDATE()
				OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
				WHERE (ID=@ExceptionID OR SRC_ID=@ExceptionID)

			IF @EXCEPTIONLEVEL='LEVEL 2'
				UPDATE EX
				SET Exception_Desc = 'OU '+@EXCEPTIONDESC
					,Exception_Value_A = @EXCEPTIONVALUE_A/T2.Retailers_Count
					,Exception_Value_B = @EXCEPTIONVALUE_B/T2.Retailers_Count
					,Exception_Value = @EXCEPTIONVALUE/T2.Retailers_Count
					,Comments = @COMMENTS
					,DateModified = GETDATE()
				OUTPUT inserted.[StatementCode],inserted.ID,'Exception details updated'
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData])
				FROM [RPWeb_Exceptions] EX
					INNER JOIN (
						SELECT COUNT(RetailerCode) AS Retailers_Count
						FROM RPWeb_Exceptions 
						WHERE SRC_ID=@ExceptionID					
					) T2
					ON 1=1				
				WHERE EX.SRC_ID=@ExceptionID
					   			 		  
			GOTO COMMIT_AND_EXIT
		END --END UPDATE 

	   


COMMIT_AND_EXIT:						
		EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY
		COMMIT TRANSACTION	
		SELECT 'SUCCESS' AS Status, '' AS ErrorMessage
	
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
		ROLLBACK TRANSACTION
		RETURN;	
	END CATCH		


		
END

