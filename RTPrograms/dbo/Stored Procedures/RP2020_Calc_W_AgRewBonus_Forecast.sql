﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_W_AgRewBonus_Forecast]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	IF @STATEMENT_TYPE='ACTUAL'
	BEGIN
		EXEC RP2020_Calc_W_AgRewBonus_Actual @SEASON, @STATEMENT_TYPE, @PROGRAM_CODE, @STATEMENTS
		RETURN
	END 


	/*
	AgRewards Bonus

	Example: Total Sales = 100K

	- 80% = Leads_Per_Closed - % of Total growers where a lead will be closed (Yes/No from chart)

	These must add up to 100%
	- 10% - Earn_No_Reward - % of Growers who earn No Reward: default to 0%
	- 20% - Inv_Bonus - % of Growers who earn InVigor Bonus only: default to 0%
	- 30% - Two_Seg_Rebate - % of Growers who earn 2-Segment rebate: default to 0%
	- 40% - Three_Seg_Rebate - % of growers who earn 3- Segment rebate	The total of these 4 lines must = 100%, Default to Max Potential: default to 100%

	Reward as follows:
	- 80K of sales eligible for 4% (Inv_Bonus), 4.5% (Two_Seg_Rebate), 6% (Three_Seg_Rebate)
	      - 80K x 20% x 4% = $640
		  - 80K x 30% x 4.5% = $1080
		  - 80K x 40% x 6% = $1920

	- 20k of sales eligible for 2% (Inv_Bonus), 2.5% (Two_Seg_Rebate), 4% (Three_Seg_Rebate)
	      - 20K x 20% x 2% = $80
		  - 20K x 30% x 2.5% = $150
		  - 20K x 40% x 4% = $320

	- $4190 = Total Reward
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_AGREWARDS_BONUS'

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RetailerCode varchar(20) NOT NULL,
		FarmCode varchar(20) NOT NULL,
		RewardBrand_Sales MONEY NOT NULL,
		Leads_Closed_Sales MONEY NOT NULL DEFAULT 0,
		Leads_Closed_Inv_Bonus_Reward MONEY NOT NULL DEFAULT 0,
		Leads_Closed_Two_Seg_Reward MONEY NOT NULL DEFAULT 0,
		Leads_Closed_Three_Seg_Reward MONEY NOT NULL DEFAULT 0,
		Leads_Closed_Reward MONEY NOT NULL DEFAULT 0,
		Leads_NotClosed_Sales MONEY NOT NULL DEFAULT 0,
		Leads_NotClosed_Inv_Bonus_Reward MONEY NOT NULL DEFAULT 0,
		Leads_NotClosed_Two_Seg_Reward MONEY NOT NULL DEFAULT 0,
		Leads_NotClosed_Three_Seg_Reward MONEY NOT NULL DEFAULT 0,
		Leads_NotClosed_Reward MONEY NOT NULL DEFAULT 0,
		Reward NUMERIC(18,2) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RetailerCode] ASC,
			[FarmCode] ASC
		)
	)

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN

		-- Determine total sales of CCP which is what qualifies for a reward
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,RewardBrand_Sales)
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode, 'FXXXXX' AS FarmCode, SUM(tx.Price_CY) AS RewardBrand_Sales
		FROM @STATEMENTS ST
			INNER JOIN RP2020_DT_Sales_Consolidated TX	ON TX.StatementCode=ST.StatementCode
			INNER JOIN RP_Statements S ON S.StatementCode = ST.StatementCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS'
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, S.RetailerCode

		-- Determine % of Total growers where a lead will be closed (Yes/No from chart) and not closed
		UPDATE T1
		SET
			[Leads_Closed_Sales] = [RewardBrand_Sales] * ISNULL(EI.FieldValue/100,1),
			[Leads_NotClosed_Sales] = [RewardBrand_Sales] * ISNULL((100-EI.FieldValue)/100,1)
		FROM @TEMP T1
		LEFT JOIN RPWeb_User_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND T1.ProgramCode = EI.ProgramCode AND EI.[FieldCode] = 'Leads_Per_Closed'
		WHERE T1.[RewardBrand_Sales] > 0

		-- Determine Inv_Bonus - % of Growers who earn InVigor Bonus only (4% or 2%)
		UPDATE T1
		SET
			[Leads_Closed_Inv_Bonus_Reward] = [Leads_Closed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.04,
			[Leads_NotClosed_Inv_Bonus_Reward] = [Leads_NotClosed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.02
		FROM @TEMP T1
		LEFT JOIN RPWeb_User_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND T1.ProgramCode = EI.ProgramCode AND EI.[FieldCode] = 'Inv_Bonus'
		WHERE T1.[RewardBrand_Sales] > 0 

		-- Determine Two_Seg_Rebate - % of Growers who earn 2-Segment rebate (4.5% or 2.5%)
		UPDATE T1
		SET
			[Leads_Closed_Two_Seg_Reward] = [Leads_Closed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.045,
			[Leads_NotClosed_Two_Seg_Reward] = [Leads_NotClosed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.025
		FROM @TEMP T1
		LEFT JOIN RPWeb_User_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND T1.ProgramCode = EI.ProgramCode AND EI.[FieldCode] = 'Two_Seg_Rebate'
		WHERE T1.[RewardBrand_Sales] > 0 

		-- Determine Three_Seg_Rebate - % of Growers who earn 3-Segment rebate (6% or 4%)
		UPDATE T1
		SET
			[Leads_Closed_Three_Seg_Reward] = [Leads_Closed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.06,
			[Leads_NotClosed_Three_Seg_Reward] = [Leads_NotClosed_Sales] * ISNULL(EI.FieldValue/100,1) * 0.04
		FROM @TEMP T1
		LEFT JOIN RPWeb_User_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND T1.MarketLetterCode = EI.MarketLetterCode AND T1.ProgramCode = EI.ProgramCode AND EI.[FieldCode] = 'Three_Seg_Rebate'
		WHERE T1.[RewardBrand_Sales] > 0 

		-- Determine Reward
		UPDATE @TEMP SET [Leads_Closed_Reward] = [Leads_Closed_Inv_Bonus_Reward] + [Leads_Closed_Two_Seg_Reward] + [Leads_Closed_Three_Seg_Reward]
		UPDATE @TEMP SET [Leads_NotClosed_Reward] = [Leads_NotClosed_Inv_Bonus_Reward] + [Leads_NotClosed_Two_Seg_Reward] + [Leads_NotClosed_Three_Seg_Reward]
		UPDATE @TEMP SET [Reward] = [Leads_Closed_Reward] + [Leads_NotClosed_Reward]

		-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0

	END

	DELETE FROM RP2020_DT_Rewards_W_AgRrewardsBonus WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_W_AgRrewardsBonus(StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, RetailerCode, FarmCode, Leads_Closed_Sales, Leads_Closed_Inv_Bonus_Reward, Leads_Closed_Two_Seg_Reward, Leads_Closed_Three_Seg_Reward, Leads_Closed_Reward, Leads_NotClosed_Sales, Leads_NotClosed_Inv_Bonus_Reward, Leads_NotClosed_Two_Seg_Reward, Leads_NotClosed_Three_Seg_Reward, Leads_NotClosed_Reward, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand_Sales, RetailerCode, FarmCode, Leads_Closed_Sales, Leads_Closed_Inv_Bonus_Reward, Leads_Closed_Two_Seg_Reward, Leads_Closed_Three_Seg_Reward, Leads_Closed_Reward, Leads_NotClosed_Sales, Leads_NotClosed_Inv_Bonus_Reward, Leads_NotClosed_Two_Seg_Reward, Leads_NotClosed_Three_Seg_Reward, Leads_NotClosed_Reward, Reward
	FROM @TEMP

END

