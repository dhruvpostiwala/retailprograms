﻿
CREATE PROCEDURE [dbo].[RP2020_Calc_W_PlanSuppBusiness_Actual]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Roundup_Value DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);
	
	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 2
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_W_PLANNING_SUPP_BUSINESS'
	
	DECLARE @TEMP TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		GroupType VARCHAR(50) NOT NULL DEFAULT 'All',
		ProgramCode VARCHAR(50) NOT NULL,
		GroupLabel VARCHAR(100) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		Growth_QualSales_CY Money NOT NULL,
		Growth_QualSales_LY Money NOT NULL,
		Growth_Reward_Sales Money NOT NULL,
		Growth_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Early_Order_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Take_Delivery_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Growth_Reward Money NOT NULL DEFAULT 0,
		Lib_Reward_Sales Money NOT NULL,
		Lib_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Lib_Reward Money NOT NULL DEFAULT 0,
		Cent_Reward_Sales Money NOT NULL,
		Cent_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Cent_Reward Money NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		Growth Decimal(7,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,						
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC,
			[Level5Code] ASC
		)
	)

	DECLARE @GROWTH_TOTALS TABLE (
		StatementCode INT,		
		MarketLetterCode VARCHAR(50) NOT NULL,		
		ProgramCode VARCHAR(50) NOT NULL,
		Level5Code VARCHAR(20) NOT NULL,
		Growth_Reward_Sales Money NOT NULL,
		Growth_QualSales_CY Money NOT NULL,
		Growth_QualSales_LY Money NOT NULL,
		Growth_Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,				
		Growth Decimal(7,4) NOT NULL DEFAULT 0,
		POG_CY Money NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[Level5Code] ASC
		)
	)

	DECLARE @PLAN_CUT_OFF_DATE AS DATE

	DECLARE @SCENARIOS TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL, 
		[ScenarioCode] [varchar](20) NOT NULL,
		[GroupType] VARCHAR(50) NOT NULL DEFAULT 'All',		
		[ScenarioStatus] [varchar](20) NOT NULL,
		[Cut_Off_Date] [Date] NOT NULL
		PRIMARY KEY CLUSTERED (
			[RetailerCode] ASC,
			[MarketLetterCode] ASC,
			[ScenarioCode] ASC,
			[GroupType] ASC
		)
	)

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,		
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL,
		SWP MONEY NOT NULL,
		Price MONEY NOT NULL	
	)

	DECLARE @ADDITIONAL_REWARD_PERCENTAGES TABLE(
		Statementcode INT NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		GroupType	VARCHAR(50) NOT NULL,		
		COP_Amount Money NOT NULL DEFAULT 0,
		EO_Amount Money NOT NULL DEFAULT 0,		
		Early_Order_Percentage Decimal(6,4) NOT NULL DEFAULT 0,	
		Take25_Amount Money  NOT NULL DEFAULT 0,
		Take75_Amount Money  NOT NULL DEFAULT 0,
		Take95_Amount Money  NOT NULL DEFAULT 0,

		PRIMARY KEY CLUSTERED (
			Statementcode ASC,
			MarketLetterCode ASC,
			GroupType ASC
		)
	)
	
	/*
	DECLARE @CUSTOMER_PURCHASE_PLAN TABLE( 
		Statementcode VARCHAR(20) NOT NULL,
		MarketLetterCode VARCHAR(50) NOT NULL,
		GroupType VARCHAR(50) NOT NULL,
		Amount	MONEY NOT NULL
	)
	*/

	DECLARE @HAS_IND_STATEMENTS BIT=0
	DECLARE @HAS_NON_IND_STATEMENTS BIT=0
	DECLARE @HAS_USECASE_STATEMENTS BIT=0

	DECLARE @UNLOCKED_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @USE_CASE_STATEMENTS TABLE (
		Statementcode INT NOT NULL,		
		Level5Code VARCHAR(20) NOT NULL,
		StatementLevel VARCHAR(20) NOT NULL
	)

	DECLARE @FCL_STATEMENTS AS UDT_RP_STATEMENT
		
	INSERT INTO @UNLOCKED_STATEMENTS(Statementcode, Level5Code, StatementLevel)
	SELECT RS.Statementcode, RS.Level5Code, RS.StatementLevel
	FROM  @STATEMENTS ST
		INNER JOIN  RP_Statements RS			ON RS.Statementcode=ST.Statementcode
	WHERE RS.Region='WEST' AND RS.VersionType='UNLOCKED' AND RS.StatementType='Actual'
		
	INSERT INTO @USE_CASE_STATEMENTS(Statementcode, Level5Code, StatementLevel)
	SELECT RS.Statementcode, RS.Level5Code, RS.StatementLevel
	FROM  @STATEMENTS ST
		INNER JOIN RP_Statements RS			ON RS.Statementcode=ST.Statementcode
	WHERE RS.Region='WEST' AND RS.VersionType='USE CASE' AND RS.StatementType='Actual'
		
	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Level5Code,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales,Lib_Reward_Sales,Cent_Reward_Sales)
	SELECT ST.StatementCode,tx.MarketLetterCode, tx.ProgramCode,IIF(tx.GroupLabel IN ('HEAT','HEAT LQ'),PR.Product,tx.GroupLabel) AS GroupLabel, tx.Level5Code
		,0 AS  Growth_QualSales_CY
		,SUM(tx.Price_LY1) AS Growth_QualSales_LY		
		,SUM(CASE WHEN tx.GroupLabel IN ('CENTURION','LIBERTY') THEN 0 ELSE tx.Price_CY END) AS Growth_Reward_Sales				
		,SUM(CASE WHEN tx.GroupLabel = 'LIBERTY' THEN tx.Price_CY ELSE 0 END) AS Lib_Reward_Sales
		,SUM(CASE WHEN tx.GroupLabel = 'CENTURION' THEN tx.Price_CY ELSE 0 END) AS Cent_Reward_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP2020_DT_Sales_Consolidated TX 		ON TX.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR	ON PR.ProductCode=Tx.ProductCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, IIF(tx.GroupLabel IN ('HEAT','HEAT LQ'),PR.Product,tx.GroupLabel), tx.Level5Code
				
	DELETE FROM @TEMP WHERE Level5Code='D0000117' AND  GroupLabel='CENTURION'

	/*
		HEAT (PRE-HARVEST)	HEAT LQ (PRE-HARVEST)	HEAT LQ BULK (PRE-HARVEST)
		HEAT (PRE-SEED)		HEAT LQ (PRE-SEED)		HEAT LQ BULK (PRE-SEED)
	
	*/

	UPDATE @TEMP
	SET GroupType='Fall Herbicides'
	WHERE MarketLetterCode = 'ML2020_W_CER_PUL_SB' AND GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)')

	UPDATE @TEMP
	SET GroupType='Spring Herbicides'
	WHERE MarketLetterCode = 'ML2020_W_CER_PUL_SB' AND GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)')
	   
	/*********************** INDEPENDENTS GROWTH *********************************/
	IF EXISTS(SELECT * FROM @TEMP WHERE Level5Code='D000001') 	
	BEGIN
		SET @HAS_IND_STATEMENTS=1
		/*
		INSERT INTO @CUSTOMER_PURCHASE_PLAN(Statementcode, MarketLetterCode, GroupType, Amount)
		SELECT Statementcode, MarketLetterCode,GroupType,SUM(SDP * Amount) AS Amount
		FROM (
			SELECT ST.Statementcode ,GR.MarketLetterCode
				,CASE 
					WHEN GR.MarketLetterCode <> 'ML2020_W_CER_PUL_SB' THEN 'All'
					WHEN GR.GroupLabel IN ('DISTINCT','IGNITE') OR PR.Product IN ('HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') THEN 'Fall Herbicides'
					WHEN GR.MarketLetterCode = 'ML2020_W_CER_PUL_SB'  THEN 'Spring Herbicides'
					ELSE 'All'
				END AS GroupType
				,Amount
				,PRP.SDP
			FROM @UNLOCKED_STATEMENTS ST
				INNER JOIN RP_StatementsMappings MAP			ON MAP.Statementcode=ST.Statementcode
				INNER JOIN Get_RC_Purchase_Plan_ByDate(@SEASON,'JANUARY 31, 2020') PP			ON PP.Retailercode=MAP.Retailercode
				INNER JOIN ProductReference PR				ON PR.Productcode=PP.Productcode					
				INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=PR.ChemicalGroup
				INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID	
				INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=PR.ProductCode AND PRP.Region='West' AND PRP.Season=@Season
			WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	 AND PR.ChemicalGroup <> 'LIBERTY 200'		
				AND Amount IS NOT NULL
		) D
		GROUP BY Statementcode, MarketLetterCode,GroupType
		*/

		-- MARKET LETTER SEED TREATMENT INOCULANTS
		SET @PLAN_CUT_OFF_DATE=CAST('2019-12-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED 
		
		INSERT @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT tx.Statementcode, GR.MarketLetterCode, GR.GroupLabel, SUM(tx.Quantity * SDP) AS Plan_Amount, 0 AS SRP, 0 AS SWP,  SUM(tx.Quantity * SDP) AS Price
		FROM TVF_Get_IndependentPlanTransactions(@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS) tx
			INNER JOIN @UNLOCKED_STATEMENTS UL			ON UL.StatementCode=tx.StatementCode
			INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
			INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
		WHERE UL.Level5Code='D000001' AND GR.MarketLetterCode='ML2020_W_ST_INC' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	
		GROUP BY tx.Statementcode, GR.MarketLetterCode, GR.GroupLabel				

		--CCP AND PULSE (SPRING HERBICIDES)
		SET @PLAN_CUT_OFF_DATE=CAST('2020-02-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED	
		
		INSERT @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT Statementcode, MarketLetterCode, GroupLabel, Plan_Amount AS SDP, 0 AS SRP, 0 AS SWP, Plan_Amount AS Price  
		FROM (
			SELECT tx.Statementcode, GR.MarketLetterCode
				,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),tx.Product,GR.GroupLabel) AS GroupLabel
				,SUM(tx.Quantity * SDP) AS Plan_Amount
			FROM TVF_Get_IndependentPlanTransactions(@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS) tx
				INNER JOIN @UNLOCKED_STATEMENTS UL			ON UL.StatementCode=tx.StatementCode
				INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
				INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
			WHERE UL.Level5Code='D000001' AND GR.MarketLetterCode IN ('ML2020_W_CCP','ML2020_W_CER_PUL_SB') AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS' AND BR.ChemicalGroup <> 'LIBERTY 200'							
			GROUP BY tx.Statementcode, GR.MarketLetterCode, IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),tx.Product,GR.GroupLabel)
		) D
		WHERE GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)')

		-- CCP AND PULSE (FALL HERBICIDES)
		SET @PLAN_CUT_OFF_DATE=CAST('2020-06-12' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED		

		INSERT @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT Statementcode, MarketLetterCode, GroupLabel, Plan_Amount AS SDP, 0 AS SRP, 0 AS SWP, Plan_Amount AS Price  		
		FROM (
			SELECT tx.Statementcode, GR.MarketLetterCode
				,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),tx.Product,GR.GroupLabel) AS GroupLabel
				,SUM(tx.Quantity * SDP) AS Plan_Amount
			FROM TVF_Get_IndependentPlanTransactions(@SEASON, @PLAN_CUT_OFF_DATE, @STATEMENTS) tx
				INNER JOIN @UNLOCKED_STATEMENTS UL			ON UL.StatementCode=tx.StatementCode	
				INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
				INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
			WHERE UL.Level5Code='D000001' AND GR.MarketLetterCode='ML2020_W_CER_PUL_SB' AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'								
			GROUP BY tx.Statementcode, GR.MarketLetterCode, IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),tx.Product,GR.GroupLabel)
		) D
		WHERE GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)')

	
		-- USE CASE STATEMENTS FOR INDEPENDENT
		UPDATE T1
		SET Growth_QualSales_CY = T2.Plan_Amount_SDP
		FROM @TEMP T1
			INNER JOIN @USE_CASE_STATEMENTS ST 
			ON ST.Statementcode=t1.Statementcode
			INNER JOIN (

					SELECT Statementcode, GroupLabel
							,SUM(PRICE_SDP) AS Plan_Amount_SDP
							,SUM(PRICE_SWP) AS Plan_Amount_SWP
					FROM (
						SELECT tx.Statementcode
							,gr.MarketLetterCode
							,tx.UC_DateModified
							,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),PR.Product,GR.GroupLabel) AS GroupLabel
							,PRICE_SDP
							,PRICE_SWP
						FROM RP_DT_SCenarioTransactions tx
								INNER JOIN @USE_CASE_STATEMENTS ST			ON ST.StatementCode=Tx.Statementcode
								INNER JOIN ProductReference	PR				ON PR.Productcode=Tx.PRoductcode
								INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=TX.ChemicalGroup
								INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID								
						WHERE GR.MarketLetterCode IN ('ML2020_W_ST_INC','ML2020_W_CCP','ML2020_W_CER_PUL_SB') AND GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	AND BR.ChemicalGroup <> 'LIBERTY 200'												
					) D
					WHERE UC_DateModified <= (		
							CASE WHEN GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') THEN CAST('2020-06-12' AS DATE)
							WHEN MarketLetterCode IN ('ML2020_W_CCP','ML2020_W_CER_PUL_SB') THEN CAST('2020-02-11' AS DATE)
							ELSE CAST('2019-12-11' AS DATE) END
						)
					GROUP BY D.Statementcode, D.GroupLabel
			) T2
			ON T2.StatementCode=T1.StatementCode AND T2.GroupLabel=T1.GroupLabel
		WHERE ST.Level5Code='D000001'

	END
	-- END OF INDEPENDENTS BLOCK

	
	/*********************** FCL/COOP  *********************************/
	-- FCL/COOP
	IF EXISTS(SELECT * FROM @TEMP WHERE Level5Code = 'D0000117') 		
	BEGIN
		SET @HAS_NON_IND_STATEMENTS=1
		SET @PLAN_CUT_OFF_DATE=CAST('2019-12-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED

		INSERT INTO @FCL_STATEMENTS(Statementcode)
		SELECT RS.StatementCode	
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS
			ON RS.StatementCode=ST.Statementcode
		WHERE RS.StatementLevel IN ('LEVEL 1','LEVEL 2') AND RS.Level5Code='D0000117'

		-- SCENARIO ID OF  FINAL OR PREFERRED OR DRAFT (LATEST EDIT) POG PLAN AS OF CUT OFF DATE
		INSERT INTO @SCENARIOS(Statementcode, Retailercode, MarketLetterCode, Scenariocode, GroupType, ScenarioStatus, Cut_Off_Date)
		SELECT Statementcode, RetailerCode, 'ML2020_W_ST_INC' as MarketLetterCode, ScenarioCode, 'All', ScenarioStatus, @PLAN_CUT_OFF_DATE
		FROM  [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @FCL_STATEMENTS)
		   	
		SET @PLAN_CUT_OFF_DATE=CAST('2020-02-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED

		INSERT INTO @SCENARIOS(Statementcode, Retailercode, MarketLetterCode, Scenariocode, GroupType, ScenarioStatus, Cut_Off_Date)
		SELECT Statementcode, RetailerCode, 'ML2020_W_CCP' as MarketLetterCode, ScenarioCode, 'All', ScenarioStatus, @PLAN_CUT_OFF_DATE		
		FROM [dbo].[TVF_Get_NonIndependent_Scenarios](@Season, @PLAN_CUT_OFF_DATE, @FCL_STATEMENTS)

			UNION ALL

		SELECT Statementcode, RetailerCode, 'ML2020_W_CER_PUL_SB' as MarketLetterCode, ScenarioCode, 'Spring Herbicides',  ScenarioStatus, @PLAN_CUT_OFF_DATE		
		FROM [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @FCL_STATEMENTS)
	
		SET @PLAN_CUT_OFF_DATE=CAST('2020-06-12' AS DATE) -- FALL HERBICIDES CUT OFF DATE
		
		INSERT INTO @SCENARIOS(Statementcode, Retailercode, MarketLetterCode, Scenariocode, GroupType, ScenarioStatus, Cut_Off_Date)
		SELECT Statementcode, RetailerCode, 'ML2020_W_CER_PUL_SB' as MarketLetterCode, ScenarioCode, 'Fall Herbicides',  ScenarioStatus, @PLAN_CUT_OFF_DATE		
		FROM [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @FCL_STATEMENTS)

		INSERT INTO @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT D.Statementcode, D.MarketLetterCode, D.GroupLabel
			,SUM(SDP) AS SDP
			,SUM(SRP) AS SRP
			,SUM(SWP) AS SWP
			,SUM(Price) AS Price
		FROM (
			SELECT Statementcode, MarketLetterCode, GroupType, Cut_Off_Date, POG_Entry_Date, GroupLabel, SDP, SRP, SWP, Price
			FROM (
				SELECT SCN.Statementcode, SCN.MarketLetterCode
					,SCN.GroupType
					,SCN.Cut_Off_Date
					,CAST(tx.UpdateDate AS DATE) AS POG_Entry_Date
					,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),pr.Product,GR.GroupLabel) AS GroupLabel				
					,TX.QTY * PRP.SDP AS SDP
					,TX.QTY * PRP.SRP AS SRP
					,TX.QTY * PRP.SWP AS SWP
					,TX.QTY * PRP.SDP AS Price
				FROM @SCENARIOS SCN
					INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
					INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
					INNER JOIN RP_Config_Groups_Brands BR					ON BR.ChemicalGroup=PR.ChemicalGroup
					INNER JOIN RP_Config_Groups  GR							ON GR.GroupID=BR.GroupID AND GR.MarketLEtterCode=SCN.MarketLetterCode								
					INNER JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'		
				WHERE  GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS' 
					 AND GR.MarketLetterCode IN ('ML2020_W_ST_INC','ML2020_W_CCP','ML2020_W_CER_PUL_SB') 				 
			) D0
			WHERE POG_Entry_Date <= Cut_Off_Date AND  (
					MarketLetterCode IN ('ML2020_W_ST_INC','ML2020_W_CCP') 				 						
						OR
					( GroupType = 'FALL HERBICIDES' AND GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)'))						
						OR 
					( GroupType = 'SPRING HERBICIDES' AND GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)'))
				)
		) D
		GROUP BY D.Statementcode, D.MarketLettercode, D.GroupLabel, D.GroupType

		-- WEST FCL / COOP
		UPDATE T1
		SET Price=SRP  		
		FROM  @SCENARIO_TRANSACTIONS  T1
			INNER JOIN @UNLOCKED_STATEMENTS RS	ON RS.Statementcode=T1.Statementcode
		WHERE RS.Level5Code='D0000117' AND GroupLabel IN ('CENTURION','LIBERTY','TITAN','INVIGOR') 			   

	END -- FCL/COOP
	
	-- LINE COMPANIES POG TRANSACTIONS
	IF EXISTS(SELECT * FROM @TEMP WHERE Level5Code IN ('D0000107','D0000137','D0000244','D520062427'))	
	BEGIN

		DROP TABLE IF EXISTS #LC_SCENARIOS
		
		SELECT RS.StatementCode, RS.RetailerCode
		INTO #LC_SCENARIOS
		FROM @STATEMENTS ST
			INNER JOIN RP_Statements RS
			ON RS.StatementCode=ST.Statementcode
		WHERE RS.StatementLevel='LEVEL 5' AND RS.RetailerCode IN ('D0000107','D0000137','D0000244','D520062427')
		
		DECLARE @ST_INC_CUT_OFF_DATE DATE=CAST('2019-12-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED
		DECLARE @CCP_CUT_OFF_DATE DATE=CAST('2020-02-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED
		DECLARE @CER_PUL_SPRING_CUT_OFF_DATE DATE=CAST('2020-02-11' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED
		DECLARE @CER_PUL_FALL_CUT_OFF_DATE DATE=CAST('2020-06-12' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED			
		
		DELETE FROM @SCENARIOS
		INSERT INTO @SCENARIOS(Statementcode, Retailercode, MarketLetterCode, Scenariocode, GroupType, ScenarioStatus, Cut_Off_Date)
		SELECT Statementcode, RetailerCode, 'ML2020_W_ST_INC' as MarketLetterCode, RetailerCode, 'All', 'Final' AS ScenarioStatus, @ST_INC_CUT_OFF_DATE
		FROM #LC_SCENARIOS 
		
			UNION

		SELECT Statementcode, RetailerCode, 'ML2020_W_CCP' as MarketLetterCode, RetailerCode, 'All', 'Final' AS ScenarioStatus, @CCP_CUT_OFF_DATE
		FROM #LC_SCENARIOS

			UNION

		SELECT Statementcode, RetailerCode, 'ML2020_W_CER_PUL_SB' as MarketLetterCode, RetailerCode, 'Spring Herbicides', 'Final' AS ScenarioStatus, @CER_PUL_SPRING_CUT_OFF_DATE
		FROM #LC_SCENARIOS

			UNION

		SELECT Statementcode, RetailerCode, 'ML2020_W_CER_PUL_SB' as MarketLetterCode, RetailerCode, 'Fall Herbicides', 'Final' AS ScenarioStatus, @CER_PUL_FALL_CUT_OFF_DATE
		FROM #LC_SCENARIOS

		DECLARE @LC_SCENARIO_TRANSACTIONS TABLE (
			RetailerCode VARCHAR(20) NOT NULL,
			MarketLetterCode VARCHAR(50) NOT NULL,
			GroupType VARCHAR(50) NOT NULL,
			ProductCode VARCHAR(65) NOT NULL,
			Quantity FLOAT NOT NULL
		)
		
		INSERT INTO @LC_SCENARIO_TRANSACTIONS(RetailerCode,MarketLetterCode, GroupType, ProductCode, Quantity)
		SELECT RetailerCode, 'ML2020_W_ST_INC' AS MarketLetterCode, 'ALL' AS GroupType, ProductCode, Quantity		
		FROM Get_Distributor_POGPlan_ByDate(@SEASON,@ST_INC_CUT_OFF_DATE)
		WHERE RetailerCode IN ('D0000107','D0000137','D0000244','D520062427')

			UNION

		SELECT RetailerCode, 'ML2020_W_CCP' AS MarketLetterCode, 'ALL' AS GroupType, ProductCode, Quantity
		FROM Get_Distributor_POGPlan_ByDate(@SEASON,@CCP_CUT_OFF_DATE)
		WHERE RetailerCode IN ('D0000107','D0000137','D0000244','D520062427')

			UNION

		SELECT RetailerCode, 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, 'Spring Herbicides' AS GroupType, ProductCode, Quantity		
		FROM Get_Distributor_POGPlan_ByDate(@SEASON,@CER_PUL_SPRING_CUT_OFF_DATE)
		WHERE RetailerCode IN ('D0000107','D0000137','D0000244','D520062427')

			UNION

		SELECT RetailerCode, 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, 'Fall Herbicides' AS GroupType, ProductCode, Quantity		
		FROM Get_Distributor_POGPlan_ByDate(@SEASON,@CER_PUL_FALL_CUT_OFF_DATE)
		WHERE RetailerCode IN ('D0000107','D0000137','D0000244','D520062427')


		INSERT INTO @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP, SWP, Price)
		SELECT D.Statementcode, D.MarketLetterCode, D.GroupLabel			
			,0 AS SDP
			,0 AS SRP
			,SUM(SWP) AS SWP
			,SUM(SWP) AS Price
		FROM (
			SELECT Statementcode, MarketLetterCode, GroupType, GroupLabel, SWP
			FROM (
				SELECT SCN.Statementcode, SCN.MarketLetterCode	,SCN.GroupType										
					,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),pr.Product,GR.GroupLabel) AS GroupLabel				
					,TX.Quantity * PRP.SWP AS SWP
				FROM @SCENARIOS SCN					
					INNER JOIN @LC_SCENARIO_TRANSACTIONS TX			ON TX.RetailerCode=SCN.RetailerCode AND TX.MarketLetterCode=SCN.MarketLetterCode AND TX.GroupType=SCN.GroupType
					INNER JOIN ProductReference PR					ON PR.ProductCode=TX.ProductCode
					INNER JOIN RP_Config_Groups_Brands BR			ON BR.ChemicalGroup=PR.ChemicalGroup
					INNER JOIN RP_Config_Groups  GR					ON GR.GroupID=BR.GroupID AND GR.MarketLetterCode=SCN.MarketLetterCode								
					INNER JOIN ProductReferencePricing PRP			ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'		
				WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS' 					 
			) D0
			WHERE (
					MarketLetterCode IN ('ML2020_W_ST_INC','ML2020_W_CCP') 				 						
						OR
					( GroupType = 'FALL HERBICIDES' AND GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)'))						
						OR 
					( GroupType = 'SPRING HERBICIDES' AND GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)'))
				)
		) D		
		GROUP BY D.Statementcode, D.MarketLettercode, D.GroupLabel
	END -- LINE COMPANIES POG TRANSACTIONS

	-- SET CURRENT YEAR POG PLAN DOLLAR VALUES
	MERGE @TEMP AS TGT
	USING (
		SELECT t1.StatementCode, t1.MarketLetterCode, t1.GroupLabel, T1.Price, t2.Level5Code
		FROM @SCENARIO_TRANSACTIONS t1
			INNER JOIN  @UNLOCKED_STATEMENTS t2
			ON t2.Statementcode=t1.StatementCode
	) SRC
	ON SRC.StatementCode=TGT.StatementCode AND SRC.GroupLabel=TGT.GroupLabel
	WHEN MATCHED THEN
		UPDATE SET Growth_QualSales_CY = SRC.Price
	WHEN NOT MATCHED THEN
		INSERT (StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Level5Code,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales,Lib_Reward_Sales,Cent_Reward_Sales)
		VALUES(SRC.StatementCode, SRC.MarketLetterCode, @PROGRAM_CODE, SRC.GroupLabel, SRC.Level5Code, SRC.Price, 0, 0, 0, 0)	;
		   	
	-- ALL RETAILERS:  Determine growth based on the sum of brand sales
	INSERT INTO @GROWTH_TOTALS(StatementCode,MarketLetterCode,ProgramCode,Level5Code,Growth_QualSales_CY,Growth_QualSales_LY,Growth_Reward_Sales,POG_CY)
	SELECT StatementCode ,MarketLetterCode, ProgramCode, Level5Code
		,SUM(IIF(GroupLabel IN ('CENTURION','LIBERTY'),0,Growth_QualSales_CY)) AS Growth_QualSales_CY
		,SUM(IIF(GroupLabel IN ('CENTURION','LIBERTY'),0,Growth_QualSales_LY)) AS Growth_QualSales_LY
		,SUM(IIF(GroupLabel IN ('CENTURION','LIBERTY'),0,Growth_Reward_Sales)) AS Growth_Reward_Sales		
		,SUM(Growth_QualSales_CY) AS POG_CY
	FROM @TEMP
	GROUP BY StatementCode,MarketLetterCode, ProgramCode, Level5Code
	
	/*
	Growth in 2020 POG Plan $ relative to the 2019 Sales $  
	west independents and coops
		>110%			3%
		100% to 109.9%	2%
		90% to 99.9%	1%

	line companies:
	'ML2020_W_ST_INC','ML2020_W_CCP','ML2020_W_CER_PUL_SB'

	*/

	-- Determine Growth on total sales even though we are storing the data at brand level sales
	UPDATE @GROWTH_TOTALS
	SET [Growth] = [Growth_QualSales_CY] / [Growth_QualSales_LY]
	WHERE [Growth_QualSales_LY] > 0 AND [Growth_QualSales_CY] > 0

	-- Rounding Rule: Apply a rounding rule of 0.5% 
	UPDATE @GROWTH_TOTALS SET [Growth_Reward_Percentage]=0.03 WHERE [Growth_Reward_Percentage]=0 AND ([Growth] = 0 AND [Growth_QualSales_CY] > 0) OR [Growth]+@Roundup_Value >= 1.1  
	UPDATE @GROWTH_TOTALS SET [Growth_Reward_Percentage]=0.02 WHERE [Growth_Reward_Percentage]=0 AND [Growth]+@Roundup_Value >= 1	
	UPDATE @GROWTH_TOTALS SET [Growth_Reward_Percentage]=0.01 WHERE [Growth_Reward_Percentage]=0 AND [Growth]+@Roundup_Value >= 0.9 
	   	
	--**new Demarey added factor to count out Growth showing when POG and CYSales are 0
	-- Update the Planned Growth Reward %	
	-- Automatically grant 4% Max reward to all line companies for the growth component of the Planning reward for the Cereal Market Letter – all other Market Letters will still require growth calculations.
	
	-- Comments Added by Mahesh on Nov/13/2020
	-- JIRA: RP-3154 All Line Companies: Planning and Supporting Gimmies
	-- I have added "ML2020_W_ST_INC" code to the existing code
	UPDATE @GROWTH_TOTALS
	SET [Growth_Reward_Percentage] = 0.04 
	WHERE Level5Code IN ('D0000107','D0000137','D0000244','D520062427') AND MarketLetterCode IN ('ML2020_W_CER_PUL_SB','ML2020_W_ST_INC') AND [Growth_QualSales_CY] > 0 --AND Growth <> 0
	   	 	
	/*
		-- Comments Added by Mahesh on Nov/13/2020
		-- JIRA: RP-3154 All Line Companies: Planning and Supporting Gimmies
		-- Commenting this code as max growth reward % is 4% and is being set above, no need to increment by 1%

		-- LINE COMPANIES  - GROWTH REWARD PERCENTAGE IS DIFFERENT FOR SOME MARKET LETTERS
		UPDATE @GROWTH_TOTALS 
		SET [Growth_Reward_Percentage] += 0.01 
		WHERE Level5Code IN ('D0000107','D0000137','D0000244','D520062427') AND MarketLetterCode='ML2020_W_ST_INC' AND [Growth_Reward_Percentage] > 0 

	*/
	
	-- GROWTH PERCENTAGE
	-- Update the Growth % and  Planned Growth Reward % back on the temp table at brand level sales
	UPDATE T1
	SET T1.[Growth]=T2.[Growth]
		,T1.[Growth_Reward_Percentage]= T2.[Growth_Reward_Percentage]
	FROM @TEMP T1
		INNER JOIN @GROWTH_TOTALS T2 
		ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode]=T2.[MarketLetterCode] AND T1.[ProgramCode]=T2.[ProgramCode] AND T1.[Level5Code]=T2.[Level5Code]
	WHERE T1.[GroupLabel] NOT IN ('LIBERTY','CENTURION')


	   	 
	/*************************** INDEPENDENTS AND LINE COMPANIES ***************************/
	IF EXISTS(SELECT * FROM @TEMP WHERE Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427'))	
	BEGIN
		INSERT INTO @ADDITIONAL_REWARD_PERCENTAGES(StatementCode, MarketLetterCode, GroupType )
		SELECT DISTINCT StatementCode, MarketLetterCode
			,IIF(MarketLetterCode='ML2020_W_CER_PUL_SB', 'Spring Herbicides','All') AS GroupType
		FROM @Temp 
		WHERE Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427')


			UNION 

		SELECT DISTINCT StatementCode, MarketLetterCode, 'Fall Herbicides' AS GroupType
		FROM @Temp 
		WHERE Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427')  AND MarketLetterCode='ML2020_W_CER_PUL_SB'
			
		IF EXISTS(SELECT * FROM @USE_CASE_STATEMENTS)
		BEGIN
			-- USE CASE STATEMENTS - Purchase Plan Amount
			UPDATE T1
			SET COP_Amount = ISNULL(T2.Growth_QualSales_CY,0)
			FROM @ADDITIONAL_REWARD_PERCENTAGES T1
				INNER JOIN @USE_CASE_STATEMENTS ST		
				ON ST.Statementcode=T1.Statementcode
				INNER JOIN  (
					SELECT StatementCode, MarketLetterCode, GroupType, SUM(Growth_QualSales_CY) AS Growth_QualSales_CY				
					FROM @TEMP
					GROUP BY StatementCode,MarketLetterCode,GroupType				
				) T2			
				ON T2.Statementcode=t1.Statementcode 
			WHERE T2.MarketLettercode=T1.MarketLetterCode AND T2.GroupType=T1.GroupType


			-- USE CASE STATEMENTS - EARLY ORDERS
			UPDATE T1
			SET EO_Amount = ISNULL(T2.Orders_Amount,0)
			FROM @ADDITIONAL_REWARD_PERCENTAGES T1
				INNER JOIN @USE_CASE_STATEMENTS ST		
				ON ST.Statementcode=T1.Statementcode
				INNER JOIN (				
					SELECT st.StatementCode, tx.MarketLetterCode, tx.GroupType
						,SUM(TX.Quantity * IIF(ST.StatementLevel='LEVEL 5',PRP.SWP,PRP.SDP))  AS Orders_Amount					
					FROM @USE_CASE_STATEMENTS ST		
						INNER JOIN (
							SELECT Statementcode, MarketLetterCode, GroupType, PRoductCode, Quantity, GroupLabel
							FROM (
								SELECT EO.StatementCode, EO.MarketLetterCode, EO.GroupType, EO.ProductCode, EO.Quantity
									 ,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),PR.Product,GR.GroupLabel) AS GroupLabel
								FROM (
									SELECT Statementcode, 'ML2020_W_ST_INC' AS MarketLetterCode, 'All' AS GroupType, ProductCode, Quantity
									FROM [TVF_Get_OrdersData](@Season, CAST('2019-12-31' AS DATE), @STATEMENTS)					

										UNION ALL

									SELECT Statementcode, 'ML2020_W_CCP' AS MarketLetterCode, 'All' AS GroupType, ProductCode, Quantity
									FROM [TVF_Get_OrdersData](@Season, CAST('2020-02-25' AS DATE), @STATEMENTS)					

										UNION ALL

									SELECT Statementcode, 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, 'Spring Herbicides' AS GroupType, ProductCode, Quantity
									FROM [TVF_Get_OrdersData](@Season, CAST('2020-04-16' AS DATE), @STATEMENTS)					
						
										UNION ALL

									SELECT Statementcode, 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, 'Fall Herbicides' AS GroupType, ProductCode, Quantity
									FROM [TVF_Get_OrdersData](@Season, CAST('2020-06-15' AS DATE), @STATEMENTS)									
								) EO
									INNER JOIN ProductReference PR				ON PR.Productcode=EO.Productcode							
									INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=PR.ChemicalGroup
									INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID AND GR.MarketLetterCode=EO.MarketLetterCode	
								WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	 AND PR.ChemicalGroup <> 'LIBERTY 200'									
							) EO2
							WHERE 
								(
									EO2.MarketLetterCode <> 'ML2020_W_CER_PUL_SB' 
											OR 
									(EO2.GroupType='Spring Herbicides' AND EO2.GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') )
											OR 
									(EO2.GroupType='Fall Herbicides' AND EO2.GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') )
								)
						) tx
						ON TX.StatementCode=ST.StatementCode												
						INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=TX.ProductCode 					
					WHERE PRP.Season=@SEASON AND PRP.Region='WEST' 													
					GROUP BY st.StatementCode, tx.MarketLetterCode, tx.GroupType		
				) T2
				ON T2.StatementCode=T1.StatementCode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.GroupType=T1.GroupType
		END

		/*
		-- RP-3039 - Client has requested that the Early Order % for all West Independent retails is automatically at maximum (1% for all MLs).
		IF EXISTS(SELECT * FROM @UNLOCKED_STATEMENTS WHERE Level5Code='D000001')
		BEGIN
			-- UNLOCKED STATEMENTS - PURCHASE PLAN
			UPDATE T1
			SET COP_Amount = ISNULL(COP.Amount,0)
			FROM @ADDITIONAL_REWARD_PERCENTAGES T1
				INNER JOIN @CUSTOMER_PURCHASE_PLAN COP
				ON COP.Statementcode=T1.Statementcode
			WHERE COP.MarketLetterCode=T1.MarketLetterCode AND COP.GroupType=T1.GroupType

									   					 
			-- UNLOCKED STATEMENTS - EARLY ORDERS
			UPDATE T1
			SET EO_Amount = T2.Orders_Amount 
			FROM @ADDITIONAL_REWARD_PERCENTAGES T1
				INNER JOIN @UNLOCKED_STATEMENTS ST 
				ON ST.Statementcode=T1.Statementcode
				INNER JOIN (				
					SELECT ST.StatementCode, tx.MarketLetterCode, tx.GroupType
						,SUM(TX.Quantity * IIF(ST.StatementLevel='LEVEL 5',PRP.SWP,PRP.SDP))  AS Orders_Amount					
					FROM @UNLOCKED_STATEMENTS ST	
						INNER JOIN RP_StatementsMappings MAP
						ON MAP.Statementcode=ST.Statementcode
						INNER JOIN (						
							-- EARLY ORDERS
							SELECT Retailercode, MarketLetterCode, GroupType, ProductCode, Quantity, GroupLabel
							FROM (
								SELECT EO.Retailercode, EO.MarketLetterCode, EO.GroupType, EO.ProductCode, EO.Quantity
									 ,IIF(GR.GroupLabel IN ('HEAT','HEAT LQ'),PR.Product,GR.GroupLabel) AS GroupLabel
								FROM (
									SELECT 'ML2020_W_ST_INC' AS MarketLetterCode, sold_to_retailercode AS RetailerCode , 'All' AS GroupType, ProductCode, CAST(Order_Quantity AS DECIMAL(18,2)) AS Quantity									
									FROM YSeed140
									WHERE CAST(ORDER_DATE AS DATE) <= CAST('2019-12-31' AS DATE) AND sold_to_retailercode IS NOT NULL
																
										UNION ALL

									SELECT 'ML2020_W_CCP' AS MarketLetterCode, sold_to_retailercode AS RetailerCode, 'All' AS GroupType, ProductCode, CAST(Order_Quantity AS DECIMAL(18,2)) AS Quantity			
									FROM YSeed140
									WHERE CAST(ORDER_DATE AS DATE) <= CAST('2020-02-25' AS DATE) AND sold_to_retailercode IS NOT NULL
								

										UNION ALL

									SELECT 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, sold_to_retailercode AS RetailerCode, 'Spring Herbicides' AS GroupType, ProductCode, CAST(Order_Quantity AS DECIMAL(18,2)) AS Quantity			
									FROM YSeed140
									WHERE CAST(ORDER_DATE AS DATE) <= CAST('2020-04-16' AS DATE) AND sold_to_retailercode IS NOT NULL		
							
										UNION ALL

									SELECT 'ML2020_W_CER_PUL_SB' AS MarketLetterCode, sold_to_retailercode AS RetailerCode, 'Fall Herbicides' AS GroupType, ProductCode, CAST(Order_Quantity AS DECIMAL(18,2)) AS Quantity			
									FROM YSeed140
									WHERE CAST(ORDER_DATE AS DATE) <= CAST('2020-06-15' AS DATE) AND sold_to_retailercode IS NOT NULL		
								) EO
									INNER JOIN ProductReference PR				ON PR.Productcode=EO.Productcode							
									INNER JOIN RP_Config_Groups_Brands BR		ON BR.ChemicalGroup=PR.ChemicalGroup
									INNER JOIN RP_Config_Groups  GR				ON GR.GroupID=BR.GroupID AND GR.MarketLetterCode=EO.MarketLetterCode	
								WHERE GR.ProgramCode=@PROGRAM_CODE AND GR.GroupType='REWARD_BRANDS'	  AND PR.ChemicalGroup <> 'LIBERTY 200'
									AND EO.Quantity IS NOT NULL
							) EO2
							WHERE (
									EO2.MarketLetterCode <> 'ML2020_W_CER_PUL_SB' 
											OR 
									(EO2.GroupType='Spring Herbicides' AND EO2.GroupLabel NOT IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') )
											OR 
									(EO2.GroupType='Fall Herbicides' AND EO2.GroupLabel IN ('DISTINCT','IGNITE','HEAT (PRE-HARVEST)','HEAT LQ (PRE-HARVEST)','HEAT LQ BULK (PRE-HARVEST)') )
								)
						) tx
						ON TX.RetailerCode=MAP.Retailercode						
						INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=TX.ProductCode 
					WHERE PRP.Season=@SEASON AND PRP.Region='WEST' 										
					GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.GroupType			
				) T2
				ON T2.StatementCode=T1.StatementCode AND T2.MarketLetterCode=T1.MarketLetterCode AND T2.GroupType=T1.GroupType

			-- ADDITIONAL EOP REWARD 1%
			UPDATE T1
			SET [Early_Order_Percentage] = 0.01
			FROM @ADDITIONAL_REWARD_PERCENTAGES  T1
				INNER JOIN @GROWTH_TOTALS T2
				ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode
			WHERE T1.MarketLetterCode='ML2020_W_ST_INC' AND T2.[Growth_QualSales_CY] > 0 
				AND T1.COP_Amount > 0 AND (T1.EO_Amount/T1.COP_Amount) + @Roundup_Value  >= 0.75
				AND T2.Level5Code='D000001'

			UPDATE T1
			SET [Early_Order_Percentage] = 0.01
			FROM @ADDITIONAL_REWARD_PERCENTAGES  T1
				INNER JOIN @GROWTH_TOTALS T2
				ON T2.Statementcode=T1.Statementcode AND T2.MarketLetterCode=T1.MarketLetterCode
			WHERE T1.MarketLetterCode IN ('ML2020_W_CCP','ML2020_W_CER_PUL_SB') AND T2.Growth_QualSales_CY > 0
				AND T1.COP_Amount > 0 AND (T1.EO_Amount/T1.COP_Amount) + @Roundup_Value >= 0.60
				AND T2.Level5Code='D000001'

			-- Early Order Percentage
			UPDATE T1
			SET T1.[Early_Order_Percentage] = T2.[Early_Order_Percentage]			
			FROM @TEMP T1
				INNER JOIN @ADDITIONAL_REWARD_PERCENTAGES T2 
				ON T1.[StatementCode] = T2.[StatementCode] 
			WHERE  T1.[MarketLetterCode]=T2.[MarketLetterCode]
				AND T1.GroupType=T2.GroupType
				AND T1.[GroupLabel] NOT IN ('LIBERTY','CENTURION')
				AND T1.Level5Code='D000001'

		END   
		*/
		-- RP-3039
		-- Client has requested that the Early Order % for all West Independent retails is automatically at maximum (1% for all MLs).

		UPDATE T1
		SET T1.[Early_Order_Percentage] = 0.01
		FROM @TEMP T1
		WHERE T1.[GroupLabel] NOT IN ('LIBERTY','CENTURION') AND T1.Level5Code='D000001'

		-- ADDITIONAL EOP REWARD 1%	
		-- LINE COMAPNIES JIRA: RP-3043
		-- NUTRIEN JIRA: RP-3224 D520062427
		UPDATE @TEMP
		SET [Early_Order_Percentage]=IIF([GroupLabel] IN ('LIBERTY','CENTURION'),0.005,0.01)
		WHERE MarketLetterCode='ML2020_W_CCP' AND Level5Code IN ('D0000107','D0000137','D0000244','D520062427') 
			

		UPDATE @TEMP
		SET [Early_Order_Percentage]=IIF([GroupLabel] IN ('LIBERTY','CENTURION'),0.005,0.01)
		WHERE MarketLetterCode='ML2020_W_ST_INC' AND  Level5Code IN ('D0000107','D0000137','D0000244','D520062427') 
		

		UPDATE @TEMP
		SET [Early_Order_Percentage]=IIF([GroupLabel] IN ('LIBERTY','CENTURION'),0.005,0.01)
		WHERE MarketLetterCode='ML2020_W_CER_PUL_SB' AND GroupType='Spring Herbicides' AND  Level5Code IN ('D0000107','D0000137','D0000244','D520062427')			

		UPDATE @TEMP
		SET [Early_Order_Percentage]=IIF([GroupLabel] IN ('LIBERTY','CENTURION'),0.005,0.01)
		WHERE MarketLetterCode='ML2020_W_CER_PUL_SB' AND GroupType='Fall Herbicides' AND  Level5Code IN ('D0000137','D520062427')						
					

		-- Take Delivery Percentages
		-- Liberty 
		UPDATE @TEMP 
		SET [Take_Delivery_Percentage] = IIF([Level5Code] = 'D000001',0.02,0.05)  
		WHERE [MarketLetterCode] = 'ML2020_W_CCP' AND [GroupLabel]='LIBERTY'
			AND Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427') 

		-- CENTURION
		UPDATE @TEMP 
		SET [Take_Delivery_Percentage] = IIF([Level5Code]='D000001', 0.01 , 0.02)
		WHERE   [MarketLetterCode] = 'ML2020_W_CCP' AND [GroupLabel]='CENTURION' 
			AND Level5Code IN ('D000001','D0000107','D0000137','D0000244','D520062427') 

		--  INDEPENDENTS (NON LIBERTY AND CENTURION)			   
		UPDATE @TEMP
		SET [Take_Delivery_Percentage]  = 0.01		
		WHERE Level5Code = 'D000001' AND [GroupLabel] NOT IN ('LIBERTY','CENTURION')

		-- LINE COMPANIES
		UPDATE @TEMP
		SET [Take_Delivery_Percentage]  = 0.02
		WHERE [Level5Code] IN ('D0000107','D0000137','D0000244','D520062427')  AND [GroupLabel] IN ('COTEGRA','LANCE','LANCE AG','FACET L')
		
		UPDATE @TEMP
		SET [Take_Delivery_Percentage]  = 0.01		
		WHERE [Level5Code] IN ('D0000107','D0000137','D0000244','D520062427')  AND [GroupLabel] NOT IN ('LIBERTY','CENTURION','COTEGRA','LANCE','LANCE AG','FACET L')
	END	
	/**************************** END OF INDEPENDENTS AND LINE COMPANIES *****************************************/
	   	 	
	-- BEGIN: COOP / FCL
	IF EXISTS(SELECT * FROM @TEMP WHERE Level5Code='D0000117')
	BEGIN
		-- UPDATE PRELIMINARY AND FINAL PLAN REWARD 
		
		UPDATE T1
		SET T1.[Early_Order_Percentage]=0.0075 -- PREMLIMINARY PLAN REWARD
			,T1.[Take_Delivery_Percentage]=0.01 -- FINAL PLAN REWARD
		FROM @TEMP T1
			INNER JOIN @GROWTH_TOTALS T2 
			ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode]=T2.[MarketLetterCode] AND T1.[ProgramCode]=T2.[ProgramCode] AND T1.[Level5Code]=T2.[Level5Code]
		WHERE T1.Level5Code='D0000117' AND T1.[GroupLabel]='LIBERTY' AND T2.[POG_CY] > 0 

		UPDATE T1
		SET T1.[Early_Order_Percentage]=0.005 -- PREMLIMINARY PLAN REWARD
			,T1.[Take_Delivery_Percentage]=0.005 -- FINAL PLAN REWARD
		FROM @TEMP T1
			INNER JOIN @GROWTH_TOTALS T2 
			ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode]=T2.[MarketLetterCode] AND T1.[ProgramCode]=T2.[ProgramCode] AND T1.[Level5Code]=T2.[Level5Code]
		WHERE T1.Level5Code='D0000117' AND T1.[GroupLabel] NOT IN ('LIBERTY','CENTURION') AND T2.[POG_CY] > 0 		
	END
	-- END: COOP / FCL

	-- Update Growth Reward
	UPDATE @TEMP 
	SET [Growth_Reward] = [Growth_Reward_Sales] * ([Growth_Reward_Percentage]+[Early_Order_Percentage]+[Take_Delivery_Percentage])			
	WHERE GroupLabel NOT IN ('LIBERTY','CENTURION')
	
	-- Update Liberty Reward
	UPDATE @TEMP SET [Lib_Reward] = [Lib_Reward_Sales] * ([Early_Order_Percentage]+[Take_Delivery_Percentage])
	WHERE [MarketLetterCode] = 'ML2020_W_CCP' AND [Lib_Reward_Sales] > 0

	-- Update Centurion Reward
	UPDATE @TEMP SET [Cent_Reward] = [Cent_Reward_Sales] * ([Early_Order_Percentage]+[Take_Delivery_Percentage])
	WHERE [MarketLetterCode] = 'ML2020_W_CCP' AND [Cent_Reward_Sales] > 0

		  		 
	-- Update final reward
	UPDATE @TEMP SET [Reward] = [Growth_Reward]+[Lib_Reward]+[Cent_Reward] 
		
	DELETE FROM RP2020_DT_Rewards_W_PlanningTheBusiness WHERE StatementCode IN (Select StatementCode From @Statements)

	INSERT INTO RP2020_DT_Rewards_W_PlanningTheBusiness(Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Growth_QualSales_CY, Growth_QualSales_LY, Growth_Reward_Sales, Growth_Reward_Percentage, Early_Order_Percentage, Take_Delivery_Percentage, Growth_Reward, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Growth_QualSales_CY, Growth_QualSales_LY, Growth_Reward_Sales, Growth_Reward_Percentage, Early_Order_Percentage, Take_Delivery_Percentage, Growth_Reward, Lib_Reward_Sales, Lib_Reward_Percentage, Lib_Reward, Cent_Reward_Sales, Cent_Reward_Percentage, Cent_Reward, Reward
	FROM @TEMP

	
END


