﻿



CREATE PROCEDURE [dbo].[RP2020_Calc_E_PlanningTheBusiness]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	Default reward to 1% across the board as it is expected that retails will receive this reward in the act of simply developing their POG Plan.
	This does not reward on POG$ of Kumulus &/or Polyram.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE StatementCode = 1
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_E_PLANNING_SUPP_BUSINESS'

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Plan_Passed BIT NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			RewardBrand ASC			
		)
	)

	DECLARE @SCENARIOS TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL, 
		[ScenarioCode] [varchar](20) NOT NULL,
		[ScenarioStatus] [varchar](20) NOT NULL,
		[Cut_Off_Date] [Date] NOT NULL
	)

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL
	)

	DECLARE @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS TABLE (
		ChemicalGroup VARCHAR(200)		
	)

	--Insert Transactions specific for CCP ALL ROW CROP EXCEPT INOCULANTS GROUP NOT Mapped in RP_CONFIG_GROUPS
	INSERT INTO @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS
	SELECT DISTINCT ChemicalGroup 
	FROM ProductReference 
	WHERE DocType in (10,80) AND Sector IN ('ROW CROP','HORTICULTURE') AND ChemicalGroup NOT IN ('KUMULUS','POLYRAM') AND Category NOT IN ('INOCULANT')

	DECLARE @RewardPercentage DECIMAL(6,2) = 0.01;

	-- THIS IS A STRAIGHT FORWARD 1% REWARD.
	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
		,SUM(tx.Price_CY) AS RewardBrand_Sales		
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ELIGIBLE_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup

	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE @TEMP
		SET Reward_Percentage = @RewardPercentage
		WHERE RewardBrand_Sales > 0
	END

	-- Check that the POG Plan was completed by December 24, 2019
	DECLARE @PLAN_CUT_OFF_DATE AS DATE=CAST('2019-12-24' AS DATE)

	-- Scenario Transactions
	INSERT INTO @SCENARIOS(StatementCode,RetailerCode,MarketLetterCode,ScenarioCode,ScenarioStatus,Cut_Off_Date)
	SELECT Statementcode, RetailerCode, 'ML2020_E_CCP' as MarketLetterCode, ScenarioCode, ScenarioStatus, @PLAN_CUT_OFF_DATE AS Cut_Off_Date		
	FROM [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @STATEMENTS)

	-- SCENARIO TRANSACTIONS AS OF CUT OFF DATE --
	INSERT INTO @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP)
	SELECT SCN.Statementcode, SCN.MarketLetterCode, GR.ChemicalGroup
		,SUM(TX.QTY * PRP.SDP) AS SDP
		,SUM(TX.QTY * PRP.SRP) AS SRP
	FROM @SCENARIOS SCN
		INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
		INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
		INNER JOIN @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS	GR		ON GR.ChemicalGroup=PR.ChemicalGroup								
		INNER JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='EAST'
	WHERE SCN.MarketLetterCode = 'ML2020_E_CCP'  AND CAST(tx.UpdateDate AS DATE) <= SCN.Cut_Off_Date
	GROUP BY SCN.Statementcode, SCN.MarketLetterCode, GR.ChemicalGroup

	-- UPDATE REWARD 
	UPDATE T1
	SET T1.Reward = RewardBrand_Sales * @RewardPercentage, Reward_Percentage = @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN @SCENARIO_TRANSACTIONS tx
		ON tx.Statementcode=IIF(T1.DataSetCode > 0, T1.DataSetCode, T1.StatementCode)			
	WHERE ST.VersionType='Unlocked' AND ST.StatementType='Actual' AND T1.RewardBrand_Sales <> 0 AND tx.SDP > 0 
	
	-- UPDATE REWARD
	-- FOR USE CASE STATEMENTS
	UPDATE T1
	SET T1.Reward = RewardBrand_Sales * @RewardPercentage, Reward_Percentage = @RewardPercentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN (
			SELECT tx.Statementcode, SUM(PRICE_SDP) AS Plan_Amount
			FROM RP_DT_ScenarioTransactions tx
					INNER JOIN @STATEMENTS ST					ON ST.StatementCode=Tx.Statementcode
					INNER JOIN @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS	PR	ON PR.ChemicalGroup=Tx.ChemicalGroup								
			WHERE tx.UC_DateModified <= @PLAN_CUT_OFF_DATE
			GROUP BY tx.Statementcode, PR.ChemicalGroup
		) T2
		ON T2.StatementCode=T1.StatementCode
	WHERE ST.VersionType='Use Case' AND ST.StatementType='Actual' AND T1.RewardBrand_Sales > 0 AND  T2.Plan_Amount > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD --
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_PlanningTheBusiness WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_PlanningTheBusiness(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,MAX(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0 --AND Reward > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand

END

