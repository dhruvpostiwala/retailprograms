﻿

CREATE PROCEDURE [dbo].[RP2020_Calc_E_PortfolioSupport]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
	NEW: RC-3756 - Qualification is done at the OU level. We take all the POG$ from each level 1 within the OU combine them in the Level 2 summary and then compare that number with
	the level 2 2year average.

	The Portfolio Support Reward is based on the table below and will be calculated by dividing
	2020 POG sales by the two-year average of 2018 and 2019 POG sales. The 2018 and 2019
	sales quantities will be adjusted to 2020 Suggested Dealer Prices.
		90-94.9%	3%
		95-99.9%	4%
		100%+		6%
	See below for the requirements the retailer must meet to be eligible for the Portfolio Support
	Reward:
		1) Total 2020 POG Sales of eligible BASF Products must be at least $100,000.
		2) Submit a 2020 POG plan no later than December 13, 2019.
	*/

	--DECLARE @STATEMENTS AS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS SELECT StatementCode FROM RP_Statements WHERE DataSetCode = 1189 or StatementCode in (1189, 904)
	--DECLARE @SEASON INT = 2020
	--DECLARE @STATEMENT_TYPE VARCHAR(20) = 'FORECAST'
	--DECLARE @PROGRAM_CODE VARCHAR(50) = 'RP2020_E_PORFOLIO_SUPPORT'

	DECLARE @ROUNDUP_VALUE DECIMAL(3,3)=dbo.SVF_GetPercentageRoundUpValue(@SEASON);


	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	DECLARE @TEMP_TOTALS TABLE (
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,	
		RewardBrand_Sales MONEY NOT NULL,
		QualBrand_Sales_CY MONEY NOT NULL,
		QualBrand_Sales_LY1 MONEY NOT NULL,
		QualBrand_Sales_LY2 MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,2) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Growth DECIMAL(6,2) NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[DataSetCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC			
		)
	)

	DECLARE @SCENARIOS TABLE(
		[StatementCode] INT,
		[RetailerCode] [Varchar](20) NOT NULL,
		[MarketLetterCode] [varchar](50) NOT NULL, 
		[ScenarioCode] [varchar](20) NOT NULL,
		[ScenarioStatus] [varchar](20) NOT NULL,
		[Cut_Off_Date] [Date] NOT NULL
	)

	DECLARE @SCENARIO_TRANSACTIONS TABLE (
		StatementCode INT,		
		MarketLetterCode varchar(50) NOT NULL,
		GroupLabel VARCHAR(50) NOT NULL,		
		SRP MONEY NOT NULL,
		SDP MONEY NOT NULL
	)

	DECLARE @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS TABLE (
		ChemicalGroup VARCHAR(200)		
	)
	--Insert Transactions specific for CCP ALL ROW CROP EXCEPT INOCULANTS GRUOP NOT Mapped in RP_CONFIG_GROUPS
	INSERT INTO @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS
	SELECT DISTINCT ChemicalGroup 
	FROM ProductReference 
	WHERE DocType in (10,80) AND Sector IN ('ROW CROP','HORTICULTURE') AND ChemicalGroup NOT IN ('KUMULUS','POLYRAM') AND Category NOT IN ('INOCULANT')



	DECLARE @PLAN_CUT_OFF_DATE AS DATE

	SET @PLAN_CUT_OFF_DATE=CAST('2019-12-24' AS DATE) -- 7 BUSINESS DAYS BUFFER DATE IS USED

	--NOTE ELIGIBLE_BRANDS IS REWARD BRANDS
	
	-- CALCULATE REWARD AT LEVEL 1 STATEMENTS ONLY
	-- Qualifying Brand sales calculated on Actual POG sale
	-- Reward Brand Sales calculated on Eligible POG Sale
	-- Ticket RP-2924
	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY1,QualBrand_Sales_LY2)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
		--,SUM(tx.Price_CY) AS RewardBrand_Sales
		/*,SUM(Price_Q_SDP_CY) AS QualBrand_Sales_CY	
		,SUM(tx.Price_CY) AS RewardBrand_Sales		
		,SUM(tx.Price_LY1) AS QualBrand_Sales_LY1
		,SUM(tx.Price_LY2) AS QualBrand_Sales_LY2*/
		,SUM(tx.Price_CY) AS RewardBrand_Sales
		,SUM(Price_Q_SDP_CY) AS QualBrand_Sales_CY	
		,SUM(tx.Price_Q_SDP_LY1) AS QualBrand_Sales_LY1
		,SUM(tx.Price_Q_SDP_LY2) AS QualBrand_Sales_LY2
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2020_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN Productreference PR ON PR.ProductCode = tx.ProductCode
	WHERE RS.StatementLevel='LEVEL 1' AND tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='ELIGIBLE_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup

	-- Determine growth based on the sum of brand sales at an OU level
	INSERT INTO @TEMP_TOTALS(DataSetCode,MarketLetterCode,ProgramCode,RewardBrand_Sales,QualBrand_Sales_CY,QualBrand_Sales_LY1,QualBrand_Sales_LY2)
	SELECT IIF(DataSetCode > 0,DataSetCode,StatementCode) AS DataSetCode,MarketLetterCode,ProgramCode,
		SUM(RewardBrand_Sales) AS RewardBrand_Sales,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY,SUM(QualBrand_Sales_LY1) AS QualBrand_Sales_LY1,SUM(QualBrand_Sales_LY2) AS QualBrand_Sales_LY2
	FROM @TEMP
	GROUP BY IIF(DataSetCode > 0,DataSetCode,StatementCode), MarketLetterCode, ProgramCode

	-- Determine growth on total sales
	UPDATE @TEMP_TOTALS
	SET Growth = QualBrand_Sales_CY / ((QualBrand_Sales_LY1 + QualBrand_Sales_LY2)/2)
	WHERE QualBrand_Sales_LY1 + QualBrand_Sales_LY2 > 0

	-- Update the reward %
	DECLARE @MIN_SALES INT = 100000
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.06 WHERE [QualBrand_Sales_CY] >= @MIN_SALES AND ((Growth = 0 AND QualBrand_Sales_CY > 0) OR Growth + @ROUNDUP_VALUE >= 1)
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.04 WHERE [QualBrand_Sales_CY] >= @MIN_SALES AND [Reward_Percentage] = 0 AND Growth + @ROUNDUP_VALUE >= 0.95
	UPDATE @TEMP_TOTALS SET [Reward_Percentage] = 0.03 WHERE [QualBrand_Sales_CY] >= @MIN_SALES AND [Reward_Percentage] = 0 AND Growth + @ROUNDUP_VALUE >= 0.90

	-- Update the growth % and reward % back on the temp table at location level sales for a family
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.DataSetCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode > 0

	-- Update the growth % and reward % back on the temp table at location level sales for a single location
	UPDATE T1
	SET T1.Growth = T2.Growth, T1.Reward_Percentage = T2.Reward_Percentage
	FROM @TEMP T1
	INNER JOIN @TEMP_TOTALS T2 ON T1.StatementCode = T2.DataSetCode AND T1.MarketLetterCode = T2.MarketLetterCode and T1.ProgramCode = T2.ProgramCode
	WHERE T1.DataSetCode = 0


	---Scenario Transactions
	INSERT INTO @SCENARIOS(StatementCode,RetailerCode,MarketLetterCode,ScenarioCode,ScenarioStatus,Cut_Off_Date)
	SELECT Statementcode, RetailerCode, 'ML2020_E_CCP' as MarketLetterCode, ScenarioCode, ScenarioStatus, @PLAN_CUT_OFF_DATE AS Cut_Off_Date		
	FROM [dbo].[TVF_Get_NonIndependent_Scenarios] (@Season, @PLAN_CUT_OFF_DATE, @STATEMENTS)

	-- SCENARIO TRANSACTIONS AS OF CUT OFF DATE --
	INSERT INTO @SCENARIO_TRANSACTIONS(Statementcode, MarketLetterCode, GroupLabel, SDP, SRP)
	SELECT SCN.Statementcode, SCN.MarketLetterCode, GR.ChemicalGroup
		,SUM(TX.QTY * PRP.SDP) AS SDP
		,SUM(TX.QTY * PRP.SRP) AS SRP
	FROM @SCENARIOS SCN
		INNER JOIN Log_Retail_Plan_Scenario_Transactions TX		ON TX.ScenarioID=SCN.ScenarioCode
		INNER JOIN ProductReference PR							ON PR.ProductCode=TX.ProductCode
		INNER JOIN @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS	GR		ON GR.ChemicalGroup=PR.ChemicalGroup								
		INNER JOIN ProductReferencePricing PRP					ON PRP.Productcode=PR.Productcode AND PRP.Season=@SEASON AND PRP.Region='EAST'
	WHERE SCN.MarketLetterCode = 'ML2020_E_CCP'  AND CAST(tx.UpdateDate AS DATE) <= SCN.Cut_Off_Date
	GROUP BY SCN.Statementcode, SCN.MarketLetterCode, GR.ChemicalGroup
	

	-- UPDATE REWARD 
	-- Used DataSetCode instead of Statementcode to inner join with @SCENARIO_TRANSACTIONS as 
	-- some retailers were missing from the table Retail_Plan_Scenarios 
	UPDATE T1
	SET T1.Reward = RewardBrand_Sales * Reward_Percentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN @SCENARIO_TRANSACTIONS tx
		ON tx.Statementcode=IIF(T1.DataSetCode > 0, T1.DataSetCode, T1.Statementcode) 			
	WHERE ST.VersionType='Unlocked' AND ST.StatementType='Actual' AND tx.SDP > 0
	
	-- UPDATE REWARD
	-- FOR USE CASE STATEMENTS
	UPDATE T1
	SET T1.Reward = RewardBrand_Sales * Reward_Percentage
	FROM @TEMP T1
		INNER JOIN RP_Statements ST 
		ON ST.Statementcode=t1.Statementcode
		INNER JOIN (
			SELECT tx.Statementcode, SUM(PRICE_SDP) AS Plan_Amount
			FROM RP_DT_ScenarioTransactions tx
					INNER JOIN @STATEMENTS ST					ON ST.StatementCode=Tx.Statementcode
					INNER JOIN @SCENARIO_ELIGIBLE_PRODUCTS_GROUPS	PR	ON PR.ChemicalGroup=Tx.ChemicalGroup								
			WHERE tx.UC_DateModified <= @PLAN_CUT_OFF_DATE
			GROUP BY tx.Statementcode, PR.ChemicalGroup
		) T2
		ON T2.StatementCode=T1.StatementCode
	WHERE ST.VersionType='Use Case' AND ST.StatementType='Actual' AND  T2.Plan_Amount > 0

	--SET REWARD_PERCENTAGE TO ZERO IF NO REWARD FOR DISPLAY PURPOSES
	UPDATE @TEMP SET Reward_Percentage = 0 WHERE Reward <= 0 

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD --
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE T1
		SET [Reward]=[Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @Temp T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage'
		WHERE [Reward] > 0
	END
	--------------------------------------

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2020_DT_Rewards_E_PortfolioSupport WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2020_DT_Rewards_E_PortfolioSupport(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, QualBrand_Sales_CY, QualBrand_Sales_LY1, QualBrand_Sales_LY2, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,SUM(QualBrand_Sales_CY) AS QualBrand_Sales_CY
		,SUM(QualBrand_Sales_LY1) AS QualBrand_Sales_LY1
		,SUM(QualBrand_Sales_LY2) AS QualBrand_Sales_LY2
		,AVG(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand

END