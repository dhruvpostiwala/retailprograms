﻿
CREATE PROCEDURE [dbo].[RP_UpdateStatementMappings] @SEASON INT, @REGION VARCHAR(4), @STATEMENT_TYPE AS [VARCHAR](20)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SOLD_TO_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-02' AS DATE)		
	-- DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-01' AS DATE)	
	DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)
	
	DELETE FROM RP_StatementsMappings
	WHERE [StatementCode] IN (SELECT [StatementCode] FROM RP_Statements WHERE [Season]=@SEASON AND [Region]=@REGION AND [StatementType]=@STATEMENT_TYPE AND [VersionType] <> 'Use Case')


	DROP TABLE IF EXISTS #TempRetailerProfile

	SELECT RetailerCode, RetailerType , Level2Code, Level5Code, SoldRetailerCode
	INTO #TempRetailerProfile
	FROM (
		SELECT RetailerCode, RetailerType , Level2Code, Level5Code, IIF(ISNULL(SoldRetailerCode,'')='',RetailerCode,SoldRetailerCode) AS SoldRetailerCode
		FROM RetailerProfile
		WHERE [Region]=@Region AND [RetailerType] IN ('LEVEL 1','LEVEL 2') 	AND [Status] IN ('ACTIVE','INACTIVE') 

			UNION

		SELECT RetailerCode, RetailerType , Level2Code, Level5Code, SoldRetailerCode
		FROM RetailerProfile
		WHERE [Region]=@Region AND [RetailerType] IN ('LEVEL 1','LEVEL 2') 	
		AND [Status]='SOLD' AND SoldDate IS NOT NULL 
		AND CAST(SoldDate AS Date) BETWEEN @SOLD_TO_START_DATE  AND  @SOLD_TO_END_DATE
	) D

   	/*
		STATEMENTLEVEL = LEVEL 1 
		MAPPING: 1 STATEMENTCODE TO 1 RETAILER CODE
	*/
	INSERT INTO RP_StatementsMappings(StatementCode, RetailerCode, ActiveRetailerCode)
	SELECT ST.[StatementCode] ,ST.[RetailerCode] ,ST.[ActiveRetailerCode]
	FROM RP_Statements ST
	WHERE ST.[Status]='Active' AND ST.[StatementLevel]='LEVEL 1' 
		AND ST.[VersionType] IN ('UNLOCKED','FINAL-PREFERRED')
		AND ST.[Season]=@Season AND ST.[Region]=@Region AND ST.[StatementType]=@STATEMENT_TYPE 

	/*
		STATEMENTLEVEL = LEVEL 2
		MAPPING: 1 STATEMENTCODE TO MULTIPLE RETAILER CODES
	*/

	INSERT INTO RP_StatementsMappings(StatementCode, RetailerCode, ActiveRetailerCode)
	SELECT [StatementCode], RP.[RetailerCode], RP.[SoldRetailerCode]
	FROM (
			SELECT StatementCode, Level2Code
			FROM RP_Statements 
			WHERE Season=@SEASON AND Region=@REGION AND StatementType=@STATEMENT_TYPE  
				AND [Status] = 'Active'
				AND [VersionType] IN ('UNLOCKED','FINAL-PREFERRED') 
				AND [StatementLevel] = 'LEVEL 2' 				
				AND [RetailerCode] <> 'R520157196'				
		) ST		 
		INNER JOIN (	
			SELECT Level2Code, RetailerCode, SoldRetailerCode
			FROM #TempRetailerProfile
			WHERE [Level2Code] <> '' AND [RetailerType] IN ('LEVEL 1','LEVEL 2') 				
		) RP
		ON RP.Level2Code=ST.Level2Code
	   	 
	-- JIRA: RP-2487 -- Aligning RP statements with RC for Recon
	IF @SEASON IN (2020,2021) AND @REGION='WEST'
	BEGIN
		DECLARE @RETAILERCODES AS TABLE (
			Retailercode VARCHAR(20) NOT NULL
		)

		INSERT INTO @RETAILERCODES(RetailerCode)
		VALUES('R520157196')
		,('R520157441')
		,('R520157200')		 
		
		UPDATE MAP
		SET StatementCode=T2.StatementCode
		FROM RP_StatementsMappings MAP
			INNER JOIN RP_Statements ST	
			ON ST.Statementcode=MAP.Statementcode
			INNER JOIN (
				SELECT ST.[StatementCode], R.RetailerCode, ST.StatementType, ST.Versiontype
				FROM RP_Statements ST, @RETAILERCODES R					
				WHERE Season=@SEASON AND StatementType=@STATEMENT_TYPE AND [Status]='Active' AND VersionType IN ('Unlocked','Final-Preferred')
					AND ST.RetailerCode='R520157196' 
			) T2
			ON T2.RetailerCode=MAP.RetailerCode	AND T2.StatementType=ST.StatementType AND T2.Versiontype=ST.VersionType
		WHERE ST.Season=@SEASON  AND ST.StatementType=@STATEMENT_TYPE 
	END
		
	/*
		LEVEL 5 STATEMENTS MAPPING (AT THIS TIME APPLIES TO WEST LINE COMPANIES ONLY)
		IN THE WEST REGION, FOR LEVEL 5 STATEMENTS 
			- MAPPING SHOULD BE MAINTAINED FOR ALL LINE COMPANIES STATEMENTS 
			- NO NEED TO MAINTAIN MAPPING FOR FCL/COOP STATEMENT AS THIS IS TO BE SERVED AS SUMMARY STATEMENT

		PLEASE NOTE: IN THE EAST REGION
			- ALL LEVEL 5 STATEMENTS ARE MEANT FOR SUMMARY PURPOSE ONLY
			- NO NEED TO MAINTAING MAPPINGS
	*/
	

	IF @STATEMENT_TYPE='ACTUAL' 
	BEGIN
		INSERT INTO RP_StatementsMappings(StatementCode, RetailerCode, ActiveRetailerCode)
		SELECT ST.[StatementCode], RP.[RetailerCode], RP.[SoldRetailerCode]
		FROM RP_Statements ST		 
			INNER JOIN RP_Config_HOPaymentLevel L5
			ON L5.Level5Code=ST.RetailerCode AND L5.Season=ST.Season
			INNER JOIN #TempRetailerProfile RP
			ON RP.Level5Code=ST.Retailercode AND RP.RetailerType = 'LEVEL 1'
		WHERE ST.[Season]=@SEASON AND ST.[Region]=@REGION AND ST.[StatementType]=@STATEMENT_TYPE  
			AND ST.[VersionType]='UNLOCKED' 
			AND ST.[Status]='Active'  AND ST.[StatementLevel]='LEVEL 5'  			
			AND ISNULL(L5.[Summary],'No')='No'

	END	
		
	UPDATE RP_StatementsMappings
	SET ActiveRetailerCode=RetailerCode
	WHERE ActiveRetailerCode=''
	

END
