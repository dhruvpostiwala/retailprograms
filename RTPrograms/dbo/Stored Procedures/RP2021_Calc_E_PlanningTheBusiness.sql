﻿CREATE PROCEDURE [dbo].[RP2021_Calc_E_PlanningTheBusiness] @SEASON INT, @STATEMENT_TYPE VARCHAR(20),  @PROGRAM_CODE VARCHAR(50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,		
		RewardBrand_Sales MONEY NOT NULL,
		Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		Plan_Passed BIT NOT NULL DEFAULT 0,	
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			RewardBrand ASC			
		)
	)

	DECLARE @RewardPercentage DECIMAL(6,2) = 0.01

	INSERT INTO @TEMP(StatementCode,DataSetCode,MarketLetterCode,ProgramCode,RewardBrand,RewardBrand_Sales)
	SELECT ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, @PROGRAM_CODE AS ProgramCode,PR.ChemicalGroup
		,SUM(tx.Price_CY) AS RewardBrand_Sales		
	FROM @STATEMENTS ST				
		INNER JOIN RP_Statements RS	ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP2021_DT_Sales_Consolidated TX ON TX.StatementCode=ST.StatementCode
		INNER JOIN ProductReference PR					ON PR.ProductCode = tx.ProductCode
	WHERE RS.StatementLevel='LEVEL 1' AND TX.MarketLetterCode = 'ML2021_E_CPP' AND tx.ProgramCode='ELIGIBLE_SUMMARY' AND tx.GroupType='ELIGIBLE_BRANDS' 
	GROUP BY ST.StatementCode, RS.DataSetCode, tx.MarketLetterCode, tx.ProgramCode,PR.ChemicalGroup
	
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
	BEGIN
		UPDATE  T1
		SET RewardBrand_Sales *= ISNULL(EI.FieldValue/100,1) 
		FROM @TEMP T1
		INNER JOIN RPWeb_USER_EI_FieldValues EI ON T1.StatementCode = EI.StatementCode AND EI.ProgramCode = 'RP2021_E_ALL_PROGRAMS' AND EI.FieldCode = 'Radius_Percentage_CPP'
	END


	
		
	UPDATE @TEMP
	SET Reward_Percentage = @RewardPercentage, Reward = RewardBrand_Sales * @RewardPercentage

	-- INSERT INTO DT TABLE FROM TEMP TABLE
	DELETE FROM RP2021_DT_Rewards_E_PlanningTheBusiness WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2021_DT_Rewards_E_PlanningTheBusiness(Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, Reward_Percentage, Reward
	FROM @TEMP

		UNION

	SELECT DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
		,SUM(RewardBrand_Sales) AS RewardBrand_Sales
		,MAX(Reward_Percentage) AS Reward_Percentage
		,SUM(Reward) AS Reward
	FROM @TEMP
	WHERE DataSetCode > 0
	GROUP BY DataSetCode,MarketLetterCode,ProgramCode,RewardBrand
END

