﻿


CREATE PROCEDURE [dbo].[RPUC_Agent_UpdateSales] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;
	
	BEGIN TRY

		IF OBJECT_ID('tempdb..#Sales_To_Delete') IS NOT NULL DROP TABLE #Sales_To_Delete
		IF OBJECT_ID('tempdb..#Sales_To_Insert') IS NOT NULL DROP TABLE #Sales_To_Insert
		IF OBJECT_ID('tempdb..#AllSales_To_Insert') IS NOT NULL DROP TABLE #AllSales_To_Insert

		SELECT @REGION=Region, @DATASETCODE=DataSetCode FROM RP_Statements WHERE StatementCode=@StatementCode
		/*
		--SET @JSON='{"created":[],"updated":[{"statementtransnumber":1,"retailercode":"R400011638","farm":{"farmcode":"F0000112","label":"F0000112 (P)"},"invoicedate":"2019-08-13T04:00:00.000Z","quantity":170,"acres":10200,"srp":57103,"sdp":57103,"productname":"DYVEL DSp 2 X 10 L CASE","product":{"productcode":"000000000059010672","productname":"DYVEL DSp 2 X 10 L CASE"},"inradius":"Yes","farmname":"CULP FARMS"},{"statementtransnumber":8,"retailercode":"R400011638","farm":{"farmcode":"F0000154","label":"F0000154 (NP)"},"invoicedate":"2020-04-14T04:00:00.000Z","quantity":50,"acres":2000,"srp":39715,"sdp":37725,"productname":"LANCE 2 X 2.83 KG CASE","product":{"productcode":"000000000059021532","productname":"LANCE 2 X 2.83 KG CASE"},"inradius":"Yes","farmname":"JAMES HALTER"}],"destroyed":[]}'
		*/
		
		-- LETS GET StatementTransNumber to be deleted 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @StatementCode AS Statementcode, t1.StatementTransNumber
		INTO #Sales_To_Delete
		FROM (
			SELECT b.statementtransnumber
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						statementtransnumber INT N'$.statementtransnumber'					
					) as b	

				UNION ALL

			SELECT b.statementtransnumber
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						statementtransnumber INT N'$.statementtransnumber'					
					) as b		
		) T1			


		-- ALL STATEMENTS TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode ,t1.*
		INTO #Sales_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (												
							retailercode varchar(20) N'$.retailercode',
							farmcode VARCHAR(20) N'$.farm.farmcode',							
							invoicedate datetime N'$.invoicedate',
							productcode varchar(65) N'$.product.productcode',
							quantity numeric(18,2) N'$.quantity',
							acres numeric(18,2) N'$.acres',
							cfstatus varchar(50) N'$.cfstatus',
							srp money N'$.srp',
							sdp money N'$.sdp',
							swp money N'$.swp',
							inradius varchar(3) N'$.inradius'						
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								farmcode VARCHAR(20) N'$.farm.farmcode',								
								invoicedate datetime N'$.invoicedate',
								productcode varchar(65) N'$.product.productcode',
								quantity numeric(18,2) N'$.quantity',
								acres numeric(18,2) N'$.acres',
								cfstatus varchar(50) N'$.cfstatus',
								srp money N'$.srp',
								sdp money N'$.sdp',
								swp money N'$.swp',
								inradius varchar(3) N'$.inradius'						
							) as b
		) T1
	

		DELETE T1		
		FROM RP_DT_Transactions T1
			INNER JOIN #Sales_To_Delete T2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.StatementTransNumber=T2.StatementTransNumber
		
		
		/*  
			
			PRIMARY KEY:
			[StatementCode] ASC,
			[StatementTransNumber] ASC		

			LETS REGENERATE VALUES FOR '[StatementTransNumber]' 
		
			STEP #1: COLLECT DATA FROM ACTUAL TABLE INTO TEMPORARY TABLE. SINCE WE ARE GOING TO UPDATE '[StatementTransNumber]' COLUMN, NO NEED TO FETCH IT 				
			STEP #2: DELETE RECORDS FROM ACTUAL TABLE WHERE STATEMENTCODE=@STATEMENTCODE
			STEP #3: INSERT RECORDS BACK INTO ACTUAL TABLE FROM TEMPORARY TABLE. USE 'ROW_NUMBER()' FUNCTION TO INSERT VALUE INTO '[StatementTransNumber]' COLUMN

		*/

		-- STEP #1: Collect data into temporary table #ALLSales_To_Insert
		SELECT StatementCode,DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,CFStatus
			,ROW_NUMBER() OVER(ORDER BY RetailerCode, ChemicalGroup, InvoiceDate DESC) AS StatementTransNumber
		INTO #AllSales_To_Insert
		FROM (
			SELECT StatementCode,DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,ISNULL(CFStatus,'-') AS CFStatus
			FROM RP_DT_Transactions
			WHERE StatementCode=@StatementCode
		
				UNION ALL

			SELECT Statementcode,@DataSetCode,RetailerCode,FarmCode
				,'Use Case' AS TransCode
				,'Use Case' AS InvoiceNumber
				,tx.InvoiceDate
				,tx.ProductCode
				,tx.Quantity,Acres
				,IIF(MONTH(tx.InvoiceDate) IN (10,11,12),YEAR(tx.InvoiceDate)+1,YEAR(tx.InvoiceDate)) AS BASFSeason
				,YEAR(tx.InvoiceDate) AS Season
				,PR.ChemicalGroup
				,tx.SRP AS Price_SRP
				,tx.SDP AS Price_SDP
				,tx.SWP AS Price_SWP
				,IIF(InRadius='YES',1,0) AS InRadius
				,IIF(PR.ChemicalGroup LIKE 'Clearfield%' AND CFStatus='Yes','Complete','Incomplete') AS CFStatus
			FROM #Sales_To_Insert tx
				INNER JOIN ProductReference PR
				ON PR.ProductCode=tx.ProductCode
		) T1	

		
		-- STEP #2: DELETE RECORDS FROM  RP_DT_TRANSACTIONS
		DELETE RP_DT_TRANSACTIONS WHERE StatementCode=@StatementCode
		
		-- STEP #3: INSERT RECORDS FROM #AllSales_To_Insert INTO RP_DT_TRANSACTIONS 
		INSERT INTO RP_DT_TRANSACTIONS (StatementCode,StatementTransNumber,DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,CFStatus)
		SELECT StatementCode,StatementTransNumber,DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,CFStatus			
		FROM #AllSales_To_Insert		
				
		IF @REGION='EAST' AND @DATASETCODE > 0   -- not sure if SWP is need here leaving for now --Demarey
		BEGIN
			DELETE RP_DT_TRANSACTIONS WHERE StatementCode=@DataSetCode

			INSERT INTO RP_DT_TRANSACTIONS (StatementCode,DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,CFStatus,StatementTransNumber)
			SELECT @DataSetCode AS StatementCode, 0 AS DataSetCode,RetailerCode,FarmCode,TransCode,InvoiceNumber,InvoiceDate,ProductCode,Quantity,Acres,BASFSeason,Season,ChemicalGroup,Price_SRP,Price_SDP,Price_SWP,InRadius,CFStatus
				,ROW_NUMBER() OVER(ORDER BY RetailerCode, ChemicalGroup, InvoiceDate DESC) AS StatementTransNumber
			FROM RP_DT_Transactions
			WHERE DataSetCode=@DataSetCode
		END
		
	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

/****** Object:  StoredProcedure [dbo].[RPUC_Agent_UpdatePlanTransactions]    Script Date: 2020-05-19 12:57:18 PM ******/
SET ANSI_NULLS ON
