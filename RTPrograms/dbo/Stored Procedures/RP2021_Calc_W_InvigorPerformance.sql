﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_InvigorPerformance] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;


	-- The level 5 codes for West Independents and West Co-ops
	DECLARE @WEST_IND VARCHAR(10) = 'D000001';
	DECLARE @WEST_COOP VARCHAR(10) = 'D0000117';

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		Level5Code  VARCHAR (10) NOT NULL DEFAULT '',
		RewardBrand_Sales Money NOT NULL,
		QualBrand_Qty Numeric(18,2) NOT NULL,
		Sales_Target Money NOT NULL DEFAULT 0,
		Reward_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0,
		LY_Reseeded_Qty NUMERIC(18,2) NOT NULL DEFAULT 0,
		CY_Reseeded_Qty NUMERIC(18,2) NOT NULL DEFAULT 0,
		Growth_Ratio NUMERIC(8,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	/*
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
		BEGIN
			-- Get data for all the statements
			INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,QualBrand_Qty,Sales_Target)
			SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
				,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
				,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',(tx.Acres_CY * (ISNULL(EI.FieldValue,100)/100))/10,0)) AS QualBrand_Qty  -- Converted using CY_acres / 10
				,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Acres_LY1/10,0)) AS Sales_Target -- Converted using LY1_acres / 10
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX
				ON TX.StatementCode=ST.StatementCode
				LEFT JOIN RPWeb_User_EI_FieldValues EI
				ON EI.StatementCode=ST.StatementCode AND EI.MarketLetterCode=TX.MarketLetterCode  AND EI.FieldCode='Radius_Percentage_InVigor'
			WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
			GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
		END
	IF @STATEMENT_TYPE IN ('ACTUAL')
		BEGIN
			-- Get data for all the statements
			INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,QualBrand_Qty,Sales_Target)
			SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
				,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
				,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Acres_CY/10,0)) AS QualBrand_Qty  -- Converted using CY_acres / 10
				,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Acres_LY1/10,0)) AS Sales_Target -- Converted using LY1_acres / 10
			FROM @STATEMENTS ST
				INNER JOIN RP2021_DT_Sales_Consolidated TX
				ON TX.StatementCode=ST.StatementCode
			WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
			GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
		END
	*/

	INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,Level5Code,RewardBrand_Sales,QualBrand_Qty,Sales_Target)
	SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code
		,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Price_CY,0)) AS RewardBrand_Sales
		,SUM(IIF(tx.GroupType='QUALIFYING_BRANDS',tx.Acres_CY/10,0)) AS QualBrand_Qty  -- Converted using CY_acres / 10
		,SUM(IIF(tx.GroupType='REWARD_BRANDS',tx.Acres_LY1/10,0)) AS Sales_Target -- Converted using LY1_acres / 10
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
	WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType IN ('REWARD_BRANDS','QUALIFYING_BRANDS')
	GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode,TX.Level5Code

	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		UPDATE T1
		SET QualBrand_Qty = QualBrand_Qty * (ISNULL(EI.FieldValue,100)/100)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode AND EI.FieldCode='Radius_Percentage_InVigor'
	END
	   	 
	-- LETS ADJUST 2021 INVIGOR ACRES BY DEDUCTING RESEEEDED ACRES
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		UPDATE T1
		SET T1.CY_Reseeded_Qty = T2.CY_Reseeded_Qty
		    ,T1.QualBrand_Qty = T1.QualBrand_Qty - T2.CY_Reseeded_Qty
		FROM @TEMP T1
			INNER JOIN (
				SELECT ST.StatementCode, SUM(APA.CY_ProblemArea/10) AS CY_Reseeded_Qty
				FROM @STATEMENTS ST
					INNER JOIN [dbo].[RP_StatementsMappings] MAP
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT C.RetailerCode, SUM(ISNULL(CB.TotalProblemArea,0)) AS CY_ProblemArea
						FROM Claims C
							LEFT JOIN Claims_BasicInfo CB
							ON C.ID = CB.Claim_ID
						WHERE ISNULL(C.SoftDeleted,'') <> 'Yes' AND C.Season = @SEASON AND C.NOI_Code IN ('W_INV_RESEED','E_INV_RESEED') 
							AND C.Status NOT IN ('Draft','Deleted')  AND ISNULL(C.RetailerCode,'') <> '' AND ISNULL(CB.TotalProblemArea,0) > 0 
						GROUP BY C.RetailerCode
					) APA
					ON APA.RetailerCode=MAP.RetailerCode
				GROUP BY ST.Statementcode				
			) T2
			ON T2.Statementcode=T1.Statementcode
				

		-- LETS ADJUST 2020 INVIGOR ACRES BY DEDUCTING RESEEEDED ACRES
		UPDATE T1
		SET T1.LY_Reseeded_Qty = T2.LY_Reseeded_Qty
			,T1.Sales_Target = T1.Sales_Target-T2.LY_Reseeded_Qty
		FROM @TEMP T1
			INNER JOIN (
				SELECT ST.StatementCode, SUM(APA.ProblemAcres/10) AS LY_Reseeded_Qty
				FROM @STATEMENTS ST
					INNER JOIN [dbo].[RP_StatementsMappings] MAP
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT DealerCode AS RetailerCode, ProblemAcres
						FROM APA
						WHERE SoftDeleted='No' AND [Year]=@SEASON - 1 AND NatureOfInquiry='INVIGOR RESEED' AND [Status] <> 'DRAFT' AND [DealerCode] <> '' AND ProblemAcres > 0		
					) APA
					ON APA.RetailerCode=MAP.RetailerCode
				GROUP BY ST.Statementcode				
			) T2
			ON T2.Statementcode=T1.Statementcode
	END

	-- Update the reward percentage for all the statements
		/*
		RC/IND:
			2021 Eligible POG Growth Relative to Target	Reward
			90% +	2%
			80% to 89.9%	1.5%
			< 80%	1%

		FCL:
			2021 Eligible POG Growth Relative to Target	Reward
			90% +	1.45%
			80% to 89.9%	1%
			< 80%	0.75%

		Line Companies (Cargill, Richardson, UFA)
			2021 Share Relative to 2020	Reward
			97.5% +	3%
			95% to 97.49%	2.5%
			< 95%	2%
	*/

	-- IF SALES TARGET IS ZERO, GIVE TOP TIER PERCENTAGE, SO SET GROWTH TO EXCEED TOP TIER ACROSS ALL RETAILERS i.e 0.975 (97.5%) FOR LINE COMPANIES
	UPDATE @TEMP 	SET Growth_Ratio = 1.0 	WHERE Sales_Target = 0

	UPDATE @TEMP
	SET Growth_Ratio  = (QualBrand_Qty/Sales_Target) 
	WHERE Sales_Target > 0

	-- RC / IND , FCL
	UPDATE @TEMP 
	SET [Reward_Percentage] = IIF(Level5Code = @WEST_IND, 0.02, 0.0145) 
	WHERE Growth_Ratio + @ROUND_PERCENT >= 0.90 		
		AND Level5Code IN (@WEST_IND,@WEST_COOP)
	
	UPDATE @TEMP 
	SET [Reward_Percentage] = IIF(Level5Code = @WEST_IND, 0.015, 0.01) 
	WHERE Growth_Ratio + @ROUND_PERCENT >= 0.80 AND Growth_Ratio + @ROUND_PERCENT < 0.90
		AND Level5Code IN (@WEST_IND,@WEST_COOP)

	UPDATE @TEMP 
	SET [Reward_Percentage] = IIF(Level5Code = @WEST_IND, 0.01, 0.0075) 
	WHERE [Reward_Percentage] = 0 AND Level5Code IN (@WEST_IND,@WEST_COOP)
	
	--LINE COMPANIES (Cargill, Richardson, UFA)	
	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.03 
	WHERE Growth_Ratio + @ROUND_PERCENT >= 0.975
		AND Level5Code IN ('D0000107','D0000137','D0000244')
	
	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.025
	WHERE Growth_Ratio + @ROUND_PERCENT >= 0.95 AND Growth_Ratio + @ROUND_PERCENT < 0.975
		AND Level5Code IN ('D0000107','D0000137','D0000244')

	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.02
	WHERE [Reward_Percentage] = 0
		AND Level5Code IN ('D0000107','D0000137','D0000244')

	/*
		Nutrien will receive 8.5% on Eligible InVigor Reward Brand POG.		
		Eligible 2021 Reward Brand InVigor POG on page 5	8.5%
	*/

	UPDATE @TEMP 
	SET [Reward_Percentage] = 0.085
	WHERE [Reward_Percentage] = 0 AND [RewardBrand_Sales] > 0
		AND Level5Code = 'D520062427'
		
	-- Set the reward for all the statements based on the reward percentage set previously
	UPDATE @TEMP SET [Reward] = [RewardBrand_Sales] * [Reward_Percentage] WHERE [RewardBrand_Sales] > 0

	DELETE FROM RP2021_DT_Rewards_W_INV_PERF WHERE StatementCode IN (SELECT StatementCode FROM @Statements)

	INSERT INTO RP2021_DT_Rewards_W_INV_PERF(StatementCode, MarketLetterCode, ProgramCode, QualBrand_Qty, RewardBrand_Sales, RewardBrand_Qty, Sales_Target, Reward_Percentage, Reward)
	SELECT StatementCode, MarketLetterCode, ProgramCode, QualBrand_Qty, RewardBrand_Sales, QualBrand_Qty, Sales_Target, Reward_Percentage, Reward
	FROM @TEMP
END
