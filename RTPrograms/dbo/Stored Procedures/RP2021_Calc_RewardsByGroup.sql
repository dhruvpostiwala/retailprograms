﻿CREATE PROCEDURE [dbo].[RP2021_Calc_RewardsByGroup] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DELETE REW
	FROM RP_DT_RewardsByGroup REW
		INNER JOIN @STATEMENTS ST
		ON REW.StatementCode = ST.StatementCode

	-- BRAND SPECIFIC SUPPORT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_BrandSpecificSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_BRAND_SPEC_SUPPORT'

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'LANCE AG' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage * 0.50, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_BrandSpecificSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_BRAND_SPEC_SUPPORT'

	-- EFFICIENCY REBATE
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL BASF BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Support_Reward_Percentage + RS.Growth_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_EFF_BONUS RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_EFF_BONUS_REWARD'

	-- INVIGOR & LIBERTY LOYALTY BONUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Bonus_Percentage + RS.Addendum_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_LLB RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_INV_LIB_LOY_BONUS'

	-- INVIGOR INNOVATION BONUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, '300 SERIES HYBRIDS' [GroupLabel], 'MONEY' [RewardType], ISNULL(SUM(RS.InVigor300_Reward), 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_INNOV RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_INV_INNOVATION_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, '200 SERIES HYBRIDS' [GroupLabel], 'MONEY' [RewardType], ISNULL(SUM(RS.InVigor200_Reward), 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_INNOV RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_INV_INNOVATION_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- INVIGOR PERFORMANCE
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_INV_PERF RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_INV_PERFORMANCE'

	-- WAREHOUSE PAYMENT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_WarehousePayments RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_WAREHOUSE_PAYMENT'

	-- PAYMENT ON TIME
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_PaymentOnTime RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_PAYMENT_ON_TIME' AND ELG.MarketLetterCode <> 'ML2021_W_CPP'

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL CPP BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_PaymentOnTime RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_PAYMENT_ON_TIME' AND ELG.MarketLetterCode = 'ML2021_W_CPP'

	-- LOGISTICS SUPPORT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, ISNULL(RS.BrandSegment, '') [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Segment_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_LogisticsSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_LOGISTICS_SUPPORT'

	-- PLANNING THE BUSINESS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL CPP BRANDS' [GroupLabel], 'CENTURION,LIBERTY 150' [ExcludedChemicalGroups], 'PERCENTAGE' [RewardType], ISNULL(RS.CPP_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_PlanningTheBusiness RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_PLANNING_SUPP_BUSINESS'

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'LIBERTY 150' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Lib_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_PlanningTheBusiness RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_PLANNING_SUPP_BUSINESS'

	-- INVENTORY MANAGEMENT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL CPP BRANDS' [GroupLabel], 'NODULATOR,NODULATOR DUO SCG,NODULATOR GRANULAR,NODULATOR LQ,NODULATOR N/T,NODULATOR PRO,NODULATOR SCG,NODULATOR XL' [ExcludedChemicalGroups], 'PERCENTAGE' [RewardType], (SELECT MAX(Percentages) FROM (VALUES (MAX(ISNULL(RS.CPP_Percentage, 0))),(MAX(ISNULL(RS.Liberty_Percentage, 0))),(MAX(ISNULL(RS.Centurion_Percentage, 0)))) Data(Percentages)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_InventoryManagement RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_INVENTORY_MGMT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- GROWTH BONUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_GrowthBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_GROWTH_BONUS'

	-- LOYALTY BONUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'SEGMENT A' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.SegmentA_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_LoyaltyBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_LOYALTY_BONUS'

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'SEGMENT B' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.SegmentB_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_LoyaltyBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_LOYALTY_BONUS'

	-- RETAIL BONUS PAYMENT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'LIBERTY' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Lib_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_RetailBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_RETAIL_BONUS_PAYMENT'

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'CENTURION' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Cent_Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_RetailBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_RETAIL_BONUS_PAYMENT'

	-- SEGMENT SELLING	
	-- NEW: RRT-2070 - need to exclude Liberty and Centurion
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL CPP BRANDS' [GroupLabel], 'CENTURION,LIBERTY 150' [ExcludedChemicalGroups], 'PERCENTAGE' [RewardType], MAX(ISNULL(RS.Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_SegmentSelling RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_SEGMENT_SELLING'
	GROUP	BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- LIBERTY / CENTURION / FACET L TANK MIX BONUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(RS.Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_TankMixBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_TANK_MIX_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- DICAMBA TOLERANT PROGRAM
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(RS.Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_DicambaTolerant RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_DICAMBA_TOLERANT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CUSTOM SEED TREATMENT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'NODULATOR PRO' [ExcludedChemicalGroups], 'PERCENTAGE' [RewardType], ISNULL(MAX(RS.RewardBrand_Percentage), 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_CustomSeed RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_CUSTOM_SEED_TREATING_REWARD' AND RS.RewardBrand <> 'NODULATOR PRO 100' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD BRANDS' [GroupLabel], 'INSURE Cereal,INSURE CEREAL FX4,INSURE Pulse' [ExcludedChemicalGroups], 'PERCENTAGE' [RewardType], ISNULL(MAX(RS.RewardBrand_Percentage), 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_CustomSeed RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_CUSTOM_SEED_TREATING_REWARD' AND RS.RewardBrand = 'NODULATOR PRO 100' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CLEARFIELD LENTILS SUPPORT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], ISNULL(RS.Reward_Percentage, 0) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_ClearfieldLentils RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_CLL_SUPPORT' AND RS.Reward > 0

	-- FUNGICIDE SUPPORT (WEST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, ISNULL(RewardBrand,'') AS [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_Fung_Support RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_FUNG_SUPPORT'
	GROUP	BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, RewardBrand

	-- NODULATOR DUO ACQUISITION PROGRAM (WEST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'NODULATOR DUO SCG' AS [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_Nod_Duo_SCG RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_NOD_DUO_SCG'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CEREAL START TO FINISH (WEST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' AS [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_Cereal_SFO RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_CEREAL_START_FINISH'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- ADVANCED WEED CONTROL (WEST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' AS [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_Adv_Weed_Control RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_ADVANCED_WEED_CTRL'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CUSTOM AERIAL APPLICATION (WEST PROGRAM)
	INSERT	INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT	ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, ISNULL(RewardBrand,'') [GroupLabel]
			,REPLACE('CARAMBA,COTEGRA,DYAX,FORUM,HEADLINE,LANCE,LANCE AG,NEXICOR,PRIAXOR,SERCADIS,TWINLINE', ISNULL(RS.RewardBrand,''), '') AS ExcludedChemicalGroups
			,'PERCENTAGE' AS [RewardType]
			,MAX(ISNULL(Reward_Percentage, 0)) AS [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_W_CustomAerial RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_W_CUSTOM_AER_APP'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, RS.RewardBrand, REPLACE('CARAMBA,COTEGRA,DYAX,FORUM,HEADLINE,LANCE,LANCE AG,NEXICOR,PRIAXOR,SERCADIS,TWINLINE', ISNULL(RS.RewardBrand,''), '')



	-- PLANNING THE BUSINESS REWARD (EAST)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL EAST BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(RS.Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PlanningTheBusiness RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PLANNING_SUPP_BUSINESS' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- PORTFOLIO SUPPORT REWARD (EAST)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL EAST BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(RS.Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PortfolioSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PORTFOLIO_SUPPORT' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CUSTOM GROUND APPLICATION REWARD (EAST)

	-- CANTUS
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CARAMBA,COTEGRA,HEADLINE AMP,PRIAXOR,SEFINA' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'CANTUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CARAMBA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CANTUS,COTEGRA,HEADLINE AMP,PRIAXOR,SEFINA' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'CARAMBA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- COTEGRA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CANTUS,CARAMBA,HEADLINE AMP,PRIAXOR,SEFINA' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'COTEGRA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- HEADLINE AMP
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CANTUS,CARAMBA,COTEGRA,PRIAXOR,SEFINA' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'HEADLINE AMP'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- PRIAXOR
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CANTUS,CARAMBA,COTEGRA,HEADLINE AMP,SEFINA' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'PRIAXOR'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- SEFINA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 1' [GroupLabel], 'CANTUS,CARAMBA,COTEGRA,HEADLINE AMP,PRIAXOR' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'SEFINA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- ARMEZON PRO
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'ARMEZON PRO'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CONQUEST LQ
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'CONQUEST LQ'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- ENGENIA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'ENGENIA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- FRONTIER MAX
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'FRONTIER'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- INTEGRITY
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,LIBERTY 200,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'INTEGRITY'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- LIBERTY 200
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,OPTILL,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'LIBERTY 200'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- OPTILL
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,PROWL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'OPTILL'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- PROWL H2O
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,ZIDUA SC,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'PROWL'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- ZIDUA SC
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ERAGON LQ' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'ZIDUA SC'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- ERAGON LQ
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD GROUP 2' [GroupLabel], 'ARMEZON PRO,CONQUEST LQ,ENGENIA,FRONTIER,INTEGRITY,LIBERTY 200,OPTILL,PROWL,ZIDUA SC' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_CUSTOM_GROUND_APP_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'ERAGON LQ'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- LOYALTY (PURSUIT) SUPPORT REWARD (EAST)

	-- PURSUIT
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'PURSUIT' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'PURSUIT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- FRONTIER
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'FRONTIER' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'FRONTIER'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- HEADLINE EC
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'HEADLINE' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'HEADLINE'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- PRIAXOR
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'PRIAXOR' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'PRIAXOR'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- CONQUEST LQ
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'CONQUEST LQ' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'CONQUEST LQ'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- FORUM
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'FORUM' [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_PURSUIT_REWARD' AND RS.Reward > 0 AND RS.RewardBrand = 'FORUM'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- FUNGICIDES SUPPORT REWARD (EAST)

	-- CARAMBA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'COTEGRA,HEADLINE AMP,HEADLINE,PRIAXOR,TWINLINE' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'CARAMBA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- COTEGRA
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'CARAMBA,HEADLINE AMP,HEADLINE,PRIAXOR,TWINLINE' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'COTEGRA'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- HEADLINE AMP
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'CARAMBA,COTEGRA,HEADLINE,PRIAXOR,TWINLINE' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'HEADLINE AMP'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- HEADLINE
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'CARAMBA,COTEGRA,HEADLINE AMP,PRIAXOR,TWINLINE' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'HEADLINE'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- PRIAXOR
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'CARAMBA,COTEGRA,HEADLINE AMP,HEADLINE,TWINLINE' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'PRIAXOR'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- TWINLINE
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, ExcludedChemicalGroups, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'CARAMBA,COTEGRA,HEADLINE AMP,HEADLINE,PRIAXOR' [ExcludedChemicalGroups], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_FUNGICIDES_SUPPORT' AND RS.Reward > 0 AND RS.RewardBrand = 'TWINLINE'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- SUPPLY OF SALES RESULTS REWARD (EAST)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL EAST BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_SupplySales RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_SUPPLY_SALES' AND ELG.MarketLetterCode = 'ML2021_E_CPP' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_SupplySales RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_SUPPLY_SALES' AND ELG.MarketLetterCode = 'ML2021_E_INV' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- POG SALES BONUS REWARD (EAST)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_POGSalesBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_POG_SALES_BONUS' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- SALES SUPPORT - LOYALTY BONUS REWARD (EAST)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT DISTINCT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_SalesSupportLoyaltyBonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_SALES_SUP_LOYALTY_BONUS' AND RS.Reward > 0
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

	-- IGNITE LOYALTY SUPPORT (EAST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, ISNULL(RS.RewardBrand,'') AS [GroupLabel], 'MONEY' [RewardType], SUM(ISNULL(Reward, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_Ignite_LoyaltySupport RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_IGNITE_LOYAL_SUPPORT'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, RS.RewardBrand

	-- SOLLIO GROWTH BONUS (EAST PROGRAM)
	INSERT INTO RP_DT_RewardsByGroup(StatementCode, MarketLetterCode, ProgramCode, GroupLabel, RewardType, Reward)
	SELECT ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode, 'ALL REWARD BRANDS' AS [GroupLabel], 'PERCENTAGE' [RewardType], MAX(ISNULL(Reward_Percentage, 0)) [Reward]
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG
		ON ST.StatementCode = ELG.StatementCode
		LEFT JOIN RP2021_DT_Rewards_E_Sollio_Growth_Bonus RS
		ON ST.StatementCode = RS.StatementCode AND ELG.MarketLetterCode = RS.MarketLetterCode AND ELG.ProgramCode = RS.ProgramCode
	WHERE ELG.ProgramCode = 'RP2021_E_SOLLIO_GROWTH_BONUS'
	GROUP	BY  ST.StatementCode, ELG.MarketLetterCode, ELG.ProgramCode

END
