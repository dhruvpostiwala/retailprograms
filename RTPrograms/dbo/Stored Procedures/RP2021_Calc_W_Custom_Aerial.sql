﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_Custom_Aerial]  @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	-- PART OF OTHER PROGRAMS , APPLICABLE ONLY ON ACTUAL STATEMENTS
	IF @STATEMENT_TYPE <> 'ACTUAL' 
	RETURN

	-- THIS PROGRAM USES SRP PRICING FOR ALL RETAILERS
	/*
	 MP: Code review on Sep/06/2021
		 This program goes on to single market letter i.e CCP
		 There is no point maintaing market letter code and program code in temp tables, waste of additional joins.
		
		I am removing market letter code and program code columns from Temp tables. If required pull a back up before Sep/6/2021 for comparison.
		By removing market letter code and program code from temp table, procedure would run faster.
		Same needs to be done in other programs as well
	*/

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4))
	DECLARE @CPP_ML_CODE VARCHAR(50) = 'ML' + @SEASON_STRING + '_W_CPP'

	DECLARE @CUST_AERIAL_STATEMENTS TABLE (
		Statementcode INT,		
		StatementLevel VARCHAR(50) NOT NULL,
		Category VARCHAR(50) NOT NULL,
		Pricing_Type VARCHAR(3) NOT NULL
	)	  	

	DECLARE @SALES TABLE (
		StatementCode INT,
		RewardBrand Varchar(50) NOT NULL,
		ProductCode Varchar(50) NOT NULL,
		POG_Sales MONEY NOT NULL DEFAULT 0,
		POG_Acres DECIMAL(20,4)  NOT NULL DEFAULT 0,
		Cust_App_Quantity DECIMAL(20,4) NOT NULL DEFAULT 0,
		Cust_App_Acres DECIMAL(20,4) NOT NULL DEFAULT 0,
		Cust_App_Sales MONEY NOT NULL DEFAULT 0,
		Reward_Percentage DECIMAL(6,5) NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED (
			[StatementCode] ASC,
			[ProductCode]
		)
	)

	DECLARE @REWARD_PERC FLOAT = 0.025;
	DECLARE @MINIMUM_ACRES DECIMAL(20,4) = 5000;
	DECLARE @TOTAL_CA_ACRES DECIMAL(20,4) 
	
	DROP TABLE IF EXISTS #Eligible_Products
	SELECT ProductCode, ProductName, ChemicalGroup
	INTO #Eligible_Products
	FROM ProductReference 
	WHERE ChemicalGroup IN ('CARAMBA','COTEGRA','DYAX','FORUM','HEADLINE','LANCE','LANCE AG','NEXICOR','PRIAXOR','SERCADIS','TWINLINE')

	-- Account types: “Custom Aerial Applicator” and/or “Third Party Custom Aerial App”
	INSERT INTO @CUST_AERIAL_STATEMENTS(Statementcode, StatementLevel, Category, Pricing_Type)
	SELECT rs.StatementCode,  rs.StatementLevel, RS.Category, 'SRP' AS [Pricing_Type]
		/*
		,MAX(CASE 
			WHEN RS.StatementLevel = 'LEVEL 5' THEN 'SWP'
			WHEN RS.Level5Code = 'D0000117' THEN 'SRP'
			ELSE 'SDP' 
		END) AS [Pricing_Type]
		*/
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode=ST.StatementCode
		INNER JOIN RP_StatementsMappings Map	ON MAP.StatementCode=ST.StatementCode
		INNER JOIN (
			SELECT Retailercode
			FROM RetailerProfileAccountType
			WHERE AccountType IN ('Custom Aerial App','Custom Aerial Applicator','Third Party Custom Aerial App')
			GROUP BY Retailercode
		) Act
		ON Act.Retailercode=Map.ActiveRetailercode
	GROUP BY rs.StatementCode,  rs.StatementLevel, RS.Category

	-- Quantity		Acres	Price_SRP	Price_SDP	Price_SWP
	--  #TEMP_SALES	-- GET SALES INFO FOR REGULAR STATEMENTS ONLY
	DROP TABLE IF EXISTS #TEMP_SALES
	SELECT ST.Statementcode,ST.StatementLevel,tx.RetailerCode,EB.ProductCode,EB.ProductName, tx.ChemicalGroup AS RewardBrand
		,SUM(ISNULL(tx.Acres,0)) AS Acres
		,SUM(ISNULL(TX.Price_SRP,0)) AS Sales		
	INTO #TEMP_SALES
	FROM @CUST_AERIAL_STATEMENTS ST			
		INNER JOIN RP_DT_Transactions TX	ON TX.StatementCode = ST.StatementCode
		INNER JOIN #Eligible_Products EB	ON EB.productcode = TX.productcode 	
	WHERE TX.BasfSeason = @SEASON and TX.InRadius = 1 AND ST.Category='REGULAR'
	GROUP BY ST.Statementcode,ST.StatementLevel,tx.RetailerCode,EB.Productcode,EB.ProductName,tx.ChemicalGroup
	   	  
	-- OU AND HEAD OFFICE BREAK DOWN	
	DELETE T1
	FROM RP_DT_HO_BreakDownSummary T1
		INNER JOIN @STATEMENTS ST
		ON ST.Statementcode=T1.Statementcode
	WHERE T1.ProgramCode=@PROGRAM_CODE 

	INSERT INTO RP_DT_HO_BreakDownSummary(StatementCode,MarketLetterCode,ProgramCode,RetailerCode,FarmCode,Sales,FarmLevelSplit)
	SELECT TX.StatementCode, @CPP_ML_CODE, @PROGRAM_CODE, tx.RetailerCode, '' AS FarmCode,SUM(Sales) AS Sales
		,0 AS FarmLevelSplit
	FROM #TEMP_SALES TX		
	WHERE TX.StatementLevel IN ('LEVEL 2','LEVEL 5') 
	GROUP BY TX.StatementCode, tx.RetailerCode
	-- END OF HEAD OFFICE BREAK DOWN
	
	-- #TEMP_SALES CONTAINS SALES FROM REGULAR STATEMENTS ONLY
	INSERT INTO @SALES(StatementCode,RewardBrand,ProductCode,POG_Sales,POG_Acres)
	SELECT tx.StatementCode,tx.RewardBrand,tx.ProductCode 
		,SUM(tx.Sales) AS POG_Sales
		,SUM(tx.Acres) AS POG_Acres
	FROM #TEMP_SALES tx 		
	GROUP BY tx.StatementCode,tx.RewardBrand,tx.ProductCode 
	   	

	-- CUSTOM APP DATA FOR ALL STATEMENT CATEGORIES (Regular, Custom Aerial)
	DROP TABLE IF EXISTS #Custom_App_Data
	SELECT ST.Statementcode ,PR.ProductCode											
		,SUM(
			CASE 
				WHEN cap.PackageSize='Jugs' AND PR.LEUConversion <> 0 THEN cap.Quantity/PR.LEUConversion
				WHEN cap.PackageSize IN ('Litres','Kgs') THEN cap.Quantity/PR.Volume				
				ELSE cap.Quantity
			END
		) AS Quantity
		,SUM(cap.Quantity * PR.ConversionW) AS Acres		
		,SUM(
			cap.Quantity *	(
				CASE 
					WHEN cap.PackageSize='Jugs' THEN IIF(RS.Pricing_Type ='SWP', PR.PricePerJug_SWP, IIF(RS.Pricing_Type='SRP',PR.PricePerJug_SRP,PR.PricePerJug_SDP))					
					WHEN cap.PackageSize IN ('Litres','Kgs') THEN IIF(RS.Pricing_Type = 'SWP',PR.PricePerLitre_SWP,IIF(RS.Pricing_Type='SRP',PR.PricePerLitre_SRP,PR.PricePerLitre_SDP))
					ELSE IIF(RS.Pricing_Type = 'SWP',PR.SWP, IIF(RS.Pricing_Type='SRP',PR.SRP,PR.SDP))
				END
			)
		) AS [Sales]					
	INTO #Custom_App_Data		
	FROM @CUST_AERIAL_STATEMENTS ST
		INNER JOIN (
			SELECT StatementCode
				,'SRP' AS Pricing_Type
				/*
				,CASE 
					WHEN StatementLevel='LEVEL 5' THEN 'SWP'
					WHEN Level5Code='D0000117' THEN 'SRP'
					ELSE 'SDP'
				END AS Pricing_Type
				*/
			FROM RP_Statements
			WHERE Season = @Season AND StatementType='Actual'
		) RS 
		ON ST.StatementCode = RS.StatementCode
		INNER JOIN (
			SELECT StatementCode,ProductCode,PackageSize,SUM(Quantity) Quantity 			
			FROM RP_DT_CustomAppProducts 			
			WHERE Season=@SEASON AND ProductCode <> '' 
			GROUP BY StatementCode,ProductCode,PackageSize
		) CAP 	
		ON CAP.Statementcode=ST.Statementcode
		INNER JOIN (					
			SELECT PR1.Productcode, PR1.ChemicalGroup, PR1.ConversionW, PR1.Jugs,  PR1.Volume, PR1.LEUConversion
				,PR1.ConversionW/PR1.Jugs AS AcresPerJug
				,PR1.ConversionW/PR1.Volume as AcresPerLitre
				,PR1.Volume/PR1.Jugs AS LitresPerJug		
				--SDP
				,PRP.SDP
				,PRP.SDP/PR1.Jugs AS PricePerJug_SDP
				,PRP.SDP/PR1.Volume AS PricePerLitre_SDP
				--SRP
				,PRP.SRP
				,PRP.SRP/PR1.Jugs AS PricePerJug_SRP
				,PRP.SRP/PR1.Volume AS PricePerLitre_SRP				
				--SWP
				,PRP.SWP
				,PRP.SWP/PR1.Jugs AS PricePerJug_SWP
				,PRP.SWP/PR1.Volume AS PricePerLitre_SWP				
			FROM #Eligible_Products ELG
				INNER JOIN ProductReference PR1				ON PR1.ProductCode=ELG.ProductCode
				INNER JOIN ProductReferencePricing PRP		ON PRP.Productcode=PR1.Productcode AND PRP.Season=@SEASON AND PRP.Region='WEST'													
		) PR			
		ON PR.ProductCode=CAP.ProductCode				
	GROUP BY ST.Statementcode, PR.ProductCode

	-- MATCH CUSTOM APP SALES 
	/*
	 PLEASE NOTE:
	 ON REGULAR STATEMENTS CUSTOM APP SALES AND ACRES BEING SAVED IS MATCHED SAELS AND ACRES (NOT THE ORIGINAL CUSTOM APP SALES AND ACRES FROM CUSTOM APP DATA)
	*/

	UPDATE T1
	SET  T1.Cust_App_Quantity = CA.Quantity
		,T1.Cust_App_Acres = IIF(CA.Acres <= POG_Acres, CA.Acres, POG_Acres)
		,T1.Cust_App_Sales = IIF(CA.Sales <= POG_Sales, CA.Sales, POG_Sales)
	FROM @SALES T1
		INNER JOIN #Custom_App_Data CA 
		ON CA.StatementCode = T1.StatementCode AND CA.ProductCode = T1.ProductCode
			   
	DROP TABLE IF EXISTS #Total_Custom_App_Acres

	SELECT StatementCode, SUM(Cust_App_Acres) Acres
	INTO #Total_Custom_App_Acres
	FROM @SALES
	GROUP BY StatementCode

	UPDATE T1
	SET Reward_Percentage = @REWARD_PERC 
	FROM @SALES T1
		INNER JOIN #Total_Custom_App_Acres T2
		ON T2.StatementCode=T1.StatementCode 
	WHERE T2.Acres+0.0001 >= @MINIMUM_ACRES	

	UPDATE @SALES 
	SET Reward = Cust_App_Sales * Reward_percentage 
	WHERE Reward_Percentage > 0 

	/******************* CUSTOM AERIAL STATEMENTS *************************/
	-- CUSTOM APP DATA FOR CUSTOM AERIAL STATEMENTS 	
	-- FOR CUSTOM AERIAL STATEMENTS , NO NEED TO GET SALES DATA

	DELETE @CUST_AERIAL_STATEMENTS WHERE Category <> 'Custom Aerial'
	
	INSERT INTO @SALES(StatementCode,RewardBrand,ProductCode,Cust_App_Quantity,Cust_App_Acres,Cust_App_Sales)
	SELECT CA.StatementCode, ELG.ChemicalGroup, CA.ProductCode		
		,SUM(CA.Quantity) AS Cust_App_Quantity
		,SUM(CA.Acres) AS Cust_App_Acres
		,SUM(CA.Sales) AS Cust_App_Sales
	FROM #Custom_App_Data CA		
		INNER JOIN #Eligible_Products ELG		ON ELG.ProductCode=CA.Productcode
		INNER JOIN @CUST_AERIAL_STATEMENTS ST	ON ST.StatementCode=CA.StatementCode
	WHERE ST.Category='Custom Aerial'
	GROUP BY CA.StatementCode, ELG.ChemicalGroup, CA.ProductCode
	
	UPDATE T1
	SET Reward_Percentage = @REWARD_PERC		
		,Reward = Cust_App_Sales * @REWARD_PERC
	FROM @SALES T1
		INNER JOIN @CUST_AERIAL_STATEMENTS RS	
		ON RS.StatementCode = T1.StatementCode
	WHERE  RS.Category='Custom Aerial'
			
	/*
	 PLEASE NOTE:
	 ON REGULAR STATEMENTS CUSTOM APP SALES AND ACRES BEING SAVED IS MATCHED SAELS AND ACRES (NOT THE ORIGINAL CUSTOM APP SALES AND ACRES FROM CUSTOM APP DATA)
	*/

	DELETE FROM RP2021_DT_Rewards_W_CustomAerial WHERE [StatementCode] IN (SELECT StatementCode FROM @Statements)
	
	INSERT INTO RP2021_DT_Rewards_W_CustomAerial (StatementCode, MarketLetterCode,ProgramCode,RewardBrand, ProductCode,RewardBrand_Sales,RewardBrand_Acres,Cust_App_Quantity, Cust_App_Acres, RewardBrand_Cust_App, Reward_Percentage, Reward)
	SELECT StatementCode, @CPP_ML_CODE, @PROGRAM_CODE,RewardBrand,ProductCode,POG_Sales, POG_Acres, Cust_App_Quantity, Cust_App_Acres, Cust_App_Sales, Reward_Percentage,Reward
	FROM @SALES
	


END