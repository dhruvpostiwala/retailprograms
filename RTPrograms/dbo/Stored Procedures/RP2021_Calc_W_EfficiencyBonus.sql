﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_EfficiencyBonus] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	--DECLARE @SEASON INT = 2021
	--DECLARE @STATEMENT_TYPE [VARCHAR](20) = 'PROJECTION'
	--DECLARE @PROGRAM_CODE [VARCHAR](50) = 'RP2021_W_EFF_BONUS_REWARD'
	--DECLARE @STATEMENTS UDT_RP_STATEMENT
	--INSERT INTO @STATEMENTS (StatementCode)
	--SELECT StatementCode FROM RP_Statements where Season = 2021 and Status = 'Active' AND StatementType = 'Projection' and Region = 'West'

	DECLARE @ROUNDING_DOLLAR MONEY = 5000;
	DECLARE @ROUNDING_PERCENT DECIMAL(6,4) = 0.005;

	DECLARE @TEMP TABLE (
		StatementCode int,
		MarketLetterCode varchar(50) NOT NULL,
		ProgramCode varchar(50) NOT NULL,
		GroupLabel varchar(100) NOT NULL,
		Support_Reward_Sales MONEY NOT NULL,		
		Support_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Support_Reward MONEY NOT NULL DEFAULT 0,
		Escalator_Bonus BIT NOT NULL DEFAULT 0,
		Growth_Reward_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		Growth_Reward MONEY NOT NULL DEFAULT 0,
		Reward MONEY NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[GroupLabel] ASC			
		)
	)

	DECLARE @QUALIFIERS TABLE (
		StatementCode INT,
		MarketLetterCode VARCHAR(50) NOT NULL,
		ProgramCode VARCHAR(50) NOT NULL,		
		All_QualSales_CY  MONEY NOT NULL DEFAULT 0,
		All_QualSales_LY  MONEY NOT NULL DEFAULT 0,
		Growth_QualSales_CY MONEY NOT NULL DEFAULT 0,
		Growth_QualSales_LY MONEY NOT NULL DEFAULT 0,		
		Support_Reward_Percentage DECIMAL(6,4) NOT NULL DEFAULT 0,
		Growth_Reward_Percentage DECIMAL(19,4) NOT NULL DEFAULT 0,
		Growth DECIMAL(19,4) NOT NULL DEFAULT 0,
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC
		)
	)

	-- Qualifiers Information
	INSERT INTO @QUALIFIERS(StatementCode,MarketLetterCode,ProgramCode,All_QualSales_CY,All_QualSales_LY,Growth_QualSales_CY,Growth_QualSales_LY)
	SELECT ST.StatementCode, 'ML2021_W_INV' AS MarketLetterCode, 'RP2021_W_EFF_BONUS_REWARD' AS ProgramCode
		,SUM(CASE
				WHEN @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION') THEN
					tx.Price_CY * CASE
									WHEN TX.GroupLabel = 'InVigor' THEN
										(ISNULL(EI1.FieldValue, 100) / 100)
									WHEN TX.GroupLabel IN ('LIBERTY', 'LIBERTY 150', 'LIBERTY 200') THEN
										(ISNULL(EI2.FieldValue, 100) / 100)
									WHEN TX.GroupLabel = 'CENTURION' THEN
										(ISNULL(EI3.FieldValue, 100) / 100)
									ELSE 
										(ISNULL(EI4.FieldValue, 100) / 100)
									END
				ELSE
					tx.Price_CY
				END) AS All_QualSales_CY
		,SUM(tx.Price_LY1) AS All_QualSales_LY
		,SUM(CASE WHEN tx.GroupLabel IN ('INVIGOR','LIBERTY 150','LIBERTY 200') THEN 0 ELSE tx.Price_CY END) AS Growth_QualSales_CY
		,SUM(CASE WHEN tx.GroupLabel IN ('INVIGOR','LIBERTY 150','LIBERTY 200') THEN 0 ELSE tx.Price_LY1 END) AS Growth_QualSales_LY		
	FROM @STATEMENTS ST
		INNER JOIN RP2021_DT_Sales_Consolidated TX
		ON TX.StatementCode=ST.StatementCode
		LEFT JOIN RPWeb_User_EI_FieldValues EI1
		ON EI1.StatementCode=ST.StatementCode AND EI1.MarketLetterCode=TX.MarketLetterCode AND EI1.FieldCode='Radius_Percentage_InVigor'
		LEFT JOIN RPWeb_User_EI_FieldValues EI2
		ON EI2.StatementCode=ST.StatementCode AND EI2.MarketLetterCode=TX.MarketLetterCode AND EI2.FieldCode='Radius_Percentage_Liberty'
		LEFT JOIN RPWeb_User_EI_FieldValues EI3
		ON EI3.StatementCode=ST.StatementCode AND EI3.MarketLetterCode=TX.MarketLetterCode AND EI3.FieldCode='Radius_Percentage_Centurion'
		LEFT JOIN RPWeb_User_EI_FieldValues EI4
		ON EI4.StatementCode=ST.StatementCode AND EI4.MarketLetterCode=TX.MarketLetterCode AND EI4.FieldCode='Radius_Percentage_CPP'
	WHERE tx.ProgramCode='ELIGIBLE_SUMMARY' AND tx.GroupType='ELIGIBLE_BRANDS'
	GROUP BY ST.StatementCode



	-- Update the support reward %
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.025 WHERE All_QualSales_CY + @ROUNDING_DOLLAR >= 2000000
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.0175 WHERE Support_Reward_Percentage=0 AND All_QualSales_CY + @ROUNDING_DOLLAR >= 1500000
	UPDATE @QUALIFIERS SET [Support_Reward_Percentage] = 0.01 WHERE Support_Reward_Percentage=0 AND All_QualSales_CY + @ROUNDING_DOLLAR >= 1000000

	-- Determine growth on total sales even though we are storing the data at brand level sales
	UPDATE @QUALIFIERS
	SET [Growth] = [Growth_QualSales_CY] / [Growth_QualSales_LY]
	WHERE [Growth_QualSales_LY] > 0

	-- Update the growth reward % but only if the support reward % is 1% or 1.75%
	UPDATE QUAL
	SET QUAL.[Growth_Reward_Percentage] = 0.0075 -- Bascially this the difference between reward tiers
	FROM @QUALIFIERS QUAL
	WHERE [Support_Reward_Percentage] IN (0.01, 0.0175) AND (([Growth] = 0 AND [Growth_QualSales_CY] > 0) OR [Growth] + @ROUNDING_PERCENT >= 1.15)

	-- LETS CALCULATE REWARDS ON ELIGIBLE SALES BY APPLY REWARD PERCENTAGES
	-- Determine sales at the brand level
	
	IF @STATEMENT_TYPE IN ('FORECAST', 'PROJECTION')
	BEGIN
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Support_Reward_Sales, Escalator_Bonus) 
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, SUM(tx.Price_CY) AS Support_Reward_Sales,
		IIF(EI.FieldValue = 100, 1, 0) AS Escalator_Bonus
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON TX.StatementCode=ST.StatementCode

			INNER JOIN RPWeb_User_EI_FieldValues EI
			ON ST.StatementCode = EI.StatementCode AND EI.MarketLetterCode = TX.MarketLetterCode AND EI.FieldCode = 'Escalator_Bonus'
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, EI.FieldValue
	END

	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		INSERT INTO @TEMP(StatementCode,MarketLetterCode,ProgramCode,GroupLabel,Support_Reward_Sales, Escalator_Bonus) 
		SELECT ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, SUM(tx.Price_CY) AS Support_Reward_Sales,
		IIF(QUAL.[Growth_Reward_Percentage] > 0, 1, 0) AS Escalator_Bonus
		FROM @STATEMENTS ST
			INNER JOIN RP2021_DT_Sales_Consolidated TX
			ON TX.StatementCode=ST.StatementCode
			INNER JOIN @QUALIFIERS QUAL ON ST.StatementCode = QUAL.StatementCode AND TX.ProgramCode = QUAL.ProgramCode AND TX.MarketLetterCode = QUAL.MarketLetterCode
		WHERE tx.ProgramCode=@PROGRAM_CODE AND tx.GroupType='REWARD_BRANDS' 
		GROUP BY ST.StatementCode, tx.MarketLetterCode, tx.ProgramCode, tx.GroupLabel, QUAL.[Growth_Reward_Percentage]
	END

	-- Update the support reward % and growth reward %  and reward values
	UPDATE T1
	SET [Support_Reward_Percentage] = T2.[Support_Reward_Percentage]
		,[Support_Reward] = [Support_Reward_Sales] * T2.[Support_Reward_Percentage]
		,[Growth_Reward_Percentage] = IIF(T1.Escalator_Bonus = 1, T2.[Growth_Reward_Percentage], 0)
		,[Growth_Reward] = IIF(T1.Escalator_Bonus = 1, [Support_Reward_Sales] * T2.[Growth_Reward_Percentage], 0)
	FROM @TEMP T1
		INNER JOIN @QUALIFIERS T2 
	ON T1.[StatementCode] = T2.[StatementCode] AND T1.[MarketLetterCode] = T2.[MarketLetterCode] AND T1.[ProgramCode] = T2.[ProgramCode]
	WHERE [Support_Reward_Sales] > 0

	-- Update final reward
	UPDATE @TEMP SET [Reward] = [Support_Reward] + [Growth_Reward] WHERE [Support_Reward] + [Growth_Reward] > 0

	-- LETS INSERT QUALIFIERS META DATA INTO ACTUAL TABLES FROM TEMP TABLES
	DELETE FROM RP2021_DT_Rewards_W_EFF_BONUS_QUAL WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_EFF_BONUS_QUAL(Statementcode, MarketLetterCode, ProgramCode, All_QualSales_CY, All_QualSales_LY, Growth_QualSales_CY, Growth_QualSales_LY, Growth)
	SELECT Statementcode, MarketLetterCode, ProgramCode, All_QualSales_CY, All_QualSales_LY, Growth_QualSales_CY, Growth_QualSales_LY, Growth
	FROM @QUALIFIERS

	-- LETS INSERT ELIIGIBLE SALES & REWARDS META DATA INTO ACTUAL TABLES FROM TEMP TABLES
	DELETE FROM RP2021_DT_Rewards_W_EFF_BONUS WHERE [StatementCode] IN (SELECT [StatementCode] FROM @STATEMENTS)
	
	INSERT INTO RP2021_DT_Rewards_W_EFF_BONUS(Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Support_Reward_Sales, Support_Reward_Percentage, Support_Reward, Growth_Reward_Percentage, Growth_Reward, Reward)
	SELECT Statementcode, MarketLetterCode, ProgramCode, GroupLabel, Support_Reward_Sales, Support_Reward_Percentage, Support_Reward, Growth_Reward_Percentage, Growth_Reward, Reward
	FROM @TEMP
END
