﻿
CREATE PROCEDURE [dbo].[RPUC_Agent_UpdateDataSubmissions] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @REGION VARCHAR(4);
	DECLARE @DATASETCODE INT=0;

	BEGIN TRY


		IF OBJECT_ID('tempdb..#Transactions_To_Delete') IS NOT NULL DROP TABLE #Transactions_To_Delete
		IF OBJECT_ID('tempdb..#Transactions_To_Insert') IS NOT NULL DROP TABLE #Transactions_To_Insert
		IF OBJECT_ID('tempdb..#AllTransactions_To_Insert') IS NOT NULL DROP TABLE #AllTransactions_To_Insert

		 
		-- Updated records should be deleted too, we wil reinsert them back
		SELECT @STATEMENTCODE AS Statementcode, t1.RP_ID
		INTO #Transactions_To_Delete
		FROM (
			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				destroyed NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.destroyed)
					WITH (					
						RP_ID INT N'$.rp_id'
					) as b
				UNION ALL

			SELECT b.RP_ID
			FROM OPENJSON(@JSON)
			WITH (								
				updated NVARCHAR(MAX) AS JSON
			) AS a
				CROSS APPLY
					OPENJSON(a.updated)
					WITH (					
						RP_ID INT N'$.rp_id'					
					) as b		
		) T1			

		-- ALL Transactions TO BE INSERTED OR UPDATED
		SELECT @StatementCode AS Statementcode, t1.*
		INTO #Transactions_To_Insert
		FROM (
			SELECT b.*
			FROM OPENJSON(@JSON)
				WITH (					
					updated NVARCHAR(MAX) AS JSON
				) AS a
					CROSS APPLY
						OPENJSON(a.updated)
						WITH (		
							retailercode varchar(20) N'$.retailercode',
							datatype varchar(200) N'$.datatype',
							modifiedDate Date N'$.modifieddate'
						) as b

						UNION ALL

				SELECT b.*
				FROM OPENJSON(@JSON)
					WITH (					
						created NVARCHAR(MAX) AS JSON
					) AS a
						CROSS APPLY
							OPENJSON(a.created)
							WITH (													
								retailercode varchar(20) N'$.retailercode',
								datatype varchar(200) N'$.datatype',
								modifiedDate Date N'$.modifieddate'							
							) as b
		) T1

		
		DELETE RP_DT_RetailerDataNotExpectedTrackerAll 
		FROM RP_DT_RetailerDataNotExpectedTrackerAll t1
		INNER JOIN #Transactions_To_Delete t2
			ON T2.StatementCode=T1.StatementCode
		WHERE T1.RP_ID=T2.RP_ID

		SELECT [StatementCode], [RetailerCode], [Season], [DataType], [MonthOfSubmission], [Comments], [DateModified],
		ROW_NUMBER() OVER(PARTITION BY StatementCode ORDER BY RetailerCode) AS RP_ID
		INTO #AllTransactions_To_Insert
		FROM (
			SELECT  [StatementCode], [RetailerCode], [Season], [DataType], [MonthOfSubmission], [Comments], [DateModified]
			FROM RP_DT_RetailerDataNotExpectedTrackerAll
			WHERE StatementCode=@StatementCode
		
			UNION ALL

			SELECT Statementcode 		
				,tx.retailerCode AS RetailerCode
				,YEAR(tx.modifiedDate) AS [Season]
				,tx.datatype AS DataType
				,DATENAME(Month, tx.modifiedDate) AS [MonthOfSubmission]
				,'Use Case' as Comments
				,tx.modifiedDate AS [DateModified]
			FROM #Transactions_To_Insert tx
		) T1	

		DELETE RP_DT_RetailerDataNotExpectedTrackerAll where STATEMENTCODE = @statementCode
		
		INSERT INTO RP_DT_RetailerDataNotExpectedTrackerAll ([StatementCode], [RP_ID], [RetailerCode], [Season], [DataType], [MonthOfSubmission], [Comments], [DateModified])
		SELECT StatementCode, RP_ID,RetailerCode, Season,DataType,MOnthOFSubmission,Comments, DateModified
		FROM #AllTransactions_To_Insert 


	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS Error_Message
		RETURN;	
	END CATCH

FINAL:

	SELECT 'Success' AS [Status], '' AS Error_Message
		
END

SET ANSI_NULLS ON
