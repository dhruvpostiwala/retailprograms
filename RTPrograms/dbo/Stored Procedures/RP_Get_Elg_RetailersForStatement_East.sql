﻿

CREATE PROCEDURE [dbo].[RP_Get_Elg_RetailersForStatement_East] @SEASON INT, @STATEMENT_TYPE AS [VARCHAR](20)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Region VARCHAR(4)='EAST';	
	DECLARE @SEASON_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-01' AS DATE)
	DECLARE @SEASON_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)

	DECLARE @SOLD_TO_START_DATE DATE = CAST(CAST(@SEASON-1 AS VARCHAR(4)) + '-10-02' AS DATE)		
	--DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-10-01' AS DATE)	
	DECLARE @SOLD_TO_END_DATE DATE = CAST(CAST(@SEASON AS VARCHAR(4)) + '-09-30' AS DATE)

	DECLARE @EligibleRetailers AS UDT_RP_RETAILERS_LIST;

	DECLARE @ValidAccountTypes Table ( 
		RetailerCode [Varchar](20) NOT NULL,	
	PRIMARY KEY CLUSTERED (
			[RetailerCode] ASC		
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	)

	/* ==================================== FIRST STEP =========================================================== */
	-- GET RETAILERCODES WITH VALID ACCOUNTTYPES (DEFINED IN CONFIG TABLE)
	INSERT INTO @ValidAccountTypes(RetailerCode)
	SELECT DISTINCT T1.RetailerCode
	FROM RetailerProfileAccountType T1
		INNER JOIN RP_Config_RetailerAccountTypes CFG
		ON CFG.AccountType=T1.AccountType					
	WHERE CFG.Season=@Season AND CFG.Region=@Region

	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code ,ISNULL(RP.SoldRetailerCode,''),RP.[Status]
		,RP.RetailerType AS [StatementLevel]
		,IIF(RP.RetailerType='LEVEL 2','FAMILY','LOCATION') AS [MappingLevel]
	FROM RetailerProfile RP
		INNER JOIN @ValidAccountTypes ACT
		ON ACT.RetailerCode=RP.RetailerCode
	WHERE RP.[Region]=@REGION AND RP.[Status] IN ('ACTIVE','INACTIVE') AND RP.[RetailerName] NOT LIKE '%BASF CANADA%' AND RP.[RetailerName] NOT LIKE '%UNKNOWN RETAIL%' 
		AND RP.[RetailerType] IN ('LEVEL 1','LEVEL 2')

	-- SOLD TO RETAILERS	
	IF @STATEMENT_TYPE = 'ACTUAL'	
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code ,ISNULL(RP.SoldRetailerCode,''),RP.[Status]
		,RP.RetailerType AS [StatementLevel]
		,IIF(RP.RetailerType='LEVEL 2','FAMILY','LOCATION') AS [MappingLevel]
	FROM RetailerProfile RP
		INNER JOIN @ValidAccountTypes ACT
		ON ACT.RetailerCode=RP.RetailerCode
	WHERE RP.[Region]=@REGION AND RP.[Status]='SOLD' AND RP.[RetailerName] NOT LIKE '%BASF CANADA%' AND RP.[RetailerName] NOT LIKE '%UNKNOWN RETAIL%' 
		AND RP.[RetailerType] IN ('LEVEL 1','LEVEL 2')
		AND SoldDate IS NOT NULL
		AND CAST(RP.SoldDate AS Date) >= @SOLD_TO_START_DATE  AND CAST(RP.SoldDate AS Date) <= @SOLD_TO_END_DATE
	   	 

	/* ==================================== NEXT STEP =========================================================== */
	-- IN CASE OF FORECAST AND PROJECTION STATEMENT TYPES, NO NEED TO CREATE STATEMENTS FOR INACTIVE AND SOLD RETAILERS 
	IF @STATEMENT_TYPE IN ('FORECAST','PROJECTION')
		DELETE FROM @EligibleRetailers WHERE [Status] IN ('INACTIVE','SOLD')
	ELSE IF @STATEMENT_TYPE = 'ACTUAL' -- IN CASE OF ACTUAL STATEMENTS TYPE, NO NEED TO CREATE STATEMENTS FOR INACTIVE AND SOLD RETAILERS HAVING NO TRANSACTIONS 
		DELETE ELG
		FROM @EligibleRetailers ELG
			LEFT JOIN (
				SELECT RetailerCode 
				FROM GrowerTransactionsBASFDetails 
				WHERE BASFSeason=@SEASON
				GROUP BY RetailerCode
			) TX
			ON TX.RetailerCode=ELG.RetailerCode
		WHERE ELG.[Status]='INACTIVE' AND TX.RetailerCode IS NULL


	/* ============================== FOR LEVEL 2 RETAILERS, CREATE LEVEL 1 STATEMENT AS WELL ============================== */
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType,  Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status
		,'LEVEL 1' AS [StatementLevel]
		,'LOCATION' AS [MappingLevel]
	FROM @EligibleRetailers
	WHERE [StatementLevel]='LEVEL 2'

	/* ==================================== LEVEL 5 HEAD OFFICE PAYMENT STATEMENTS =========================================================== */
	IF @STATEMENT_TYPE='ACTUAL'
	INSERT INTO @EligibleRetailers(RetailerCode, RetailerType, Level2Code, Level5Code, ActiveRetailerCode, Status, StatementLevel, MappingLevel)
	SELECT RP.RetailerCode, RP.RetailerType, RP.Level2Code, RP.Level5Code
		,ISNULL(RP.SoldRetailerCode,'') AS ActiveRetailerCode
		,RP.[Status]		
		,'LEVEL 5' AS [StatementLevel]
		,'FAMILY' AS [MappingLevel]		
	FROM RetailerProfile RP
		INNER JOIN RP_Config_HOPaymentLevel HO
		ON HO.Level5Code=RP.RETAILERCODE
	WHERE RP.REGION=@REGION AND HO.SEASON=@SEASON AND RP.RetailerType='LEVEL 5'

	DELETE T1
	FROM @EligibleRetailers T1
		INNER JOIN RetailerProfile RP
		ON RP.SoldRetailerCode=T1.Retailercode
	WHERE CAST(RP.SoldDate AS Date) = @SOLD_TO_END_DATE
	
	SELECT * FROM @EligibleRetailers
	
END
