﻿
CREATE PROCEDURE [dbo].[RP2021_Calc_W_InvigorInnovation] @SEASON INT, @STATEMENT_TYPE [VARCHAR](20),  @PROGRAM_CODE [VARCHAR](50), @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @ROUND_PERCENT DECIMAL(6,4) = 0.005;
	DECLARE @Qualifying_Percentage Decimal(6,4) = 0.30;

	DECLARE @TEMP TABLE (
		StatementCode Int,
		Level5Code Varchar(20) NOT NULL,
		RetailerCode Varchar(50) NOT NULL,
		MarketLetterCode Varchar(50) NOT NULL,
		ProgramCode Varchar(50) NOT NULL,
		RewardBrand Varchar(100) NOT NULL,
		InVigor_Bags Decimal NOT NULL,
		Innovation_Bags Decimal NOT NULL,
		RewardBrand_Sales Money NOT NULL,
		Innovation_Sales Money NOT NULL DEFAULT 0,
		InVigor300_Sales Money NOT NULL,
		InVigor200_Sales Money NOT NULL,
		Qualifying_Percentage Decimal(19,4) NOT NULL DEFAULT 0,
		InVigor300_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		InVigor200_Percentage Decimal(6,4) NOT NULL DEFAULT 0,
		InVigor300_Reward Money NOT NULL DEFAULT 0,
		InVigor200_Reward Money NOT NULL DEFAULT 0,
		Reseed_InVigor_Bags Decimal NOT NULL DEFAULT 0,
		Reseed_Innovation_Bags Decimal NOT NULL DEFAULT 0,
		Reward Money NOT NULL DEFAULT 0
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[RetailerCode] ASC,
			[MarketLetterCode]  ASC,
			[ProgramCode] ASC,
			[RewardBrand] ASC
		)
	)

	-- Insert reward data into temporary table variable
	INSERT INTO @TEMP(StatementCode, Level5Code, RetailerCode, MarketLetterCode, ProgramCode, RewardBrand, InVigor_Bags, Innovation_Bags, RewardBrand_Sales, InVigor300_Sales, InVigor200_Sales, Innovation_Sales)
	SELECT ST.StatementCode, RS.Level5Code, Map.ActiveRetailerCode, 'ML2021_W_INV' AS MarketLetterCode, @PROGRAM_CODE AS ProgramCode,
			'INVIGOR' AS RewardBrand,
			SUM(ISNULL(TX.Acres_CY,0) / 10) AS InVigor_Bags,
			SUM(IIF(PRH.Hybrid_Name IN ('L234PC', 'L340PC', 'CHOICE LR344PC', 'L345PC', 'L352C', 'L357P'), ISNULL(TX.Acres_CY,0) / 10, 0)) AS Innovation_Bags,
			SUM(ISNULL(TX.Price_CY,0)) AS RewardBrand_Sales,
			SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][3][0-9]%', ISNULL(TX.Price_CY,0), 0)) AS InVigor300_Sales,
			SUM(IIF(PRH.Hybrid_Name LIKE '%[A-Z][2][0-9]%', ISNULL(TX.Price_CY,0), 0)) AS InVigor200_Sales,
			SUM(IIF(PRH.Hybrid_Name IN ('L234PC', 'L340PC', 'CHOICE LR344PC', 'L345PC', 'L352C', 'L357P'), ISNULL(TX.Price_CY,0) ,0)) AS Innovation_Sales
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS				ON RS.StatementCode=ST.STatementCode
		INNER JOIN RP_StatementsMappings Map	ON ST.StatementCode = Map.StatementCode		

		LEFT JOIN RP2021_DT_Sales_Consolidated TX
		ON ST.StatementCode = TX.StatementCode AND TX.RetailerCode = Map.ActiveRetailerCode	 AND TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'

		LEFT JOIN ProductReference PR
		ON TX.ProductCode = PR.ProductCode

		LEFT JOIN ProductReference_Hybrids PRH
		ON PR.Hybrid_ID = PRH.ID
	--WHERE TX.ProgramCode = @PROGRAM_CODE AND TX.GroupType = 'QUALIFYING_BRANDS'
	GROUP BY ST.StatementCode, RS.Level5Code, Map.ActiveRetailerCode --, TX.MarketLetterCode, TX.ProgramCode, TX.GroupLabel



	-- LETS ADJUST 2021 INVIGOR, INNOVATION BAGS BY DEDUCTING RESEEEDED ACRES
	IF @STATEMENT_TYPE = 'ACTUAL'
	BEGIN
		UPDATE T1
		SET T1.InVigor_Bags = T1.InVigor_Bags - T2.Reseeded_InVigor_Bag
		    ,T1.Innovation_Bags = T1.Innovation_Bags - T2.Reseeded_Innovation_Bag
			,T1.Reseed_InVigor_Bags = T2.Reseeded_InVigor_Bag
			,T1.Reseed_Innovation_Bags = T2.Reseeded_Innovation_Bag
		FROM @TEMP T1
			INNER JOIN (
				SELECT ST.StatementCode, APA.RetailerCode
				       ,SUM(APA.InVigor_ProblemArea/10) AS Reseeded_InVigor_Bag
					   ,SUM(APA.Innovation_ProblemArea/10) AS Reseeded_Innovation_Bag
				FROM @STATEMENTS ST
					INNER JOIN [dbo].[RP_StatementsMappings] MAP
					ON MAP.StatementCode=ST.StatementCode
					INNER JOIN (
						SELECT C.RetailerCode
							   ,SUM(CB.TotalProblemArea) AS InVigor_ProblemArea
							   ,SUM(IIF(CB.ReseedHybridName IN ('InVigor L234PC', 'InVigor L340PC', 'InVigor LR344PC', 'InVigor L345PC', 'InVigor L352C', 'InVigor L357P'), CB.TotalProblemArea ,0)) AS Innovation_ProblemArea
						FROM Claims C
							LEFT JOIN Claims_BasicInfo CB
							ON C.ID = CB.Claim_ID
						WHERE ISNULL(C.SoftDeleted,'') <> 'Yes' AND C.Season = @SEASON AND C.NOI_Code IN ('W_INV_RESEED','E_INV_RESEED') 
						AND C.Status NOT IN ('Draft','Deleted')  AND ISNULL(C.RetailerCode,'') <> '' AND CB.TotalProblemArea > 0 
						GROUP BY C.RetailerCode
					) APA
					ON APA.RetailerCode=MAP.RetailerCode
				GROUP BY ST.Statementcode, APA.RetailerCode								
			) T2
			ON T2.Statementcode=T1.Statementcode AND T2.RetailerCode=T1.RetailerCode
	END


	-- Get the qualifying percentage for each statement
	UPDATE @TEMP		
		SET Qualifying_Percentage = IIF(Innovation_Bags >= 20, Innovation_Bags / InVigor_Bags, 0)
	WHERE InVigor_Bags <> 0

	-- Set the reward percentage for InVigor 300 and InVigor 200
	-- RC/IND, CARGILL, UFA, NUTRIEN
	UPDATE @TEMP
		SET InVigor300_Percentage = 0.04
			,InVigor200_Percentage = 0.02
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage
		AND Level5Code IN ('D000001','D0000107','D0000244','D520062427')
	
	-- FCL
	UPDATE @TEMP
		SET InVigor300_Percentage = 0.038
			,InVigor200_Percentage = 0.02
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage
		AND Level5Code = 'D0000117'	

	-- RICHARDSON
	--https://kennahome.jira.com/browse/RP-4238 
	-- All retails under Richardson will receive 2% and 1% automatically, regardless of their qualification %.
	-- If the qualification % for a retail under Richardson is > 30% (with buffer), then that location will receive an additional 2% and 1% on their Innovation POG, totaling to 4% and 2%.
	UPDATE @TEMP
		SET InVigor300_Percentage = 0.02 
			,InVigor200_Percentage = 0.01 
	WHERE Level5Code = 'D0000137'	

	UPDATE @TEMP
		SET InVigor300_Percentage += 0.02
			,InVigor200_Percentage += 0.01
	WHERE Qualifying_Percentage + @ROUND_PERCENT >= @Qualifying_Percentage
		AND Level5Code = 'D0000137'	


	-- Calculate the InVigor 300 and 200 Series reward for each statement
	UPDATE @TEMP
		SET InVigor300_Reward = InVigor300_Sales * InVigor300_Percentage,
			InVigor200_Reward = InVigor200_Sales * InVigor200_Percentage
	

	-- Calculate the total reward as InVigor 300 reward + InVigor 200 reward
	UPDATE @TEMP
		SET Reward = InVigor300_Reward + InVigor200_Reward

	--Commented for RP-4273. Final reward is adjusted at OU level in [dbo].[RP2021_Calc_Rewards_Summary_West]
	--WHERE InVigor300_Reward > 0 OR InVigor200_Reward > 0

	-- APPLY RADIUS PERCENTAGE TO FINAL REWARD
	IF @STATEMENT_TYPE='FORECAST' OR @STATEMENT_TYPE='PROJECTION'	
	BEGIN
		UPDATE T1
		SET [Reward] = [Reward] * ISNULL(EI.FieldValue/100,1)
		FROM @TEMP T1
			LEFT JOIN RPWeb_User_EI_FieldValues EI
			ON EI.StatementCode=T1.StatementCode AND EI.MarketLetterCode=T1.MarketLetterCode  AND EI.FieldCode='Radius_Percentage_InVigor'
		WHERE [Reward] > 0
	END

	-- Insert values from temporary table into permanent table
	DELETE FROM RP2021_DT_Rewards_W_INV_INNOV WHERE [StatementCode] IN (SELECT StatementCode FROM @STATEMENTS)

	INSERT INTO RP2021_DT_Rewards_W_INV_INNOV(StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode)
	SELECT StatementCode, MarketLetterCode, ProgramCode, RewardBrand, RewardBrand_Sales, InVigor_Bags, Innovation_Bags, InVigor300_Sales, InVigor200_Sales, Qualifying_Percentage, InVigor300_Percentage, InVigor200_Percentage, InVigor300_Reward, InVigor200_Reward, Reward, Innovation_Sales, RetailerCode
	FROM @TEMP
END