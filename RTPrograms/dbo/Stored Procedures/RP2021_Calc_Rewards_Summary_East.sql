﻿


CREATE PROCEDURE [dbo].[RP2021_Calc_Rewards_Summary_East] @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN

	SET NOCOUNT ON

	DELETE T1
	FROM RP_DT_Rewards_Summary T1
		INNER JOIN @STATEMENTS ST
		ON ST.StatementCode=T1.StatementCode

	-- Planning the Business
	INSERT INTO RP_DT_Rewards_Summary(StatementCode,MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_PlanningTheBusiness RS	ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_PLANNING_SUPP_BUSINESS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Portfolio Support
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_PortfolioSupport RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_PORTFOLIO_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Custom Ground Application
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG							ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG								ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_CustomGroundApplication RS		ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_CUSTOM_GROUND_APP_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Loyalty Support Reward
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_PursuitSupport RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_PURSUIT_REWARD'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Supply of Sales Results
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_SupplySales RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_SUPPLY_SALES'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Fungicides Support
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG						ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG							ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_FungicidesSupport RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_FUNGICIDES_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- POG Sales Bonus
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG					ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG						ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_POGSalesBonus RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_POG_SALES_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		UNION

	-- Sales Support - Loyalty Bonus
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG								ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG									ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_SalesSupportLoyaltyBonus RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_SALES_SUP_LOYALTY_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	UNION

	
	-- IGNITE Loyalty Bonus
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG								ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG									ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_Ignite_LoyaltySupport RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_IGNITE_LOYAL_SUPPORT'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

	UNION
	-- SOLLIO GROWTH BONUS
	SELECT ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode, SUM(ISNULL(RS.Reward,0)) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_DT_ML_ELG_Programs ELG								ON ELG.StatementCode=ST.StatementCode 
		INNER JOIN RP_Config_Programs PRG									ON PRG.ProgramCode=ELG.ProgramCode
		LEFT JOIN RP2021_DT_Rewards_E_Sollio_Growth_Bonus RS			ON RS.StatementCode=ST.StatementCode AND RS.ProgramCode=ELG.ProgramCode AND RS.MarketLetterCode=ELG.MarketLetterCode
	WHERE ELG.ProgramCode='RP2021_E_SOLLIO_GROWTH_BONUS'
	GROUP BY ST.StatementCode, ELG.MarketLetterCode, PRG.RewardCode

		
	--Incentives
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2021_E_ADD_PROGRAMS'AS MarketLetterCode, 'Incentives_E' AS RewardCode
		,ISNULL(SUM(RI.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN  RP_DT_Incentives RI							
		ON RI.StatementCode = ST.StatementCode 
	GROUP BY ST.StatementCode
	
		
	--APA Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2021_E_ADD_PROGRAMS'AS MarketLetterCode, APA.RewardCode AS RewardCode
		,ISNULL(SUM(APA.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
		INNER JOIN RP_Seeding_APAPayments APA ON APA.RetailerCode = MAP.RetailerCode AND RS.Season = APA.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'EAST' --AND RS.StatementLevel = 'LEVEL 1'
	GROUP BY ST.StatementCode, APA.RewardCode
	
	--Over/Under Payments
	INSERT INTO RP_DT_Rewards_Summary(StatementCode, MarketLetterCode,RewardCode,RewardValue,RewardEligibility,RewardMessage)
	SELECT ST.StatementCode, 'ML2021_E_ADD_PROGRAMS'AS MarketLetterCode, OUP.RewardCode AS RewardCode
	,ISNULL(SUM(OUP.Amount),0) AS RewardValue, 'Eligible' AS RewardEligibility, '' AS RewardMessage
	FROM @STATEMENTS ST
		INNER JOIN RP_Statements RS ON RS.StatementCode = ST.StatementCode
		INNER JOIN RP_StatementsMappings MAP ON MAP.StatementCode = RS.StatementCode
		INNER JOIN RP_Seeding_Over_Under_Payments OUP ON OUP.RetailerCode = MAP.RetailerCode AND RS.Season = OUP.Season
	WHERE RS.StatementType = 'Actual' AND VersionType = 'Unlocked' AND Region = 'EAST'
	GROUP BY ST.StatementCode, OUP.RewardCode
	

END