﻿


CREATE PROCEDURE [dbo].[zzz_MP_RP2021_Exception_E_Compare_Data] @SEASON INT, @STATEMENTCODE INT
AS
BEGIN
		SET NOCOUNT ON;
		
	
		DECLARE @REGION VARCHAR(4);
		DECLARE @VERSION_TYPE VARCHAR(20);

		SELECT @REGION = UPPER(Region), @VERSION_TYPE = UPPER(VersionType) From RPWeb_Statements Where StatementCode = @STATEMENTCODE
		
		IF @REGION <> 'EAST' OR @VERSION_TYPE <> 'LOCKED' RETURN;
		
		DECLARE @STATEMENTS AS UDT_RP_STATEMENT
		INSERT INTO @STATEMENTS SELECT StatementCode FROM RPWeb_Statements WHERE StatementCode = @STATEMENTCODE OR DataSetCode = @STATEMENTCODE
		
		DECLARE @EDIT_HISTORY AS RP_EDITHISTORY;
		DECLARE @USERNAME VARCHAR(50) = 'Exception Verifier'
		
		DECLARE @TEMP TABLE (
		StatementCode INT NOT NULL,
		ProgramCode varchar(100) NOT NULL,
		RewardCode varchar(50) NOT NULL,
		MarketLetterCode varchar(50) NOT NULL,
		season INT NOT NULL Default 2020,
		current_margin DECIMAL(6,4) NOT NULL DEFAULT 0,
		current_reward MONEY NOT NULL DEFAULT 0,
		exception_type varchar(50) NULL,
		exception_valid varchar(50) NULL
		PRIMARY KEY CLUSTERED 
		(
			[StatementCode] ASC,
			[ProgramCode]  ASC,
			[RewardCode]  ASC,
			[MarketLetterCode] ASC
			)
		)
		
		
		INSERT into @TEMP(StatementCode,ProgramCode,RewardCode,MarketLetterCode)
		SELECT E.StatementCode,E.ProgramCode,R.RewardCode,E.MarketLetterCode
		FROM [RP_Config_Rewards_Summary] R 
				INNER JOIN RP_Config_programs P ON P.RewardCode = R.RewardCode AND R.Season=@SEASON
				INNER JOIN RPWeb_ML_ELG_Programs E ON E.ProgramCode = P.ProgramCode 
				INNER JOIN @STATEMENTS ST ON ST.StatementCode = E.StatementCode
		WHERE R.RewardCode NOT IN('CGAS_E','PRX_HEAD_E')
		
		--SupplySales only using on CCP MarketLetter
		DELETE FROM  @TEMP WHERE MarketLetterCode = 'ML2021_E_INV' AND RewardCode = 'SUPPLY_SALES_E'
		
		UPDATE @TEMP SET exception_type = 'TIER' WHERE RewardCode IN ('SS_LOYALTY_E','PORT_SUP_E','SUPPLY_SALES_E')
	
		UPDATE @TEMP SET exception_type = 'NON-TIER' WHERE exception_type IS NULL
	
		--update TIER VALUES
			UPDATE T1
				SET T1.current_margin = TIER.CurrentMargin,
					T1.current_reward = TIER.CurrentReward
			FROM @TEMP T1
			INNER JOIN 
				(
				SELECT StatementCode, MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode
				from [dbo].[RP2021Web_Rewards_E_PortfolioSupport]
				GROUP BY StatementCode
				UNION ALL
				SELECT StatementCode,MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode
				from [dbo].[RP2021Web_Rewards_E_SalesSupportLoyaltyBonus]
				GROUP BY StatementCode
				UNION ALL
				SELECT StatementCode, MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode)
				FROM [dbo].[RP2021Web_Rewards_E_SupplySales]
				GROUP BY StatementCode				
				) TIER	ON TIER.StatementCode = T1.StatementCode and T1.ProgramCode = TIER.ProgramCode
				WHERE T1.exception_type = 'TIER'
	
		--UDPATE NON-TIER VALUES
		--SIMPLE BUT LETS PUT IN IN TABLE TOO SO EASY FOR DEBUGGING
		--update TIER VALUES
			UPDATE T1
				SET T1.current_margin = NTIER.CurrentMargin,
					T1.current_reward = NTIER.CurrentReward
			FROM @TEMP T1
			INNER JOIN 
				(
				SELECT StatementCode,MAX(Reward_Percentage) AS 'CurrentMargin', SUM(Reward) as 'CurrentReward',MAX(ProgramCode) ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_Sollio_Growth_Bonus]
				GROUP BY StatementCode
				UNION ALL
				SELECT StatementCode,/*Reward_Percentage AS */0 'CurrentMargin', Reward as 'CurrentReward',ProgramCode
				FROM [dbo].[RP2021Web_Rewards_E_Ignite_LoyaltySupport]
				UNION ALL
				SELECT StatementCode, Pursuit_Reward_Amount AS 'CurrentMargin', Reward as 'CurrentReward',ProgramCode
				FROM [dbo].[RP2020Web_Rewards_E_PursuitSupport]
		
				) NTIER	ON NTIER.StatementCode = T1.StatementCode and T1.ProgramCode = NTIER.ProgramCode
				WHERE T1.exception_type = 'NON-TIER'
				

				--Update reward Codes to Exception Ones
				UPDATE @TEMP SET RewardCode = RewardCode + 'X'


				
				UPDATE T1
				SET T1.exception_valid = 'No'
				FROM @TEMP T1
				INNER JOIN RPWeb_Exceptions E ON E.RewardCode = T1.rewardCode AND T1.StatementCode = E.StatementCode
				WHERE E.Status NOT IN ('Rejected','Obselete') AND E.Exception_type <> 'OTHER' and E.Season = @SEASON 
				AND T1.current_margin >= E.Exception_Reward_Margin;  
				
				--omit these
				DELETE FROM @TEMP WHERE exception_valid IS NULL
				

			
	/*
	BEGIN TRY	
			BEGIN TRANSACTION
				
					--NOW COMPARE WITH EXCEPTIONS AND FIND OUT WHAT TO DELETE FROM REWARDS SUMAMRY
					--AND WHICH EXCEPTIONS TO MAKE OBSELETE --Probably delete...will confirm later
				
				
				---review to add functionality for exceptions to roll over on rules when exception is paid already and retailer is now qualifiying
				
				MERGE [dbo].[RPWeb_Rewards_Summary] AS TGT
				USING (
					SELECT StatementCode,MarketLetterCode,RewardCode
					FROM @TEMP 
					WHERE exception_valid = 'No'
				) AS SRC
				ON TGT.StatementCode = SRC.StatementCode AND SRC.MarketLetterCode = TGT.MarketLetterCode AND SRC.RewardCode=TGT.RewardCode    
				WHEN MATCHED THEN  
					--DELETE
					--instead of deleting this should go to negative. needs work
				;

				UPDATE T1
				SET Status = 'Obselete',
				DateModified = GETDATE()
				OUTPUT inserted.[StatementCode], inserted.ID, 'Retailer no longer qualifies for exception','Field','Status',deleted.[Status],inserted.[Status]
				INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
				FROM RPWeb_Exceptions T1
				INNER JOIN @TEMP T ON T.StatementCode = T1.StatementCode AND T1.RewardCode = T.RewardCode AND T.Season = T1.Season
				WHERE T.exception_valid = 'No'

				EXEC RP_GenerateEditHistory 'Exception', @USERNAME,@EDIT_HISTORY

		COMMIT TRANSACTION
				
	END TRY
					
	BEGIN CATCH
		SELECT 'failed' AS Status, ERROR_MESSAGE() + ' at Line ' + CAST(ERROR_LINE() AS VARCHAR)  AS ErrorMessage
		ROLLBACK TRANSACTION
		RETURN;	
	END CATCH
	*/
	SELECT 'success' AS Status, '' AS ErrorMessage, '' as confirmation_message, '' as warning_message


END

