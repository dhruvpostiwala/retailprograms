﻿

CREATE PROCEDURE [dbo].[RP2020_Estimate_W_IncentiveAmounts] @SEASON INT, @STATEMENTS AS UDT_RP_STATEMENT READONLY
AS
BEGIN
	SET NOCOUNT ON;

	/*
		WE WILL BE PASSING UNLOCKED STATEMENT CODES
		SINCE WE DO NOT MAINTAIN STATEMENT MAPPINGS ON LOCKED STATEMENTS
		USE UNLOCKED STATEMENT CODE TO GET INCENTIVE AMOUNTS 	
	*/

	DECLARE @SEASON_STRING VARCHAR(4) = CAST(@SEASON AS VARCHAR(4));
	
	DECLARE @ESTIMATES TABLE(
		Statementcode int NOT NULL,
		Locked_Statementcode INT NOT NULL DEFAULT 0,		
		Amount MONEY NOT NULL DEFAULT 0
	)

	DECLARE @STATMENT_MAPPINGS TABLE (
		Statementcode INT,
		RetailerCode VARCHAR(20)
	)
	
	INSERT INTO @ESTIMATES(Statementcode, Locked_Statementcode)
	SELECT UL.StatementCode, ISNULL(LCK.Statementcode,0) AS Locked_Statementcode
	FROM @STATEMENTS UL
		INNER JOIN RPWeb_Statements LCK
		ON LCK.SRC_Statementcode=UL.Statementcode AND LCK.VersionType='Locked' AND LCK.[Status]='Active' AND LCK.ChequeRunName='OCT RECON'
	
	IF CAST(GETDATE() AS DATE) >  CAST('2020-09-03' AS DATE)  
	BEGIN
		UPDATE T1
		SET Statementcode=T2.Locked_Statementcode		
		FROM RPWeb_Est_Incentives T1
			INNER JOIN @ESTIMATES T2
			ON T2.Statementcode=T1.SRC_StatementCode
		WHERE T1.Statementcode <> T2.Locked_Statementcode
		
		RETURN
	END

	-- DELETE INACTIVE LOCKED STATEMENT ENTRIES
	DELETE T1
	FROM RPWeb_Est_Incentives T1		
		INNER JOIN RPWeb_Statements LCK		ON LCK.StatementCode=T1.StatementCode
		INNER JOIN @STATEMENTS UL			ON UL.StatementCode=LCK.SRC_Statementcode
	WHERE  LCK.[Status] IN ('Delete','Deleted')

	INSERT INTO @STATMENT_MAPPINGS(StatementCode, Retailercode)
	SELECT MAP.StatementCode, MAP.RetailerCode
	FROM RP_StatementsMappings MAP
		INNER JOIN @STATEMENTS ST	
		ON ST.Statementcode=MAP.Statementcode
		
	UPDATE T1
	SET Amount = t2.Amount
	FROM @ESTIMATES T1
		INNER JOIN (
			SELECT MAP.StatementCode, SUM(ISNULL(i.Amount,0)) AS Amount
			FROM @STATMENT_MAPPINGS MAP
				INNER JOIN ( 

					-- For Max Exposure, pull from Resource
					-- For In-Season Estimate = SeasonEstimate
					-- For Total Earned = TotalEarned

					SELECT RetailerCode, ISNULL(SeasonEstimate,0) AS Amount
					FROM Incentive
					WHERE Season=@SEASON AND PaymentMethod='Paid by Retail Statement' AND [Status] NOT IN ('DRAFT','DELETED')
				


				) i 
				ON i.retailercode = map.retailercode 
			GROUP BY MAP.StatementCode
		) T2
		ON T2.Statementcode=T1.Statementcode
			   		
	INSERT INTO RPWeb_Est_Incentives(SRC_Statementcode, Statementcode ,Amount)
	SELECT Statementcode, Locked_Statementcode,ISNULL(Amount,0) Amount
	FROM @ESTIMATES
	WHERE Locked_Statementcode < 0	     	 	

END
