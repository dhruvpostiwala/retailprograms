﻿CREATE PROCEDURE [dbo].[RP_User_UpdateInputValues] @STATEMENTCODE INT, @JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SRW_StatementCode INT;
	DECLARE @STATEMENT_TYPE VARCHAR(20);

	DECLARE @TEMP_EI_VALUES TABLE( 
		StatementCode INT,
		FieldCode Varchar(50),
		ProgramCode varchar(50),
		MarketLetterCode varchar(50),
		FieldValue Numeric(18,2),
		CalcFieldValue varchar(3),
PRIMARY KEY CLUSTERED(
		[StatementCode] ASC,
		[FieldCode] ASC,
		[ProgramCode] ASC,
		[MarketLetterCode] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY])

	/*
statementcode,input_values,field_code,field_value,program_code,ml_codes
JSON Sample:
{
"statementcode":1,
"input_values":[
	{"field_code":"Radius_Percentage","field_value":80,"calc_value":"No","program_code":"RP2020_W_ALL_PROGRAMS","ml_codes":["ML2020_W_INV"]},
	{"field_code":"Radius_Percentage","field_value":85,"calc_value":"No","program_code":"RP2020_W_ALL_PROGRAMS","ml_codes":["ML2020_W_CCP"]},
	{"field_code":"Radius_Percentage","field_value":100,"calc_value":"No","program_code":"RP2020_W_ALL_PROGRAMS","ml_codes":["ML2020_W_ST_INC"]},
	{"field_code":"Radius_Percentage","field_value":100,"calc_value":"No","program_code":"RP2020_W_ALL_PROGRAMS","ml_codes":["ML2020_W_CER_PUL_SB"]},
	{"field_code":"Sales_Target","field_value":1000000,"calc_value":"Yes","program_code":"RP2020_W_INV_PERFORMANCE","ml_codes":["ML2020_W_CCP","ML2020_W_INV"]},
	{"field_code":"Rebate_Qualified","field_value":0,"calc_value":"No","program_code":"RP2020_W_EFF_REBATE","ml_codes":["ML2020_W_CCP","ML2020_W_INV"]},
	{"field_code":"Matched_Centurion","field_value":50,"calc_value":"No","program_code":"RP2020_W_TANK_MIX_BONUS","ml_codes":["ML2020_W_CCP"]},
	{"field_code":"Matched_Facet_L","field_value":60,"calc_value":"No","program_code":"RP2020_W_TANK_MIX_BONUS","ml_codes":["ML2020_W_CCP"]}
  ]
}	
*/

	BEGIN TRY 

		SELECT @STATEMENT_TYPE=StatementType
		FROM RP_Statements
		WHERE StatementCode=@StatementCode

	   	INSERT INTO @TEMP_EI_VALUES(StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)
		SELECT a.StatementCode, b.field_code, b.program_code, ml_codes = c.Value, b.field_value, b.calc_value
		FROM OPENJSON(@JSON)
			WITH (
				statementcode INT N'$.statementcode'
				,input_values NVARCHAR(MAX) AS JSON
			) AS a
			CROSS APPLY
				OPENJSON(a.input_values)
				WITH (
					field_code VARCHAR(50)
					,field_value NUMERIC(18,2)
					,calc_value VARCHAR(3)
					,program_code VARCHAR(50)			
					,ml_codes NVARCHAR(MAX) AS JSON
				) AS b
				CROSS APPLY OPENJSON(b.ml_codes) AS c;

		IF @STATEMENT_TYPE='FORECAST'		
		BEGIN
			/* WE ALSO NEED TO MAINTAIN EDIT INPUT VALUES FOR SRW STATEMENTS */
			SELECT @SRW_StatementCode=RS.StatementCode
			FROM RP_Statements RS
				INNER JOIN (
					SELECT Season, RetailerCode, StatementLevel, StatementType
					FROM RP_Statements 
					WHERE StatementCode=@STATEMENTCODE
				) RS2
				ON RS2.StatementType=RS.StatementType AND RS2.RetailerCode=RS.RetailerCode	AND RS2.Season=RS.Season AND RS2.StatementLevel=RS.StatementLevel 
			WHERE RS.Status='Active' AND RS.VersionType='FINAL-PREFERRED'


			INSERT INTO @TEMP_EI_VALUES(StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)
			SELECT @SRW_StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue
			FROM @TEMP_EI_VALUES
		END		

		-- FOR EAST FAMILY, EDIT INPUT VALUES ARE UPDATED ON FORECAST SUMMARY PAGE, IS TAKEN CARE IN FOLLOWING TWO STEPS
		-- FOR EAST #1: PRECAUTION: JUST IN CASE, IF USER WAS ABLE TO EDIT INPUT VALUES FROM EAST LEVEL 1 STATEMENTS BELONGING TO A FAMILY. WE SHOULD IGNORE THOSE ENTRIES
		DELETE FROM @TEMP_EI_VALUES
		WHERE STATEMENTCODE IN (SELECT STATEMENTCODE FROM RP_Statements WHERE Region='EAST' AND StatementType IN ('FORECAST','PROJECTION') AND  StatementLevel='LEVEL 1' AND DataSetCode > 0)


		-- FOR EAST #2: HENCE WE NEED TO INSERT SAME VALUES FOR ALL STATEMENTS IN FAMILY WHERE STATEMENTLEVEL='LEVEL 1'
		INSERT INTO @TEMP_EI_VALUES(StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)
		SELECT RS.StatementCode, T1.FieldCode, T1.ProgramCode, T1.MarketLetterCode, T1.FieldValue, T1.CalcFieldValue
		FROM @TEMP_EI_VALUES T1
			INNER JOIN RP_Statements RS
			ON RS.DataSetCode=T1.StatementCode 
		WHERE Region='EAST' AND RS.StatementType IN ('FORECAST','PROJECTION') AND RS.StatementLevel='Level 1' 

		MERGE RPWeb_User_EI_FieldValues TGT
		USING (
			SELECT StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue FROM @TEMP_EI_VALUES
		) SRC
		ON SRC.StatementCode=TGT.StatementCode AND SRC.FieldCode=TGT.FieldCode AND SRC.ProgramCode=TGT.ProgramCode AND SRC.MarketLetterCode=TGT.MarketLetterCode
		WHEN MATCHED AND (SRC.FieldValue <> TGT.FieldValue OR SRC.CalcFieldValue <> TGT.CalcFieldValue) THEN 
			UPDATE SET TGT.FieldValue=SRC.FieldValue, TGT.CalcFieldValue=SRC.CalcFieldValue
		WHEN NOT MATCHED THEN
			INSERT ( StatementCode, FieldCode, ProgramCode, MarketLetterCode, FieldValue, CalcFieldValue)  
			VALUES ( SRC.StatementCode, SRC.FieldCode, SRC.ProgramCode, SRC.MarketLetterCode, SRC.FieldValue, SRC.CalcFieldValue);

		-- NOW RECALCULATE REWARDS 	
		--IF @STATEMENT_TYPE  = 'REWARD PLANNER'
		--	EXEC [rew_planner].[RefreshRewards] @STATEMENTCODE
		--ELSE IF @STATEMENT_TYPE IN ('Forecast','Projection')
		IF @STATEMENT_TYPE IN ('Forecast','Projection')
			EXEC RP_User_RecalculateRewards @STATEMENTCODE

		-- NO NEED TO CALCULATE REWARDS ON SRW STATEMENTS. AS REWARD CALCULATIONS OCCUR UP ON ACCESSING THE REWARDS PAGE IN SRW
		-- EXEC RP_User_RecalculateRewards @SRW_StatementCode

	END TRY
					
	BEGIN CATCH
		SELECT 'Failed' AS [Status], ERROR_MESSAGE() AS Error_Message
		RETURN;	
	END CATCH

	SELECT 'Success' AS [Status], '' AS Error_Message

END
