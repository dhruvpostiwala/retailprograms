﻿
CREATE VIEW [rp].[CARMappingView]
AS
SELECT CASE
           WHEN ag.AGCARID IS NOT NULL THEN
               ag.AGCARID
           WHEN ex.CARID IS NOT NULL THEN
               ex.CARID
           ELSE
               map.CARID
       END CARID
     , CASE
           WHEN ex.CARID IS NOT NULL THEN
               ex.RetailerCode
           ELSE
               map.RetailerCode
       END RetailerCode
     , ex.ExceptionApprovedBy
     , ex.ExceptionDateApproved
FROM rp.CARMapping                   map
    LEFT JOIN rp.CARMappingException ex
        ON ex.RetailerCode = map.RetailerCode
           AND ex.IsActive = 1
    LEFT JOIN rp.AGCARMapping        ag
        ON ag.CARID = (CASE
                           WHEN ex.CARID IS NOT NULL THEN
                               ex.CARID
                           ELSE
                               map.CARID
                       END);
