﻿CREATE TABLE [rp].[CropAcByCAR] (
    [CropAcByCARID] INT        IDENTITY (1, 1) NOT NULL,
    [CARID]         INT        NOT NULL,
    [Province]      CHAR (2)   NOT NULL,
    [CropGroupID]   INT        NOT NULL,
    [2017Acres]     FLOAT (53) NULL,
    [2018Acres]     FLOAT (53) NULL,
    [2019Acres]     FLOAT (53) NULL,
    [2020Acres]     FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([CropAcByCARID] ASC)
);

