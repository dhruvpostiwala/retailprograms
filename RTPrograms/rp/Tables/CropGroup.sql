﻿CREATE TABLE [rp].[CropGroup] (
    [CropGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [CropGroup]   VARCHAR (50)  NULL,
    [Active]      BIT           DEFAULT ((1)) NULL,
    [CreatedBy]   VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate] DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([CropGroupID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CropGroup]
    ON [rp].[CropGroup]([CropGroup] ASC);

