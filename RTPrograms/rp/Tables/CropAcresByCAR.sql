﻿CREATE TABLE [rp].[CropAcresByCAR] (
    [CropAcresByCARID] INT           IDENTITY (1, 1) NOT NULL,
    [CARID]            INT           NOT NULL,
    [Province]         CHAR (2)      NOT NULL,
    [CropGroupID]      INT           NOT NULL,
    [Season]           INT           NOT NULL,
    [Acres]            FLOAT (53)    NULL,
    [CreatedBy]        VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]      DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       VARCHAR (100) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [IsUsed]           BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([CropAcresByCARID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Season]
    ON [rp].[CropAcresByCAR]([Season] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CropGroupID]
    ON [rp].[CropAcresByCAR]([CropGroupID] ASC);

