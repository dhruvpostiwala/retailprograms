﻿CREATE TABLE [rp].[AGCARMapping] (
    [CARID]       INT           NOT NULL,
    [AGCARID]     INT           NOT NULL,
    [CreatedBy]   VARCHAR (100) DEFAULT (suser_sname()) NULL,
    [CreatedDate] DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([CARID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AGCARID]
    ON [rp].[AGCARMapping]([AGCARID] ASC);

