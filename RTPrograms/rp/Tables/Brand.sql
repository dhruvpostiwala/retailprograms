﻿CREATE TABLE [rp].[Brand] (
    [BrandID]     INT           IDENTITY (1, 1) NOT NULL,
    [Brand]       VARCHAR (50)  NOT NULL,
    [CropGroupID] INT           NOT NULL,
    [CreatedDate] DATETIME      DEFAULT (getdate()) NOT NULL,
    [CreatedUSer] VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    PRIMARY KEY CLUSTERED ([BrandID] ASC)
);

