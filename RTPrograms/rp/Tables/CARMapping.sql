﻿CREATE TABLE [rp].[CARMapping] (
    [CARMappingID] INT          IDENTITY (1, 1) NOT NULL,
    [CARID]        INT          NOT NULL,
    [RetailerCode] VARCHAR (20) NOT NULL,
    [IsActive]     BIT          CONSTRAINT [DF__CARMappin__IsAct__3243098F] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__CARMappi__0B70E17F76464097] PRIMARY KEY CLUSTERED ([CARMappingID] ASC),
    CONSTRAINT [UQ_RetailerCode] UNIQUE NONCLUSTERED ([RetailerCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_RetailerCode]
    ON [rp].[CARMapping]([RetailerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CARID]
    ON [rp].[CARMapping]([CARID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CARID_RetailerCode]
    ON [rp].[CARMapping]([CARID] ASC, [RetailerCode] ASC);

