﻿CREATE TABLE [rp].[Province] (
    [ProvinceID]   INT           IDENTITY (1, 1) NOT NULL,
    [Province]     VARCHAR (2)   NULL,
    [ProvinceName] VARCHAR (100) NULL,
    [Active]       BIT           DEFAULT ((1)) NULL,
    [CreatedBy]    VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]  DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([ProvinceID] ASC),
    CONSTRAINT [UQ_Province] UNIQUE NONCLUSTERED ([Province] ASC),
    CONSTRAINT [UQ_ProvinceName] UNIQUE NONCLUSTERED ([ProvinceName] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProvinceName]
    ON [rp].[Province]([ProvinceName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Province]
    ON [rp].[Province]([Province] ASC);

