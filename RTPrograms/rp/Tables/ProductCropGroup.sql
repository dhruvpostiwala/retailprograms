﻿CREATE TABLE [rp].[ProductCropGroup] (
    [ProductCropGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [ProductCode]        VARCHAR (65)  NOT NULL,
    [CropGroupID]        INT           NOT NULL,
    [Season]             INT           NULL,
    [CreatedDate]        DATETIME      DEFAULT (getdate()) NOT NULL,
    [CreatedUSer]        VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate]       DATETIME      NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ProductCropGroupID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CropGroupID]
    ON [rp].[ProductCropGroup]([CropGroupID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductCode]
    ON [rp].[ProductCropGroup]([ProductCode] ASC);

