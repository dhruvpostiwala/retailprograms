﻿CREATE TABLE [rp].[CARMappingException] (
    [CARMappingExceptionID] INT           IDENTITY (1, 1) NOT NULL,
    [CARID]                 INT           NOT NULL,
    [RetailerCode]          VARCHAR (20)  NULL,
    [ExceptionApprovedBy]   VARCHAR (100) NULL,
    [ExceptionDateApproved] DATETIME      NULL,
    [IsActive]              BIT           NULL,
    PRIMARY KEY CLUSTERED ([CARMappingExceptionID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_RetailerCode]
    ON [rp].[CARMappingException]([RetailerCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CARID]
    ON [rp].[CARMappingException]([CARID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CARID_RetailerCode]
    ON [rp].[CARMappingException]([CARID] ASC, [RetailerCode] ASC);

