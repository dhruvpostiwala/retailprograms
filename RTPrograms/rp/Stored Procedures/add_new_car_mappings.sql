﻿
CREATE   PROCEDURE [rp].[add_new_car_mappings]
AS
BEGIN

	insert into rp.carmapping (carid,retailercode,isactive)
	select	car.CARUID,
			rp.retailercode,
			1
	from	dbo.retailerprofile rp
	left join rp.carmapping m
		on	m.retailercode=rp.retailercode
	left join dbo.postalcodereference pcr
		on	pcr.postalcode=iif(rp.physicalpostalcode='',rp.mailingpostalcode,rp.physicalpostalcode)
	join (
		select	case car.pruid
					when 10 then 'NL'
					when 11 then 'PE'
					when 12 then 'NS'
					when 13 then 'NB'
					when 24 then 'QC'
					when 35 then 'ON'
					when 46 then 'MB'
					when 47 then 'SK'
					when 48 then 'AB'
					when 59 then 'BC'
					when 60 then 'YK'
					when 61 then 'NT'
					when 62 then 'NU'
				end province, caruid, geom
		from	dbo.STATSCAN2011_spatial_CAR car 
	) car
		on	car.province=iif(rp.physicalpostalcode='' or rp.physicalprovince='',rp.mailingprovince,rp.physicalprovince)
		and	car.geom.STIntersects(geography::Point(coalesce(rp.latitude,pcr.latitude), coalesce(rp.longitude,pcr.longitude), 4326) ) = 1
	where	rp.[status]='active'
		and	m.retailercode is null
		and rp.retailertype in ('level 1','level 2')
		and coalesce(rp.latitude,pcr.latitude) is not null
		and coalesce(rp.longitude,pcr.longitude) is not null
		and isnull(car.CARUID,'')!=''
	
END

