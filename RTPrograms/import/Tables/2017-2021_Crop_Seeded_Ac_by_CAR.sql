﻿CREATE TABLE [import].[2017-2021_Crop_Seeded_Ac_by_CAR] (
    [CAR]        FLOAT (53)     NULL,
    [Province]   NVARCHAR (255) NULL,
    [Crop Group] NVARCHAR (255) NULL,
    [Crop]       NVARCHAR (255) NULL,
    [2017]       FLOAT (53)     NULL,
    [2018]       FLOAT (53)     NULL,
    [2019]       FLOAT (53)     NULL,
    [2020]       FLOAT (53)     NULL,
    [2021]       FLOAT (53)     NULL
);

