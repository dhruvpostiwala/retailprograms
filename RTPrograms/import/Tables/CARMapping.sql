﻿CREATE TABLE [import].[CARMapping] (
    [DistributorCode] NVARCHAR (255) NULL,
    [DistributorName] NVARCHAR (255) NULL,
    [RetailerCode]    NVARCHAR (255) NULL,
    [RetailerName]    NVARCHAR (255) NULL,
    [RetailerType]    NVARCHAR (255) NULL,
    [LocationName]    NVARCHAR (255) NULL,
    [Territory]       NVARCHAR (255) NULL,
    [MailingAddress1] NVARCHAR (255) NULL,
    [MailingAddress2] NVARCHAR (255) NULL,
    [MailingCity]     NVARCHAR (255) NULL,
    [MailingProvince] NVARCHAR (255) NULL,
    [Status]          NVARCHAR (255) NULL,
    [ID]              FLOAT (53)     NULL,
    CONSTRAINT [UQ_RetailerCode] UNIQUE NONCLUSTERED ([RetailerCode] ASC)
);

