﻿CREATE TABLE [import].[RemoveFromProductCropGroup] (
    [ProductCropGroupID] FLOAT (53)     NULL,
    [ProductCode]        NVARCHAR (255) NULL,
    [CropGroupID]        FLOAT (53)     NULL,
    [Season]             FLOAT (53)     NULL
);

