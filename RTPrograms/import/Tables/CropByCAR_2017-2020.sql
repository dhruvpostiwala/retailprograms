﻿CREATE TABLE [import].[CropByCAR_2017-2020] (
    [CAR]        NVARCHAR (255) NULL,
    [Province]   NVARCHAR (255) NULL,
    [Crop Group] NVARCHAR (255) NULL,
    [Crop]       NVARCHAR (255) NULL,
    [2017]       NVARCHAR (255) NULL,
    [2018]       NVARCHAR (255) NULL,
    [2019]       NVARCHAR (255) NULL,
    [2020]       NVARCHAR (255) NULL
);

