﻿
CREATE PROCEDURE [access].[GetUser_Privileges_ModuleLevel] (
	@USER_NAME VARCHAR(100), 	
	@ROLE_PRIVILEGES_JSON VARCHAR(1050)= '{}' OUTPUT,
	@EXECUTION_STATUS VARCHAR(20)='' OUTPUT,	
	@ERROR_MESSAGE VARCHAR(2048)='' OUTPUT	
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @USER_ROLE  VARCHAR(50)='UNKNOWN'
	DECLARE @ID INT, @USER_EXISTS BIT=0		
	DECLARE @IS_ADMIN BIT=0 , @IS_EAST BIT=0, @IS_WEST BIT=0
	DECLARE @IS_KENNA_ADMIN BIT=0
	DECLARE @IS_BASF_ADMIN BIT=0

	DECLARE @REGION VARCHAR(4)=''
	DECLARE @PRIVILEGES_JSON NVARCHAR(MAX)= '';

			DECLARE @PRIVILEGES  TABLE (
			permission varchar(50), 
			level varchar(50), 
			access int
		);

		DECLARE @USER_REGIONS TABLE (
			[Permission] VARCHAR(50),
			[level]	VARCHAR(50),
			[access] INT
		)

		DECLARE @USER_ROLES  TABLE (
			[Role] varchar(50), 
			[Label] varchar(100)
		);
	
		DECLARE @ROLE_RANKINGS TABLE(
			[Rank] [int] IDENTITY(1,1),
			[Role] varchar(50)	
		)		
	   
	BEGIN TRY
		--SET @CAN_APPROVE= 'X'  -- to force throw the error			
		SELECT @ID = ID FROM [USER].[ACCOUNTS] WHERE USERNAME = @USER_NAME
				
		IF @ID IS NULL				
			THROW 51000, 'User does not exist.', 1;  		
		ELSE		
			SET @USER_EXISTS = 1
	
		INSERT INTO @USER_ROLES(Role,Label)
		EXEC [access].[getPermissionRoles] @ID,'AGC_RETAILER_PROGRAMS'

		INSERT INTO @USER_REGIONS(Permission,Level,Access)
		EXEC [access].[getPermissionLevels] @ID,'EAST'

		INSERT INTO @USER_REGIONS(Permission,Level,Access)
		EXEC [access].[getPermissionLevels] @ID,'WEST'

		IF EXISTS(SELECT TOP 1 1 FROM @USER_REGIONS WHERE Permission='EAST')
			SET @IS_EAST=1

		IF EXISTS(SELECT TOP 1 1 FROM @USER_REGIONS WHERE Permission='WEST')
			SET @IS_WEST=1


		IF @IS_WEST = 1 AND @IS_EAST = 1
			SET @REGION='Both'
		ELSE IF @IS_WEST = 1
			SET @REGION = 'West'
		ELSE IF @IS_EAST = 1
			SET @REGION = 'East'



		-- INSERT ROLES IN TOP TO LOW RANKING ORDDER 
		INSERT INTO @ROLE_RANKINGS([Role])
		VALUES('AGS') -- call Center
		,('Kenna') 	
		,('RET')
		,('GWR')
		,('SA') -- Sales Associate
		,('TS')
		,('TD')
		,('BR') -- Retailer Rep
		,('IS') -- Grower Rep
		,('HORT') -- Grower (HORT) Rep
		,('RM')
		,('HORT RSM')	
		,('RC')
		,('RC-TL')
		,('SAM')
		,('NAM')
		,('HO')
		,('NSM')
		,('NSAM')

	
		IF NOT EXISTS(SELECT COUNT(*) FROM @USER_ROLES)
			SET @USER_ROLE = 'UNKNOWN'			
		ELSE
			BEGIN		
				SELECT TOP 1 @USER_ROLE = [role]
				FROM (
					SELECT ISNULL(UR.[Role],'UNKNOWN') as [role], ISNULL(RR.[rank],9999) as [rank]
					FROM @USER_ROLES UR
						LEFT JOIN @ROLE_RANKINGS RR
						ON RR.[Role]=UR.[Role]
				) DATA
				ORDER BY [Rank]
			END

		-- PRIVILEGES
		INSERT INTO @PRIVILEGES(permission, [level], access)
		EXEC [access].[getPermissionLevels] @ID, 'AGC_RETAILER_PROGRAMS';
		
		/*
		IF EXISTS(SELECT TOP 1 1 FROM @PRIVILEGES WHERE [Level]='ADMIN')
			SET @IS_ADMIN = 1
					
		IF @USER_ROLE='KENNA' AND @IS_ADMIN = 1
			SET @IS_KENNA_ADMIN=1
		ELSE IF @USER_ROLE='HO'  AND @IS_ADMIN = 1
			SET @IS_BASF_ADMIN=1
		*/
		
		-- LETS BUILD PRIVELEGES JSON
		SELECT @PRIVILEGES_JSON =  '{"' +  STRING_AGG(LOWER([level]) +'":' + IIF(access=1,'true','false') ,',"')  + '}' 
		FROM @PRIVILEGES
	
		SET @PRIVILEGES_JSON =  ISNULL(@PRIVILEGES_JSON,'{}') 		

		SET @ROLE_PRIVILEGES_JSON = (
			SELECT @USER_ROLE [role]
				,@REGION [region]
	--			,@IS_KENNA_ADMIN as 'is_kenna_admin'
	--			,@IS_BASF_ADMIN as 'is_basf_admin'
				,JSON_QUERY(@PRIVILEGES_JSON) [privileges]
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER 
		) 

		SET @EXECUTION_STATUS='success'
	END TRY

	BEGIN CATCH
		SET @EXECUTION_STATUS='error';		
		THROW		
	END CATCH
		
END



