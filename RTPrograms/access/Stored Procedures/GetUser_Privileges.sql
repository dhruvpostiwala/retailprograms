﻿
CREATE PROCEDURE [access].[GetUser_Privileges] (
	@USER_NAME VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @LANGUAGE VARCHAR(2)='EN';
	
	
	DECLARE @USER_ROLE VARCHAR(50)='';
	DECLARE @PRIVILEGES_JSON NVARCHAR(MAX)= '';
	DECLARE @IS_KENNA_ADMIN BIT=0
	DECLARE @IS_BASF_ADMIN BIT=0
	DECLARE @CAN_READ BIT=0
	DECLARE @CAN_EDIT BIT=0
	DECLARE @CAN_APPROVE BIT=0


	
	DECLARE @ID INT = (SELECT ID FROM [USER].[ACCOUNTS] WHERE USERNAME = @USER_NAME);
	DECLARE @PRIVILEGES  TABLE (permission varchar(50), level varchar(50), access int);
	DECLARE @USER_ROLES  TABLE (role varchar(50), label varchar(100));
	DECLARE @ROLE_RANKINGS TABLE(
		[Rank] [int] IDENTITY(1,1),
		[Role] varchar(50)	
	)
	
	IF @ID IS NULL
		RETURN

	
	INSERT INTO @USER_ROLES(Role,Label)
	EXEC [access].[getPermissionRoles] @ID,'AGC_RETAILER_PROGRAMS'

	-- INSERT ROLES IN TOP TO LOW RANKING ORDDER 
	INSERT INTO @ROLE_RANKINGS([Role])
	VALUES('AGS') -- call Center
	,('Kenna') 
	,('API') -- AgrPotection Intl Inc
	,('RET')
	,('GWR')
	,('SA') -- Sales Associate
	,('TS')
	,('TD')
	,('BR') -- Retailer Rep
	,('IS') -- Grower Rep
	,('HORT') -- Grower (HORT) Rep
	,('RM')
	,('HORT RSM')	
	,('HO')
	,('NSM')
	,('NSAM')
	,('RET')
	
	IF NOT EXISTS(SELECT COUNT(*) FROM @USER_ROLES)
		BEGIN
			SET @USER_ROLE = 'UNKNOWN'
		END
	ELSE
		BEGIN		
			SELECT TOP 1 @USER_ROLE = [role]
			FROM (
				SELECT ISNULL(UR.[Role],'UNKNOWN') as [role], ISNULL(RR.[rank],9999) as [rank]
				FROM @USER_ROLES UR
					LEFT JOIN @ROLE_RANKINGS RR
					ON RR.[Role]=UR.[Role]
			) DATA
			ORDER BY [Rank]
		END

	-- PRIVILEGES
	INSERT INTO @PRIVILEGES(permission, [level], access)
	EXEC [access].[getPermissionLevels] @ID, 'AGC_RETAILER_PROGRAMS';


	IF @USER_ROLE='Kenna' AND EXISTS(SELECT * FROM @PRIVILEGES WHERE [Level]='ADMIN')
		SET @IS_KENNA_ADMIN=1
	ELSE IF @USER_ROLE='HO' AND EXISTS(SELECT * FROM @PRIVILEGES WHERE [Level]='ADMIN')
		SET @IS_BASF_ADMIN=1
		

	SET @PRIVILEGES_JSON = (SELECT * FROM
		(SELECT @USER_NAME as 'user_name', @USER_ROLE AS 'user_role',@IS_KENNA_ADMIN as 'is_kenna_admin', @IS_BASF_ADMIN as 'is_basf_admin'
	)user_data  FOR JSON PATH,WITHOUT_ARRAY_WRAPPER 
	)

	SELECT 'success' as status, @PRIVILEGES_JSON AS 'data'
	
END



