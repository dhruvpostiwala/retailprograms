﻿
CREATE PROCEDURE [Cheques].[Agent_PostDocumentUpdate] @SEASON INT,  @USERNAME VARCHAR(100), 	@SRC_CHEQUES UDT_RP_CHEQUES_INFO READONLY
AS
BEGIN

SET NOCOUNT ON; 
BEGIN TRANSACTION;  

BEGIN TRY	
BEGIN
	
	/****DEMAREY ---TO DO 
	UPDATES ARE NEEDED FOR STATUSES RELATED TO PAYMENT AND SETTING ARCHIVE STATUS. 
	WE NEED TO ENSURE PDF IS PRINTED AND VALIDATED BEFORE MOVING INTO NEXT PHASE. ALSO ALLOW TIME FOR PDF TO GENERATED AND VALIDATED BEFORE WE CHANGE STATUS TO ARCHIVE SO THAT THAT STATEMENT CAN GET ITS PDF
		
	*/	
		
	
	DECLARE @CHEQUES AS UDT_RP_CHEQUES_INFO
	DECLARE @TARGET_STATEMENTS AS UDT_RP_STATEMENT
	DECLARE @EDIT_HISTORY AS RP_EDITHISTORY
	
	DECLARE @LOCKED_STATEMENTS TABLE(
		StatementCode INT NOT NULL,
		DataSetCode INT NOT NULL,
		SRC_StatementCode INT NOT NULL,
		Summary VARCHAR(3) NOT NULL		
	)

	DECLARE @CHEQUE_UPDATES AS TABLE(
		[ChequeDocCode] [varchar](25) NOT NULL,
		[NEW_ChequeNumber] [varchar](10) NULL,
		[NEW_ChequeDate] [datetime] NULL,
		[NEW_ChequeStatus] [varchar](50) NOT NULL,
		[NEW_PayableToName] [varchar](150)  NULL,
		[NEW_SendTo] [varchar](75) NULL,
		[NEW_AmtPaid] [Money] NOT NULL,
		[OLD_ChequeNumber] [varchar](10) NULL,
		[OLD_ChequeDate] [datetime] NULL,
		[OLD_ChequeStatus] [varchar](50) NULL,
		[OLD_PayableToName] [varchar](150) NULL,
		[OLD_SendTo] [varchar](75) NULL,
		[OLD_AmtPaid] [Money] NULL
	)

	DECLARE @RUN_REFRESH_PAYMENTINFO BIT=0


	INSERT INTO @CHEQUES([ChequeDocCode],[StatementCode],[RetailerCode],[VersionType],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeAmount],[SendTo],[PayableToName],[ChequeStatus],[AmtRequested],[AmtReceived],[AmtPaid],[Unlocked],[Voided])
	SELECT [ChequeDocCode],[StatementCode],[RetailerCode],[VersionType],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeAmount],[SendTo],[PayableToName],[ChequeStatus],[AmtRequested],[AmtReceived],[AmtPaid],[Unlocked],[Voided]
	FROM @SRC_CHEQUES
	WHERE [VersionType]='Current'

	MERGE RPWeb_Cheques dest
	USING(
		SELECT [ChequeDocCode],[StatementCode],[RetailerCode],[VersionType],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeAmount],[SendTo],[PayableToName],[ChequeStatus],[AmtRequested],[AmtReceived],[AmtPaid]
		FROM @CHEQUES
		WHERE [VersionType]='Current'
	) src 
	ON src.[ChequeDocCode]=dest.[ChequeDocCode] AND dest.[VersionType]='Current'
	WHEN MATCHED THEN
		UPDATE SET [Statementcode]=src.[StatementCode]
			,[ChequeType]=src.[ChequeType]		
			,[ChequeNumber]=src.[ChequeNumber]
			,[ChequeDate]=src.[ChequeDate]		
			,[SendTo]=src.[SendTo]
			,[PayableToName]=src.[PayableToName]
			,[ChequeStatus]=src.[ChequeStatus]
			,[AmtRequested]=src.[AmtRequested]
			,[AmtReceived]=src.[AmtReceived]
			,[AmtPaid]=src.[AmtPaid]
	WHEN NOT MATCHED THEN
		INSERT([InsertedDate],[ChequeDocCode],[StatementCode],[RetailerCode],[VersionType],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeAmount],[SendTo],[PayableToName],[ChequeStatus],[AmtRequested],[AmtReceived],[AmtPaid])
		VALUES(GetDate(),[ChequeDocCode],[StatementCode],[RetailerCode],[VersionType],[ChequeType],[ChequeNumber],[ChequeDate],[ChequeAmount],[SendTo],[PayableToName],[ChequeStatus],[AmtRequested],[AmtReceived],[AmtPaid])
	OUTPUT inserted.[ChequeDocCode],inserted.[ChequeNumber],inserted.[ChequeDate],inserted.[ChequeStatus],inserted.[PayableToName],inserted.[SendTo],inserted.[AmtPaid],deleted.[ChequeNumber],deleted.[ChequeDate],deleted.[ChequeStatus],deleted.[PayableToName],deleted.[SendTo],deleted.[AmtPaid]
	INTO @CHEQUE_UPDATES([ChequeDocCode],[NEW_ChequeNumber],[NEW_ChequeDate],[NEW_ChequeStatus],[NEW_PayableToName],[NEW_SendTo],[NEW_AmtPaid],[OLD_ChequeNumber],[OLD_ChequeDate],[OLD_ChequeStatus],[OLD_PayableToName],[OLD_SendTo],[OLD_AmtPaid]);

	DELETE FROM RPWeb_Cheques WHERE [ChequeType]='Adjustment' 
	DELETE FROM @CHEQUES WHERE [ChequeType]='Adjustment'
	

	UPDATE RPWeb_Cheques 
	SET [PDFJobID]=NULL, [PDFDocCode]=NULL
	WHERE [VersionType]='Current' AND [ChequeStatus]='New'
		
	/************************************************************************/
	-- LETS MAINTAIN PAYMENT_STATUS VALUE ON ARCHIVE STATEMENTS
	-- PAYMENT_STATUS VALUE SHOULD BE STATUS VALUE OF CHEQUE / EFT

	DECLARE @TEMP TABLE(
		Statementcode INT,
		ChequeStatus [Varchar](50)	
	)
	
	
	INSERT INTO @TEMP(StatementCode, ChequeStatus)
	SELECT Statementcode, ChequeStatus
	FROM @Cheques 
	--WHERE [ChequeType] <> 'Adjustment'
	
	INSERT INTO @TEMP(StatementCode, ChequeStatus) 		
	SELECT S.StatementCode, T.ChequeStatus
	FROM RPWeb_Statements  S
		INNER JOIN @TEMP T		ON S.DataSetCode=T.StatementCode
	WHERE S.[Status]='Active' AND S.[VersionType]='Archive' 
	
		UNION
	
	SELECT S.StatementCode, T.ChequeStatus
	FROM RPWeb_Statements  S
		INNER JOIN @TEMP T		ON S.DataSetCode_L5=T.StatementCode
	WHERE S.[Status]='Active' AND S.[VersionType]='Archive' 
	   
	MERGE RPWeb_StatementInformation TGT
	USING (		
		SELECT DISTINCT Statementcode, ChequeStatus 
		FROM @TEMP T1
		WHERE T1.ChequeStatus NOT IN ('Deleted','Void','Void EFT') 
	) SRC
	ON SRC.StatementCode=TGT.Statementcode AND TGT.FieldName='Payment_Status'
	WHEN MATCHED AND TGT.FieldValue <> SRC.ChequeStatus THEN
		UPDATE SET TGT.FieldValue=SRC.ChequeStatus
	WHEN NOT MATCHED THEN
		INSERT (StatementCode, FieldName, FieldValue)
		VALUES(SRC.StatementCode, 'Payment_Status', SRC.ChequeStatus);


	/************************************************************************/
	-- READY FOR PDF CREATION
	DELETE @TARGET_STATEMENTS
	INSERT INTO @TARGET_STATEMENTS(StatementCode)
	SELECT Statementcode FROM @Cheques	WHERE [ChequeStatus] IN ('Ready to Print','Submitted To Bank')
	
	IF EXISTS(SELECT * FROM @TARGET_STATEMENTS)
	BEGIN
		INSERT INTO @TARGET_STATEMENTS(StatementCode)
		SELECT StatementCode FROM RPWeb_Statements WHERE [Status]='Active' AND [VersionType]='Archive' AND [DataSetCode] IN (SELECT Statementcode FROM @TARGET_STATEMENTS)
			UNION
		SELECT StatementCode FROM RPWeb_Statements WHERE [Status]='Active' AND [VersionType]='Archive' AND [DataSetCode_L5] IN (SELECT Statementcode FROM @TARGET_STATEMENTS)
		
	--	DELETE @EDIT_HISTORY

		DELETE STI
		FROM RPWeb_StatementInformation  STI
			INNER JOIN @TARGET_STATEMENTS TGT
			ON TGT.Statementcode=STI.Statementcode
		WHERE STI.FieldName='PDF_Status' 

		INSERT INTO RPWeb_StatementInformation(StatementCode,FieldName,FieldValue)
		SELECT StatementCode, 'PDF_Status', 'Ready For PDF Creation' 			
		FROM @TARGET_STATEMENTS 
		
	
	--	EXEC RP_GenerateEditHistory 'Statement' ,@USERNAME ,@EDIT_HISTORY
	
	END
					   		 	  	  	 
	/********************* UNLOCK PROCESS *****************************/
	-- ChequeStatus : ADJUSTMENT,  VOID,  VOID EFT, DELETED (Cheque deleted from status 'New')
	
	DELETE @TARGET_STATEMENTS
	INSERT INTO @TARGET_STATEMENTS (Statementcode)
	SELECT T1.Statementcode 
	FROM @CHEQUES	T1
		LEFT JOIN (
			SELECT Statementcode 
			FROM @CHEQUES
			WHERE [ChequeStatus]='New'
			--WHERE [ChequeStatus] NOT IN ('Adjustment','Void','Void EFT')
		)T2
		ON T2.Statementcode=T1.Statementcode 
	WHERE T1.[ChequeStatus] IN ('Deleted','Void','Void EFT')
		AND T2.Statementcode IS NULL
	
	IF EXISTS(SELECT * FROM @TARGET_STATEMENTS)
	BEGIN
		SET @RUN_REFRESH_PAYMENTINFO=1

		INSERT INTO @TARGET_STATEMENTS (Statementcode)
		SELECT StatementCode	FROM RPWeb_Statements 	WHERE [Status]='Active' AND [VersionType]='Archive' AND DataSetCode IN (SELECT Statementcode FROM @TARGET_STATEMENTS)	 
			UNION
		SELECT StatementCode	FROM RPWeb_Statements 	WHERE [Status]='Active' AND [VersionType]='Archive' AND DataSetCode_L5 IN (SELECT Statementcode FROM @TARGET_STATEMENTS)	
	   
		/* Update Archive Statement Status to 'void' */
		UPDATE RPWeb_Statements 
		SET [Status]='Void'
		WHERE [VersionType]='Archive' AND [StatementCode] IN (SELECT Statementcode FROM @TARGET_STATEMENTS)
	

		/* UPDATE UNLOCKED STATEMENT STATUS TO READY FOR QA */	
		DELETE @EDIT_HISTORY

		UPDATE STI
		SET [FieldValue]='Ready For QA'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Void Cheque/EFT','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI
			INNER JOIN RPWeb_Statements ARC		ON ARC.[SRC_StatementCode]=STI.[StatementCode]
			INNER JOIN @TARGET_STATEMENTS TGT	ON TGT.Statementcode=ARC.Statementcode
		WHERE STI.[FieldName]='Status' AND STI.[FieldValue]='Cheque Created' --  IN ('Cheque Created','Ready For PDF Creation')
	
		EXEC RP_GenerateEditHistory 'Statement',@USERNAME ,@EDIT_HISTORY
		DELETE @EDIT_HISTORY
	END -- 'Adjustment','Void','Void EFT','Deleted'


	/**************************************************************************************************/
	-- CHEQUE STATUS : Sent , Submitted To Bank
	DELETE @TARGET_STATEMENTS
	INSERT INTO @TARGET_STATEMENTS (Statementcode)
	SELECT Statementcode 	FROM @CHEQUES	WHERE [ChequeStatus] IN ('Sent','Submitted To Bank','Cleared')
	
	IF EXISTS(SELECT * FROM @TARGET_STATEMENTS)
	BEGIN
		SET @RUN_REFRESH_PAYMENTINFO=1

		INSERT INTO @TARGET_STATEMENTS (Statementcode)
		SELECT StatementCode	FROM RPWeb_Statements 	WHERE [Status]='Active'  AND [VersionType]='Archive' AND DataSetCode IN (SELECT Statementcode FROM @Cheques)	 
			UNION
		SELECT StatementCode	FROM RPWeb_Statements 	WHERE [Status]='Active'  AND [VersionType]='Archive' AND DataSetCode_L5 IN (SELECT Statementcode FROM @Cheques)	

		/*
		-- Unlocked statement should go back 'Open' status  
		-- 'Remove QA Assignments  
		*/
			
		DELETE @EDIT_HISTORY 

		/* UPDATE UNLOCKED STATEMENT STATUS TO OPEN */
		UPDATE STI
		SET [FieldValue]='Open'
		OUTPUT inserted.[StatementCode],inserted.[StatementCode],'Cheque Sent','Field','Status',deleted.[FieldValue],inserted.[FieldValue]
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue])
		FROM RPWeb_StatementInformation STI
			INNER Join RPWeb_Statements ARC		ON ARC.[SRC_StatementCode]=STI.[StatementCode]
			INNER JOIN @TARGET_STATEMENTS TGT	ON TGT.Statementcode=ARC.Statementcode
		WHERE STI.[FieldName]='Status' AND STI.[FieldValue] IN ('Cheque Created','Ready For PDF Creation') 
				
		DELETE STI
		OUTPUT deleted.[StatementCode],deleted.[StatementCode],'Cheque Sent/EFT QA Completed','Field','QA Assignment',deleted.[FieldValue],NULL
		INTO @EDIT_HISTORY([StatementCode],[EHLinkCode],[TagData],[EHEntryType],[EHFieldName],[EHOriginalValue],[EHNewValue]) 
		FROM RPWeb_StatementInformation STI
			INNER JOIN RPWeb_Statements ARC		ON ARC.SRC_Statementcode=STI.Statementcode
			INNER JOIN @TARGET_STATEMENTS TGT	ON TGT.Statementcode=ARC.Statementcode
		WHERE STI.[FieldCategory]='Assignment' 
	
		
		EXEC RP_GenerateEditHistory 'Statement',@USERNAME ,@EDIT_HISTORY

	END --'Sent','Submitted To Bank'
			   		 


	IF @RUN_REFRESH_PAYMENTINFO=1
	BEGIN

		DELETE @TARGET_STATEMENTS

		INSERT INTO @TARGET_STATEMENTS(StatementCode)
		SELECT DISTINCT ST.SRC_Statementcode 
		FROM @CHEQUES	C
			INNER JOIN RPWEB_Statements ST
			ON ST.StatementCode=C.StatementCode
		WHERE C.[Unlocked]=1 OR C. [ChequeStatus] IN ('Sent','Submitted To Bank')
				
		EXEC RP_RefreshPaymentInformation @SEASON, 'ACTUAL', @TARGET_STATEMENTS;	
	END


END	   	
END TRY

BEGIN CATCH
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;

	SELECT  'Failed' AS Status
        ,ERROR_NUMBER() AS ErrorNumber                      
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  	
	RETURN 
END CATCH

IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  

SELECT 'Success' AS Status 
	
END




